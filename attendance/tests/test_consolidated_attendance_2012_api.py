from datetime import timedelta
from pprint import pprint

from django.test import Client
from django.test import TestCase

from accounts.models import student, staff, active_batches, CustomUser, programme, department_programmes,\
    regulation, batch
from curriculum.models import courses, attendance, semester, semester_migration, programme_coordinator, \
    department_sections, section_students
from django.utils import timezone
from attendance.views.ConsolidatedAttendance2012API import ConsolidatedAttendance2012API
from enrollment.models import course_enrollment
from unittest.case import skip



class TestConsolidatedAttendance2012API(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choosen_course_1 = courses.objects.get(course_id='12S105')
        self.choosen_course_2 = courses.objects.get(course_id='12S106')
        self.choose_student_1 = student.objects.get(user__email='s1@gmail.com')
        self.choose_student_2 = student.objects.get(user__email='s2@gmail.com')
        # change the regulation of both students
        batch_instance = batch.objects.get(start_year=2013)
        self.choose_student_1.batch = batch_instance        
        self.choose_student_1.save()
        self.choose_student_1.user.is_approved = True
        self.choose_student_1.user.save()
        self.choose_student_2.batch = batch_instance
        self.choose_student_2.user.is_approved = True
        self.choose_student_2.user.save()
        self.choose_student_2.save()
        self.choose_staff = staff.objects.get(user__email='fa1@gmail.com')
        self.choosen_date_1 = timezone.now().date()
        self.choosen_date_2 = timezone.now().date() + timedelta(days=1)

        active_batch_inst = active_batches.objects.filter(department__acronym='CSE').filter(programme='UG').get(
            current_semester_number_of_this_batch=1)
        self.choosen_batch = active_batch_inst.batch



        # create two sections
        self.dept_sec_A = department_sections.objects.get(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='A'
        )
        self.dept_sec_A.save()
        self.dept_sec_B = department_sections(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='B'
        )
        self.dept_sec_B.save()


#     def enroll_students(self):
#         instance = course_enrollment(semester=self.expected_semester,
#                                      course=self.choosen_course,
#                                      student=self.choose_student_1,
#                                      requested=True,
#                                      enrolled_date=timezone.now().date(),
#                                      registered=True,
#                                     registered_date = timezone.now().date(),
#                                     approved = True,
#                                      )
#         instance.save()
# 
#         instance = course_enrollment(semester=self.expected_semester,
#                                      course=self.choosen_course,
#                                      student=self.choose_student_2,
#                                      requested=True,
#                                      enrolled_date=timezone.now().date(),
#                                      registered=True,
#                                      registered_date=timezone.now().date(),
#                                      approved=True,
#                                      )
#         instance.save()

    def add_students_to_section(self):
        section_students.objects.create(
            student = self.choose_student_1,
            section = self.dept_sec_A,
            )
        section_students.objects.create(
            student = self.choose_student_2,
            section = self.dept_sec_A,
            )

    def create_semester_plan(self):
        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.choosen_batch.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        print(vars(response))
        self.expected_semester = semester.objects.filter(batch=self.choosen_batch).filter(
            faculty_advisor=self.choose_staff)
        self.assertTrue(
            self.expected_semester.exists()
        )

        self.assertTrue(
            semester_migration.objects.filter(semester=self.expected_semester).exists()
        )

        self.expected_semester = self.expected_semester.get()
        self.expected_semester.department_section = self.dept_sec_A
        self.expected_semester.save()
        
        self.expected_semester = semester.objects.filter(batch=self.choosen_batch).filter(
            faculty_advisor=self.choose_staff).get(department_section = self.dept_sec_A )

    def create_attendance_entries(self):
        attendance_inst = attendance(
            course=self.choosen_course_1,
            date=self.choosen_date_1,
            hour=1,
            staff=self.choose_staff,
            semester = self.expected_semester,
            grant_period = timezone.now(),
        )
        attendance_inst.save()
        attendance_inst.present_students.add(self.choose_student_1)
        attendance_inst.absent_students.add(self.choose_student_2)
        attendance_inst.save()
        
        attendance_inst = attendance(
            course=self.choosen_course_2,
            date=self.choosen_date_1,
            hour=5,
            staff=self.choose_staff,
            semester = self.expected_semester,
            grant_period = timezone.now(),
        )
        attendance_inst.save()
        attendance_inst.present_students.add(self.choose_student_1)
        attendance_inst.present_students.add(self.choose_student_2)
        attendance_inst.save()

        attendance_inst = attendance(
            course=self.choosen_course_1,
            date=self.choosen_date_2,
            hour=1,
            staff=self.choose_staff,
            semester=self.expected_semester,
            grant_period=timezone.now(),
        )
        attendance_inst.save()
        attendance_inst.present_students.add(self.choose_student_1)
        attendance_inst.absent_students.add(self.choose_student_2)
        attendance_inst.save()        
        
        attendance_inst = attendance(
            course=self.choosen_course_2,
            date=self.choosen_date_2,
            hour=6,
            staff=self.choose_staff,
            semester=self.expected_semester,
            grant_period=timezone.now(),
        )
        attendance_inst.save()
        attendance_inst.absent_students.add(self.choose_student_1)
        attendance_inst.absent_students.add(self.choose_student_2)
        attendance_inst.save()

    def login_pc(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        programme_inst = programme.objects.create(
            name="Under Graduate",
            acronym="UG"
        )
        department_programmes_inst = department_programmes.objects.create(
            department=self.choose_staff.department,
            programme=programme_inst
        )
        pc_table_inst = programme_coordinator.objects.create(
            department_programme=department_programmes_inst
        )
        pc_table_inst.programme_coordinator_staffs.add(self.choose_staff)
        pc_table_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def test_get_total_working_hours(self):
        self.login_pc()
        self.create_semester_plan()
#         self.enroll_students()
        self.create_attendance_entries()
        
        consolidated_attendance_2012_api_instance = ConsolidatedAttendance2012API()
        consolidated_attendance_2012_api_instance.start_date_instance = self.choosen_date_1
        consolidated_attendance_2012_api_instance.end_date_instance = self.choosen_date_2
        consolidated_attendance_2012_api_instance.semester_instance = self.expected_semester
        
        self.assertEqual(consolidated_attendance_2012_api_instance.get_total_working_days_count(
            consolidated_attendance_2012_api_instance.get_attendance_queryset(), self.choose_student_2
            ), 2)
        
        
        #add third day attendance for today and check whether output is 2
        
        attendance_inst = attendance(
            course=self.choosen_course_1,
            date=self.choosen_date_2,
            hour=2,
            staff=self.choose_staff,
            semester=self.expected_semester,
            grant_period=timezone.now(),
        )
        attendance_inst.save()
        
        self.assertEqual(consolidated_attendance_2012_api_instance.get_total_working_days_count(
            consolidated_attendance_2012_api_instance.get_attendance_queryset(), self.choose_student_2
            ), 2)

    def test_get_report_as_staff(self):
        self.login_pc()
        self.create_semester_plan()
        self.add_students_to_section()
#         self.enroll_students()
        self.create_attendance_entries()
        
        user_instance = self.choose_staff.user
        expected_list = []
        temp = {}
        temp['student_instance'] = self.choose_student_2
        temp['total_days'] = 2.0
        temp['absent_days'] = 1.5
        temp['present_days'] = 0.5
        temp['percentage'] = format(25,'.2f')
        expected_list.append(temp)
        temp={}
        temp['student_instance'] = self.choose_student_1
        temp['total_days'] = 2.0
        temp['absent_days'] = 0.5
        temp['present_days'] = 1.5
        temp['percentage'] = format(75,'.2f')
        expected_list.append(temp)

        consolidated_attendance_2012_api_instance = ConsolidatedAttendance2012API()
        consolidated_attendance_2012_api_instance.start_date_instance = self.choosen_date_1
        consolidated_attendance_2012_api_instance.end_date_instance = self.choosen_date_2
        consolidated_attendance_2012_api_instance.semester_instance = self.expected_semester

        got_list = consolidated_attendance_2012_api_instance.get_report_as_dictionary(user_instance)

        pprint(expected_list)
        pprint(got_list)
        self.assertEqual(expected_list,got_list)
        
        
        
        
    def test_get_report_as_student(self):
        self.login_pc()
        self.create_semester_plan()
#         self.enroll_students()
        self.create_attendance_entries()
        self.add_students_to_section()

        user_instance = self.choose_student_2.user
        expected_list = []
        temp = {}
        temp['student_instance'] = self.choose_student_2
        temp['total_days'] = 2.0
        temp['absent_days'] = 1.5
        temp['present_days'] = 0.5
        temp['percentage'] = format(25, '.2f')
        expected_list.append(temp)

        consolidated_attendance_2012_api_instance = ConsolidatedAttendance2012API()
        consolidated_attendance_2012_api_instance.start_date_instance = self.choosen_date_1
        consolidated_attendance_2012_api_instance.end_date_instance = self.choosen_date_2
        consolidated_attendance_2012_api_instance.semester_instance = self.expected_semester

        got_list = consolidated_attendance_2012_api_instance.get_report_as_dictionary(user_instance)

        pprint(expected_list)
        pprint(got_list)
        self.assertEqual(expected_list, got_list)
