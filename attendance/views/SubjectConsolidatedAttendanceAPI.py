from pprint import pprint

from django.db.models import Q

from accounts.models import student, staff
from curriculum.models import attendance
from curriculum.views.common_includes import get_list_of_student_queryset

class SubjectConsolidatedAttendanceAPI:

    start_date_instance = None
    end_date_instance = None
    course_instance = None
    semester_instance = None

    def __init__(self):
        self.start_date_instance = None
        self.end_date_instance = None
        self.course_instance = None
        self.semester_instance = None

    def get_student_list(self, CustomUserInstance):
        student_query = student.objects.filter(user=CustomUserInstance)
        if student_query.exists():
            student_inst = student_query.get()
            student_list = []
            student_list.append(student_inst)
            return student_list
        staff_query = staff.objects.filter(user=CustomUserInstance)
        if staff_query.exists():
            return get_list_of_student_queryset(self.semester_instance,self.course_instance)

        return None

    def is_allowed_to_access(self, request_instance):
        pass

    def get_total_days(self, queryset,student_instance):
        return queryset.filter(present_students = student_instance).count() + \
               queryset.filter(onduty_students = student_instance).count() + \
               queryset.filter(absent_students = student_instance).count()

    def get_present_days(self, queryset, student_instance):
        return queryset.filter(present_students = student_instance).count() + queryset.filter(onduty_students = student_instance).count()

    def get_attendance_queryset(self):
        return attendance.objects.filter(
            semester = self.semester_instance,
            course = self.course_instance,
            date__gte = self.start_date_instance,
            date__lte = self.end_date_instance
        ).order_by('date')


    def get_percentage(self, queryset, student_instance):
        try:
            return (self.get_present_days(queryset,student_instance) / self.get_total_days(queryset,student_instance)) * 100
        except ZeroDivisionError:
            return 0

    def get_report_as_dictionary(self, CustomUserInstance):
        list_of_students = self.get_student_list(CustomUserInstance)
        result_list = []
        pprint(list_of_students)
        queryset = self.get_attendance_queryset()
        for stud_inst in list_of_students:
            temp = {}
            temp['student_instance'] = stud_inst
            temp['present_days'] = self.get_present_days(queryset,stud_inst)
            temp['total_days'] = self.get_total_days(queryset,stud_inst)
            try:
                temp['percentage'] = format((temp['present_days'] / temp['total_days']) * 100,'.2f')
            except ZeroDivisionError:
                temp['percentage'] = 0
            result_list.append(temp)

        return result_list

    def get_absent_days_queryset(self, queryset, student_instance):
        return queryset.filter(absent_students=student_instance)


    def get_absent_date_and_hour_as_dictionary(self,CustomUserInstance):
        list_of_students = self.get_student_list(CustomUserInstance)
        print('list of students dict')
        pprint(list_of_students)
        queryset = self.get_attendance_queryset()
        result_dict = {}
        for stud_inst in list_of_students:
            absent_queryset = self.get_absent_days_queryset(queryset,stud_inst)
            date_hour_list = []
            for entry in absent_queryset:
                temp1 = {}
                temp1['date'] = entry.date
                temp1['hour'] = entry.hour
                date_hour_list.append(temp1)
            result_dict[stud_inst.roll_no] = date_hour_list
        return result_dict

