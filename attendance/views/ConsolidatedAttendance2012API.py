from accounts.models import student, staff
from curriculum.views.common_includes import get_list_of_students_of_semester_queryset
from django.db.models import Q
from curriculum.models import attendance
from pprint import pprint


class ConsolidatedAttendance2012API:
    start_date_instance = None
    end_date_instance = None
    semester_instance = None

    fn_absent_days = []
    an_absent_days = []

    def __init__(self):
        self.start_date_instance = None
        self.end_date_instance = None
        self.semester_instance = None
        self.working_days_list = []

    def get_student_list(self, CustomUserInstance):
        student_query = student.objects.filter(user=CustomUserInstance)
        if student_query.exists():
            student_inst = student_query.get()
            student_list = []
            if student_inst.batch.regulation.start_year == 2012:
                student_list.append(student_inst)
            return student_list
        staff_query = staff.objects.filter(user=CustomUserInstance)
        if staff_query.exists():
            return get_list_of_students_of_semester_queryset(self.semester_instance)

        return None

    def is_allowed_to_access(self, request_instance):
        pass

    def get_total_working_days_count(self, queryset, student_instance):
        fn_working_days = []
        an_working_days = []
        for entry in queryset.filter(present_students = student_instance):
            if entry.hour < 5:
                if entry.date not in fn_working_days:
                    fn_working_days.append(entry.date)
            else:
                if entry.date not in an_working_days:
                    an_working_days.append(entry.date)
        for entry in queryset.filter(absent_students = student_instance):
            if entry.hour < 5:
                if entry.date not in fn_working_days:
                    fn_working_days.append(entry.date)
            else:
                if entry.date not in an_working_days:
                    an_working_days.append(entry.date)
        for entry in queryset.filter(onduty_students = student_instance):
            if entry.hour < 5:
                if entry.date not in fn_working_days:
                    fn_working_days.append(entry.date)
            else:
                if entry.date not in an_working_days:
                    an_working_days.append(entry.date)
        return (len(fn_working_days) * 0.5) + (len(an_working_days) * 0.5)
                
                

    def get_absent_days_count(self, queryset, student_instance):
        queryset_for_this_student = queryset.filter(
            absent_students = student_instance
            )
        self.fn_absent_days = []
        self.an_absent_days = []
        for entry in queryset_for_this_student:
            if entry.hour < 5:
                if entry.date not in self.fn_absent_days:
                    self.fn_absent_days.append(entry.date)
            else:
                if entry.date not in self.an_absent_days:
                    self.an_absent_days.append(entry.date)
        
        return (len(self.fn_absent_days) * 0.5) + (len(self.an_absent_days) * 0.5)

        

    def get_attendance_queryset(self):
        return attendance.objects.filter(
            semester = self.semester_instance,
            date__gte = self.start_date_instance,
            date__lte = self.end_date_instance
        ).order_by('date')

    def get_report_as_dictionary(self, CustomUserInstance):
        list_of_students = self.get_student_list(CustomUserInstance)
        result_list = []
        print('list of students dict')
        pprint(list_of_students)
        queryset = self.get_attendance_queryset()
        for stud_inst in list_of_students:
            temp = {}
            temp['student_instance'] = stud_inst
            temp['total_days'] = self.get_total_working_days_count(queryset,stud_inst)
            temp['absent_days'] = self.get_absent_days_count(queryset,stud_inst)
            temp['present_days'] = temp['total_days'] - temp['absent_days']
            temp['fn_absent_days'] = self.fn_absent_days
            temp['an_absent_days'] = self.an_absent_days
            try:
                temp['percentage'] = format((temp['present_days'] / temp['total_days']) * 100,'.2f')
            except ZeroDivisionError:
                temp['percentage'] = 0
            result_list.append(temp)

        return result_list