from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from easy_pdf.views import PDFTemplateView

from accounts.models import staff, department
from curriculum.views.common_includes import get_active_semesters_tabs_of_department,\
    get_active_semesters_tabs_for_overall_reports
    
from pprint import pprint
from curriculum.models import semester_migration, semester
from django.utils import timezone

from attendance.views.ConsolidatedAttendance2012API import ConsolidatedAttendance2012API


@login_required
def consolidated_attendance_2012_report(request, semester_pk=None, data_for_pdf=False):
    
    staff_instance = staff.objects.get(user=request.user)

    all_departments = department.objects.filter(is_core=True)
    user_is_pricipal_or_coe = False

    selected_department_pk = None
    if request.method == "POST" and 'selected_department_pk' in request.POST:
        selected_department_pk = int(request.POST.get('selected_department_pk'))
    if request.user.groups.filter(name='principal').exists() or request.user.groups.filter(name='coe_staff').exists():
        if not selected_department_pk:
            selected_department = all_departments.first()
            selected_department_pk = selected_department.pk
        else:
            selected_department = department.objects.get(pk=selected_department_pk)
        avaliable_semesters = get_active_semesters_tabs_of_department(selected_department)
        user_is_pricipal_or_coe = True
    else:
        selected_department = staff_instance.department
        selected_department_pk = selected_department.pk
        avaliable_semesters = get_active_semesters_tabs_for_overall_reports(request)

    if not avaliable_semesters:
        modal = {
            'heading': 'Error',
            'body': 'No semester plan has been created for your access',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    pprint(avaliable_semesters)
    filtered_available_semesters = []
    for entry in avaliable_semesters:
        if entry['semester_instance'].batch.regulation.start_year == 2012:
            print('filterd semseter')
            filtered_available_semesters.append(entry)
    avaliable_semesters = filtered_available_semesters

    if request.user.groups.filter(name='faculty_advisor').exists() and not \
            request.user.groups.filter(name='hod').exists() and not user_is_pricipal_or_coe:
        new_avaliable_semesters = []
        semesters_as_fa = semester_migration.objects.filter(
            semester__faculty_advisor=staff_instance
        )
        for entry in semesters_as_fa:
            new_avaliable_semesters.append(entry.semester)

        pprint('new semesters ' + str(new_avaliable_semesters))

        final_semesters = []
        for entry in avaliable_semesters:
            print(entry)
            sem_inst = semester.objects.get(pk=entry['semester_instance'].pk)
            if sem_inst in new_avaliable_semesters:
                print((sem_inst))
                final_semesters.append(entry)

        pprint('final batches = ' + str(final_semesters))
        if final_semesters:
            avaliable_semesters = final_semesters

    try:
        selected_semester_pk = avaliable_semesters[0]['semester_instance'].pk
    except:
        modal = {
            'heading': 'Error',
            'body': 'No semesters available',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    if request.method == "POST" and 'semester_pk' in request.POST:
        selected_semester_pk = int(request.POST.get('semester_pk'))

    if semester_pk:
        selected_semester_pk = int(semester_pk)

    print(selected_semester_pk)

    selected_semester = semester.objects.get(pk=selected_semester_pk)

    start_date = selected_semester.start_term_1
    if start_date is None:
        msg = {
            'page_title': 'Date Error',
            'title': 'Start Date Not Found',
            'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    end_date = selected_semester.end_term_3
    if end_date is None:
        end_date = timezone.now().date()
        
    consolidated_attendance_2012_api_instance = ConsolidatedAttendance2012API()
    consolidated_attendance_2012_api_instance.semester_instance = selected_semester
    consolidated_attendance_2012_api_instance.start_date_instance = start_date
    consolidated_attendance_2012_api_instance.end_date_instance = end_date

    result_list = consolidated_attendance_2012_api_instance.get_report_as_dictionary(request.user)

    context_data = {
                      'all_departments': all_departments,
                      'selected_department_pk': selected_department_pk,
                      'semesters': avaliable_semesters,
                      'selected_semester_pk': selected_semester_pk,
                      'final_result': result_list,
                      'current_start_date': start_date, 'current_end_date': end_date,
                  }
    if data_for_pdf:
        return context_data
    
    return render(request, 'attendance/consolidated_attendance_2012_report.html',context_data)

class overallConsolidated2012PdfReport(PDFTemplateView):
    template_name = "attendance/consolidated_attendance_2012_pdf_report.html"
    semester_pk = None
    def get(self, request, *args, **kwargs):
        self.semester_pk = kwargs.pop('semester_pk')
        return super(overallConsolidated2012PdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = consolidated_attendance_2012_report(self.request,
                                                        semester_pk=self.semester_pk,
                                                        data_for_pdf=True
                                                        )
        print(context_data)
        return super(overallConsolidated2012PdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )
