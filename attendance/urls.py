from django.conf.urls import url
from attendance.views.consolidated_attendance_2012_report import consolidated_attendance_2012_report, overallConsolidated2012PdfReport

urlpatterns = [
    url(r'^consolidated_attendance_2012_report', consolidated_attendance_2012_report, name='consolidated_attendance_2012_report'),
    url(r'^consolidated_attendance_2012_pdf_report/(?P<semester_pk>\d+)', overallConsolidated2012PdfReport.as_view(), name='consolidated_attendance_2012_pdf_report'),
]
