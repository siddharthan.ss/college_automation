from datetime import datetime

from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.shortcuts import render, redirect
from django.urls import reverse
from easy_pdf.views import PDFTemplateView
from django.http import JsonResponse

from accounts.models import staff
from curriculum.models import staff_course
from quiz.models import QuizTime, QuizQuestion, WrapQuizTime, WrapQuizQuestion
from curriculum.views.common_includes import get_list_of_students
from common.utils.ReportTabsUtil import getCoursesHandlingByStaff


def canViewQuiz(user):
    if user.has_perm('marks.can_add_internal_marks') or \
            user.has_perm('curriculum.can_view_overall_reports'):
        return True


def getQuizMarks(staffCourseInstance, unitTestNumber):
    dictionary = {}
    semesterInstance = staffCourseInstance.semester
    courseInstance = staffCourseInstance.course

    studentList = []
    dictionary['wrapQuizAvail'] = 'false'

    if not QuizTime.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber):
        dictionary['avail'] = 'false'
        return dictionary

    quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)
    dictionary['avail'] = 'true'

    quizQuestionList = QuizQuestion.objects.filter(quizTime=quizTimeInstance).order_by('questionNumber')

    maximumMarks = 0
    for each in quizQuestionList:
        maximumMarks = maximumMarks + each.markAllotted

    dictionary['maximumMarks'] = maximumMarks
    dictionary['staffCoursePk'] = staffCourseInstance.pk
    dictionary['unitTestNumber'] = unitTestNumber

    for eachStudent in get_list_of_students(semesterInstance, courseInstance):
        studentInstance = eachStudent['student_obj']
        temp = {}
        totalMarks = 0

        if WrapQuizTime.objects.filter(student=studentInstance, quizTime=quizTimeInstance):
            wrapQuizTimeInstance = WrapQuizTime.objects.get(student=studentInstance, quizTime=quizTimeInstance)
            dictionary['wrapQuizAvail'] = 'true'
            temp['attended'] = 'true'

            for eachQuestion in quizQuestionList:
                wrapQuizQuestionInstance = WrapQuizQuestion.objects.get(wrapQuizTime=wrapQuizTimeInstance,
                                                                        quizQuestion=eachQuestion)
                if wrapQuizQuestionInstance.isCorrect:
                    totalMarks = totalMarks + eachQuestion.markAllotted
        else:
            temp['attended'] = 'false'

        temp['studentName'] = str(eachStudent['student_obj'])
        temp['registerNumber'] = eachStudent['student_obj'].roll_no
        temp['studentPk'] = eachStudent['student_obj'].pk

        temp['totalMarks'] = totalMarks
        studentList.append(temp)

    dictionary['studentList'] = studentList
    return dictionary


@login_required
@permission_required('marks.can_add_internal_marks')
def setQuiz(request):
    staffInstance = staff.objects.get(user=request.user)

    courseList = []
    for each in getCoursesHandlingByStaff(staffInstance):
        if each['staffCourseInstance'].course.subject_type == 'T':
            courseList.append(each)

    try:
        staffCoursePk = courseList[0]['staffCourseInstance'].pk
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course that have quiz marks',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if request.is_ajax():
            if request.POST.get('action') == 'unitTestSelection':
                unitTestNumber = int(request.POST.get('unitTestNumber'))
                staffCoursePk = str(request.POST.get('hiddenStaffCoursePk'))
                staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

                dictionary = {}
                if QuizTime.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).exists():
                    dictionary['avail'] = 'true'
                    quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)
                    if quizTimeInstance.isActive:
                        dictionary['active'] = 'true'
                    else:
                        dictionary['active'] = 'false'
                else:
                    dictionary['avail'] = 'false'

                return JsonResponse(dictionary)

            if request.POST.get('action') == 'deactivateQuiz':
                unitTestNumber = int(request.POST.get('unitTestNumber'))
                staffCoursePk = str(request.POST.get('hiddenStaffCoursePk'))
                staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

                dictionary = {}
                quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)

                quizTimeInstance.isActive = False
                quizTimeInstance.save()
                return JsonResponse(dictionary)

        if 'staffCoursePk' in request.POST:
            staffCoursePk = request.POST.get('staffCoursePk')

        if 'submitPattern' in request.POST:
            staffCoursePk = str(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            unitTestNumber = int(request.POST.get('unitTestNumber'))
            totalQuestions = int(request.POST.get('totalQuestions'))
            quizTimeInstance = QuizTime(staffCourse=staffCourseInstance, unitTest=unitTestNumber)
            quizTimeInstance.save()
            counter = 1
            while counter <= totalQuestions:
                totalOptions = str(request.POST.get('subQuestion-' + str(counter)))
                question = str(request.POST.get('question-' + str(counter)))
                markAllotted = str(request.POST.get('mark-' + str(counter)))
                rightAnswer = request.POST.get('answer-' + str(counter))
                # print('totaloption ' + str(counter) + ':' + str(totalOptions))
                # print('question ' + str(counter) + ':' + str(question))
                # print('mark ' + str(counter) + ':' + str(markAllotted))
                # print('rightAnswer='+str(rightAnswer))

                try:
                    option1 = str(request.POST.get('option-' + str(counter) + '-1'))
                except:
                    option1 = None
                try:
                    option2 = str(request.POST.get('option-' + str(counter) + '-2'))
                except:
                    option2 = None
                try:
                    option3 = str(request.POST.get('option-' + str(counter) + '-3'))
                except:
                    option3 = None
                try:
                    option4 = str(request.POST.get('option-' + str(counter) + '-4'))
                except:
                    option4 = None
                try:
                    option5 = str(request.POST.get('option-' + str(counter) + '-5'))
                except:
                    option5 = None

                if rightAnswer == None:
                    rightAnswer = 0

                quizQuestionInstance = QuizQuestion(quizTime=quizTimeInstance, questionNumber=counter,
                                                    totalOptions=totalOptions, question=question, option1=option1,
                                                    option2=option2, rightAnswer=rightAnswer, markAllotted=markAllotted)
                quizQuestionInstance.save()
                if int(totalOptions) == 3:
                    quizQuestionInstance.option3 = option3
                elif int(totalOptions) == 4:
                    quizQuestionInstance.option3 = option3
                    quizQuestionInstance.option4 = option4
                elif int(totalOptions) == 5:
                    quizQuestionInstance.option3 = option3
                    quizQuestionInstance.option4 = option4
                    quizQuestionInstance.option5 = option5
                quizQuestionInstance.save()
                counter = counter + 1

        if 'setQuizTime' in request.POST:
            unitTestNumber = request.POST.get('unitTestNumber')
            staffCoursePk = request.POST.get('hiddenStaffCoursePk')
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            startDate = None
            endDate = None
            startTime = None
            endTime = None
            timeDuration = None

            quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)
            if quizTimeInstance.startTime:
                startDateTime = quizTimeInstance.startTime
                endDateTime = quizTimeInstance.endTime

                startDate = datetime.date(startDateTime).strftime('%d/%m/%Y')
                endDate = datetime.date(endDateTime).strftime('%d/%m/%Y')

                startTime = datetime.time(startDateTime).strftime('%I:%M %p')
                endTime = datetime.time(endDateTime).strftime('%I:%M %p')

                timeDuration = quizTimeInstance.timeDuration

            return render(request, 'quiz/setQuizTime.html',
                          {
                              'staffCourseInstance': staffCourseInstance,
                              'unitTestNumber': unitTestNumber,
                              'startDate': startDate,
                              'startTime': startTime,
                              'endDate': endDate,
                              'endTime': endTime,
                              'timeDuration': timeDuration

                          })

        if 'confirmQuizTime' in request.POST:
            unitTestNumber = request.POST.get('hiddenUnitTestNumber')
            startDate = request.POST.get('startDate')
            endDate = request.POST.get('endDate')
            startTime = request.POST.get('startTime')
            endTime = request.POST.get('endTime')
            timeDuration = request.POST.get('testDuration')
            staffCoursePk = request.POST.get('hiddenStaffCoursePk')
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            startDate = datetime.strptime(startDate, '%d/%m/%Y').strftime('%Y-%m-%d')
            startDate = datetime.strptime(startDate, '%Y-%m-%d').strftime('%x')
            startDate = datetime.strptime(startDate, '%x')

            startTime = datetime.strptime(startTime, '%I:%M %p')
            startTime = datetime.time(startTime)
            start_datetime = datetime.combine(startDate, startTime)

            endDate = datetime.strptime(endDate, '%d/%m/%Y').strftime('%Y-%m-%d')
            endDate = datetime.strptime(endDate, '%Y-%m-%d').strftime('%x')
            endDate = datetime.strptime(endDate, '%x')

            endTime = datetime.strptime(endTime, '%I:%M %p')
            endTime = datetime.time(endTime)
            end_datetime = datetime.combine(endDate, endTime)

            quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)
            quizTimeInstance.startTime = start_datetime
            quizTimeInstance.endTime = end_datetime
            quizTimeInstance.timeDuration = timeDuration
            quizTimeInstance.save()

        if 'deletePattern' in request.POST:
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))
            unitTestNumber = int(request.POST.get('hiddenUnitTestNumber'))

            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getQuizMarks(staffCourseInstance, unitTestNumber)

            if dictionary['wrapQuizAvail'] == 'true':
                return render(request, 'quiz/preDeleteQuizConfirmation.html',
                              {
                                  'staffCourseInstance': staffCourseInstance,
                                  'unitTestNumber': unitTestNumber,
                                  'dictionary': dictionary
                              })
            else:
                QuizTime.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).delete()

        if 'confirmDeleteQuiz' in request.POST:
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))
            unitTestNumber = int(request.POST.get('hiddenUnitTestNumber'))

            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            QuizTime.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).delete()

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    return render(request, 'quiz/setQuiz.html',
                  {
                      'staffCourseInstance': staffCourseInstance,
                      'courseList': courseList,
                  })


@login_required
@permission_required('marks.can_add_internal_marks')
def viewQuizQuestions(request, staffCoursePk, unitTestNumber, data_for_pdf=False):
    staffInstance = staff.objects.get(user=request.user)

    try:
        staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    except:
        msg = {
            'page_title': 'Access Denied',
            'title': 'Access Denied',
            'description': 'No Page found for this url',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if staffInstance not in staffCourseInstance.staffs.all():
        msg = {
            'page_title': 'No access',
            'title': 'No access to this course',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to this course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)
    quizQuestionList = QuizQuestion.objects.filter(quizTime=quizTimeInstance).order_by('questionNumber')

    if data_for_pdf:
        context_data = {
            'quizQuestionList': quizQuestionList,
            'staffCourseInstance': staffCourseInstance,
            'unitTestNumber': unitTestNumber

        }
        return context_data

    return render(request, 'quiz/viewQuizQuestions.html',
                  {
                      'quizQuestionList': quizQuestionList,
                      'staffCourseInstance': staffCourseInstance,
                      'unitTestNumber': unitTestNumber
                  })


@login_required
@permission_required('marks.can_add_internal_marks')
def editQuizQuestions(request, staffCoursePk, unitTestNumber):
    staffInstance = staff.objects.get(user=request.user)

    try:
        staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    except:
        msg = {
            'page_title': 'Access Denied',
            'title': 'Access Denied',
            'description': 'No Page found for this url',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if staffInstance not in staffCourseInstance.staffs.all():
        msg = {
            'page_title': 'No access',
            'title': 'No access to this course',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to this course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)

    if quizTimeInstance.isActive:
        msg = {
            'page_title': 'Edit Error',
            'title': 'Deactivation Required!',
            'description': 'In order to edit the quiz contents, you have to deactivate the quiz first.',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    quizQuestionList = QuizQuestion.objects.filter(quizTime=quizTimeInstance).order_by('questionNumber')

    if request.method == 'POST':
        for question in quizQuestionList:
            quizQuestionInstance = question
            quizQuestionInstance.question = str(request.POST.get('question-' + str(question.questionNumber)))
            answer = request.POST.get('answer-' + str(question.questionNumber))
            if not answer:
                answer = 0
            quizQuestionInstance.rightAnswer = answer
            if quizQuestionInstance.option1 or question.option1 == '':
                quizQuestionInstance.option1 = str(request.POST.get('option-' + str(question.questionNumber) + '-1'))
            if quizQuestionInstance.option2 or question.option2 == '':
                quizQuestionInstance.option2 = str(request.POST.get('option-' + str(question.questionNumber) + '-2'))
            if quizQuestionInstance.option3 or question.option3 == '':
                quizQuestionInstance.option3 = str(request.POST.get('option-' + str(question.questionNumber) + '-3'))
            if quizQuestionInstance.option4 or question.option4 == '':
                quizQuestionInstance.option4 = str(request.POST.get('option-' + str(question.questionNumber) + '-4'))
            if quizQuestionInstance.option5 or question.option5 == '':
                quizQuestionInstance.option5 = str(request.POST.get('option-' + str(question.questionNumber) + '-5'))
            quizQuestionInstance.save()

        return redirect(reverse('viewQuizQuestions', args=[staffCoursePk, unitTestNumber]))

    return render(request, 'quiz/editQuizQuestions.html',
                  {
                      'quizQuestionList': quizQuestionList,
                      'staffCourseInstance': staffCourseInstance,
                      'unitTestNumber': unitTestNumber
                  })


@login_required
@permission_required('marks.can_add_internal_marks')
def activateQuiz(request, staffCoursePk, unitTestNumber):
    staffInstance = staff.objects.get(user=request.user)

    try:
        staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    except:
        msg = {
            'page_title': 'Access Denied',
            'title': 'Access Denied',
            'description': 'No Page found for this url',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if staffInstance not in staffCourseInstance.staffs.all():
        msg = {
            'page_title': 'No access',
            'title': 'No access to this course',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to this course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)
    quizQuestionList = QuizQuestion.objects.filter(quizTime=quizTimeInstance).order_by('questionNumber')

    if request.method == 'POST':
        for question in quizQuestionList:
            quizQuestionInstance = question
            quizQuestionInstance.question = str(request.POST.get('question-' + str(question.questionNumber)))
            answer = str(request.POST.get('answer-' + str(question.questionNumber)))
            quizQuestionInstance.rightAnswer = answer
            if quizQuestionInstance.option1 or question.option1 == '':
                quizQuestionInstance.option1 = str(request.POST.get('option-' + str(question.questionNumber) + '-1'))
            if quizQuestionInstance.option2 or question.option2 == '':
                quizQuestionInstance.option2 = str(request.POST.get('option-' + str(question.questionNumber) + '-2'))
            if quizQuestionInstance.option3 or question.option3 == '':
                quizQuestionInstance.option3 = str(request.POST.get('option-' + str(question.questionNumber) + '-3'))
            if quizQuestionInstance.option4 or question.option4 == '':
                quizQuestionInstance.option4 = str(request.POST.get('option-' + str(question.questionNumber) + '-4'))
            if quizQuestionInstance.option5 or question.option5 == '':
                quizQuestionInstance.option5 = str(request.POST.get('option-' + str(question.questionNumber) + '-5'))
            quizQuestionInstance.save()

        quizTimeInstance.isActive = True
        quizTimeInstance.save()

        return redirect(reverse('setQuiz'))

    return render(request, 'quiz/activateQuiz.html',
                  {
                      'quizQuestionList': quizQuestionList,
                      'staffCourseInstance': staffCourseInstance,
                      'unitTestNumber': unitTestNumber
                  })


@login_required
@user_passes_test(canViewQuiz)
def quizQuestionsPdfData(request, staffCoursePk, unitTestNumber, data_for_pdf=False):
    staffInstance = staff.objects.get(user=request.user)

    try:
        staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    except:
        context_data = {
            'noStaffCourse': True
        }
        return context_data

    if staffInstance not in staffCourseInstance.staffs.all():
        context_data = {
            'noPermission': True
        }
        return context_data

    quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)
    quizQuestionList = QuizQuestion.objects.filter(quizTime=quizTimeInstance).order_by('questionNumber')

    if data_for_pdf:
        context_data = {
            'quizQuestionList': quizQuestionList,
            'staffCourseInstance': staffCourseInstance,
            'unitTestNumber': unitTestNumber

        }
        return context_data


class QuizQuestionAnswerPdfReport(PDFTemplateView):
    template_name = "quiz/quizQuestionAnswerPdfReport.html"
    staffCoursePk = None
    unitTestNumber = None

    def get(self, request, *args, **kwargs):
        self.staffCoursePk = kwargs.pop('staffCoursePk')
        self.unitTestNumber = kwargs.pop('unitTestNumber')
        return super(QuizQuestionAnswerPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = quizQuestionsPdfData(self.request,
                                            staffCoursePk=self.staffCoursePk,
                                            unitTestNumber=self.unitTestNumber,
                                            data_for_pdf=True
                                            )
        return super(QuizQuestionAnswerPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )


class QuizQuestionPdfReport(PDFTemplateView):
    template_name = "quiz/quizQuestionPdfReport.html"
    staffCoursePk = None
    unitTestNumber = None

    def get(self, request, *args, **kwargs):
        self.staffCoursePk = kwargs.pop('staffCoursePk')
        self.unitTestNumber = kwargs.pop('unitTestNumber')
        return super(QuizQuestionPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = quizQuestionsPdfData(self.request,
                                            staffCoursePk=self.staffCoursePk,
                                            unitTestNumber=self.unitTestNumber,
                                            data_for_pdf=True
                                            )
        return super(QuizQuestionPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )
