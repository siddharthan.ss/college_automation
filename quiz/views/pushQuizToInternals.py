import math
from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from accounts.models import staff, student
from curriculum.models import staff_course
from marks.models import internalmarks, internalmarksold
from quiz.views.quiz import getQuizMarks

from common.utils.ReportTabsUtil import getCoursesHandlingByStaff


@login_required
@permission_required('marks.can_add_internal_marks')
def pushQuizToInternals(request):
    staffInstance = staff.objects.get(user=request.user)

    courseList = []
    for each in getCoursesHandlingByStaff(staffInstance):
        if each['staffCourseInstance'].course.subject_type == 'T':
            courseList.append(each)

    try:
        staffCoursePk = courseList[0]['staffCourseInstance'].pk
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course that have quiz marks',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))

        if 'pushQuiz1' in request.POST:
            unitTestNumber = 1
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getQuizMarks(staffCourseInstance, unitTestNumber)

            if dictionary['wrapQuizAvail'] == 'true':
                return render(request, 'quiz/prePushConfirmation.html',
                              {
                                  'staffCourseInstance': staffCourseInstance,
                                  'unitTestNumber': unitTestNumber,
                                  'dictionary': dictionary
                              })

            if dictionary['avail'] == 'true':
                return render(request, 'quiz/pushQuizToInternals.html',
                              {
                                  'noQuizWrap': True,
                                  'staffCourseInstance': staffCourseInstance,
                                  'courseList': courseList
                              })

            if dictionary['avail'] == 'false':
                return render(request, 'quiz/pushQuizToInternals.html',
                              {
                                  'noQuizPattern': True,
                                  'staffCourseInstance': staffCourseInstance,
                                  'courseList': courseList
                              })

        if 'pushQuiz2' in request.POST:
            unitTestNumber = 2
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getQuizMarks(staffCourseInstance, unitTestNumber)

            if dictionary['wrapQuizAvail'] == 'true':
                return render(request, 'quiz/prePushConfirmation.html',
                              {
                                  'staffCourseInstance': staffCourseInstance,
                                  'unitTestNumber': unitTestNumber,
                                  'dictionary': dictionary
                              })

            if dictionary['avail'] == 'true':
                return render(request, 'quiz/pushQuizToInternals.html',
                              {
                                  'noQuizWrap': True,
                                  'staffCourseInstance': staffCourseInstance,
                                  'courseList': courseList
                              })

            if dictionary['avail'] == 'false':
                return render(request, 'quiz/pushQuizToInternals.html',
                              {
                                  'noQuizPattern': True,
                                  'staffCourseInstance': staffCourseInstance,
                                  'courseList': courseList
                              })

        if 'pushQuiz3' in request.POST:
            unitTestNumber = 3
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getQuizMarks(staffCourseInstance, unitTestNumber)

            if dictionary['wrapQuizAvail'] == 'true':
                return render(request, 'quiz/prePushConfirmation.html',
                              {
                                  'staffCourseInstance': staffCourseInstance,
                                  'unitTestNumber': unitTestNumber,
                                  'dictionary': dictionary
                              })

            if dictionary['avail'] == 'true':
                return render(request, 'quiz/pushQuizToInternals.html',
                              {
                                  'noQuizWrap': True,
                                  'staffCourseInstance': staffCourseInstance,
                                  'courseList': courseList
                              })

            if dictionary['avail'] == 'false':
                return render(request, 'quiz/pushQuizToInternals.html',
                              {
                                  'noQuizPattern': True,
                                  'staffCourseInstance': staffCourseInstance,
                                  'courseList': courseList
                              })

        if 'confirmPushQuiz' in request.POST:
            unitTestNumber = int(request.POST.get('hiddenUnitTestNumber'))
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            courseInstance = staffCourseInstance.course
            semesterInstance = staffCourseInstance.semester
            departmentInstance = staffCourseInstance.department
            batchInstance = staffCourseInstance.batch

            dictionary = getQuizMarks(staffCourseInstance, unitTestNumber)

            if dictionary['wrapQuizAvail'] == 'false':
                return render(request, 'quiz/pushQuizToInternals.html',
                              {
                                  'errorInPush': True,
                                  'staffCourseInstance': staffCourseInstance,
                                  'courseList': courseList
                              })

            maximumMarks = dictionary['maximumMarks']

            if staffCourseInstance.course.regulation.start_year >= 2016:
                for eachStudent in dictionary['studentList']:
                    studentInstance = student.objects.get(pk=eachStudent['studentPk'])
                    totalMarks = eachStudent['totalMarks']
                    if int(totalMarks) != 0:
                        totalMarks = (totalMarks / maximumMarks) * 5
                        if float(totalMarks - int(totalMarks)) >= 0.5:
                            totalMarks = math.ceil(totalMarks)
                        else:
                            totalMarks = math.floor(totalMarks)

                    if eachStudent['attended'] == 'false':
                        totalMarks = None

                    if internalmarks.objects.filter(student=studentInstance, course=courseInstance,
                                                    semester=semesterInstance):
                        internalMarksInstance = internalmarks.objects.get(student=studentInstance,
                                                                          course=courseInstance,
                                                                          semester=semesterInstance,
                                                                          department=departmentInstance,
                                                                          batch=batchInstance)
                        if unitTestNumber == 1:
                            internalMarksInstance.tutorial_or_quiz_1 = totalMarks
                        elif unitTestNumber == 2:
                            internalMarksInstance.tutorial_or_quiz_2 = totalMarks
                        elif unitTestNumber == 3:
                            internalMarksInstance.tutorial_or_quiz_3 = totalMarks
                        internalMarksInstance.save()

                    else:
                        internalMarksInstance = internalmarks(
                            student=studentInstance,
                            course=courseInstance,
                            semester=semesterInstance,
                            department=departmentInstance,
                            batch=batchInstance,
                            staff=staffInstance
                        )
                        internalMarksInstance.save()
                        if unitTestNumber == 1:
                            internalMarksInstance.tutorial_or_quiz_1 = totalMarks
                        elif unitTestNumber == 2:
                            internalMarksInstance.tutorial_or_quiz_2 = totalMarks
                        elif unitTestNumber == 3:
                            internalMarksInstance.tutorial_or_quiz_3 = totalMarks
                        internalMarksInstance.save()
            else:
                for eachStudent in dictionary['studentList']:
                    studentInstance = student.objects.get(pk=eachStudent['studentPk'])
                    totalMarks = eachStudent['totalMarks']
                    if int(totalMarks) != 0:
                        totalMarks = (totalMarks / maximumMarks) * 10
                        if float(totalMarks - int(totalMarks)) >= 0.5:
                            totalMarks = math.ceil(totalMarks)
                        else:
                            totalMarks = math.floor(totalMarks)

                    if eachStudent['attended'] == 'false':
                        totalMarks = None

                    if internalmarksold.objects.filter(student=studentInstance, course=courseInstance,
                                                       semester=semesterInstance, department=departmentInstance,
                                                       batch=batchInstance):
                        internalMarksOldInstance = internalmarksold.objects.get(student=studentInstance,
                                                                                course=courseInstance,
                                                                                semester=semesterInstance,
                                                                                department=departmentInstance,
                                                                                batch=batchInstance)
                        if unitTestNumber == 1:
                            internalMarksOldInstance.ass1 = totalMarks
                        elif unitTestNumber == 2:
                            internalMarksOldInstance.ass2 = totalMarks
                        elif unitTestNumber == 3:
                            internalMarksOldInstance.ass3 = totalMarks
                        internalMarksOldInstance.save()

                    else:
                        internalMarksOldInstance = internalmarksold(
                            student=studentInstance,
                            course=courseInstance,
                            semester=semesterInstance,
                            department=departmentInstance,
                            batch=batchInstance,
                            staff=staffInstance
                        )
                        internalMarksOldInstance.save()
                        if unitTestNumber == 1:
                            internalMarksOldInstance.ass1 = totalMarks
                        elif unitTestNumber == 2:
                            internalMarksOldInstance.ass2 = totalMarks
                        elif unitTestNumber == 3:
                            internalMarksOldInstance.ass3 = totalMarks
                        internalMarksOldInstance.save()

            return render(request, 'quiz/pushQuizToInternals.html',
                          {
                              'donePush': True,
                              'staffCourseInstance': staffCourseInstance,
                              'courseList': courseList
                          })

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    return render(request, 'quiz/pushQuizToInternals.html',
                  {
                      'staffCourseInstance': staffCourseInstance,
                      'courseList': courseList
                  })
