from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import JsonResponse
from django.shortcuts import render
from easy_pdf.views import PDFTemplateView

from accounts.models import staff, department, student
from curriculum.models import staff_course, semester
from quiz.views.quiz import getQuizMarks
from quiz.views.wrapQuiz import getQuizMarksByStudent

from common.utils.ReportTabsUtil import getActiveDepartmentsForStaff, getActiveSemesterTabsForStaff, \
    getActiveCoursesTabsForStaff, courseViewPermissionByStaff
from common.API.departmentAPI import getDepartmentOfPk


def canViewQuizReports(user):
    if user.has_perm('marks.can_add_internal_marks') or \
            user.has_perm('curriculum.can_view_overall_reports'):
        return True


@login_required
@user_passes_test(canViewQuizReports)
def quizReports(request):
    staffInstance = staff.objects.get(user=request.user)
    departmentList = getActiveDepartmentsForStaff(staffInstance)

    if not departmentList:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact Programme co-ordinator to ensure whether courses are properly alloted',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    departmentInstance = department.objects.get(pk=departmentList[0].pk)

    if request.method == 'POST':
        if 'departmentPk' in request.POST:
            departmentPk = int(request.POST.get('departmentPk'))
            departmentInstance = getDepartmentOfPk(departmentPk)

        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            departmentInstance = semesterInstance.department

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            departmentInstance = staffCourseInstance.department

    semesterList = getActiveSemesterTabsForStaff(staffInstance, departmentInstance)

    if not semesterList:
        return render(request, 'quiz/quizReports.html',
                      {
                          'noSemester': True,
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                      })

    semesterInstance = semesterList[0]['semesterInstance']

    if request.method == 'POST':
        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            departmentInstance = semesterInstance.department

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            semesterInstance = staffCourseInstance.semester
            departmentInstance = staffCourseInstance.department

    courseList = getActiveCoursesTabsForStaff(staffInstance, semesterInstance)

    try:
        if not 'staffCoursePk' in request.POST:
            staffCoursePk = courseList[0]['staffCourseInstance'].pk
    except:
        return render(request, 'quiz/quizReports.html',
                      {
                          'noCourses': True,
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                          'semesterInstance': semesterInstance,
                          'semesterList': semesterList,
                      })

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    if request.method == 'POST':
        if request.is_ajax():
            unitTestNumber = int(request.POST.get('unitTestNumber'))
            staffCoursePk = str(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getQuizMarks(staffCourseInstance, unitTestNumber)

            return JsonResponse(dictionary)

    return render(request, 'quiz/quizReports.html',
                  {
                      'departmentList': departmentList,
                      'departmentInstance': departmentInstance,
                      'semesterInstance': semesterInstance,
                      'semesterList': semesterList,
                      'staffCourseInstance': staffCourseInstance,
                      'courseList': courseList,
                  })


@login_required
@user_passes_test(canViewQuizReports)
def studentQuizMarks(request, registerNumber, staffCoursePk, unitTestNumber):
    staffInstance = staff.objects.get(user=request.user)
    try:
        studentInstance = student.objects.get(roll_no=registerNumber)
    except:
        msg = {
            'page_title': 'Access Denied',
            'title': 'No student',
            'description': 'No student available with this register number',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    try:
        staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course that have quiz marks',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if not courseViewPermissionByStaff(staffCourseInstance, staffInstance):
        msg = {
            'page_title': 'Access Denied',
            'title': 'No Permission',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to this course to gain access',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    dictionary = getQuizMarksByStudent(studentInstance, staffCourseInstance, unitTestNumber)

    if dictionary.get('notWrappedQuiz'):
        return render(request, 'quiz/studentQuizMarks.html',
                      {
                          'staffCourseInstance': staffCourseInstance,
                          'unitTestNumber': unitTestNumber,
                          'studentInstance': studentInstance,
                          'notWrappedQuiz': True
                      })

    return render(request, 'quiz/studentQuizMarks.html',
                  {
                      'quizQuestionAnswerList': dictionary['quizQuestionAnswerList'],
                      'staffCourseInstance': staffCourseInstance,
                      'unitTestNumber': unitTestNumber,
                      'totalMarks': dictionary['totalMarks'],
                      'maximumMarks': dictionary['maximumMarks'],
                      'totalQuestions': len(dictionary['quizQuestionList']),
                      'studentInstance': studentInstance
                  })


def pdfQuizMarksData(request, staffCoursePk, unitTestNumber):
    staffInstance = staff.objects.get(user=request.user)
    try:
        staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

        if courseViewPermissionByStaff(staffCourseInstance, staffInstance):
            dictionary = {
                'data': getQuizMarks(staffCourseInstance, unitTestNumber),
                'staffCourseInstance': staffCourseInstance,
                'unitTestNumber': unitTestNumber
            }

        else:
            dictionary = {
                'noPermission': True
            }

        return dictionary

    except:
        dictionary = {
            'noPermission': True
        }
        return dictionary


class QuizMarksPdfReport(PDFTemplateView):
    template_name = "quiz/quizMarksPdf.html"
    staffCoursePk = None
    unitTestNumber = None

    def get(self, request, *args, **kwargs):
        self.staffCoursePk = kwargs.pop('staffCoursePk')
        self.unitTestNumber = kwargs.pop('unitTestNumber')
        return super(QuizMarksPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        contextData = pdfQuizMarksData(self.request,
                                       staffCoursePk=self.staffCoursePk,
                                       unitTestNumber=self.unitTestNumber
                                       )
        return super(QuizMarksPdfReport, self).get_context_data(
            pagesize="A4",
            context=contextData,
            **kwargs
        )
