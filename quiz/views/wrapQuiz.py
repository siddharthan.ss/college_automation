import random

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils import timezone

from common.utils.studentUtil import getStaffCoursesForStudent
from quiz.models import QuizTime, QuizQuestion, WrapQuizTime, WrapQuizQuestion
from accounts.models import student
from curriculum.models import staff_course

from common.utils.studentUtil import getSemesterPlanofStudent, courseEligibleForStudent


def getQuizMarksByStudent(studentInstance, staffCourseInstance, unitTestNumber):
    quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)
    quizQuestionList = QuizQuestion.objects.filter(quizTime=quizTimeInstance).order_by('questionNumber')

    dictionary = {}

    if WrapQuizTime.objects.filter(student=studentInstance, quizTime=quizTimeInstance):
        wrapQuizTimeInstance = WrapQuizTime.objects.get(student=studentInstance, quizTime=quizTimeInstance)
        totalMarks = 0
        maximumMarks = 0
        quizQuestionAnswerList = []

        for eachQuestion in quizQuestionList:
            temp = {}
            temp['questionNumber'] = eachQuestion.questionNumber
            temp['question'] = eachQuestion.question
            temp['option1'] = eachQuestion.option1
            temp['option2'] = eachQuestion.option2
            temp['option3'] = eachQuestion.option3
            temp['option4'] = eachQuestion.option4
            temp['option5'] = eachQuestion.option5
            temp['rightAnswer'] = eachQuestion.rightAnswer
            wrapQuizQuestionInstance = WrapQuizQuestion.objects.get(wrapQuizTime=wrapQuizTimeInstance,
                                                                    quizQuestion=eachQuestion)
            temp['studentAnswer'] = wrapQuizQuestionInstance.givenAnswer
            if wrapQuizQuestionInstance.isCorrect:
                totalMarks = totalMarks + eachQuestion.markAllotted
            maximumMarks = maximumMarks + eachQuestion.markAllotted

            quizQuestionAnswerList.append(temp)

            dictionary['quizQuestionAnswerList'] = quizQuestionAnswerList
            dictionary['totalMarks'] = totalMarks
            dictionary['maximumMarks'] = maximumMarks
            dictionary['quizQuestionList'] = quizQuestionList

    else:
        dictionary['notWrappedQuiz'] = True

    return dictionary


@login_required
@permission_required('quiz.can_attend_quiz')
def quizView(request):
    studentInstance = student.objects.get(user=request.user)

    semesterInstance = getSemesterPlanofStudent(studentInstance)

    if not semesterInstance:
        msg = {
            'page_title': 'Access Withheld',
            'title': 'Access Withheld',
            'description': 'You have not assigned to any of the section. Contact your Programme Coordinator for immediate remedy.',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    courseList = getStaffCoursesForStudent(studentInstance)

    try:
        staffCoursePk = int(courseList[0].pk)
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses Found',
            'description': 'Contact your Programme co-ordinator for more information',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))

        if 'quiz1' in request.POST:
            staffCoursePk = str(request.POST.get('hiddenStaffCoursePk'))
            return redirect(reverse('wrapQuiz', args=[staffCoursePk, 1]))
        if 'quiz2' in request.POST:
            staffCoursePk = str(request.POST.get('hiddenStaffCoursePk'))
            return redirect(reverse('wrapQuiz', args=[staffCoursePk, 2]))
        if 'quiz3' in request.POST:
            staffCoursePk = str(request.POST.get('hiddenStaffCoursePk'))
            return redirect(reverse('wrapQuiz', args=[staffCoursePk, 3]))

        if 'viewquiz1marks' in request.POST:
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getQuizMarksByStudent(studentInstance, staffCourseInstance, 1)

            if dictionary.get('notWrappedQuiz'):
                return render(request, 'quiz/studentQuizAnswers.html',
                              {
                                  'staffCourseInstance': staffCourseInstance,
                                  'unitTestNumber': 1,
                                  'notWrappedQuiz': True,
                              })

            return render(request, 'quiz/studentQuizAnswers.html',
                          {
                              'quizQuestionAnswerList': dictionary['quizQuestionAnswerList'],
                              'staffCourseInstance': staffCourseInstance,
                              'unitTestNumber': 1,
                              'totalMarks': dictionary['totalMarks'],
                              'totalQuestions': len(dictionary['quizQuestionList'])
                          })

        if 'viewquiz2marks' in request.POST:
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getQuizMarksByStudent(studentInstance, staffCourseInstance, 2)

            if dictionary.get('notWrappedQuiz'):
                return render(request, 'quiz/studentQuizAnswers.html',
                              {
                                  'staffCourseInstance': staffCourseInstance,
                                  'unitTestNumber': 2,
                                  'notWrappedQuiz': True,
                              })

            return render(request, 'quiz/studentQuizAnswers.html',
                          {
                              'quizQuestionAnswerList': dictionary['quizQuestionAnswerList'],
                              'staffCourseInstance': staffCourseInstance,
                              'unitTestNumber': 2,
                              'totalMarks': dictionary['totalMarks'],
                              'totalQuestions': len(dictionary['quizQuestionList'])
                          })

        if 'viewquiz3marks' in request.POST:
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getQuizMarksByStudent(studentInstance, staffCourseInstance, 3)

            if dictionary.get('notWrappedQuiz'):
                return render(request, 'quiz/studentQuizAnswers.html',
                              {
                                  'staffCourseInstance': staffCourseInstance,
                                  'unitTestNumber': 3,
                                  'notWrappedQuiz': True,
                              })

            return render(request, 'quiz/studentQuizAnswers.html',
                          {
                              'quizQuestionAnswerList': dictionary['quizQuestionAnswerList'],
                              'staffCourseInstance': staffCourseInstance,
                              'unitTestNumber': 3,
                              'totalMarks': dictionary['totalMarks'],
                              'totalQuestions': len(dictionary['quizQuestionList'])
                          })

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    timenow = timezone.now()

    ut1Quiz = QuizTime.objects.filter(staffCourse=staffCourseInstance, unitTest=1, isActive=True)
    ut2Quiz = QuizTime.objects.filter(staffCourse=staffCourseInstance, unitTest=2, isActive=True)
    ut3Quiz = QuizTime.objects.filter(staffCourse=staffCourseInstance, unitTest=3, isActive=True)

    showUt1Marks = False
    showUt2Marks = False
    showUt3Marks = False

    if ut1Quiz and ut1Quiz[0].endTime:
        if timenow > ut1Quiz[0].endTime:
            showUt1Marks = True
    if ut2Quiz and ut2Quiz[0].endTime:
        if timenow > ut2Quiz[0].endTime:
            showUt2Marks = True
    if ut3Quiz and ut3Quiz[0].endTime:
        if timenow > ut3Quiz[0].endTime:
            showUt3Marks = True

    return render(request, 'quiz/quizView.html',
                  {
                      'staffCourseInstance': staffCourseInstance,
                      'courseList': courseList,
                      'ut1Quiz': ut1Quiz,
                      'ut2Quiz': ut2Quiz,
                      'ut3Quiz': ut3Quiz,
                      'showUt1Marks': showUt1Marks,
                      'showUt2Marks': showUt2Marks,
                      'showUt3Marks': showUt3Marks
                  })


@login_required
@permission_required('quiz.can_attend_quiz')
def wrapQuiz(request, staffCoursePk, unitTestNumber):
    studentInstance = student.objects.get(user=request.user)

    try:
        staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    except:
        msg = {
            'page_title': 'Access Denied',
            'title': 'Access Denied',
            'description': 'No Page found for this url',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if not courseEligibleForStudent(studentInstance, staffCourseInstance):
        msg = {
            'page_title': 'Access Denied',
            'title': 'Access Denied',
            'description': 'Invalid Course access detected',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    courseInstance = staffCourseInstance.course

    if QuizTime.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber):
        quizTimeInstance = QuizTime.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber)
        if not quizTimeInstance.isActive:
            msg = {
                'page_title': 'Not Active',
                'title': 'Quiz Not Active',
                'description': 'You don\'t have a permission to access this page',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    else:
        msg = {
            'page_title': 'Access Denied',
            'title': 'Access Denied',
            'description': 'You don\'t have a permission to access this page',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if quizTimeInstance.startTime:
        timenow = timezone.now()
        if timenow < quizTimeInstance.startTime:
            remainingTimeToStartQuiz = (quizTimeInstance.startTime - timenow).total_seconds()
            msg = {
                'page_title': 'Not started',
                'title': 'Quiz not started',
                'description': 'Quiz is not started yet. Check the quiz timings and come back later',
            }
            return render(request, 'quiz/quizStartTime.html', {
                'message': msg,
                'remainingTimeToStartQuiz': remainingTimeToStartQuiz
            })
        if timenow > quizTimeInstance.endTime:
            msg = {
                'page_title': 'Quiz expired',
                'title': 'Quiz timings expired',
                'description': 'Quiz has got over. Check the quiz timings for more details',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

        quizQuestionList = []
        for eachQuestion in QuizQuestion.objects.filter(quizTime=quizTimeInstance).order_by('questionNumber'):
            quizQuestionList.append(eachQuestion)

        if WrapQuizTime.objects.filter(student=studentInstance, quizTime=quizTimeInstance):
            if quizTimeInstance.endTime < timezone.now():
                msg = {
                    'page_title': 'Quiz completed',
                    'title': 'Successful Quiz Completion',
                    'description': 'You have successfully completed Quiz',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
        else:
            wrapQuizTimeInstance = WrapQuizTime(student=studentInstance, quizTime=quizTimeInstance,
                                                startTime=timezone.now())
            wrapQuizTimeInstance.save()
            random.shuffle(quizQuestionList)
            for eachQuestion in quizQuestionList:
                wrapQuizQuestionInstance = WrapQuizQuestion(wrapQuizTime=wrapQuizTimeInstance,
                                                            quizQuestion=eachQuestion, givenAnswer=0)
                wrapQuizQuestionInstance.save()

        wrapQuizTimeInstance = WrapQuizTime.objects.get(student=studentInstance, quizTime=quizTimeInstance)

        if wrapQuizTimeInstance.isWrapped:
            msg = {
                'page_title': 'Quiz completed',
                'title': 'Successful Quiz Completion',
                'description': 'You have submitted Quiz, this cannot be changed',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

        wrapQuizQuestionList = WrapQuizQuestion.objects.filter(wrapQuizTime=wrapQuizTimeInstance)

        if request.method == 'POST':
            usedQuizDuration = (timezone.now() - wrapQuizTimeInstance.startTime).total_seconds()
            quizTimeDuration = quizTimeInstance.timeDuration
            remainingTimeToWrapQuiz = quizTimeDuration * 60 - usedQuizDuration
            if (usedQuizDuration > quizTimeDuration * 60):
                msg = {
                    'page_title': 'Quiz Timings exhausted',
                    'title': 'Time duration finished',
                    'description': 'You have used up quiz timing duration',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

            if 'nextQuestion' in request.POST:
                questionNumber = int(request.POST.get('questionNumber'))
                wrapQuizQuestionPk = request.POST.get('wrapQuizQuestionPk')
                wrapQuizQuestionInstance = WrapQuizQuestion.objects.get(pk=wrapQuizQuestionPk)

                enableSubmit = False
                if questionNumber == len(wrapQuizQuestionList) - 1:
                    enableSubmit = True
                givenOption = request.POST.get('answer')

                if not givenOption:
                    givenOption = 0
                if str(givenOption) == str(wrapQuizQuestionList[questionNumber - 1].quizQuestion.rightAnswer):
                    wrapQuizQuestionInstance.givenAnswer = givenOption
                    wrapQuizQuestionInstance.isCorrect = True
                    wrapQuizQuestionInstance.save()
                else:
                    wrapQuizQuestionInstance.givenAnswer = givenOption
                    wrapQuizQuestionInstance.isCorrect = False
                    wrapQuizQuestionInstance.save()
                return render(request, 'quiz/wrapQuiz.html',
                              {
                                  'remainingTimeToWrapQuiz': remainingTimeToWrapQuiz,
                                  'quizTimeInstance': quizTimeInstance,
                                  'questionNumber': questionNumber + 1,
                                  'enableSubmit': enableSubmit,
                                  'enablePrevious': True,
                                  'question': wrapQuizQuestionList[questionNumber],
                                  'course': courseInstance
                              })

            if 'previousQuestion' in request.POST:
                questionNumber = int(request.POST.get('questionNumber'))
                wrapQuizQuestionPk = request.POST.get('wrapQuizQuestionPk')
                wrapQuizQuestionInstance = WrapQuizQuestion.objects.get(pk=wrapQuizQuestionPk)

                enablePrevious = True
                if questionNumber == 2:
                    enablePrevious = False
                givenOption = request.POST.get('answer')

                if not givenOption:
                    givenOption = 0
                if str(givenOption) == str(wrapQuizQuestionList[questionNumber - 1].quizQuestion.rightAnswer):
                    wrapQuizQuestionInstance.givenAnswer = givenOption
                    wrapQuizQuestionInstance.isCorrect = True
                    wrapQuizQuestionInstance.save()
                else:
                    wrapQuizQuestionInstance.givenAnswer = givenOption
                    wrapQuizQuestionInstance.isCorrect = False
                    wrapQuizQuestionInstance.save()
                return render(request, 'quiz/wrapQuiz.html',
                              {
                                  'remainingTimeToWrapQuiz': remainingTimeToWrapQuiz,
                                  'quizTimeInstance': quizTimeInstance,
                                  'questionNumber': questionNumber - 1,
                                  'enablePrevious': enablePrevious,
                                  'question': wrapQuizQuestionList[questionNumber - 2],
                                  'course': courseInstance
                              })

            if 'submitQuiz' in request.POST:
                questionNumber = int(request.POST.get('questionNumber'))
                wrapQuizQuestionPk = request.POST.get('wrapQuizQuestionPk')
                wrapQuizQuestionInstance = WrapQuizQuestion.objects.get(pk=wrapQuizQuestionPk)

                givenOption = request.POST.get('answer')

                if not givenOption:
                    givenOption = 0
                if str(givenOption) == str(wrapQuizQuestionList[questionNumber - 1].quizQuestion.rightAnswer):
                    wrapQuizQuestionInstance.givenAnswer = givenOption
                    wrapQuizQuestionInstance.isCorrect = True
                    wrapQuizQuestionInstance.save()
                else:
                    wrapQuizQuestionInstance.givenAnswer = givenOption
                    wrapQuizQuestionInstance.isCorrect = False
                    wrapQuizQuestionInstance.save()

                wrapQuizTimeInstance.isWrapped = True
                wrapQuizTimeInstance.endTime = timezone.now()
                wrapQuizTimeInstance.save()

                return redirect(reverse('quizView'))

        usedQuizDuration = (timezone.now() - wrapQuizTimeInstance.startTime).total_seconds()
        quizTimeDuration = quizTimeInstance.timeDuration
        remainingTimeToWrapQuiz = quizTimeDuration * 60 - usedQuizDuration
        if (usedQuizDuration > quizTimeDuration * 60):
            msg = {
                'page_title': 'Quiz Timings exhausted',
                'title': 'Time duration finished',
                'description': 'You have used up quiz timing duration',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

        return render(request, 'quiz/wrapQuiz.html',
                      {
                          'remainingTimeToWrapQuiz': remainingTimeToWrapQuiz,
                          'quizTimeInstance': quizTimeInstance,
                          'questionNumber': 1,
                          'question': wrapQuizQuestionList[0],
                          'course': courseInstance
                      })
    else:
        msg = {
            'page_title': 'Access Denied',
            'title': 'Not active',
            'description': 'Quiz is not active. Contact your faculty for further details',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
