from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from quiz.views.quiz import setQuiz, viewQuizQuestions, editQuizQuestions, activateQuiz, QuizQuestionAnswerPdfReport, \
    QuizQuestionPdfReport

from quiz.views.wrapQuiz import quizView, wrapQuiz
from quiz.views.pushQuizToInternals import pushQuizToInternals

from quiz.views.quizReports import quizReports, studentQuizMarks, QuizMarksPdfReport

urlpatterns = [

    url(r'^create_quiz/$', setQuiz, name="setQuiz"),
    url(r'^view_quiz/(?P<staffCoursePk>[0-9]+)/(?P<unitTestNumber>[1-3])/$', viewQuizQuestions,
        name='viewQuizQuestions'),
    url(r'^edit_quiz/(?P<staffCoursePk>[0-9]+)/(?P<unitTestNumber>[1-3])/$', editQuizQuestions,
        name='editQuizQuestions'),
    url(r'^activate_quiz/(?P<staffCoursePk>[0-9]+)/(?P<unitTestNumber>[1-3])/$', activateQuiz, name='activateQuiz'),
    url(r'^pdf_quiz_with_answer/(?P<staffCoursePk>[0-9]+)/(?P<unitTestNumber>[1-3])/$',
        login_required(QuizQuestionAnswerPdfReport.as_view()),
        name='quizQuestionAnswerPdfReport'),
    url(r'^pdf_quiz_without_answer/(?P<staffCoursePk>[0-9]+)/(?P<unitTestNumber>[1-3])/$',
        login_required(QuizQuestionPdfReport.as_view()), name='quizQuestionPdfReport'),
    url(r'^available_quiz/$', quizView, name='quizView'),
    url(r'^attend_quiz/(?P<staffCoursePk>[0-9]+)/(?P<unitTestNumber>[1-3])/$', wrapQuiz, name='wrapQuiz'),
    url(r'^quiz_reports/$', quizReports, name='quizReports'),
    url(r'^student_quiz_marks/(?P<registerNumber>[0-9LT]+)/(?P<staffCoursePk>[0-9]+)/(?P<unitTestNumber>[1-3])/$',
        studentQuizMarks, name='student_individual_quiz_marks'),

    url(r'^pdf_quiz_report/(?P<staffCoursePk>[0-9]+)/(?P<unitTestNumber>[1-3])/$',
        login_required(QuizMarksPdfReport.as_view()),
        name='quizMarksPdfReport'),

    url(r'^push_quiz_marks/$', pushQuizToInternals, name='pushQuizToInternals'),
]
