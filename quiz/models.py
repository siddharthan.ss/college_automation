from django.db import models
from simple_history.models import HistoricalRecords

from accounts.models import student, batch, staff
from curriculum.models import department, courses, semester, staff_course


class quiz_timings(models.Model):
    course = models.ForeignKey(courses)
    batch = models.ForeignKey(batch)
    department = models.ForeignKey(department)
    semester = models.ForeignKey(semester)
    ut_num = models.PositiveSmallIntegerField()
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    time_to_complete = models.PositiveIntegerField(blank=True, null=True)
    is_active = models.BooleanField(default=False)
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            'course', 'batch', 'department', 'semester', 'ut_num'
        )


class quiz_questions(models.Model):
    quiz_timings_ref = models.ForeignKey(quiz_timings)
    question_number = models.PositiveSmallIntegerField()
    options_count = models.PositiveSmallIntegerField()
    question = models.CharField(max_length=5000, null=True, blank=True)
    option1 = models.CharField(max_length=1000, null=True, blank=True)
    option2 = models.CharField(max_length=1000, null=True, blank=True)
    option3 = models.CharField(max_length=1000, null=True, blank=True)
    option4 = models.CharField(max_length=1000, null=True, blank=True)
    option5 = models.CharField(max_length=1000, null=True, blank=True)
    answer = models.PositiveSmallIntegerField(null=True, blank=True)  # option number will be the answer
    marks = models.PositiveSmallIntegerField(null=True, blank=True)
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            'quiz_timings_ref', 'question_number'
        )


class attend_quiz_timings(models.Model):
    student = models.ForeignKey(student)
    quiz_timings_ref = models.ForeignKey(quiz_timings)
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    complete_test = models.BooleanField(default=False)
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            'student', 'quiz_timings_ref'
        )


class attend_quiz_answers(models.Model):
    attend_quiz_timings_ref = models.ForeignKey(attend_quiz_timings)
    quiz_questions_ref = models.ForeignKey(quiz_questions)
    chosen_answer = models.PositiveSmallIntegerField()
    is_correct = models.BooleanField(default=False)
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            'attend_quiz_timings_ref', 'quiz_questions_ref'
        )


class QuizTime(models.Model):
    staffCourse = models.ForeignKey(staff_course)
    unitTest = models.PositiveSmallIntegerField()
    startTime = models.DateTimeField(blank=True, null=True)
    endTime = models.DateTimeField(blank=True, null=True)
    timeDuration = models.PositiveSmallIntegerField(blank=True, null=True)
    isActive = models.BooleanField(default=False)

    class Meta:
        unique_together = (
            'staffCourse', 'unitTest'
        )


class QuizQuestion(models.Model):
    quizTime = models.ForeignKey(QuizTime)
    questionNumber = models.PositiveSmallIntegerField()
    totalOptions = models.PositiveSmallIntegerField()
    question = models.CharField(max_length=10000, null=True, blank=True)
    option1 = models.CharField(max_length=5000, null=True, blank=True)
    option2 = models.CharField(max_length=5000, null=True, blank=True)
    option3 = models.CharField(max_length=5000, null=True, blank=True)
    option4 = models.CharField(max_length=5000, null=True, blank=True)
    option5 = models.CharField(max_length=5000, null=True, blank=True)
    rightAnswer = models.PositiveSmallIntegerField(null=True, blank=True)  # option number will be the answer
    markAllotted = models.PositiveSmallIntegerField(null=True, blank=True)

    class Meta:
        unique_together = (
            'quizTime', 'questionNumber'
        )


class WrapQuizTime(models.Model):
    student = models.ForeignKey(student)
    quizTime = models.ForeignKey(QuizTime)
    startTime = models.DateTimeField(blank=True, null=True)
    endTime = models.DateTimeField(blank=True, null=True)
    isWrapped = models.BooleanField(default=False)

    class Meta:
        unique_together = (
            'student', 'quizTime'
        )


class WrapQuizQuestion(models.Model):
    wrapQuizTime = models.ForeignKey(WrapQuizTime)
    quizQuestion = models.ForeignKey(QuizQuestion)
    givenAnswer = models.PositiveSmallIntegerField()
    isCorrect = models.BooleanField(default=False)

    class Meta:
        unique_together = (
            'wrapQuizTime', 'quizQuestion'
        )
