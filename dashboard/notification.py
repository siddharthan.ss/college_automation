from notifications.signals import notify


def send_notification(recipient, message, url):
    notify.send(recipient, recipient=recipient, verb=message, description=url)
