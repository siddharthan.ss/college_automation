from datetime import datetime
from pprint import pprint

from django.utils import timezone

from accounts.models import staff, student, CustomUser, batch
from common.utils.TimeTableUtil import getTodayTimetable, getDayInstanceForTodaysTimetable
from curriculum.models import time_table, courses, semester_migration, student_enrolled_courses, \
    staff_course, section_students
from curriculum.views.common_includes import get_staff_course_allotment_instances_for_staff, \
    get_current_semester_of_given_batch
from enrollment.models import course_enrollment


def staff_hours(user):
    staffInstance = staff.objects.get(user=user)
    timetableList = getTodayTimetable(staffInstance)
    if not timetableList:
        return []

    totalHours = 8

    currentHour = 1
    today_hours = []
    while currentHour <= totalHours:
        temp = {}
        temp['hour'] = currentHour
        hourList = []
        for timetable in timetableList:
            if timetable.hour == currentHour:
                tempList = {}
                tempList['course_id'] = timetable.course.course_id
                tempList['course_name'] = timetable.course.course_name
                tempList['programme'] = timetable.course.programme
                semester_number = timetable.course.semester
                if semester_number == 1 or semester_number == 2:
                    tempList['year'] = 'I Year'
                elif semester_number == 3 or semester_number == 4:
                    tempList['year'] = 'II Year'
                elif semester_number == 5 or semester_number == 6:
                    tempList['year'] = 'III Year'
                elif semester_number == 7 or semester_number == 8:
                    tempList['year'] = 'IV Year'
                if timetable.course.code:
                    tempList['code'] = timetable.course.code
                hourList.append(tempList)

        if hourList:
            temp['courses'] = hourList
        today_hours.append(temp)

        currentHour = currentHour + 1

    return today_hours


def staff_hourss(request):
    now = datetime.now()
    day_name = now.strftime("%A")
    staff_instance = staff.objects.get(user=request)
    # department_instance = staff_instance.department
    list_of_staff_handling_courses = get_staff_course_allotment_instances_for_staff(staff_instance)

    semester_pks = []
    for each in list_of_staff_handling_courses:
        if each.semester.end_term_3:
            if each.semester.end_term_3 < timezone.now().date():
                continue
        if each.semester.end_term_1:
            if each.semester.end_term_1 > timezone.now().date():
                continue
        semester_pks.append(each.semester.pk)

    print('staff handling courses')
    pprint(list_of_staff_handling_courses)

    courses_list = []
    for each in list_of_staff_handling_courses:
        courses_list.append(each.course)

    available_hours = 8

    current_hour = 1
    today_hours = []
    while current_hour <= available_hours:
        temp = {}
        temp['hour'] = current_hour
        if time_table.objects.filter(is_active=True,
                                     day__day_name=day_name, hour=current_hour).exists():
            list_of_hours = []
            for each_course_hour in time_table.objects.filter(is_active=True,
                                                              day__day_name=day_name, hour=current_hour,
                                                              course__in=courses_list):
                if each_course_hour.semester.pk in semester_pks:
                    course_instance_code = courses.objects.get(pk=each_course_hour.course.pk)
                    temp_list = {}
                    # temp['hour'] = current_hour
                    temp_list['course_id'] = course_instance_code.course_id
                    temp_list['course_name'] = course_instance_code.course_name
                    temp_list['programme'] = course_instance_code.programme
                    semester_number = course_instance_code.semester
                    if semester_number == 1 or semester_number == 2:
                        temp_list['year'] = 'I Year'
                    elif semester_number == 3 or semester_number == 4:
                        temp_list['year'] = 'II Year'
                    elif semester_number == 5 or semester_number == 6:
                        temp_list['year'] = 'III Year'
                    elif semester_number == 7 or semester_number == 8:
                        temp_list['year'] = 'IV Year'
                    if course_instance_code.code:
                        temp_list['code'] = course_instance_code.code
                    list_of_hours.append(temp_list)
            if list_of_hours:
                temp['courses'] = list_of_hours
            today_hours.append(temp)
        else:
            today_hours.append(temp)
        # today_hours.append(hour_list)
        current_hour = current_hour + 1

    return today_hours


def student_hours(user):
    custom_today_hours = []
    now = datetime.now()

    student_instance = student.objects.get(user=user)
    department_instance = student_instance.department
    selected_batch = student_instance.batch_id
    selected_section = section_students.objects.get(student=student_instance)

    semester_instance = get_current_semester_of_given_batch(department_instance, selected_section.section)
    batch_instance = batch.objects.get(id=selected_batch)


    dayInstance = getDayInstanceForTodaysTimetable(semester_instance)

    available_hours = 8
    current_hour = 1

    while current_hour <= available_hours:
        temp = {}
        temp['hour'] = current_hour
        if time_table.objects.filter(department=department_instance, is_active=True,
                                     batch=batch_instance, semester=semester_instance,
                                     day=dayInstance, hour=current_hour).exists():
            list_of_courses = []
            for each_course_hour in time_table.objects.filter(department=department_instance, is_active=True,
                                                              batch=batch_instance, semester=semester_instance,
                                                              day=dayInstance, hour=current_hour):
                course_instance = each_course_hour.course
                if course_instance.regulation.start_year <= 2012:
                    if course_instance.is_elective or course_instance.is_open or course_instance.is_one_credit:
                        if student_enrolled_courses.objects.filter(course=course_instance,
                                                                   student=student_instance, approved=True):
                            temp_list = {}
                            temp_list['course_id'] = course_instance.course_id
                            temp_list['course_name'] = course_instance.course_name
                            for instance in staff_course.objects.filter(course=course_instance,
                                                                        department=department_instance,
                                                                        batch=batch_instance,
                                                                        semester=semester_instance):
                                temp_list['staff_list'] = instance.staffs.all()
                            if course_instance.code:
                                temp_list['code'] = course_instance.code
                            list_of_courses.append(temp_list)
                    else:
                        temp_list = {}
                        temp_list['course_id'] = course_instance.course_id
                        temp_list['course_name'] = course_instance.course_name
                        for instance in staff_course.objects.filter(course=course_instance,
                                                                    department=department_instance,
                                                                    batch=batch_instance,
                                                                    semester=semester_instance):
                            temp_list['staff_list'] = instance.staffs.all()
                        if course_instance.code:
                            temp_list['code'] = course_instance.code
                        list_of_courses.append(temp_list)
                else:
                    if course_enrollment.objects.filter(course=course_instance, semester=semester_instance,
                                                        registered=True):
                        temp_list = {}
                        temp_list['course_id'] = course_instance.course_id
                        temp_list['course_name'] = course_instance.course_name
                        for instance in staff_course.objects.filter(course=course_instance,
                                                                    department=department_instance,
                                                                    batch=batch_instance,
                                                                    semester=semester_instance):
                            temp_list['staff_list'] = instance.staffs.all()
                        if course_instance.code:
                            temp_list['code'] = course_instance.code
                        list_of_courses.append(temp_list)

            temp['courses'] = list_of_courses
            custom_today_hours.append(temp)
        else:
            custom_today_hours.append(temp)
        current_hour = current_hour + 1

    # pprint(custom_today_hours)
    # if new_regulation:
    hours = [1, 2, 3, 4, 5, 6, 7, 8]
    # else:
    #     hours = [1, 2, 3, 4, 5, 6, 7]
    for hour in hours:
        if any(hour == item['hour'] for item in custom_today_hours):
            pass
        else:
            temp = {}
            temp['hour'] = hour
            custom_today_hours.append(temp)

    # today_hours = sorted(custom_today_hours, key=itemgetter('hour'))

    # pprint(today_hours)

    return custom_today_hours


def new_approval(request):
    current_user = staff.objects.get(user=request)
    custom_list_of_hods = []
    custom_list_of_staffs_associates = []
    custom_list_of_staffs_assistants = []
    custom_list_of_staffs_network_admins = []
    custom_list_of_students = []
    list_of_hods = []
    list_of_staffs = []
    list_of_students = []
    if request.groups.filter(name='principal').exists():
        custom_list_of_hods = CustomUser.objects.filter(
            staff__designation='Professor',
            is_approved=False,
            is_verified=True,
            has_filled_data=True
        )
    if request.groups.filter(name='hod').exists():
        custom_list_of_staffs_associates = CustomUser.objects.filter(
            staff__department=current_user.department,
            staff__designation='Associate Professor',
            is_approved=False,
            is_verified=True,
            has_filled_data=True
        )
        custom_list_of_staffs_assistants = CustomUser.objects.filter(
            staff__department=current_user.department,
            staff__designation='Assistant Professor',
            is_approved=False,
            is_verified=True,
            has_filled_data=True
        )
        custom_list_of_staffs_network_admins = CustomUser.objects.filter(
            staff__department=current_user.department,
            staff__designation='Network Admin',
            is_approved=False,
            is_verified=True,
            has_filled_data=True
        )
    if request.groups.filter(name='faculty_advisor').exists():
        list_of_batches = []
        for active_sem in semester_migration.objects.filter(department=current_user.department):
            faculty_advisors = active_sem.semester.faculty_advisor.all()
            if current_user in faculty_advisors:
                list_of_batches.append(active_sem.semester.batch)
        custom_list_of_students = CustomUser.objects.filter(
            student__department=current_user.department,
            student__batch__in=list_of_batches,
            is_approved=False,
            is_verified=True,
            has_filled_data=True
        )

    list0 = staff.objects.filter(user__in=custom_list_of_hods).order_by('staff_id')

    list1 = staff.objects.filter(user__in=custom_list_of_staffs_associates).order_by('staff_id')
    list2 = staff.objects.filter(user__in=custom_list_of_staffs_assistants).order_by('staff_id')
    list3 = staff.objects.filter(user__in=custom_list_of_staffs_network_admins).order_by('staff_id')

    for each in list0:
        list_of_hods.append(each)
    for each in list1:
        list_of_staffs.append(each)
    for each in list2:
        list_of_staffs.append(each)
    for each in list3:
        list_of_staffs.append(each)

    list4 = student.objects.filter(user__in=custom_list_of_students).order_by('roll_no')
    for each in list4:
        list_of_students.append(each)

    no_of_users_to_approve = len(list_of_hods) + len(list_of_staffs) + len(list_of_students)
    return no_of_users_to_approve


def datetimereturn(request):
    return datetime.now()
