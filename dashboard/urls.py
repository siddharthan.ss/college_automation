from django.conf.urls import url

from dashboard.views import get_dashboard
from dashboard.dashboard_includes import datetimereturn

urlpatterns = [
    url(r'^', get_dashboard, name='dashboard'),
    url(r'^', datetimereturn, name='datetimereturn'),
]
