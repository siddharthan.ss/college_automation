from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils import timezone
from accounts.models import student,staff
from curriculum.models import day, section_students,semester_migration
from curriculum.views import is_holiday
from curriculum.views.courses_session import add_courses_offered_to_session
from curriculum.views.common_includes import get_students_under_staff,get_students_under_staffPrev,get_departments_of_hod
from dashboard.notification import send_notification
from messaging.views import show_notifications
from .dashboard_includes import staff_hours, student_hours, new_approval
import datetime as dt
from alumni.models import alumini_scholoarship_registration,rejectedAppl,bonafideIns


@login_required
def get_dashboard(request):
    add_courses_offered_to_session(request)
    # add_labs_to_session(request)

    logged_first = None
    if request.session.get('logged_first', None):
        logged_first = request.session['logged_first']
    request.session['logged_first'] = False

    now = timezone.now()
    day_name = now.strftime("%A")
    hour_avail_today = False
    today_hours = None
    new_approval_avail = False
    staff_account = True
    if is_holiday(timezone.now().date()):
        hour_avail_today = False
    elif day.objects.filter(day_name=day_name):
        hour_avail_today = True

    no_section_assigned = False

    if request.user.groups.filter(name='hod').exists():
        hod_in=staff.objects.get(user=request.user)
        eligibleDepts=get_departments_of_hod(hod_in)
        studentLis=student.objects.filter(department__in=eligibleDepts)
        bonLen=len(bonafideIns.objects.filter(student__in=studentLis,status=2))
        if bonLen>0:
            send_notification(recipient=request.user,message=str(bonLen)+' bonafides are waiting for your forwarding.',url='alumni/apprHOD')

    if request.user.groups.filter(name='faculty_advisor').exists():
        semSample=semester_migration.objects.all()[0]
        date=semSample.semester.start_term_1-dt.timedelta(120)
        print(date)
        LisStud=get_students_under_staff(staff.objects.get(user=request.user))
        studLisBon=len(bonafideIns.objects.filter(student__in=LisStud,status=1))
        if studLisBon>0:
            send_notification(recipient=request.user,message=str(studLisBon)+' bonafides are waiting for your forwarding.',url='alumni/apprFA')
        #LisStud=get_students_under_staff(staff.objects.get(user=request.user))
        count1=0
        for i in LisStud:
            if alumini_scholoarship_registration.objects.filter(student=i,applicationStatus=1):
                count1+=1
        if rejectedAppl.objects.filter(staff=staff.objects.get(user=request.user)).exists():
            send_notification(recipient=request.user,message='Are you want to allow reapply for the disApproved candidates?',url='alumni/reApply')
        if count1>0:
            print('Student',LisStud)
            send_notification(recipient=request.user,message='You have to fill conduct for Students',url='alumni/fillConduct')
            
    if request.user.groups.filter(name='faculty').exists():
        LisStud=get_students_under_staff(staff.objects.get(user=request.user))
        #LisStud=get_students_under_staff(staff.objects.get(user=request.user))
        count1=0
        for i in LisStud:
            if alumini_scholoarship_registration.objects.filter(student=i,applicationStatus=1):
                count1+=1
        if rejectedAppl.objects.filter(staff=staff.objects.get(user=request.user)).exists():
            send_notification(recipient=request.user,message='Are you want to allow reapply for the disApproved candidates?',url='alumni/reApply')
        if count1>0:
            print('Student',LisStud)
            send_notification(recipient=request.user,message='You have to fill conduct for Students',url='alumni/fillConduct')
        today_hours = staff_hours(request.user)
    elif request.user.groups.filter(name='student').exists():
        staff_account = False
        if day.objects.filter(day_name=day_name).exists():
            try:
                selected_section = section_students.objects.get(student__user=request.user)
                today_hours = student_hours(request.user)
            except:
                hour_avail_today = False
                no_section_assigned = True


        '''if student.objects.get(user=request.user).Pending_GPA:
            stud_inst=student.objects.get(user=request.user)
            stud_inst.Pending_GPA=False
            stud_inst.save()
            send_notification(recipient=request.user,message='You have to fill the semester marks',url='alumni/fill_sem_marks') 
        '''
            


    if logged_first:
        # request.session['pending_attendances'] = True
        # notify_pending_attendances(request)
        if request.user.groups.filter(name='principal').exists() or request.user.groups.filter(
                name='hod').exists() or request.user.groups.filter(name='faculty_advisor').exists():
            list_len = new_approval(request.user)
            if list_len != 0:
                new_approval_avail = True
            if new_approval_avail:
                send_notification(recipient=request.user, message='You have ' + str(list_len) + ' users to approve',
                                  url='approval')

    list_of_notifications = show_notifications(request)

    return render(request, 'dashboard/dashboard.html', {
        'notifications': list_of_notifications,
        'today_hours': today_hours,
        'staff_account': staff_account,
        'hour_avail_today': hour_avail_today,
        'new_approval_avail': new_approval_avail,
        'logged_first': logged_first,
        'no_section_assigned': no_section_assigned
    })