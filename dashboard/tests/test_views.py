from datetime import datetime

from django.test import TestCase, Client

from accounts.models import department, batch, staff, CustomUser
from curriculum.models import semester, time_table, courses, day


class DashboardTest(TestCase):
    fixtures = [
        'ct.json', 'perms.json',
        'group.json', 'customuser.json', 'batch.json',
        'department.json', 'staff.json', 'student.json',
        'courses.json', 'day.json'
    ]

    def setUp(self):
        self.client = Client()
        dept_inst = department.objects.get(acronym='CSE')
        batch_inst = batch.objects.filter(start_year=2016).get(end_year=2020)
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        staff_inst = staff.objects.get(user=cust_inst)
        semester_obj = semester(
            department=dept_inst,
            batch=batch_inst,
            semester_number=1,
        )
        semester_obj.save()
        semester_obj.faculty_advisor.add(staff_inst)

        self.course_inst = courses.objects.get(course_id='16SHS1Z1')
        day_inst = day.objects.get(day_name=str(datetime.today().strftime("%A")))

        self.timetable_inst = time_table(
            course=self.course_inst,
            department=dept_inst,
            day=day_inst,
            hour=2,
            staff=staff_inst,
            sem=semester_obj,
            batch=batch_inst,
        )
        self.timetable_inst.save()

    def test_anonymous_login(self):
        response = self.client.get('/dashboard')
        self.assertTemplateNotUsed(response, 'dashboard/dashboard.html')

    def test_staff_dashbord(self):
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.get('/dashboard')
        # print(vars(response))
        self.assertTemplateUsed(response, 'dashboard/dashboard.html')
        self.assertContains((response), str(self.course_inst))
        self.assertContains((response), self.course_inst.programme)

    def test_staff_other_dept_dashbord(self):
        logged = self.client.login(email='itf1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.get('/dashboard')
        # print(vars(response))
        self.assertTemplateUsed(response, 'dashboard/dashboard.html')
        self.assertNotContains((response), str(self.course_inst))
        self.assertNotContains((response), self.course_inst.programme)

    def test_student_dashboard(self):
        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.get('/dashboard')
        # print(vars(response))
        self.assertTemplateUsed(response, 'dashboard/dashboard.html')
        self.assertContains((response), str(self.course_inst))
        self.assertContains((response), str(self.timetable_inst.staff))

    def test_student_other_dept_dashboard(self):
        logged = self.client.login(email='s4@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.get('/dashboard')
        # print(vars(response))
        self.assertTemplateUsed(response, 'dashboard/dashboard.html')
        self.assertNotContains((response), str(self.course_inst))
        self.assertNotContains((response), str(self.timetable_inst.staff))
