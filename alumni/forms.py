from django import forms

from alumni.models import alumini_scholoarship_registration


class AlumniScholarshipForm(forms.ModelForm):
    class Meta:
        model = alumini_scholoarship_registration
        fields = [
            'brother_sister_details',
            'bank_name',
            'account_number',
        ]

    def __init__(self, *args, **kwargs):
        super(AlumniScholarshipForm, self).__init__(*args, **kwargs)
        self.fields['brother_sister_details'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Name and Designation of siblings in separete lines'
        })
        self.fields['bank_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Name of bank'
        })
        self.fields['account_number'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Account Number of bank'
        })
