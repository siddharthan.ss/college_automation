from accounts.models import student
from alumni.models import *
from curriculum.views.common_includes import get_students_under_staff,get_departments_of_hod
from django.utils import timezone
from django.shortcuts import render,redirect
from easy_pdf.views import PDFTemplateView
from django.http import Http404
from django.contrib.auth.decorators import permission_required
from dashboard.notification import send_notification
from accounts.views.mail import sendEmail

def apply_bonafide(request):
    student_instance=student.objects.get(user=request.user)
    if request.method=='POST':
        if 'updateAmt' in request.POST:
            lisIns=[]
            for ins in bonafideIns.objects.filter(student=student_instance):
                if ins.status==9:
                    lisIns.append(ins)
            for ins in lisIns:
                try:
                    status=request.POST['status'+str(ins.pk)]
                    print(status)
                except:
                    value=request.POST['val'+str(ins.pk)]
                    ins.status=10
                    ins.max_amount=value
                    ins.save()
                
        try:
            method=request.POST['method']
            pk=method.split('%^')[2]
            print(pk)
            comment=request.POST['comment']
            print(comment)
            max_am=int(request.POST['max_amt'])
            print(max_am)
            bon=bonafide.objects.get(pk=pk)
            print('haha')
            bonafideIns.objects.create(student=student_instance,comment=comment,status=1,max_amount=max_am,bonafide=bon,applied_date=timezone.now(),approved_date=timezone.now(),yrDuringAppr=student_instance.get_current_year())
        except:
            print('Err')
    bonLis=[]
    lisIns=[]
    for instan in bonafideIns.objects.filter(student=student_instance):
        if instan.status==9:
            lisIns.append(instan)
    for ins1 in bonafide.objects.filter(disabled=False):
        bonLis.append(ins1)
    past=[]
    for i in bonafideIns.objects.filter(student=student_instance):
        pas={}
        tempLi=i.bonafide.typeOfTrack.staffs.split(',')
        if i.status==-1:
            pas['str']='Rejected'
        elif i.status<6:
            pas['str']='Waiting for '+tempLi[i.status-1]
        elif i.status==8:
            pas['str']='Waiting for Principal'
        elif i.status==9:
            pas['str']='Approved'
        elif i.status==10:
            pas['str']='Received'
        pas['appl']=i
        past.append(pas)
    if len(lisIns)>0:
        return render(request,'alumni/bonafide_apply.html',context={'list':lisIns,'exis':True,'lis':bonLis,'past':past,'ins':student_instance,'now':timezone.now().date})
    return render(request,'alumni/bonafide_apply.html',context={'lis':bonLis,'past':past,'ins':student_instance,'now':timezone.now().date})


'''
def track_status(request):
    student_instance=student.objects.get(user=request.user)
    apprLis=bonafideIns.objects.filter(student=student_instance,status__gte=9)
    rejLis=bonafideIns.objects.filter(student=student_instance,status=-1)
    applLis=bonafideIns.objects.filter(student=student_instance,status__gte=1,status__lte=8)
    return render(request,'bonafide_apply',{'apprLis':apprLis,'rejLis':rejLis,'applLis':applLis})
'''
def approve_bonafide_fa(request):
    staff_ins=staff.objects.get(user=request.user)
    studentsLis=get_students_under_staff(staff_ins)
    bonLis=[]
    for studBon in bonafideIns.objects.filter(student__in=studentsLis,status=1).order_by('-applied_date'):
        bonLi={}
        bonLi['lis']=studBon
        stud=studBon.student
        bonLi['pasLis']=bonafideIns.objects.filter(student=stud,status__gte=9)
        bonLis.append(bonLi)
    if request.method=='POST':
        for ins in bonLis:
            try:
                instance=ins['lis']
                status=request.POST['status'+str(instance.pk)]
                if status=='Approve':
                    instance.status+=1
                    instance.save()
                elif status=='Reject':
                    reason=instance.bonafide.reason
                    send_notification(recipient=instance.student.user,message='It seems you are not eligible for this bonafide for '+reason+'. Please contact your Faculty Advisor',url='dashboard')
                    data={}
                    data['message']='It seems you are not eligible for this bonafide for '+reason+'. Please contact your Faculty Advisor'
                    data['email']=instance.student.user.email
                    print(data['email'])
                    data['subject']='Issues regarding Bonafide Scholarship' 
                    sendEmail(data)
                    instance.status=-1
                    instance.save()
            except:
                print('Err')
        bonLis=[]
        for studBon in bonafideIns.objects.filter(student__in=studentsLis,status=1).order_by('-applied_date'):
            bonLi={}
            bonLi['lis']=studBon
            stud=studBon.student
            bonLi['pasLis']=bonafideIns.objects.filter(student=stud,status__gte=9)
            bonLis.append(bonLi)
    if len(bonLis)>0:
        return render(request,'alumni/apprBonStaf.html',{'bonLis1':bonLis,'hasData':1,'len1':len(bonLis),'len2':0})
    else:
        return render(request,'alumni/apprBonStaf.html',{'hasData':0})

@permission_required('alumni.can_download_bonafide_pdfs')
def approve_bonafide_hod(request):
    hod_in=staff.objects.get(user=request.user)
    eligibleDepts=get_departments_of_hod(hod_in)
    studentLis=student.objects.filter(department__in=eligibleDepts)
    typ1=Type.objects.filter(num__in=[1])
    typ2=Type.objects.filter(num__in=[2])
    bonLis1=[]
    bonLis2=[]
    for studBon in bonafideIns.objects.filter(student__in=studentLis,bonafide__typeOfTrack__in=typ1,status=2):
        bonLi={}
        bonLi['lis']=studBon
        bonLi['pasLis']=bonafideIns.objects.filter(student=studBon.student,status__gte=9)
        bonLis1.append(bonLi)
    for studBon in bonafideIns.objects.filter(student__in=studentLis,bonafide__typeOfTrack__in=typ2,status=2):
        bonLi={}
        bonLi['lis']=studBon
        bonLi['pasLis']=bonafideIns.objects.filter(student=studBon.student,status__gte=9)
        bonLis2.append(bonLi)
    if request.method=='POST':
        for ins in bonLis1:
            try:
                instance=ins['lis']
                status=request.POST['status'+str(instance.pk)]
                print(status)
                if status=='Approve':
                    instance.status=8
                    instance.save()
                elif status=='Reject':
                    print('Here')
                    reason=instance.bonafide.reason
                    send_notification(recipient=instance.student.user,message='It seems you are not eligible for this bonafide for '+reason+'. Please contact your HOD',url='dashboard')
                    print('heree')
                    data={}
                    data['message']='It seems you are not eligible for this bonafide for '+reason+'. Please contact your HOD'
                    data['email']=instance.student.user.email
                    print(data['email'])
                    data['subject']='Issues regarding Bonafide Scholarship' 
                    sendEmail(data)
                    print('Hereee')
                    instance.status=-1
                    instance.save() 
                    print('Hereeeee')
            except:
                print('Error')
        pkLis=[]
        minPk=100000000000000000000
        for ins in bonLis2:
            try:
                instance=ins['lis']
                status=request.POST['status'+str(instance.pk)]
                print(status)
                if status=='Approve':
                    instance.status=9
                    instance.approved_date=timezone.now()
                    if instance.pk<minPk:
                        minPk=instance.pk
                    instance.yrDuringAppr=instance.student.get_current_year()
                    instance.save()
                    pkLis.append(instance.pk)
                elif status=='Reject':
                    reason=instance.bonafide.reason
                    send_notification(recipient=instance.student.user,message='It seems you are not eligible for this bonafide for '+reason+'. Please contact your HOD',url='dashboard')
                    data={}
                    data['message']='It seems you are not eligible for this bonafide for '+reason+'. Please contact your HOD'
                    data['email']=instance.student.user.email
                    print(data['email'])
                    data['subject']='Issues regarding Bonafide Scholarship' 
                    sendEmail(data)
                    instance.status=-1
                    instance.save() 
            except:
                print('Error')
        if len(pkLis)>0:
            redir='alumni/download_bonafides/'+str(minPk)+str(2)
            return redirect(redir)
        bonLis1=[]
        bonLis2=[]
        for studBon in bonafideIns.objects.filter(student__in=studentLis,bonafide__typeOfTrack__in=typ1,status=2):
            bonLi={}
            bonLi['lis']=studBon
            bonLi['pasLis']=bonafideIns.objects.filter(student=studBon.student,status__gte=9)
            bonLis1.append(bonLi)
        for studBon in bonafideIns.objects.filter(student__in=studentLis,bonafide__typeOfTrack__in=typ2,status=2):
            bonLi={}
            bonLi['lis']=studBon
            bonLi['pasLis']=bonafideIns.objects.filter(student=studBon.student,status__gte=9)
            bonLis2.append(bonLi)
    if len(bonLis1)>0 or len(bonLis2)>0:
        return render(request,'alumni/apprBonStaf.html',{'hasData':1,'bonLis1':bonLis1,'bonLis2':bonLis2,'len1':len(bonLis1),'len2':len(bonLis2)})
    else:
        return render(request,'alumni/apprBonStaf.html',{'hasData':0})


@permission_required('alumni.can_change_bonafide_format')
def approve_bonafide_princ(request):
    bonLis=[]
    for studBon in bonafideIns.objects.filter(status=8):
        bonLi={}
        bonLi['lis']=studBon
        bonLi['pasLis']=bonafideIns.objects.filter(student=studBon.student,status__gte=9)
        bonLis.append(bonLi)
    if request.method=='POST':
        pkLis=[]
        minPk=100000000000000000000
        for ins in bonLis:
            print(ins['lis'].pk)
            try:
                instance=ins['lis']
                status=request.POST['status'+str(instance.pk)]
                if status=='Approve':
                    instance.status=9
                    if instance.pk<minPk:
                        minPk=instance.pk
                    instance.approved_date=timezone.now()
                    instance.yrDuringAppr=instance.student.get_current_year()
                    instance.save()
                    pkLis.append(instance.pk)
                elif status=='Reject':
                    reason=instance.bonafide.reason
                    send_notification(recipient=instance.student.user,message='It seems you are not eligible for this bonafide for '+reason+'. Please contact office admin in main block.',url='dashboard')
                    data={}
                    data['message']='It seems you are not eligible for this bonafide for for '+reason+'. Please contact office admin in main block.'
                    data['email']=instance.student.user.email
                    print(data['email'])
                    data['subject']='Issues regarding Bonafide Scholarship' 
                    sendEmail(data)
                    instance.status=-1
                    instance.save()
            except:
                print('Error')
        if len(pkLis)>0:
            redir='alumni/download_bonafides/'+str(minPk)+str(1)
            return redirect(redir)
        bonLis=[]
        for studBon in bonafideIns.objects.filter(status=8):
            bonLi={}
            bonLi['lis']=studBon
            bonLi['pasLis']=bonafideIns.objects.filter(student=studBon.student,status__gte=9)
            bonLis.append(bonLi)
    if len(bonLis)>0:
        return render(request,'alumni/apprBonStaf.html',{'hasData':1,'bonLis2':bonLis,'len1':0,'len2':len(bonLis)})
    else:
        print('Hii')
        return render(request,'alumni/apprBonStaf.html',{'hasData':0})
'''
class pdfBonafide(PDFTemplateView):
    template_name = "alumni/alumni_scholarship_appl_alum_pdf.html"
    application_instance = None
    def get(self, request, *args, **kwargs):
        return super(applFormPdfAlum, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        bona
        sem = \
        semester.objects.filter(semester_number=active_batches_instance.current_semester_number_of_this_batch).order_by(
            '-start_term_1')[0]
        print('semester_number')
        print(active_batches_instance.current_semester_number_of_this_batch)

        current_semester = active_batches_instance.current_semester_number_of_this_batch
        try:
            studHisIns = student_history.objects.get(semester=sem, student=stud_inst)
            SchDetLis = {}
        
            SchDetLis['prevst']=studHisIns.prev_st
            SchDetLis['curst']=studHisIns.cur_st
            SchDetLis['prevcen']=studHisIns.prev_cen
            SchDetLis['curcen']=studHisIns.cur_cen
            SchDetLis['preval']=studHisIns.prev_alum
            SchDetLis['cural']=studHisIns.cur_alum
        except:
            SchDetLis = {}
        
            SchDetLis['prevst']=-1
            SchDetLis['curst']=-1
            SchDetLis['prevcen']=-1
            SchDetLis['curcen']=-1
            SchDetLis['preval']=-1
            SchDetLis['cural']=-1
        weightIns=weightage.objects.get(name='CommonAlumniScholarship')
        try:
            coeIns=COEdetails.objects.get(student=stud_inst)
            COEmark=coeIns.CGPA
            COEnoOfar=coeIns.noOfArrears
            COEattPer=coeIns.attendancePerc
            COESch=coeIns.prevStateSch
        except:
            COEmark=0
            COEnoOfar=0
            COEattPer=0
            COESch=0
        if (current_semester % 2 == 0):
            year = int(current_semester / 2)
        else:
            year = int((current_semester + 1) / 2)
        context_data = {
            'application': self.application_instance,
            'current_semester': current_semester,
            'year': year,
            'SchDet': SchDetLis,
            'weightage':weightIns,
            'COEmark':COEmark,
            'COEnoOfar':COEnoOfar,
            'COEattPer':COEattPer,
            'COESch':COESch,
        }
        print(context_data)
        return super(applFormPdfAlum, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )
'''


@permission_required('alumni.can_change_bonafide_format')
def view_bonafide_formats(request):
    bonLists=bonafide.objects.all()
    return render(request,'alumni/view_bonafide_format.html',{'lists':bonLists})


@permission_required('alumni.can_change_bonafide_format')
def edit_formats(request):
    if request.method=='POST':
        try:
            changedPks=request.POST.getlist('change1')
            for inst in changedPks:
                bon=bonafide.objects.get(pk=inst)
                if bon.disabled==True:
                    bon.disabled=False
                else:
                    bon.disabled=True
                bon.save()
        except:
            print('Wrong')
    bonLists=bonafide.objects.all()
    return render(request,'alumni/disable_format.html',{'lists':bonLists})


@permission_required('alumni.can_change_bonafide_format')
def add_bonafide_formats(request):
    if request.method=='POST':
        try:
            reason=request.POST['reason']
            description=request.POST['description']
            amount=request.POST['max_amnt']
            if amount==0:
                amount=-1
            typ=request.POST['method']
            Typ=Type.objects.get(pk=typ)
            bonafide.objects.create(reason=reason,description=description,typeOfTrack=Typ,max_amnt=amount)
            modal = {
                'heading': 'Added',
                'body': 'Your newly added bonafide format is saved',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
        except:
            print('Error in getting variables')
    Typ=Type.objects.all()
    return render(request,'alumni/add_bonafide_format.html',{'type':Typ})

class download_bonafides(PDFTemplateView):
    template_name = "alumni/bonafide_pdf.html"
    def get(self, request, *args, **kwargs):
        application_id = kwargs.pop('pk')
        Type=kwargs.pop('type')
        print(application_id,Type)
        lists=bonafideIns.objects.filter(status=9,pk__gte=application_id,bonafide__typeOfTrack__num=Type).order_by('-approved_date')
        if lists.exists():
            self.lists =lists 
        else:
            raise Http404
        return super(download_bonafides, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        print(self.lists)
        return super(download_bonafides, self).get_context_data(
            pagesize="A4",
            context={'lists':self.lists},
            **kwargs
        )

class download30DaysPdf(PDFTemplateView):
    template_name="alumni/bonafide_pdf.html"
    def get(self, request, *args, **kwargs):
        print('HOIIIIIIIIIIIIIIIIIIIIIIIIIIi')
        application_id = kwargs.pop('pk')
        print(application_id)
        applns=[]
        for i in application_id.split(','):
            applns.append(int(i))
        print('applicationId',applns)
        lists=bonafideIns.objects.filter(status__gte=9,pk__in=applns).order_by('-approved_date')
        if lists.exists():
            self.lists =lists 
        else:
            raise Http404
        return super(download30DaysPdf, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        print(self.lists,'jhhhhhhhhhhhhhhhhhhhhhhhh')
        return super(download30DaysPdf, self).get_context_data(
            pagesize="A4",
            context={'lists':self.lists},
            **kwargs
        )


@permission_required('alumni.can_download_bonafide_pdfs')
def download_30Days(request):
    if request.method=='POST':
        try:
            lisOfAppl=request.POST.getlist('selected_applns')
            retStr='-'.join(lisOfAppl)
            ret='alumni/download30DaysPdf/'+retStr
            print('Ready to send')
            return redirect(ret)
        except:
            print('Some error here')
    staffIns=staff.objects.get(user=request.user)
    now=timezone.now()
    lis=[]
    if request.user.groups.filter(name="principal").exists():
        lis=[]
        bon=bonafide.objects.filter(typeOfTrack__num=1)
        for ins in bonafideIns.objects.filter(status__gte=9,bonafide__in=bon):
            day=(now-ins.approved_date).days
            if day<=30:
                lis.append(ins)
    elif request.user.groups.filter(name='hod').exists():
        bon=bonafide.objects.filter(typeOfTrack__num=2)
        lis=[]
        for ins in bonafideIns.objects.filter(status__gte=9,bonafide__in=bon):
            day=(now-ins.approved_date).days
            if day<=30:
                lis.append(ins)
    if len(lis)>0:
        return render(request,'alumni/downloadPDF.html',{'list':lis})
    else:
        msg = {
            'page_title': 'No Application',
            'title': 'Non-Availability',
            'description': 'There is no application available for 30 days',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})