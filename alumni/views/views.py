import datetime as dt
import pandas as pd
import xlrd,csv
from datetime import datetime
from django.contrib.auth.decorators import permission_required
from django.http import Http404
from django.http import HttpResponseRedirect, JsonResponse,HttpResponse
from django.shortcuts import render,redirect
# Create your views here.
from django.utils import timezone
from easy_pdf.views import PDFTemplateView
from django.db.models import Q
from accounts.models import student, active_batches,staff
from accounts.views.mail import sendEmail 
from alumni.forms import AlumniScholarshipForm
from alumni.models import *
from curriculum.models import attendance, semester, batch, staff_course,semester_migration
from attendance.views.ConsolidatedAttendance2012API import ConsolidatedAttendance2012API
from attendance.views.SubjectConsolidatedAttendanceAPI import SubjectConsolidatedAttendanceAPI
from curriculum.views.common_includes import get_courses_studied_by_student,get_students_under_staffPrev,get_students_under_staff
from common.utils.ReportTabsUtil import getStaffCoursesForSemester
from common.API.departmentAPI import getDepartmentOfPk, getCoreDepartments
from dashboard.notification import send_notification
from decimal import *
         
@permission_required('alumni.can_apply_alumini_scholarship')
def apply_alumni_scholarship(request):
    student_instance = student.objects.get(user=request.user)
    active_batches_instance = active_batches.objects.filter(
        department=student_instance.department
    ).get(
        batch=student_instance.batch
    )

    if student_instance.aadhaar_number is None or len(student_instance.aadhaar_number) == 0:
        msg = {
            'page_title': 'Profile Incomplete',
            'title': 'Incomplete Profile',
            'description': 'Complete your Profile inorder to apply for alumni scholarship',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    sem = semester.objects.filter(semester_number=active_batches_instance.current_semester_number_of_this_batch).order_by(
        '-start_term_1')[0]
    #GPcheck = GradePoints.objects.filter(student=student_instance, semester=sem)
    '''if not GPcheck.exists():
        msg = {
            'page_title': 'Mark Not Updated',
            'title': 'No Mark Updations',
            'description': 'Complete your Mark filling inorder to apply for alumni scholarship',
        }
        return render(request, 'prompt_pages/marks_not_filled.html', {'message': msg})'''

    apply_alumni_scholarship_form = AlumniScholarshipForm()
    if alumini_scholoarship_registration.objects.filter(student=student_instance,applicationStatus__gte=1).exists():
        inst = alumini_scholoarship_registration.objects.filter(student=student_instance,applicationStatus__gte=1).order_by('-date_of_application')[0]
        alumniForms={}
        alumniForms['form']=inst
        if inst.applicationStatus==-1:
            alumniForms['status']='Closed by Staff'
        elif inst.applicationStatus==1:
            alumniForms['status']='Applied to Faculty'
        elif inst.applicationStatus==2:
            alumniForms['status']='Faculty Approved'


        msg = {
                'page_title': 'Already open application',
                'title': 'Application already exists',
                'description': 'You have already applied for alumni scholarship this year',
        }
        return render(request, 'prompt_pages/alumniFilled.html', {'message': msg,'form':alumniForms})
    if alumini_scholoarship_registration.objects.filter(student=student_instance,applicationStatus=-1).exists():
        msg = {
                'page_title': 'Already open application',
                'title': 'Application already exists',
                'description': 'You have already applied for alumni scholarship this year and it is not processed. You contact Faculty Advisor',
        }
        return render(request, 'prompt_pages/alumniFilled.html', {'message': msg,'form':[]})
    if request.method == "POST" and request.POST.get('apply_scholarship'):
        
        apply_alumni_scholarship_form = AlumniScholarshipForm(data=request.POST)
        if alumini_scholoarship_registration.objects.filter(student=student_instance,applicationStatus__gte=1).exists():
            inst = alumini_scholoarship_registration.objects.filter(student=student_instance,applicationStatus__gte=1).order_by('-date_of_application')[0]
            alumniForms={}
            alumniForms['form']=inst
            if inst.applicationStatus==-1:
                alumniForms['status']='Closed by Staff'
            elif inst.applicationStatus==1:
                alumniForms['status']='Applied to Faculty'
            elif inst.applicationStatus==2:
                alumniForms['status']='Faculty Approved'


            msg = {
                'page_title': 'Already open application',
                'title': 'Application already exists',
                'description': 'You have already applied for alumni scholarship this year',
            }
            return render(request, 'prompt_pages/alumniFilled.html', {'message': msg,'form':alumniForms})

        if apply_alumni_scholarship_form.is_valid():
            print('Entered')
            prev_cent = prev_state = prev_alum = cur_cent = cur_state = cur_alum = -1
            try:
                studHisIns = student_history.objects.get(semester=sem, student=student_instance)
                print(studHisIns.cur_cen, studHisIns.cur_st, studHisIns.cur_alum, studHisIns.prev_cen,
                      studHisIns.prev_st, studHisIns.prev_alum)

                if studHisIns.cur_cen == -1:
                    cur_cent = int(request.POST['This year central govt'])
                if studHisIns.cur_st == -1:
                    cur_state = int(request.POST['This year state govt'])
                if active_batches_instance.current_semester_number_of_this_batch % 2 == 0 and studHisIns.cur_alum == -1:
                    cur_alum = int(request.POST['This year alumni'])
                if active_batches_instance.current_semester_number_of_this_batch > 2:
                    if studHisIns.prev_cen == -1:
                        prev_cent = int(request.POST['Previous year central govt'])
                    if studHisIns.prev_st == -1:
                        prev_state = int(request.POST['Previous year state govt'])
                    if studHisIns.prev_alum == -1:
                        prev_alum = int(request.POST['Previous year alumni'])
                if cur_cent != -1:
                    studHisIns.cur_cen = cur_cent
                    print(cur_cent)
                if prev_cent != -1:
                    print(prev_cent)
                    studHisIns.prev_cen = prev_cent
                if cur_state != -1:
                    print(cur_state)
                    studHisIns.cur_st = cur_state
                if prev_state != -1:
                    print(prev_state)
                    studHisIns.prev_st = prev_state
                if cur_alum != -1:
                    print(cur_alum)
                    studHisIns.cur_alum = cur_alum
                if prev_alum != -1:
                    print(prev_alum)
                    studHisIns.prev_alum = prev_alum
                studHisIns.save()
                temp_instance = apply_alumni_scholarship_form.save(commit=False)
                temp_instance.diffAbled = request.POST['phyabdes']
                temp_instance.disParent = request.POST['disdes']
                levelsp = request.POST['levelsp']
                if levelsp != 'None':
                    temp_instance.sport = levelsp
                    temp_instance.spPlace ='('+request.POST['placesp']+')'
                temp_instance.project = request.POST['placepr']
                temp_instance.patent = request.POST['stagepat']
                temp_instance.ppt = request.POST['placep']
                temp_instance.internship = request.POST['internships']
                temp_instance.cgpa = request.POST['cgpa']
                temp_instance.noOfArrears = int(request.POST['noOfArrears'])
                club = ''
                for ins in request.POST.getlist('club_activities'):
                    club += ins + ','
                temp_instance.clubActivities = club
                prim_club = request.POST['primClAct'] 
                if prim_club != 'none':
                    temp_instance.primClub = prim_club
                    temp_instance.role = '(' + request.POST['role']+')'

            except:
                print('Failure in getting variables')
            temp_instance.date_of_application = timezone.now()
            temp_instance.student = student_instance
            temp_instance.applicationStatus = 1
            
            
            print(temp_instance)
            temp_instance.save()
            message={}
            message['title']="Waiting for Faculty Advisors' approval"
            message['description']="Contact your Faculty Advisor for updation of your Conduct marks and approval"
            AlForm={}
            AlForm['status']='Applied to Faculty'
            AlForm['form']=temp_instance
            return render(request,'prompt_pages/alumniFilled.html',{'message':message,'form':AlForm})
    # find alumni scholarships applied by student
    

    sch_detl = [False, False, False, False, False, False]
    # The part for computing the semester studied by a student so far successfully
    '''semes_list=attendance.objects.filter(Q(present_students=student_instance)|Q(absent_students=student_instance)|Q(onduty_students=student_instance)).values_list('semester',flat=True).distinct()
    semester_lis=list(semes_list)
    for inst in semes_list:
        sem_inst=semester.objects.get(id=inst)
        sem_nol.append(sem_inst.semester_number)
    dup_seml=set([x for x in sem_nol if sem_nol.count(x)>1])
    for duplicat_num in dup_seml:
        dup_sem_cur=[]
        def get_start_time(elem):
            return elem.start_term_1
        for inst in semes_list:
            sem_inst=semester.objects.get(id=inst)
            if sem_inst.semester_number==duplicat_num:
                dup_sem_cur.append(sem_inst)
        dup_sem_cur.sort(key=get_start_time,reverse=True)
        for i in range(1,len(dup_sem_cur):
            semester_lis.remove(i.id)'''
    # the part ends
    try:
        studHisIns = student_history.objects.get(semester=sem, student=student_instance)
    except:
        studHisIns = student_history.objects.create(semester=sem, student=student_instance)
    if studHisIns.cur_cen == -1:
        sch_detl[3] = 'This year central govt'
    if studHisIns.cur_st == -1:
        sch_detl[4] = 'This year state govt'
    if active_batches_instance.current_semester_number_of_this_batch % 2 == 0 and studHisIns.cur_alum == -1:
        sch_detl[5] = 'This year alumni'
    if active_batches_instance.current_semester_number_of_this_batch > 2:
        if studHisIns.prev_cen == -1:
            sch_detl[0] = 'Previous year central govt'
        if studHisIns.prev_st == -1:
            sch_detl[1] = 'Previous year state govt'
        if studHisIns.prev_alum == -1:
            sch_detl[2] = 'Previous year alumni'
    print(sch_detl)
    levels = ['University', 'State', 'District', 'National', 'zonal', 'International']
    places = ['Participation', 'Consolation Prize', '2ndPrize', '1stPrize']
    stages = ['Submitted', 'stage1', 'approved', 'granted']
    clubs = ['Tamil Mandram', 'LDS', 'SJC', 'NCC', 'NSS', 'Eliminators', 'Orchestra','FineArts','Green Club','TedX']
    other = ['Member', 'Active Participant', 'Leadership role', 'Awards']

    return render(request,
                  'alumni/alumni_scholarship_application.html',
                  {
                      'sch_det': sch_detl,
                      'form': apply_alumni_scholarship_form,
                      'applied_alumni_scholarships': [],
                      'sem_no': active_batches_instance.current_semester_number_of_this_batch,
                      'levels': levels, 'places': places, 'stages': stages, 'clubs': clubs, 'other': other,
                  }
                  )



@permission_required('alumni.can_apply_alumini_scholarship')
def delete_my_application(request, application_id):
    application_instance = alumini_scholoarship_registration.objects.get(pk=int(application_id))
    if application_instance.student.user == request.user:
        application_instance.delete()
        return HttpResponseRedirect('/alumni/apply_alumni_scholarship/')
    else:
        return Http404


#def finalScholarship(request):
#    alumniSchForms=alumini_scholoarship_registration.objects.filter(applicationStatus=3).filter(date_of_application__).filter()
#    if request.method=='POST':
#        number=request.POST['count']
#        eligibleForms=alumni_scholoarship_registration.objects.filter(applicationStatus=3).filter()
#    return render(request,'finalScholarship.html',{'form':alumniSchForms})
@permission_required('alumni.can_process_alumini_scholarship')
def close_application(request, application_id):
    application_query = alumini_scholoarship_registration.objects.filter(pk=int(application_id), applicationStatus__gte=1)
    if application_query.exists():
        application_instance = application_query.get()
        application_instance.applicationStatus = -1
        rejectedAppl.objects.create(staff=staff.objects.get(user=request.user),applId=application_instance)
        send_notification(recipient=application_instance.student.user,message='It seems you have done a mistake.You meet the alumni staff about progress of the your scholarship',url='dashboard')
        data={}
        data['message']='It seems you have done a mistake.You meet the alumni staff about progress of the your scholarship'
        data['email']=application_instance.student.user.email
        print(data['email'])
        data['subject']='Issues regarding Alumni Scholarship' 
        sendEmail(data)
        application_instance.save()
        
    return HttpResponseRedirect('/alumni/view_applications')


def closeApplStaff(request, application_id):
    #student_inst=get_students_under_staff(staff.objects.get(user=request.user))
    application_query = alumini_scholoarship_registration.objects.filter(pk=int(application_id), applicationStatus=1)
    if application_query.exists():
        application_instance = application_query[0]
        application_instance.applicationStatus = -1
        
        application_instance.save()
        rejectedAppl.objects.create(staff=staff.objects.get(user=request.user),applId=application_instance)
        send_notification(recipient=application_instance.student.user,message='It seems you have done a mistake.You concern your Faculty Advisor about progress of the your scholarship',url='dashboard')
        data={}
        data['message']='It seems you have done a mistake.You concern your Faculty Advisor about progress of the your scholarship'
        data['email']=application_instance.student.user.email
        print(data['email'])
        data['subject']='Issues regarding Alumni Scholarship'
        sendEmail(data)
        student_inst=get_students_under_staff(staff.objects.get(user=request.user))
        count=0
        scholarAppLis=[]
        for stud in student_inst:
            scholarshipIns = alumini_scholoarship_registration.objects.filter(student=stud).filter(applicationStatus=1).order_by('-date_of_application')
            if scholarshipIns.exists():
                count+=1
                scholarAppLis.append(scholarshipIns[0])
        if count>0:
            return render(request,'alumni/fillConduct.html',{'context':scholarAppLis,'len':count})
        else:
            message={}
            message['title']='Done Successfully'
            message['description']='You have no application for your wait. Thank You'
            return render(request,'prompt_pages/signup_success.html',{'message':message})
    else:
        message={}
        message['title']='Cant close Application'
        message['description']='That Application doesnt exist or you cant close it. Thank You'
        return render(request,'prompt_pages/signup_success.html',{'message':message})

def reApply(request):
    staffIns=staff.objects.get(user=request.user)
    if request.method=='POST':
        for scholarIns in rejectedAppl.objects.filter(staff=staffIns):
            #print(scholarIns.applicationStatus)
            try:
                if request.POST['allow'+str(scholarIns.pk)]=='Yes':
                    applL=scholarIns.applId
                    applL.applicationStatus=0
                    applL.save()
                    scholarIns.delete()
                    send_notification(recipient=applL.student.user,message='You can now reapply the scholarship. Dont repeat the mistakes',url='alumni/apply_alumni_scholarship')
                    scholarIns.save()
                else:
                    applL=scholarIns.applId
                    send_notification(recipient=applL.student.user,message='Your application rejected and deleted',url='dashboard')
                    scholarIns.delete()
                    applL.delete()
            except:
                print('error in getting variables in reApply')
        message={}
        message['title']='Reallow decision implemented'
        message['description']='You have updated your decision to us or no application in wait. Thank You'
        return render(request,'prompt_pages/signup_success.html',{'message':message})  
    lis=[]
    for i in rejectedAppl.objects.filter(staff=staffIns):
        lis.append(i.applId)
    return render(request,'alumni/reApply.html',{'list':lis})


#@permission_required('can_approve_student')
def fillConduct(request):
    student_inst=get_students_under_staff(staff.objects.get(user=request.user))
    print(student_inst)
    scholarAppLis=[]
    if request.method=='POST':
        for stud in student_inst:
            scholarshipIns = alumini_scholoarship_registration.objects.filter(student=stud).filter(applicationStatus=1).order_by('-date_of_application')
            if scholarshipIns.exists():
                scholarIns=scholarshipIns[0]
                print(scholarIns.conduct)
                try:
                    str1=int(request.POST['conduct'+str(scholarIns.pk)])
                    scholarIns.conduct=str1
                    scholarIns.applicationStatus=2
                    scholarIns.save()
                except:
                    print('error in getting variables in fillconduct')
        #print('|||'+str1+'|||')
        #appl=alumini_scholoarship_registration.objects.get(pk=int(str1))
    count=0
    for stud in student_inst:    
        scholarshipIns = alumini_scholoarship_registration.objects.filter(student=stud).filter(applicationStatus=1).order_by('-date_of_application')
        if scholarshipIns.exists():
            count+=1
            scholarAppLis.append(scholarshipIns[0])
    print('count',count)
    if count>0:
        return render(request,'alumni/fillConduct.html',{'context':scholarAppLis,'len':count})
    else:
        message={}
        message['title']='Filled Conduct successfully'
        message['description']='You have updated conduct data or no application for your wait. Thank You'
        return render(request,'prompt_pages/signup_success.html',{'message':message})
#@permission_required('can_approve_student')
def getDetails(request,application_id):
    
    alumniIns=alumini_scholoarship_registration.objects.get(pk=int(application_id))
    print(alumniIns.pk)
    return render(request,'alumni/getDetails.html',{'inst':alumniIns})



class appliacationFormPDF(PDFTemplateView):
    template_name = "alumni/alumni_scholarship_application_pdf.html"
    application_instance = None

    def get(self, request, *args, **kwargs):
        application_id = kwargs.pop('application_id')
        alumini_scholoarship_application_query = alumini_scholoarship_registration.objects.filter(
            pk=int(application_id))
        if alumini_scholoarship_application_query.exists():
            self.application_instance = alumini_scholoarship_application_query.get()
        else:
            return Http404
        return super(appliacationFormPDF, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        active_batches_instance = active_batches.objects.filter(
            department=self.application_instance.student.department
        ).get(
            batch=self.application_instance.student.batch
        )
        print('semester_number')
        print(active_batches_instance.current_semester_number_of_this_batch)

        current_semester = active_batches_instance.current_semester_number_of_this_batch
        dup_seml = []
        sem_nol = []
        sem_per_lis = ['', '', '', '', '', '', '', '']
        stud_inst = self.application_instance.student
        '''semes_list = attendance.objects.filter(
            Q(present_students=stud_inst) | Q(absent_students=stud_inst) | Q(onduty_students=stud_inst)).values_list(
            'semester', flat=True).distinct()
        for inst in semes_list:
            sem_inst = semester.objects.get(id=inst)
            sem_nol.append(sem_inst.semester_number)
        dup_seml = set([x for x in sem_nol if sem_nol.count(x) > 1])
        
        for inst in semes_list:
            sem_inst = semester.objects.get(id=inst)
            if stud_inst.batch.regulation.start_year == 2012:
                consolidated_attendance_2012_api_instance = ConsolidatedAttendance2012API()
                consolidated_attendance_2012_api_instance.semester_instance = sem_inst
                consolidated_attendance_2012_api_instance.start_date_instance = sem_inst.start_term_1
                if sem_inst.end_term_3:
                    consolidated_attendance_2012_api_instance.end_date_instance = sem_inst.end_term_3
                else:
                    consolidated_attendance_2012_api_instance.end_date_instance = timezone.now().date()
                result = consolidated_attendance_2012_api_instance.get_report_as_dictionary(stud_inst.user)[0]
                att_perc = float(result['percentage'])


            elif stud_inst.batch.regulation.start_year == 2016:
                subject_consolidated_attendance_api_instance = SubjectConsolidatedAttendanceAPI()
                subject_consolidated_attendance_api_instance.semester_instance = sem_inst
                subject_consolidated_attendance_api_instance.start_date_instance = sem_inst.start_term_1
                if sem_inst.end_term_3:
                    subject_consolidated_attendance_api_instance.end_date_instance = sem_inst.end_term_3
                else:
                    subject_consolidated_attendance_api_instance.end_date_instance = timezone.now().date()
                perc_sum = 0
                cnt = 0
                for course_inst in get_courses_studied_by_student(stud_inst, sem_inst, ):
                    subject_consolidated_attendance_api_instance.course_instance = course_inst
                    result = subject_consolidated_attendance_api_instance.get_report_as_dictionary(stud_inst.user)[0]
                    perc_sum += float(result['percentage'])
                    cnt += 1
                att_perc = perc_sum / cnt
            if sem_inst.semester_number in dup_seml:
                sem_per_lis[sem_inst.semester_number - 1] += str(att_perc) + ','
                #studHisIns=student_history.objects.get_or_create(student=stud_inst,semester=sem_inst)
                #studHisIns.attendancePerc=att_perc
                #studHisIns.save()
            else:
                sem_per_lis[sem_inst.semester_number - 1] = att_perc
                #studHisIns=student_history.objects.get(student=stud_inst,semester=sem_inst)
                #studHisIns.attendancePerc=att_perc
                #studHisIns.save()'''
        sem = \
        semester.objects.filter(semester_number=active_batches_instance.current_semester_number_of_this_batch).order_by(
            '-start_term_1')[0]
        '''curAttPerc=sem_per_lis[sem.semester_number-1]
        studHisInst=student_history.objects.get(student=stud_inst,semester=sem)
        if curAttPerc=='':
            curAttPerc=0.0
        #studHisInst.attendancePerc=curAttPerc
        studHisInst.save()
        try:
            if curAttPerc.find(',')!=-1:
                print(1/0)
        except:
            self.application_instance.weightage+=curAttPerc*1.5'''
        studHisIns = student_history.objects.get(semester=sem, student=stud_inst)
        SchDetLis = []
        
        SchDetLis.append(studHisIns.prev_st)
        SchDetLis.append(studHisIns.cur_st)
        SchDetLis.append(studHisIns.prev_cen)
        SchDetLis.append(studHisIns.cur_cen)
        SchDetLis.append(studHisIns.prev_alum)
        SchDetLis.append(studHisIns.cur_alum)

        if (current_semester % 2 == 0):
            year = int(current_semester / 2)
        else:
            year = int((current_semester + 1) / 2)
        context_data = {
            'application': self.application_instance,
            'current_semester': current_semester,
            'year': year,
            'att_perc': sem_per_lis,
            'SchDet': SchDetLis,
        }
        print(context_data)
        return super(appliacationFormPDF, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )



class applFormPdfAlum(PDFTemplateView):
    template_name = "alumni/alumni_scholarship_appl_alum_pdf.html"
    application_instance = None
    def get(self, request, *args, **kwargs):
        application_id = kwargs.pop('application_id')
        alumini_scholoarship_application_query = alumini_scholoarship_registration.objects.filter(
            pk=int(application_id))
        if alumini_scholoarship_application_query.exists():
            self.application_instance = alumini_scholoarship_application_query.get()
        else:
            return Http404
        return super(applFormPdfAlum, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        active_batches_instance = active_batches.objects.filter(
            department=self.application_instance.student.department
        ).get(
            batch=self.application_instance.student.batch
        )
        stud_inst=self.application_instance.student
        sem = \
        semester.objects.filter(semester_number=active_batches_instance.current_semester_number_of_this_batch).order_by(
            '-start_term_1')[0]
        print('semester_number')
        print(active_batches_instance.current_semester_number_of_this_batch)

        current_semester = active_batches_instance.current_semester_number_of_this_batch
        try:
            studHisIns = student_history.objects.get(semester=sem, student=stud_inst)
            SchDetLis = {}
        
            SchDetLis['prevst']=studHisIns.prev_st
            SchDetLis['curst']=studHisIns.cur_st
            SchDetLis['prevcen']=studHisIns.prev_cen
            SchDetLis['curcen']=studHisIns.cur_cen
            SchDetLis['preval']=studHisIns.prev_alum
            SchDetLis['cural']=studHisIns.cur_alum
        except:
            SchDetLis = {}
        
            SchDetLis['prevst']=-1
            SchDetLis['curst']=-1
            SchDetLis['prevcen']=-1
            SchDetLis['curcen']=-1
            SchDetLis['preval']=-1
            SchDetLis['cural']=-1
        weightIns=weightage.objects.get(name='CommonAlumniScholarship')
        try:
            coeIns=COEdetails.objects.get(student=stud_inst)
            COEmark=coeIns.CGPA
            COEnoOfar=coeIns.noOfArrears
            COEattPer=coeIns.attendancePerc
            COESch=coeIns.prevStateSch
        except:
            COEmark=0
            COEnoOfar=0
            COEattPer=0
            COESch=0
        if (current_semester % 2 == 0):
            year = int(current_semester / 2)
        else:
            year = int((current_semester + 1) / 2)
        context_data = {
            'application': self.application_instance,
            'current_semester': current_semester,
            'year': year,
            'SchDet': SchDetLis,
            'weightage':weightIns,
            'COEmark':COEmark,
            'COEnoOfar':COEnoOfar,
            'COEattPer':COEattPer,
            'COESch':COESch,
        }
        print(context_data)
        return super(applFormPdfAlum, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )

def ResultSchCSV(request,cat):
    response = HttpResponse(content_type='text/csv')
    lis=['Rank','Application Number','Roll no','weightage','Name','Dept','Year','Comm','Resident','Parents job & income','Remarks','Mobile No','E-mail ID','Temporary Address','Permanent Address','Amount(Rs)','Signature']
    cat=int(cat)
    date_str='01-10-2018'
    date_1=datetime.strptime(date_str,"%d-%m-%Y").date()
    if cat==0:
        response['Content-Disposition'] = 'attachment; filename="diffAbledScholarship.csv"'
        writer=csv.writer(response)
        writer.writerow(lis)
        count=1
        for i in alumini_scholoarship_registration.objects.filter(applicationStatus=3,date_of_application__gte=date_str).order_by('-weightage'):
            if i.diffAbled!='No':
                active_batches_instance = active_batches.objects.filter(
                    department=i.student.department
                    ).get(
                    batch=i.student.batch
                    )
                inc=i.student.father_annual_income+i.student.mother_annual_income
                jobAinc=i.student.father_occupation+' '+str(inc)
                writer.writerow([count,i.pk,i.student.roll_no,i.weightage,i.student,i.student.department,active_batches_instance.current_semester_number_of_this_batch//2 + 1,i.student.community,i.student.student_type,jobAinc,'Differently Abled',i.student.phone_number,i.student.user,i.student.temporary_address,i.student.permanent_address,'5000'])
            count+=1
        return response
    elif cat==1:
        response['Content-Disposition'] = 'attachment; filename="OneParentScholarship.csv"'
        writer=csv.writer(response)
        writer.writerow(lis)
        count=1
        for i in alumini_scholoarship_registration.objects.filter(applicationStatus=3, date_of_application__gte=date_1).order_by('-weightage'):
            if i.disParent=='1' and i.diffAbled=='No':
                active_batches_instance = active_batches.objects.filter(
                    department=i.student.department
                    ).get(
                    batch=i.student.batch
                    )

                inc=i.student.father_annual_income+i.student.mother_annual_income
                jobAinc=i.student.father_occupation+str(inc)
                writer.writerow([count,i.pk,i.student.roll_no,i.weightage,i.student,i.student.department,active_batches_instance.current_semester_number_of_this_batch//2 + 1,i.student.community,i.student.student_type,jobAinc,'One Parent',i.student.phone_number,i.student.user,i.student.temporary_address,i.student.permanent_address,'5000'])
            count+=1
        return response
    elif cat==2:
        response['Content-Disposition'] = 'attachment; filename="NoParentScholarship.csv"'
        writer=csv.writer(response)
        writer.writerow(lis)
        count=1
        for i in alumini_scholoarship_registration.objects.filter(applicationStatus=3,date_of_application__gte=date_1).order_by('-weightage'):
            if i.disParent=='2' and i.diffAbled=='No':
                active_batches_instance = active_batches.objects.filter(
                    department=i.student.department
                    ).get(
                    batch=i.student.batch
                    )

                inc=i.student.father_annual_income+i.student.mother_annual_income
                jobAinc=i.student.father_occupation+str(inc)
                writer.writerow([count,i.pk,i.student.roll_no,i.weightage,i.student,i.student.department,active_batches_instance.current_semester_number_of_this_batch//2 + 1,i.student.community,i.student.student_type,jobAinc,'No Parent',i.student.phone_number,i.student.user,i.student.temporary_address,i.student.permanent_address,'5000'])
            count+=1
        return response
    elif cat==3:
        response['Content-Disposition'] = 'attachment; filename="commonScholarship.csv"'
        writer=csv.writer(response)
        writer.writerow(lis)
        count=1
        for i in alumini_scholoarship_registration.objects.filter(applicationStatus=3,date_of_application__gte=date_1).order_by('-weightage'):
            if (i.disParent=='' or i.disParent==None or i.disParent=='None') and i.diffAbled=='No':
                active_batches_instance = active_batches.objects.filter(
                    department=i.student.department
                    ).get(
                    batch=i.student.batch
                    )

                inc=i.student.father_annual_income+i.student.mother_annual_income
                jobAinc=i.student.father_occupation+str(inc)
                writer.writerow([count,i.pk,i.student.roll_no,i.weightage,i.student,i.student.department,active_batches_instance.current_semester_number_of_this_batch//2 + 1,i.student.community,i.student.student_type,jobAinc,'-',i.student.phone_number,i.student.user,i.student.temporary_address,i.permanent_address,'5000'])
            count+=1 
        return response
        
    else:
        response['Content-Disposition'] = 'attachment; filename="allCatScholarship.csv"'
        writer=csv.writer(response)
        writer.writerow(lis)
        count=1
        for i in alumini_scholoarship_registration.objects.filter(applicationStatus=3,date_of_application__gte=date_1).order_by('-weightage'):
            active_batches_instance = active_batches.objects.filter(
                    department=i.student.department
                    ).get(
                    batch=i.student.batch
                    )

            inc=i.student.father_annual_income+i.student.mother_annual_income
            jobAinc=i.student.father_occupation+' '+str(inc)
            writer.writerow([count,i.pk,i.student.roll_no,i.weightage,i.student,i.student.department,active_batches_instance.current_semester_number_of_this_batch//2 + 1,i.student.community,i.student.student_type,jobAinc,'',i.student.phone_number,i.student.user,i.student.temporary_address,i.student.permanent_address,'5000'])
            count+=1 
        return response




class ResultSchPdf(PDFTemplateView):
    template_name = "alumni/ResultSchPdf.html"
    def get(self, request, *args, **kwargs):
        self.category = kwargs.pop('cat')
        return super(ResultSchPdf,self).get(request, *args, **kwargs)
    def get_context_data(self, **kwargs):
        count=1
        returnLis=[]
        self.category=int(self.category)
        if self.category==0:
            for i1 in alumini_scholoarship_registration.objects.filter(applicationStatus=3).order_by('-weightage'):
                if i1.diffAbled!='No':
                    active_batches_instance = active_batches.objects.filter(
                    department=i1.student.department
                    ).get(
                    batch=i1.student.batch
                    )
                    i={}
                    i['rank']=count
                    i['appl']=i1
                    i['year']=active_batches_instance.current_semester_number_of_this_batch//2 + 1
                    returnLis.append(i)
                count+=1
            context_data={'List':returnLis,
                'category':self.category}
            print(context_data)
            return super(ResultSchPdf, self).get_context_data(
                pagesize="A4",
                context=context_data,
                **kwargs
                )
        elif self.category==1:
            for i1 in alumini_scholoarship_registration.objects.filter(applicationStatus=3).order_by('-weightage'):
                if i1.disParent=='1' and i1.diffAbled=='No':
                    active_batches_instance = active_batches.objects.filter(
                    department=i1.student.department
                    ).get(
                    batch=i1.student.batch
                    )

                    i={}
                    i['rank']=count
                    i['appl']=i1
                    i['year']=active_batches_instance.current_semester_number_of_this_batch//2 + 1
                    returnLis.append(i)
                count+=1
            context_data={'List':returnLis,
                'category':self.category}
            print(context_data)
            return super(ResultSchPdf, self).get_context_data(
                pagesize="A4",
                context=context_data,
                **kwargs
                )
        elif self.category==2:
            for i1 in alumini_scholoarship_registration.objects.filter(applicationStatus=3).order_by('-weightage'):
                if i1.disParent=='2' and i1.diffAbled=='No':
                    active_batches_instance = active_batches.objects.filter(
                    department=i1.student.department
                    ).get(
                    batch=i1.student.batch
                    )
                    i={}
                    i['rank']=count
                    i['appl']=i1
                    i['year']=active_batches_instance.current_semester_number_of_this_batch//2 + 1
                    returnLis.append(i)
                count+=1
            context_data={'List':returnLis,
                'category':self.category}
            print(context_data)
            return super(ResultSchPdf, self).get_context_data(
                pagesize="A4",
                context=context_data,
                **kwargs
                )
        elif self.category==3:
            for i1 in alumini_scholoarship_registration.objects.filter(applicationStatus=3).order_by('-weightage'):
                if (i1.disParent=='' or i1.disParent==None or i1.disParent=='None') and i1.diffAbled=='No':
                    active_batches_instance = active_batches.objects.filter(
                    department=i1.student.department
                    ).get(
                    batch=i1.student.batch
                    )
                    i={}
                    i['rank']=count
                    i['appl']=i1
                    i['year']=active_batches_instance.current_semester_number_of_this_batch//2 + 1
                    returnLis.append(i)
                count+=1
            context_data={'List':returnLis,
                'category':self.category}
            print(context_data)
            return super(ResultSchPdf, self).get_context_data(
                pagesize="A4",
                context=context_data,
                **kwargs
                )
                
        else:
            for i1 in alumini_scholoarship_registration.objects.filter(applicationStatus=3).order_by('-weightage'):
                active_batches_instance = active_batches.objects.filter(
                    department=i1.student.department
                    ).get(
                    batch=i1.student.batch
                    )
                i={}
                i['rank']=count
                i['appl']=i1
                i['year']=active_batches_instance.current_semester_number_of_this_batch//2 + 1
                returnLis.append(i)
                count+=1
            context_data={'List':returnLis,
                'category':self.category}
            print(context_data)
            return super(ResultSchPdf, self).get_context_data(
                pagesize="A4",
                context=context_data,
                **kwargs
                )
 
@permission_required('alumni.can_process_alumini_scholarship')
def view_alumni_scholarships(request):
    modal = None
    toggle_modal = False
    application=[]
    alumni_application_instances = None
    if request.method == "POST":
        if request.POST.get('shortlist'):
            start_date = request.POST.get('s_date')
            end_date = request.POST.get('e_date')
            start_date_inst = datetime.strptime(start_date, "%d-%m-%Y").date()
            end_date_inst = datetime.strptime(end_date, "%d-%m-%Y").date()
            print(start_date_inst)
            print(end_date_inst)
        
            if start_date_inst > end_date_inst:
                modal = {
                'heading': 'Error',
                'body': 'Start date should be less than end date',
                }
                toggle_modal = True

            alumni_application_instances = alumini_scholoarship_registration.objects.filter(
                date_of_application__gte=start_date_inst
            ).filter(
                date_of_application__lte=end_date_inst,applicationStatus__gte=1
         
            )
            for inst in alumni_application_instances:
                application1={}
                application1['appl']=inst
                if inst.applicationStatus==1:
                    application1['status']="Staff's wait"
                elif inst.applicationStatus==2:
                    application1['status']="Can edit"
                elif inst.applicationStatus==3:
                    application1['status']="Already Edited"
                application.append(application1)
            # alumni_application_instances = alumini_scholoarship_registration.objects.all()

            print(alumni_application_instances)

        if request.POST.get('shortlist1'):
            messge='You have not submitted the followed documents:\n'
            lis=request.POST.getlist('documen')
            for doc in lis:
                messge+=doc+' , '
            messge+='\nSo submit the documents to alumni office'
            for i in request.POST.getlist('studDoc'):
                stud_in=student.objects.get(pk=int(i))
                data={}
                data['subject']='Incompletion in submitting documents'
                data['message']=messge
                data['email']=stud_in.user.email
                sendEmail(data)
                send_notification(recipient=stud_in.user,message=messge,url='dashboard')
            return HttpResponseRedirect('alumni/view_applications')
    documents_needed=['Income Certificate','Sports','Club','Patent','Project','Internship','Presentation','Differently Abled','Parents Deceased','Aadhar Xerox','Passbook','Requisition Letter']
    context_data = {
            'toggle_model': toggle_modal,
            'modal': modal,
            'alumni_application_instances': application,
            'documents':documents_needed,
            }
    return render(
            request,
            'alumni/view_applications.html',
            context_data
            )

@permission_required('alumni.can_process_alumini_scholarship')
def editApplication(request,application_id): 
    if request.method=='POST':
        temp_instance =alumini_scholoarship_registration.objects.get(pk=application_id)
        active_batches_instance = active_batches.objects.filter(
            department=temp_instance.student.department
        ).get(
            batch=temp_instance.student.batch
        )
        temp_instance =alumini_scholoarship_registration.objects.get(pk=application_id)
        temp_instance.diffAbled = request.POST['phyabdes']
        temp_instance.disParent = request.POST['disdes']
        levelsp = request.POST['levelsp']
        if levelsp != 'None':
            temp_instance.sport = levelsp
        temp_instance.project = request.POST['placepr']
        temp_instance.patent = request.POST['stagepat']
        temp_instance.ppt = request.POST['placep']
        temp_instance.internship = request.POST['internships']
        temp_instance.cgpa = request.POST['cgpa']
        temp_instance.noOfArrears = int(request.POST['noOfArrears'])
        temp_instance.role = request.POST['role']
        temp_instance.conduct=request.POST['conduct']
        temp_instance.financial=request.POST['finScore']
         
        '''
        if temp_instance.student.batch.regulation.start_year == 2012:
            consolidated_attendance_2012_api_instance = ConsolidatedAttendance2012API()
            consolidated_attendance_2012_api_instance.semester_instance = sem
            consolidated_attendance_2012_api_instance.start_date_instance = sem.start_term_1
            if sem.end_term_3:
                consolidated_attendance_2012_api_instance.end_date_instance = sem.end_term_3
            else:
                consolidated_attendance_2012_api_instance.end_date_instance = timezone.now().date()
            result = consolidated_attendance_2012_api_instance.get_report_as_dictionary(temp_instance.student.user)[0]
            att_perc = float(result['percentage'])


        elif temp_instance.student.batch.regulation.start_year == 2016:
            subject_consolidated_attendance_api_instance = SubjectConsolidatedAttendanceAPI()
            subject_consolidated_attendance_api_instance.semester_instance = sem
            subject_consolidated_attendance_api_instance.start_date_instance = sem.start_term_1
            if sem.end_term_3:
                subject_consolidated_attendance_api_instance.end_date_instance = sem.end_term_3
            else:
                subject_consolidated_attendance_api_instance.end_date_instance = timezone.now().date()
            perc_sum = 0
            cnt = 0
            print(get_courses_studied_by_student(temp_instance.student, sem, ),temp_instance.student,sem)
            for course_inst in get_courses_studied_by_student(temp_instance.student, sem, ):
                subject_consolidated_attendance_api_instance.course_instance = course_inst
                result = subject_consolidated_attendance_api_instance.get_report_as_dictionary(temp_instance.user)[0]
                perc_sum += float(result['percentage'])
                cnt += 1
            print(perc_sum)
            if cnt!=0:
                att_perc = perc_sum / cnt
            else:
                att_perc=0
              
        temp_instance.weightage+=att_perc * 1.5'''
        temp_instance.applicationStatus=3
        temp_instance.save()
        message={}
        message['title']='Updated'
        message['description']='You have successfully updated data. Thank You'
        return render(request,'prompt_pages/signup_success.html',{'message':message})
        #return redirect('view_alumni_scholarships')


    alumniIns = alumini_scholoarship_registration.objects.filter(pk=int(application_id),applicationStatus__gte=1)
    if alumniIns.exists():
        applIns=alumniIns.get()
        levels = ['University', 'State', 'District', 'National', 'zonal', 'International']
        places = ['Participation', 'Consolation Prize', '2ndPrize', '1stPrize']
        stages = ['Submitted', 'stage1', 'approved', 'granted']
        clubs = ['Tamil Mandram', 'LDS', 'SJC', 'NCC', 'NSS', 'Eliminators', 'Orchestra','FineArts','Green Club','TedX']
        other = ['(Member)', '(Active Participant)', '(Leadership role)', '(Awards)']
        if applIns.financial!=None:
            return render(request,'alumni/editApplication.html',{'applIn':applIns,'levels':levels,'places':places,'stages':stages,'clubs':clubs,'other':other,'finFilled':True})
        return render(request,'alumni/editApplication.html',{'applIn':applIns,'levels':levels,'places':places,'stages':stages,'clubs':clubs,'other':other,'finFilled':False})
    elif alumini_scholoarship_registration.objects.filter(pk=int(application_id),applicationStatus=1).exists():
        message={}
        message['title']="Faculty's delay"
        message['description']='Faculty should fill the conduct marks to proceed'
        return render(request,'prompt_pages/signup_success.html',{'message':message})
    else:
        print(alumini_scholoarship_registration.objects.filter(pk=int(application_id))[0].applicationStatus)
        message={}
        message['title']="Unknown problem"
        message['description']='Contact developers about the problem in Report bug'
        return render(request,'prompt_pages/signup_success.html',{'message':message})



@permission_required('alumni.can_process_alumini_scholarship')
def calcWeightage(request):
    if request.method=='POST':
        if 'ignoreErrors' in request.POST:
            for i in alumini_scholoarship_registration.objects.filter(applicationStatus__gte=1):
                try:
                    var=request.POST['exCGP'+str(i.pk)]
                    i.cgpa=COEdetails.objects.get(student=i.student).CGPA
                    i.save()
                except:
                    print('N')
                try:
                    var=request.POST['exnOAr'+str(i.pk)]
                    i.noOfArrears=COEdetails.objects.get(student=i.student).noOfArrears
                    i.save()
                except:
                    print('N')
                try:
                    var=request.POST['exSch'+str(i.pk)]
                    active_batches_instance = active_batches.objects.filter(
                    department=i.student.department
                    ).get(
                    batch=i.student.batch
                    )
                    sem = \
                    semester.objects.filter(semester_number=active_batches_instance.current_semester_number_of_this_batch).order_by(
                    '-start_term_1')[0]
                    stuHi=student_history.objects.get(student=i.student,semester=sem)
                    stuHi.prev_st=COEdetails.objects.get(student=i.student).prevStateSch
                    stuHi.save()
                except:
                    continue
        else:
            try:
                warning=request.POST['warning']
                print('warning'+warning)
                sortedSch=alumini_scholoarship_registration.objects.filter(applicationStatus__gte=1)
                fields='Wrongly entered fields:'
                print(fields)
                for i in sortedSch:
                    isTrue=False
                    active_batches_instance = active_batches.objects.filter(
                    department=i.student.department
                    ).get(
                    batch=i.student.batch
                    )
                    sem = \
                    semester.objects.filter(semester_number=active_batches_instance.current_semester_number_of_this_batch).order_by(
                    '-start_term_1')[0]
                    COEins=COEdetails.objects.get(student=i.student)
                    print(COEins)
                    if i.cgpa!=COEins.CGPA and COEins.CGPA!=None:
                        isTrue=True
                        fields+='\nCGPA:  Entered: '+str(i.cgpa)+' Real CGPA:'+str(COEins.CGPA)
                        print(fields)
                    try:
                        studHis=student_history.objects.get(student=i.student,semester=sem)
                        if COEins.prevStateSch!=-1 and studHis.prev_st!=COEins.prevStateSch and COEins.prevStateSch!=None:
                            isTrue=True
                            fields+='\nPrevious yr State Govt Scholarship:  Entered: '+str(studHis.prev_st)+' Real amount:'+str(COEins.prevStateSch)
                    except:
                        print('Failure in getting history variables')
                    if i.noOfArrears!=COEins.noOfArrears and COEins.noOfArrears!=None:
                        isTrue=True
                        fields+='\nNo of Arrears: Entered: '+str(i.noOfArrears)+' Real count:'+str(COEins.noOfArrears)
                    if isTrue==True:
                        message1=warning+fields
                        data={}
                        data['subject']='Mistake in entering Alumni Scholarship details'
                        data['message']=message1+'Please visit alumni office to clarify it.'
                        data['email']=i.student.user.email
                        sendEmail(data)
                        send_notification(recipient=i.student.user,message=message1,url='dashboard')
                return HttpResponseRedirect('view_applications')
            except:
                print('No warning')
    else:                 
        ins=weightage.objects.get(name='CommonAlumniScholarship')
        parent_diseased={'':0,'1':ins.disSc,'2':ins.disSc*2,None:0,'None':0}
        Project={'':0,None:0,'None':0,'Participation':ins.projectPar,'Consolation Prize':ins.projectCon,'2ndPrize':ins.projectSec,'1stPrize':ins.projectFir}
        Patent={'':0,None:0,'None':0,'Submitted':ins.patentSub,'stage1':ins.patentSta,'approved':ins.patentApp,'granted':ins.patentGra}
        Ppt={'':0,None:0,'None':0,'Participation':ins.pptPar,'Consolation Prize':ins.pptCon,'2ndPrize':ins.pptSec,'1stPrize':ins.pptFir}
        Sports={'':0,None:0,'None':0,'University':ins.sportUniv,'zonal':ins.sportZonal,'District':ins.sportDis,'State':ins.sportSta,'National':ins.sportNati,'International':ins.sportInter}
        Club={'':0,None:0,'None':0,'(Member)':ins.clubMem, '(Active Participant)':ins.clubAct, '(Leadership role)':ins.clubLead, '(Awards)':ins.clubAward}
        
        
        for inst in alumini_scholoarship_registration.objects.filter(applicationStatus=3):
            active_batches_instance = active_batches.objects.filter(
                department=inst.student.department
            ).get(
                batch=inst.student.batch
            )
            weigh=0
            weigh=(Project[inst.project]*ins.project) + (Patent[inst.patent]*ins.patent) + (Ppt[inst.ppt]*ins.ppt) + (parent_diseased[inst.disParent]*ins.parentdis) + (Sports[inst.sport]*ins.sports) + (inst.internship*ins.internship*ins.internSc)
            try:
                weigh+=Club[inst.role]*ins.club
            except:
                print('no')
            if inst.applicationStatus==3:
                weigh+=inst.financial*ins.finCondn*ins.finanSc
            else:
                weigh+=5*ins.finCondn*ins.finanSc 
            if inst.diffAbled!='No':
                weigh+=ins.diffAbl*ins.diffSc
            sem = \
            semester.objects.filter(semester_number=active_batches_instance.current_semester_number_of_this_batch).order_by(
                '-start_term_1')[0]
            COEpresent=False
            try:
                COEins=COEdetails.objects.get(student=inst.student)
                if COEins.CGPA!=None:
                    mar=COEins.noOfArrears*ins.noOfArrSc
                    if mar>10:
                        mar=10
                    weigh+=(float(COEins.CGPA)*ins.marksSc*ins.marks)+(mar*ins.noOfArrears)
                else:
                    mar=inst.noOfArrears*ins.noOfArrSc
                    if mar>10:
                        mar=10
                    weigh+=(float(inst.cgpa)*ins.marksSc*ins.marks)+(marins.noOfArrears)
                if COEins.attendancePerc!=None:
                    weigh+=float(COEins.attendancePerc)*ins.attendance*ins.attScore
                else:
                    weigh+=90*ins.attendance*ins.attScore
                COEpresent=True
            except:
                mar=inst.noOfArrears*ins.noOfArrSc
                if mar>10:
                    mar=10
                weigh+=(float(inst.cgpa)*ins.marksSc*ins.marks)+(mar*ins.noOfArrears)+(90*ins.attendance*ins.attScore)
                print('Error in COE access')
            try:
                stuHis=student_history.objects.get(student=inst.student,semester=sem)
                if COEpresent==True:
                    if stuHis.prev_st!=-1:
                        if stuHis.prev_st>COEins.prevStateSch:
                            weigh+=stuHis.prev_st*ins.otherAss*ins.othAssSc
                        elif COEins.prevStateSch!=None:
                            weigh+=COEins.prevStateSch*ins.otherAss*ins.othAssSc
                    elif COEins.prevStateSch!=None:
                        weigh+=COEins.prevStateSch*ins.otherAss*ins.othAssSc
                elif stuHis.prev_st!=-1:
                    weigh+=stuHis.prev_st*ins.otherAss*ins.othAssSc
                if stuHis.prev_cen!=-1:
                    weigh+=stuHis.prev_cen*ins.otherAss*ins.othAssSc
                if stuHis.prev_alum>1000:
                    weigh+=ins.gctAAass*ins.prYearAssSc
            except:
                print('Error in Student History')
                if COEpresent==True and COEins.prevStateSch!=None:
                    weigh+=COEins.prevStateSch*ins.otherAss*ins.othAssSc
                else:
                    weigh+=10000*ins.otherAss*ins.othAssSc
            
            inst.weightage=Decimal(str(weigh))
            inst.save()
    sortedSch=alumini_scholoarship_registration.objects.filter(applicationStatus=3).order_by('-weightage')
    count=1
    diffAbl=[]
    oneParent=[]
    bothPar=[]
    common=[]
    exceptionCGPA=[]
    exceptionArr=[]
    exceptionSchH=[]
    exceptionSchL=[]
    print(sortedSch)
    for i1 in sortedSch:
        active_batches_instance = active_batches.objects.filter(
            department=i1.student.department
        ).get(
            batch=i1.student.batch
        )
        sem = \
        semester.objects.filter(semester_number=active_batches_instance.current_semester_number_of_this_batch).order_by(
            '-start_term_1')[0]
        try:
            COEins=COEdetails.objects.get(student=i1.student)
            if i1.cgpa!=COEins.CGPA and COEins.CGPA!=None:
                i={}
                i['appl']=i1
                i['cgpa']=COEins.CGPA
                exceptionCGPA.append(i)
            if i1.noOfArrears!=COEins.noOfArrears and COEins.noOfArrears!=None:
                i={}
                i['appl']=i1
                i['Arr']=COEins.noOfArrears
                exceptionArr.append(i)
        except:
            print('Error')
        try:
            studHis=student_history.objects.get(student=i1.student,semester=sem)
            if COEins.prevStateSch!=None:
                if studHis.prev_st>COEins.prevStateSch:
                    i={}
                    i['appl']=i1
                    i['stuSch']=studHis.prev_st
                    i['COESch']=COEins.prevStateSch
                    exceptionSchH.append(i)
                elif studHis.prev_st<COEins.prevStateSch:
                    i={}
                    i['appl']=i1
                    i['stuSch']=studHis.prev_st
                    i['COESch']=COEins.prevStateSch
                    exceptionSchL.append(i) 
        except:
            print('Error')
        
        i={}
        i['rank']=count
        i['appl']=i1
        if i1.diffAbled!='No':
            diffAbl.append(i)
        elif i1.disParent=='1':
            oneParent.append(i)
        elif i1.disParent=='2':
            bothPar.append(i)
        else:
            common.append(i)
        count+=1
    print('Count',count)
    difPres=onePres=bothPres=commPres=exArr=exSchH=exCGPA=exSchL=False
    if len(diffAbl)>0:
        difPres=True
    if len(oneParent)>0:
        onePres=True
    if len(bothPar)>0:
        bothPres=True
    if len(common)>0:
        commPres=True
    if len(exceptionCGPA)>0:
        exCGPA=True
    if len(exceptionSchL)>0:
        exSchL=True
    if len(exceptionSchH)>0:
        exSchH=True
    if len(exceptionArr)>0:
        exArr=True
    #print(exceptionCGPA,'||||',exceptionSchH,'||||',exceptionSchL,'||||',exceptionArr)
    return render(request,'alumni/view_applications.html',{'diffAbl':diffAbl,'oneParent':oneParent,'bothParent':bothPar,'common':common,'difPres':difPres,'onePres':onePres, 'bothPres':bothPres,'commPres':commPres , 'exArr':exArr ,'exSchH':exSchH,'exSchL':exSchL,'exCGPA':exCGPA,'exceptionCGPA':exceptionCGPA ,'exceptionSchL':exceptionSchL ,'exceptionSchH':exceptionSchH ,'exceptionArr':exceptionArr})


@permission_required('alumni.can_process_alumini_scholarship')
def editWeightage(request):
    weightageIns=weightage.objects.get(name='CommonAlumniScholarship')
    if request.method=='POST':
        try:
            weightageIns.attendance=request.POST['attendance']
            weightageIns.marks=request.POST['marks']
            weightageIns.conduct=request.POST['conduct']
            weightageIns.gctAAass=request.POST['gctAAass']
            weightageIns.finCondn=request.POST['finCondn']
            weightageIns.diffAbl=request.POST['diffAbl']
            weightageIns.parentdis=request.POST['parentdis']
            weightageIns.noOfArrears=request.POST['noOfArrears']
            weightageIns.otherAss=request.POST['otherAss']
            weightageIns.project=request.POST['project']
            weightageIns.patent=request.POST['patent']
            weightageIns.ppt=request.POST['ppt']
            weightageIns.internship=request.POST['internship']
            weightageIns.sports=request.POST['sports']
            weightageIns.club=request.POST['club']
            weightageIns.attScore=request.POST['attScore']
            weightageIns.marksSc=request.POST['marksSc']
            weightageIns.conductSc=request.POST['conductSc']
            weightageIns.prYearAssSc=request.POST['prYearAssSc']
            weightageIns.finanSc=request.POST['finanSc']
            weightageIns.diffSc=request.POST['diffSc']
            weightageIns.disSc=request.POST['disSc']
            weightageIns.noOfArrSc=request.POST['noOfArrSc']
            weightageIns.othAssSc=request.POST['othAssSc']
            weightageIns.projectPar=request.POST['projectPar']
            weightageIns.projectCon=request.POST['projectCon']
            weightageIns.projectSec=request.POST['projectSec']
            weightageIns.projectFir=request.POST['projectFir']
            weightageIns.patentSub=request.POST['patentSub']
            weightageIns.patentSta=request.POST['patentSta']
            weightageIns.patentApp=request.POST['patentApp']
            weightageIns.patentGra=request.POST['patentGra']
            weightageIns.pptPar=request.POST['pptPar']
            weightageIns.pptCon=request.POST['pptCon']
            weightageIns.pptSec=request.POST['pptSec']
            weightageIns.pptFir=request.POST['pptFir']
            weightageIns.internSc=request.POST['internSc']
            weightageIns.sportUniv=request.POST['sportUniv']
            weightageIns.sportZonal=request.POST['sportZonal']
            weightageIns.sportDis=request.POST['sportDis']
            weightageIns.sportSta=request.POST['sportSta']
            weightageIns.sportNati=request.POST['sportNati']
            weightageIns.sportInter=request.POST['sportInter']
            weightageIns.clubMem=request.POST['clubMem']
            weightageIns.clubAct=request.POST['clubAct']
            weightageIns.clubLead=request.POST['clubLead']
            weightageIns.clubAward=request.POST['clubAward'] 
            weightageIns.save()
        except:
            print('Error in getting variables')
        return HttpResponseRedirect('/alumni/view_applications')
    
    weightageIns=weightage.objects.get(name='CommonAlumniScholarship')
    return render(request,'alumni/editWeigh.html',{'weigh':weightageIns})

def semesters_studied(request):
    student_instance = request.student
    semes_list = attendance.objects.filter(
        Q(present_students=student_instance) | Q(absent_students=student_instance) | Q(
            onduty_students=student_instance)).values_list('semester', flat=True).distinct()
    semester_lis = []
    sem_nol = []
    for inst in semes_list:
        sem_inst = semester.objects.get(id=inst)
        sem_nol.append(sem_inst.semester_number)
        semester_lis.append(sem_inst)
    dup_seml = set([x for x in sem_nol if sem_nol.count(x) > 1])
    for duplicat_num in dup_seml:
        dup_sem_cur = []

        def get_start_time(elem):
            return elem.start_term_1

        for inst in semes_list:
            sem_inst = semester.objects.get(id=inst)
            if sem_inst.semester_number == duplicat_num:
                dup_sem_cur.append(sem_inst)
        dup_sem_cur.sort(key=get_start_time, reverse=True)
        for i in range(1, len(dup_sem_cur)):
            for j in GradePoints.objects.filter(semester=i):
                j.delete()
            semester_lis.remove(i)

    def get_sem_num(elem):
        return elem.semester_number

    semester_lis.sort(key=get_sem_num)
    return semester_lis


def upload_result_file(request):
    return Http404
'''
# @permission_required('alumni.can_apply_alumini_scholarship')
def upload_result_file(request):
    departmentList = getCoreDepartments()
    batchList = batch.objects.all()
    semesterList = semester.objects.all()
    if request.method == 'POST':
        if request.is_ajax():
            if request.POST.get('action') == 'getSemesters':
                departmentPk = int(request.POST.get('departmentPk'))
                departmentInstance = getDepartmentOfPk(departmentPk)
                batchPk=int(request.POST.get('batchPk'))
                batchInstance=batch.objects.get(pk=batchPk)
                semesterList = []
                for eachSemester in semester.objects.filter(department=departmentInstance,batch=batchInstance):
                    temp = {}
                    temp['displayText'] = eachSemester.getDisplayTextForPastReport()
                    temp['pk'] = eachSemester.pk
                    semesterList.append(temp)
                print(semesterList)
                dictionary = {}
                dictionary['semesterList'] = semesterList
                return JsonResponse(dictionary)

        if 'xlsheet' in request.FILES:
            SemesterPk = request.POST.get('semester')
            Xlsheet=request.FILES['xlsheet']
            Pdata=pd.read_excel(Xlsheet)
            semesterInstance = semester.objects.get(pk=SemesterPk)
            Qual=student.objects.get(roll_no=Pdata['register'][0]).qualification
            colslis=[]
            for ins in Pdata.columns:
                if ins.find('gp')!=-1:
                    colslis.append(ins)
            colslis.pop()
            reqData=Pdata[colslis]
            regData=Pdata['register']
            count=-1
            for i in regData:
                count+=1
                stud_ins=student.objects.get(roll_no=i)
                #for j in colslis:
                for j in colslis:
                    datum=reqData[j][count]
                    if datum!='--' and str(datum)!=str('nan'):
                        try:
                            gdIns=GradePoints.objects.get(semester=semesterInstance,course=courses.objects.get(course_id=j[2:].upper(),programme=Qual),student=stud_ins)
                            gdIns.GrdPtReal=datum
                            gdIns.save()
                        except:
                            GradePoints.objects.create(semester=semesterInstance,course=courses.objects.get(course_id=j[2:].upper(),programme=Qual),student=stud_ins,GrdPtReal=datum)
            count1=1
            if semesterInstance.end_term_2.month > 6:
                name='fil'+str(semesterInstance.end_term_2.year)+'Dec,Jan'
            else:
                name='fil'+str(semesterInstance.end_term_2.year)+'May,June'
            for i in FileUpload.objects.filter(title__icontains=name):
                count1+=1
            title=name+'_'+str(count1)+'.xlsx'
            Xlsheet.name=title
            FileUpload.objects.create(title=title,fil=Xlsheet)
            #FlObj=pd.read_excel(Xlsheet)
            #print(FlObj.shape)
            message={}
            message['title']='Uploaded'
            message['description']='You have successfully uploaded file. Thank You'
            return render(request,'prompt_pages/signup_success.html',{'message':message})
    return render(request,'alumni/markSheetUpload.html',{'departmentList':departmentList,'batchList':batchList,'semesterList':semesterList})
'''
