from accounts.models import student,active_batches
from curriculum.models import semester,courses,attendance
from alumni.models import *
from django.db.models import Q
from django.shortcuts import render
from django.contrib.auth.decorators import login_required,permission_required


def semesters_studied(request):
    student_instance=student.objects.get(user=request.user)
    semes_list=attendance.objects.filter(Q(present_students=student_instance)|Q(absent_students=student_instance)|Q           (onduty_students=student_instance)).values_list('semester',flat=True).distinct()
    semester_lis=[]
    sem_nol=[]
    for inst in semes_list:
        sem_inst=semester.objects.get(id=inst)
        sem_nol.append(sem_inst.semester_number)
        semester_lis.append(sem_inst)
    dup_seml=set([x for x in sem_nol if sem_nol.count(x)>1])
    for duplicat_num in dup_seml:
        dup_sem_cur=[]
        def get_start_time(elem):
            return elem.start_term_1
        for inst in semes_list:
            sem_inst=semester.objects.get(id=inst)
            if sem_inst.semester_number==duplicat_num:
                dup_sem_cur.append(sem_inst)
        dup_sem_cur.sort(key=get_start_time,reverse=True)
        for i in range(1,len(dup_sem_cur)):
            for j in GradePoints.objects.filter(semester=i):
                j.delete()
            semester_lis.remove(i)
    def get_sem_num(elem):
        return elem.semester_number
    semester_lis.sort(key=get_sem_num)
    return semester_lis

@login_required
@permission_required('curriculum.can_choose_courses')
def fill_sem_marks(request):
    student_instance=student.objects.get(user=request.user)
    exis_list=[]
    sem_inst=-1
    active_batches_instance = active_batches.objects.filter(
                    department=student_instance.department
                ).get(
                    batch=student_instance.batch
                )
    if request.method=='POST':
        if 'semester' in request.POST:
            sem_inst=request.POST['semester']
            for each in GradePoints.objects.filter(semester=each):
                gp=request.POST[each.course_id]
                gpInst=GradePoint.objects.get(student=student_instance,semester=sem_inst,course=each.course)
                gpInst.GradePt=gp
                if gp==0:
                    exis_list.append(each.course)               #if any arrear occurs course is noted
                gpInst.save()
    semester_lis=semesters_studied(request)
    if len(exis_list)>0:                                          #check if any arrear and append that course to next sem courses
        for i in semester_lis:
            if i.semester_number==sem_inst.semester_number+1:
                for each in exis_list:
                    Gradepoints.objects.create(student=student_instance,semester=sem_inst,course=each)
                course_list=attendance.objects.filter(Q(present_students=student_instance)|Q(absent_students=student_instance)|Q           (onduty_students=student_instance)|Q(semester=i)).values_list('course',flat=True).distinct()
                for each in course_list:
                    GradePoints.objects.get_or_create(student=student_instance,semester=i,course=courses.objects.get(pk=each))
                if i.semester_number!=active_batches_instance.current_semester_number_of_this_batch:      #semester is less than current sem
                    return render(request,'alumni/GPAfilling.html',{'course':course_list+exis_list,'semester':i})
                else:                             #sem is same as current, so next sem only result will come
                    message={}
                    message['title']="success in filling marks"
                    message['description']="End Semester marks filled successfully"
                    return render(request,{'message':message},'prompt_pages/signup_success.html')
    for each in semester_lis:
        try:
            student_arr=GradePoints.objects.filter(student=student_instance,semester=each)      #executed during first call of the function
            print('NOOOOOOOOOOOOOOo')
            if student_arr[0].GradePt==-1:                      #if any not filled sem marks
                student_arr1=GradePoints.objects.filter(student=student_instance,semester=each).course
                #student_arr1+=exis_list
                if each.semester_number!=active_batches_instance.current_semester_number_of_this_batch:
                    return render(request,'alumni/GPAfilling.html',{'course':student_arr1,'semester':each})
            elif not student_arr.exists():                     # Occurs for a new scholarship applied student where many semesters were not noticed         
                print('hiiiiii')
        except:
            print('Errorenous') 
            if each.semester_number!=active_batches_instance.current_semester_number_of_this_batch:
                cour_lis=[]
                course_list=attendance.objects.filter(Q(present_students=student_instance)|Q(absent_students=student_instance)|Q           (onduty_students=student_instance)|Q(semester=each)).values_list('course',flat=True).distinct()
                print(course_list,'You are in right way')
                for eachh in course_list:
                    cour_lis.append(courses.objects.get(pk=eachh))
                cour_lis+=exis_list
                for each1 in cour_lis:
                    GradePoints.objects.create(student=student_instance,semester=each,course=each1)
                return render(request,'alumni/GPAfilling.html',{'course':cour_lis,'semester':each})
            else:                         #For next sem, the list of GPA format is created
                cour_lis=[]
                course_list=attendance.objects.filter(Q(present_students=student_instance)|Q(absent_students=student_instance)|Q           (onduty_students=student_instance)|Q(semester=each)).values_list('course',flat=True).distinct()
                print(course_list+'You are here')
                for eachh in course_list:
                    cour_lis.append(courses.objects.get(pk=eachh))
                cour_lis+=exis_list
                for each1 in cour_lis:
                    GradePoints.objects.create(student=student_instance,semester=each,course=each1) 
    message={}
    message['title']='No result to fill'
    message['description']='You have no result to fill. Thank You'
    return render(request,'prompt_pages/signup_success.html',{'message':message})
