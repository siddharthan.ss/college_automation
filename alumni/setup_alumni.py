import xlrd
import pandas as pd
from accounts.models import student
from alumni.models import COEdetails
lis=['BIOTECH-2015BATCH.xls','BIOTECH-2016BATCH.xls','BIOTECH-2017BATCH.xls','CIVIL-2015BATCH.xls','CIVIL-2016BATCH.xls','CIVIL-2017BATCH.xls','CSE-2015BATCH.xls','CSE-2016BATCH.xls','CSE-2017BATCH.xls','ECE-2015BATCH.xls','ECE-2016BATCH.xls','ECE-2017BATCH.xls','EEE-2015BATCH.xls','EEE-2016BATCH.xls','EEE-2017BATCH.xls','EIE-2015BATCH.xls','EIE-2016BATCH.xls','EIE-2017BATCH.xls','IT-2015BATCH.xls','IT-2016BATCH.xls','IT-2017BATCH.xls','MECHANICAL-2015BATCH.xls','MECHANICAL-2016BATCH.xls','MECHANICAL-2017BATCH.xls','PRODUCTION-2015BATCH.xls','PRODUCTION-2016BATCH.xls','PRODUCTION-2017BATCH.xls']
for i in lis:
    count=0
    df=pd.read_excel('./alumni/STUDENT-DETAILS/'+i)
    col=df.columns
    for j in df['register']:
        try:
            stud_inst=student.objects.get(roll_no=j)
            print(j)
        except:
            count+=1
            continue
        try:
            coeInst=COEdetails.objects.get(student=stud_inst)
            coeInst.CGPA=df['cgpa'][count]
        except:
            print('NEEEEEEEEEEEEEEEEEEEEw')
            coeInst=COEdetails.objects.create(student=stud_inst,CGPA=df['cgpa'][count])
        print(j,df['cgpa'][count])
        arrCount=0
        for k in col:
            if df[k][count]=='RA':
                arrCount+=1
        print(arrCount)
        count+=1
        coeInst.noOfArrears=arrCount
        coeInst.save()


lis=['BIOTECH-2016BATCH.xlsx','BIOTECH-2017BATCH.xlsx','CIVIL-2016-ASECTION.xlsx','CIVIL-2016-BSECTION.xlsx','CIVIL-2017-ASECTION.xlsx','CIVIL-2017-BSECTION.xlsx','CSE-2016BATCH.xlsx','CSE-2017BATCH.xlsx','ECE-2016BATCH.xlsx','ECE-2017BATCH.xlsx','EEE-2016BATCH.xls','EEE-2017BATCH.xls','EIE-2016BATCH.xlsx','EIE-2017BATCH.xlsx','IT-2017BATCH.xlsx','MECHANICAL-2016-ASECTION.xlsx','MECHANICAL-2016-BSECTION.xlsx','MECHANICAL-2017-ASECTION.xls','MECHANICAL-2017-BSECTION.xls','PRODUCTION-2016BATCH.xlsx','PRODUCTION-2017BATCH.xlsx']
for i in lis:
    df=pd.read_excel('./alumni/STUDENTS-ATTENDANCE/'+i)
    col=df.columns[2:]
    for j in df['register']:
        if str(j)=='nan' or str(j).find('r')!=-1 or str(j).find('R')!=-1:
            continue
        count=0
        try:
            stud_inst=student.objects.get(roll_no=j)
            print(j)
        except:
            continue
        cnt=0
        att=0
        df1=df[df['register']==j]
        print(df1)
        for index,jj in df1.iterrows():
            print(index)
            for k in col:
                if str(df1[k][index]).find('--')==-1 and str(df1[k][index])!='nan':
                    cnt+=1
                    att+=int(df[k][index])
            count+=1
        print(att/cnt)
        try:
            coeIns=COEdetails.objects.get(student=stud_inst)
            coeIns.attendancePerc=float(att/cnt)
            coeIns.save()
        except:
            print('Exceptedddddddddddddddddddd')
            coeIns=COEdetails.objects.create(student=stud_inst,attendancePerc=(att/cnt))
            coeIns.save()
