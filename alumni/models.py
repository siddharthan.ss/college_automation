from django.db import models

from accounts.models import student,staff
from curriculum.models import semester, courses
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.


class alumini_scholoarship_registration(models.Model):
    student = models.ForeignKey(student)
    brother_sister_details = models.TextField()
    bank_name = models.CharField(max_length=160)
    account_number = models.CharField(max_length=20)
    date_of_application = models.DateTimeField()
    project = models.CharField(max_length=160, blank=True, null=True)
    diffAbled = models.CharField(max_length=160, blank=True, null=True)
    disParent = models.CharField(max_length=160, blank=True, null=True)
    patent = models.CharField(max_length=160, blank=True, null=True)
    ppt = models.CharField(max_length=160, blank=True, null=True)
    internship = models.IntegerField(default=0)
    internshipDet = models.CharField(max_length=160, blank=True, null=True)
    sport = models.CharField(max_length=160, blank=True, null=True)
    spPlace = models.CharField(max_length=160, blank=True, null=True)
    cgpa = models.DecimalField(default=0, decimal_places=2, max_digits=5)
    clubActivities = models.CharField(max_length=300, blank=True, null=True)
    primClub = models.CharField(max_length=160, blank=True, null=True)
    role = models.CharField(max_length=160, blank=True, null = True)
    noOfArrears = models.IntegerField(default=0)
    applicationStatus = models.IntegerField(blank=True,null=True)#-1 disabled by staff 0 inactive 1 ACTIVE but not approved 2 approved by faculty 3 Approved by alumni and amount confirmed
    conduct = models.IntegerField(blank=True,null=True)
    weightage = models.DecimalField(max_digits=6,decimal_places=2,blank=True, null=True)
    financial = models.IntegerField(blank=True,null=True)

class COEdetails(models.Model):
    student=models.ForeignKey(student)
    CGPA=models.DecimalField(blank=True,null=True,decimal_places=2, max_digits=5)
    attendancePerc=models.DecimalField(blank=True,null=True,decimal_places=2, max_digits=5)
    prevStateSch=models.IntegerField(blank=True,null=True)
    noOfArrears=models.IntegerField(blank=True,null=True)

class rejectedAppl(models.Model):
    staff=models.ForeignKey(staff)
    applId=models.ForeignKey(alumini_scholoarship_registration)
    class Meta:
        unique_together = (
            ('staff', 'applId')
        )

class student_history(models.Model):
    student = models.ForeignKey(student)
    semester = models.ForeignKey(semester)
    prev_cen = models.IntegerField(default=-1)
    prev_st = models.IntegerField(default=-1)
    prev_alum = models.IntegerField(default=-1)
    cur_cen = models.IntegerField(default=-1)
    cur_st = models.IntegerField(default=-1)
    cur_alum = models.IntegerField(default=-1)
    attendancePerc = models.DecimalField(default=-1,decimal_places=2, max_digits=3)


class GradePoints(models.Model):
    student = models.ForeignKey(student)
    semester = models.ForeignKey(semester)
    course = models.ForeignKey(courses)
    GradePt = models.IntegerField(default=-1)
    GrdPtReal = models.IntegerField(default=-1)

    class Meta:
        unique_together = ('student', 'semester', 'course')

class weightage(models.Model):
    name=models.CharField(max_length=100)
    attendance=models.IntegerField(default=0)
    marks=models.IntegerField(default=0)
    conduct=models.IntegerField(default=0)
    gctAAass=models.IntegerField(default=0)
    finCondn=models.IntegerField(default=0)
    diffAbl=models.IntegerField(default=0)
    parentdis=models.IntegerField(default=0)
    noOfArrears=models.IntegerField(default=0)
    otherAss=models.IntegerField(default=0)
    project=models.IntegerField(default=0)
    patent=models.IntegerField(default=0)
    ppt=models.IntegerField(default=0)
    internship=models.IntegerField(default=0)
    sports=models.IntegerField(default=0)
    club=models.IntegerField(default=0)
    attScore=models.FloatField(default=0.0)
    marksSc=models.FloatField(default=0.0)
    conductSc=models.FloatField(default=0.0)
    prYearAssSc=models.FloatField(default=0.0)
    finanSc=models.FloatField(default=0.0)
    diffSc=models.FloatField(default=0.0)
    disSc=models.FloatField(default=0.0)
    noOfArrSc=models.FloatField(default=0.0)
    othAssSc=models.FloatField(default=0.0)
    projectPar=models.FloatField(default=0.0)
    projectCon=models.FloatField(default=0.0)
    projectSec=models.FloatField(default=0.0)
    projectFir=models.FloatField(default=0.0)
    patentSub=models.FloatField(default=0.0)
    patentSta=models.FloatField(default=0.0)
    patentApp=models.FloatField(default=0.0)
    patentGra=models.FloatField(default=0.0)
    pptPar=models.FloatField(default=0.0)
    pptCon=models.FloatField(default=0.0)
    pptSec=models.FloatField(default=0.0)
    pptFir=models.FloatField(default=0.0)
    internSc=models.FloatField(default=0.0)
    sportUniv=models.FloatField(default=0.0)
    sportZonal=models.FloatField(default=0.0)
    sportDis=models.FloatField(default=0.0)
    sportSta=models.FloatField(default=0.0)
    sportNati=models.FloatField(default=0.0)
    sportInter=models.FloatField(default=0.0)
    clubMem=models.FloatField(default=0.0)
    clubAct=models.FloatField(default=0.0)
    clubLead=models.FloatField(default=0.0)
    clubAward=models.FloatField(default=0.0)



class FileUpload(models.Model):
    title = models.CharField(max_length=500)
    fil = models.FileField('resultXLfile', upload_to='result_files', null=True, blank=True)

class Type(models.Model):
    num=models.IntegerField()
    staffs=models.CharField(max_length=500)

class bonafide(models.Model):
    reason = models.CharField(max_length=500)
    description = models.CharField(max_length=500)
    typeOfTrack=models.ForeignKey(Type)
    max_amnt=models.IntegerField(default=-1)
    disabled=models.BooleanField(default=False)

class bonafideIns(models.Model):
    student=models.ForeignKey(student)
    bonafide=models.ForeignKey(bonafide)
    comment=models.CharField(max_length=500)
    status=models.IntegerField()
    max_amount=models.IntegerField()
    applied_date=models.DateTimeField()
    approved_date=models.DateTimeField()
    yrDuringAppr=models.IntegerField()

'''
class PlacementDetail(models.Model):
    student=models.ForeignKey(student)
    internshipDet=models.ManyToManyField(internshipDet)
    placementDet=models.ManyToManyField(placementDet)


class internshipDet(models.Model):
    company=models.ForeignKey(Company)
    durationDays=models.IntegerField()
    startDate=models.DateField()
    stipPerMon=models.IntegerField()
    designation=models.CharField(max_length=55)
    place=models.CharField(max_length=55)
    

class placementDet(models.Model):
    company=models.ForeignKey(Company)
    designation=models.CharField(max_length=355)
    CTC=models.FloatField()
    dateOfPlaced=models.DateField()
    dateOfJoin=models.DateField()

    

class Company(models.Model):
    name=models.CharField(max_length=30)
    contactNo=models.PhoneNumberField()
    emailIdS=models.EmailField()
    typeOfComp=models.CharField(max_length=51,choices=)
    contPersFN=models.CharField(max_length=51,)
    contPersLN=models.CharField(max_length=51)
    contEmail=models.EmailField()
    contPhNo=models.PhoneNumberField()

class yrOfAcademic(models.Model):
    startDate=models.DateField()
    endDate=models.DateField()

class yearCompIns(models.Model):
    comapany=models.ForeignKey(Company)
    yrOfAcademic=models.ForeignKey(yrOfAcademic)
    PtOfContact=models.ForeignKey(student)
    studsPlaced=models.ManyToManyField(student)
     
    def getDeptOfPOC(self):
        return self.PtOfContact.department

class CRFform(models.Model):
    compIns=models.ForeignKey(yearCompIns)
    designation=models.CharField(max_length=255)
    description=models.CharField(max_length=555)
    placeOfPost=models.CharField(max_length=55)
    eligDepts=models.ManyToManyField(department_programmes)
    noOfVacc=models.SmallIntegerField()
    packCTCmin=models.FloatField()
    packCTCmax=models.FloatField()
    packageInWords=models.CharField()
    bondOrServYrs=models.FloatField()
    prePlacmntTalk=models.BooleanField()
    sizeOfTeam=models.IntegerField()
    accommodation=models.BooleanField()
    travelConv=models.BooleanField()
    techHRpanel=models.SmallIntegerField()
    genHRpanel=models.SmallIntegerField()
    GDpanels=models.SmallIntegerField()
    systemsReq=models.SmallIntegerField()
    aptiType=models.CharField(max_length=355,choices=)
    foodConv=models.BooleanField()
    dateOfPlacement=models.DateTimeField()'''