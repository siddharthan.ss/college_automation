from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from alumni.views.fill_sem_marks import fill_sem_marks
from alumni.views.views1 import *
from alumni.views.views import apply_alumni_scholarship, delete_my_application, appliacationFormPDF,applFormPdfAlum, \
    view_alumni_scholarships, upload_result_file, close_application, fillConduct,editApplication,getDetails,closeApplStaff,editWeightage,calcWeightage,reApply,ResultSchPdf,ResultSchCSV
urlpatterns = [
    url(r'^apply_alumni_scholarship/$', apply_alumni_scholarship, name="apply_alumini_scholarship"),
    url(r'^delete_my_application/(?P<application_id>\d+)/$', delete_my_application, name="delete_my_application"),
    url(r'^download_application_as_pdf/(?P<application_id>\d+)/$', login_required(appliacationFormPDF.as_view()),
        name="download_application_as_pdf"),
    url(r'^download_result_as_pdf_alumni/(?P<cat>\d+)/$', login_required(ResultSchPdf.as_view()),
        name="download_result_as_pdf_alumni"),
    url(r'^download_result_as_csv_alumni/(?P<cat>\d+)/$', login_required(ResultSchCSV),
        name="download_result_as_csv_alumni"),
    url(r'^download_appl_as_pdf_alumni/(?P<application_id>\d+)/$', login_required(applFormPdfAlum.as_view()),
        name="download_appl_as_pdf_alumni"),
    url(r'download_bonafides/(?P<pk>\d+)(?P<type>\d+)/$',login_required(download_bonafides.as_view()),name="download_bonafides"),
    #url(r'download30DaysPdf/(?P<pk>.+)/$',login_required(download30DaysPdf.as_view()),name="download30DaysPdf"),
    url(r'^fill_sem_marks/$', fill_sem_marks, name="fill_sem_marks"),
    #url(r'^download_30Days/',download_30Days,name="download_30Days"),
    url(r'^add_bonafide_formats/',add_bonafide_formats,name="add_bonafide_formats"),
    url(r'^view_bonafide_formats/',view_bonafide_formats,name="view_bonafide_formats"),
    url(r'^edit_formats/',edit_formats,name="edit_formats"),
    url(r'^view_applications/', view_alumni_scholarships, name="view_applications"),
    url(r'^upload_result_file',upload_result_file,name="upload_result_file"),
    url(r'close_application/(?P<application_id>\d+)/$', close_application, name="close_application"),
    url(r'^fillConduct/',fillConduct, name='fillConduct'),
    url(r'^edit_application/(?P<application_id>\d+)/$',editApplication, name='edit_application'),
    url(r'^getDetails/(?P<application_id>\d+)/$', getDetails, name='getDetails'),
    url(r'closeApplStaff/(?P<application_id>\d+)/$',closeApplStaff,name='closeApplStaff'),
    url(r'editWeightage',editWeightage,name='editWeightage'),
    url(r'calcWeightage',calcWeightage,name='calcWeightage'),
    url(r'reApply',reApply,name='reApply'),
    url(r'applyBon',apply_bonafide,name='applyBon'),
    url(r'apprFA',approve_bonafide_fa,name='apprFA'),
    url(r'apprHOD',approve_bonafide_hod,name='apprHOD'),
    url(r'apprPrinc',approve_bonafide_princ,name='apprPrinc'),
    #url(r'^finalScholarship/',finalScholarship,name='finalScholarship')
]