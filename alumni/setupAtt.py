import xlrd
import pandas as pd
from accounts.models import student
from alumni.models import COEdetails
lis=['CI15FATT.xls','CS15FATT.xls','EC15FATT.xls','EE15FATT.xls','IB15FATT.xls','IC15FATT.xls','IT15FATT.xls','ME15FATT.xls','PR15FATT.xls']
for i in lis:
    count=0
    df=pd.read_excel('./alumni/ATTENDANCE-2015BATCH/'+i)
    col=df.columns
    for ins in df['REGISTER']:
        try:
            stud_inst=student.objects.get(roll_no=ins)
            print(ins)
        except:
            count+=1
            continue
        if str(df['ATTENDANCE'][count])==str('nan'):
            print('NOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO',df['ATTENDANCE'][count])
            count+=1
            continue
        try:
            COEins=COEdetails.objects.get(student=stud_inst)
            COEins.attendancePerc=float(df['ATTENDANCE'][count])
            COEins.save()
            print(df['ATTENDANCE'][count])
        except:
            COEdetails.objects.create(student=stud_inst,attendancePerc=float(df['ATTENDANCE'][count]))
            print('Except',df['ATTENDANCE'][count])
        count+=1
