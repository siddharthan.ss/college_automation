from django.conf.urls import url

from messaging.views import inbox, sentbox, recyclebin, show_all_notifications, ajax_call
from messaging.views.notify import notif

urlpatterns = [

    # url(r'^messaging', messaging, name='messaging'),
    url(r'^inbox/$', inbox, name='inbox'),
    url(r'^sentbox/$', sentbox, name='sentbox'),
    url(r'^recyclebin/$', recyclebin, name='recyclebin'),
    url(r'^notifications/$', show_all_notifications, name='notifications'),
    url(r'^new_notifications/$', ajax_call, name='new_notifications'),
    url(r'^notify', notif, name='notify'),
]
