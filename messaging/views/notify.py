from pprint import pprint

from django.shortcuts import render
# from notify.models import Notification
from notifications.models import Notification
from notifications.signals import notify

from accounts.models import CustomUser


def notif(request):
    user = CustomUser.objects.get(email=request.user)
    user1 = CustomUser.objects.get(email='s1@gmail.com')
    notify.send(user1, recipient=user, verb='you reached level 10')

    # "recipient" can also be a Group, the notification will be sent to all the Users in the Group
    # notify.send(comment.user, recipient='hod@gmail.com', verb=u'replied', action_object=comment,
    #           description=comment.comment, target=comment.content_object)

    # notify.send(request.user, recipient=user, actor=request.user, verb = 'followed you.', nf_type = 'followed_by_one_user')

    # notify.send(follow_instance.user, recipient=follow_instance.follow_object, verb=u'has followed you',
    #           action_object=instance, description=u'', target=follow_instance.follow_object, level='success')

    notification = Notification.objects.unread()
    pprint(notification)

    return render(request, 'messaging/notif.html')
