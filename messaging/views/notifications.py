from datetime import datetime

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render
from notifications.models import Notification

from accounts.models import CustomUser
from messaging.models import notifications


def send_message_notifications(dept, receiver, content, url, read, action, is_group):
    dept_obj = dept
    if is_group:
        list_of_receivers = []
        list_of_emails = []
        for member in receiver:
            for each in CustomUser.objects.filter(staff__department=dept_obj, groups__name=member):
                list_of_emails.append(each)
            for each in CustomUser.objects.filter(student__department=dept_obj, groups__name=member):
                list_of_emails.append(each)
        for there in list_of_emails:
            if there not in list_of_receivers:
                list_of_receivers.append(str(there))
        for each in list_of_receivers:
            receiver_obj = CustomUser.objects.get(email=each)
            notifications.objects.create(recipient=receiver_obj, content=content, url=url, date=datetime.now(),
                                         is_read=read, is_action=action)
    else:
        for each in receiver:
            receiver_obj = CustomUser.objects.get(email=each)
            notifications.objects.create(recipient=receiver_obj, content=content, url=url, date=datetime.now(),
                                         is_read=read, is_action=action)


@login_required
def show_notifications(request):
    current_user = request.user
    list_of_notifications = []
    read = 0
    action = 0
    for each in notifications.objects.filter(recipient=current_user).filter(is_action=True).order_by('-date'):
        action += 1  # action is required
        list_of_notifications.append(each.content)
    for each in notifications.objects.filter(recipient=current_user).filter(is_read=False).order_by('-date'):
        read += 1  # not yet read the notifiactions
        display_message = str(each.content)
        display_message = display_message[:35]  # no of characters to be displayed in popup
        display_message += '...'
        temp = {}
        temp['content'] = display_message
        temp['link'] = each.url
        list_of_notifications.append(temp)
    new = read + action
    request.session['new'] = new
    if not list_of_notifications:
        list_of_notifications.append('No_noti_avail')
    request.session['noti'] = list_of_notifications
    # print(list_of_notifications)
    list1 = ['hod', 'faculty', 'student']  # passing groups
    # list= ['hod@gmail.com']                #passing selected users as list
    # send_message_notifications('CSE', list, 'helloall', 'view_timetable', False, False, False)               #last field indicates whether the receiver parameter passes group or individual users

    return list_of_notifications


@login_required
def show_all_notifications(request):
    if request.method == 'POST':
        if 'delete' in request.POST:
            delete_notifications_list = request.POST.getlist('delete_list')
            print(delete_notifications_list)
            for each in delete_notifications_list:
                Notification.objects.filter(id=each).delete()

        if 'delete_all' in request.POST:
            Notification.objects.filter(recipient=request.user).delete()

    list_of_notifications = []
    for each in Notification.objects.filter(recipient=request.user):
        list_of_notifications.append(each)

    return render(request, 'messaging/notifications.html', {'notifications': list_of_notifications})


@login_required
def ajax_call(request):
    if request.method == 'POST':
        if request.is_ajax():
            Notification.objects.filter(recipient=request.user).update(unread=False)
            return HttpResponse('ok')
