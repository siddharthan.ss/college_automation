from datetime import datetime, date, timedelta

from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from accounts.models import CustomUser, student, staff
from messaging.models import message

"""
def messaging(request):
    current_user = request.user
    print(current_user)
    if request.method == 'POST':
        if 'send_mail' in request.POST:
            recipient = request.POST.get('selected_recipient')
            subject = request.POST.get('subject')
            body = request.POST.get('body')
            print(recipient)
            recipient_obj = CustomUser.objects.get(email=recipient)
            send_message(current_user, recipient_obj, subject, body)
        if 'delete' in request.POST:
            if request.POST.getlist('inbox_delete'):
                print('inbox_delete')
                delete_list = []
                delete_list = request.POST.getlist('inbox_delete')
                print(delete_list)
                for each in delete_list:
                    temp_split = str(each).split('~')
                    inbox_delete_mail = temp_split[0]
                    inbox_delete_date = temp_split[1]
                    inbox_delete_mail_obj = CustomUser.objects.get(email=inbox_delete_mail)
                    print('inbox_delete_mail=' + str(inbox_delete_mail_obj))
                    print('inbox_delete_date=' + str(inbox_delete_date))
                    message.objects.filter(sender=inbox_delete_mail_obj, date=inbox_delete_date).update(recipient_copy=False)
            if request.POST.getlist('sentbox_delete'):
                delete_list = []
                delete_list = request.POST.getlist('sentbox_delete')
                # print(delete_list)
                for each in delete_list:
                    temp_split = str(each).split('~')
                    inbox_delete_mail = temp_split[0]
                    inbox_delete_date = temp_split[1]
                    inbox_delete_mail_obj = CustomUser.objects.get(email=inbox_delete_mail)
                    # print('inbox_delete_mail=' + str(inbox_delete_mail_obj))
                    # print('inbox_delete_date=' + str(inbox_delete_date))
                    message.objects.filter(recipient=inbox_delete_mail_obj, date=inbox_delete_date).update(sender_copy=False)
            if request.POST.getlist('recyclebin_inbox_mail'):
                delete_list = []
                delete_list = request.POST.getlist('recyclebin_inbox_mail')
                print(delete_list)
                for each in delete_list:
                    temp_split = str(each).split('~')
                    inbox_delete_mail = temp_split[0]
                    inbox_delete_date = temp_split[1]
                    inbox_delete_mail_obj = CustomUser.objects.get(email=inbox_delete_mail)
                    print('recyclebin_inbox_mail_obj=' + str(inbox_delete_mail_obj))
                    print('recyclebin_inbox_date=' + str(inbox_delete_date))
                    message.objects.filter(sender=inbox_delete_mail_obj, date=inbox_delete_date).update(recipient_recyclable=False)
                    print(ob)
            print(request.POST.getlist('recyclebin_sentbox_mail'))
            if request.POST.getlist('recyclebin_sentbox_mail'):
                delete_list = []
                delete_list = request.POST.getlist('recyclebin_sentbox_mail')
                print(delete_list)
                for each in delete_list:
                    temp_split = str(each).split('~')
                    inbox_delete_mail = temp_split[0]
                    inbox_delete_date = temp_split[1]
                    inbox_delete_mail_obj = CustomUser.objects.get(email=inbox_delete_mail)
                    print('recyclebin_sentbox_mail_obj=' + str(inbox_delete_mail_obj))
                    print('recyclebin_sentbox_date=' + str(inbox_delete_date))
                    message.objects.filter(recipient=inbox_delete_mail_obj, date=inbox_delete_date).update(sender_recyclable=False)
                    # print(ob)
        if 'restore' in request.POST:
            print(request.POST.getlist('recyclebin_inbox_mail'))
            print(request.POST.getlist('recyclebin_sentbox_mail'))
            if request.POST.getlist('recyclebin_inbox_mail'):
                restore_list = []
                restore_list = request.POST.getlist('recyclebin_inbox_mail')
                print(restore_list)
                for each in restore_list:
                    temp_split = str(each).split('~')
                    inbox_delete_mail = temp_split[0]
                    inbox_delete_date = temp_split[1]
                    inbox_delete_mail_obj = CustomUser.objects.get(email=inbox_delete_mail)
                    print('inbox_delete_mail=' + str(inbox_delete_mail_obj))
                    print('inbox_delete_date=' + str(inbox_delete_date))
                    ob = message.objects.filter(sender=inbox_delete_mail_obj, date=inbox_delete_date).update(recipient_copy=True)
            if request.POST.getlist('recyclebin_sentbox_mail'):
                restore_list = []
                restore_list = request.POST.getlist('recyclebin_sentbox_mail')
                print(restore_list)
                for each in restore_list:
                    temp_split = str(each).split('~')
                    inbox_delete_mail = temp_split[0]
                    inbox_delete_date = temp_split[1]
                    inbox_delete_mail_obj = CustomUser.objects.get(email=inbox_delete_mail)
                    print('inbox_delete_mail=' + str(inbox_delete_mail_obj))
                    print('inbox_delete_date=' + str(inbox_delete_date))
                    ob = message.objects.filter(recipient=inbox_delete_mail_obj, date=inbox_delete_date).update(sender_copy=True)
    '''recipient list'''
    recipt_list = []
    for each in CustomUser.objects.all():
        if current_user != each:
            temp = {}
            ob = ''
            if student.objects.filter(user__email=each.email).exists():
                for ob in student.objects.filter(user__email=each.email):
                    temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
                    temp['mail'] = each.email
            elif staff.objects.filter(user__email=each.email).exists():
                for ob in staff.objects.filter(user__email=each.email):
                    temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
                    temp['mail'] = each.email
            if ob != '':
                recipt_list.append(temp)
                # print(temp)
    '''inbox'''
    inbox_list = []
    for each in message.objects.filter(recipient=current_user, recipient_copy=True).order_by('-date'):
        temp = {}
        temp['sender'] = each.sender
        temp['subject'] = each.subject
        temp['body'] = each.body
        temp['date'] = str(each.date)
        if student.objects.filter(user__email=each.sender).exists():
            for ob in student.objects.filter(user__email=each.sender):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        elif staff.objects.filter(user__email=each.sender).exists():
            for ob in staff.objects.filter(user__email=each.sender):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        inbox_list.append(temp)
        # print(temp)
    '''sentbox'''
    sentbox_list = []
    for each in message.objects.filter(sender=current_user, sender_copy=True).order_by('-date'):
        temp = {}
        temp['recipient'] = each.recipient
        temp['subject'] = each.subject
        temp['body'] = each.body
        temp['date'] = str(each.date)
        if student.objects.filter(user__email=each.recipient).exists():
            for ob in student.objects.filter(user__email=each.recipient):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        elif staff.objects.filter(user__email=each.recipient).exists():
            for ob in staff.objects.filter(user__email=each.recipient):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        sentbox_list.append(temp)
        # print(temp)
    '''recyclebin_inbox'''
    recyclebin_inbox_list = []
    for each in message.objects.filter(recipient=current_user, recipient_copy=False, recipient_recyclable=True).order_by('-date'):
        temp = {}
        temp['sender'] = each.sender
        temp['subject'] = each.subject
        temp['body'] = each.body
        temp['date'] = str(each.date)
        if student.objects.filter(user__email=each.sender).exists():
            for ob in student.objects.filter(user__email=each.sender):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        elif staff.objects.filter(user__email=each.sender).exists():
            for ob in staff.objects.filter(user__email=each.sender):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        recyclebin_inbox_list.append(temp)
        # print(temp)
    '''recyclebin_sentbox'''
    recyclebin_sentbox_list = []
    for each in message.objects.filter(sender=current_user, sender_copy=False, sender_recyclable=True).order_by('-date'):
        temp = {}
        temp['recipient'] = each.recipient
        temp['subject'] = each.subject
        temp['body'] = each.body
        temp['date'] = str(each.date)
        if student.objects.filter(user__email=each.recipient).exists():
            for ob in student.objects.filter(user__email=each.recipient):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        elif staff.objects.filter(user__email=each.recipient).exists():
            for ob in staff.objects.filter(user__email=each.recipient):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        recyclebin_sentbox_list.append(temp)
        # print(temp)
    return render(request, 'messaging/message.html', {'recipt_list': recipt_list, 'inbox_list': inbox_list, 'sentbox_list': sentbox_list, 'recyclebin_inbox_list': recyclebin_inbox_list, 'recyclebin_sentbox_list': recyclebin_sentbox_list})
"""


@login_required
def inbox(request):
    current_user = request.user
    print(current_user)
    if request.method == 'POST':
        if 'send_mail' in request.POST:
            recipient_list = request.POST.getlist('selected_recipient')
            subject = request.POST.get('subject')
            body = request.POST.get('body')
            # print(recipient_list)
            for recipient in recipient_list:
                recipient_obj = CustomUser.objects.get(email=recipient)
                send_message(current_user, recipient_obj, subject, body)
        if 'delete' in request.POST:
            if request.POST.getlist('inbox_delete'):
                delete_list = []
                delete_list = request.POST.getlist('inbox_delete')
                # print(delete_list)
                for each in delete_list:
                    message.objects.filter(id=int(each)).update(recipient_copy=False)
    '''recipient list'''
    recipt_list_staff = []
    recipt_list_student = []
    for each in CustomUser.objects.all():
        if each:  # current_user != each
            temp = {}
            ob = ''
            if student.objects.filter(user__is_approved=True).filter(user__email=each.email).exists():
                for ob in student.objects.filter(user__email=each.email):
                    temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
                    temp['mail'] = each.email
                    temp['dept'] = ob.department.acronym
                    recipt_list_student.append(temp)
            elif staff.objects.filter(user__is_approved=True).filter(user__email=each.email).exists():
                for ob in staff.objects.filter(user__email=each.email):
                    temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
                    temp['mail'] = each.email
                    temp['dept'] = ob.department.acronym
                    recipt_list_staff.append(temp)
    '''inbox'''
    inbox_list = []
    for each in message.objects.filter(recipient=current_user, recipient_copy=True).order_by('-date'):
        temp = {}
        temp['id'] = each.id
        temp['sender'] = each.sender
        temp['subject'] = each.subject
        temp['body'] = each.body
        temp['date'] = date_convertion(each.date)
        if student.objects.filter(user__email=each.sender).exists():
            for ob in student.objects.filter(user__email=each.sender):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        elif staff.objects.filter(user__email=each.sender).exists():
            for ob in staff.objects.filter(user__email=each.sender):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        inbox_list.append(temp)
    return render(request, 'messaging/inbox-sentbox.html', {
        'recipt_list_staff': recipt_list_staff,
        'recipt_list_student': recipt_list_student,
        'inbox_list': inbox_list,
        'inbox': True
    })


@login_required
def sentbox(request):
    current_user = request.user
    print(current_user)
    if request.method == 'POST':
        if 'send_mail' in request.POST:
            recipient_list = request.POST.getlist('selected_recipient')
            subject = request.POST.get('subject')
            body = request.POST.get('body')
            # print(recipient_list)
            for recipient in recipient_list:
                recipient_obj = CustomUser.objects.get(email=recipient)
                send_message(current_user, recipient_obj, subject, body)
        if 'delete' in request.POST:
            if request.POST.getlist('sentbox_delete'):
                delete_list = []
                delete_list = request.POST.getlist('sentbox_delete')
                # print(delete_list)
                for each in delete_list:
                    message.objects.filter(id=int(each)).update(
                        sender_copy=False)
    '''recipient list'''
    recipt_list_staff = []
    recipt_list_student = []
    for each in CustomUser.objects.all():
        if each:  # current_user != each
            temp = {}
            ob = ''
            if student.objects.filter(user__is_approved=True).filter(user__email=each.email).exists():
                for ob in student.objects.filter(user__email=each.email):
                    temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
                    temp['mail'] = each.email
                    temp['dept'] = ob.department.acronym
                    recipt_list_student.append(temp)
            elif staff.objects.filter(user__is_approved=True).filter(user__email=each.email).exists():
                for ob in staff.objects.filter(user__email=each.email):
                    temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
                    temp['mail'] = each.email
                    temp['dept'] = ob.department.acronym
                    recipt_list_staff.append(temp)
    '''sent_box'''
    sentbox_list = []
    for each in message.objects.filter(sender=current_user, sender_copy=True).order_by('-date'):
        temp = {}
        temp['id'] = each.id
        temp['recipient'] = each.recipient
        temp['subject'] = each.subject
        temp['body'] = each.body
        temp['date'] = date_convertion(each.date)
        if student.objects.filter(user__email=each.recipient).exists():
            for ob in student.objects.filter(user__email=each.recipient):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        elif staff.objects.filter(user__email=each.recipient).exists():
            for ob in staff.objects.filter(user__email=each.recipient):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        sentbox_list.append(temp)
    return render(request, 'messaging/inbox-sentbox.html', {
        'recipt_list_staff': recipt_list_staff,
        'recipt_list_student': recipt_list_student,
        'sentbox_list': sentbox_list,
        'sentbox': True
    })


@login_required
def recyclebin(request):
    current_user = request.user
    # print(current_user)
    default_selected = 'inbox'
    if request.method == 'POST':
        if 'delete' in request.POST:
            if request.POST.getlist('recyclebin_inbox_mail'):
                delete_list = []
                delete_list = request.POST.getlist('recyclebin_inbox_mail')
                # print(delete_list)
                for each in delete_list:
                    message.objects.filter(id=int(each)).update(
                        recipient_recyclable=False)
                    default_selected = 'inbox'
                    # print(ob)
            # print(request.POST.getlist('recyclebin_sentbox_mail'))
            if request.POST.getlist('recyclebin_sentbox_mail'):
                delete_list = []
                delete_list = request.POST.getlist('recyclebin_sentbox_mail')
                # print(delete_list)
                for each in delete_list:
                    message.objects.filter(id=int(each)).update(
                        sender_recyclable=False)
                    # print(ob)
        if 'restore' in request.POST:
            # print(request.POST.getlist('recyclebin_inbox_mail'))
            # print(request.POST.getlist('recyclebin_sentbox_mail'))
            if request.POST.getlist('recyclebin_inbox_mail'):
                restore_list = []
                restore_list = request.POST.getlist('recyclebin_inbox_mail')
                # print(restore_list)
                for each in restore_list:
                    ob = message.objects.filter(id=int(each)).update(
                        recipient_copy=True)
            if request.POST.getlist('recyclebin_sentbox_mail'):
                restore_list = []
                restore_list = request.POST.getlist('recyclebin_sentbox_mail')
                # print(restore_list)
                for each in restore_list:
                    ob = message.objects.filter(id=int(each)).update(
                        sender_copy=True)

    '''recyclebin_inbox'''
    recyclebin_inbox_list = []
    for each in message.objects.filter(recipient=current_user, recipient_copy=False,
                                       recipient_recyclable=True).order_by('-date'):
        temp = {}
        temp['id'] = each.id
        temp['sender'] = each.sender
        temp['subject'] = each.subject
        temp['body'] = each.body
        temp['date'] = date_convertion(each.date)
        if student.objects.filter(user__email=each.sender).exists():
            for ob in student.objects.filter(user__email=each.sender):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        elif staff.objects.filter(user__email=each.sender).exists():
            for ob in staff.objects.filter(user__email=each.sender):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        recyclebin_inbox_list.append(temp)
        # print(temp)
    '''recyclebin_sentbox'''
    recyclebin_sentbox_list = []
    for each in message.objects.filter(sender=current_user, sender_copy=False, sender_recyclable=True).order_by(
            '-date'):
        temp = {}
        temp['id'] = each.id
        temp['recipient'] = each.recipient
        temp['subject'] = each.subject
        temp['body'] = each.body
        temp['date'] = date_convertion(each.date)
        if student.objects.filter(user__email=each.recipient).exists():
            for ob in student.objects.filter(user__email=each.recipient):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        elif staff.objects.filter(user__email=each.recipient).exists():
            for ob in staff.objects.filter(user__email=each.recipient):
                temp['name'] = ob.first_name + ' ' + ob.middle_name + ' ' + ob.last_name
        recyclebin_sentbox_list.append(temp)
        # print(temp)
    return render(request, 'messaging/inbox-sentbox.html', {
        'recyclebin_inbox_list': recyclebin_inbox_list,
        'recyclebin_sentbox_list': recyclebin_sentbox_list,
        'recyclebin': True
    })


@login_required
def send_message(sender, receiver, subject, body):
    message.objects.create(sender=sender, recipient=receiver, subject=subject, body=body, date=datetime.now(),
                           sender_copy=True, recipient_copy=True)


@login_required
def date_convertion(date_str):
    today = str(date.today())
    yesterday = str(date.today() - timedelta(1))
    req_date = str(date_str.strftime("%Y-%m-%d"))
    if req_date == today:
        req_time = str(date_str.strftime("%l:%M:%S %p"))
        date_time = req_time
    elif req_date == yesterday:
        req_time = str(date_str.strftime(" at %l:%M:%S %p"))
        date_time = 'Yesterday' + req_time
    else:
        date_time = str(date_str.strftime("%d %B %Y at %l:%M:%S %p"))

    return date_time
