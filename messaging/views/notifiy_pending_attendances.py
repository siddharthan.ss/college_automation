from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required

from accounts.models import staff
from curriculum.views import get_pending_attendance_periods
from curriculum.views.common_includes import get_staff_course_allotment_instances_for_staff
from dashboard.notification import send_notification


@login_required
@permission_required('curriculum.can_mark_attendance')
def notify_pending_attendances(request):
    try:
        staff_instance = staff.objects.get(user=request.user)
    except staff.DoesNotExist:
        return None
    allotment_list = get_staff_course_allotment_instances_for_staff(staff_instance)

    if allotment_list is None:
        return None

    pending_attendance_count = 0

    for allotment_inst in allotment_list:
        pending_attendance_periods = get_pending_attendance_periods(allotment_inst.semester, staff_instance,
                                                                    allotment_inst.course)

        if pending_attendance_periods is None:
            return None

        pprint(len(pending_attendance_periods))

        pending_attendance_count += len(pending_attendance_periods)

    if pending_attendance_count > 0:
        request.session['pending_attendances'] = True
        content = 'You have ' + str(pending_attendance_count) + ' pending attendances to Mark'
        url = 'pending_attendances'
        send_notification(request.user, message=content, url=url)
