from django.db import models
# Create your models here.
from simple_history.models import HistoricalRecords

from accounts.models import CustomUser
from curriculum.models import department


class notifications(models.Model):
    recipient = models.ForeignKey(CustomUser, related_name='recipient_noti')
    content = models.CharField(max_length=400)
    date = models.DateTimeField()
    # department = models.ForeignKey(department)
    is_read = models.BooleanField(default=False)
    is_action = models.BooleanField(default=False)
    url = models.CharField(max_length=200, blank=True, null=True)
    history = HistoricalRecords()


class message(models.Model):
    subject = models.CharField(max_length=200)
    body = models.CharField(max_length=2000)
    date = models.DateTimeField()
    sender_copy = models.BooleanField(default=True)
    recipient_copy = models.BooleanField(default=True)
    sender_recyclable = models.BooleanField(default=True)
    recipient_recyclable = models.BooleanField(default=True)
    sender = models.ForeignKey(CustomUser, related_name='sender')
    recipient = models.ForeignKey(CustomUser, related_name='recipient')
    history = HistoricalRecords()


class draft_box(models.Model):
    sender = models.ForeignKey(CustomUser)
    subject = models.CharField(max_length=200)
    body = models.CharField(max_length=2000)
    date = models.DateTimeField()
    history = HistoricalRecords()
