from django.conf.urls import url
from assignment.views.assignment import addAssignment, viewAssignments

urlpatterns = [

    url(r'^add_assignment/$', addAssignment, name="addAssignment"),
    url(r'^view_assignment/$', viewAssignments, name="viewAssignments"),
]
