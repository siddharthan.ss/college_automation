from django.core.files.storage import FileSystemStorage
from django.db import models
from accounts.models import staff
from curriculum.models import staff_course

fs = FileSystemStorage(location='/media/assignment-files')

class assignmentFiles(models.Model):
    file = models.FileField(storage=fs)
    name = models.CharField(max_length=300)
    owner = models.ForeignKey(staff)

class assignment(models.Model):
    staff_course = models.ForeignKey(staff_course)
    files = models.ManyToManyField(assignmentFiles)
    title = models.CharField(max_length=2000)
    description =  models.CharField(max_length=5000, blank=True, null=True)
    last_date = models.DateTimeField()
