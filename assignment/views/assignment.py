from datetime import datetime

from django.contrib.auth.decorators import login_required, permission_required
from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from django.utils import timezone

from accounts.models import student, staff
from curriculum.models import staff_course
from assignment.models import assignment, assignmentFiles

from common.utils.studentUtil import getSemesterPlanofStudent, getStaffCoursesForStudent
from common.utils.ReportTabsUtil import getCoursesHandlingByStaff
from django.conf import settings


@login_required
@permission_required('marks.can_add_internal_marks')
def addAssignment(request):
    staffInstance = staff.objects.get(user=request.user)

    courseList = []
    for each in getCoursesHandlingByStaff(staffInstance):
        if each['staffCourseInstance'].course.subject_type == 'T':
            courseList.append(each)

    try:
        staffCoursePk = courseList[0]['staffCourseInstance'].pk
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course that have quiz marks',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'staffCoursePk' in request.POST:
            staffCoursePk = request.POST.get('staffCoursePk')

        elif 'postAssignment' in request.POST:
            staffCoursePk = request.POST.get('hiddenStaffCoursePk')
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            title = request.POST.get('title')
            description = request.POST.get('description')
            date = request.POST.get('date')
            dateTimeInstance = datetime.strptime(date, '%d/%m/%Y').strftime('%Y-%m-%d')
            uploadedFile = request.FILES['uploadFile']
            fs = FileSystemStorage(location= settings.MEDIA_ROOT + 'assignment-files/')
            filename = fs.save(uploadedFile.name, uploadedFile)
            uploadedFileUrl = str(fs.url(filename)).split('media/')[1]
            uploadedFileUrl = '/media/assignment-files/' + uploadedFileUrl

            assignmentFilesInstance = assignmentFiles(file=uploadedFileUrl, name=uploadedFile.name, owner=staffInstance)
            assignmentFilesInstance.save()

            assignmentInstance = assignment(title=title, description=description, last_date=dateTimeInstance,
                                            staff_course=staffCourseInstance)
            assignmentInstance.save()
            assignmentInstance.files.add(assignmentFilesInstance)

        elif 'update' in request.POST:
            editPk = request.POST.get('editPk')
            title = request.POST.get('title')
            description = request.POST.get('description')
            date = request.POST.get('date')
            dateTimeInstance = datetime.strptime(date, '%d/%m/%Y').strftime('%Y-%m-%d')

            assignmentInstance = assignment.objects.get(pk=editPk)
            assignmentInstance.title = title
            assignmentInstance.description = description
            assignmentInstance.last_date = dateTimeInstance
            assignmentInstance.save()

        elif 'delete' in request.POST:
            deletePk = request.POST.get('deletePk')

            assignmentInstance = assignment.objects.get(pk=deletePk)
            assignmentInstance.delete()

        else:
            assignmentList = assignment.objects.all()

            required_edit = ''

            for each in assignmentList:
                if str(request.POST.get('edit-' + str(each.pk))) == 'Edit':
                    required_edit = int(each.pk)

            assignmentInfo = assignment.objects.get(pk=required_edit)

            return render(request, 'assignment/editAssignment.html', {
                'assignmentInfo': assignmentInfo
            })

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    assignmentList = assignment.objects.filter(staff_course=staffCourseInstance)

    return render(request, 'assignment/addAssignment.html', {
        'staffCourseInstance': staffCourseInstance,
        'courseList': courseList,
        'assignmentList': assignmentList
    })


@login_required
@permission_required('curriculum.can_choose_courses')
def viewAssignments(request):
    studentInstance = student.objects.get(user=request.user)
    semesterInstance = getSemesterPlanofStudent(studentInstance)

    if not semesterInstance:
        msg = {
            'page_title': 'Access Withheld',
            'title': 'Access Withheld',
            'description': 'You have not assigned to any of the section. Contact your Programme Coordinator for immediate remedy.',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    courseList = getStaffCoursesForStudent(studentInstance)

    try:
        staffCoursePk = int(courseList[0].pk)
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses Found',
            'description': 'Contact your Programme co-ordinator for more information',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    assignmentList = assignment.objects.filter(staff_course=staffCourseInstance,
                                               last_date__gte=timezone.now().date()).order_by('last_date')

    return render(request, 'assignment/viewAssignments.html', {
        'courseList': courseList,
        'staffCourseInstance': staffCourseInstance,
        'student_instance': studentInstance,
        'assignmentList': assignmentList
    })
