from django.db import models
# Create your models here.
from simple_history.models import HistoricalRecords

from accounts.models import CustomUser

DEVICE_CHOICES = (
    ('Desktop', 'Desktop'),
    ('Laptop', 'Laptop'),
    ('Mobile', 'Mobile'),
)

OPERATING_SYSTEM = (
    ('Windows', 'Windows'),
    ('Linux', 'Linux'),
    ('Mac', 'Mac'),
    ('Android', 'Android'),
)


class wifi(models.Model):
    user = models.ForeignKey(CustomUser)
    type_of_device = models.CharField(max_length=8, choices=DEVICE_CHOICES)
    make_and_model = models.CharField(max_length=50, verbose_name='Device make and model')
    mac_id = models.CharField(max_length=17, verbose_name="MAC ID", unique=True)
    operating_system = models.CharField(max_length=10, choices=OPERATING_SYSTEM)
    connection_approved = models.BooleanField(default=False)
    username = models.CharField(max_length=20, verbose_name='Wi-Fi Username')
    password = models.CharField(max_length=10, verbose_name='Wi-Fi Password')
    history = HistoricalRecords()

    class Meta:
        unique_together = ('username', 'mac_id')


class change_mac(models.Model):
    new_mac = models.CharField(max_length=17, verbose_name="MAC ID", unique=True)
    wifi_user = models.ForeignKey(wifi)
    approved = models.BooleanField(default=False)
    request_date = models.DateField()
    reason = models.TextField()
    history = HistoricalRecords()


class allowed_no_of_accounts_user(models.Model):
    user = models.ForeignKey(CustomUser)
    no_of_accounts = models.IntegerField()
    history = HistoricalRecords()


class allowed_no_of_accounts_group(models.Model):
    GROUP_CHOICES = (
        ('student', 'student'),
        ('staff', 'staff'),
    )
    group_name = models.CharField(max_length=7, choices=GROUP_CHOICES)
    no_of_accounts = models.IntegerField()
    history = HistoricalRecords()
