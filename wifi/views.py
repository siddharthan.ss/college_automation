from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone
from ldap3 import MODIFY_REPLACE
from curriculum.views.common_includes import get_students_under_staff
from accounts.models import student, staff, site_settings
from curriculum.models import semester_migration
from wifi.forms import wifi_form, reset_wifi_password_form, existing_wifi_form, change_mac_form
from wifi.ldap_functions import *
from wifi.models import wifi, allowed_no_of_accounts_group, allowed_no_of_accounts_user, change_mac


@login_required
def applied_success(request):
    modal = {
        'heading': 'Success',
        'body': 'Your application has been successfully submitted.Your connection will be activated mostly in 48 working hours',
    }
    return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})


@login_required
def existing_account(request):
    if 'userAccountControl' not in request.session or 'msNPCallingStationID' not in request.session:
        return HttpResponseRedirect('/wifi/apply')

    userAccountControlValue = request.session['userAccountControl']

    if request.POST.get('resubmit') or request.POST.get('apply_wifi') or request.POST.get('sync'):
        derieved_post_data = {
            'mac_id': request.session['msNPCallingStationID'],
        }
        form = existing_wifi_form(req=request, initial=derieved_post_data)

        if request.method == 'POST' and request.POST.get('apply_wifi'):
            form = existing_wifi_form(request.POST, req=request)
            if form.is_valid():
                temp = form.save(commit=False)
                temp.user = request.user
                temp.save()
                if request.session['sync']:
                    wifi_account_inst = wifi.objects.get(mac_id=request.session['msNPCallingStationID'])
                    wifi_account_inst.connection_approved = True
                    wifi_account_inst.save()
                return HttpResponseRedirect('/wifi/apply')

        return render(request, 'wifi/wifi_form.html', {'form': form})

    temp = {}
    if userAccountControlValue == 66048:
        request.session['sync'] = True
        temp['account_status'] = 'Active'
    if userAccountControlValue == 66050:
        request.session['sync'] = False
        temp['account_status'] = 'Deactive'

    temp['mac_id'] = request.session['msNPCallingStationID']
    temp['username'] = request.session['roll_no']

    return render(request, 'wifi/ad_existing_account.html', {'info': temp})


@login_required
def apply_wifi(request):
    form = wifi_form(req=request)
    if request.method == 'POST' and request.POST.get('apply_wifi'):
        form = wifi_form(request.POST, req=request)
        if form.is_valid():
            temp = form.save(commit=False)
            temp.user = request.user
            temp.save()

    try:
        wifi_user_instance = wifi.objects.get(user=request.user)
        userAccountControlValue = None
        if wifi_user_instance.connection_approved:
            conn_obj = connect_ldap_server(request)
            if not conn_obj:
                modal = {
                    'heading': 'Error',
                    'body': 'Unable to connect to wifi server. Contact Network Admin',
                }
                return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
            if get_wifi_domiain(request) == 'sid.gct.com':
                dc = 'dc=sid,dc=gct,dc=com'
            else:
                dc = 'dc=cse,dc=gct,dc=com'
            result = conn_obj.search('ou=GCT,' + dc,
                                     '(&(objectclass=person)(|(userPrincipalName=' + wifi_user_instance.username + '@' + get_wifi_domiain(
                                         request) + ')(sAMAccountName=' + wifi_user_instance.username + ')))',
                                     attributes=['userAccountControl'])
            print(result)
            if not result and request.method != 'POST':
                wifi_user_instance.delete()
                return apply_wifi(request)
            entry = conn_obj.entries[0]
            json_entry_string = entry.entry_to_json()
            json_entry = json.loads(json_entry_string)
            print(json_entry['attributes'])
            attr = json_entry['attributes']
            userAccountControlValue = attr['userAccountControl'][0]
            print('UserAccountControl = ' + str(userAccountControlValue))

            conn_obj.unbind()
        temp = {}
        temp['username'] = wifi_user_instance.username
        temp['password'] = wifi_user_instance.password
        temp['mac_id'] = wifi_user_instance.mac_id
        temp['device_type'] = wifi_user_instance.type_of_device
        temp['model'] = wifi_user_instance.make_and_model
        temp['os'] = wifi_user_instance.operating_system
        if wifi_user_instance.connection_approved == True:
            temp['request_status'] = 'Connection Approved'
        else:
            temp['request_status'] = 'Pending Approval from Network Admin'

        if not userAccountControlValue:
            temp['account_status'] = 'Pending'
        else:
            if userAccountControlValue == 66048:
                temp['account_status'] = 'Active'
            if userAccountControlValue == 66050:
                temp['account_status'] = 'Deactive'

        request.session['wifi_user_id'] = wifi_user_instance.pk

        # add info regarding change of mac
        change_mac_inst_query = change_mac.objects.filter(wifi_user=wifi_user_instance, approved=False)
        if change_mac_inst_query.exists():
            change_mac_inst = change_mac_inst_query.get()
            temp['new_mac'] = change_mac_inst.new_mac
            temp['new_mac_status'] = 'Pending Approval from Network Admin'
        return render(request, 'wifi/existing_account.html', {'info': temp})

    except wifi.DoesNotExist:
        student_instance = student.objects.filter(user=request.user)
        if student_instance.exists():
            roll_no = student_instance.get().roll_no
            conn_obj = connect_ldap_server(request)
            if not conn_obj:
                modal = {
                    'heading': 'Error',
                    'body': 'Unable to connect to wifi server. Contact Network Admin',
                }
                return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
            if get_wifi_domiain(request) == 'sid.gct.com':
                dc = 'dc=sid,dc=gct,dc=com'
            else:
                dc = 'dc=cse,dc=gct,dc=com'
            result = conn_obj.search('ou=GCT,' + dc,
                                     '(&(objectclass=person)(|(userPrincipalName=' + roll_no + '@' + get_wifi_domiain(
                                         request) + ')(sAMAccountName=' + roll_no + ')))',
                                     attributes=['userAccountControl', 'msNPCallingStationID'])
            print('already exists')
            print(result)
            if result:
                entry = conn_obj.entries[0]
                json_entry_string = entry.entry_to_json()
                json_entry = json.loads(json_entry_string)
                print(json_entry['attributes'])
                attr = json_entry['attributes']
                userAccountControlValue = attr['userAccountControl'][0]
                if len(attr['msNPCallingStationID']) <= 0:
                    mac_id = None
                    modal = {
                        'heading': 'MAC ID issue',
                        'body': 'There seems to be an error in WIFI auth server regarding your account.It will be fixed in 48 working hours',
                    }
                    return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
                else:
                    mac_id = attr['msNPCallingStationID'][0]
                print('UserAccountControl = ' + str(userAccountControlValue))

                request.session['userAccountControl'] = userAccountControlValue
                request.session['roll_no'] = roll_no
                request.session['msNPCallingStationID'] = ':'.join(a + b for a, b in zip(*[iter(mac_id)] * 2))

                conn_obj.unbind()
                return HttpResponseRedirect('/wifi/existing_account')

        return render(request, 'wifi/wifi_form.html', {'form': form})


@login_required
@permission_required('wifi.can_approve_wifi_users')
def approve_wifi_users(request):
    if request.method == 'POST' and request.POST.get("approve"):
        select_wifi_registarations = request.POST.getlist('selected_users')
        pprint(select_wifi_registarations)

        conn_obj = connect_ldap_server(request)
        if not conn_obj:
            modal = {
                'heading': 'Error',
                'body': 'Unable to connect to Active directory server',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

        for requested_user_id in select_wifi_registarations:
            requested_user = wifi.objects.get(pk=requested_user_id)
            try:
                stud_inst = student.objects.get(user=requested_user.user)
                print(stud_inst)

                # ldap dn string building
                cn = 'cn=' + requested_user.username
                ug_pg_ou = 'ou=' + str(stud_inst.qualification)
                year_ou = 'ou=' + str(stud_inst.batch.get_start_year())
                if get_wifi_domiain(request) == 'sid.gct.com':
                    dc = 'dc=sid,dc=gct,dc=com'
                else:
                    dc = 'dc=cse,dc=gct,dc=com'

                user = cn + ',' + year_ou + ',' + ug_pg_ou + ',ou=Students,ou=GCT,' + dc

                # attribute dict

                caller_id = requested_user.mac_id.replace(':', '')
                caller_id = caller_id.replace('-', '')
                print(caller_id)

                description = str(stud_inst.qualification + ' Year ' + str(stud_inst.get_current_year()))

                attributes = {
                    'objectClass': ['user', 'person', 'organizationalPerson', 'top'],
                    'sAMAccountName': requested_user.username,
                    'userPrincipalName': requested_user.username + '@' + get_wifi_domiain(request),
                    'msNPCallingStationID': caller_id,
                    'msNPSavedCallingStationID': caller_id,
                    'givenName': str(stud_inst.first_name),
                    'sn': str(stud_inst.last_name),
                    'displayName': str(stud_inst.roll_no),
                    'description': description,
                }
                return_val = add_ldap_user(conn_obj, user, attributes, requested_user.password, dc, 'student')
                if not return_val:
                    modal = {
                        'heading': 'Error',
                        'body': 'Unable to add user ' + requested_user.username + '(' + requested_user.user.email + ')',
                    }
                    return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

                requested_user.connection_approved = True
                requested_user.save()
            except student.DoesNotExist:
                staff_inst = staff.objects.get(user=requested_user.user)
                print(staff_inst)

                # ldap dn string building
                cn = 'cn=' + requested_user.username
                if get_wifi_domiain(request) == 'sid.gct.com':
                    dc = 'dc=sid,dc=gct,dc=com'
                else:
                    dc = 'dc=cse,dc=gct,dc=com'

                user = cn + ',' + 'ou=Teaching Faculty,ou=Staffs,ou=GCT,' + dc

                # attribute dict

                caller_id = requested_user.mac_id.replace(':', '')
                caller_id = caller_id.replace('-', '')
                print(caller_id)

                attributes = {
                    'objectClass': ['user', 'person', 'organizationalPerson', 'top'],
                    'sAMAccountName': requested_user.username,
                    'userPrincipalName': requested_user.username + '@' + get_wifi_domiain(request),
                    'msNPCallingStationID': caller_id,
                    'msNPSavedCallingStationID': caller_id,
                    'givenName': str(staff_inst.first_name),
                    'sn': str(staff_inst.last_name),
                    'displayName': str(staff_inst),
                    'description': staff_inst.designation,
                }
                return_val = add_ldap_user(conn_obj, user, attributes, requested_user.password, dc, 'staff')
                if not return_val:
                    modal = {
                        'heading': 'Error',
                        'body': 'Unable to add user ' + requested_user.username + '(' + requested_user.user.email + ')',
                    }
                    return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

                requested_user.connection_approved = True
                requested_user.save()
        conn_obj.unbind()

    unapproved_users = wifi.objects.filter(connection_approved=False)

    list_of_users = []
    for each_user in unapproved_users:
        temp = {}
        try:
            staff_inst = staff.objects.get(user=each_user.user)
            temp['user_type'] = 'staff'
            temp['name'] = str(staff_inst)
            temp['uname'] = each_user.username
            temp['department'] = staff_inst.department.acronym
            temp['designation'] = staff_inst.designation
        except staff.DoesNotExist:
            stud_inst = student.objects.get(user=each_user.user)
            temp['user_type'] = 'student'
            temp['name'] = str(stud_inst)
            temp['uname'] = each_user.username
            temp['department'] = stud_inst.department.acronym
            curr_year = stud_inst.get_current_year()
            if (curr_year == 1):
                curr_year_string = str(curr_year) + 'st'
            elif (curr_year == 2):
                curr_year_string = str(curr_year) + 'nd'
            elif (curr_year == 3):
                curr_year_string = str(curr_year) + 'rd'
            elif (curr_year == 4):
                curr_year_string = str(curr_year) + 'th'
            temp['designation'] = stud_inst.qualification + ' ' + curr_year_string + ' year'

        temp['device_type'] = each_user.type_of_device
        temp['mac_id'] = each_user.mac_id
        temp['wifi_request_id'] = each_user.pk
        list_of_users.append(temp)

    return render(request, 'wifi/approve_wifi.html', {'users': list_of_users})


@login_required
@permission_required('wifi.can_approve_wifi_staff')
def approve_wifi_students(request):
    if request.method == 'POST' and request.POST.get("approve"):
        select_wifi_registarations = request.POST.getlist('selected_users')
        pprint(select_wifi_registarations)
        conn_obj = connect_ldap_server(request)
        if not conn_obj:
            modal = {
                'heading': 'Error',
                'body': 'Unable to connect to Active directory server',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

        for requested_user_id in select_wifi_registarations:
            requested_user = wifi.objects.get(pk=requested_user_id)
            try:
                stud_inst = student.objects.get(user=requested_user.user)
                print(stud_inst)

                # ldap dn string building
                cn = 'cn=' + requested_user.username
                ug_pg_ou = 'ou=' + str(stud_inst.qualification)
                year_ou = 'ou=' + str(stud_inst.batch.get_start_year())
                if get_wifi_domiain(request) == 'sid.gct.com':
                    dc = 'dc=sid,dc=gct,dc=com'
                else:
                    dc = 'dc=cse,dc=gct,dc=com'

                user = cn + ',' + year_ou + ',' + ug_pg_ou + ',ou=Students,ou=GCT,' + dc

                # attribute dict

                caller_id = requested_user.mac_id.replace(':', '')
                caller_id = caller_id.replace('-', '')
                print(caller_id)

                description = str(stud_inst.qualification + ' Year ' + str(stud_inst.get_current_year()))

                attributes = {
                    'objectClass': ['user', 'person', 'organizationalPerson', 'top'],
                    'sAMAccountName': requested_user.username,
                    'userPrincipalName': requested_user.username + '@' + get_wifi_domiain(request),
                    'msNPCallingStationID': caller_id,
                    'msNPSavedCallingStationID': caller_id,
                    'givenName': str(stud_inst.first_name),
                    'sn': str(stud_inst.last_name),
                    'displayName': str(stud_inst.roll_no),
                    'description': description,
                }
                return_val = add_ldap_user(conn_obj, user, attributes, requested_user.password, dc, 'student')
                if not return_val:
                    modal = {
                        'heading': 'Error',
                        'body': 'Unable to add user ' + requested_user.username + '(' + requested_user.user.email + ')',
                    }
                    return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

                requested_user.connection_approved = True
                requested_user.save()
            except student.DoesNotExist:
                modal = {
                    'heading': 'Error',
                    'body': 'Unable to add user because no permissions' + requested_user.username + '(' + requested_user.user.email + ')',
                }
                return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
        conn_obj.unbind()
        
    staff_instance = staff.objects.get(user=request.user)
    students=[]
    students=get_students_under_staff(staff_instance)
    unapproved_users = wifi.objects.filter(connection_approved=False).filter(user__in=students)
    list_of_users = []
    for each_user in unapproved_users:
        temp = {}
        stud_inst = student.objects.get(user=each_user.user)
        temp['user_type'] = 'student'
        temp['name'] = str(stud_inst)
        temp['uname'] = each_user.username
        temp['department'] = stud_inst.department.acronym
        curr_year = stud_inst.get_current_year()
        if (curr_year == 1):
            curr_year_string = str(curr_year) + 'st'
        elif (curr_year == 2):
            curr_year_string = str(curr_year) + 'nd'
        elif (curr_year == 3):
            curr_year_string = str(curr_year) + 'rd'
        elif (curr_year == 4):
            curr_year_string = str(curr_year) + 'th'
        temp['designation'] = stud_inst.qualification + ' ' + curr_year_string + ' year'

        temp['device_type'] = each_user.type_of_device
        temp['mac_id'] = each_user.mac_id
        temp['wifi_request_id'] = each_user.pk
        list_of_users.append(temp)

    return render(request, 'wifi/approve_wifi.html', {'users': list_of_users})



@login_required
@permission_required('wifi.can_approve_wifi_staff')
def approve_change_mac_staff(request):
    if request.method == 'POST' and request.POST.get("approve"):
        select_change_mac_registerations = request.POST.getlist('selected_users')
        pprint(select_change_mac_registerations)

        conn_obj = connect_ldap_server(request)
        if not conn_obj:
            modal = {
                'heading': 'Error',
                'body': 'Unable to connect to Active directory server',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

        for change_request_id in select_change_mac_registerations:
            change_request_obj = change_mac.objects.get(pk=change_request_id)
            if get_wifi_domiain(request) == 'sid.gct.com':
                dc = 'dc=sid,dc=gct,dc=com'
            else:
                dc = 'dc=cse,dc=gct,dc=com'

            conn_obj.search('ou=GCT,' + dc,
                            '(&(objectclass=person)(sAMAccountName=' + change_request_obj.wifi_user.username + '))')

            if len(conn_obj.entries) == 1:
                json_entry_string = conn_obj.entries[0].entry_to_json()
                json_entry = json.loads(json_entry_string)
                user = json_entry['dn']
                print(user)
                new_mac_string = change_request_obj.new_mac.replace(':', '')
                new_mac_string = new_mac_string.replace('-', '')
                newMacStatus = conn_obj.modify(user,
                                               {'msNPCallingStationID': [(MODIFY_REPLACE, [new_mac_string])]})
                newSavedMacStatus = conn_obj.modify(user,
                                                    {'msNPSavedCallingStationID': [(MODIFY_REPLACE, [new_mac_string])]})

                if not newMacStatus or not newSavedMacStatus:
                    modal = {
                        'heading': 'Error',
                        'body': 'Unable to change MAC for user ' + str(change_request_obj.wifi_user.username),
                    }
                    return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

                change_request_obj.approved = True
                wifi_user_inst = change_request_obj.wifi_user
                wifi_user_inst.mac_id = change_request_obj.new_mac
                wifi_user_inst.save()
                change_request_obj.wifi_user.save()
                change_request_obj.save()

    list_of_unapproved_change_macs = []
    staff_instance = staff.objects.get(user=request.user)
    students=[]
    students=get_students_under_staff(staff_instance)
    unapproved_change_mac_query = change_mac.objects.filter(approved=False,wifi_user__user__in=students)

    for entry in unapproved_change_mac_query:
        temp = {}
        try:
            staff_inst = staff.objects.get(user=entry.wifi_user.user)
            temp['user_type'] = 'staff'
            temp['name'] = str(staff_inst)
            temp['uname'] = entry.wifi_user.username
            temp['department'] = staff_inst.department.acronym
            temp['designation'] = staff_inst.designation
        except staff.DoesNotExist:
            stud_inst = student.objects.get(user=entry.wifi_user.user)
            temp['user_type'] = 'student'
            temp['name'] = str(stud_inst)
            temp['uname'] = entry.wifi_user.username
            temp['department'] = stud_inst.department.acronym
            curr_year = stud_inst.get_current_year()
            if (curr_year == 1):
                curr_year_string = str(curr_year) + 'st'
            elif (curr_year == 2):
                curr_year_string = str(curr_year) + 'nd'
            elif (curr_year == 3):
                curr_year_string = str(curr_year) + 'rd'
            elif (curr_year == 4):
                curr_year_string = str(curr_year) + 'th'
            temp['designation'] = stud_inst.qualification + ' ' + curr_year_string + ' year'

        temp['new_mac_id'] = entry.new_mac
        temp['new_mac_request_id'] = entry.pk
        temp['reason'] = entry.reason
        list_of_unapproved_change_macs.append(temp)

    return render(request, 'wifi/change_mac.html', {"users": list_of_unapproved_change_macs})






@login_required
@permission_required('wifi.can_approve_wifi_users')
def approve_change_mac(request):
    if request.method == 'POST' and request.POST.get("approve"):
        select_change_mac_registerations = request.POST.getlist('selected_users')
        pprint(select_change_mac_registerations)

        conn_obj = connect_ldap_server(request)
        if not conn_obj:
            modal = {
                'heading': 'Error',
                'body': 'Unable to connect to Active directory server',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

        for change_request_id in select_change_mac_registerations:
            change_request_obj = change_mac.objects.get(pk=change_request_id)
            if get_wifi_domiain(request) == 'sid.gct.com':
                dc = 'dc=sid,dc=gct,dc=com'
            else:
                dc = 'dc=cse,dc=gct,dc=com'

            conn_obj.search('ou=GCT,' + dc,
                            '(&(objectclass=person)(sAMAccountName=' + change_request_obj.wifi_user.username + '))')

            if len(conn_obj.entries) == 1:
                json_entry_string = conn_obj.entries[0].entry_to_json()
                json_entry = json.loads(json_entry_string)
                user = json_entry['dn']
                print(user)
                new_mac_string = change_request_obj.new_mac.replace(':', '')
                new_mac_string = new_mac_string.replace('-', '')
                newMacStatus = conn_obj.modify(user,
                                               {'msNPCallingStationID': [(MODIFY_REPLACE, [new_mac_string])]})
                newSavedMacStatus = conn_obj.modify(user,
                                                    {'msNPSavedCallingStationID': [(MODIFY_REPLACE, [new_mac_string])]})

                if not newMacStatus or not newSavedMacStatus:
                    modal = {
                        'heading': 'Error',
                        'body': 'Unable to change MAC for user ' + str(change_request_obj.wifi_user.username),
                    }
                    return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

                change_request_obj.approved = True
                wifi_user_inst = change_request_obj.wifi_user
                wifi_user_inst.mac_id = change_request_obj.new_mac
                wifi_user_inst.save()
                change_request_obj.wifi_user.save()
                change_request_obj.save()

    list_of_unapproved_change_macs = []

    unapproved_change_mac_query = change_mac.objects.filter(approved=False)

    for entry in unapproved_change_mac_query:
        temp = {}
        try:
            staff_inst = staff.objects.get(user=entry.wifi_user.user)
            temp['user_type'] = 'staff'
            temp['name'] = str(staff_inst)
            temp['uname'] = entry.wifi_user.username
            temp['department'] = staff_inst.department.acronym
            temp['designation'] = staff_inst.designation
        except staff.DoesNotExist:
            stud_inst = student.objects.get(user=entry.wifi_user.user)
            temp['user_type'] = 'student'
            temp['name'] = str(stud_inst)
            temp['uname'] = entry.wifi_user.username
            temp['department'] = stud_inst.department.acronym
            curr_year = stud_inst.get_current_year()
            if (curr_year == 1):
                curr_year_string = str(curr_year) + 'st'
            elif (curr_year == 2):
                curr_year_string = str(curr_year) + 'nd'
            elif (curr_year == 3):
                curr_year_string = str(curr_year) + 'rd'
            elif (curr_year == 4):
                curr_year_string = str(curr_year) + 'th'
            temp['designation'] = stud_inst.qualification + ' ' + curr_year_string + ' year'

        temp['new_mac_id'] = entry.new_mac
        temp['new_mac_request_id'] = entry.pk
        temp['reason'] = entry.reason
        list_of_unapproved_change_macs.append(temp)

    return render(request, 'wifi/change_mac.html', {"users": list_of_unapproved_change_macs})


@login_required
@permission_required('wifi.can_approve_wifi_users')
def deactivate_non_mac_users(request):
    conn_obj = connect_ldap_server(request)
    if request.method == 'POST' and request.POST.get("deactivate"):
        selected_accounts_to_deactivate = request.POST.getlist('selected_users')
        for selected_user in selected_accounts_to_deactivate:
            newans = conn_obj.modify(selected_user,
                                     {'userAccountControl': [(MODIFY_REPLACE, ['66050'])]})
            print(newans)
            print(conn_obj.result)
        print('selected' + str(selected_accounts_to_deactivate))

    if not conn_obj:
        modal = {
            'heading': 'Error',
            'body': 'Unable to connect to Active directory server',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    if get_wifi_domiain(request) == 'sid.gct.com':
        dc = 'dc=sid,dc=gct,dc=com'
    else:
        dc = 'dc=cse,dc=gct,dc=com'

    conn_obj.search('ou=GCT,' + dc,
                    '(&(&(objectclass=person)(!(msNPCallingStationID=*))(!(userAccountControl=66050))))',
                    attributes=['cn', 'userPrincipalName', 'description', 'sAMAccountName'])

    invalid_mac_user_list = []
    for entry in conn_obj.entries:
        # print(entry.entry_to_json())
        json_entry_string = entry.entry_to_json()
        json_entry = json.loads(json_entry_string)

        print(json_entry['attributes'])

        attr = json_entry['attributes']
        temp_dict = {}
        temp_dict['dn'] = json_entry['dn']
        print(temp_dict['dn'])
        if len(attr['userPrincipalName']) > 0:
            temp_dict['userPrincipalName'] = attr['userPrincipalName'][0]
        if len(attr['sAMAccountName']) > 0:
            temp_dict['sAMAccountName'] = attr['sAMAccountName'][0]
        if attr['description']:
            temp_dict['description'] = attr['description'][0]
        else:
            temp_dict['description'] = ''
        temp_dict['cn'] = attr['cn'][0]

        invalid_mac_user_list.append(temp_dict)

    conn_obj.unbind()

    return render(request, 'wifi/invalid_mac.html', {'invalid_users': invalid_mac_user_list})

@login_required
def get_student_user_name(request):
    if request.method == 'POST':
        if request.is_ajax():
            dictionary = {}
            try:
                student_inst = student.objects.get(user=request.user)
                dictionary['username'] = str(student_inst.roll_no)
                return JsonResponse(dictionary)

            except student.DoesNotExist:
                dictionary['username'] = 'none'
                return JsonResponse(dictionary)


@login_required
@permission_required('wifi.can_approve_wifi_users')
def no_of_group_accounts(request):
    if request.method == 'POST' and request.POST.get('update_accounts'):
        staff_no = request.POST.get('staff')
        student_no = request.POST.get('student')
        if int(student_no) <= 3 and int(staff_no) <= 3:
            staff_no_obj = allowed_no_of_accounts_group.objects.get(group_name='staff')
            staff_no_obj.no_of_accounts = staff_no
            staff_no_obj.save()

            student_no_obj = allowed_no_of_accounts_group.objects.get(group_name='student')
            student_no_obj.no_of_accounts = student_no
            student_no_obj.save()

    no_of_acc_objs = allowed_no_of_accounts_group.objects.all()
    return render(request, 'wifi/no_group_accounts.html', {'acc_obj': no_of_acc_objs})


@login_required
@permission_required('wifi.can_approve_wifi_users')
def no_of_user_accounts(request):
    if request.method == 'POST' and request.POST.get('update_user_account'):
        roll_no_email_id = request.POST.get('roll_no_or_email_id')

        student_no = request.POST.get('student')
        if int(student_no) <= 3 and int(roll_no_email_id) <= 3:
            staff_no_obj = allowed_no_of_accounts_group.objects.get(group_name='staff')
            staff_no_obj.no_of_accounts = roll_no_email_id
            staff_no_obj.save()

            student_no_obj = allowed_no_of_accounts_group.objects.get(group_name='student')
            student_no_obj.no_of_accounts = student_no
            student_no_obj.save()

    no_of_user_acc_objs = allowed_no_of_accounts_user.objects.all()
    return render(request, 'wifi/no_of_user_accounts.html', {'acc_obj': no_of_user_acc_objs})


@login_required
@permission_required('wifi.can_approve_wifi_users')
def active_directory_settings(request):
    host_name = request.get_host()

    if '127.0.0.1' in host_name:
        IP_KEY = 'LOCAL_WIFI_LDAP_SERVER_ADDR'
        ADMIN_DN_KEY = 'LOCAL_ADMIN_DN'
        ADMIN_PASSWORD_KEY = 'LOCAL_ADMIN_PASSWORD'
        DOMAIN_KEY = 'LOCAL_WIFI_DOMAIN'
    else:
        IP_KEY = 'GCT_WIFI_LDAP_SERVER_ADDR'
        ADMIN_DN_KEY = 'GCT_ADMIN_DN'
        ADMIN_PASSWORD_KEY = 'GCT_ADMIN_PASSWORD'
        DOMAIN_KEY = 'GCT_WIFI_DOMAIN'

    if request.method == 'POST' and request.POST.get('update_settings'):
        new_ip = request.POST.get('ip')
        new_admin_dn = request.POST.get('admin_dn')
        new_admin_password = request.POST.get('admin_password')
        new_domain = request.POST.get('domain')
        try:
            local_ad_ip = site_settings.objects.get(key=IP_KEY)
            local_ad_ip.value = new_ip
            local_ad_ip.save()
        except site_settings.DoesNotExist:
            local_ad_ip = site_settings.objects.create(
                key=IP_KEY,
                value=new_ip
            )
        try:
            local_ad_domain = site_settings.objects.get(key=DOMAIN_KEY)
            local_ad_domain.value = new_domain
            local_ad_domain.save()
        except site_settings.DoesNotExist:
            local_ad_domain = site_settings.objects.create(
                key=DOMAIN_KEY,
                value=new_domain
            )
        try:
            local_ad_admin_dn = site_settings.objects.get(key=ADMIN_DN_KEY)
            local_ad_admin_dn.value = new_admin_dn
            local_ad_admin_dn.save()
        except site_settings.DoesNotExist:
            local_ad_admin_dn = site_settings.objects.create(
                key=ADMIN_DN_KEY,
                value=new_admin_dn
            )
        try:
            local_ad_admin_password = site_settings.objects.get(key=ADMIN_PASSWORD_KEY)
            local_ad_admin_password.value = new_admin_password
            local_ad_admin_password.save()
        except site_settings.DoesNotExist:
            local_ad_admin_password = site_settings.objects.create(
                key=ADMIN_PASSWORD_KEY,
                value=new_admin_password
            )

    temp = {}
    try:
        local_ad_ip = site_settings.objects.get(key=IP_KEY)
        temp['IP'] = local_ad_ip.value
    except site_settings.DoesNotExist:
        temp['IP'] = ''

    try:
        local_ad_domain = site_settings.objects.get(key=DOMAIN_KEY)
        temp['DOMAIN'] = local_ad_domain.value
    except site_settings.DoesNotExist:
        temp['DOMAIN'] = ''

    try:
        local_ad_admin_dn = site_settings.objects.get(key=ADMIN_DN_KEY)
        temp['ADMIN_DN'] = local_ad_admin_dn.value
    except site_settings.DoesNotExist:
        temp['ADMIN_DN'] = ''

    try:
        local_ad_admin_password = site_settings.objects.get(key=ADMIN_PASSWORD_KEY)
        temp['ADMIN_PASSWORD'] = local_ad_admin_password.value
    except site_settings.DoesNotExist:
        temp['IP'] = ''

    return render(request, 'wifi/active_directory_settings.html', {'settings': temp})


@login_required
def reset_wifi_password(request):
    form = reset_wifi_password_form()

    if request.method == 'POST' and request.POST.get('reset_wifi_password') and 'wifi_user_id' in request.session:
        form = reset_wifi_password_form(request.POST)
        if form.is_valid():
            wifi_user_instance = wifi.objects.get(pk=request.session['wifi_user_id'])
            new_password = form.cleaned_data['password1']

            conn_obj = connect_ldap_server(request)

            if not conn_obj:
                modal = {
                    'heading': 'Error',
                    'body': 'Unable to connect to Wifi server.Please contact Network admin',
                }
                return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

            return_val = password_update(request, conn_obj, wifi_user_instance.username, new_password)
            if return_val:
                wifi_user_instance.password = form.cleaned_data['password1']
                wifi_user_instance.save()
                modal = {
                    'heading': 'Success',
                    'body': 'Your Wi-Fi password has been reset successfully',
                }
                return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
            else:
                modal = {
                    'heading': 'Error',
                    'body': 'Unable to reset your password.Please contact Network admin',
                }
                return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    return render(request, 'wifi/reset_wifi_password_form.html', {'form': form})


@login_required
@permission_required('wifi.can_approve_wifi_users')
def fix_missing_logon_name(request):
    conn_obj = connect_ldap_server(request)
    if not conn_obj:
        modal = {
            'heading': 'Error',
            'body': 'Unable to connect to Active directory server',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
    if request.method == 'POST' and request.POST.get("correct"):
        selected_accounts_to_correct = request.POST.getlist('selected_users')
        print('selected' + str(selected_accounts_to_correct))
        for selected_user in selected_accounts_to_correct:
            print(selected_user)
            split_list = selected_user.split(',')
            print(split_list)
            cn_alone = split_list[0]
            cn_alone_list = cn_alone.split('=')
            cn_name = cn_alone_list[1]
            print('cn name = ' + cn_name)

            newans = conn_obj.modify(selected_user,
                                     {'userPrincipalName': [
                                         (MODIFY_REPLACE, [cn_name + '@' + get_wifi_domiain(request)])]})
            print(newans)
            print(conn_obj.result)

    if get_wifi_domiain(request) == 'sid.gct.com':
        dc = 'dc=sid,dc=gct,dc=com'
    else:
        dc = 'dc=cse,dc=gct,dc=com'

    conn_obj.search('ou=GCT,' + dc,
                    '(&(&(objectclass=person)(!(userPrincipalName=*))(!(userAccountControl=66050))))',
                    attributes=['cn', 'description', 'sAMAccountName', 'msNPCallingStationID'])

    invalid_mac_user_list = []
    for entry in conn_obj.entries:
        # print(entry.entry_to_json())
        json_entry_string = entry.entry_to_json()
        json_entry = json.loads(json_entry_string)

        print(json_entry['attributes'])

        attr = json_entry['attributes']
        temp_dict = {}
        temp_dict['dn'] = json_entry['dn']
        print(temp_dict['dn'])
        if len(attr['msNPCallingStationID']) > 0:
            temp_dict['msNPCallingStationID'] = attr['msNPCallingStationID'][0]
        if len(attr['sAMAccountName']) > 0:
            temp_dict['sAMAccountName'] = attr['sAMAccountName'][0]
        if attr['description']:
            temp_dict['description'] = attr['description'][0]
        else:
            temp_dict['description'] = ''
        temp_dict['cn'] = attr['cn'][0]

        invalid_mac_user_list.append(temp_dict)

    conn_obj.unbind()

    return render(request, 'wifi/missing_logon.html', {'invalid_users': invalid_mac_user_list})


@login_required
def new_mac(request):
    form = change_mac_form()

    if 'wifi_user_id' not in request.session:
        return HttpResponseRedirect('/wifi/apply')

    if request.method == "POST":
        form = change_mac_form(request.POST, req=request)
        if form.is_valid():
            inst = form.save(commit=False)
            wifi_request_inst = wifi.objects.get(pk=request.session['wifi_user_id'])
            inst.wifi_user = wifi_request_inst
            inst.request_date = timezone.now().date()
            inst.save()
            return HttpResponseRedirect('/wifi/apply')

    return render(request, 'wifi/new_mac.html', {'form': form})