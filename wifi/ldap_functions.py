import json
import ssl

from ldap3 import Server, Connection, MODIFY_REPLACE, ALL, MODIFY_ADD
from ldap3 import Tls

from accounts.models import site_settings


def get_wifi_domiain(request):
    host_name = request.get_host()
    print(host_name)

    if '127.0.0.1' in host_name:
        DOMAIN_KEY = 'LOCAL_WIFI_DOMAIN'
    else:
        DOMAIN_KEY = 'GCT_WIFI_DOMAIN'

    domain_obj = site_settings.objects.get(key=DOMAIN_KEY)
    return domain_obj.value


def connect_ldap_server(request):
    host_name = request.get_host()
    print(host_name)

    if '127.0.0.1' in host_name:
        IP_KEY = 'LOCAL_WIFI_LDAP_SERVER_ADDR'
        ADMIN_DN_KEY = 'LOCAL_ADMIN_DN'
        ADMIN_PASSWORD_KEY = 'LOCAL_ADMIN_PASSWORD'
        DOMAIN_KEY = 'LOCAL_WIFI_DOMAIN'
    else:
        IP_KEY = 'GCT_WIFI_LDAP_SERVER_ADDR'
        ADMIN_DN_KEY = 'GCT_ADMIN_DN'
        ADMIN_PASSWORD_KEY = 'GCT_ADMIN_PASSWORD'
        DOMAIN_KEY = 'GCT_WIFI_DOMAIN'

    ip_obj = site_settings.objects.get(key=IP_KEY)
    WIFI_LDAP_SERVER_ADDR = ip_obj.value
    admin_dn_obj = site_settings.objects.get(key=ADMIN_DN_KEY)
    ADMIN_DN = admin_dn_obj.value
    password_obj = site_settings.objects.get(key=ADMIN_PASSWORD_KEY)
    ADMIN_PASSWORD = password_obj.value
    print(locals())

    try:
        tls_configuration = Tls(validate=ssl.CERT_NONE, version=ssl.PROTOCOL_TLSv1)

        server = Server(WIFI_LDAP_SERVER_ADDR, use_ssl=True, tls=tls_configuration, get_info=ALL)

        conn = Connection(server, ADMIN_DN, ADMIN_PASSWORD, auto_bind=True)
        print(conn)
        if conn:
            print('Connection Successfull')

        return conn
    except:
        return False


def add_ldap_user(conn, user, attr, password, dc, type):
    print(user)

    ans = conn.add(user,
                   attributes=attr)

    print(conn.result)

    if (conn.result['description'] == 'entryAlreadyExists'):
        conn.delete(user)
        ans = conn.add(user, attributes=attr)
        print(conn.result)

    print(ans)

    conn.extend.microsoft.modify_password(user, password)

    newans = conn.modify(user,
                         {'userAccountControl': [(MODIFY_REPLACE, ['65536'])]})
    print(newans)

    if type == 'staff':
        group = 'cn=Staffs,ou=Staffs,ou=GCT,' + dc
    if type == 'student':
        group = 'cn=Students,ou=Students,ou=GCT,' + dc
    print(group)
    group_add = conn.modify(group,
                            {'member': [(MODIFY_ADD, [user])]})

    print(group_add)
    print(conn.result)

    if ans and newans and group_add:
        return True
    else:
        return False


def password_update(request, conn_obj, username, new_password):
    if get_wifi_domiain(request) == 'sid.gct.com':
        dc = 'dc=sid,dc=gct,dc=com'
    else:
        dc = 'dc=cse,dc=gct,dc=com'

    result = conn_obj.search('ou=GCT,' + dc,
                             '(&(objectclass=person)(|(userPrincipalName=' + username + '@' + get_wifi_domiain(
                                 request) + ')(sAMAccountName=' + username + ')))', attributes=['userAccountControl'])

    if result:
        entry = conn_obj.entries[0]
        json_entry_string = entry.entry_to_json()
        json_entry = json.loads(json_entry_string)
        dn = json_entry['dn']
        print(dn)

        res = conn_obj.extend.microsoft.modify_password(dn, new_password)
        print(res)

        if res:
            return True

    return False
