from django.conf.urls import url

from wifi.views import apply_wifi, applied_success, approve_wifi_users, deactivate_non_mac_users,approve_wifi_students
from wifi.views import get_student_user_name, no_of_group_accounts, active_directory_settings, \
    reset_wifi_password, fix_missing_logon_name, existing_account, new_mac, approve_change_mac,approve_change_mac_staff

urlpatterns = [
    url(r'^apply', apply_wifi, name='apply_wifi'),
    url(r'^applied_successfully', applied_success, name='applied_successfully'),
    url(r'^approve_wifi_users', approve_wifi_users, name='approve_wifi'),
    url(r'^approve_wifi_users', approve_wifi_students, name='approve_only_wifi'),
    url(r'^empty_mac_id', deactivate_non_mac_users, name='empty_mac'),
    url(r'^get_student_user_name', get_student_user_name, name='get_student_user_name'),
    url(r'^no_of_group_accounts', no_of_group_accounts, name='no_of_group_accounts'),
    url(r'^active_directory_settings', active_directory_settings, name='active_directory_settings'),
    url(r'^reset_wifi_password', reset_wifi_password, name='reset_wifi_password'),
    url(r'^fix_missing_logon_name', fix_missing_logon_name, name='fix_missing_logon_name'),
    url(r'^existing_account', existing_account, name='existing_account'),
    url(r'^new_mac', new_mac, name='new_mac'),
    url(r'^approve_change_mac', approve_change_mac, name='approve_change_mac'),
    url(r'^approve_change_mac', approve_change_mac_staff, name='change_only_mac')
]