import re

from django import forms

from accounts.models import student
from wifi.ldap_functions import *
from wifi.models import wifi, change_mac


class wifi_form(forms.ModelForm):
    req = None

    class Meta:
        model = wifi
        fields = ['type_of_device', 'make_and_model', 'mac_id', 'operating_system', 'username', 'password']

    def __init__(self, *args, **kwargs):
        self.req = kwargs.pop('req', None)
        super(wifi_form, self).__init__(*args, **kwargs)
        form_fields = ['type_of_device', 'make_and_model', 'mac_id', 'operating_system', 'username', 'password']
        for field in form_fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })

        self.fields['mac_id'].widget.attrs.update({
            'placeholder': "xx:xx:xx:xx:xx:xx"
        })

    def clean_make_and_model(self):
        cd = self.cleaned_data
        make_and_model = cd.get('make_and_model')
        make_and_model = make_and_model.title()
        if not re.match(r'^([a-zA-Z0-9 ]+)$', make_and_model):
            raise forms.ValidationError("Enter valid Make and Model")
        return make_and_model

    def clean_mac_id(self):
        cd = self.cleaned_data
        mac_id = cd.get('mac_id')
        if not re.match(r'^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$', mac_id):
            raise forms.ValidationError("Enter valid MAC id")
        number_occurrences = wifi.objects.filter(mac_id=mac_id).count()
        if number_occurrences > 0:
            raise forms.ValidationError("This MAC id is already been registered")

        conn_obj = connect_ldap_server(self.req)
        if not conn_obj:
            raise forms.ValidationError("Unable to access wifi server.Contact Network admin")
        if get_wifi_domiain(self.req) == 'sid.gct.com':
            dc = 'dc=sid,dc=gct,dc=com'
        else:
            dc = 'dc=cse,dc=gct,dc=com'

        caller_id = mac_id.replace(':', '')
        result = conn_obj.search('ou=GCT,' + dc,
                                 '(&(objectclass=person)(msNPCallingStationID=' + caller_id + '))',
                                 attributes=['sAMAccountName'])
        print(result)
        list_of_same_mac_users = []
        for entry in conn_obj.entries:
            json_entry_string = entry.entry_to_json()
            json_entry = json.loads(json_entry_string)
            attr = json_entry['attributes']
            temp_dict = {}
            temp_dict['dn'] = json_entry['dn']
            print(temp_dict['dn'])
            if len(attr['sAMAccountName']) > 0:
                temp_dict['sAMAccountName'] = attr['sAMAccountName'][0]
            list_of_same_mac_users.append(temp_dict)
        conn_obj.unbind()

        if len(list_of_same_mac_users) > 1:
            raise forms.ValidationError("This mac id is been registered my many users.Please report Network Admin")
        elif len(list_of_same_mac_users) == 1:
            user = list_of_same_mac_users[0]
            raise forms.ValidationError("MAC id already registered by user " + user['sAMAccountName'])

        return mac_id

    def clean_username(self):
        cd = self.cleaned_data
        username = cd.get('username')
        if(student.objects.filter(user=self.req.user).exists()):
            student_inst = student.objects.get(user=self.req.user)
            if student_inst.roll_no != username:
                raise forms.ValidationError("Registration number can only be your username")
            if not re.match(r'^([0-9L]+)$', username):
                raise forms.ValidationError("You cannot apply for GCT WiFi untill you get a permanent registration number")
        number_occurrences = wifi.objects.filter(username=username).count()
        if number_occurrences > 0:
            raise forms.ValidationError("This User_id id is already been registered")
        conn_obj = connect_ldap_server(self.req)
        if not conn_obj:
            raise forms.ValidationError("Unable to access wifi server.Contact Network admin")
        if get_wifi_domiain(self.req) == 'sid.gct.com':
            dc = 'dc=sid,dc=gct,dc=com'
        else:
            dc = 'dc=cse,dc=gct,dc=com'
        result = conn_obj.search('ou=GCT,' + dc,
                                 '(&(objectclass=person)(|(userPrincipalName=' + username + '@' + get_wifi_domiain(
                                     self.req) + ')(sAMAccountName=' + username + ')))')
        print(result)
        conn_obj.unbind()
        if result:
            raise forms.ValidationError("Username already exists")
        if len(username) < 5:
            raise forms.ValidationError("Username should be of atleast length 5")
        if not re.match(r'^([a-zA-Z0-9. ]+)$', username):
            raise forms.ValidationError("Username can contain only numbers,letters,space,dots")
        return username

    def clean_password(self):
        cd = self.cleaned_data
        password = cd.get('password')
        if len(password) < 6:
            raise forms.ValidationError("Password should be of atleast length 6")
        if not re.match(r'^([a-zA-Z0-9]+)$', password):
            raise forms.ValidationError("Password can contain only numbers and letters")
        return password

    def clean(self):
        return self.cleaned_data


class reset_wifi_password_form(forms.Form):
    password1 = forms.CharField(widget=forms.PasswordInput, label='New Password')
    password2 = forms.CharField(widget=forms.PasswordInput, label='Re-enter Password')

    required_field = [
        'password1',
        'password2',
    ]

    def __init__(self, *args, **kwargs):
        super(reset_wifi_password_form, self).__init__(*args, **kwargs)
        for field in reset_wifi_password_form.required_field:
            self.fields[field].required = True
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })

    def clean_password1(self):
        cd = self.cleaned_data
        password1 = cd.get('password1')
        if len(password1) < 6:
            raise forms.ValidationError("Password should be of atleast length 6")
        if len(password1) > 10:
            raise forms.ValidationError("Password should be maximum of length 10")
        if not re.match(r'^([a-zA-Z0-9]+)$', password1):
            raise forms.ValidationError("Password can contain only numbers and letters")
        return password1

    def clean_password2(self):
        cd = self.cleaned_data
        password1 = cd.get('password1')
        password2 = cd.get('password2')
        if not password2:
            raise forms.ValidationError("You must confirm your password")
        if password1 != password2:
            raise forms.ValidationError("Your passwords do not match")
        return password2

    def clean(self):
        return self.cleaned_data


class existing_wifi_form(forms.ModelForm):
    req = None

    class Meta:
        model = wifi
        fields = ['type_of_device', 'make_and_model', 'mac_id', 'operating_system', 'username', 'password']

    def __init__(self, *args, **kwargs):
        self.req = kwargs.pop('req', None)
        super(existing_wifi_form, self).__init__(*args, **kwargs)
        form_fields = ['type_of_device', 'make_and_model', 'mac_id', 'operating_system', 'username', 'password']
        for field in form_fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })
        self.fields['mac_id'].widget.attrs.update({
            'placeholder': "xx:xx:xx:xx:xx:xx"
        })

    def clean_make_and_model(self):
        cd = self.cleaned_data
        make_and_model = cd.get('make_and_model')
        make_and_model = make_and_model.title()
        if not re.match(r'^([a-zA-Z0-9 ]+)$', make_and_model):
            raise forms.ValidationError("Enter valid Make and Model")
        return make_and_model

    def clean_mac_id(self):
        cd = self.cleaned_data
        mac_id = cd.get('mac_id')
        if mac_id != self.req.session['msNPCallingStationID']:
            raise forms.ValidationError("You are not allowed to change your MAC id during Syncing.")
        if not re.match(r'^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$', mac_id):
            raise forms.ValidationError("Enter valid MAC id")
        number_occurrences = wifi.objects.filter(mac_id=mac_id).count()
        if number_occurrences > 0:
            raise forms.ValidationError("This MAC id is already been registered")

        conn_obj = connect_ldap_server(self.req)
        if not conn_obj:
            raise forms.ValidationError("Unable to access wifi server.Contact Network admin")
        if get_wifi_domiain(self.req) == 'sid.gct.com':
            dc = 'dc=sid,dc=gct,dc=com'
        else:
            dc = 'dc=cse,dc=gct,dc=com'

        caller_id = mac_id.replace(':', '')
        result = conn_obj.search('ou=GCT,' + dc,
                                 '(&(objectclass=person)(msNPCallingStationID=' + caller_id + '))',
                                 attributes=['sAMAccountName'])
        print(result)
        list_of_same_mac_users = []
        for entry in conn_obj.entries:
            json_entry_string = entry.entry_to_json()
            json_entry = json.loads(json_entry_string)
            attr = json_entry['attributes']
            temp_dict = {}
            temp_dict['dn'] = json_entry['dn']
            print(temp_dict['dn'])
            if len(attr['sAMAccountName']) > 0:
                temp_dict['sAMAccountName'] = attr['sAMAccountName'][0]
            list_of_same_mac_users.append(temp_dict)
        conn_obj.unbind()

        if len(list_of_same_mac_users) > 1:
            raise forms.ValidationError("This mac id is been registered my many users.Please report Network Admin")

        return mac_id

    def clean_username(self):
        cd = self.cleaned_data
        username = cd.get('username')
        if self.req.user.groups.filter(name="student").exists():
            student_inst = student.objects.get(user=self.req.user)
            if username != student_inst.roll_no:
                raise forms.ValidationError("Student username cannot be anything other than roll number")
        number_occurrences = wifi.objects.filter(username=username).count()
        if number_occurrences > 0:
            raise forms.ValidationError("This User_id id is already been registered")
        return username

    def clean_password(self):
        cd = self.cleaned_data
        password = cd.get('password')
        if len(password) < 6:
            raise forms.ValidationError("Password should be of atleast length 6")
        if not re.match(r'^([a-zA-Z0-9]+)$', password):
            raise forms.ValidationError("Password can contain only numbers and letters")
        return password

    def clean(self):
        return self.cleaned_data


class change_mac_form(forms.ModelForm):
    req = None

    class Meta:
        model = change_mac
        fields = ['new_mac', 'reason']

    def __init__(self, *args, **kwargs):
        self.req = kwargs.pop('req', None)
        super(change_mac_form, self).__init__(*args, **kwargs)
        form_fields = ['new_mac']
        for field in form_fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })

        self.fields['new_mac'].widget.attrs.update({
            'placeholder': "xx:xx:xx:xx:xx:xx"
        })
        self.fields['reason'].widget.attrs.update({
            'placeholder': "Specify a clear reason for changing MAC"
        })

    def clean_new_mac(self):
        cd = self.cleaned_data
        new_mac = cd.get('new_mac')
        if not re.match(r'^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$', new_mac):
            raise forms.ValidationError("Enter valid MAC id")
        number_occurrences = wifi.objects.filter(mac_id=new_mac).count()
        if number_occurrences > 0:
            raise forms.ValidationError("This MAC id is already been registered")

        conn_obj = connect_ldap_server(self.req)
        if not conn_obj:
            raise forms.ValidationError("Unable to access wifi server.Contact Network admin")
        if get_wifi_domiain(self.req) == 'sid.gct.com':
            dc = 'dc=sid,dc=gct,dc=com'
        else:
            dc = 'dc=cse,dc=gct,dc=com'

        caller_id = new_mac.replace(':', '')
        result = conn_obj.search('ou=GCT,' + dc,
                                 '(&(objectclass=person)(msNPCallingStationID=' + caller_id + '))',
                                 attributes=['sAMAccountName'])
        print(result)
        list_of_same_mac_users = []
        for entry in conn_obj.entries:
            json_entry_string = entry.entry_to_json()
            json_entry = json.loads(json_entry_string)
            attr = json_entry['attributes']
            temp_dict = {}
            temp_dict['dn'] = json_entry['dn']
            print(temp_dict['dn'])
            if len(attr['sAMAccountName']) > 0:
                temp_dict['sAMAccountName'] = attr['sAMAccountName'][0]
            list_of_same_mac_users.append(temp_dict)
        conn_obj.unbind()

        if len(list_of_same_mac_users) > 1:
            raise forms.ValidationError("This mac id is been registered my many users.Please report Network Admin")
        elif len(list_of_same_mac_users) == 1:
            user = list_of_same_mac_users[0]
            raise forms.ValidationError("MAC id already registered by user " + user['sAMAccountName'])

        return new_mac
