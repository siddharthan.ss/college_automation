from lxml import etree

from accounts.models import department, regulation
from curriculum.models import courses

root = etree.parse('doc.xml')

tree = root.xpath('/course_list')[0]

dept = tree.get('department')
programme = tree.get('programme')
st_yr = tree.get('regulation_start_year')
ed_yr = tree.get('regulation_end_year')

print(dept + programme + st_yr + ed_yr)

dept_inst = department.objects.get(acronym=dept)
reg_inst = regulation.objects.filter(start_year=st_yr).get(end_year=ed_yr)

for course in root.xpath('//course'):
    id = course.get('id')
    semester = course.get('semester')
    subject_type = course.get('subject_type')
    is_elective = course.get('is_elective')
    if is_elective == 'Industrial':
        is_elective = True
    is_open = False

    name = course.find('course_name').text
    internal = course.find('internal').text
    external = course.find('external').text
    l = course.find('L').text
    t = course.find('T').text
    p = course.find('P').text
    c = course.find('C').text

    course_inst = courses.objects.get_or_create(
        department=dept_inst,
        semester=semester,
        course_id=id,
        course_name=name,
        subject_type=subject_type,
        CAT='BS',
        CAT_marks=int(internal),
        end_semester_marks=int(external),
        L_credits=int(l),
        T_credits=int(t),
        P_credits=int(p),
        C_credits=int(c),
        programme=programme,
        is_elective=is_elective,
        is_open=is_open,
        regulation=reg_inst,
    )

    print('Created course ' + str(course_inst))
