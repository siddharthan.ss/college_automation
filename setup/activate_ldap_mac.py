import json
import ssl

from ldap3 import Tls, Server, Connection, ALL

from accounts.models import site_settings

IP_KEY = 'GCT_WIFI_LDAP_SERVER_ADDR'
ADMIN_DN_KEY = 'GCT_ADMIN_DN'
ADMIN_PASSWORD_KEY = 'GCT_ADMIN_PASSWORD'
DOMAIN_KEY = 'GCT_WIFI_DOMAIN'

ip_obj = site_settings.objects.get(key=IP_KEY)
WIFI_LDAP_SERVER_ADDR = ip_obj.value
admin_dn_obj = site_settings.objects.get(key=ADMIN_DN_KEY)
ADMIN_DN = admin_dn_obj.value
password_obj = site_settings.objects.get(key=ADMIN_PASSWORD_KEY)
ADMIN_PASSWORD = password_obj.value
print(locals())

tls_configuration = Tls(validate=ssl.CERT_NONE, version=ssl.PROTOCOL_TLSv1)

server = Server(WIFI_LDAP_SERVER_ADDR, use_ssl=True, tls=tls_configuration, get_info=ALL)

conn_obj = Connection(server, ADMIN_DN, ADMIN_PASSWORD, auto_bind=True)
print(conn_obj)
if conn_obj:
    print('Connection Successfull')

    dc = 'dc=cse,dc=gct,dc=com'

    conn_obj.search('ou=GCT,' + dc,
                    '(&(objectclass=person)(userAccountControl=66050))',
                    attributes=['cn', 'userPrincipalName', 'description', 'sAMAccountName'])

    invalid_mac_user_list = []
    for entry in conn_obj.entries:
        json_entry_string = entry.entry_to_json()
        json_entry = json.loads(json_entry_string)

        print(json_entry['dn'])
else:
    print('Unsuccessfull connection')
