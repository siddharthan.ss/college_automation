from curriculum.models import staff_course

for each in staff_course.objects.all():
    staff_obj = each.staff
    each.staffs.add(staff_obj)
    each.save()
    print(str(each.course) + ' ' + str(each.staff) + 'added to m2m')
