from accounts.models import student, department

dept_inst = department.objects.get(acronym="CIVIL")

for entry in student.objects.filter(department=dept_inst).filter(batch__programme="PG"):
    print(entry.roll_no + " - " + str(entry))
    entry.user.is_approved = False
    entry.user.save()