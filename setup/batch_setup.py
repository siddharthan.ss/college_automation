from accounts.models import batch, regulation

regulation.objects.all().delete()
regulation_inst_old = regulation.objects.create(
    start_year=2012,
    end_year=2015,
)
regulation_inst_new = regulation.objects.create(
    start_year=2016,
    end_year=2019,
)

batch.objects.all().delete()

batch_list = (
    ('2013-06-26', 2013, 2017, 'UG'),
    ('2014-06-26', 2014, 2018, 'UG'),
    ('2015-06-26', 2015, 2019, 'UG'),
)

for entry in batch_list:
    batch.objects.create(
        start_year=entry[1],
        end_year=entry[2],
        programme=entry[3],
        regulation=regulation_inst_old,
    )
    print('Created ' + str(entry[1]) + ' - ' + str(entry[2]))

batch_list = (
    ('2016-06-26', 2016, 2020, 'UG'),
    ('2015-06-26', 2015, 2017, 'PG'),
    ('2016-06-26', 2016, 2018, 'PG'),
    ('2015-06-26', 2015, 2017, 'Phd'),
    ('2016-06-26', 2016, 2018, 'Phd'),
)

for entry in batch_list:
    batch.objects.create(
        start_year=entry[1],
        end_year=entry[2],
        programme=entry[3],
        regulation=regulation_inst_new,
    )
    print('Created ' + str(entry[1]) + ' - ' + str(entry[2]))
