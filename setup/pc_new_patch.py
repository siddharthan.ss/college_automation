from django.contrib.auth.models import Group

from accounts.models import department, staff, department_programmes
from curriculum.models import programme_coordinator, programme

g = Group.objects.get(name="programme_coordinator")
programme_inst = programme.objects.get(acronym="UG")
for dept in department.objects.filter(is_core=True):
    for staff_inst in staff.objects.filter(department=dept):
        if staff_inst.user.groups.filter(name="programme_coordinator").exists():
            print(dept.acronym)
            dept_programme_inst = department_programmes.objects.get(
                department=dept,
                programme__acronym="UG"
            )
            if not programme_coordinator.objects.filter(department_programme = dept_programme_inst).exists():
                pc_inst = programme_coordinator.objects.create(
                    department_programme = dept_programme_inst
                )
                pc_inst.programme_coordinator_staffs.add(staff_inst)
                pc_inst.save()
                print(pc_inst)
