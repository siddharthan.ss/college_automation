from accounts.models import hours

hour_list = (
    (1, '1st hour'),
    (2, '2nd hour'),
    (3, '3rd hour'),
    (4, '4th hour'),
    (5, '5th hour'),
    (6, '6th hour'),
    (7, '7th hour'),
    (8, '8th hour')
)

for entry in hour_list:
    if hours.objects.filter(hour=entry[0]):
        print('Hour exists' + str(entry[0]))
    else:
        hours.objects.create(
            hour=entry[0],
            hour_name=entry[1]
        )
        print('Created hour ' + str(entry[0]))
