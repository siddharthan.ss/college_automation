from accounts.models import department

department.objects.all().delete()

departments = (
    ('Computer Science and Engineering', 'CSE'),
    ('Information Technology', 'IT'),
    ('Mechanical Engineering', 'MECH'),
    ('Production Engineering', 'PROD'),
    ('Chemistry', 'CHE'),
    ('Physics', 'PHY'),
    ('Maths', 'MAT'),
    ('Hostel', 'HOSTE'),
    ('Civil', 'CIVIL'),
    ('Electrical', 'EEE'),
    ('Electronics', 'ECE'),
    ('Instrumentation', 'EIE'),
    ('bio-tech', 'IBT'),
)

for dept in departments:
    if dept[1] == "CHE" or dept[1] == "HOSTEL" or dept[1] == "PHY" or dept[1] == "MAT":
        department.objects.get_or_create(name=dept[0], acronym=dept[1], is_core=False)
    else:
        department.objects.get_or_create(name=dept[0], acronym=dept[1], is_core=True)
    print('Created department ' + dept[1])
