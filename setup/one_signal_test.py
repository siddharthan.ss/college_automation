from one_signal_sdk.notification_handler import handle_user_notification
from accounts.models import CustomUser
from django.conf import settings
from django.urls import reverse



customUserInstance = CustomUser.objects.get(email='dummystaff@gmail.com')


handle_user_notification(
    contents = 'hello testing 2 content',
    heading= 'test heading',
    custom_user_instances=customUserInstance,
    url=settings.CURRENT_HOST_NAME[:-1] + reverse('viewprofile'),
)