from curriculum.models import attendance, semester_migration
from curriculum.views.common_includes import get_list_of_students, get_filtered_students_for_lab

list_of_active_semsters = []

for entry in semester_migration.objects.filter(semester__semester_number=3):
    list_of_active_semsters.append(entry.semester)

count = 0
overall_counter = 0
for att_entry in attendance.objects.filter(semester__in = list_of_active_semsters,
                                           course__subject_type="P"):
    if not att_entry.date.isoweekday() in range(1, 6):
        print("Saturday")
    eligible_students_list = get_list_of_students(att_entry.semester, att_entry.course)
    eligible_students_queryset_list = []
    b, filtered_list = get_filtered_students_for_lab(att_entry.semester,eligible_students_list,att_entry.course,att_entry.date)
    for fil_entry in filtered_list:
        eligible_students_queryset_list.append(fil_entry['student_obj'])
    for o in att_entry.onduty_students.all():
        if o not in eligible_students_queryset_list:
            print(str(att_entry.course) + ' - ' + str(o) + ' - ' + str(att_entry.date) +  ' - ' + str(att_entry.hour))
            count += 1

    for p in att_entry.present_students.all():
        if p not in eligible_students_queryset_list:
            print(str(att_entry.course) + ' - ' + str(o) + ' - ' + str(att_entry.date) +  ' - ' + str(att_entry.hour))
            count += 1

    for a in att_entry.absent_students.all():
        if a not in eligible_students_queryset_list:
            print(str(att_entry.course) + ' - ' + str(o) + ' - ' + str(att_entry.date) +  ' - ' + str(att_entry.hour))
            count += 1

    print(".", end='')
    overall_counter += 1
    if overall_counter % 50 == 0:
        print(overall_counter)

print('count = '  + str(count))

