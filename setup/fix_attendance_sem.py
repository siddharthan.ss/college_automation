from curriculum.models import courses, attendance

first_sem_ug_courses = courses.objects.filter(
    semester=1
).filter(
    programme='UG'
)

first_sem_pg_courses = courses.objects.filter(
    semester=1
).filter(
    programme='PG'
)

count = 0
for entry in attendance.objects.all():
    if entry.course in first_sem_pg_courses:
        entry.semester_id = 2
        entry.save()
        print(entry.course_id + 'Updated')
        count += 1
