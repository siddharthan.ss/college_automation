from curriculum.models import attendance, flexi_attendance, semester_migration

min_date = None
for entry in semester_migration.objects.all():
    if min_date is None:
        min_date = entry.semester.start_term_1
    else:
        if entry.semester.start_term_1 < min_date:
            min_date = entry.semester.star

count = 0
for flexi_entry in flexi_attendance.objects.filter(attendance_date__gte = min_date,is_marked = False).order_by('attendance_date'):
    if attendance.objects.filter(
        date = flexi_entry.attendance_date,
        hour = flexi_entry.attendance_hour,
        course = flexi_entry.marked_course,
        semester = flexi_entry.semester,
    ).exists():
        if not flexi_attendance.objects.filter(
            attendance_date=flexi_entry.attendance_date,
            attendance_hour=flexi_entry.attendance_hour,
            marked_course=flexi_entry.marked_course,
            semester=flexi_entry.semester,
            is_marked = True
        ).exists():
            print(vars(flexi_entry))
            print()
            count+=1

print(count)