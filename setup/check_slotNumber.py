from datetime import datetime

from common.utils.slotNumberUtil import getSlotNumber, is_weekend
from curriculum.models import timeTableSlot, day

for entry in timeTableSlot.objects.all():
    if is_weekend(entry.date):
        entry.delete()
    print(str(entry))
    print(" New calc:" + str(getSlotNumber(entry.semester.start_term_1, entry.date, entry.semester.department)))
    if entry.date < datetime.strptime("18-12-2017","%d-%m-%Y").date():
        print(str(entry))
    entry.slotday = day.objects.get(slot_number=getSlotNumber(entry.semester.start_term_1, entry.date, entry.semester.department))
    entry.save()