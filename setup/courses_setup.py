from accounts.models import department, regulation
from curriculum.models import courses

courses.objects.all().delete()

courses_list = (
    (1, '16SHS1Z1', 'Communication skills in English', 'HS', 50, 50, 2, 2, 0, 3, 1, 'UG', 'T'),
    (1, '16SBS1Z2', 'Engineering Mathematics-I', 'BS', 50, 50, 3, 2, 0, 4, 1, 'UG', 'T'),
    (1, '16SBS103', 'Engineering Physics', 'BS', 50, 50, 3, 0, 0, 3, 1, 'UG', 'T'),
    (1, '16SBS104', 'Applied Chemistry', 'BS', 50, 50, 3, 0, 0, 3, 1, 'UG', 'T'),
    (1, '16SES105', 'Fundamentals of Electrical and Electronics Engineering', 'ES', 50, 50, 3, 0, 0, 3, 1, 'UG', 'T'),
    (1, '16SBS106', 'Chemistry Laboratory', 'BS', 50, 50, 0, 0, 4, 2, 1, 'UG', 'P'),
    (1, '16SES107', 'Workshop Practice', 'ES', 50, 50, 0, 0, 4, 2, 1, 'UG', 'P'),
    (2, '16SHS2Z1', 'Technical English', 'HS', 50, 50, 2, 2, 0, 3, 1, 'UG', 'T'),
    (2, '16SBS2Z2', 'Engineering Mathematics-II', 'BS', 50, 50, 3, 2, 0, 4, 1, 'UG', 'T'),
    (2, '16SBS2Z3', 'Materials Science', 'BS', 50, 50, 3, 0, 0, 3, 1, 'UG', 'T'),
    (2, '16SHS2Z4', 'Environmental Science and Engineering', 'HS', 50, 50, 3, 0, 0, 3, 1, 'UG', 'T'),
    (2, '16SES2Z5', 'Programming in C', 'ES', 50, 50, 3, 0, 0, 3, 1, 'UG', 'T'),
    (2, '16SBS206', 'Physics Laboratory', 'BS', 50, 50, 0, 0, 4, 2, 1, 'UG', 'P'),
    (2, '16SES207', 'Engineering Graphics', 'ES', 50, 50, 2, 0, 4, 4, 1, 'UG', 'P'),
    (2, '16SES2Z8', 'Programming in C Laboratory', 'ES', 50, 50, 0, 0, 4, 2, 1, 'UG', 'P'),
    (1, '17SES2Z8', 'Programming in C Laboratory', 'ES', 50, 50, 0, 0, 4, 2, 1, 'PG', 'P'),
)

courses_list_elective_cse = (
    (2, '17SEE2Z8', 'Elective1CSE', 'ES', 50, 50, 0, 0, 4, 2, 1, 'UG', 'T'),
    (2, '17SEE3Z8', 'Elective2CSE', 'ES', 50, 50, 0, 0, 4, 2, 1, 'UG', 'T'),
)

courses_list_open_it = (
    (1, '17SEE4Z8', 'Elective1IT', 'ES', 50, 50, 0, 0, 4, 2, 1, 'UG', 'T'),
    (1, '17SEE5Z8', 'Elective2IT', 'ES', 50, 50, 0, 0, 4, 2, 1, 'UG', 'T'),
)

regulation_inst = regulation.objects.get(start_year=2016)

print('created regulation ' + str(regulation_inst))
for course in courses_list:
    department_obj = department.objects.all().get(name='Computer Science and Engineering')
    courses.objects.create(semester=course[0],
                           course_id=course[1],
                           course_name=course[2],
                           CAT=course[3],
                           CAT_marks=course[4],
                           end_semester_marks=course[5],
                           L_credits=course[6],
                           T_credits=course[7],
                           P_credits=course[8],
                           C_credits=course[9],
                           department=department_obj,
                           programme=course[11],
                           regulation=regulation_inst,
                           subject_type=course[12]
                           )
    print('Created course ' + course[2])

for course in courses_list_elective_cse:
    department_obj = department.objects.all().get(name='Computer Science and Engineering')
    courses.objects.create(semester=course[0],
                           course_id=course[1],
                           course_name=course[2],
                           CAT=course[3],
                           CAT_marks=course[4],
                           end_semester_marks=course[5],
                           L_credits=course[6],
                           T_credits=course[7],
                           P_credits=course[8],
                           C_credits=course[9],
                           department=department_obj,
                           programme=course[11],
                           is_elective=True,
                           is_open=True,
                           regulation=regulation_inst,
                           subject_type=course[12]
                           )
    print('Created course ' + course[2])

for course in courses_list_open_it:
    department_obj = department.objects.all().get(name='Information Technology')
    courses.objects.create(semester=course[0],
                           course_id=course[1],
                           course_name=course[2],
                           CAT=course[3],
                           CAT_marks=course[4],
                           end_semester_marks=course[5],
                           L_credits=course[6],
                           T_credits=course[7],
                           P_credits=course[8],
                           C_credits=course[9],
                           department=department_obj,
                           programme=course[11],
                           is_elective=True,
                           is_open=True,
                           regulation=regulation_inst,
                           subject_type=course[12]
                           )
    print('Created course ' + course[2])
