from curriculum.models import hod, department, programme_coordinator, department_programmes, programme
from accounts.models import staff

departmentAcronym = 'CSE'
departmentInstance = department.objects.get(acronym=departmentAcronym)
staffInstance = staff.objects.get(user__email='hod@gmail.com')

if hod.objects.filter(department=departmentInstance):
    hodInstance = hod.objects.get(department=departmentInstance)
    hodInstance.staff = staffInstance
    hodInstance.save()
else:
    hodInstance = hod(department=departmentInstance, staff=staffInstance)
    hodInstance.save()

print('Allotted hod of cse as HOD')


programmeInstance = programme.objects.get(acronym='UG')
departmentProgrammeInstance = department_programmes.objects.get(department=departmentInstance, programme=programmeInstance)
if programme_coordinator.objects.filter(department_programme=departmentProgrammeInstance):
    programmeCoordinatorInstance = programme_coordinator.objects.get(department_programme=departmentProgrammeInstance)
    programmeCoordinatorInstance.save()
    programmeCoordinatorInstance.programme_coordinator_staffs.add(staffInstance)
    programmeCoordinatorInstance.save()
else:
    programmeCoordinatorInstance = programme_coordinator(department_programme=departmentProgrammeInstance)
    programmeCoordinatorInstance.save()
    programmeCoordinatorInstance.programme_coordinator_staffs.add(staffInstance)
    programmeCoordinatorInstance.save()

print('Allotted hod of cse as PC for UG')

programmeInstance = programme.objects.get(acronym='PG')
departmentProgrammeInstance = department_programmes.objects.get(department=departmentInstance, programme=programmeInstance)
if programme_coordinator.objects.filter(department_programme=departmentProgrammeInstance):
    programmeCoordinatorInstance = programme_coordinator.objects.get(department_programme=departmentProgrammeInstance)
    programmeCoordinatorInstance.save()
    programmeCoordinatorInstance.programme_coordinator_staffs.add(staffInstance)
    programmeCoordinatorInstance.save()
else:
    programmeCoordinatorInstance = programme_coordinator(department_programme=departmentProgrammeInstance)
    programmeCoordinatorInstance.save()
    programmeCoordinatorInstance.programme_coordinator_staffs.add(staffInstance)
    programmeCoordinatorInstance.save()

print('Allotted hod of cse as PC for PG')