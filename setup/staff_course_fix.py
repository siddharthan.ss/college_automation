from curriculum.models import staff_course

staff_course_instances = staff_course.objects.all()

print(staff_course_instances)

for inst in staff_course_instances:
    staff_instance = getattr(inst, 'staff')
    dept_instance = getattr(staff_instance, 'department')
    inst.department = dept_instance
    inst.save()
