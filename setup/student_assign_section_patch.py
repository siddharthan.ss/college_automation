from accounts.models import student
from curriculum.models import section_students, department_sections

for stud in student.objects.all():
    corresponding_dept_section = department_sections.objects.filter(
        department=stud.department
    ).get(
        batch=stud.batch
    )

    created_inst = section_students.objects.get_or_create(
        student=stud,
        section=corresponding_dept_section
    )
    print(str(stud) + ' ')
    print(created_inst)

    if created_inst[1] == False:
        print('something fishy')
