from accounts.models import programme, department, department_programmes

programmes = (
    ('Under Graduate', 'UG'),
    ('Post Graduate', 'PG'),
)

for each in programmes:
    print('Created ' + str(each[0]) + ' : ' + str(programme.objects.get_or_create(name=each[0], acronym=each[1])))

for dept in department.objects.filter(is_core=True):
    for prog in programmes:
        programme_instance = programme.objects.get(acronym=prog[1])
        print('Created ' + str(prog[1]) + ' for ' + str(dept.acronym) + ' : ' + str(department_programmes.objects.get_or_create(department=dept, programme=programme_instance)))
