from curriculum.models import programme_coordinator
from accounts.models import staff, department

du_inst = staff.objects.get(first_name="Dummy")

for pc_inst in programme_coordinator.objects.filter(programme_coordinator_staffs=du_inst):
    pc_inst.programme_coordinator_staffs.remove(du_inst)
    pc_inst.save()

dept_acronym = "EIE"
prg_acronym= "UG"

dept_inst = department.objects.get(acronym=dept_acronym)
du_inst.department = dept_inst
du_inst.save()

pc_instance = programme_coordinator.objects.get(
    department_programme__department__acronym=dept_acronym,
    department_programme__programme__acronym=prg_acronym
)

pc_instance.programme_coordinator_staffs.add(du_inst)
pc_instance.save()