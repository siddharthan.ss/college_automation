from accounts.models import active_batches
from curriculum.models import department_sections

for entry in active_batches.objects.all():
    batch_inst = entry.batch
    dept_inst = entry.department
    section_name = 'A'

    created_inst = department_sections.objects.get_or_create(
        department=dept_inst,
        batch=batch_inst,
        section_name=section_name
    )
    print(created_inst)

    if created_inst[1] == False:
        print('something fishy')
