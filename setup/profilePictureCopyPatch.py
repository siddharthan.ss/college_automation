from accounts.models import CustomUser, staff, student

counter = 0
staffPictureCounter = 0
for account in staff.objects.all():
    if account.avatar:
        user_instance = CustomUser.objects.get(email=account.user.email)
        user_instance.avatar = account.avatar
        user_instance.save()
        staffPictureCounter = staffPictureCounter + 1
        # print('Staff Copied : ' + str(account))
    counter = counter + 1
    print('staffCounter:' + str(counter))

counter = 0
studentPictureCounter = 0
for account in student.objects.all():
    if account.avatar:
        user_instance = CustomUser.objects.get(email=account.user.email)
        user_instance.avatar = account.avatar
        user_instance.save()
        studentPictureCounter = studentPictureCounter + 1
        # print('Student Copied : ' + str(account))
        counter = counter + 1
        print('StudentCounter:' + str(counter))

staffCount = staff.objects.all().count()
studentCount = student.objects.all().count()

print()
print('staffCount:' + str(staffCount))
print('staffPictureCounter:' + str(staffPictureCounter))
print('studentCount:' + str(studentCount))
print('studentPictureCounter:' + str(studentPictureCounter))
