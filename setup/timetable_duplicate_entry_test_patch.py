from curriculum.models import time_table

time_table_list = time_table.objects.all()

counter = 0

list_of_viewed_pks = []

for tt_entry in time_table_list:
    got_start_date = datetime.strptime(str(tt_entry.created_date), "%Y-%m-%d")
    got_end_date = datetime.strptime(str(tt_entry.modified_date), "%Y-%m-%d")
    for each in time_table.objects.filter(semester=tt_entry.semester, day=tt_entry.day, hour=tt_entry.hour,
                                          course=tt_entry.course).exclude(pk=tt_entry.pk):
        if not tt_entry.pk in list_of_viewed_pks:
            start_date = datetime.strptime(str(each.created_date), "%Y-%m-%d")
            take_date = datetime.strptime(str(each.modified_date), "%Y-%m-%d")
            end_date = take_date - timedelta(1)

            crash = False

            if tt_entry.is_active:
                if start_date == got_start_date:
                    crash = True

            if start_date <= got_start_date <= end_date:
                crash = True

            if start_date <= got_end_date <= end_date:
                crash = True

            if crash:
                print()
                print('course:' + str(tt_entry.course) + ' || hour:' + str(tt_entry.hour) + ' || day:' + str(
                    tt_entry.day.day_name) + ' || department:' + str(tt_entry.department.acronym) + ' || semester:' + str(
                    tt_entry.semester) + ' || batch:' + str(tt_entry.batch))
                print('active_date: pk:' + str(tt_entry.pk))

            print('inactive_date: pk:' + str(each.pk))
            print()
            list_of_viewed_pks.append(each.pk)
            counter = counter + 1

print('total_entries_found:' + str(counter))
