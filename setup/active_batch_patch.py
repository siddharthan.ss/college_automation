from accounts.models import batch, active_batches
from accounts.models import department, department_programmes

pg_depts = department_programmes.objects.filter(programme__acronym="PG")

batch_insts = batch.objects.filter(
    start_year__gte=2017,
    programme="PG",
)
print((batch_insts))

for dept_prg in pg_depts:
    for bat_inst in batch_insts:
        active_batches_inst = active_batches.objects.get_or_create(
            batch = bat_inst,
            department = dept_prg.department,
            programme = dept_prg.programme.acronym,
            current_semester_number_of_this_batch=1
        )
        print(active_batches_inst)
