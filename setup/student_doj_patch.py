from datetime import datetime

from accounts.models import student

date = '23-01-2017'

date_instance = datetime.strptime(date, "%d-%m-%Y").date()

count = 0
for entry in student.objects.filter(date_of_joining__gt=date_instance):
    f = open("fake_doj_prod.txt", "a+");
    f.write(entry.roll_no)
    f.write("\r\n")

    entry.date_of_joining = date_instance
    entry.save()

    count = count + 1

    print(entry.roll_no)

print('count= ' + str(count))
