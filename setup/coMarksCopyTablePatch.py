from curriculum.models import staff_course
from marks.models import paper_pattern, co_marks, co_weightage
from courseOutcomes.models import CoPattern, CoMarks, CoWeightage

patternCounter = 0
totalCoCount = 0
for entry in paper_pattern.objects.all():

    staffCourseInstance = staff_course.objects.get(semester=entry.semester, course=entry.course)
    coPatternInstance = CoPattern(staffCourse=staffCourseInstance, unitTest=entry.ut_num,
                                                        questionNumber=entry.question_number,
                                                        userPrespectiveQuestionNumber=entry.user_prespective_question_number,
                                                        coMapped=entry.co_mapping, maximumMark=entry.marks)
    coPatternInstance.save()
    coMarksCounter = 0
    for coMarksEntry in co_marks.objects.filter(paper_pattern_ref=entry):
        coMarksInstance = CoMarks(coPattern=coPatternInstance, student=coMarksEntry.student,
                                                        markObtained=coMarksEntry.obtained_marks)
        coMarksInstance.save()
        coMarksCounter = coMarksCounter + 1
        totalCoCount = totalCoCount + 1

    patternCounter = patternCounter + 1

    print('Pattern:' + str(patternCounter) + ' --> ' + str(coMarksCounter))

print('totalCoCount:' + str(totalCoCount))

coWeightageCounter = 0
for entry in co_weightage.objects.all():
    coWeightageInstance = CoWeightage(staffCourse=entry.staff_course, unitTest=entry.ut_num,
                                                            coMapped=entry.co_mapping,
                                                            weightage=entry.weightage)
    coWeightageInstance.save()
    coWeightageCounter = coWeightageCounter + 1

    print('Weightage:' + str(coWeightageCounter))

coPatternOld = paper_pattern.objects.all().count()
coPatternNew = CoPattern.objects.all().count()
coMarksOld = co_marks.objects.all().count()
coMarksNew = CoMarks.objects.all().count()
coWeightageOld = co_weightage.objects.all().count()
coWeightageNew = CoWeightage.objects.all().count()

print('coPatternOld:' + str(coPatternOld))
print('coMarksOld:' + str(coMarksOld))
print('coWeightageOld:' + str(coWeightageOld))
print('coPatternNew:' + str(coPatternNew))
print('coMarksNew:' + str(coMarksNew))
print('coWeightageNew:' + str(coWeightageNew))
