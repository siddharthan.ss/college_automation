from accounts.models import student

file = open('fake_doj_prod.txt')

# print(file.read())
#
# for l in file.readlines():
#     print(l.strip().split("\r\n")[0])


import csv

with open('fake_doj_list.csv', 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(['Roll Number', 'Name', 'department', 'current semester', 'date of joining'])
    for l in file.readlines():
        roll_no = l.strip().split("\r\n")[0]
        student_instance = student.objects.get(roll_no=roll_no)
        print(student_instance.roll_no)
        spamwriter.writerow([str(student_instance.roll_no), str(student_instance),
                             str(student_instance.department.acronym), str(student_instance.current_semester),
                             str(student_instance.date_of_joining)])
