from datetime import datetime

from accounts.models import CustomUser
from messaging.models import notifications

# notifications.objects.all().delete()

list = (
    ('hod@gmail.com', 'helloallu wanna get this ticket all is well', 'False', 'False'),
    ('hod@gmail.com', 'hello', 'False', 'False')
)

for each in list:
    receiver = CustomUser.objects.get(email=each[0])
    notifications.objects.create(recipient=receiver, content=each[1], date=datetime.now(), is_read=each[2],
                                 is_action=each[3])
    print('Created ' + str(each[1]))
