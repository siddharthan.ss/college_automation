from curriculum.models import staff_course
from quiz.models import quiz_timings, quiz_questions, attend_quiz_timings, attend_quiz_answers
from quiz.models import QuizTime, QuizQuestion, WrapQuizTime, WrapQuizQuestion

quizTimeCounter = 0
quizQuestionCounter = 0
wrapQuizTimeCounter = 0
wrapQuizQuestionCounter = 0

for quizTimeEntry in quiz_timings.objects.all():
    staffCourseInstance = staff_course.objects.get(semester=quizTimeEntry.semester, course=quizTimeEntry.course)
    quizTimeInstance = QuizTime(staffCourse=staffCourseInstance, unitTest=quizTimeEntry.ut_num,
                                startTime=quizTimeEntry.start_time,
                                endTime=quizTimeEntry.end_time, timeDuration=quizTimeEntry.time_to_complete,
                                isActive=quizTimeEntry.is_active)
    quizTimeInstance.save()

    quizTimeCounter = quizTimeCounter + 1

    for quizQuestionEntry in quiz_questions.objects.filter(quiz_timings_ref=quizTimeEntry):
        quizQuestionInstance = QuizQuestion(quizTime=quizTimeInstance, questionNumber=quizQuestionEntry.question_number,
                                            totalOptions=quizQuestionEntry.options_count,
                                            question=quizQuestionEntry.question, option1=quizQuestionEntry.option1,
                                            option2=quizQuestionEntry.option2, option3=quizQuestionEntry.option3,
                                            option4=quizQuestionEntry.option4, option5=quizQuestionEntry.option5,
                                            rightAnswer=quizQuestionEntry.answer, markAllotted=quizQuestionEntry.marks)
        quizQuestionInstance.save()

        quizQuestionCounter = quizQuestionCounter + 1

    for wrapQuizTimeEntry in attend_quiz_timings.objects.filter(quiz_timings_ref=quizTimeEntry):
        wrapQuizTimeInstance = WrapQuizTime(quizTime=quizTimeInstance, student=wrapQuizTimeEntry.student,
                                            startTime=wrapQuizTimeEntry.start_time, endTime=wrapQuizTimeEntry.end_time,
                                            isWrapped=wrapQuizTimeEntry.complete_test)
        wrapQuizTimeInstance.save()

        wrapQuizTimeCounter = wrapQuizTimeCounter + 1

        for entry in quiz_questions.objects.filter(quiz_timings_ref=quizTimeEntry).order_by('question_number'):
            for wrapQuizQuestionEntry in attend_quiz_answers.objects.filter(attend_quiz_timings_ref=wrapQuizTimeEntry,
                                                                            quiz_questions_ref=entry):
                if attend_quiz_answers.objects.filter(attend_quiz_timings_ref=wrapQuizTimeEntry,
                                                      quiz_questions_ref=entry):
                    quizQuestionInstance = QuizQuestion.objects.get(quizTime=quizTimeInstance,
                                                                    questionNumber=entry.question_number)

                    wrapQuizQuestionInstance = WrapQuizQuestion(wrapQuizTime=wrapQuizTimeInstance,
                                                                quizQuestion=quizQuestionInstance,
                                                                givenAnswer=wrapQuizQuestionEntry.chosen_answer,
                                                                isCorrect=wrapQuizQuestionEntry.is_correct)
                    wrapQuizQuestionInstance.save()

                    wrapQuizQuestionCounter = wrapQuizQuestionCounter + 1

    print('Counter: ' + str(quizTimeCounter) + ' --> ' + str(quizQuestionCounter) + ' --> ' + str(
        wrapQuizTimeCounter) + ' --> ' + str(wrapQuizQuestionCounter))

print('quizTimeOld: ' + str(quiz_timings.objects.all().count()))
print('quizQuestionOld: ' + str(quiz_questions.objects.all().count()))
print('wrapQuizTimeOld: ' + str(attend_quiz_timings.objects.all().count()))
print('wrapQuizQuestionOld: ' + str(attend_quiz_answers.objects.all().count()))

print('quizTimeNew: ' + str(QuizTime.objects.all().count()))
print('quizQuestionNew: ' + str(QuizQuestion.objects.all().count()))
print('wrapQuizTimeNew: ' + str(WrapQuizTime.objects.all().count()))
print('wrapQuizQuestionNew: ' + str(WrapQuizQuestion.objects.all().count()))
