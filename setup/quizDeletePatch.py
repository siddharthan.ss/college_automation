from curriculum.models import staff_course
from quiz.models import quiz_timings, quiz_questions, attend_quiz_timings, attend_quiz_answers
from quiz.models import QuizTime, QuizQuestion, WrapQuizTime, WrapQuizQuestion

quizTimeCounter = 0
quizQuestionCounter = 0
wrapQuizTimeCounter = 0
wrapQuizQuestionCounter = 0

print('quizTimeNew: ' + str(QuizTime.objects.all().delete()))
print('quizQuestionNew: ' + str(QuizQuestion.objects.all().delete()))
print('wrapQuizTimeNew: ' + str(WrapQuizTime.objects.all().delete()))
print('wrapQuizQuestionNew: ' + str(WrapQuizQuestion.objects.all().delete()))

