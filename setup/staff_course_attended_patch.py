from accounts.models import staff_course_attended

for entry in staff_course_attended.objects.all():
    if not entry.end_date:
        staff_course_attended_instance = staff_course_attended.objects.get(pk=entry.pk)
        staff_course_attended_instance.end_date = staff_course_attended_instance.date
        staff_course_attended_instance.save()
        print('Created for '+str(entry.staff))
    else:
        print('End date exists for ' + str(entry.staff))
