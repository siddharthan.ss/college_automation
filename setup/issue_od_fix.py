from accounts.models import issue_onduty
from curriculum.models import attendance

count = 0
for entry in issue_onduty.objects.all():
    for att_entry in attendance.objects.filter(date=entry.date).filter(absent_students=entry.student):
        att_entry.absent_students.remove(entry.student)
        att_entry.onduty_students.add(entry.student)
        att_entry.save()
        count = count + 1

print(count)