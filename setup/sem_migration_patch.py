

from accounts.models import department, batch, active_batches

core_dept_list = ['CSE', 'IT', 'MECH', 'CIVIL', 'PROD', 'IBT', 'EEE', 'ECE', 'EIE']

for acronym in core_dept_list:
    try:
        dept_inst = department.objects.get(acronym=acronym)
        st_yr = 2013
        end_yr = 2016

        while (st_yr <= end_yr):
            batch_obj = batch.objects.filter(start_year=st_yr).get(programme='UG')

            no_of_years = 2017 - st_yr
            sem = no_of_years * 2

            active_batch_obj = active_batches.objects.get_or_create(
                batch=batch_obj,
                department=dept_inst,
                programme='UG',
                current_semester_number_of_this_batch=sem,
            )
            print('Created ' + str(active_batch_obj))
            st_yr += 1
        st_yr = 2015
        while (st_yr <= end_yr):
            batch_obj = batch.objects.filter(start_year=st_yr).get(programme='PG')

            no_of_years = 2017 - st_yr
            sem = no_of_years * 2 + 1

            active_batch_obj = active_batches.objects.get_or_create(
                batch=batch_obj,
                department=dept_inst,
                programme='PG',
                current_semester_number_of_this_batch=sem,
            )
            print('Created ' + str(active_batch_obj))
            st_yr += 1
        st_yr = 2015
        while (st_yr <= end_yr):
            batch_obj = batch.objects.filter(start_year=st_yr).get(programme='Phd')

            no_of_years = 2017 - st_yr
            sem = no_of_years * 2 + 1

            active_batch_obj = active_batches.objects.get_or_create(
                batch=batch_obj,
                department=dept_inst,
                programme='Phd',
                current_semester_number_of_this_batch=sem,
            )
            print('Created ' + str(active_batch_obj))
            st_yr += 1
    except department.DoesNotExist:
        print('Could not find dept ' + acronym)
