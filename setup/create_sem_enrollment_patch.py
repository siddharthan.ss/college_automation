from accounts.models import batch
from curriculum.models import department_sections
from curriculum.models import semester

batch_instance = batch.objects.filter(start_year=2016).get(end_year=2020)

for dept_section in department_sections.objects.filter(batch=batch_instance):
    if not semester.objects.filter(
            department=dept_section.department,
            department_section=dept_section,
            batch=batch_instance,
            semester_number=1
    ).exists():
        created_sem_instance = semester.objects.get_or_create(
            department=dept_section.department,
            department_section=dept_section,
            batch=batch_instance,
            semester_number=1,
        )

        print(created_sem_instance)

    if not semester.objects.filter(
            department=dept_section.department,
            department_section=dept_section,
            batch=batch_instance,
            semester_number=2
    ).exists():
        created_sem_instance = semester.objects.get_or_create(
            department=dept_section.department,
            department_section=dept_section,
            batch=batch_instance,
            semester_number=2,
        )

        print(created_sem_instance)
