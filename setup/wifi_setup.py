from accounts.models import site_settings
from wifi.models import allowed_no_of_accounts_group

# group setup for wifi
allowed_no_of_accounts_group.objects.get_or_create(
    group_name='student',
    no_of_accounts=1,
)
print('1 accounts created for group student')

allowed_no_of_accounts_group.objects.get_or_create(
    group_name='staff',
    no_of_accounts=1,
)
print('1 accounts created for group staff')

# wifi login credentials setup

created_obj = site_settings.objects.get_or_create(
    key='LOCAL_WIFI_LDAP_SERVER_ADDR',
    value='192.168.56.200'
)
print('Added setting ' + str(created_obj))
created_obj = site_settings.objects.get_or_create(
    key='LOCAL_ADMIN_DN',
    value='siddy@sid.gct.com'
)
print('Added setting ' + str(created_obj))
created_obj = site_settings.objects.get_or_create(
    key='LOCAL_ADMIN_PASSWORD',
    value='server#1234'
)
print('Added setting ' + str(created_obj))
created_obj = site_settings.objects.get_or_create(
    key='LOCAL_WIFI_DOMAIN',
    value='sid.gct.com'
)
print('Added setting ' + str(created_obj))
