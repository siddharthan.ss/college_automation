from accounts.models import staff

"""
for obj in CustomUser.objects.all():
    obj.has_filled_data = True
    obj.save()
    print(str(obj)  + 'done')"""

for obj in staff.objects.all():
    cust_obj = obj.user
    cust_obj.is_staff_account = True
    cust_obj.save()

    print(str(cust_obj) + ' done')
