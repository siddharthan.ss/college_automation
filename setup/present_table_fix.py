from accounts.models import student
from curriculum.models import attendance
from curriculum.models import courses

attendance_instances = attendance.objects.all()

for attendance_entry in attendance_instances:
    marked_course = getattr(attendance_entry, 'course_id')
    print('marked coursee = ' + str(marked_course))
    course_instance = courses.objects.get(course_id=marked_course)
    course_ug_pg = getattr(course_instance, 'programme')
    course_sem_number = getattr(course_instance, 'semester')

    absent_students_instances = attendance_entry.absent_students.all()
    all_student_instances = student.objects.filter(current_semester=course_sem_number).filter(
        qualification=course_ug_pg)
    for stud_entry in all_student_instances:
        if stud_entry not in absent_students_instances:
            attendance_entry.present_students.add(stud_entry)
            print(stud_entry)
