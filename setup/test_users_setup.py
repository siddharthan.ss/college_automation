from django.contrib.auth.models import Group
from django.utils import timezone

from accounts.models import student, staff, CustomUser, department, batch
from developers.models import DeveloperProfile

"""
student.objects.all().delete()
staff.objects.all().delete()
"""
CustomUser.objects.all().delete()
# CustomUser.objects.all().get(email='gct@gct.ac.in').delete()
CustomUser.objects.create_superuser(email='gct@gct.ac.in', password='server#1234', activation_key='abcd',
                                    key_expires=timezone.now())
CustomUser.objects.filter(email='gct@gct.ac.in').update(is_verified=True)
print('Created superuser gct@gct.ac.in')
list_of_staffs = (
    ('principal@gmail.com', '123456', '145281', 'Principal', 'of', 'college', 'M', '1968-02-02', 'CSE', 'Principal',
     'PG', 'ME', 'Talking', 'cbe', 'cbe', '9658741230'),
    ('hod@gmail.com', '123456', '145287', 'HOD', 'of', 'cse', 'M', '1968-02-02', 'CSE', 'Professor', 'PG', 'ME',
     'Talking', 'cbe', 'cbe', '9658741230'),
    ('ithod@gmail.com', '123456', '1452847', 'HOD', 'of', 'IT', 'M', '1968-02-02', 'IT', 'Professor', 'PG', 'ME',
     'Talking', 'cbe', 'cbe', '9658741230'),
    ('phyhod@gmail.com', '123456', '151548', 'HOD', 'of', 'Physics', 'M', '1968-02-02', 'PHY', 'Professor', 'PG', 'ME',
     'Talking', 'cbe', 'cbe', '9658741230'),
    ('mathod@gmail.com', '123456', '654654', 'HOD', 'of', 'Maths', 'M', '1968-02-02', 'MAT', 'Professor', 'PG', 'ME',
     'Talking', 'cbe', 'cbe', '9658741230'),
    ('mechhod@gmail.com', '123456', '45245', 'HOD', 'of', 'Mech', 'M', '1968-02-02', 'MECH', 'Professor', 'PG', 'ME',
     'Talking', 'cbe', 'cbe', '9658741230'),
    ('fa1@gmail.com', '123456', '145288', 'FA1', 'of', 'cse', 'M', '1968-02-02', 'CSE', 'Associate Professor', 'PG',
     'ME', 'Talking', 'cbe', 'cbe', '9658741236'),
    ('fa2@gmail.com', '123456', '145209', 'FA2', 'of', 'cse', 'M', '1968-02-02', 'CSE', 'Assistant Professor', 'PG',
     'ME', 'Talking', 'cbe', 'cbe', '9658741237'),
    ('f1@gmail.com', '123456', '145299', 'F1', 'of', 'cse', 'M', '1968-02-02', 'CSE', 'Assistant Professor', 'PG', 'ME',
     'Talking', 'cbe', 'cbe', '9658741239'),
    ('f2@gmail.com', '123456', '145294', 'F2', 'of', 'cse', 'M', '1968-02-02', 'CSE', 'Assistant Professor', 'PG', 'ME',
     'Talking', 'cbe', 'cbe', '9658741235'),
    (
        'itf1@gmail.com', '123456', '1452954', 'F1', 'of', 'it', 'M', '1968-02-02', 'IT', 'Assistant Professor', 'PG',
        'ME',
        'Talking', 'cbe', 'cbe', '9658741235'),
    (
        'itf2@gmail.com', '123456', '1452944', 'F2', 'of', 'it', 'M', '1968-02-02', 'IT', 'Assistant Professor', 'PG',
        'ME',
        'Talking', 'cbe', 'cbe', '9658741235'),
    ('net_admin@gmail.com', '123456', '1452945', 'Network', 'admin', 'cse', 'M', '1968-02-02', 'CSE', 'Network Admin',
     'UG', 'NA', 'NA', 'cbe', 'cbe', '9658741235'),
    ('hostel_admin@gmail.com', '123456', '14523445', 'Hostel', '', 'admin', 'M', '1968-02-02', 'CSE', 'Hostel Admin',
     'UG', 'NA', 'NA', 'cbe', 'cbe', '9658741235'),
    ('alumni_staff@gmail.com', '123456', '14523990', 'Alumni', '', 'staff', 'M', '1968-02-02', 'CSE', 'Alumni Staff',
     'UG', 'NA', 'NA', 'cbe', 'cbe', '9658741235'),

)

for entry in list_of_staffs:
    # CustomUser.objects.all().get(email=entry[0]).delete()
    user_obj = CustomUser.objects.create_user(email=entry[0],
                                              password=entry[1],
                                              is_staff_account=True,
                                              activation_key='abcd',
                                              key_expires=timezone.now()
                                              )
    print('Created staff object with mail ' + user_obj.email)
    setattr(user_obj, 'is_verified', True)
    department_obj = department.objects.get(acronym=entry[8])
    staff.objects.create(user=user_obj,
                         staff_id=entry[2],
                         first_name=entry[3],
                         middle_name=entry[4],
                         last_name=entry[5],
                         gender=entry[6],
                         dob=entry[7],
                         department=department_obj,
                         designation=entry[9],
                         qualification=entry[10],
                         degree=entry[11],
                         specialization=entry[12],
                         temporary_address=entry[13],
                         permanent_address=entry[14],
                         phone_number=entry[15]
                         )
    print('Created staff object for ' + user_obj.email)
    designation = entry[9]
    if designation == 'Principal':
        g = Group.objects.get(name='principal')
    elif designation == 'Professor':
        faculty_group_obj = Group.objects.get(name='faculty')
        faculty_group_obj.user_set.add(user_obj)
        Group.objects.get(name='coe_staff').user_set.add(user_obj)
        g = Group.objects.get(name='hod')
    elif designation == 'Network Admin':
        g = Group.objects.get(name='network_admin')
    elif designation == 'Hostel Admin':
        g = Group.objects.get(name='hostel_admin')
    elif designation == 'Alumni Staff':
        g = Group.objects.get(name='alumni_staff')
    else:
        g = Group.objects.get(name='faculty')
    g.user_set.add(user_obj)
    print('Staff object assigned to group ' + g.name)
    if (entry[9] == 'Professor' or entry[9] == 'Principal' or entry[9] == 'Hostel Admin' or entry[
        9] == 'Network Admin'):
        CustomUser.objects.filter(email=entry[0]).update(is_approved=True)
    CustomUser.objects.filter(email=entry[0]).update(is_verified=True)
    CustomUser.objects.filter(email=entry[0]).update(has_filled_data=True)

list_of_students = (
    ('s1@gmail.com', '123456', '1317148', 'student', 'alias ',
     'A', 'M', 'AFather', 'AMother', 'CSE', '1', 'UG', 'cbe', 'cbe',
     '1968-02-02', '2016-06-26', '9658741277',
     'student-uploaded-images/1_DSC_0151.jpg', 'dayscholar',
     'regular',
     '123456781234', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s2@gmail.com', '123456', '1317111', 'student', 'alias ', 'B', 'M', 'BFather', 'BMother', 'CSE', '1', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741255', 'student-uploaded-images/DSC_0177.jpg', 'dayscholar', 'lateral',
     '123456781324', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s3@gmail.com', '123456', '1317138', 'student', 'alias ', 'C', 'F', 'CFather', 'CMother', 'CSE', '1', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741266', 'student-uploaded-images/_MG_1044.JPG', 'dayscholar', 'transfer',
     '123456781244', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s4@gmail.com', '123456', '1317127', 'student', 'alias ', 'D', 'M', 'DFather', 'DMother', 'CSE', '1', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456781233', 'caste', 'BC', 'worker', 0, 'worker', 0),
)

for entry in list_of_students:
    batch_obj = batch.objects.filter(programme='UG').get(start_year=2016)

    # CustomUser.objects.all().get(email=entry[0]).delete()
    user_obj = CustomUser.objects.create_user(email=entry[0],
                                              password=entry[1],
                                              is_staff_account=False,
                                              activation_key='abcd',
                                              key_expires=timezone.now(),
                                              )
    print('Created student object with mail ' + user_obj.email)
    department_obj = department.objects.get(acronym=entry[9])

    student.objects.create(
        user=user_obj,
        roll_no=entry[2],
        first_name=entry[3],
        middle_name=entry[4],
        last_name=entry[5],
        gender=entry[6],
        father_name=entry[7],
        mother_name=entry[8],
        department=department_obj,
        batch=batch_obj,
        current_semester=entry[10],
        qualification=entry[11],
        temporary_address=entry[12],
        permanent_address=entry[13],
        dob=entry[14],
        date_of_joining=entry[15],
        phone_number=entry[16],
        avatar=entry[17],
        student_type=entry[18],
        entry_type=entry[19],
        aadhaar_number=entry[20],
        caste=entry[21],
        community=entry[22],
        father_occupation=entry[23],
        father_annual_income=entry[24],
        mother_occupation=entry[25],
        mother_annual_income=entry[26],
    )
    print('Created student object for ' + user_obj.email)
    g = Group.objects.get(name='student')
    g.user_set.add(user_obj)
    CustomUser.objects.filter(email=entry[0]).update(is_verified=True)
    CustomUser.objects.filter(email=entry[0]).update(has_filled_data=True)
    print('Student object assigned to group ' + g.name)

    DeveloperProfile.objects.create(
        user=user_obj,
        core_developer=True,
        current_designation='Student'
    )

list_of_students = (
    ('s5@gmail.com', '123456', '1317101', 'student', 'alias ', 'E', 'M', 'DFather', 'DMother', 'CSE', '3', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456784321', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s6@gmail.com', '123456', '1317102', 'student', 'alias ', 'F', 'M', 'DFather', 'DMother', 'CSE', '3', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456784444', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s7@gmail.com', '123456', '1317103', 'student', 'alias ', 'G', 'M', 'DFather', 'DMother', 'CSE', '3', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456781433', 'caste', 'BC', 'worker', 0, 'worker', 0),
)

for entry in list_of_students:
    batch_obj = batch.objects.filter(programme='UG').get(start_year=2015)

    # CustomUser.objects.all().get(email=entry[0]).delete()
    user_obj = CustomUser.objects.create_user(email=entry[0],
                                              password=entry[1],
                                              is_staff_account=False,
                                              activation_key='abcd',
                                              key_expires=timezone.now()
                                              )
    print('Created student object with mail ' + user_obj.email)
    department_obj = department.objects.get(acronym=entry[9])

    student.objects.create(
        user=user_obj,
        roll_no=entry[2],
        first_name=entry[3],
        middle_name=entry[4],
        last_name=entry[5],
        gender=entry[6],
        father_name=entry[7],
        mother_name=entry[8],
        department=department_obj,
        batch=batch_obj,
        current_semester=entry[10],
        qualification=entry[11],
        temporary_address=entry[12],
        permanent_address=entry[13],
        dob=entry[14],
        date_of_joining=entry[15],
        phone_number=entry[16],
        avatar=entry[17],
        student_type=entry[18],
        entry_type=entry[19],
        aadhaar_number=entry[20],
        caste=entry[21],
        community=entry[22],
        father_occupation=entry[23],
        father_annual_income=entry[24],
        mother_occupation=entry[25],
        mother_annual_income=entry[26],
    )
    print('Created student object for ' + user_obj.email)
    g = Group.objects.get(name='student')
    g.user_set.add(user_obj)
    CustomUser.objects.filter(email=entry[0]).update(is_verified=True)
    CustomUser.objects.filter(email=entry[0]).update(has_filled_data=True)
    print('Student object assigned to group ' + g.name)

list_of_students = (
    ('s8@gmail.com', '123456', '1317104', 'student', 'alias ', 'H', 'M', 'DFather', 'DMother', 'CSE', '5', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456783659', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s9@gmail.com', '123456', '1317105', 'student', 'alias ', 'I', 'M', 'DFather', 'DMother', 'CSE', '5', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456785616', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s10@gmail.com', '123456', '1317106', 'student', 'alias ', 'J', 'M', 'DFather', 'DMother', 'CSE', '5', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456768457', 'caste', 'BC', 'worker', 0, 'worker', 0),
)

for entry in list_of_students:
    batch_obj = batch.objects.filter(programme='UG').get(start_year=2014)

    # CustomUser.objects.all().get(email=entry[0]).delete()
    user_obj = CustomUser.objects.create_user(email=entry[0],
                                              password=entry[1],
                                              is_staff_account=False,
                                              activation_key='abcd',
                                              key_expires=timezone.now()
                                              )
    print('Created student object with mail ' + user_obj.email)
    department_obj = department.objects.get(acronym=entry[9])

    student.objects.create(
        user=user_obj,
        roll_no=entry[2],
        first_name=entry[3],
        middle_name=entry[4],
        last_name=entry[5],
        gender=entry[6],
        father_name=entry[7],
        mother_name=entry[8],
        department=department_obj,
        batch=batch_obj,
        current_semester=entry[10],
        qualification=entry[11],
        temporary_address=entry[12],
        permanent_address=entry[13],
        dob=entry[14],
        date_of_joining=entry[15],
        phone_number=entry[16],
        avatar=entry[17],
        student_type=entry[18],
        entry_type=entry[19],
        aadhaar_number=entry[20],
        caste=entry[21],
        community=entry[22],
        father_occupation=entry[23],
        father_annual_income=entry[24],
        mother_occupation=entry[25],
        mother_annual_income=entry[26],
    )
    print('Created student object for ' + user_obj.email)
    g = Group.objects.get(name='student')
    g.user_set.add(user_obj)
    CustomUser.objects.filter(email=entry[0]).update(is_verified=True)
    CustomUser.objects.filter(email=entry[0]).update(has_filled_data=True)
    print('Student object assigned to group ' + g.name)

list_of_students = (
    ('s11@gmail.com', '123456', '1317107', 'student', 'alias ', 'K', 'M', 'DFather', 'DMother', 'CSE', '7', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456788963', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s12@gmail.com', '123456', '1317108', 'student', 'alias ', 'L', 'M', 'DFather', 'DMother', 'CSE', '7', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456789658', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s13@gmail.com', '123456', '1317109', 'student', 'alias ', 'M', 'M', 'DFather', 'DMother', 'CSE', '7', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456783256', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s14@gmail.com', '123456', '1317110', 'student', 'alias ', 'N', 'M', 'DFather', 'DMother', 'CSE', '7', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456787458', 'caste', 'BC', 'worker', 0, 'worker', 0),
    ('s15@gmail.com', '123456', '1317112', 'student', 'alias ', 'O', 'M', 'DFather', 'DMother', 'CSE', '7', 'UG', 'cbe',
     'cbe', '1968-02-02', '2016-06-26', '9658741288', 'student-uploaded-images/mt.jpg', 'hosteller', 'regular',
     '123456784563', 'caste', 'BC', 'worker', 0, 'worker', 0),
)

for entry in list_of_students:
    batch_obj = batch.objects.filter(programme='UG').get(start_year=2013)

    # CustomUser.objects.all().get(email=entry[0]).delete()
    user_obj = CustomUser.objects.create_user(email=entry[0],
                                              password=entry[1],
                                              is_staff_account=False,
                                              activation_key='abcd',
                                              key_expires=timezone.now()
                                              )
    print('Created student object with mail ' + user_obj.email)
    department_obj = department.objects.get(acronym=entry[9])

    student.objects.create(
        user=user_obj,
        roll_no=entry[2],
        first_name=entry[3],
        middle_name=entry[4],
        last_name=entry[5],
        gender=entry[6],
        father_name=entry[7],
        mother_name=entry[8],
        department=department_obj,
        batch=batch_obj,
        current_semester=entry[10],
        qualification=entry[11],
        temporary_address=entry[12],
        permanent_address=entry[13],
        dob=entry[14],
        date_of_joining=entry[15],
        phone_number=entry[16],
        avatar=entry[17],
        student_type=entry[18],
        entry_type=entry[19],
        aadhaar_number=entry[20],
        caste=entry[21],
        community=entry[22],
        father_occupation=entry[23],
        father_annual_income=entry[24],
        mother_occupation=entry[25],
        mother_annual_income=entry[26],
    )
    print('Created student object for ' + user_obj.email)
    g = Group.objects.get(name='student')
    g.user_set.add(user_obj)
    CustomUser.objects.filter(email=entry[0]).update(is_verified=True)
    CustomUser.objects.filter(email=entry[0]).update(has_filled_data=True)
    print('Student object assigned to group ' + g.name)
