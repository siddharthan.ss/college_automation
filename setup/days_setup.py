from curriculum.models import day

day.objects.all().delete()

day_list = (
    'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday','Saturday','Sunday'
)

for i,days in enumerate(day_list):
    day.objects.create(day_name=days, slot_number=i + 1)
    print('Created ' + days)
