from django.contrib.auth.models import Group

from accounts.models import department, active_batches
from curriculum.models import semester_migration

for dept_entry in department.objects.filter(is_core=True):

    try:
        act_batch_obj = active_batches.objects.filter(department=dept_entry).filter(
            current_semester_number_of_this_batch__lte=2).get(
            programme='UG')
        act_batch_obj.current_semester_number_of_this_batch += 1
        act_batch_obj.save()
        semester_migration_instances = semester_migration.objects.filter(department=dept_entry)

        semester_migration_instances = semester_migration_instances.filter(semester__batch__programme='UG')
        semester_migration_instances = semester_migration_instances.filter(semester_number__lte=2)
        for semester_migration_inst in semester_migration_instances:
            fa_group = Group.objects.get(name='faculty_advisor')
            list_of_faculty_advisors = []
            for faculty in semester_migration_inst.semester.faculty_advisor.all():
                print("staff = " + str(faculty))
                for active_semester in semester_migration.objects.all():
                    print("active semester = " + str(active_semester))
                    semester_instance = active_semester.semester
                    if semester_instance != semester_migration_inst.semester:
                        for entry in semester_instance.faculty_advisor.all():
                            list_of_faculty_advisors.append(entry)
            print("list of fa")
            print(list_of_faculty_advisors)
            for faculty in semester_migration_inst.semester.faculty_advisor.all():
                if faculty not in list_of_faculty_advisors:
                    fa_group.user_set.remove(faculty.user)
                    fa_group.save()
        semester_migration_instances.delete()
    except semester_migration.DoesNotExist:
        print('No semester plans hav been configured for this batch yet')
