"""
* this is a standalone script for creating all groups and each group permissions
* run it as execfile('accounts/group_setup.py')
"""

from django.contrib.auth.models import Group, Permission
from django.contrib.contenttypes.models import ContentType

# Delete all groups
# Delete all permissions assigned to all groups
# this is done to avoid overwritting records in respective tables/models

# create groups
groups = (
    'principal',
    'hod',
    'programme_coordinator',
    'faculty_advisor',
    'faculty',
    'student',
    'network_admin',
    'hostel_admin',
    'alumni_staff',
    'coe_staff',
)

for group in groups:
    g = Group.objects.get_or_create(name=group)
    print('created group ' + group)
# print(g)

# create permissions for groups
# general syntax
"""
content_type = ContentType.objects.get(app_label='myapp', model='BlogPost')
permission = Permission.objects.create(codename='can_publish',
                                       name='Can Publish Posts',
                                       content_type=content_type)
group = Group.objects.get(name='hod')
group.permissions.add(permission)
"""

# all permission adding to permission table
all_permissions = (
    ('accounts', 'staff', 'can_allot_hod', 'Can allot hod'),
    ('accounts', 'staff', 'can_approve_staffs', 'Can approve staffs'),
    ('accounts', 'site_settings', 'can_allot_first_year_managing_department',
     'Can allot first year managing department'),
    ('accounts', 'site_settings', 'can_allot_coe', 'Can allot COE'),
    ('curriculum', 'courses', 'can_choose_courses', 'Can choose courses'),
    ('curriculum', 'student_enrolled_courses', 'can_approve_students_for_courses', 'Can approve students for courses'),
    ('curriculum', 'semester', 'can_assign_programme_coordinator', 'Can assign programme coordinator'),
    ('curriculum', 'attendance', 'can_reallow_attendance', 'Can Re-allow attendance'),
    ('curriculum', 'semester', 'can_create_semester_plan', 'Can create semester plan'),
    ('curriculum', 'time_table', 'can_create_timetable', 'Can create timetable'),
    ('accounts', 'staff', 'can_approve_student', 'Can approve student'),
    ('curriculum', 'staff_course', 'can_allot_staffs', 'Can allot staffs'),
    ('curriculum', 'open_course_staff', 'can_allot_open_courses_staffs', 'Can allot open courses staffs'),
    ('curriculum', 'semester', 'can_view_semester_plan', 'Can view semester plan'),
    ('curriculum', 'time_table', 'can_view_timetable', 'Can view timetable'),
    ('curriculum', 'time_table', 'can_view_my_timetable', 'Can view my timetable'),
    ('marks', 'internal_marks', 'can_add_internal_marks', 'Can add internal marks'),
    ('curriculum', 'semester', 'can_view_semester_plan_of_his_batch', 'Can view semester plan of his batch'),
    ('curriculum', 'time_table', 'can_view_timetable_of_his_batch', 'Can view timetable of his batch'),
    ('curriculum', 'batch_split', 'can_split_batch', 'Can split batch'),
    ('curriculum', 'attendance', 'can_view_weekly_report', 'Can view weekly report'),
    ('curriculum', 'attendance', 'can_mark_attendance', 'Can mark attendance'),
    ('curriculum', 'attendance', 'can_view_overall_reports', 'Can view overall reports'),
    ('curriculum', 'holidays', 'can_assign_holidays', 'Can assign holidays'),
    ('curriculum', 'holidays', 'can_view_holidays', 'Can view holidays'),
    ('curriculum', 'department_sections', 'can_split_sections', 'Can split sections'),
    ('curriculum', 'unit_test_timetable', 'can_create_ut_timetable', 'Can create UT timetable'),
    ('wifi', 'wifi', 'can_approve_wifi_users', 'Can approve Wi-Fi users'),

    ('accounts', 'CustomUser', 'can_view_profiles', 'Can view profiles'),
    ('accounts', 'CustomUser', 'can_issue_onduty', 'Can issue onduty'),
    ('accounts', 'CustomUser', 'can_request_onduty', 'Can request onduty'),

    ('quiz', 'attend_quiz', 'can_attend_quiz', 'Can attend quiz'),
    ('hostel', 'mess_bill', 'can_enter_fee', 'Can enter fees'),
    ('hostel', 'mess_bill', 'can_view_own_fee', 'Can view own fees'),

    ('accounts', 'CustomUser', 'can_add_achievements', 'Can add achievements'),
    ('alumni', 'alumini_scholoarship_registration', 'can_apply_alumini_scholarship', 'Can Apply Alumini Scholarship'),
    ('alumni', 'alumini_scholoarship_registration', 'can_process_alumini_scholarship',
     'Can Process Alumini Scholarship'),
    ('enrollment', 'course_enrollment', 'can_enroll_students', 'Can enroll students'),
    ('enrollment', 'course_enrollment', 'can_view_enrollment_details', 'Can view enrollment details'),
    ('enrollment', 'course_enrollment', 'can_enable_courses', 'Can enable courses'),
)
for entry in all_permissions:
    content_type = ContentType.objects.get_or_create(app_label=entry[0], model=entry[1])

for entry in all_permissions:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get_or_create(codename=entry[2],
                                                  name=entry[3],
                                                  content_type=content_type)
    print('Permission created' + str(permission))

# principal
principal_access_specifier = (
    ('accounts', 'staff', 'can_allot_hod', 'Can allot hod'),
    ('curriculum', 'attendance', 'can_view_overall_reports', 'Can view overall reports'),
    ('accounts', 'site_settings', 'can_allot_first_year_managing_department',
     'Can allot first year managing department'),
    ('accounts', 'site_settings', 'can_allot_coe', 'Can allot COE'),

)
for entry in principal_access_specifier:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get(codename=entry[2],
                                        name=entry[3],
                                        content_type=content_type)
    group = Group.objects.get(name='principal')
    group.permissions.add(permission)

    print('Assigned ' + entry[2] + ' permission for Principal')

# access specifier of group hod
# format : (app_label,model,codename,name)
hod_access_specifier = (
    ('accounts', 'staff', 'can_approve_staffs', 'Can approve staffs'),
    ('curriculum', 'semester', 'can_assign_programme_coordinator', 'Can assign programme coordinator'),
    ('curriculum', 'attendance', 'can_reallow_attendance', 'Can Re-allow attendance'),
    ('curriculum', 'attendance', 'can_view_overall_reports', 'Can view overall reports'),
    ('enrollment', 'course_enrollment', 'can_view_enrollment_details', 'Can view enrollment details'),
    ('enrollment', 'course_enrollment', 'can_enable_courses', 'Can enable courses'),
)
for entry in hod_access_specifier:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get(codename=entry[2],
                                        name=entry[3],
                                        content_type=content_type)
    group = Group.objects.get(name='hod')
    group.permissions.add(permission)

    print('Assigned ' + entry[2] + ' permission for HOD')

# access specifier of group programme_coordinator
# format : (app_label,model,codename,name)
programme_coordinator_access_specifier = (
    ('curriculum', 'semester', 'can_create_semester_plan', 'Can create semester plan'),
    ('curriculum', 'time_table', 'can_create_timetable', 'Can create timetable'),
    ('curriculum', 'open_course_staff', 'can_allot_open_courses_staffs', 'Can allot open courses staffs'),
    ('curriculum', 'staff_course', 'can_allot_staffs', 'Can allot staffs'),
    ('curriculum', 'holidays', 'can_assign_holidays', 'Can assign holidays'),
    ('curriculum', 'batch_split', 'can_split_batch', 'Can split batch'),
    ('curriculum', 'department_sections', 'can_split_sections', 'Can split sections'),
    ('curriculum', 'unit_test_timetable', 'can_create_ut_timetable', 'Can create UT timetable'),
    ('enrollment', 'course_enrollment', 'can_view_enrollment_details', 'Can view enrollment details'),
    ('enrollment', 'course_enrollment', 'can_enable_courses', 'Can enable courses'),
)
for entry in programme_coordinator_access_specifier:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get(codename=entry[2],
                                        name=entry[3],
                                        content_type=content_type)
    group = Group.objects.get(name='programme_coordinator')
    group.permissions.add(permission)

    print('Assigned ' + entry[2] + ' permission for programme coordinator')

# access specifier of group faculty_advisor
# format : (app_label,model,codename,name)
faculty_advisor_access_specifier = (
    ('accounts', 'staff', 'can_approve_student', 'Can approve student'),
    ('curriculum', 'attendance', 'can_view_overall_reports', 'Can view overall reports'),
    ('enrollment', 'course_enrollment', 'can_view_enrollment_details', 'Can view enrollment details'),
    ('enrollment', 'course_enrollment', 'can_enable_courses', 'Can enable courses'),
)
for entry in faculty_advisor_access_specifier:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get(codename=entry[2],
                                        name=entry[3],
                                        content_type=content_type)
    group = Group.objects.get(name='faculty_advisor')
    group.permissions.add(permission)

    print('Assigned ' + entry[2] + ' permission for FA')

faculty_access_specifier = (
    ('curriculum', 'semester', 'can_view_semester_plan', 'Can view semester plan'),
    ('curriculum', 'student_enrolled_courses', 'can_approve_students_for_courses', 'Can approve students for courses'),
    ('curriculum', 'time_table', 'can_view_timetable', 'Can view timetable'),
    ('curriculum', 'time_table', 'can_view_my_timetable', 'Can view my timetable'),
    ('marks', 'internal_marks', 'can_add_internal_marks', 'Can add internal marks'),
    ('curriculum', 'attendance', 'can_view_weekly_report', 'Can view weekly report'),
    ('curriculum', 'attendance', 'can_mark_attendance', 'Can mark attendance'),
    ('curriculum', 'holidays', 'can_view_holidays', 'Can view holidays'),

    ('accounts', 'CustomUser', 'can_view_profiles', 'Can view profiles'),
    ('accounts', 'CustomUser', 'can_issue_onduty', 'Can issue onduty'),
)
for entry in faculty_access_specifier:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get(codename=entry[2],
                                        name=entry[3],
                                        content_type=content_type)
    group = Group.objects.get(name='faculty')
    group.permissions.add(permission)
    print('Assigned ' + entry[2] + ' permission for Faculty')

student_access_specifier = (
    ('curriculum', 'semester', 'can_view_semester_plan_of_his_batch', 'Can view semester plan of his batch'),
    ('curriculum', 'courses', 'can_choose_courses', 'Can choose courses'),
    ('curriculum', 'time_table', 'can_view_timetable_of_his_batch', 'Can view timetable of his batch'),
    ('curriculum', 'holidays', 'can_view_holidays', 'Can view holidays'),
    ('quiz', 'attend_quiz', 'can_attend_quiz', 'Can attend quiz'),
    # ('curriculum','internal_marks','can_view_own_internal_marks','Can view own internal marks'),

    ('accounts', 'CustomUser', 'can_request_onduty', 'Can request onduty'),
    ('hostel', 'mess_bill', 'can_view_own_fee', 'Can view own fees'),

    ('accounts', 'CustomUser', 'can_add_achievements', 'Can add achievements'),
    ('alumni', 'alumini_scholoarship_registration', 'can_apply_alumini_scholarship', 'Can Apply Alumini Scholarship'),

)
for entry in student_access_specifier:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get(codename=entry[2],
                                        name=entry[3],
                                        content_type=content_type)
    group = Group.objects.get(name='student')
    group.permissions.add(permission)
    print('Assigned ' + entry[2] + ' permission for student')

network_admin_access_specifier = (
    ('wifi', 'wifi', 'can_approve_wifi_users', 'Can approve Wi-Fi users'),
    ('accounts', 'CustomUser', 'can_view_profiles', 'Can view profiles'),
)
for entry in network_admin_access_specifier:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get(codename=entry[2],
                                        name=entry[3],
                                        content_type=content_type)
    group = Group.objects.get(name='network_admin')
    group.permissions.add(permission)
    print('Assigned ' + entry[2] + ' permission for network admin')

hostel_admin_access_specifier = (
    ('hostel', 'mess_bill', 'can_enter_fee', 'Can enter fees'),
)
for entry in hostel_admin_access_specifier:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get(codename=entry[2],
                                        name=entry[3],
                                        content_type=content_type)
    group = Group.objects.get(name='hostel_admin')
    group.permissions.add(permission)
    print('Assigned ' + entry[2] + ' permission for hostel_admin')

alumni_staff_access_specifier = (
    ('alumni', 'alumini_scholoarship_registration', 'can_process_alumini_scholarship',
     'Can Process Alumini Scholarship'),
)
for entry in alumni_staff_access_specifier:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get(codename=entry[2],
                                        name=entry[3],
                                        content_type=content_type)
    group = Group.objects.get(name='alumni_staff')
    group.permissions.add(permission)
    print('Assigned ' + entry[2] + ' permission for alumni_staff')

coe_staff_access_specifier = (
    ('curriculum', 'attendance', 'can_view_overall_reports', 'Can view overall reports'),
    ('enrollment', 'course_enrollment', 'can_enroll_students', 'Can enroll students'),
    ('enrollment', 'course_enrollment', 'can_enable_courses', 'Can enable courses'),
    ('enrollment', 'course_enrollment', 'can_view_enrollment_details', 'Can view enrollment details')
)

for entry in coe_staff_access_specifier:
    content_type = ContentType.objects.get(app_label=entry[0], model=entry[1])
    permission = Permission.objects.get(codename=entry[2],
                                        name=entry[3],
                                        content_type=content_type)
    group = Group.objects.get(name='coe_staff')
    group.permissions.add(permission)
    print('Assigned ' + entry[2] + ' permission for coe_staff')


grp=Group.objects.create(name="principalOfficial")
content=ContentType.objects.create(app_label='alumni',model='bonafide')
perm=Permission.objects.create(codename='can_change_bonafide_format',name='Can change bonafide format',content_type=content)
grp.permissions.add(perm)
thatUser=staff.objects.get(first_name="Principal")
grp.user_set.add(thatUser)
grp=Group.objects.create(name="bonafideApprover")
perm=Permission.objects.get_or_create(codename='can_download_bonafide_pdfs',name='Can download bonafide Pdfs',content_type=content)
grp.permissions.add(perm)
content=ContentType.objects.create(app_label='wifi',model='wifi')
perm=Permission.objects.create(codename='can_approve_wifi_staff',name='Can Approve Wifi Staff',content_type=content)
group = Group.objects.get(name='faculty_advisor')
group.permissions.add(permission)