from curriculum.models import staff_course, courses, semester, staff_course_split
from courseOutcomes.models import CoPattern, CoWeightage
from marks.models import student_approved_internals_mark, LabMarks2012
from quiz.models import QuizTime
from feedback.models import FeedbackByStudents
from assignment.models import assignment

# coursePk = '16SBS103'
# coursePk='16SBS104'
# coursePk='16SBS1Z2'
# coursePk='16SES107'

courseList = courses.objects.all()
semesterList = semester.objects.all()
staffCourseList = staff_course.objects.all()

print(staffCourseList)

counter = 0
for eachCourse in courseList:
    for eachSemester in semesterList:
        if staffCourseList.filter(semester=eachSemester, course=eachCourse):
            try:
                instance = staffCourseList.get(semester=eachSemester, course=eachCourse, batch=eachSemester.batch,
                                               department=eachSemester.department)
            except:
                print('semesterpk:' + str(eachSemester.pk) + 'coursepk:' + str(eachCourse.pk))
    counter = counter + 1
    print(counter)

# semesterInstance = semester.objects.get(pk=1)
# courseInstance = courses.objects.get(pk=coursePk)
#
# staffCourseInstance = staff_course.objects.get(semester=semesterInstance, course=courseInstance, department=13)
#
# print(staffCourseInstance)
#
# instance = CoPattern.objects.filter(staffCourse=staffCourseInstance)
# print('CoPattern: ' + str(instance.count()))
#
# instance = CoWeightage.objects.filter(staffCourse=staffCourseInstance)
# print('CoWeightage: ' + str(instance.count()))
#
# instance = student_approved_internals_mark.objects.filter(staff_course=staffCourseInstance)
# print('student_approved_internals_mark: ' + str(instance.count()))
#
# instance = LabMarks2012.objects.filter(staffCourse=staffCourseInstance)
# print('LabMarks2012: ' + str(instance.count()))
#
# instance = QuizTime.objects.filter(staffCourse=staffCourseInstance)
# print('QuizTime: ' + str(instance.count()))
#
# instance = FeedbackByStudents.objects.filter(staffCourse=staffCourseInstance)
# print('FeedbackByStudents: ' + str(instance.count()))
#
# instance = staff_course_split.objects.filter(staff_course=staffCourseInstance)
# print('staff_course_split: ' + str(instance.count()))
#
# instance = assignment.objects.filter(staff_course=staffCourseInstance)
# print('assignment: ' + str(instance.count()))
