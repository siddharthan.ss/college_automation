from accounts.models import department
from curriculum.models import semester_duration

for each_department in department.objects.filter(is_core=True):
    print("Create duration 2017 even for " + str(each_department.acronym) + ' - ' + str(
        semester_duration.objects.get_or_create(
            department=each_department, year=2017, is_even_semester=True)))

for each_department in department.objects.filter(is_core=True):
    print("Create duration 2017 odd for " + str(each_department.acronym) + ' - ' + str(
        semester_duration.objects.get_or_create(
            department=each_department, year=2017, is_even_semester=False)))

for each_department in department.objects.filter(is_core=True):
    print("Create duration 2018 even for " + str(each_department.acronym) + ' - ' + str(
        semester_duration.objects.get_or_create(
            department=each_department, year=2018, is_even_semester=True)))

for each_department in department.objects.filter(is_core=True):
    print("Create duration 2018 odd for " + str(each_department.acronym) + ' - ' + str(
        semester_duration.objects.get_or_create(
            department=each_department, year=2018, is_even_semester=False)))

for each_department in department.objects.filter(is_core=True):
    print("Create duration 2019 even for " + str(each_department.acronym) + ' - ' + str(
        semester_duration.objects.get_or_create(
            department=each_department, year=2019, is_even_semester=True)))

for each_department in department.objects.filter(is_core=True):
    print("Create duration 2019 odd for " + str(each_department.acronym) + ' - ' + str(
        semester_duration.objects.get_or_create(
            department=each_department, year=2019, is_even_semester=False)))
