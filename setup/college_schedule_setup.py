from curriculum.models import college_schedule, regulation

college_schedule.objects.all().delete()

college_schedule_list = (
    ('first_year', 1, '09:20:00', '10:10:00', 2, 'EOH', 2016),
    ('first_year', 2, '10:10:00', '11:00:00', 2, 'EOH', 2016),
    ('first_year', 3, '11:10:00', '12:00:00', 2, 'EOH', 2016),
    ('first_year', 4, '12:00:00', '12:50:00', 2, 'EOH', 2016),
    ('first_year', 5, '14:10:00', '15:00:00', 2, 'EOH', 2016),
    ('first_year', 6, '15:00:00', '15:50:00', 2, 'EOH', 2016),
    ('first_year', 7, '15:50:00', '16:40:00', 2, 'EOH', 2016),
    ('first_year', 8, '15:50:00', '16:40:00', 2, 'EOH', 2016),
)

for entry in college_schedule_list:
    college_schedule.objects.get_or_create(
        year=entry[0],
        hour=entry[1],
        start_of_hour=entry[2],
        end_of_hour=entry[3],
        attendance_marking_time_limit=entry[4],
        limit_starts_from=entry[5],
        regulation=regulation.objects.get(start_year=entry[6]),
    )
    print('Created college schedule for ' + entry[0] + ' hour ' + str(entry[1]))

college_schedule_list = (
    ('remaining_years', 4, '12:05:00', '13:00:00', 2, 'EOH', 2012),
)

for entry in college_schedule_list:
    college_schedule.objects.get_or_create(
        year=entry[0],
        hour=entry[1],
        start_of_hour=entry[2],
        end_of_hour=entry[3],
        attendance_marking_time_limit=entry[4],
        limit_starts_from=entry[5],
        regulation=regulation.objects.get(start_year=entry[6]),
    )
    print('Created college schedule for ' + entry[0] + ' hour ' + str(entry[1]))
