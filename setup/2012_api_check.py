from curriculum.models import attendance
from curriculum.views.common_includes import get_current_semester_plan_of_student
from django.utils import timezone
from accounts.models import student

from attendance.views.ConsolidatedAttendance2012API import ConsolidatedAttendance2012API

stud_inst = student.objects.get(roll_no='1417160')
sem_inst = get_current_semester_plan_of_student(stud_inst)
attendance_query = attendance.objects.filter(
    semester = sem_inst,
    date__gte = sem_inst.start_term_1,
    date__lte = timezone.now().date()
)

print(attendance_query.count())

print(attendance_query.filter(absent_students = stud_inst).count())

c_inst = ConsolidatedAttendance2012API()
c_inst.semester_instance = sem_inst
c_inst.start_date_instance = sem_inst.start_term_1
c_inst.end_date_instance = timezone.now().date()

print(c_inst.get_absent_days_count(c_inst.get_attendance_queryset(),stud_inst))
print(c_inst.get_total_working_days_count(c_inst.get_attendance_queryset(),stud_inst))