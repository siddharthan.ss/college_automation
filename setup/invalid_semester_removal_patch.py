from accounts.models import programme
from curriculum.models import semester,department_programmes

count = 0
for sem in semester.objects.all():
    dept_prg_inst = department_programmes.objects.filter(department=sem.department)
    prog_inst = programme.objects.get(acronym=sem.batch.programme)

    list_of_available_programmes = []

    for entry in dept_prg_inst:
        list_of_available_programmes.append(entry.programme)

    # print('list of available programmes')
    # print((list_of_available_programmes))
    #
    # print('prog_inst')
    # print(vars(prog_inst))

    if prog_inst not in list_of_available_programmes:
        print(vars(sem))
        count+=1

print('Count = ' + str(count))

