import re

from accounts.models import student
from wifi.models import wifi


count = 0
for entry in wifi.objects.all():
    if student.objects.filter(user=entry.user).exists():
        if not re.match(r'^([0-9L]+)$', entry.username):
            print(str(entry.user) + ' - ' + str(entry.connection_approved))
            if not entry.connection_approved:
                entry.delete()
            count+=1


print()
print(count)