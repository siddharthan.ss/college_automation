from curriculum.models import student_enrolled_courses, staff_course

for entry in student_enrolled_courses.objects.all():
    print(str(entry.course))

    semester_inst = staff_course.objects.filter(
        course=entry.course
    ).get(
        staffs=entry.under_staff
    ).semester

    entry.studied_semester = semester_inst
    entry.save()

    print('Done with fixing ' + str(entry.course) + '-' + str(entry.student) + '-' + str(entry.student.roll_no))
