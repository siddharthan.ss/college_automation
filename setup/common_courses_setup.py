from accounts.models import department, regulation
from curriculum.models import courses

courses_list = (
    (10, 'CC1', 'Tutorial', '', 0, 0, 0, 0, 0, 0, 0, 'UG', '(T)'),
    (10, 'CC2', 'Seminar', '', 0, 0, 0, 0, 0, 0, 0, 'UG', 'SEMINAR'),
    (10, 'CC3', 'Library', '', 0, 0, 0, 0, 0, 0, 0, 'UG', 'LIBRARY'),
    (10, 'CC4', 'Placement', '', 0, 0, 0, 0, 0, 0, 0, 'UG', 'PLACEMENT'),
    (10, 'CC5', 'Mentoring Hour', '', 0, 0, 0, 0, 0, 0, 0, 'UG', 'MH'),
    (10, 'CC6', 'Psychological Welfare Hour', '', 0, 0, 0, 0, 0, 0, 0, 'UG', 'PWH'),
)

regulation_inst = regulation.objects.get(start_year=2016)

print('created regulation ' + str(regulation_inst))
for course in courses_list:
    department_obj = department.objects.all().get(name='Computer Science and Engineering')
    # if courses.objects.filter(course_id=course[1]):
    #     print('Course exists ' + course[2])
    # else:
    courses.objects.get(semester=course[0],
                           course_id=course[1],
                           course_name=course[2],
                           CAT=course[3],
                           CAT_marks=course[4],
                           end_semester_marks=course[5],
                           L_credits=course[6],
                           T_credits=course[7],
                           P_credits=course[8],
                           C_credits=course[9],
                           department=department_obj,
                           programme=course[11],
                           regulation=regulation_inst,
                           code=course[12],
                           common_course=True
                           ).delete()
    print('Created course ' + course[2])
