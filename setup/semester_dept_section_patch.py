from curriculum.models import department_sections, semester

for entry in semester.objects.all():
    department = entry.department
    batch = entry.batch

    department_section_instance = department_sections.objects.filter(
        department=department
    ).get(
        batch=batch
    )

    entry.department_section = department_section_instance
    entry.save()

    print('fixed department section, semester of ' + str(department.acronym) + " " + str(batch))
