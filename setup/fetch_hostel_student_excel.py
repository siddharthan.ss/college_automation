import openpyxl
from pip._vendor.distlib.compat import raw_input

from accounts.models import department
from hostel.models import temp_hostel_student_details

file_name = raw_input('Enter file path')
wb = openpyxl.load_workbook(filename=file_name)

print(wb.get_sheet_names())

sheet_name = raw_input('Enter sheet name')
sheet = wb.get_sheet_by_name(sheet_name)

start_cell = raw_input('Enter start cell ')
end_cell = raw_input('Enter end cell ')
curr_sem = raw_input('Enter current semester ')
gender = raw_input('Enter Gender ')

for rowOfCellObjects in sheet[start_cell:end_cell]:
    roll_no = rowOfCellObjects[0].value
    print((roll_no))
    roll_no = str(roll_no)
    dept_code = (roll_no[2:4])
    if dept_code == '11':
        dept_acronym = 'CIVIL'
    elif dept_code == '12':
        dept_acronym = 'MECH'
    elif dept_code == '13':
        dept_acronym = 'EEE'
    elif dept_code == '14':
        dept_acronym = 'ECE'
    elif dept_code == '15':
        dept_acronym = 'PROD'
    elif dept_code == '16':
        dept_acronym = 'EIE'
    elif dept_code == '17':
        dept_acronym = 'CSE'
    elif dept_code == '18':
        dept_acronym = 'IT'
    elif dept_code == '19':
        dept_acronym = 'IBT'
    name = rowOfCellObjects[1].value
    dept = department.objects.get(acronym=dept_acronym)

    created_obj = temp_hostel_student_details.objects.get_or_create(
        roll_no=roll_no,
        name=name,
        department=dept,
        current_semester=curr_sem,
        gender=gender,
    )

    print((created_obj))

    print('--- END OF ROW ---')
