from pprint import pprint

from accounts.models import active_batches, department_programmes, department
from curriculum.models import department_sections

for entry in active_batches.objects.filter(batch__programme="PG"):
    batch_inst = entry.batch
    dept_inst = entry.department
    section_name = 'A'

    print(dept_inst)
    print(batch_inst)

    print(department_sections.objects.filter(
        department=dept_inst,
        batch=batch_inst,
        section_name=section_name
    ))

    created_inst = department_sections.objects.get_or_create(
        department=dept_inst,
        batch=batch_inst,
        section_name=section_name
    )
    print(created_inst)

    if created_inst[1] == False:
        print('something fishy')

# all_core_departments = department.objects.filter(is_core=True)
#
# for entry in department_programmes.objects.filter(programme__acronym="PG"):
#     all_core_departments = all_core_departments.exclude(acronym = entry.department.acronym)
#
#
# pprint(all_core_departments)
#
# ug_only_departments = all_core_departments
#
# for entry in active_batches.objects.filter(programme="PG").filter(department__in=ug_only_departments):
#     entry.delete()
#     pprint(entry)

