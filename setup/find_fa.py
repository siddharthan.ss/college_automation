from curriculum.models import semester_migration
from accounts.models import staff

du_inst = staff.objects.get(first_name="Dummy")

for entry in semester_migration.objects.all():
    for fa in entry.semester.faculty_advisor.all():
        if fa == du_inst:
            entry.semester.faculty_advisor.remove(du_inst)
            entry.semester.save()
            print(entry.semester)