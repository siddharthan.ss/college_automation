from curriculum.models import semester, semester_migration

for sem in semester.objects.all():
    if not semester_migration.objects.filter(semester=sem).exists():
        print(str(sem.semester_number) + ' - ' + str(sem.batch.programme) + ' - ' + str(sem.department.acronym))
        print(sem.faculty_advisor.all())