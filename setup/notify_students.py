from accounts.models import onesignal

count = 0
for entry in onesignal.objects.all():
    if entry.user.is_student():
        if entry.user.get_student_instance().regulation.start_year == 2012:
            count += 1

print(count)