from curriculum.models import courses, semester, time_table, day

# coursePk = '16SBS103'
# coursePk='16SBS104'
# coursePk='16SBS1Z2'
# coursePk='16SES107'

courseList = courses.objects.all()
semesterList = semester.objects.all()
timeTableList = time_table.objects.all()
dayList = day.objects.all()
hourList = [1, 2, 3, 4, 5, 6, 7, 8]

print('start')

counter = 0
for eachCourse in courseList:
    for eachSemester in semesterList:
        for eachDay in dayList:
            for eachHour in hourList:
                if timeTableList.filter(semester=eachSemester, course=eachCourse, day=eachDay, hour=eachHour):
                    try:
                        instance = timeTableList.get(semester=eachSemester, course=eachCourse, day=eachDay,
                                                     hour=eachHour)
                    except:
                        print('semesterpk:' + str(eachSemester.pk) + ' coursepk:' + str(eachCourse.pk) + ' day:' + str(
                            eachDay) + ' hour:' + str(eachHour))
                        print('------------')
                        for each in timeTableList.filter(semester=eachSemester, course=eachCourse, day=eachDay,
                                                         hour=eachHour):
                            print('pk:' + str(each.pk) + ' sem:' + str(each.semester.pk) + ' course:' + str(
                                each.course.pk) + ' day:' + str(eachDay) + ' hour:' + str(eachHour) + ' created:' + str(
                                each.created_date) + ' modified:' + str(each.modified_date) + ' isActive:' + str(
                                each.is_active) + ' batch:' + str(each.batch) + ' dept:' + str(each.department.acronym))
                        print('------------')
    counter = counter + 1
    print(counter)

print('end')

# semesterInstance = semester.objects.get(pk=1)
# courseInstance = courses.objects.get(pk=coursePk)
#
# staffCourseInstance = staff_course.objects.get(semester=semesterInstance, course=courseInstance, department=13)
#
# print(staffCourseInstance)
#
# instance = CoPattern.objects.filter(staffCourse=staffCourseInstance)
# print('CoPattern: ' + str(instance.count()))
#
# instance = CoWeightage.objects.filter(staffCourse=staffCourseInstance)
# print('CoWeightage: ' + str(instance.count()))
#
# instance = student_approved_internals_mark.objects.filter(staff_course=staffCourseInstance)
# print('student_approved_internals_mark: ' + str(instance.count()))
#
# instance = LabMarks2012.objects.filter(staffCourse=staffCourseInstance)
# print('LabMarks2012: ' + str(instance.count()))
#
# instance = QuizTime.objects.filter(staffCourse=staffCourseInstance)
# print('QuizTime: ' + str(instance.count()))
#
# instance = FeedbackByStudents.objects.filter(staffCourse=staffCourseInstance)
# print('FeedbackByStudents: ' + str(instance.count()))
#
# instance = staff_course_split.objects.filter(staff_course=staffCourseInstance)
# print('staff_course_split: ' + str(instance.count()))
#
# instance = assignment.objects.filter(staff_course=staffCourseInstance)
# print('assignment: ' + str(instance.count()))
