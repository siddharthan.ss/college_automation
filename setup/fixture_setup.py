import json

from django.core import serializers

from accounts.models import department, batch, CustomUser, staff, student

serialized_dept = serializers.serialize('json', department.objects.all(), use_natural_foreign_keys=True,
                                        use_natural_primary_keys=True)
serialized_dept = json.dumps(json.loads(serialized_dept), indent=4)
print(serialized_dept)
file = open('accounts/fixtures/department.json', 'w')
file.write(serialized_dept)
file.close()

serialized_batch = serializers.serialize('json', batch.objects.all(), use_natural_foreign_keys=True,
                                         use_natural_primary_keys=True)
serialized_batch = json.dumps(json.loads(serialized_batch), indent=4)
print(serialized_batch)
file = open('accounts/fixtures/batch.json', 'w')
file.write(serialized_batch)
file.close()

serialized_CustomUser = serializers.serialize('json', CustomUser.objects.all(), use_natural_foreign_keys=True,
                                              use_natural_primary_keys=True)
serialized_CustomUser = json.dumps(json.loads(serialized_CustomUser), indent=4)
print(serialized_CustomUser)
file = open('accounts/fixtures/customuser.json', 'w')
file.write(serialized_CustomUser)
file.close()

serialized_staff = serializers.serialize('json', staff.objects.all(), use_natural_foreign_keys=True,
                                         use_natural_primary_keys=True)
serialized_staff = json.dumps(json.loads(serialized_staff), indent=2)
print(serialized_staff)
file = open('accounts/fixtures/staff.json', 'w')
file.write(serialized_staff)
file.close()

serialized_student = serializers.serialize('json', student.objects.all(), use_natural_foreign_keys=True,
                                           use_natural_primary_keys=True)
serialized_student = json.dumps(json.loads(serialized_student), indent=4)
print(serialized_student)
file = open('accounts/fixtures/student.json', 'w')
file.write(serialized_student)
file.close()

from curriculum.models import day, college_schedule, courses, semester, time_table

serialized_day = serializers.serialize('json', day.objects.all(), use_natural_foreign_keys=True,
                                       use_natural_primary_keys=True)
serialized_day = json.dumps(json.loads(serialized_day), indent=4)
print(serialized_day)
file = open('curriculum/fixtures/day.json', 'w')
file.write(serialized_day)
file.close()

serialized_schedule = serializers.serialize('json', college_schedule.objects.all(), use_natural_foreign_keys=True,
                                            use_natural_primary_keys=True)
serialized_schedule = json.dumps(json.loads(serialized_schedule), indent=4)
print(serialized_schedule)
file = open('curriculum/fixtures/college_schedule.json', 'w')
file.write(serialized_schedule)
file.close()

serialized_courses = serializers.serialize('json', courses.objects.all(), use_natural_foreign_keys=True,
                                           use_natural_primary_keys=True)
serialized_courses = json.dumps(json.loads(serialized_courses), indent=4)
print(serialized_courses)
file = open('curriculum/fixtures/courses.json', 'w')
file.write(serialized_courses)
file.close()

serialized_semester = serializers.serialize('json', semester.objects.all(), use_natural_foreign_keys=True,
                                            use_natural_primary_keys=True)
serialized_semester = json.dumps(json.loads(serialized_semester), indent=4)
print(serialized_semester)
file = open('curriculum/fixtures/semester.json', 'w')
file.write(serialized_semester)
file.close()

serialized_tt = serializers.serialize('json', time_table.objects.all(), use_natural_foreign_keys=True,
                                      use_natural_primary_keys=True)
serialized_tt = json.dumps(json.loads(serialized_tt), indent=4)
print(serialized_tt)
file = open('curriculum/fixtures/timetable.json', 'w')
file.write(serialized_tt)
file.close()

from django.contrib.auth.models import *

serialized_perm = serializers.serialize('json', Permission.objects.all(), use_natural_foreign_keys=True,
                                        use_natural_primary_keys=True)
serialized_perm = json.dumps(json.loads(serialized_perm), indent=4)
print(serialized_perm)
file = open('accounts/fixtures/perms.json', 'w')
file.write(serialized_perm)
file.close()

serialized_group = serializers.serialize('json', Group.objects.all(), use_natural_foreign_keys=True,
                                         use_natural_primary_keys=True)
serialized_group = json.dumps(json.loads(serialized_group), indent=4)
print(serialized_group)
file = open('accounts/fixtures/group.json', 'w')
file.write(serialized_group)
file.close()

from django.contrib.contenttypes.models import ContentType

serialized_ct = serializers.serialize('json', ContentType.objects.all(), use_natural_foreign_keys=True,
                                      use_natural_primary_keys=True)
serialized_ct = json.dumps(json.loads(serialized_ct), indent=4)
print(serialized_ct)
file = open('accounts/fixtures/ct.json', 'w')
file.write(serialized_ct)
file.close()
