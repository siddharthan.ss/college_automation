from common.utils.slotNumberUtil import getSlotNumberForSemesterInstance
from accounts.models import department
from curriculum.models import semester
from datetime import  datetime

department_instance = department.objects.get(acronym="CSE")

d = datetime.strptime("01-01-2018","%d-%m-%Y")

sem = semester.objects.filter(start_term_1__gte=d).first()

ed = datetime.strptime("02-04-2018","%d-%m-%Y")
print(getSlotNumberForSemesterInstance(sem, ed))