

from django.contrib.auth.models import Group

from accounts.models import department, staff, department_programmes
from curriculum.models import programme_coordinator, programme, hod

for dept in department.objects.filter(is_core=True):
    for staff_inst in staff.objects.filter(department=dept):
        if staff_inst.user.groups.filter(name="hod").exists():
            print(dept.acronym)
            hod_inst = hod.objects.get_or_create(
                department= dept,
                staff=staff_inst,
            )
            print(hod_inst)