from datetime import datetime, timedelta
from django.utils import timezone
from accounts.models import staff
from curriculum.models import courses, semester, semester_migration, attendance
from curriculum.views import get_staff_handling_course
from curriculum.views.pending_attendances import get_pending_attendance_periods




for course_inst in courses.objects.filter(department__acronym="EIE",regulation__start_year=2016,semester=3):
    print(course_inst)

    sem_inst = semester_migration.objects.get(semester__semester_number=3,department__acronym="EIE").semester

    pending_hours = get_pending_attendance_periods(sem_inst,staff.objects.none(),course_inst)

    print(pending_hours)
    # print(pending_hours.count())

    for entry in pending_hours:
        att_query = attendance.objects.filter(
            course=entry['course'],
            semester=entry['semester'],
            date=entry['date'],
            hour=entry['hour'],
        )

        if att_query.exists():
            print("exists")
            attendance_inst = att_query.get()
            attendance_inst.granted_edit_access = True
            attendance_inst.grant_period = datetime.now() + timedelta(days=5)
            attendance_inst.save()
            print(attendance_inst)

        else:
            if entry['date'] < (timezone.now().date()):
                list_of_staffs = get_staff_handling_course(entry['course'],entry['semester'])
                attentance_obj = attendance.objects.create(
                    date=entry['date'],
                    hour=entry['hour'],
                    course=entry['course'],
                    staff=list_of_staffs[0],
                    semester=entry['semester'],
                    granted_edit_access=True,
                    grant_period=datetime.now() + timedelta(days=5)
                )
                print("Reallowed for " + str(entry['date']) + ' - ' + str(entry['hour']) + ' - ' + str(entry['course']) + ' - ' + str(entry['semester']))
            else:
                print("Reallowed for " + str(entry['date']) + ' - ' + str(entry['hour']) + ' - ' + str(
                    entry['course']) + ' - ' + str(entry['semester']))

        # attentance_obj = attendance.objects.create(
        #     date=date,
        #     hour=hour,+ ' - ' + str(entry['hour'])
        #     course=staff_course_inst.course,
        #     staff=staff_instance,
        #     semester=staff_course_inst.semester,
        #     granted_edit_access=True,
        #     grant_period=datetime.now() + timedelta(hours=2)
        # )