from curriculum.models import flexi_attendance, attendance, semester_migration

min_date = None
for entry in semester_migration.objects.all():
    if min_date is None:
        min_date = entry.semester.start_term_1
    else:
        if entry.semester.start_term_1 < min_date:
            min_date = entry.semester.start_term_1
count = 0
for entry in flexi_attendance.objects.filter(attendance_date__gte = min_date,marked=True).order_by('attendance_date'):
    if entry.original_course != entry.marked_course:
        if attendance.objects.filter(date= entry.attendance_date). \
            filter(hour=entry.attendance_hour). \
            filter(course = entry.original_course).filter(semester = entry.semester).exists() and \
                attendance.objects.filter(date=entry.attendance_date). \
                        filter(hour=entry.attendance_hour). \
                        filter(course=entry.marked_course).filter(semester=entry.semester).exists():
                print(str(entry.original_course) + ' - ' +str(entry.attendance_date) + ' - ' + str(entry.attendance_hour))
                print(str(entry.marked_course))
                print()
                count = count + 1

print(count)
