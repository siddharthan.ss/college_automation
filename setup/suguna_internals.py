from django.core.exceptions import MultipleObjectsReturned

from accounts.models import staff
from curriculum.views.common_includes import get_staff_course_allotment_instances_for_staff, get_list_of_students
from marks.common.util.internalMarksUtil import get_internal_marks_queryset
from marks.models import internalmarksold

s = staff.objects.get(user__email="a.sugu53@gmail.com")

for sc_entry in get_staff_course_allotment_instances_for_staff(s):
    for s_entry in get_list_of_students(sc_entry.semester,sc_entry.course):
        print('student_name')
        print(s_entry)
        try:
            get_internal_marks_queryset(s_entry['student_obj'],sc_entry.course,sc_entry.semester)
        except MultipleObjectsReturned:
            for entry in internalmarksold.objects.filter(
            student=s_entry['student_obj'],
            course=sc_entry.course,
            semester=sc_entry.semester,
            ):
                print(entry)
