from accounts.models import CustomUser

from pathlib import Path

pathLocation = "/home/local/ZOHOCORP/muthamil-5223/PycharmProjects/quiz_fix/college_automation"

counter = 0
deleteCounter = 0
for user in CustomUser.objects.all():
    if user.avatar:
        file = Path(pathLocation + "/media/" + str(user.avatar))
        if not file.is_file():
            user.avatar = None
            user.has_profile_picture = False
            user.save()
            deleteCounter = deleteCounter + 1
    counter = counter + 1
    print('counter:' + str(counter))

print()
print('deleteCounter:' + str(deleteCounter))
