import json

from accounts.models import department
from hostel.models import temp_hostel_student_details

temp_hostel_student_details.objects.all().delete()

with open('test.json') as data_file:
    data = json.load(data_file)

for each in data:
    fields = each['fields']
    name = fields['name']
    roll_no = fields['roll_no']
    gender = fields['gender']
    dept_acronym = fields['department']
    dept_inst = department.objects.get(acronym=dept_acronym)
    current_sem = fields['current_semester']
    is_registered = fields['is_registered']

    obj = temp_hostel_student_details.objects.get_or_create(
        name=name,
        roll_no=roll_no,
        gender=gender,
        department=dept_inst,
        current_semester=current_sem,
        is_registered=is_registered,
    )

    print(roll_no)
    print(obj)
