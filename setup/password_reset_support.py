from accounts.models import student

roll_numbers = [
    '1514108',
    '1514110',
    '1514120',
    '1514131',
]

for entry in roll_numbers:
    stud_inst = student.objects.get(roll_no=entry)
    stud_inst.user.set_password('test#1234')
    stud_inst.user.save()

    print(str(stud_inst) + ' - ' + str(stud_inst.user.email))