from datetime import timedelta, datetime

from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone

from accounts.models import student, staff
from curriculum.models import semester, open_course_staff, courses, time_table, day, student_enrolled_courses, \
    staff_course
from enrollment.models import course_enrollment

from curriculum.views.common_includes import staffBelongsToNonCore, staffNotEligible, \
    get_first_year_managing_department, get_active_semesters_tabs, get_current_batches, \
    get_current_course_allotment_with_display_text, check_department_batch_has_multiple_sections

from common.utils.studentUtil import getSemesterPlanofStudent


