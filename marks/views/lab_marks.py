from csv import DictWriter
from datetime import datetime
from decimal import Decimal

from io import StringIO
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.core.mail import mail_admins
from django.shortcuts import render, redirect
from easy_pdf.views import PDFTemplateView
from django.core.urlresolvers import reverse
from django.http import JsonResponse, HttpResponse

from accounts.models import CustomUser, staff, student
from curriculum.models import courses, staff_course, student_enrolled_courses, section_students, department_sections

from curriculum.views.common_includes import get_current_course_allotment_with_display_text, get_list_of_students
from marks.models import lab_pattern, lab_marks
from marks.views.internal_marks import temp_filter_students


@login_required
@permission_required('marks.can_add_internal_marks')
def create_lab_pattern(request):
    list_of_practical_staff_course_entries = []
    staff_instance = staff.objects.get(user=request.user)
    list_of_staff_course_ins = get_current_course_allotment_with_display_text(staff_instance)
    # print(course_list)
    for entry in list_of_staff_course_ins:
        if entry['staff_course_instance'].course.subject_type == 'P':
            list_of_practical_staff_course_entries.append(entry)

    if len(list_of_practical_staff_course_entries) > 0:
        pass
    else:
        msg = {
            'page_title': 'No Practical Courses',
            'title': 'No Practical Courses',
            'description': 'No practical courses has been alloted to you. Contact Programme Coordinator',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    selected_staff_course_instance = list_of_practical_staff_course_entries[0]['staff_course_instance']
    if request.method == 'POST':
        if request.is_ajax():
            if 'delcourse' in request.POST:
                selected_staff_course_instance_pk = str(request.POST.get('sel_crs'))
                selected_staff_course_instance = staff_course.objects.get(pk=selected_staff_course_instance_pk)

                lab_pattern.objects.filter(course=selected_staff_course_instance.course,
                                           batch=selected_staff_course_instance.semester.batch,
                                           department=selected_staff_course_instance.department,
                                           semester=selected_staff_course_instance.semester,
                                           ).delete()
                dictionary = {}
                dictionary['avail'] = 'true'
                print((dictionary))
                return JsonResponse(dictionary)

            else:
                selected_staff_course_instance_pk = str(request.POST.get('sel_crs'))
                selected_staff_course_instance = staff_course.objects.get(pk=selected_staff_course_instance_pk)

                list = []
                dictionary = {}
                if lab_pattern.objects.filter(course=selected_staff_course_instance.course,
                                              batch=selected_staff_course_instance.semester.batch,
                                              department=selected_staff_course_instance.department,
                                              semester=selected_staff_course_instance.semester,
                                              ).exists():
                    # print("i am true")
                    dictionary['avail'] = 'true'
                    for each in lab_pattern.objects.filter(
                            course=selected_staff_course_instance.course,
                            batch=selected_staff_course_instance.semester.batch,
                            department=selected_staff_course_instance.department,
                            semester=selected_staff_course_instance.semester,
                            ).order_by('question_number'):
                        temp = {}
                        temp['quesno'] = each.user_prespective_question_number
                        temp['co'] = each.qn_mapping
                        temp['mark'] = each.marks
                        list.append(temp)
                    dictionary['list'] = list
                else:
                    dictionary['avail'] = 'false'
                return JsonResponse(dictionary)

        if 'question_count' in request.POST:
            question_count = int(request.POST.get('question_count'))
            selected_staff_course_instance_pk = str(request.POST.get('sel_crs'))
            selected_staff_course_instance = staff_course.objects.get(pk=selected_staff_course_instance_pk)
            # print('question_count=' + str(question_count))

            # print(request.POST.getlist('subdivision'))
            # print(request.POST.getlist('co'))
            # print(request.POST.getlist('mark'))
            counter = 1
            subdivision_list = []
            while counter <= question_count:
                # print('subdivision ' + str(counter) + ':' + str(request.POST.get('subdivision' + str(counter))))
                subcounter = 1
                list = []
                while subcounter <= int(request.POST.get('subdivision' + str(counter))):
                    temp = {}
                    # print('co ' + str(counter) + '-' + str(subcounter) + ':' + str(
                    #     request.POST.get('co' + str(counter) + '-' + str(subcounter))))
                    # print('mark ' + str(counter) + '-' + str(subcounter) + ':' + str(
                    #     request.POST.get('mark' + str(counter) + '-' + str(subcounter))))

                    temp['co'] = str(request.POST.get('co' + str(counter) + '-' + str(subcounter)))
                    temp['mark'] = str(request.POST.get('mark' + str(counter) + '-' + str(subcounter)))
                    list.append(temp)
                    subcounter = subcounter + 1
                subdivision_list.append(list)
                counter = counter + 1

            # print('utno=' + str(request.POST.get('utno')))\

            if lab_pattern.objects.filter(course=selected_staff_course_instance.course,
                                          batch=selected_staff_course_instance.semester.batch,
                                          department=selected_staff_course_instance.department,
                                          semester=selected_staff_course_instance.semester
                                          ).exists():
                msg = {
                    'page_title': 'Pattern already exists',
                    'title': 'Pattern already exists',
                    'description': 'Please click on \'View previous pattern\' to view the previous pattern set. Delete if neccessary inorder to create a new pattern!',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
            question_counter = 0
            user_pres_num_part = 0
            for each in subdivision_list:
                user_pres_num_part = user_pres_num_part + 1
                alphacounter = 'a'
                for every in each:
                    question_counter = question_counter + 1
                    if len(each) == 1:
                        user_prespective_number = user_pres_num_part
                    else:
                        user_prespective_number = str(user_pres_num_part) + str(alphacounter)

                    if not lab_pattern.objects.filter(
                        course=selected_staff_course_instance.course,
                        batch=selected_staff_course_instance.semester.batch,
                        department=selected_staff_course_instance.department,
                        semester=selected_staff_course_instance.semester,
                        question_number=question_counter,
                        user_prespective_question_number=str(user_prespective_number),
                        qn_mapping=str(every['co']),
                        marks=int(every['mark']),
                        internal_marks=10
                    ).exists():
                        lab_pattern.objects.create(
                            course=selected_staff_course_instance.course,
                            batch=selected_staff_course_instance.semester.batch,
                            department=selected_staff_course_instance.department,
                            semester=selected_staff_course_instance.semester,
                            question_number=question_counter,
                            user_prespective_question_number=str(user_prespective_number),
                            qn_mapping=str(every['co']),
                            marks=int(every['mark']),
                            internal_marks=10
                        )
                    else:
                        mail_admins("Duplicate lab paper pattern",locals())

                    # print(str(user_prespective_number) + '-' + str(alphacounter) + '==>' + str(every))
                    alphacounter = chr(ord(alphacounter) + 1)

        if 'selected_staff_course_pk' in request.POST:
            selected_staff_course_pk = int(request.POST.get('selected_staff_course_pk'))
            selected_staff_course_instance = staff_course.objects.get(pk=selected_staff_course_pk)

    # print('selected_course = ' + str(selected_course))
    subject_total_marks = selected_staff_course_instance.course.CAT_marks

    already_created_paper_pattern = False
    if lab_pattern.objects.filter(course=selected_staff_course_instance.course,
                                  batch=selected_staff_course_instance.semester.batch,
                                  department=selected_staff_course_instance.department,
                                  semester=selected_staff_course_instance.semester
                                  ).exists():
        already_created_paper_pattern = True

    print(selected_staff_course_instance.pk)
    return render(request, 'co_marks/create_lab_pattern.html',{
                   'selected_staff_course_instance': selected_staff_course_instance,
                   'list_of_practical_staff_course_entries': list_of_practical_staff_course_entries,
                   'subject_total_marks': subject_total_marks,
                    'already_created_paper_pattern' : already_created_paper_pattern,
                   })


class labPdfReport(PDFTemplateView):
    template_name = "co_marks/lab_pdf_report.html"
    selected_course = None

    def get(self, request, *args, **kwargs):
        self.selected_course = kwargs.pop('course_id')
        # print('course pdf ' + self.selected_course)
        # print('ut pdf ' + self.selected_ut)
        return super(labPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = lab_pdf_data(self.request,
                                    selected_course=self.selected_course
                                    )
        # print(context_data)
        return super(labPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )
@login_required
@permission_required('marks.can_add_internal_marks')
def labCsvReport(request, course_id):
    f = StringIO()
    writer = DictWriter(f, ["Roll no", "Name","Total experiment Marks","Out of 40","Models (10)" , "CA marks"])
    writer.writeheader()
    context_data = lab_pdf_data(request,
                                selected_course=course_id
                                )
    for entry in context_data['student_list']:
        report_line = {
            "Roll no": entry['regno'],
            "Name": entry['name'],
            "Total experiment Marks": entry['tot'],
            "Out of 40": entry['avg'],
            "Models (10)": entry['internal_mark'],
            "CA marks": entry['ca_mark'],
        }
        writer.writerow(report_line)
    report = f.getvalue()
    resp = HttpResponse(report)
    resp["Content-Disposition"] = "attachment; filename='{}'".format( str(context_data['selected_course']) + ".csv")
    return resp


@login_required
@permission_required('marks.can_add_internal_marks')
def lab_pdf_data(request, selected_course=False):
    current_year = datetime.now()
    academic_year = int(current_year.year)
    faculty_instance = staff.objects.get(user=request.user)
    curr_ins = staff_course.objects.get(id=selected_course)
    course_instance = curr_ins.course
    current_batch = course_instance.regulation.start_year

    instance = staff_course.objects.get(id=selected_course)
    batch_instance = getattr(instance, 'batch')
    semester_instance = getattr(instance, 'semester')
    sem = semester_instance.semester_number

    list = []
    count = 0
    co_total_marks = 0
    for each in lab_pattern.objects.filter(course=course_instance, batch=batch_instance,
                                           department=curr_ins.department, semester=semester_instance
                                           ).order_by('question_number'):
        count += 1
        temp = {}
        temp['id'] = each.id
        temp['quesno'] = each.user_prespective_question_number
        temp['co'] = each.qn_mapping
        temp['mark'] = each.marks
        temp['internal'] = each.internal_marks
        co_total_marks = co_total_marks + each.marks
        list.append(temp)
    print(count)
    multiply_total = temp['mark'] * count
    print(multiply_total)
    semester_number = getattr(course_instance, 'semester')
    department_id = getattr(course_instance, 'department_id')
    ug_or_pg = getattr(course_instance, 'programme')
    # student_list = student.objects.all().filter(department=department_id).filter(
    #     current_semester=semester_number).filter(qualification=ug_or_pg).order_by('roll_no')
    #
    # student_batch = ''
    # for stud in student_list:
    #     student_batch = stud.batch
    #     break
    #
    # # print(student_batch)
    # batch_split = str(student_batch).split('-')
    # current_batch = int(batch_split[0])
    student_list = get_list_of_students(instance.semester, course_instance)
    student_list = temp_filter_students(student_list, course_instance)
    current_batch = student_list[0]['student_obj'].batch.start_year
    co_numbers = []
    for each in list:
        if each['co'] not in co_numbers:
            co_numbers.append(each['co'])

    print(co_numbers)

    co_number_marks = []
    for co in co_numbers:
        mark_count = 0
        for each in list:
            if co == each['co']:
                mark_count = mark_count + each['mark']
        co_number_marks.append(mark_count)

    mark_list = []
    for stud in student_list:
        co_list = []
        temp_list = {}
        mark_arr = []
        mark_avg = []
        tot = 0
        cust_user_ins = CustomUser.objects.get(id=stud['student_obj'].user_id)
        if cust_user_ins.is_approved:
            for co in co_numbers:
                co_mark = 0
                stud_mark = 0
                internal_mark = 0
                for each in list:
                    if co == each['co']:
                        co_mark = co_mark + each['mark']
                        for data in lab_marks.objects.filter(lab_pattern_ref=each['id'], student=stud['student_obj']):
                            stud_mark = stud_mark + data.obtained_marks
                            tot += data.obtained_marks
                            internal_mark = internal_mark + data.internal_marks
                mark_arr.append(stud_mark)
                mark_avg.append(Decimal(format((stud_mark / co_mark) * 100, '.2f')).normalize())
            temp_list['studcomark'] = mark_arr
            temp_list['studcomarkavg'] = mark_avg
            temp_list['name'] = str(stud['student_obj'])
            temp_list['regno'] = stud['student_obj'].roll_no
            for each in list:
                for data in lab_marks.objects.filter(lab_pattern_ref=each['id'], student=stud['student_obj']):
                    co_list.append(data.obtained_marks)

            total_marks = -5
            avg = (tot / multiply_total) * 40
            avg = round(avg + 0.01)
            ca_mark = avg + internal_mark
            # avg=round(avg+0.01)
            """if current_batch >= 2016:
                for take in internalmarks.objects.filter(student=stud):
                    if int(unit_test_number) == 1:
                        total_marks = take.ut1
                    elif int(unit_test_number) == 2:
                        total_marks = take.ut2
                    elif int(unit_test_number) == 3:
                        total_marks = take.ut3
            else:
                for take in internalmarksold.objects.filter(student=stud):
                    if int(unit_test_number) == 1:
                        total_marks = take.ut1
                    elif int(unit_test_number) == 2:
                        total_marks = take.ut2
                    elif int(unit_test_number) == 3:
                        total_marks = take.ut3"""
            temp_list['tot'] = tot
            temp_list['avg'] = avg
            temp_list['internal_mark'] = internal_mark
            temp_list['ca_mark'] = ca_mark
            temp_list['total_marks'] = total_marks
            temp_list['data_list'] = co_list
            mark_list.append(temp_list)

            # for each in mark_list:
            #   print(each)

    print_current_batch = ''

    for stud in student_list:
        print_current_batch = stud['student_obj'].batch
        break

    column_width = 200 / (len(list) + len(co_numbers) + 1 + len(co_numbers))
    sec_stu = section_students.objects.get(student_id=student_list[0]['student_obj'].id)
    dep_sec = department_sections.objects.get(id=sec_stu.section_id)
    print(dep_sec.section_name)
    context_data = {
        'selected_course': course_instance,
        'student_list': mark_list,
        'question_list': list,
        'count': count,
        'co_list': co_numbers,
        'co_mark_list': co_number_marks,
        'co_total_marks': co_total_marks,
        'print_current_batch': print_current_batch,
        'current_batch': current_batch,
        'column_width': column_width,
        'department': curr_ins.department,
        'academic_year': academic_year,
        'multiply_total': multiply_total,
        'faculty': faculty_instance,
        'semester': semester_instance,
        'sem': sem,
        'section': dep_sec
    }

    return context_data


@login_required
@permission_required('marks.can_add_internal_marks')
def add_lab_marks(request):

    course_list = []
    faculty_instance = staff.objects.get(user=request.user)
    list_of_staff_course_ins = get_current_course_allotment_with_display_text(faculty_instance)
    for entry in list_of_staff_course_ins:
        if entry['staff_course_instance'].course.subject_type == 'P':
            course_list.append(entry)

    first_time = True

    if 'labMarksStaffCoursePk' in request.session:
        selected_course = request.session['labMarksStaffCoursePk']
        del request.session['labMarksStaffCoursePk']
        first_time = False

    if request.method == 'POST':
        if request.is_ajax():
            elective = False
            # got_utno = int(request.POST.get('unittestno'))
            got_course = str(request.POST.get('course'))
            sel_crs = str(request.POST.get('sel_crs'))
            print('sel_crs=' + str(sel_crs))
            curr_course = staff_course.objects.get(id=sel_crs)
            course_instance = curr_course.course

            instance = staff_course.objects.get(id=sel_crs)
            batch_instance = getattr(instance, 'batch')
            semester_instance = getattr(instance, 'semester')

            list = []
            dictionary = {}
            if lab_pattern.objects.filter(course=course_instance, batch=batch_instance,
                                          department=curr_course.department, semester=semester_instance).exists():
                dictionary['avail'] = 'true'
                for each in lab_pattern.objects.filter(course=course_instance, batch=batch_instance,
                                                       department=curr_course.department,
                                                       semester=semester_instance).order_by('question_number'):
                    temp = {}
                    temp['id'] = each.id
                    temp['quesno'] = each.user_prespective_question_number
                    temp['co'] = each.qn_mapping
                    temp['mark'] = each.marks
                    temp['internal'] = each.internal_marks
                    # for take in co_marks.objects.filter(paper_pattern_ref=each.id):
                    #   temp['studmark'] = take.obtained_marks
                    list.append(temp)

                dictionary['quescount'] = len(list)

                # print(list)

                semester_number = getattr(course_instance, 'semester')
                list_of_entries = []
                department_id = getattr(course_instance, 'department_id')
                ug_or_pg = getattr(course_instance, 'programme')

                # student_list = student.objects.all().filter(department=department_id).filter(
                #     current_semester=semester_number).filter(qualification=ug_or_pg).order_by('roll_no')
                student_list = get_list_of_students(instance.semester, course_instance)
                student_list = temp_filter_students(student_list, course_instance)
                # print(student_list[0]['student_obj'].batch.start_year)
                # student_batch = ''
                # for stud in student_list:
                #     student_batch = stud.batch
                #     break
                #
                # # print(student_batch)
                # batch_split = str(student_batch).split('-')
                # current_batch = int(batch_split[0])

                current_batch = student_list[0]['student_obj'].batch.start_year
                mark_list = []
                for stud in student_list:
                    marks_by_adding_co = 0
                    co_list = []
                    temp_list = {}
                    cust_user_ins = CustomUser.objects.get(id=stud['student_obj'].user_id)
                    if cust_user_ins.is_approved:
                        if elective:
                            if student_enrolled_courses.objects.filter(course=course_instance,
                                                                       student=stud.user.student, approved=True):
                                temp_list['id'] = stud['student_obj'].id
                                temp_list['name'] = str(stud['student_obj'])
                                temp_list['regno'] = stud['student_obj'].roll_no
                                for each in list:
                                    for data in lab_marks.objects.filter(lab_pattern_ref=each['id'],
                                                                         student=stud['student_obj']):
                                        co_list.append(data.obtained_marks)
                                        marks_by_adding_co = marks_by_adding_co + data.obtained_marks
                                        temp_list['internal_marks'] = data.internal_marks
                                temp_list['data_list'] = co_list

                                total_marks = -1

                                """if current_batch >= 2016:
                                    for take in internalmarks.objects.filter(student=stud):
                                        if got_utno == 1:
                                            total_marks = take.ut1
                                        elif got_utno == 2:
                                            total_marks = take.ut2
                                        elif got_utno == 3:
                                            total_marks = take.ut3
                                else:
                                    for take in internalmarksold.objects.filter(student=stud):
                                        if got_utno == 1:
                                            total_marks = take.ut1
                                        elif got_utno == 2:
                                            total_marks = take.ut2
                                        elif got_utno == 3:
                                            total_marks = take.ut3"""

                                if total_marks != -1:
                                    temp_list['total_marks'] = total_marks
                                else:
                                    temp_list['total_marks'] = 'Not entered '
                                if marks_by_adding_co == total_marks:
                                    temp_list['marks_equal_co'] = 'true'
                                temp_list['marks_obtained'] = marks_by_adding_co
                                mark_list.append(temp_list)
                        else:
                            temp_list['id'] = stud['student_obj'].id
                            temp_list['name'] = str(stud['student_obj'])
                            temp_list['regno'] = stud['student_obj'].roll_no
                            for each in list:
                                for data in lab_marks.objects.filter(lab_pattern_ref=each['id'],
                                                                     student=stud['student_obj']):
                                    co_list.append(data.obtained_marks)
                                    marks_by_adding_co = marks_by_adding_co + data.obtained_marks
                                    temp_list['internal_marks'] = data.internal_marks
                            temp_list['data_list'] = co_list

                            total_marks = -1

                            """if current_batch >= 2016:
                                for take in internalmarks.objects.filter(student=stud):
                                    if got_utno == 1:
                                        total_marks = take.ut1
                                    elif got_utno == 2:
                                        total_marks = take.ut2
                                    elif got_utno == 3:
                                        total_marks = take.ut3
                            else:
                                for take in internalmarksold.objects.filter(student=stud):
                                    if got_utno == 1:
                                        total_marks = take.ut1
                                    elif got_utno == 2:
                                        total_marks = take.ut2
                                    elif got_utno == 3:
                                        total_marks = take.ut3"""

                            if total_marks != -1:
                                temp_list['total_marks'] = total_marks
                            else:
                                temp_list['total_marks'] = 'Not entered '
                            if marks_by_adding_co == total_marks:
                                temp_list['marks_equal_co'] = 'true'
                            temp_list['marks_obtained'] = marks_by_adding_co
                            mark_list.append(temp_list)

                for each in mark_list:
                    print(each)

                # print(mark_list)

                dictionary['mark_list'] = mark_list

                dictionary['list'] = list
            else:
                dictionary['avail'] = 'false'

            return JsonResponse(dictionary)

        elif 'goback' in request.POST:
            sel_course = str(request.POST.get('gobackcourse'))
            selected_course = str(request.POST.get('sel_course_ins'))
            first_time = False
            sel_course_ins = staff_course.objects.get(id=selected_course)
        elif 'studid' in request.POST:
            # unit_test_number = int(request.POST.get('utno'))
            sel_course = str(request.POST.get('course'))
            selected_student = int(request.POST.get('studid'))
            question_count = int(request.POST.get('quescount'))
            internal_marks = int(request.POST.get('internal'))
            # convert internal marks from 100 to 10
            # internal_marks = old_school_round(internal_marks/10)
            first_time = False

            # print('unit_test_number='+str(unit_test_number))
            # print('selected_course='+str(selected_course))
            # print('selected_student='+str(selected_student))
            # print('question_count='+str(question_count))

            student_instance = student.objects.get(id=selected_student)

            counter = 1
            while counter <= question_count:
                pattern_id = str(request.POST.get('id' + str(counter)))
                mark_obtained = int(request.POST.get(str(counter)))
                # print('pattern_id=' + str(pattern_id))

                pattern_instance = lab_pattern.objects.get(id=pattern_id)

                try:
                    lab_marks.objects.create(
                        lab_pattern_ref=pattern_instance,
                        student=student_instance,
                        obtained_marks=mark_obtained,
                        internal_marks=internal_marks
                    )
                except:
                    lab_marks.objects.filter(lab_pattern_ref=pattern_instance, student=student_instance).update(
                        obtained_marks=mark_obtained, internal_marks=internal_marks)
                counter = counter + 1
            selected_course = str(request.POST.get('sel_course_ins'))
            sel_course_ins = staff_course.objects.get(id=selected_course)
        elif 'subject_code' in request.POST:
            selected_course = request.POST.get('subject_code')
            sel_course = staff_course.objects.get(id=selected_course)

            first_time = False
        else:
            sel_course_pk = request.POST.get('course')
            first_time = False
            sel_course = staff_course.objects.get(id=sel_course_pk)

            list_of_id = []
            course_instance = sel_course.course
            semester_number = sel_course.semester
            department_id = sel_course.department
            ug_or_pg = getattr(course_instance, 'programme')

            student_list = get_list_of_students(semester_number, course_instance)
            for each in student_list:
                curr_student = student.objects.get(roll_no=each['registration_number'])
                stu_section_ins = section_students.objects.get(student_id=curr_student.id)
                # student_list = student.objects.all().filter(department=department_id).filter(
                #     current_semester=semester_number).filter(qualification=ug_or_pg).order_by('roll_no')
                # for stud in student_list:
                list_of_id.append(each['student_obj'].id)

            required_edit = ''

            for each in list_of_id:
                if str(request.POST.get(str(each))) == 'Edit':
                    required_edit = int(each)

            stud = student.objects.get(id=required_edit)

            student_batch = getattr(stud, 'batch')
            batch_split = str(student_batch).split('-')
            current_batch = int(batch_split[0])

            student_avail = True
            total_marks = 0
            ut1_mark = 0
            ut2_mark = 0
            ut3_mark = 0
            for val in lab_pattern.objects.filter(course=course_instance):
                total_marks += val.marks

            instance = staff_course.objects.get(id=sel_course_pk)
            batch_instance = getattr(instance, 'batch')
            semester_instance = getattr(instance, 'semester')

            list = []
            for each in lab_pattern.objects.filter(course=course_instance, batch=batch_instance,
                                                   department=instance.department, semester=semester_instance
                                                   ).order_by('question_number'):
                temp = {}
                temp['id'] = each.id
                temp['quesno'] = each.user_prespective_question_number
                temp['co'] = each.qn_mapping
                temp['mark'] = each.marks
                for take in lab_marks.objects.filter(lab_pattern_ref=each.id, student=required_edit):
                    temp['studmark'] = take.obtained_marks
                temp['internal'] = each.internal_marks
                list.append(temp)

            return render(request, 'co_marks/updatelabmarks.html',
                          {'list_of_questions': list,
                           # 'unit_test_number': got_utno,
                           'student': stud, 'course': course_instance,
                           'student_avail': student_avail,
                           'total_marks': total_marks,
                           'no_of_questions': len(list),
                           'sel_course_ins': sel_course_pk
                           })

    if first_time:
        try:
            selected_course = course_list[0]['staff_course_instance'].pk
            # sel_course_ins = course_list[0]['staff_course_instance'].course_id
        except:
            msg = {
                'page_title': 'No courses',
                'title': 'No courses alloted',
                'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    staffCourseInstance = staff_course.objects.get(pk=selected_course)

    if staffCourseInstance.course.regulation.start_year == 2012:
        request.session['labMarksStaffCoursePk'] = selected_course
        return redirect(reverse('internalMarksOldRegulation'))

    return render(request, 'co_marks/add_lab_marks.html',
                  {'selected_course': (selected_course),
                   'course_list': course_list,
                   # 'sel_course_ins': sel_course_ins
                   # 'unit_test_number': str(unit_test_number)
                   })
