from datetime import datetime

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render
from easy_pdf.views import PDFTemplateView

from curriculum.views.common_includes import get_active_semesters_tabs_of_department, \
    get_active_semesters_tabs_for_overall_reports, get_course_allotments_for_overall_report, get_list_of_students
from accounts.models import staff, department, student
from curriculum.models import semester_migration, semester, staff_course, section_students, department_sections
from marks.models import internalmarks
from marks.views.internal_marks import temp_filter_students
from marks.common.util.internalMarksUtil import get_internal_marks_data_as_dictionary


@login_required
@permission_required('curriculum.can_view_overall_reports')
def overall_internals_report(request, semester_pk=None, selected_course=False, data_for_pdf=False):
    staff_instance = staff.objects.get(user=request.user)

    all_departments = department.objects.filter(is_core=True)
    user_is_pricipal_or_coe = False

    selected_department_pk = None
    if request.method == "POST" and 'selected_department_pk' in request.POST:
        selected_department_pk = int(request.POST.get('selected_department_pk'))
    if request.user.groups.filter(name='principal').exists() or request.user.groups.filter(name='coe_staff').exists():
        if not selected_department_pk:
            selected_department = all_departments.first()
            selected_department_pk = selected_department.pk
        else:
            selected_department = department.objects.get(pk=selected_department_pk)
        avaliable_semesters = get_active_semesters_tabs_of_department(selected_department)
        user_is_pricipal_or_coe = True
    else:
        avaliable_semesters = get_active_semesters_tabs_for_overall_reports(request)

    if not avaliable_semesters:
        modal = {
            'heading': 'Error',
            'body': 'No semester plan has been created for your access',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    if request.user.groups.filter(name='faculty_advisor').exists() and not \
            request.user.groups.filter(name='hod').exists() and not user_is_pricipal_or_coe:
        new_avaliable_semesters = []
        semesters_as_fa = semester_migration.objects.filter(
            semester__faculty_advisor=staff_instance
        )
        for entry in semesters_as_fa:
            new_avaliable_semesters.append(entry.semester)

        final_semesters = []
        for entry in avaliable_semesters:
            print(entry)
            sem_inst = semester.objects.get(pk=entry['semester_instance'].pk)
            if sem_inst in new_avaliable_semesters:
                print((sem_inst))
                final_semesters.append(entry)

        if final_semesters:
            avaliable_semesters = final_semesters

    selected_semester_pk = avaliable_semesters[0]['semester_instance'].pk

    if request.method == "POST" and 'semester_pk' in request.POST:
        selected_semester_pk = int(request.POST.get('semester_pk'))

    if semester_pk:
        selected_semester_pk = int(semester_pk)

    selected_semester = semester.objects.get(pk=selected_semester_pk)

    alloted_course_instances_of_selected_semester = staff_course.objects.filter(
        semester=selected_semester
    )

    course_allotment_list = get_course_allotments_for_overall_report(alloted_course_instances_of_selected_semester)

    if not selected_course:
        try:
            selected_course = course_allotment_list[0]['staff_course_instance'].pk
        except:
            msg = {
                'page_title': 'No courses',
                'title': 'No courses alloted',
                'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST' and request.POST.get('selected_allotment_pk'):
        selected_course = request.POST.get('selected_allotment_pk')
    # print('selected_course = ' + selected_course)
    selected_allotment_instance = staff_course.objects.get(pk=selected_course)
    faculty_id_list = []
    for each_staff in selected_allotment_instance.staffs.all():
        faculty_id_list.append(str(each_staff.id))
    if len(faculty_id_list) > 0:
        faculty_instance = staff.objects.get(id=faculty_id_list[0])
    else:
        faculty_instance = None
    # facul_instance=staff_course.staffs.objects.get(staff_course_id=selected_allotment_instance.id)
    # faculty_instance=staff.objects.get(id=facul_instance.staff_id)
    course_instance = selected_allotment_instance.course
    # timetable_instances = time_table.objects.filter(course=course_instance)
    #
    # pprint(timetable_instances)
    #
    # if not timetable_instances:
    #     msg = {
    #         'page_title': 'Allotment Error',
    #         'title': 'Timetable not Found',
    #         'description': 'No timetable has been assigned to this course',
    #     }
    #     return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    semester_plan_instance = selected_allotment_instance.semester

    curr_course = staff_course.objects.get(id=selected_course)
    course_instance = curr_course.course
    isonecredit = course_instance.is_one_credit
    semester_number = getattr(course_instance, 'semester')
    list_of_entries = []
    department_id = getattr(course_instance, 'department_id')
    # print(semester)

    ug_or_pg = getattr(course_instance, 'programme')
    print(ug_or_pg)
    student_list = get_list_of_students(curr_course.semester, course_instance)
    student_list = temp_filter_students(student_list, course_instance)
    for each in student_list:
        curr_student = student.objects.get(roll_no=each['registration_number'])
        stu_section_ins = section_students.objects.get(student_id=curr_student.id)

    # student_list = student.objects.all().filter(department=department_id).filter(
    #    current_semester=semester_number).filter(qualification=ug_or_pg).order_by('roll_no')
    current_year = datetime.now()
    academic_year = int(current_year.year)
    # batch_list = student_list.batch__start_year;
    # current_batch = batch_list[0]['batch__start_year']
    # print(current_batch)
    print('Student list after query :')
    # print(vars(student_list))
    current_batch = course_instance.regulation.start_year
    print("CURRENT BATCH: " + str(current_batch))
    for stud in student_list:
        list_of_entries.append(get_internal_marks_data_as_dictionary(stud['student_obj'], course_instance,
                                                                     selected_allotment_instance.semester))

    print('Each student list : ')
    print(list_of_entries)
    print(student_list)
    try:
        sec_stu = section_students.objects.get(student_id=student_list[0]['student_obj'].id)
    except:
        msg = {
            'page_title': 'No students',
            'title': 'No students',
            'description': 'No students are available in this semester',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    dep_sec = department_sections.objects.get(id=sec_stu.section_id)
    if data_for_pdf:
        context_data = {
            'selected_course': course_instance,
            'student_list': list_of_entries,
            'current_batch': current_batch,
            'academic_year': academic_year,
            'faculty': faculty_instance,
            'semester': semester_number,
            # 'sel_crs_ins': sel_crs_ins,
            'dep_sec': dep_sec
        }
        return context_data


    return render(request, 'marks/overall_internals_report.html',
                  {
                      'all_departments': all_departments,
                      'selected_department_pk': selected_allotment_instance .department.pk,
                      'semesters': avaliable_semesters,
                      'student_list': list_of_entries,
                      'selected_semester_pk': selected_semester_pk,
                      'faculty': faculty_instance,
                      'current_batch': current_batch,
                      'course_allotment_list': course_allotment_list,
                      'selected_allotment_pk': int(selected_course),
                      'selected_course': course_instance,
                  })


class overallInternalsPdfReport(PDFTemplateView):
    template_name = "marks/internal_pdf_report.html"
    selected_course = None

    def get(self, request, *args, **kwargs):
        self.selected_course = kwargs.pop('course_id')
        return super(overallInternalsPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = overall_internals_report(self.request,
                                                selected_course=self.selected_course,
                                                data_for_pdf=True
                                                )
        print(context_data)
        return super(overallInternalsPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )
