from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, redirect
from easy_pdf.views import PDFTemplateView
from django.core.urlresolvers import reverse

from accounts.models import staff
from curriculum.models import staff_course
from marks.models import LabMarks2012

from curriculum.views.common_includes import get_current_course_allotment_with_display_text, get_list_of_students

from marks.common.util.internalMarksUtil import get_internal_marks_data_as_dictionary


@login_required
@permission_required('curriculum.can_mark_attendance')
def internalMarksOldRegulation(request):
    staffInstance = staff.objects.get(user=request.user)

    courseList = []
    for each in get_current_course_allotment_with_display_text(staffInstance):
        if each['staff_course_instance'].course.subject_type == 'P':
            courseList.append(each)

    try:
        staffCoursePk = courseList[0]['staff_course_instance'].pk
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a 2012 regulation lab courses that have quiz marks',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if 'labMarksStaffCoursePk' in request.session:
        staffCoursePk = request.session['labMarksStaffCoursePk']
        del request.session['labMarksStaffCoursePk']

    if request.method == 'POST':
        if 'staffCoursePk' in request.POST:
            staffCoursePk = request.POST.get('staffCoursePk')
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

        if 'getSection' in request.POST:
            staffCoursePk = request.POST.get('hiddenStaffCoursePk')
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            updateSection = request.POST.get('updateSection')

            listOfStudents = get_list_of_students(staffCourseInstance.semester, staffCourseInstance.course)

            studentList = []
            for eachStudent in listOfStudents:
                temp = {}
                temp['student'] = eachStudent['student_obj']
                for instance in LabMarks2012.objects.filter(student=eachStudent['student_obj'],
                                                            staffCourse=staffCourseInstance):
                    if updateSection == 'modals':
                        temp['mark'] = instance.modalMarks
                    if updateSection == 'record':
                        temp['mark'] = instance.recordMarks
                studentList.append(temp)

            maximumMarks = 40
            if updateSection == 'modals':
                maximumMarks = 100

            return render(request, 'marks/updateLabSection.html',
                          {
                              'studentList': studentList,
                              'staffCourseInstance': staffCourseInstance,
                              'updateSection': updateSection,
                              'maximumMarks': maximumMarks
                          })

        if 'updateMarks' in request.POST:
            updateSection = str(request.POST.get('hiddenUpdateSection'))
            staffCoursePk = request.POST.get('hiddenStaffCoursePk')
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            listOfStudents = get_list_of_students(staffCourseInstance.semester, staffCourseInstance.course)

            for student in listOfStudents:
                obtained_mark = request.POST.get(str(student['student_obj'].pk))
                if LabMarks2012.objects.filter(student=student['student_obj'], staffCourse=staffCourseInstance):
                    labMarksInstance = LabMarks2012.objects.get(student=student['student_obj'],
                                                                staffCourse=staffCourseInstance)
                    if obtained_mark:
                        if updateSection == 'modals':
                            labMarksInstance.modalMarks = obtained_mark
                        if updateSection == 'record':
                            labMarksInstance.recordMarks = obtained_mark
                    else:
                        if updateSection == 'modals':
                            labMarksInstance.modalMarks = None
                        if updateSection == 'record':
                            labMarksInstance.recordMarks = None

                    labMarksInstance.save()

                else:
                    labMarksInstance = LabMarks2012(student=student['student_obj'], staffCourse=staffCourseInstance)
                    labMarksInstance.save()
                    if obtained_mark:
                        if updateSection == 'modals':
                            labMarksInstance.modalMarks = obtained_mark
                        if updateSection == 'record':
                            labMarksInstance.recordMarks = obtained_mark
                    else:
                        if updateSection == 'modals':
                            labMarksInstance.modalMarks = None
                        if updateSection == 'record':
                            labMarksInstance.recordMarks = None

                    labMarksInstance.save()
    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    if staffCourseInstance.course.regulation.start_year == 2016:
        request.session['labMarksStaffCoursePk'] = staffCoursePk
        return redirect(reverse('add_lab_marks'))

    listOfStudents = get_list_of_students(staffCourseInstance.semester, staffCourseInstance.course)

    studentList = []
    for student in listOfStudents:
        studentList.append(get_internal_marks_data_as_dictionary(student['student_obj'], staffCourseInstance.course,
                                                                 staffCourseInstance.semester))

    return render(request, 'marks/internalMarksOldRegulationLab.html', {
        'staffCoursePk': int(staffCoursePk),
        'courseList': courseList,
        'studentList': studentList,
        'selected_course':staffCourseInstance.course
    })


def getinternalMarksOldRegulation(request, staffCoursePk):
    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    listOfStudents = get_list_of_students(staffCourseInstance.semester, staffCourseInstance.course)

    studentList = []
    for student in listOfStudents:
        studentList.append(get_internal_marks_data_as_dictionary(student['student_obj'], staffCourseInstance.course,
                                                                 staffCourseInstance.semester))

    try:
        studentDepartment = listOfStudents[0]['student_obj'].department
        context_data = {
            'studentList': studentList,
            'staffCourseInstance': staffCourseInstance,
            'studentDepartment': studentDepartment
        }
    except:
        context_data = {
            'studentList': studentList,
            'staffCourseInstance': staffCourseInstance,
        }

    return context_data


class internalMarksOldRegulationLabPdfReport(PDFTemplateView):
    template_name = "marks/internalMarksOldRegulationLabPdfReports.html"
    staffCoursePk = None

    def get(self, request, *args, **kwargs):
        self.staffCoursePk = kwargs.pop('staffCoursePk')
        return super(internalMarksOldRegulationLabPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = getinternalMarksOldRegulation(self.request, staffCoursePk=self.staffCoursePk)
        return super(internalMarksOldRegulationLabPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )
