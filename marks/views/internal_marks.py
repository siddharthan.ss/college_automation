import csv

from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from easy_pdf.views import PDFTemplateView

from accounts.views import sendEmail
from curriculum.views import allowed_course
from marks.forms import internal_mark_update_form, internal_mark_update_form_old, one_credit_course_update_form

from accounts.models import staff, student
from curriculum.models import staff_course, section_students, department_sections, courses, semester
from marks.models import internalmarksold, internalmarks, onecreditcourse

from curriculum.views.common_includes import get_current_course_allotment_with_display_text, get_list_of_students

from datetime import datetime

from marks.common.util.internalMarksUtil import get_internal_marks_data_as_dictionary


def temp_filter_students(student_list, course_inst):
    temp_student_list = []
    if course_inst.course_id == '16MES2Z9' \
            or course_inst.course_id == '16MES206' \
            or course_inst.course_id == '16MES2Z5' \
            or course_inst.course_id == '16CES2Z5' \
            or course_inst.course_id == '16CES2Z5' \
            or course_inst.course_id == '16CES2Z9':
        for entry in student_list:
            if entry['registration_number'] == "1512147" \
                    or entry['registration_number'] == "1512148" \
                    or entry['registration_number'] == "1511198":
                continue
            else:
                temp_student_list.append(entry)
        return temp_student_list
    else:
        return student_list


def get_internal_marks(student_list, semester_instance, course_instance, update_section):
    list_of_students = []

    if course_instance.is_one_credit:
        if update_section == 'UT 1':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in onecreditcourse.objects.filter(course=course_instance, semester=semester_instance,
                                                               student=each_student['student_obj']):
                    temp['mark'] = instance.ut1
                list_of_students.append(temp)

        if update_section == 'UT 2':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in onecreditcourse.objects.filter(course=course_instance,
                                                               semester=semester_instance,
                                                               student=each_student['student_obj']):
                    temp['mark'] = instance.ut2
                list_of_students.append(temp)

        if update_section == 'ReUT':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in onecreditcourse.objects.filter(course=course_instance,
                                                               semester=semester_instance,
                                                               student=each_student['student_obj']):
                    temp['mark'] = instance.reut
                list_of_students.append(temp)

    else:
        if update_section == 'UT 1':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in internalmarks.objects.filter(course=course_instance, semester=semester_instance,
                                                             student=each_student['student_obj']):
                    temp['mark'] = instance.ut1
                list_of_students.append(temp)

        if update_section == 'UT 2':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in internalmarks.objects.filter(course=course_instance,
                                                             semester=semester_instance,
                                                             student=each_student['student_obj']):
                    temp['mark'] = instance.ut2
                list_of_students.append(temp)

        if update_section == 'UT 3':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in internalmarks.objects.filter(course=course_instance,
                                                             semester=semester_instance,
                                                             student=each_student['student_obj']):
                    temp['mark'] = instance.ut3
                list_of_students.append(temp)

        if update_section == 'ReUT':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in internalmarks.objects.filter(course=course_instance,
                                                             semester=semester_instance,
                                                             student=each_student['student_obj']):
                    temp['mark'] = instance.reut
                list_of_students.append(temp)

        if update_section == 'ASSIGNMENT 1':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in internalmarks.objects.filter(course=course_instance,
                                                             semester=semester_instance,
                                                             student=each_student['student_obj']):
                    temp['mark'] = instance.ass1
                list_of_students.append(temp)

        if update_section == 'ASSIGNMENT 2':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in internalmarks.objects.filter(course=course_instance,
                                                             semester=semester_instance,
                                                             student=each_student['student_obj']):
                    temp['mark'] = instance.ass2
                list_of_students.append(temp)

        if update_section == 'ASSIGNMENT 3':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in internalmarks.objects.filter(course=course_instance,
                                                             semester=semester_instance,
                                                             student=each_student['student_obj']):
                    temp['mark'] = instance.ass3
                list_of_students.append(temp)

        if update_section == 'TUTORIAL 1':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in internalmarks.objects.filter(course=course_instance,
                                                             semester=semester_instance,
                                                             student=each_student['student_obj']):
                    temp['mark'] = instance.tutorial_or_quiz_1
                list_of_students.append(temp)

        if update_section == 'TUTORIAL 2':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in internalmarks.objects.filter(course=course_instance,
                                                             semester=semester_instance,
                                                             student=each_student['student_obj']):
                    temp['mark'] = instance.tutorial_or_quiz_2
                list_of_students.append(temp)

        if update_section == 'TUTORIAL 3':
            for each_student in student_list:
                temp = {}
                temp['student'] = each_student['student_obj']
                for instance in internalmarks.objects.filter(course=course_instance,
                                                             semester=semester_instance,
                                                             student=each_student['student_obj']):
                    temp['mark'] = instance.tutorial_or_quiz_3
                list_of_students.append(temp)

    return list_of_students


def get_internal_marks_old(student_list, semester_instance, course_instance, update_section):
    list_of_students = []

    if update_section == 'UT 1':
        for each_student in student_list:
            temp = {}
            temp['student'] = each_student['student_obj']
            for instance in internalmarksold.objects.filter(course=course_instance, semester=semester_instance,
                                                            student=each_student['student_obj']):
                temp['mark'] = instance.ut1
            list_of_students.append(temp)

    if update_section == 'UT 2':
        for each_student in student_list:
            temp = {}
            temp['student'] = each_student['student_obj']
            for instance in internalmarksold.objects.filter(course=course_instance,
                                                            semester=semester_instance,
                                                            student=each_student['student_obj']):
                temp['mark'] = instance.ut2
            list_of_students.append(temp)

    if update_section == 'UT 3':
        for each_student in student_list:
            temp = {}
            temp['student'] = each_student['student_obj']
            for instance in internalmarksold.objects.filter(course=course_instance,
                                                            semester=semester_instance,
                                                            student=each_student['student_obj']):
                temp['mark'] = instance.ut3
            list_of_students.append(temp)

    if update_section == 'ReUT':
        for each_student in student_list:
            temp = {}
            temp['student'] = each_student['student_obj']
            for instance in internalmarksold.objects.filter(course=course_instance,
                                                            semester=semester_instance,
                                                            student=each_student['student_obj']):
                temp['mark'] = instance.reut
            list_of_students.append(temp)

    if update_section == 'ASSIGNMENT 1':
        for each_student in student_list:
            temp = {}
            temp['student'] = each_student['student_obj']
            for instance in internalmarksold.objects.filter(course=course_instance,
                                                            semester=semester_instance,
                                                            student=each_student['student_obj']):
                temp['mark'] = instance.ass1
            list_of_students.append(temp)

    if update_section == 'ASSIGNMENT 2':
        for each_student in student_list:
            temp = {}
            temp['student'] = each_student['student_obj']
            for instance in internalmarksold.objects.filter(course=course_instance,
                                                            semester=semester_instance,
                                                            student=each_student['student_obj']):
                temp['mark'] = instance.ass2
            list_of_students.append(temp)

    if update_section == 'ASSIGNMENT 3':
        for each_student in student_list:
            temp = {}
            temp['student'] = each_student['student_obj']
            for instance in internalmarksold.objects.filter(course=course_instance,
                                                            semester=semester_instance,
                                                            student=each_student['student_obj']):
                temp['mark'] = instance.ass3
            list_of_students.append(temp)

    if update_section == 'ATTENDANCE':
        for each_student in student_list:
            temp = {}
            temp['student'] = each_student['student_obj']
            for instance in internalmarksold.objects.filter(course=course_instance,
                                                            semester=semester_instance,
                                                            student=each_student['student_obj']):
                temp['mark'] = instance.att
            list_of_students.append(temp)

    return list_of_students


@login_required
@permission_required('curriculum.can_mark_attendance')
def internal_marks(request, selected_course=False, data_for_pdf=False):
    # print('data_for_pdf = ' + str(data_for_pdf))
    current_year = datetime.now()
    sel_crs_ins = False
    academic_year = int(current_year.year)
    faculty_instance = staff.objects.get(user=request.user)
    list_of_staff_course_ins = get_current_course_allotment_with_display_text(faculty_instance)
    course_list = []
    if 'current_course' in request.session:
        selected_course = request.session['current_course']
        sel_crs_ins = staff_course.objects.get(id=selected_course)

    for entry in list_of_staff_course_ins:
        if entry['staff_course_instance'].course.subject_type == 'T':
            course_list.append(entry)

    if not selected_course:
        try:
            selected_course = course_list[0]['staff_course_instance'].pk
            sel_crs_ins = staff_course.objects.get(id=selected_course)
        except:
            msg = {
                'page_title': 'No courses',
                'title': 'No courses alloted',
                'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course. For practicals click on Set Lab Pattern from the side pane to set pattern for practicals and then click on Add Lab Marks to add marks',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'subject_code' in request.POST:
            selected_course = request.POST.get('subject_code')
            sel_crs_ins = staff_course.objects.get(id=selected_course)
            request.session['current_course'] = selected_course

        if 'get_section' in request.POST:
            selected_course = request.POST.get('hidden_staff_course_pk')
            update_section = request.POST.get('update_section')
            sel_crs_ins = staff_course.objects.get(pk=selected_course)

            course_instance = sel_crs_ins.course
            semester_instance = sel_crs_ins.semester

            student_list = get_list_of_students(semester_instance, course_instance)

            if course_instance.regulation.start_year >= 2016:
                if update_section == 'UT 1' or update_section == 'UT 2' or update_section == 'UT 3' or update_section == 'ReUT':
                    maximum_marks = 50
                if update_section == 'ASSIGNMENT 1' or update_section == 'ASSIGNMENT 2' or update_section == 'ASSIGNMENT 3':
                    maximum_marks = 15
                if update_section == 'TUTORIAL 1' or update_section == 'TUTORIAL 2' or update_section == 'TUTORIAL 3':
                    maximum_marks = 5

                list_of_students = get_internal_marks(student_list, semester_instance, course_instance, update_section)
            else:
                if update_section == 'UT 1' or update_section == 'UT 2' or update_section == 'UT 3' or update_section == 'ReUT':
                    maximum_marks = 50
                if update_section == 'ASSIGNMENT 1' or update_section == 'ASSIGNMENT 2' or update_section == 'ASSIGNMENT 3':
                    maximum_marks = 10
                if update_section == 'ATTENDANCE':
                    maximum_marks = 10

                list_of_students = get_internal_marks_old(student_list, semester_instance, course_instance,
                                                          update_section)

            return render(request, 'marks/update_mark_section.html',
                          {
                              'list_of_students': list_of_students,
                              'staff_course_instance': sel_crs_ins,
                              'update_section': update_section,
                              'maximum_marks': maximum_marks
                          })

        if 'update_marks' in request.POST:
            update_section = str(request.POST.get('hidden_update_section'))
            selected_course = request.POST.get('hidden_staff_course_pk')
            sel_crs_ins = staff_course.objects.get(pk=selected_course)

            course_instance = sel_crs_ins.course
            semester_instance = sel_crs_ins.semester
            batch_instance = sel_crs_ins.batch
            department_instance = sel_crs_ins.department
            student_list = get_list_of_students(semester_instance, course_instance)

            if course_instance.regulation.start_year >= 2016:
                if update_section == 'UT 1':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if course_instance.is_one_credit:
                            if onecreditcourse.objects.filter(semester=semester_instance, course=course_instance,
                                                              student=each_student['student_obj']):
                                onecreditcourseinstance = onecreditcourse.objects.get(semester=semester_instance,
                                                                                      course=course_instance,
                                                                                      student=each_student[
                                                                                          'student_obj'])
                                if obtained_mark:
                                    onecreditcourseinstance.ut1 = obtained_mark
                                else:
                                    onecreditcourseinstance.ut1 = None
                                onecreditcourseinstance.save()

                            else:
                                onecreditcourseinstance = onecreditcourse(semester=semester_instance,
                                                                          batch=batch_instance,
                                                                          department=department_instance,
                                                                          staff=faculty_instance,
                                                                          course=course_instance,
                                                                          student=each_student['student_obj'])
                                if obtained_mark:
                                    onecreditcourseinstance.ut1 = obtained_mark
                                else:
                                    onecreditcourseinstance.ut1 = None
                                onecreditcourseinstance.save()
                        else:
                            if internalmarks.objects.filter(semester=semester_instance, course=course_instance,
                                                            student=each_student['student_obj']):
                                internal_marks_instance = internalmarks.objects.get(semester=semester_instance,
                                                                                    course=course_instance,
                                                                                    student=each_student['student_obj'])
                                if obtained_mark:
                                    internal_marks_instance.ut1 = obtained_mark
                                else:
                                    internal_marks_instance.ut1 = None
                                internal_marks_instance.save()
                            else:
                                internal_marks_instance = internalmarks(semester=semester_instance,
                                                                        batch=batch_instance,
                                                                        department=department_instance,
                                                                        course=course_instance,
                                                                        student=each_student['student_obj'],
                                                                        staff=faculty_instance)
                                if obtained_mark:
                                    internal_marks_instance.ut1 = obtained_mark
                                else:
                                    internal_marks_instance.ut1 = None
                                internal_marks_instance.save()

                if update_section == 'UT 2':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if course_instance.is_one_credit:
                            if onecreditcourse.objects.filter(semester=semester_instance, course=course_instance,
                                                              student=each_student['student_obj']):
                                onecreditcourseinstance = onecreditcourse.objects.get(semester=semester_instance,
                                                                                      course=course_instance,
                                                                                      student=each_student[
                                                                                          'student_obj'])
                                if obtained_mark:
                                    onecreditcourseinstance.ut2 = obtained_mark
                                else:
                                    onecreditcourseinstance.ut2 = None
                                onecreditcourseinstance.save()

                            else:
                                onecreditcourseinstance = onecreditcourse(semester=semester_instance,
                                                                          batch=batch_instance,
                                                                          department=department_instance,
                                                                          staff=faculty_instance,
                                                                          course=course_instance,
                                                                          student=each_student['student_obj'])
                                if obtained_mark:
                                    onecreditcourseinstance.ut2 = obtained_mark
                                else:
                                    onecreditcourseinstance.ut2 = None
                                onecreditcourseinstance.save()
                        else:
                            if internalmarks.objects.filter(semester=semester_instance, course=course_instance,
                                                            student=each_student['student_obj']):
                                internal_marks_instance = internalmarks.objects.get(semester=semester_instance,
                                                                                    course=course_instance,
                                                                                    student=each_student['student_obj'])
                                if obtained_mark:
                                    internal_marks_instance.ut2 = obtained_mark
                                else:
                                    internal_marks_instance.ut2 = None
                                internal_marks_instance.save()
                            else:
                                internal_marks_instance = internalmarks(semester=semester_instance,
                                                                        batch=batch_instance,
                                                                        department=department_instance,
                                                                        course=course_instance,
                                                                        student=each_student['student_obj'],
                                                                        staff=faculty_instance)
                                if obtained_mark:
                                    internal_marks_instance.ut2 = obtained_mark
                                else:
                                    internal_marks_instance.ut2 = None
                                internal_marks_instance.save()

                if update_section == 'UT 3':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarks.objects.filter(semester=semester_instance, course=course_instance,
                                                        student=each_student['student_obj']):
                            internal_marks_instance = internalmarks.objects.get(semester=semester_instance,
                                                                                course=course_instance,
                                                                                student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.ut3 = obtained_mark
                            else:
                                internal_marks_instance.ut3 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarks(semester=semester_instance, batch=batch_instance,
                                                                    department=department_instance,
                                                                    course=course_instance,
                                                                    student=each_student['student_obj'],
                                                                    staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.ut3 = obtained_mark
                            else:
                                internal_marks_instance.ut3 = None
                            internal_marks_instance.save()

                if update_section == 'ReUT':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if course_instance.is_one_credit:
                            if onecreditcourse.objects.filter(semester=semester_instance, course=course_instance,
                                                              student=each_student['student_obj']):
                                onecreditcourseinstance = onecreditcourse.objects.get(semester=semester_instance,
                                                                                      course=course_instance,
                                                                                      student=each_student[
                                                                                          'student_obj'])
                                if obtained_mark:
                                    onecreditcourseinstance.ut3 = obtained_mark
                                else:
                                    onecreditcourseinstance.ut3 = None
                                onecreditcourseinstance.save()

                            else:
                                onecreditcourseinstance = onecreditcourse(semester=semester_instance,
                                                                          batch=batch_instance,
                                                                          department=department_instance,
                                                                          staff=faculty_instance,
                                                                          course=course_instance,
                                                                          student=each_student['student_obj'])
                                if obtained_mark:
                                    onecreditcourseinstance.ut3 = obtained_mark
                                else:
                                    onecreditcourseinstance.ut3 = None
                                onecreditcourseinstance.save()
                        else:
                            if internalmarks.objects.filter(semester=semester_instance, course=course_instance,
                                                            student=each_student['student_obj']):
                                internal_marks_instance = internalmarks.objects.get(semester=semester_instance,
                                                                                    course=course_instance,
                                                                                    student=each_student['student_obj'])
                                if obtained_mark:
                                    internal_marks_instance.reut = obtained_mark
                                else:
                                    internal_marks_instance.reut = None
                                internal_marks_instance.save()
                            else:
                                internal_marks_instance = internalmarks(semester=semester_instance,
                                                                        batch=batch_instance,
                                                                        department=department_instance,
                                                                        course=course_instance,
                                                                        student=each_student['student_obj'],
                                                                        staff=faculty_instance)
                                if obtained_mark:
                                    internal_marks_instance.reut = obtained_mark
                                else:
                                    internal_marks_instance.reut = None
                                internal_marks_instance.save()

                if update_section == 'ASSIGNMENT 1':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarks.objects.filter(semester=semester_instance, course=course_instance,
                                                        student=each_student['student_obj']):
                            internal_marks_instance = internalmarks.objects.get(semester=semester_instance,
                                                                                course=course_instance,
                                                                                student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.ass1 = obtained_mark
                            else:
                                internal_marks_instance.ass1 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarks(semester=semester_instance, batch=batch_instance,
                                                                    department=department_instance,
                                                                    course=course_instance,
                                                                    student=each_student['student_obj'],
                                                                    staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.ass1 = obtained_mark
                            else:
                                internal_marks_instance.ass1 = None
                            internal_marks_instance.save()

                if update_section == 'ASSIGNMENT 2':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarks.objects.filter(semester=semester_instance, course=course_instance,
                                                        student=each_student['student_obj']):
                            internal_marks_instance = internalmarks.objects.get(semester=semester_instance,
                                                                                course=course_instance,
                                                                                student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.ass2 = obtained_mark
                            else:
                                internal_marks_instance.ass2 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarks(semester=semester_instance, batch=batch_instance,
                                                                    department=department_instance,
                                                                    course=course_instance,
                                                                    student=each_student['student_obj'],
                                                                    staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.ass2 = obtained_mark
                            else:
                                internal_marks_instance.ass2 = None
                            internal_marks_instance.save()

                if update_section == 'ASSIGNMENT 3':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarks.objects.filter(semester=semester_instance, course=course_instance,
                                                        student=each_student['student_obj']):
                            internal_marks_instance = internalmarks.objects.get(semester=semester_instance,
                                                                                course=course_instance,
                                                                                student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.ass3 = obtained_mark
                            else:
                                internal_marks_instance.ass3 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarks(semester=semester_instance, batch=batch_instance,
                                                                    department=department_instance,
                                                                    course=course_instance,
                                                                    student=each_student['student_obj'],
                                                                    staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.ass3 = obtained_mark
                            else:
                                internal_marks_instance.ass3 = None
                            internal_marks_instance.save()

                if update_section == 'TUTORIAL 1':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarks.objects.filter(semester=semester_instance, course=course_instance,
                                                        student=each_student['student_obj']):
                            internal_marks_instance = internalmarks.objects.get(semester=semester_instance,
                                                                                course=course_instance,
                                                                                student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.tutorial_or_quiz_1 = obtained_mark
                            else:
                                internal_marks_instance.tutorial_or_quiz_1 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarks(semester=semester_instance, batch=batch_instance,
                                                                    department=department_instance,
                                                                    course=course_instance,
                                                                    student=each_student['student_obj'],
                                                                    staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.tutorial_or_quiz_1 = obtained_mark
                            else:
                                internal_marks_instance.tutorial_or_quiz_1 = None
                            internal_marks_instance.save()

                if update_section == 'TUTORIAL 2':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarks.objects.filter(semester=semester_instance, course=course_instance,
                                                        student=each_student['student_obj']):
                            internal_marks_instance = internalmarks.objects.get(semester=semester_instance,
                                                                                course=course_instance,
                                                                                student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.tutorial_or_quiz_2 = obtained_mark
                            else:
                                internal_marks_instance.tutorial_or_quiz_2 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarks(semester=semester_instance, batch=batch_instance,
                                                                    department=department_instance,
                                                                    course=course_instance,
                                                                    student=each_student['student_obj'],
                                                                    staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.tutorial_or_quiz_2 = obtained_mark
                            else:
                                internal_marks_instance.tutorial_or_quiz_2 = None
                            internal_marks_instance.save()

                if update_section == 'TUTORIAL 3':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarks.objects.filter(semester=semester_instance, course=course_instance,
                                                        student=each_student['student_obj']):
                            internal_marks_instance = internalmarks.objects.get(semester=semester_instance,
                                                                                course=course_instance,
                                                                                student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.tutorial_or_quiz_3 = obtained_mark
                            else:
                                internal_marks_instance.tutorial_or_quiz_3 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarks(semester=semester_instance, batch=batch_instance,
                                                                    department=department_instance,
                                                                    course=course_instance,
                                                                    student=each_student['student_obj'],
                                                                    staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.tutorial_or_quiz_3 = obtained_mark
                            else:
                                internal_marks_instance.tutorial_or_quiz_3 = None
                            internal_marks_instance.save()

            else:
                if update_section == 'UT 1':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarksold.objects.filter(semester=semester_instance, course=course_instance,
                                                           student=each_student['student_obj']):
                            internal_marks_instance = internalmarksold.objects.get(semester=semester_instance,
                                                                                   course=course_instance,
                                                                                   student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.ut1 = obtained_mark
                            else:
                                internal_marks_instance.ut1 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarksold(semester=semester_instance, batch=batch_instance,
                                                                       department=department_instance,
                                                                       course=course_instance,
                                                                       student=each_student['student_obj'],
                                                                       staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.ut1 = obtained_mark
                            else:
                                internal_marks_instance.ut1 = None
                            internal_marks_instance.save()

                if update_section == 'UT 2':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarksold.objects.filter(semester=semester_instance, course=course_instance,
                                                           student=each_student['student_obj']):
                            internal_marks_instance = internalmarksold.objects.get(semester=semester_instance,
                                                                                   course=course_instance,
                                                                                   student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.ut2 = obtained_mark
                            else:
                                internal_marks_instance.ut2 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarksold(semester=semester_instance, batch=batch_instance,
                                                                       department=department_instance,
                                                                       course=course_instance,
                                                                       student=each_student['student_obj'],
                                                                       staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.ut2 = obtained_mark
                            else:
                                internal_marks_instance.ut2 = None
                            internal_marks_instance.save()

                if update_section == 'UT 3':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarksold.objects.filter(semester=semester_instance, course=course_instance,
                                                           student=each_student['student_obj']):
                            internal_marks_instance = internalmarksold.objects.get(semester=semester_instance,
                                                                                   course=course_instance,
                                                                                   student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.ut3 = obtained_mark
                            else:
                                internal_marks_instance.ut3 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarksold(semester=semester_instance, batch=batch_instance,
                                                                       department=department_instance,
                                                                       course=course_instance,
                                                                       student=each_student['student_obj'],
                                                                       staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.ut3 = obtained_mark
                            else:
                                internal_marks_instance.ut3 = None
                            internal_marks_instance.save()

                if update_section == 'ReUT':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarksold.objects.filter(semester=semester_instance, course=course_instance,
                                                           student=each_student['student_obj']):
                            internal_marks_instance = internalmarksold.objects.get(semester=semester_instance,
                                                                                   course=course_instance,
                                                                                   student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.reut = obtained_mark
                            else:
                                internal_marks_instance.reut = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarksold(semester=semester_instance, batch=batch_instance,
                                                                       department=department_instance,
                                                                       course=course_instance,
                                                                       student=each_student['student_obj'],
                                                                       staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.reut = obtained_mark
                            else:
                                internal_marks_instance.reut = None
                            internal_marks_instance.save()

                if update_section == 'ASSIGNMENT 1':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarksold.objects.filter(semester=semester_instance, course=course_instance,
                                                           student=each_student['student_obj']):
                            internal_marks_instance = internalmarksold.objects.get(semester=semester_instance,
                                                                                   course=course_instance,
                                                                                   student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.ass1 = obtained_mark
                            else:
                                internal_marks_instance.ass1 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarksold(semester=semester_instance, batch=batch_instance,
                                                                       department=department_instance,
                                                                       course=course_instance,
                                                                       student=each_student['student_obj'],
                                                                       staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.ass1 = obtained_mark
                            else:
                                internal_marks_instance.ass1 = None
                            internal_marks_instance.save()

                if update_section == 'ASSIGNMENT 2':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarksold.objects.filter(semester=semester_instance, course=course_instance,
                                                           student=each_student['student_obj']):
                            internal_marks_instance = internalmarksold.objects.get(semester=semester_instance,
                                                                                   course=course_instance,
                                                                                   student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.ass2 = obtained_mark
                            else:
                                internal_marks_instance.ass2 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarksold(semester=semester_instance, batch=batch_instance,
                                                                       department=department_instance,
                                                                       course=course_instance,
                                                                       student=each_student['student_obj'],
                                                                       staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.ass2 = obtained_mark
                            else:
                                internal_marks_instance.ass2 = None
                            internal_marks_instance.save()

                if update_section == 'ASSIGNMENT 3':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarksold.objects.filter(semester=semester_instance, course=course_instance,
                                                           student=each_student['student_obj']):
                            internal_marks_instance = internalmarksold.objects.get(semester=semester_instance,
                                                                                   course=course_instance,
                                                                                   student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.ass3 = obtained_mark
                            else:
                                internal_marks_instance.ass3 = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarksold(semester=semester_instance, batch=batch_instance,
                                                                       department=department_instance,
                                                                       course=course_instance,
                                                                       student=each_student['student_obj'],
                                                                       staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.ass3 = obtained_mark
                            else:
                                internal_marks_instance.ass3 = None
                            internal_marks_instance.save()

                if update_section == 'ATTENDANCE':
                    for each_student in student_list:
                        obtained_mark = request.POST.get(str(each_student['student_obj'].pk))
                        if internalmarksold.objects.filter(semester=semester_instance, course=course_instance,
                                                           student=each_student['student_obj']):
                            internal_marks_instance = internalmarksold.objects.get(semester=semester_instance,
                                                                                   course=course_instance,
                                                                                   student=each_student['student_obj'])
                            if obtained_mark:
                                internal_marks_instance.att = obtained_mark
                            else:
                                internal_marks_instance.att = None
                            internal_marks_instance.save()
                        else:
                            internal_marks_instance = internalmarksold(semester=semester_instance, batch=batch_instance,
                                                                       department=department_instance,
                                                                       course=course_instance,
                                                                       student=each_student['student_obj'],
                                                                       staff=faculty_instance)
                            if obtained_mark:
                                internal_marks_instance.att = obtained_mark
                            else:
                                internal_marks_instance.att = None
                            internal_marks_instance.save()

    # print('selected_course = ' + str(selected_course))
    curr_course = staff_course.objects.get(id=selected_course)
    course_instance = curr_course.course
    # print('course_instance=' + str(course_instance))
    isonecredit = course_instance.is_one_credit
    semester_number = getattr(course_instance, 'semester')
    list_of_entries = []
    department_id = getattr(course_instance, 'department_id')
    # print(semester)

    ug_or_pg = getattr(course_instance, 'programme')
    student_list = get_list_of_students(curr_course.semester, course_instance)
    # a temporary filter to remove 3 students from 3 subjects
    student_list = temp_filter_students(student_list, course_instance)
    for each in student_list:
        curr_student = student.objects.get(roll_no=each['registration_number'])
        stu_section_ins = section_students.objects.get(student_id=curr_student.id)

    current_batch = course_instance.regulation.start_year
    for stud in student_list:
        list_of_entries.append(
            get_internal_marks_data_as_dictionary(stud['student_obj'], course_instance, curr_course.semester))

    try:
        sec_stu = section_students.objects.get(student_id=student_list[0]['student_obj'].id)
        dep_sec = department_sections.objects.get(id=sec_stu.section_id)
        if data_for_pdf:
            context_data = {
                'selected_course': course_instance,
                'student_list': list_of_entries,
                'current_batch': current_batch,
                'academic_year': academic_year,
                'faculty': faculty_instance,
                'semester': semester_number,
                'sel_crs_ins': sel_crs_ins,
                'dep_sec': dep_sec
            }
            return context_data
    except:
        pass

    if data_for_pdf:
        context_data = {
            'selected_course': course_instance,
            'student_list': list_of_entries,
            'current_batch': current_batch,
            'academic_year': academic_year,
            'faculty': faculty_instance,
            'semester': semester_number,
            'sel_crs_ins': sel_crs_ins,
        }
        return context_data

    # print(selected_course)
    return render(request, 'marks/internal_marks.html',
                  {'course_list': course_list, 'selected_course': int(selected_course),
                   'student_list': list_of_entries, 'isonecredit': isonecredit,
                   'course_id': course_instance, 'current_batch': current_batch,
                   'academic_year': academic_year, 'faculty': faculty_instance,
                   'semester': semester_number, 'sel_crs_ins': sel_crs_ins
                   })


from django.views.generic.detail import SingleObjectTemplateResponseMixin
from django.views.generic.edit import ModelFormMixin, ProcessFormView


class CreateUpdateView(SingleObjectTemplateResponseMixin, ModelFormMixin,
                       ProcessFormView):
    template_name = 'marks/updatemarks.html'
    success_url = '/internal_marks'
    form_class = internal_mark_update_form

    def dispatch(self, request, *args, **kwargs):
        if request.user.has_perm('curriculum.can_mark_attendance'):
            staff_ins = staff.objects.get(user=self.request.user)
            course_id = self.request.GET.get('course')
            course_ins = courses.objects.get(pk=course_id)
            check = allowed_course(course_ins, staff_ins)
            if check == False:
                msg = {
                    'page_title': 'Permission denied',
                    'title': 'Permission denied',
                    'description': 'You dont have permissions to view this page!',
                }
                mail = {
                    'subject': 'Illegal access',
                    'message': str(staff_ins) + ' tried to update marks for another subject ' + str(
                        course_ins.course_id) + '-' + str(
                        course_ins.course_name) + ' using URL.\n\n\nSTAFF DETAILS:\n\nName: ' + str(
                        staff_ins) + '\nStaff ID: ' + str(staff_ins.staff_id) + '\nGender: ' + str(
                        staff_ins.gender) + '\nDepartment: ' + str(
                        staff_ins.department.name) + '\nPhone Number: ' + str(staff_ins.phone_number),
                    'email': 'automatorsgct@googlegroups.com'
                }
                # print(mail['message'])
                sendEmail(mail)
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
        if not request.user.has_perm('curriculum.can_mark_attendance'):
            student_ins = student.objects.get(user_id=request.user.id)
            course_id = self.request.GET.get('course')
            course_ins = courses.objects.get(pk=course_id)
            # print(student_ins.roll_no)
            msg = {
                'page_title': 'Permission denied',
                'title': 'Permission denied',
                'description': 'You dont have permissions to view this page!',
            }
            mail = {
                'subject': 'Illegal access',
                'message': str(student_ins) + ' tried to update marks for ' + str(course_ins.course_id) + '-' + str(
                    course_ins.course_name) + ' using URL.\n\n\nSTUDENT DETAILS:\n\nName: ' + str(
                    student_ins) + '\nRoll No: ' + str(student_ins.roll_no) + '\nGender: ' + str(
                    student_ins.gender) + '\nCurrent Semester: ' + str(
                    student_ins.current_semester) + '\nEntry type: ' + str(
                    student_ins.entry_type) + '\nStudent type: ' + str(
                    student_ins.student_type) + '\nDepartment: ' + str(
                    student_ins.department.name) + '\nPhone Number: ' + str(student_ins.phone_number),
                'email': 'automatorsgct@googlegroups.com'
            }
            # print(mail['message'])
            sendEmail(mail)
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
        return super(CreateUpdateView, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        student_instance = student.objects.get(roll_no=self.request.GET.get('roll_no'))
        student_batch = student_instance.batch.start_year
        course_id = self.request.GET.get('course')
        start_year = courses.objects.get(pk=course_id).regulation.start_year
        isonecredit = courses.objects.get(pk=course_id).is_one_credit
        # print("start_year " + str(start_year))
        # print("course instance" + course_id)
        # print(vars(student_instance))
        # print("student batch=")
        # print(student_batch)
        if (start_year < 2016):
            # self.model = internalmarksold
            self.template_name = "marks/updatemarksold.html"
            self.form_class = internal_mark_update_form_old
        if (isonecredit == 1):
            self.form_class = one_credit_course_update_form
        try:
            if (isonecredit == 1):
                obj = onecreditcourse.objects.filter(student=student_instance).get(
                    course=self.request.GET.get('course'))
                return obj
            else:
                obj = internalmarks.objects.filter(student=student_instance).get(
                    course__pk=self.request.GET.get('course'))
                # print(vars(obj))
                return obj

        except:
            try:
                obj = internalmarksold.objects.filter(student=student_instance).get(
                    course__pk=self.request.GET.get('course'))
                # print("old batch :")
                # print(vars(obj))
                return obj
            except:
                return None

    def form_valid(self, form):
        student_instance = student.objects.get(roll_no=self.request.GET.get('roll_no'))
        form.instance.student = student_instance

        student_department = getattr(student_instance, 'department')
        form.instance.department = student_department

        student_batch = getattr(student_instance, 'batch')
        form.instance.batch = student_batch

        # student_current_semester_number=get_current_semester_plan_of_student(student_instance)

        # student_current_semester_number = getattr(student_instance, 'current_semester')
        # form.instance.semester = semester.objects.filter(department=student_department).filter(batch=student_batch).get(
        #    semester_number=student_current_semester_number)
        # form.instance.semester=self.request.GET.get('semester')

        sem_instance = semester.objects.get(pk=int(self.request.GET.get('semester')))
        form.instance.semester = sem_instance
        course_instance = courses.objects.get(pk=self.request.GET.get('course'))
        form.instance.course = course_instance

        staff_instance = staff.objects.get(user=self.request.user)
        form.instance.staff = staff_instance

        self.object = form.save()

        return HttpResponseRedirect(self.get_success_url())

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(CreateUpdateView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(CreateUpdateView, self).post(request, *args, **kwargs)


class internalsPdfReport(PDFTemplateView):
    template_name = "marks/internal_pdf_report.html"
    selected_course = None

    def get(self, request, *args, **kwargs):
        self.selected_course = kwargs.pop('course_id')
        # print('course pdf ' + self.selected_course)
        return super(internalsPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = internal_marks(self.request,
                                      selected_course=self.selected_course,
                                      data_for_pdf=True
                                      )
        print(context_data)
        return super(internalsPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )

@login_required
@permission_required('curriculum.can_mark_attendance')
def internalsCsvReport(request,selected_course):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="users.csv"'

    writer = csv.writer(response)
    writer.writerow(['Username', 'First name', 'Last name', 'Email address'])

    context_data = internal_marks(request,
                                  selected_course=selected_course,
                                  data_for_pdf=True
                                  )

    if context_data['current_batch'] >= 2016:
        if context_data['selected_course'].is_one_credit:
            for student_instance in context_data['student_list']:
                writer.writerow(
                    student_instance['registration_number'],

                )
        else:
            for student_instance in context_data['student_list']:
                writer.writerow(
                    student_instance['registration_number'],
                    student_instance['student_name'],
                    student_instance['internal_mark_instance'].ut1,
                    student_instance['internal_mark_instance'].ut2,
                    student_instance['internal_mark_instance'].ut3,
                )

    return response