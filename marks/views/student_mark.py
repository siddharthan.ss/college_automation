from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import render

from accounts.models import student
from curriculum.models import courses, staff_course
from feedback.models import FeedbackByStudents, FeedbackFormat
from marks.models import student_approved_internals_mark
from curriculum.common.util.courseUtil import get_coures_currently_studying_by_student
from marks.common.util.internalMarksUtil import get_internal_marks_data_as_dictionary
from common.utils.studentUtil import getSemesterPlanofStudent


@login_required
@permission_required('curriculum.can_choose_courses')
def student_internal_marks(request):
    student_instance = student.objects.get(user=request.user)
    semester_instance = getSemesterPlanofStudent(student_instance)

    list_of_courses = get_coures_currently_studying_by_student(student_instance)

    try:
        selected_course = str(list_of_courses[0].pk)
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses Found',
            'description': 'Contact your Programme co-ordinator for more information',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'selected_tab' in request.POST:
            selected_course = str(request.POST.get('selected_tab'))

    course_instance = courses.objects.get(pk=selected_course)
    staff_course_instance = staff_course.objects.get(
        semester=getSemesterPlanofStudent(student_instance),
        course=course_instance
    )

    internal_marks_data = get_internal_marks_data_as_dictionary(student_instance, course_instance, semester_instance)

    return render(request, 'marks/student_internal_marks.html', {
        'list_of_courses': list_of_courses,
        'selected_course': selected_course,
        'internal_marks_data': internal_marks_data,
        'staff_course_instance': (staff_course_instance),
        'student_instance': student_instance
    })


@login_required
@permission_required('curriculum.can_choose_courses')
def approve_internal_marks(request):
    data = {}
    if request.method == "POST":
        if 'staff_course_pk' in request.POST and 'approve' in request.POST:
            staff_course_pk = request.POST['staff_course_pk']
            staff_course_instance = staff_course.objects.get(pk=staff_course_pk)
            # check whether feedback has been provided
            FeedbackByStudents_query = FeedbackByStudents.objects.filter(
                student=request.user.get_student_instance(),
                staffCourse=staff_course_instance
            )

            if not FeedbackFormat.objects.filter(isLive=True).exists():
                data['message'] = "Error : Feedback form questions not set"
                return JsonResponse(data)
            FeedbackFormat_instance = FeedbackFormat.objects.get(
                isLive=True
            )
            if not FeedbackByStudents_query.count() == FeedbackFormat_instance.noOfQuestions:
                data['message'] = "Please fill your feedback for this subject before approving"
            else:
                student_approved_internals_mark.objects.create(
                    staff_course=staff_course_instance,
                    student=request.user.get_student_instance(),
                    approved_mark=get_internal_marks_data_as_dictionary(
                        request.user.get_student_instance(),
                        staff_course_instance.course,
                        staff_course_instance.semester,
                    )['final_assesment_mark']
                )
                data['message'] = "success"
            return JsonResponse(data)
    data['message'] = "Something went wrong"
    return JsonResponse(data)
