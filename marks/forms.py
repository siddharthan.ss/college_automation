from django import forms

from marks.models import internalmarks, internalmarksold, onecreditcourse


class internal_mark_update_form(forms.ModelForm):
    class Meta:
        model = internalmarks
        fields = ['ut1', 'ut2', 'ut3', 'reut', 'ass1', 'ass2', 'ass3', 'tutorial_or_quiz_1', 'tutorial_or_quiz_2',
                  'tutorial_or_quiz_3']

    def __init__(self, *args, **kwargs):
        super(internal_mark_update_form, self).__init__(*args, **kwargs)
        form_fields = ['ut1', 'ut2', 'ut3', 'reut', 'ass1', 'ass2', 'ass3', 'tutorial_or_quiz_1', 'tutorial_or_quiz_2',
                       'tutorial_or_quiz_3']
        for field in form_fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })


class one_credit_course_update_form(forms.ModelForm):
    class Meta:
        model = onecreditcourse
        fields = ['ut1', 'ut2', 'reut']

    def __init__(self, *args, **kwargs):
        super(one_credit_course_update_form, self).__init__(*args, **kwargs)
        form_fields = ['ut1', 'ut2', 'reut']
        for field in form_fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })


class internal_mark_update_form_old(forms.ModelForm):
    class Meta:
        model = internalmarksold
        fields = ['ut1', 'ut2', 'ut3', 'reut', 'ass1', 'ass2', 'ass3']

    def __init__(self, *args, **kwargs):
        super(internal_mark_update_form_old, self).__init__(*args, **kwargs)
        form_fields = ['ut1', 'ut2', 'ut3', 'reut', 'ass1', 'ass2', 'ass3']
        for field in form_fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })
