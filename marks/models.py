from django.db import models
from simple_history.models import HistoricalRecords

from accounts.models import student, batch, staff
from curriculum.models import department, courses, semester, staff_course


# Create your models here.


class internalmarks(models.Model):
    student = models.ForeignKey(student)
    ut1 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Unit Test 1', null=True)
    ut2 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Unit Test 2', null=True)
    ut3 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Unit Test 3', null=True)
    reut = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Re Unit Test', null=True)
    ass1 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Assignment 1', null=True)
    ass2 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Assignment 2', null=True)
    ass3 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Assignment 3', null=True)
    tutorial_or_quiz_1 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Tutorial or Quiz 1',
                                                          null=True)
    tutorial_or_quiz_2 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Tutorial or Quiz 2',
                                                          null=True)
    tutorial_or_quiz_3 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Tutorial or Quiz 3',
                                                          null=True)
    course = models.ForeignKey(courses)
    semester = models.ForeignKey(semester)
    department = models.ForeignKey(department)
    staff = models.ForeignKey(staff)
    batch = models.ForeignKey(batch)
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            'course', 'semester', 'student',
        )
        permissions = (
            ('can_view_internal_marks', 'Can view internal marks'),
            ('can_add_internal_marks', 'Can add internal marks'),
            # ('can_view_own_internal_marks', 'Can view own internal marks')
        )


class internalmarksold(models.Model):
    student = models.ForeignKey(student)
    ut1 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Unit Test 1', null=True)
    ut2 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Unit Test 2', null=True)
    ut3 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Unit Test 3', null=True)
    reut = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Re Unit Test', null=True)
    ass1 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Assignment 1', null=True)
    ass2 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Assignment 2', null=True)
    ass3 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Assignment 3', null=True)
    att = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Attendance', null=True)
    course = models.ForeignKey(courses)
    semester = models.ForeignKey(semester)
    department = models.ForeignKey(department)
    staff = models.ForeignKey(staff)
    batch = models.ForeignKey(batch)
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            'course', 'semester', 'student',
        )
        permissions = (
            ('can_view_internal_marks', 'Can view internal marks'),
            ('can_add_internal_marks', 'Can add internal marks'),
            # ('can_view_own_internal_marks', 'Can view own internal marks')
        )


class onecreditcourse(models.Model):
    student = models.ForeignKey(student)
    ut1 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Unit Test 1', null=True)
    ut2 = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Unit Test 2', null=True)
    reut = models.PositiveSmallIntegerField(default=0, blank=True, verbose_name='Re Unit Test', null=True)
    course = models.ForeignKey(courses)
    semester = models.ForeignKey(semester)
    department = models.ForeignKey(department)
    staff = models.ForeignKey(staff)
    batch = models.ForeignKey(batch)
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            'course', 'semester', 'student',
        )
        permissions = (
            ('can_view_internal_marks', 'Can view internal marks'),
            ('can_add_internal_marks', 'Can add internal marks'),
            # ('can_view_own_internal_marks', 'Can view own internal marks')
        )


class paper_pattern(models.Model):
    course = models.ForeignKey(courses)
    batch = models.ForeignKey(batch)
    department = models.ForeignKey(department)
    semester = models.ForeignKey(semester)
    ut_num = models.PositiveSmallIntegerField()
    question_number = models.PositiveSmallIntegerField()
    user_prespective_question_number = models.CharField(max_length=3)
    co_mapping = models.PositiveSmallIntegerField()
    marks = models.PositiveSmallIntegerField()
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            'course', 'batch', 'department', 'semester', 'ut_num', 'question_number'
        )


class lab_pattern(models.Model):
    course = models.ForeignKey(courses)
    batch = models.ForeignKey(batch)
    department = models.ForeignKey(department)
    semester = models.ForeignKey(semester)
    question_number = models.PositiveSmallIntegerField()
    user_prespective_question_number = models.CharField(max_length=3)
    qn_mapping = models.TextField()
    marks = models.PositiveSmallIntegerField()
    internal_marks = models.PositiveSmallIntegerField()

    class Meta:
        unique_together = (
            'course', 'semester', 'question_number'
        )


class co_marks(models.Model):
    paper_pattern_ref = models.ForeignKey(paper_pattern)
    student = models.ForeignKey(student)
    obtained_marks = models.PositiveSmallIntegerField()
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            'paper_pattern_ref', 'student'
        )


class lab_marks(models.Model):
    lab_pattern_ref = models.ForeignKey(lab_pattern)
    student = models.ForeignKey(student)
    obtained_marks = models.PositiveSmallIntegerField()
    internal_marks = models.PositiveSmallIntegerField()
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            'lab_pattern_ref', 'student'
        )


class co_weightage(models.Model):
    staff_course = models.ForeignKey(staff_course)
    ut_num = models.PositiveSmallIntegerField()
    co_mapping = models.PositiveSmallIntegerField()
    weightage = models.PositiveSmallIntegerField()


class student_approved_internals_mark(models.Model):
    staff_course = models.ForeignKey(staff_course)
    student = models.ForeignKey(student)
    approved_mark = models.PositiveSmallIntegerField()

    class Meta:
        unique_together = (
            'staff_course', 'student'
        )


class LabMarks2012(models.Model):
    staffCourse = models.ForeignKey(staff_course)
    student = models.ForeignKey(student)
    modalMarks = models.PositiveSmallIntegerField(null=True, blank=True)
    recordMarks = models.PositiveSmallIntegerField(null=True, blank=True)

    class Meta:
        unique_together = ('staffCourse', 'student')
