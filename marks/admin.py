from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

from marks.models import paper_pattern, co_marks, internalmarks, internalmarksold, onecreditcourse, \
    student_approved_internals_mark, lab_pattern


class PaperPatternAdmin(admin.ModelAdmin):
    list_display = (
    'semester', 'course', 'batch', 'departments', 'ut_num', 'question_number', 'user_prespective_question_number',
    'co_mapping', 'marks')
    list_filter = ('course__regulation', 'department', 'ut_num', 'course__programme', 'batch', 'semester')
    search_fields = (
        'course__course_id', 'id', 'course__course_name')

    ordering = ('semester', 'course__course_id', 'course__course_name', 'id')

    def departments(self, obj):
        return obj.department.acronym


class CoMarksAdmin(admin.ModelAdmin):
    list_display = (
    'semester', 'course', 'batch', 'department', 'ut_num', 'question_number', 'user_prespective_question_number',
    'co_mapping', 'marks', 'student', 'obtained_marks')
    list_filter = (
    'paper_pattern_ref__course__regulation', 'paper_pattern_ref__department', 'paper_pattern_ref__ut_num',
    'paper_pattern_ref__course__programme', 'paper_pattern_ref__batch', 'paper_pattern_ref__semester')
    search_fields = (
        'paper_pattern_ref__course__course_id', 'id', 'paper_pattern_ref__course__course_name', 'student__first_name',
        'student__middle_name', 'student__last_name', 'student__roll_no', 'student__user__email')

    ordering = (
    'paper_pattern_ref__semester', 'paper_pattern_ref__course__course_id', 'paper_pattern_ref__course__course_name',
    'paper_pattern_ref__ut_num', 'student__roll_no', 'paper_pattern_ref__question_number', 'id')

    def semester(self, obj):
        return obj.paper_pattern_ref.semester

    def course(self, obj):
        return obj.paper_pattern_ref.course

    def batch(self, obj):
        return obj.paper_pattern_ref.batch

    def department(self, obj):
        return obj.paper_pattern_ref.department.acronym

    def ut_num(self, obj):
        return obj.paper_pattern_ref.ut_num

    def question_number(self, obj):
        return obj.paper_pattern_ref.question_number

    def user_prespective_question_number(self, obj):
        return obj.paper_pattern_ref.user_prespective_question_number

    def co_mapping(self, obj):
        return obj.paper_pattern_ref.co_mapping

    def marks(self, obj):
        return obj.paper_pattern_ref.marks


class InternalMarksAdmin(admin.ModelAdmin):
    list_display = (
    'semester', 'course', 'batch', 'departments', 'staff', 'student', 'ut1', 'ut2', 'ut3', 'reut', 'ass1', 'ass2',
    'ass3', 'tutorial_or_quiz_1', 'tutorial_or_quiz_2', 'tutorial_or_quiz_3')
    list_filter = ('course__regulation', 'department', 'course__programme', 'batch', 'semester')
    search_fields = (
        'course__course_id', 'id', 'course__course_name', 'student__first_name', 'student__middle_name',
        'student__last_name', 'student__roll_no', 'student__user__email', 'staff__first_name', 'staff__middle_name',
        'staff__last_name', 'staff__user__email')

    ordering = ('semester', 'course__course_id', 'course__course_name', 'student', 'id')

    def departments(self, obj):
        return obj.department.acronym


class InternalMarksOldAdmin(admin.ModelAdmin):
    list_display = (
    'semester', 'course', 'batch', 'departments', 'staff', 'student', 'ut1', 'ut2', 'ut3', 'reut', 'ass1', 'ass2',
    'ass3', 'att')
    list_filter = ('course__regulation', 'department', 'course__programme', 'batch', 'semester')
    search_fields = (
        'course__course_id', 'id', 'course__course_name', 'student__first_name', 'student__middle_name',
        'student__last_name', 'student__roll_no', 'student__user__email', 'staff__first_name', 'staff__middle_name',
        'staff__last_name', 'staff__user__email')

    ordering = ('semester', 'course__course_id', 'course__course_name', 'student', 'id')

    def departments(self, obj):
        return obj.department.acronym


admin.site.register(paper_pattern, PaperPatternAdmin)
admin.site.register(co_marks, CoMarksAdmin)
admin.site.register(internalmarks, InternalMarksAdmin)
admin.site.register(internalmarksold, InternalMarksOldAdmin)
admin.site.register(onecreditcourse, SimpleHistoryAdmin)
admin.site.register(student_approved_internals_mark, SimpleHistoryAdmin)
admin.site.register(lab_pattern)
