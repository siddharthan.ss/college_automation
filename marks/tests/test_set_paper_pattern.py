import unittest
from datetime import datetime as dt

from django.test import Client
from django.test import TestCase

from accounts.models import staff, CustomUser
from curriculum.models import courses
from curriculum.views import get_current_course_allotment_with_display_text

@unittest.skip('to be updated')
class setPaperPatternTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'student.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_access_without_login(self):
        url_string = '/set_paper_pattern/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='co_marks/create_paper_pattern.html')

    def test_access_student(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/set_paper_pattern/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='co_marks/create_paper_pattern.html')

    def test_access_faculty(self):
        # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        faculty_inst = staff.objects.get(user_id=cust_inst.id)
        courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        theory_list = []
        for entry in courses_handled:
            if entry['staff_course_instance'].course.subject_type == 'T':
                theory_list.append(entry)
        url_string = '/set_paper_pattern/'
        response = self.client.get(url_string)
        if not theory_list:
            self.assertTemplateNotUsed(response, template_name='co_marks/create_paper_pattern.html')
            self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
        else:
            self.assertTemplateUsed(response, template_name='co_marks/create_paper_pattern.html')
            self.assertTemplateNotUsed(response, template_name='prompt_pages/error_page_base.html')

    def test_get_method(self):
        # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)
        current_year = dt.now()
        academic_year = int(current_year.year)
        faculty_inst = staff.objects.get(user_id=cust_inst.id)
        courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        theory_list = []
        for entry in courses_handled:
            if entry['staff_course_instance'].course.subject_type == 'T':
                theory_list.append(entry)

        url_string = '/set_paper_pattern/'
        response = self.client.get(url_string)
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course. For practicals click on Set Lab Pattern from the side pane to set pattern for practicals and then click on Add Lab Marks to add marks',
        }
        if not theory_list:
            self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
            self.assertEqual(response.context['message'], msg)
        else:
            selected_course = theory_list[0]['subject_code']
            course_obj = courses.objects.get(course_id=selected_course)
            subject_total_marks = course_obj.CAT_marks
            self.assertTemplateUsed(response, template_name='marks/internal_marks.html')
            self.assertEqual(response.context['course_list'], theory_list)
            self.assertEqual(response.context['selected_course'], selected_course)
            self.assertEqual(response.context['subject_total_marks'], subject_total_marks)

    def test_post_select_dept(self):
        # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)
        faculty_inst = staff.objects.get(user_id=cust_inst.id)
        courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        theory_list = []
        for entry in courses_handled:
            if entry['staff_course_instance'].course.subject_type == 'T':
                theory_list.append(entry)
        if len(theory_list) > 1:
            other_course = theory_list[1]['subject_code']
            response = self.client.post('/set_paper_pattern/', {u'subject_code': [other_course]})
            self.assertEqual(response.status_code, 200)
            response = self.client.post('/set_paper_pattern/', {u'question_count': [u'5']})

    def test_post_delete_pattern(self):
        # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)
        faculty_inst = staff.objects.get(user_id=cust_inst.id)
        courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        theory_list = []
        for entry in courses_handled:
            if entry['staff_course_instance'].course.subject_type == 'T':
                theory_list.append(entry)
        if len(theory_list) > 1:
            other_course = theory_list[1]['subject_code']
            response = self.client.post('/set_paper_pattern/', {u'subject_code': [other_course]})
            self.assertEqual(response.status_code, 200)
            response = self.client.post('/set_paper_pattern/', {u'delcourse': [other_course]})
            response = self.client.post('/set_paper_pattern/', {u'delutno': [u'1']})
