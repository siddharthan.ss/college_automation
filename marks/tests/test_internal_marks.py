from datetime import datetime as dt

from django.test import Client
from django.test import TestCase

from accounts.models import staff, student, CustomUser
from curriculum.models import courses
from curriculum.views import get_list_of_students, get_current_course_allotment_with_display_text
from marks.models import onecreditcourse, internalmarks, internalmarksold


class addInternalsTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'student.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_access_without_login(self):
        url_string = '/internal_marks/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='marks/internal_marks.html')

    def test_access_student(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/internal_marks/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='marks/internal_marks.html')

    def test_access_faculty(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        faculty_inst = staff.objects.get(user_id=cust_inst.id)
        # courses_handled=get_coureses_handled_by_faculty_this_sem(faculty_inst)
        courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        theory_list = []
        for entry in courses_handled:
            if entry['staff_course_instance'].course.subject_type == 'T':
                theory_list.append(entry)
        url_string = '/internal_marks/'
        response = self.client.get(url_string)
        if not theory_list:
            self.assertTemplateNotUsed(response, template_name='marks/internal_marks.html')
            self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
        else:
            self.assertTemplateUsed(response, template_name='marks/internal_marks.html')
            self.assertTemplateNotUsed(response, template_name='prompt_pages/error_page_base.html')

    # def test_access_faculty_no_alloted_courses(self):
    #     cust_inst = CustomUser.objects.get(email='f2@gmail.com')
    #     cust_inst.is_approved = True
    #     cust_inst.save()
    #
    #     logged = self.client.login(email=cust_inst.email, password='123456')
    #     self.assertTrue(logged)
    #
    #     faculty_inst = staff.objects.get(user_id=cust_inst.id)
    #     courses_handled = get_coureses_handled_by_faculty_this_sem(faculty_inst)
    #     url_string = '/internal_marks/'
    #     response = self.client.get(url_string)
    #     if not courses_handled:
    #         self.assertTemplateNotUsed(response, template_name='marks/internal_marks.html')
    #         self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
    #     else:
    #         self.assertTemplateUsed(response, template_name='marks/internal_marks.html')
    #         self.assertTemplateNotUsed(response, template_name='prompt_pages/error_page_base.html')
    #
    # def test_access_faculty_no_alloted_theory_courses(self):
    #     cust_inst = CustomUser.objects.get(email='f2@gmail.com')
    #     cust_inst.is_approved = True
    #     cust_inst.save()
    #
    #     logged = self.client.login(email=cust_inst.email, password='123456')
    #     self.assertTrue(logged)
    #
    #     faculty_inst=staff.objects.get(user_id=cust_inst.id)
    #     courses_handled=get_coureses_handled_by_faculty_this_sem(faculty_inst)
    #     theory_list=[]
    #     for entry in courses_handled:
    #         if entry['subject_type'] == 'T':
    #             theory_list.append(entry)
    #     url_string = '/internal_marks/'
    #     response = self.client.get(url_string)
    #     if not theory_list:
    #         self.assertTemplateNotUsed(response, template_name='marks/internal_marks.html')
    #         self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
    #     else:
    #         self.assertTemplateUsed(response, template_name='marks/internal_marks.html')
    #         self.assertTemplateNotUsed(response, template_name='prompt_pages/error_page_base.html')
    #
    # def test_access_faculty_alloted_theory_courses(self):
    #     cust_inst = CustomUser.objects.get(email='hod@gmail.com')
    #     cust_inst.is_approved = True
    #     cust_inst.save()
    #
    #     logged = self.client.login(email=cust_inst.email, password='123456')
    #     self.assertTrue(logged)
    #
    #     faculty_inst=staff.objects.get(user_id=cust_inst.id)
    #     courses_handled=get_coureses_handled_by_faculty_this_sem(faculty_inst)
    #     theory_list=[]
    #     for entry in courses_handled:
    #         if entry['subject_type'] == 'T':
    #             theory_list.append(entry)
    #     url_string = '/internal_marks/'
    #     response = self.client.get(url_string)
    #     if not theory_list:
    #         self.assertTemplateNotUsed(response, template_name='marks/internal_marks.html')
    #         self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
    #     else:
    #         self.assertTemplateUsed(response, template_name='marks/internal_marks.html')
    #         self.assertTemplateNotUsed(response, template_name='prompt_pages/error_page_base.html')

    def test_get_method(self):
        # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)
        current_year = dt.now()
        academic_year = int(current_year.year)
        faculty_inst = staff.objects.get(user_id=cust_inst.id)
        courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        theory_list = []
        for entry in courses_handled:
            if entry['staff_course_instance'].course.subject_type == 'T':
                theory_list.append(entry)
        url_string = '/internal_marks/'
        response = self.client.get(url_string)
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course. For practicals click on Set Lab Pattern from the side pane to set pattern for practicals and then click on Add Lab Marks to add marks',
        }
        if not theory_list:
            self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
            self.assertEqual(response.context['message'], msg)
        else:
            selected_course = theory_list[0]['subject_code']
            course_instance = courses.objects.get(course_id=selected_course)
            current_batch = course_instance.regulation.start_year
            semester_number = getattr(course_instance, 'semester')
            student_list = get_list_of_students(course_instance)
            list_of_entries = []
            if course_instance.regulation.start_year >= 2016:
                for stud in student_list:
                    temp_list = {}
                    temp_list['student_name'] = stud["student_obj"]
                    student_register_number = stud["student_obj"].roll_no
                    temp_list['registration_number'] = student_register_number
                    if course_instance.is_one_credit == 1:
                        try:
                            internal_marks_instance = onecreditcourse.objects.filter(student=stud["student_obj"]).get(
                                course=course_instance)
                            temp_list['internal_mark_instance'] = internal_marks_instance
                            if internal_marks_instance.ut1 <= internal_marks_instance.ut2 and internal_marks_instance.ut1 <= internal_marks_instance.reut:
                                smallest = internal_marks_instance.ut1
                            elif internal_marks_instance.ut2 <= internal_marks_instance.ut1 and internal_marks_instance.ut2 <= internal_marks_instance.reut:
                                smallest = internal_marks_instance.ut2
                            else:
                                smallest = internal_marks_instance.reut
                            temp_list[
                                'total_internal_marks'] = internal_marks_instance.ut1 + internal_marks_instance.ut2 + internal_marks_instance.reut - smallest
                            temp_list['final_assesment_mark'] = (temp_list['total_internal_marks'] / 2)
                        except:
                            pass
                    else:
                        try:
                            internal_marks_instance = internalmarks.objects.filter(student=stud["student_obj"]).get(
                                course=course_instance)
                            temp_list['internal_mark_instance'] = internal_marks_instance
                            if internal_marks_instance.ut1 <= internal_marks_instance.ut2 and internal_marks_instance.ut1 <= internal_marks_instance.ut3 and internal_marks_instance.ut1 <= internal_marks_instance.reut:
                                smallest = internal_marks_instance.ut1
                            elif internal_marks_instance.ut2 <= internal_marks_instance.ut1 and internal_marks_instance.ut2 <= internal_marks_instance.ut3 and internal_marks_instance.ut2 <= internal_marks_instance.reut:
                                smallest = internal_marks_instance.ut2
                            elif internal_marks_instance.ut3 <= internal_marks_instance.ut1 and internal_marks_instance.ut3 <= internal_marks_instance.ut2 and internal_marks_instance.ut3 <= internal_marks_instance.reut:
                                smallest = internal_marks_instance.ut3
                            else:
                                smallest = internal_marks_instance.reut
                            temp_list[
                                'total_internal_marks'] = internal_marks_instance.ut1 + internal_marks_instance.ut2 + internal_marks_instance.ut3 + internal_marks_instance.reut - smallest
                            temp_list[
                                'total_assignment_marks'] = internal_marks_instance.ass1 + internal_marks_instance.ass2 + internal_marks_instance.ass3
                            temp_list[
                                'total_tutorial_or_quiz_marks'] = internal_marks_instance.tutorial_or_quiz_1 + internal_marks_instance.tutorial_or_quiz_2 + internal_marks_instance.tutorial_or_quiz_3
                            temp_list['average_internal_marks'] = format(temp_list['total_internal_marks'] / 5, '.2f')
                            avg_internal = (temp_list['total_internal_marks'] / 5)
                            temp_list['average_assignment_marks'] = format(temp_list['total_assignment_marks'] / 3,
                                                                           '.2f')
                            avg_assignment = (temp_list['total_assignment_marks'] / 3)
                            temp_list['average_tutorial_or_quiz_marks'] = format(
                                temp_list['total_tutorial_or_quiz_marks'] / 12,
                                '.2f')
                            avg_quiz = (temp_list['total_tutorial_or_quiz_marks'] / 12)
                            temp_list['final_assesment_mark1'] = round(
                                round(avg_internal + 0.01) + round(avg_assignment + 0.01) + round(
                                    avg_quiz + 0.01) + 0.01)  # student favourable
                            temp_list['final_assesment_mark2'] = round(
                                avg_internal + avg_assignment + avg_quiz + 0.01)  # actual result
                            if temp_list['final_assesment_mark1'] >= temp_list['final_assesment_mark2']:
                                temp_list['final_assesment_mark'] = temp_list['final_assesment_mark1']
                            else:
                                temp_list['final_assesment_mark'] = temp_list['final_assesment_mark2']

                        except:
                            pass
                    list_of_entries.append(temp_list)
                    # print(student_register_number)
            else:
                for stud in student_list:
                    temp_list = {}
                    temp_list['student_name'] = stud["student_obj"]
                    student_register_number = stud["student_obj"].roll_no
                    temp_list['registration_number'] = student_register_number
                    try:
                        internal_marks_instance = internalmarksold.objects.filter(student=stud["student_obj"]).get(
                            course=course_instance)
                        temp_list['internal_mark_instance'] = internal_marks_instance
                        if internal_marks_instance.ut1 <= internal_marks_instance.ut2 and internal_marks_instance.ut1 <= internal_marks_instance.ut3 and internal_marks_instance.ut1 <= internal_marks_instance.reut:
                            smallest = internal_marks_instance.ut1
                        elif internal_marks_instance.ut2 <= internal_marks_instance.ut1 and internal_marks_instance.ut2 <= internal_marks_instance.ut3 and internal_marks_instance.ut2 <= internal_marks_instance.reut:
                            smallest = internal_marks_instance.ut2
                        elif internal_marks_instance.ut3 <= internal_marks_instance.ut1 and internal_marks_instance.ut3 <= internal_marks_instance.ut2 and internal_marks_instance.ut3 <= internal_marks_instance.reut:
                            smallest = internal_marks_instance.ut3
                        else:
                            smallest = internal_marks_instance.reut
                        temp_list[
                            'total_internal_marks'] = internal_marks_instance.ut1 + internal_marks_instance.ut2 + internal_marks_instance.ut3 + internal_marks_instance.reut - smallest
                        temp_list[
                            'total_assignment_marks'] = internal_marks_instance.ass1 + internal_marks_instance.ass2 + internal_marks_instance.ass3
                        temp_list['avg_internal_marks'] = format(temp_list['total_internal_marks'] * 2 / 5, '.2f')
                        intmarks = (temp_list['total_internal_marks'] * 2 / 5)
                        temp_list['avg_internal_mark'] = round(intmarks + 0.01)
                        finalinternal = temp_list['total_assignment_marks'] + round(
                            intmarks + 0.01)  # 10 for temporary attendance
                        temp_list['final_assesment_mark'] = round(finalinternal / 4 + 0.01)
                    except:
                        pass
                    list_of_entries.append(temp_list)
            self.assertTemplateUsed(response, template_name='marks/internal_marks.html')
            self.assertEqual(response.context['course_list'], theory_list)
            self.assertEqual(response.context['selected_course'], selected_course)
            self.assertEqual(response.context['isonecredit'], selected_course.isonecredit)
            self.assertEqual(response.context['course_id'], course_instance)
            self.assertEqual(response.context['student_list'], list_of_entries)
            self.assertEqual(response.context['current_batch'], current_batch)
            self.assertEqual(response.context['academic_year'], academic_year)
            self.assertEqual(response.context['faculty'], faculty_inst)
            self.assertEqual(response.context['semester'], semester_number)

    def test_post_select_dept(self):
        # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)
        faculty_inst = staff.objects.get(user_id=cust_inst.id)
        courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        theory_list = []
        for entry in courses_handled:
            if entry['staff_course_instance'].course.subject_type == 'T':
                theory_list.append(entry)
        if len(theory_list) > 1:
            other_course = theory_list[1]['subject_code']
            response = self.client.post('/internal_marks/', {u'subject_code': [other_course]})
            self.assertEqual(response.status_code, 200)


class updateInternalsTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'student.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_access_without_login(self):
        url_string = '/update_marks'
        course = courses.objects.get(course_id='16SHS2Z1')
        rollno = student.objects.get(roll_no='1317148')
        url_string = url_string + '3Fcourse3D' + str(course.course_id) + "26roll_no3D" + str(rollno.roll_no)
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='marks/updatemarks.html')

    def test_access_student(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/update_marks'
        course = courses.objects.get(course_id='16SHS2Z1')
        rollno = student.objects.get(roll_no='1317148')
        url_string = url_string + '?course=' + str(course.course_id) + "&roll_no=" + str(rollno.roll_no)
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')

    def test_get_access_student(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/update_marks'
        course = courses.objects.get(course_id='16SHS2Z1')
        rollno = student.objects.get(roll_no='1317148')
        url_string = url_string + '?course=' + str(course.course_id) + "&roll_no=" + str(rollno.roll_no)
        response = self.client.get(url_string)
        msg = {
            'page_title': 'Permission denied',
            'title': 'Permission denied',
            'description': 'You dont have permissions to view this page!',
        }
        self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
        self.assertEqual(response.context['message'], msg)


        # def test_access_faculty(self):
        #     # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        #     cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        #     cust_inst.is_approved = True
        #     cust_inst.save()
        #
        #     logged = self.client.login(email=cust_inst.email, password='123456')
        #     self.assertTrue(logged)
        #
        #     faculty_inst = staff.objects.get(user_id=cust_inst.id)
        #     courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        #     theory_list = []
        #     for entry in courses_handled:
        #         if entry['staff_course_instance'].course.subject_type == 'T':
        #             theory_list.append(entry['staff_course_instance'])
        #     url_string = '/update_marks'
        #     course = courses.objects.get(course_id='16SHS2Z1')
        #     rollno = student.objects.get(roll_no='1317148')
        #     url_string = url_string + '?course=' + str(course.course_id) + "&roll_no=" + str(rollno.roll_no)
        #     response = self.client.get(url_string)
        #     if not theory_list:
        #         self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
        #     else:
        #         if course.regulation.start_year == "2016":
        #             self.assertTemplateUsed(response, template_name='marks/update_marks.html')
        #         else:
        #             self.assertTemplateUsed(response, template_name='marks/updatemarksold.html')
        #
        # def test_get_access_faculty(self):
        #     # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        #     cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        #     cust_inst.is_approved = True
        #     cust_inst.save()
        #
        #     logged = self.client.login(email=cust_inst.email, password='123456')
        #     self.assertTrue(logged)
        #
        #     faculty_inst = staff.objects.get(user_id=cust_inst.id)
        #     courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        #     theory_list = []
        #     for entry in courses_handled:
        #         if entry['staff_course_instance'].course.subject_type == 'T':
        #             theory_list.append(entry['staff_course_instance'])
        #     url_string = '/update_marks'
        #     course = courses.objects.get(course_id='16SHS2Z1')
        #     rollno = student.objects.get(roll_no='1317148')
        #     url_string = url_string + '?course=' + str(course.course_id) + "&roll_no=" + str(rollno.roll_no)
        #     response = self.client.get(url_string)
        #     if not theory_list:
        #         msg = {
        #             'page_title': 'Permission denied',
        #             'title': 'Permission denied',
        #             'description': 'You dont have permissions to view this page!',
        #         }
        #         self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
        #         self.assertEqual(response.context['message'], msg)
        #     else:
        #         if course.regulation.start_year=="2016":
        #             self.assertTemplateUsed(response, template_name='marks/update_marks.html')
        #         else:
        #             self.assertTemplateUsed(response, template_name='marks/updatemarksold.html')
