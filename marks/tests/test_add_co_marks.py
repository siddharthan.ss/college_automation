from django.test import Client
from django.test import TestCase

from accounts.models import staff, student, CustomUser
from curriculum.models import courses
from curriculum.views import get_current_course_allotment_with_display_text


class addCoMarksTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'student.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_access_without_login(self):
        url_string = '/add_co_marks/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='co_marks/add_co_marks.html')

    def test_access_student(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/add_co_marks/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='co_marks/add_co_marks.html')

    def test_access_faculty(self):
        # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        faculty_inst = staff.objects.get(user_id=cust_inst.id)
        courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        theory_list = []
        for entry in courses_handled:
            if entry['staff_course_instance'].course.subject_type == 'T':
                theory_list.append(entry)
        url_string = '/add_co_marks/'
        response = self.client.get(url_string)
        if not theory_list:
            self.assertTemplateNotUsed(response, template_name='co_marks/add_co_marks.html')
            self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
        else:
            self.assertTemplateUsed(response, template_name='co_marks/add_co_marks.html')
            self.assertTemplateNotUsed(response, template_name='prompt_pages/error_page_base.html')

    def test_get_method(self):
        # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)
        faculty_instance = staff.objects.get(user_id=cust_inst.id)
        courses_handled = get_current_course_allotment_with_display_text(faculty_instance)
        course_list = []
        for entry in courses_handled:
            if entry['staff_course_instance'].course.subject_type != 'P':
                course_list.append(entry)
        url_string = '/add_co_marks/'
        response = self.client.get(url_string)
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
        }
        if not course_list:
            self.assertTemplateUsed(response, template_name='prompt_pages/error_page_base.html')
            self.assertEqual(response.context['message'], msg)
        else:
            self.assertTemplateUsed(response, template_name='co_marks/add_co_marks.html')
            self.assertEqual(response.context['selected_course'], course_list[0])
            self.assertEqual(response.context['course_list'], course_list)
            self.assertEqual(response.context['unit_test_number'], ' ')

    def test_post_method(self):
        # cust_inst = CustomUser.objects.filter(is_staff_account=True).filter(is_superuser=False).order_by('?').first()
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)
        faculty_inst = staff.objects.get(user_id=cust_inst.id)
        courses_handled = get_current_course_allotment_with_display_text(faculty_inst)
        theory_list = []
        for entry in courses_handled:
            if entry['staff_course_instance'].course.subject_type != 'P':
                theory_list.append(entry)
        if len(theory_list) > 1:
            other_course = theory_list[1]['subject_code']
            response = self.client.post('/add_co_marks/', {u'subject_code': [other_course]})
            self.assertEqual(response.status_code, 200)
            response = self.client.post('/add_co_marks/', {u'editutno': [u'1']})
            list_of_id = []
            course_instance = courses.objects.get(course_id=other_course)
            semester_number = getattr(course_instance, 'semester')
            department_id = getattr(course_instance, 'department_id')
            ug_or_pg = getattr(course_instance, 'programme')

            student_list = student.objects.all().filter(department=department_id).filter(
                current_semester=semester_number).filter(qualification=ug_or_pg).order_by('roll_no')
            for stud in student_list:
                list_of_id.append(stud.id)

            required_edit = ''

            stud = student.objects.get(id=required_edit)

            student_batch = getattr(stud, 'batch')
            # print(student_batch)
            batch_split = str(student_batch).split('-')
            current_batch = int(batch_split[0])
            response = self.client.post('/add_co_marks/', {u'course': [course_instance]})
