from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from marks.views.lab_marks import create_lab_pattern, add_lab_marks, labPdfReport,labCsvReport
from marks.views import internal_marks, CreateUpdateView, internalsPdfReport
from marks.views.overall_internals_report import overall_internals_report, overallInternalsPdfReport
from marks.views.student_mark import student_internal_marks, approve_internal_marks
from marks.views.internalMarksOldRegulationLab import internalMarksOldRegulation, internalMarksOldRegulationLabPdfReport

urlpatterns = [
    # url(r'^get_courses', get_courses_handled_by_faculty, name='get_courses'),
    url(r'^internal_marks', internal_marks, name="internal_marks"),
    url(r'^update_marks', login_required(CreateUpdateView.as_view()), name='update_marks'),
    url(r'^pdf_internal_marks/(?P<course_id>[0-9]+)', login_required(internalsPdfReport.as_view()),
        name='pdf_internal_marks'),

    url(r'^pdf_overall_internal_marks/(?P<course_id>[0-9]+)', login_required(overallInternalsPdfReport.as_view()),
        name='pdf_overall_internal_marks'),

    url(r'^set_lab_pattern/$', create_lab_pattern, name="create_lab_pattern"),
    url(r'^add_lab_marks/$', add_lab_marks, name="add_lab_marks"),
    url(r'^pdf_lab_marks/(?P<course_id>[0-9]+)', login_required(labPdfReport.as_view()), name='pdf_lab_marks'),
    url(r'^csv_lab_marks/(?P<course_id>[0-9]+)', labCsvReport, name='csv_lab_marks'),
    url(r'^overall_internals/$', overall_internals_report, name='overall_internals_report'),

    url(r'^my_internal_marks/$', student_internal_marks, name="student_internal_marks"),
    url(r'^approve_internal_marks/$', approve_internal_marks, name="approve_internal_marks"),

    url(r'^add_lab_internals_2012_regulation/$', internalMarksOldRegulation, name="internalMarksOldRegulation"),
    url(r'^pdf_internal_marks_lab/(?P<staffCoursePk>[0-9]+)',
        login_required(internalMarksOldRegulationLabPdfReport.as_view()),
        name='internalMarksOldRegulationLabPdfReport'),

]
