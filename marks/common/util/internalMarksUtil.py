from curriculum.models import staff_course
from marks.models import internalmarks, internalmarksold, onecreditcourse, student_approved_internals_mark, LabMarks2012
from attendance.views.SubjectConsolidatedAttendanceAPI import SubjectConsolidatedAttendanceAPI
from django.utils import timezone
import math


def old_school_round(number):
    if number % 1 >= 0.5:
        return math.ceil(number)
    else:
        return round(number)


def get_internal_marks_queryset(student_instance, course_instance, semester_instance):
    if course_instance.regulation.start_year >= 2016:
        if course_instance.is_one_credit:
            internalmarks_queryset = onecreditcourse.objects.filter(
                student=student_instance,
                course=course_instance,
                semester=semester_instance
            )
        else:
            internalmarks_queryset = internalmarks.objects.filter(
                student=student_instance,
                course=course_instance,
                semester=semester_instance,
            )
    else:
        if course_instance.subject_type == "T":
            internalmarks_queryset = internalmarksold.objects.filter(
                student=student_instance,
                course=course_instance,
                semester=semester_instance,
            )
        else:
            internalmarks_queryset = LabMarks2012.objects.filter(
                student=student_instance,
                staffCourse__course=course_instance,
                staffCourse__semester=semester_instance,
            )
    if internalmarks_queryset.exists():
        # todo handle Many instance exception
        return internalmarks_queryset.get()
    else:
        return None


# use only for 2012 regulation student
def get_attendance_internal_mark_for_student(student_instance, course_instance, semester_instance):
    subject_consolidated_api_instance = SubjectConsolidatedAttendanceAPI()
    subject_consolidated_api_instance.start_date_instance = semester_instance.start_term_1
    if semester_instance.end_term_3:
        subject_consolidated_api_instance.end_date_instance = semester_instance.end_term_3
    else:
        subject_consolidated_api_instance.end_date_instance = timezone.now().date()

    subject_consolidated_api_instance.course_instance = course_instance
    subject_consolidated_api_instance.semester_instance = semester_instance

    try:
        queryset = subject_consolidated_api_instance.get_attendance_queryset()
        percentage = subject_consolidated_api_instance.get_percentage(queryset, student_instance)
    except:
        percentage = 0

    if percentage >= 95:
        return 10
    elif percentage >= 90:
        return 8
    elif percentage >= 85:
        return 6
    elif percentage >= 80:
        return 4
    elif percentage > 75:
        return 2
    else:
        return 0


def get_student_internal_approval_status(student_instance, course_instance, semester_instance):
    staff_course_instance = staff_course.objects.get(
        semester=semester_instance,
        course=course_instance,
    )

    return student_approved_internals_mark.objects.filter(
        staff_course=staff_course_instance,
        student=student_instance,
    ).exists()


def get_internal_marks_data_as_dictionary(student_instance, course_instance, semester_instance):
    temp = {}
    temp['student_name'] = str(student_instance)
    temp['registration_number'] = student_instance.roll_no
    temp['student_approved_status'] = get_student_internal_approval_status(student_instance, course_instance,
                                                                           semester_instance)
    internal_marks_instance = get_internal_marks_queryset(student_instance, course_instance, semester_instance)
    if internal_marks_instance is not None:
        temp['internal_mark_instance'] = internal_marks_instance
        if course_instance.regulation.start_year == 2012:
            if course_instance.subject_type == "T":
                ut1 = internal_marks_instance.ut1 if internal_marks_instance.ut1 else 0
                ut2 = internal_marks_instance.ut2 if internal_marks_instance.ut2 else 0
                ut3 = internal_marks_instance.ut3 if internal_marks_instance.ut3 else 0
                reut = internal_marks_instance.reut if internal_marks_instance.reut else 0
                ass1 = internal_marks_instance.ass1 if internal_marks_instance.ass1 else 0
                ass2 = internal_marks_instance.ass2 if internal_marks_instance.ass2 else 0
                ass3 = internal_marks_instance.ass3 if internal_marks_instance.ass3 else 0
                smallest = ut1
                if ut2 < smallest:
                    smallest = ut2
                if ut3 < smallest:
                    smallest = ut3
                if reut < smallest:
                    smallest = reut
                total_ut_marks = ut1 + ut2 + ut3 + reut - smallest  # 150
                total_ut_marks_for_60 = total_ut_marks * 2 / 5  # 60
                total_assignment_marks = ass1 + ass2 + ass3  # 30
                attendance_mark = get_attendance_internal_mark_for_student(student_instance, course_instance,
                                                                           semester_instance)  # 10
                total_internal_marks_for_100 = total_ut_marks_for_60 + total_assignment_marks + attendance_mark
                total_internal_marks_for_25 = total_internal_marks_for_100 / 4

                temp['attendance_mark'] = attendance_mark
                temp['total_assignment_marks'] = old_school_round(total_assignment_marks)
                temp['total_internal_marks'] = old_school_round(total_ut_marks)
                temp['avg_internal_mark'] = old_school_round(total_ut_marks_for_60)
                temp['final_assesment_mark'] = old_school_round(total_internal_marks_for_25)
            else:
                modalMarks = 0
                recordMarks = 0
                if internal_marks_instance.modalMarks:
                    modalMarks = internal_marks_instance.modalMarks
                if internal_marks_instance.recordMarks:
                    recordMarks = internal_marks_instance.recordMarks
                attendance_mark = get_attendance_internal_mark_for_student(student_instance, course_instance,
                                                                           semester_instance)
                temp['attendance_mark'] = attendance_mark
                temp['final_assesment_mark'] = old_school_round(
                    (old_school_round(modalMarks / 2) + recordMarks + attendance_mark) / 4)
        else:
            if course_instance.is_one_credit:
                ut1 = internal_marks_instance.ut1 if internal_marks_instance.ut1 else 0
                ut2 = internal_marks_instance.ut2 if internal_marks_instance.ut2 else 0
                reut = internal_marks_instance.reut if internal_marks_instance.reut else 0
                smallest = ut1
                if ut2 < smallest:
                    smallest = ut2
                if reut < smallest:
                    smallest = reut
                temp['total_internal_marks'] = ut1 + ut2 + reut - smallest
                temp['final_assesment_mark'] = (temp['total_internal_marks'] / 2)

            else:
                ut1 = internal_marks_instance.ut1 if internal_marks_instance.ut1 else 0
                ut2 = internal_marks_instance.ut2 if internal_marks_instance.ut2 else 0
                ut3 = internal_marks_instance.ut3 if internal_marks_instance.ut3 else 0
                reut = internal_marks_instance.reut if internal_marks_instance.reut else 0
                ass1 = internal_marks_instance.ass1 if internal_marks_instance.ass1 else 0
                ass2 = internal_marks_instance.ass2 if internal_marks_instance.ass2 else 0
                ass3 = internal_marks_instance.ass3 if internal_marks_instance.ass3 else 0
                tutorial_or_quiz_1 = internal_marks_instance.tutorial_or_quiz_1 if internal_marks_instance.tutorial_or_quiz_1 else 0
                tutorial_or_quiz_2 = internal_marks_instance.tutorial_or_quiz_2 if internal_marks_instance.tutorial_or_quiz_2 else 0
                tutorial_or_quiz_3 = internal_marks_instance.tutorial_or_quiz_3 if internal_marks_instance.tutorial_or_quiz_3 else 0

                smallest = ut1
                if ut2 < smallest:
                    smallest = ut2
                if ut3 < smallest:
                    smallest = ut3
                if reut < smallest:
                    smallest = reut

                temp['total_internal_marks'] = ut1 + ut2 + ut3 + reut - smallest
                temp['total_assignment_marks'] = ass1 + ass2 + ass3
                temp['total_tutorial_or_quiz_marks'] = tutorial_or_quiz_1 + tutorial_or_quiz_2 + tutorial_or_quiz_3
                temp['average_internal_marks'] = format(temp['total_internal_marks'] / 5, '.2f')
                avg_internal = temp['total_internal_marks'] / 5
                temp['average_assignment_marks'] = format(temp['total_assignment_marks'] / 3, '.2f')
                avg_assignment = temp['total_assignment_marks'] / 3
                temp['average_tutorial_or_quiz_marks'] = format(temp['total_tutorial_or_quiz_marks'] / 3, '.2f')

                avg_quiz = (temp['total_tutorial_or_quiz_marks'] / 3)
                temp['final_assesment_mark1'] = old_school_round(
                    old_school_round(avg_internal + 0.01) + old_school_round(avg_assignment + 0.01) + old_school_round(
                        avg_quiz + 0.01) + 0.01)  # student favourable
                temp['final_assesment_mark2'] = old_school_round(
                    avg_internal + avg_assignment + avg_quiz + 0.01)  # actual result
                # print(temp_list['final_assesment_mark2'])
                if temp['final_assesment_mark1'] >= temp['final_assesment_mark2']:
                    temp['final_assesment_mark'] = temp['final_assesment_mark1']
                else:
                    temp['final_assesment_mark'] = temp['final_assesment_mark2']

    return temp
