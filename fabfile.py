# Fabfile to:
#    - update the remote system(s)
#    - download and install an application

# Import Fabric's API module
from fabric.api import *

env.hosts = [
    'gctportal.com',
]

env.user = "root"


def update():
    run("apt-get update")


def dbbackup():
    with prefix('source /home/ubuntu/ca/envs/ca_production_site/bin/activate'):
        with cd('/home/ubuntu/ca/ca_production_site/college_automation'):
            run('python3 manage.py dbbackup')

def mediabackup():
    with prefix('source /home/ubuntu/ca/envs/ca_production_site/bin/activate'):
        with cd('/home/ubuntu/ca/ca_production_site/college_automation'):
            run('python3 manage.py mediabackup')


def test_site():
    with prefix('source /home/ubuntu/ca/envs/ca_testing_site/bin/activate'):
        with cd('/home/ubuntu/ca/ca_testing_site/college_automation'):
            run('git reset --hard')
            run('git pull')
            run('pip install -r requirements.txt')
            run('python3 manage.py collectstatic')
            run('sh rm_migrations.sh')
            run('python3 manage.py makemigrations')
            run('service apache2 restart')


def production():
    with prefix('source /home/ubuntu/ca/envs/ca_production_site/bin/activate'):
        with cd('/home/ubuntu/ca/ca_production_site/college_automation'):
            run('python3 manage.py dbbackup')
            run('git reset --hard')
            run('git pull')
            run('pip install -r requirements.txt')
            run('python3 manage.py collectstatic')
            run('sh rm_migrations.sh')
            run('python3 manage.py makemigrations')
            run('service apache2 restart')
