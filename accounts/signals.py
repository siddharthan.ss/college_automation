from datetime import datetime

from django.contrib.auth.models import Group
from django.db.models.signals import pre_save, m2m_changed
from django.dispatch import receiver

from accounts.models import hourly_onduty_request, student, active_batches
from curriculum.models import semester, attendance, semester_migration
from curriculum.views.common_includes import get_course_of_student_given_date_and_hour
from dashboard.views import send_notification

from common.utils.studentUtil import getSemesterPlanofStudent


@receiver(pre_save, sender=student)
def correct_current_semester_during_update(sender, instance, **kwargs):
    batch_instance = instance.batch
    active_batch_instance = active_batches.objects.filter(department=instance.department).get(batch=batch_instance)
    current_semester = active_batch_instance.current_semester_number_of_this_batch
    print('previous current semester')
    print(instance.current_semester)
    instance.current_semester = current_semester
    print('after updating')
    print(instance.current_semester)


#
# @receiver(post_create, sender=student)
# def set_section_of_student_to_A(sender, instance, **kwargs):
#     department_instance = instance.department
#     batch_instance = instance.batch
#     department_sections_instance = department_sections.objects.filter(
#         department=department_instance
#     ).filter(
#         batch=batch_instance
#     ).get(
#         section_name='A'
#     )
#
#     section_students.objects.get_or_create(
#         section = department_sections_instance,
#         student = instance
#     )
#     print('Successfully assigned student to section A')


@receiver(pre_save, sender=hourly_onduty_request)
def mark_od_for_past_hour_hourly_od(sender, instance, **kwargs):
    print('inside hourly od signal')
    print(instance.granted)
    if type(instance.date) is str:
        instance_date_obj = datetime.strptime(instance.date, "%Y-%m-%d")
    else:
        instance_date_obj = instance.date
    if instance.granted:
        list_of_courses = get_course_of_student_given_date_and_hour(
            student_inst=instance.student,
            date=instance_date_obj,
        )
        print('list of courses')
        print(list_of_courses)
        current_semester_plan_of_student = getSemesterPlanofStudent(instance.student)
        print('current semsester plan of student')
        print(vars(instance))
        hour_instances = instance.hour.all()
        print('hour instances')
        print(hour_instances)
        list_of_hours = []
        for entry in hour_instances:
            list_of_hours.append(entry.hour)
        for course in list_of_courses:
            attendance_instances = attendance.objects.filter(
                course=course,
                date=instance.date,
                semester=current_semester_plan_of_student,
                hour__in=list_of_hours,
            )
            print('Attendance instances ')
            print(vars(attendance_instances))

            # fix - to handle improper OD marking of hours that the student was not part of
            # parallel lab hours
            attendance_marked = False
            for entries in attendance_instances:
                absent_list = entries.absent_students.all()
                if instance.student in absent_list:
                    entries.absent_students.remove(instance.student)
                    attendance_marked = True
                present_list = entries.present_students.all()
                if instance.student in present_list:
                    entries.present_students.remove(instance.student)
                    attendance_marked = True
                onduty_list = entries.onduty_students.all()
                if instance.student not in onduty_list and attendance_marked:
                    entries.onduty_students.add(instance.student)
                    entries.save()


@receiver(m2m_changed, sender=hourly_onduty_request.hour.through)
def notify_od_granted(sender, instance, **kwargs):
    print('going')
    if instance.granted:
        print(vars(instance))
        print(str(instance.staff.designation).upper())
        datas = {}
        datas['subject'] = 'GCT Portal - Granted OD'
        datas['message'] = 'As per your request to ' + str(instance.staff.designation) + ' ' + str(
            instance.staff).upper() + ',' + 'you have been granted on-duty on ' + \
                           str(instance.date) + ' for hours '
        hour_instances = instance.hour.all()
        print(hour_instances)
        i = 0
        hours = ''
        for entry in hour_instances:
            datas['message'] += str(entry.hour)
            hours += str(entry.hour)
            i = i + 1
            if i != hour_instances.count():
                datas['message'] += ','
                hours += ','

        datas['email'] = instance.student.user.email
        print(instance.student.user.email)
        content = 'OD granted'
        url = 'my_granted_ods'
        print('message to be mailed')
        print(datas['message'])
        send_notification(recipient=instance.student.user, message=content, url=url)
        # sendEmail(datas)


@receiver(m2m_changed, sender=semester.faculty_advisor.through)
def manage_faculty_group(sender, **kwargs):
    instance = kwargs.pop('instance', None)
    pk_set = kwargs.pop('pk_set', None)
    action = kwargs.pop('action', None)

    print(instance.faculty_advisor.all())

    """
        for every staff to be remove from facutly advisor group,
        check whether the faculty is not in any of the active semester faculty
    """

    g = Group.objects.get(name='faculty_advisor')

    if action == "pre_remove":
        list_of_faculty_advisors = []
        for active_semester in semester_migration.objects.all():
            semester_instance = active_semester.semester
            if semester_instance != instance:
                for entry in semester_instance.faculty_advisor.all():
                    list_of_faculty_advisors.append(entry)

        print("list of fa")
        print(list_of_faculty_advisors)

        for faculty in instance.faculty_advisor.all():
            if faculty not in list_of_faculty_advisors:
                g.user_set.remove(faculty.user)
                g.save()

    if action == "post_add":
        list_of_faculty_advisors = []
        for faculty in instance.faculty_advisor.all():
            print("staff = " + str(faculty))
            for active_semester in semester_migration.objects.all():
                print("active semester = " + str(active_semester))
                semester_instance = active_semester.semester
                if semester_instance != instance:
                    for entry in semester_instance.faculty_advisor.all():
                        list_of_faculty_advisors.append(entry)
        print("list of fa")
        print(list_of_faculty_advisors)
        for faculty in instance.faculty_advisor.all():
            if faculty not in list_of_faculty_advisors:
                g.user_set.add(faculty.user)
                g.save()

    print(locals())
