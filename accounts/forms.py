import datetime
import re

from PIL import Image
from django import forms
from django.conf import settings
from django.contrib.admin import widgets
from django.core.mail import send_mail
from django.forms.widgets import RadioFieldRenderer, FileInput
from django.utils.encoding import force_text
from django.utils.html import format_html_join

from accounts.models import CustomUser, batch
from .models import student, staff


class RadioFieldWithoutULRenderer(RadioFieldRenderer):
    def render(self):
        return format_html_join(
            '\n',
            '{0}',
            [(force_text(w),) for w in self],
        )


class Signup_form(forms.ModelForm):
    email = forms.CharField(max_length=256)
    email_hidden = forms.CharField(widget=forms.PasswordInput, label='Email')
    password1 = forms.CharField(widget=forms.PasswordInput, label='Password')
    password2 = forms.CharField(widget=forms.PasswordInput, label='Re-enter Password')

    def clean_email(self):
        cd = self.cleaned_data
        email = cd.get('email')
        email_hidden = cd.get('email_hidden')
        if not re.match(r'^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$', email):
            raise forms.ValidationError("Please enter a valid email id")
        if CustomUser.objects.filter(email=email):
            raise forms.ValidationError("This email id has already been registered")
        if not email_hidden:
            raise forms.ValidationError("You must confirm your email")
        if email != email_hidden:
            raise forms.ValidationError("Your emails do not match!")
        return email

    def clean_password1(self):
        cd = self.cleaned_data
        password1 = cd.get('password1')
        if not re.match(r'(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*-+=_/;:~.]).{8,}', password1):
            raise forms.ValidationError(
                "Password should contain atleast 8 characters as well as a character,a digit and a special character")
        return password1

    def clean_password2(self):
        cd = self.cleaned_data
        password1 = cd.get('password1')
        password2 = cd.get('password2')
        if not password2:
            raise forms.ValidationError("You must confirm your password")
        if password1 != password2:
            raise forms.ValidationError("Your passwords do not match")
        return password2

    required_field = ['email_hidden',
                      'email',
                      'password1',
                      'password2',
                      ]

    class Meta:
        model = CustomUser
        fields = ['email_hidden',
                  'email',
                  'password1',
                  'password2',
                  ]
        exclude = [
            'is_staff_account',
            'is_approved',
            'is_verified'
        ]

    def __init__(self, *args, **kwargs):
        super(Signup_form, self).__init__(*args, **kwargs)
        for field in Signup_form.required_field:
            self.fields[field].required = True

        self.fields['email_hidden'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Your Email ID'
        })
        self.fields['email'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Retype your Email ID'
        })
        self.fields['password1'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Password'
        })
        self.fields['password2'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Retype Password'
        })

    def sendEmail(self, datas):

        link = settings.CURRENT_HOST_NAME + 'activate/' + datas['activation_key']
        subject = 'GCT Portal - Account Verification'
        message = 'Welcome to GCT Portal /n Click the following link to verify your account ' + link
        from_email = 'dummy@email.com'
        send_mail(subject, message, from_email, [datas['email']], fail_silently=False)


class RegisterStudent(forms.ModelForm):
    gender = forms.ChoiceField(
        widget=forms.RadioSelect(renderer=RadioFieldWithoutULRenderer),
        choices=staff.GENDER_CHOICES
    )
    student_type = forms.ChoiceField(
        widget=forms.RadioSelect(renderer=RadioFieldWithoutULRenderer),
        choices=student.DAYSCHOLAR_HOSTELLER
    )
    """
    entry_type = forms.ChoiceField(
        widget=forms.RadioSelect(renderer=RadioFieldWithoutULRenderer),
        choices=student.ENTRY_TYPE
    )
    """

    def clean_roll_no(self):
        cd = self.cleaned_data
        roll_no = cd.get('roll_no')
        if student.objects.filter(roll_no=roll_no):
            raise forms.ValidationError("This roll number has already been registered")
        if not re.match(r'^([0-9L]+)$', roll_no):
            raise forms.ValidationError("Enter a valid roll_number")
        return roll_no

    def clean_first_name(self):
        cd = self.cleaned_data
        first_name = cd.get('first_name')
        first_name = first_name.title()
        if not re.match(r'^([a-zA-Z ]+)$', first_name):
            raise forms.ValidationError("Enter a valid name")
        return first_name

    def clean_middle_name(self):
        cd = self.cleaned_data
        middle_name = cd.get('middle_name')
        middle_name = middle_name.title()
        if not re.match(r'^([a-zA-Z]*)$', middle_name):
            raise forms.ValidationError("Enter a valid name")
        return middle_name

    def clean_last_name(self):
        cd = self.cleaned_data
        last_name = cd.get('last_name')
        last_name = last_name.title()
        if not re.match(r'^([a-zA-Z. ]+)$', last_name):
            raise forms.ValidationError("Enter a valid name")
        return last_name

    def clean_father_name(self):
        cd = self.cleaned_data
        father_name = cd.get('father_name')
        father_name = father_name.title()
        if not re.match(r'^([a-zA-Z. ]*)$', father_name):
            raise forms.ValidationError("Enter a valid name")
        return father_name

    def clean_mother_name(self):
        cd = self.cleaned_data
        mother_name = cd.get('mother_name')
        mother_name = mother_name.title()
        if not re.match(r'^([a-zA-Z. ]*)$', mother_name):
            raise forms.ValidationError("Enter a valid name")
        return mother_name

    def clean_date_of_joining(self):
        cd = self.cleaned_data
        date_of_joining = cd.get('date_of_joining')
        if not datetime.datetime.strptime(str(date_of_joining), '%Y-%m-%d'):
            raise forms.ValidationError("Enter the date in the given format (YYYY-MM-DD)")
        # batch_pk = self.data.get('batch')
        # entry_type = self.data.get('entrytype')
        # print('batch pk')
        # print(batch_pk)
        # if batch.objects.filter(pk=batch_pk).exists():
        #     batch_instance = batch.objects.get(pk=batch_pk)
        # else:
        #     raise forms.ValidationError("Please select a proper batch")
        # date_of_joining_instance = datetime.datetime.strptime(str(date_of_joining), '%Y-%m-%d')
        # if entry_type == 'regular':
        #     if batch_instance.start_year != date_of_joining_instance.year:
        #         raise forms.ValidationError("Please enter a proper date of joining")
        # else:
        #     if batch_instance.start_year + 1 != date_of_joining_instance.year:
        #         raise forms.ValidationError("Please enter a proper date of joining")
        return date_of_joining

    def clean_dob(self):
        cd = self.cleaned_data
        dob = cd.get('dob')
        batch_pk = self.data.get('batch')
        if not datetime.datetime.strptime(str(dob), '%Y-%m-%d'):
            raise forms.ValidationError("Enter the date in the given format (YYYY-MM-DD)")
        dob_instance = datetime.datetime.strptime(str(dob), '%Y-%m-%d')
        if batch.objects.filter(pk=batch_pk).exists():
            batch_instance = batch.objects.get(pk=batch_pk)
        else:
            raise forms.ValidationError("Please select a proper batch")
        if (batch_instance.start_year - dob_instance.year) < 16 :
            raise forms.ValidationError("Please enter proper date of birth")
        return dob

    # def clean_avatar(self):
    #    cd=self.cleaned_data
    #    avatar=cd.get('avatar')
    #   return avatar

    def clean_phone_number(self):
        cd = self.cleaned_data
        phone_number = cd.get('phone_number')
        if not re.match(r'^((\d+){10})$', phone_number):
            raise forms.ValidationError("Enter a valid mobile number")
        return phone_number

    def clean(self):
        cd = self.cleaned_data
        roll_no = cd.get('roll_no')
        first_name = cd.get('first_name')
        middle_name = cd.get('middle_name')
        last_name = cd.get('last_name')
        father_name = cd.get('father_name')
        mother_name = cd.get('mother_name')
        date_of_joining = cd.get('date_of_joining')
        dob = cd.get('dob')
        #        avatar=cd.get('avatar')
        phone_number = cd.get('phone_number')
        return cd

    required_field = [
        'roll_no',
                      'first_name',
                      'last_name',
                      'gender',
                      # 'department',
                      'date_of_joining',
                      'student_type',
                      # 'entry_type',
                      'permanent_address',
                      'temporary_address',
                      'dob',
                      'phone_number',
                      # 'avatar'
                      ]

    class Meta:
        model = student
        fields = [
                'roll_no',
                  'first_name',
                  'middle_name',
                  'last_name',
                  'gender',
                  'student_type',
                  # 'entry_type',
                  'father_name',
                  'father_occupation',
                  'father_annual_income',
                  'mother_name',
                  'mother_occupation',
                  'mother_annual_income',
                  # 'department',
                  # 'qualification',
                  # 'batch',
                  'date_of_joining',
                  'permanent_address',
                  'temporary_address',
                  'caste',
                  'community',
                  'aadhaar_number',
                  'dob',
                  'phone_number',
                  # 'avatar',
                  ]
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(RegisterStudent, self).__init__(*args, **kwargs)
        for field in RegisterStudent.required_field:
            self.fields[field].required = True

        self.fields['roll_no'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Roll Number'
        })
        self.fields['first_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'First Name'
        })
        self.fields['middle_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Can be blank'
        })
        self.fields['last_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Surname or Initials'
        })
        self.fields['phone_number'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': '10 digit mobile number'
        })
        self.fields['dob'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'YYYY-MM-DD',
        })
        self.fields['date_of_joining'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'YYYY-MM-DD',
        })
        self.fields['temporary_address'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Hostel/Room/Home address'
        })
        self.fields['permanent_address'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Home address'
        })
        self.fields['father_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Father\'s Full name (initials at back)'
        })
        self.fields['mother_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Mother\'s Full name (initials at back)'
        })
        self.fields['aadhaar_number'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Apply soon if you dont have one'
        })
        self.fields['caste'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'caste'
        })
        self.fields['community'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['father_occupation'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Father or Guardian occupation'
        })
        self.fields['mother_occupation'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Mother or Guardian occupation'
        })

        self.fields['father_annual_income'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Annual income of Father or Guardian'
        })

        self.fields['mother_annual_income'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Annual income of Mother or Guardian'
        })
        #      self.fields['avatar'].widget.attrs.update({
        #         'class': 'form-control',
        #    })


class RegisterStaff(forms.ModelForm):
    gender = forms.ChoiceField(
        widget=forms.RadioSelect(renderer=RadioFieldWithoutULRenderer),
        choices=staff.GENDER_CHOICES
    )

    def clean_first_name(self):
        cd = self.cleaned_data
        first_name = cd.get('first_name')
        first_name = first_name.title()
        if not re.match(r'^([a-zA-Z]+)$', first_name):
            raise forms.ValidationError("Enter a valid name")
        return first_name

    def clean_middle_name(self):
        cd = self.cleaned_data
        middle_name = cd.get('middle_name')
        middle_name = middle_name.title()
        if not re.match(r'^([a-zA-Z]*)$', middle_name):
            raise forms.ValidationError("Enter a valid name")
        return middle_name

    def clean_last_name(self):
        cd = self.cleaned_data
        last_name = cd.get('last_name')
        last_name = last_name.title()
        if not re.match(r'^([a-zA-Z ]+)$', last_name):
            raise forms.ValidationError("Enter a valid name")
        return last_name

    def clean_staff_id(self):
        cd = self.cleaned_data
        staff_id = cd.get('staff_id')
        if staff.objects.filter(staff_id=staff_id):
            raise forms.ValidationError("This staff number has already been registered")
        if not re.match(r'^([0-9a-zA-Z]+)$', staff_id):
            raise forms.ValidationError("Enter a valid staff_id")
        return staff_id

    def clean_dob(self):
        cd = self.cleaned_data
        dob = cd.get('dob')
        if not datetime.datetime.strptime(str(dob), '%Y-%m-%d'):
            raise forms.ValidationError("Enter the date in the given format (YYYY-MM-DD)")
        return dob

    def clean_phone_number(self):
        cd = self.cleaned_data
        phone_number = cd.get('phone_number')
        if not re.match(r'^((\d+){10})$', phone_number):
            raise forms.ValidationError("Enter a valid mobile number")
        return phone_number

    def clean(self):
        cd = self.cleaned_data
        staff_id = cd.get('staff_id')
        first_name = cd.get('first_name')
        middle_name = cd.get('middle_name')
        last_name = cd.get('last_name')
        dob = cd.get('dob')
        phone_number = cd.get('phone_number')
        return cd

    required_field = ['staff_id',
                      'first_name',
                      'last_name',
                      'gender',
                      'degree',
                      'designation',
                      'permanent_address',
                      'temporary_address',
                      'dob',
                      'phone_number',
                      # 'avatar',
                      ]

    class Meta:
        model = staff
        fields = ['staff_id',
                  'first_name',
                  'middle_name',
                  'last_name',
                  'gender',
                  'department',
                  'designation',
                  'qualification',
                  'degree',
                  'specialization',
                  'permanent_address',
                  'temporary_address',
                  'dob',
                  'phone_number',
                  # 'avatar',
                  ]
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(RegisterStaff, self).__init__(*args, **kwargs)
        for field in RegisterStaff.required_field:
            self.fields[field].required = True
        self.fields['staff_id'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Staff id number'
        })
        self.fields['first_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'First Name'
        })
        self.fields['middle_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Can be blank'
        })
        self.fields['last_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Surname or Initials'
        })
        self.fields['department'].widget.attrs.update({
            'class': 'form-control'
        })
        self.fields['qualification'].widget.attrs.update({
            'class': 'form-control'
        })
        self.fields['phone_number'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Phone Number'
        })
        self.fields['dob'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['designation'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['dob'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'YYYY-MM-DD',
        })
        self.fields['temporary_address'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Room/Home address'
        })
        self.fields['permanent_address'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Home address'
        })
        self.fields['degree'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Degree Earned'
        })
        self.fields['specialization'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Specialization(Can be empty)'
        })


class regstud(forms.ModelForm):
    # email = forms.CharField(max_length=256)
    gender = forms.ChoiceField(
        widget=forms.RadioSelect(renderer=RadioFieldWithoutULRenderer),
        choices=staff.GENDER_CHOICES
    )
    student_type = forms.ChoiceField(
        widget=forms.RadioSelect(renderer=RadioFieldWithoutULRenderer),
        choices=student.DAYSCHOLAR_HOSTELLER
    )
    entry_type = forms.ChoiceField(
        widget=forms.RadioSelect(renderer=RadioFieldWithoutULRenderer),
        choices=student.ENTRY_TYPE
    )
    # avatar = forms.ImageField(label=('Profile Pic'), required=False, \
    #                           error_messages={'invalid': ("Image files only")}, \
    #                           widget=FileInput)

    def clean_first_name(self):
        cd = self.cleaned_data
        first_name = cd.get('first_name')
        first_name = first_name.title()
        if not re.match(r'^([a-zA-Z]+)$', first_name):
            raise forms.ValidationError("Enter a valid name")
        return first_name

    def clean_middle_name(self):
        cd = self.cleaned_data
        middle_name = cd.get('middle_name')
        middle_name = middle_name.title()
        if not re.match(r'^([a-zA-Z]*)$', middle_name):
            raise forms.ValidationError("Enter a valid name")
        return middle_name

    def clean_last_name(self):
        cd = self.cleaned_data
        last_name = cd.get('last_name')
        last_name = last_name.title()
        if not re.match(r'^([a-zA-Z ]+)$', last_name):
            raise forms.ValidationError("Enter a valid name")
        return last_name

    def clean_father_name(self):
        cd = self.cleaned_data
        father_name = cd.get('father_name')
        father_name = father_name.title()
        if not re.match(r'^([a-zA-Z. ]*)$', father_name):
            raise forms.ValidationError("Enter a valid name")
        return father_name

    def clean_mother_name(self):
        cd = self.cleaned_data
        mother_name = cd.get('mother_name')
        mother_name = mother_name.title()
        if not re.match(r'^([a-zA-Z. ]*)$', mother_name):
            raise forms.ValidationError("Enter a valid name")
        return mother_name

    def clean_date_of_joining(self):
        cd = self.cleaned_data
        date_of_joining = cd.get('date_of_joining')
        if not datetime.datetime.strptime(str(date_of_joining), '%Y-%m-%d'):
            raise forms.ValidationError("Enter the date in the given format (YYYY-MM-DD)")
        # batch_pk = self.data.get('batch')
        # entry_type = self.data.get('entrytype') or self.instance.entry_type
        # print('batch pk')
        # print(batch_pk)
        # if batch.objects.filter(pk=batch_pk).exists():
        #     batch_instance = batch.objects.get(pk=batch_pk)
        #     print(batch_instance)
        # else:
        #     raise forms.ValidationError("Please select a proper batch")
        # date_of_joining_instance = datetime.datetime.strptime(str(date_of_joining), '%Y-%m-%d')
        # if entry_type == 'regular':
        #     if batch_instance.start_year != date_of_joining_instance.year:
        #         raise forms.ValidationError("Please enter a proper date of joining")
        # else:
        #     if batch_instance.start_year + 1 != date_of_joining_instance.year:
        #         raise forms.ValidationError("Please enter a proper date of joining")
        return date_of_joining

    def clean_dob(self):
        cd = self.cleaned_data
        dob = cd.get('dob')
        batch_pk = self.data.get('batch')
        if not datetime.datetime.strptime(str(dob), '%Y-%m-%d'):
            raise forms.ValidationError("Enter the date in the given format (YYYY-MM-DD)")
        dob_instance = datetime.datetime.strptime(str(dob), '%Y-%m-%d')
        if batch.objects.filter(pk=batch_pk).exists():
            batch_instance = batch.objects.get(pk=batch_pk)
        else:
            raise forms.ValidationError("Please select a proper batch")
        if (batch_instance.start_year - dob_instance.year) < 16:
            raise forms.ValidationError("Please enter proper date of birth")
        return dob

    def clean_phone_number(self):
        cd = self.cleaned_data
        phone_number = cd.get('phone_number')
        if not re.match(r'^((\d+){10})$', phone_number):
            raise forms.ValidationError("Enter a valid mobile number")
        return phone_number

    def clean(self):
        cd = self.cleaned_data
        email = cd.get('email')
        first_name = cd.get('first_name')
        middle_name = cd.get('middle_name')
        last_name = cd.get('last_name')
        father_name = cd.get('father_name')
        mother_name = cd.get('mother_name')
        date_of_joining = cd.get('date_of_joining')
        dob = cd.get('dob')
        phone_number = cd.get('phone_number')
        return cd

    required_field = [
        'first_name',
        'last_name',
        'gender',
        'department',
        'batch',
        'qualification',
        'date_of_joining',
        'permanent_address',
        'temporary_address',
        'dob',
        'phone_number',
        'caste',
        'community',
        # 'avatar',
    ]

    class Meta:
        model = student
        fields = [
            'first_name',
            'middle_name',
            'last_name',
            'gender',
            'student_type',
            'entry_type',
            'father_name',
            'father_occupation',
            'father_annual_income',
            'mother_name',
            'mother_occupation',
            'mother_annual_income',
            'department',
            'qualification',
            'batch',
            'date_of_joining',
            'permanent_address',
            'temporary_address',
            'caste',
            'community',
            'aadhaar_number',
            'dob',
            'phone_number',
            # 'avatar',
        ]
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(regstud, self).__init__(*args, **kwargs)
        for field in regstud.required_field:
            self.fields[field].required = True

        self.fields['first_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'First Name'
        })
        self.fields['middle_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Can be blank'
        })
        self.fields['last_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Surname or Initials'
        })
        self.fields['department'].widget.attrs.update({
            'class': 'form-control'
        })
        self.fields['batch'].widget.attrs.update({
            'class': 'form-control'
        })
        self.fields['phone_number'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': '10 digit mobile number'
        })
        self.fields['qualification'].widget.attrs.update({
            'class': 'form-control'
        })
        self.fields['dob'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'YYYY-MM-DD',
        })
        self.fields['date_of_joining'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'YYYY-MM-DD',
        })
        self.fields['temporary_address'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Hostel/Room/Home address'
        })
        self.fields['permanent_address'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Home address'
        })
        self.fields['father_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Father\'s Full name (initials at back)'
        })
        self.fields['mother_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Mother\'s Full name (initials at back)'
        })
        self.fields['aadhaar_number'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Apply soon if you dont have one'
        })
        self.fields['caste'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'caste'
        })
        self.fields['community'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['father_occupation'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Father or Guardian occupation'
        })
        self.fields['mother_occupation'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Mother or Guardian occupation'
        })

        self.fields['father_annual_income'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Annual income of Father or Guardian'
        })

        self.fields['mother_annual_income'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Annual income of Mother or Guardian'
        })

class updateStudentByStaff(regstud):

    class Meta:
        model = student
        fields = [
            'roll_no',
            'first_name',
            'middle_name',
            'last_name',
            'gender',
            'student_type',
            'entry_type',
            'father_name',
            'father_occupation',
            'father_annual_income',
            'mother_name',
            'mother_occupation',
            'mother_annual_income',
            'department',
            'qualification',
            'batch',
            'date_of_joining',
            'permanent_address',
            'temporary_address',
            'caste',
            'community',
            'aadhaar_number',
            'dob',
            'phone_number',
            'avatar',
        ]
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(updateStudentByStaff, self).__init__(*args, **kwargs)
        self.fields['roll_no'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Roll number'
        })
        self.fields['roll_no'].required = True

    def clean_roll_no(self):
        cd = self.cleaned_data
        roll_no = cd.get('roll_no')
        if student.objects.filter(roll_no=roll_no).exclude(pk=self.instance.pk):
            raise forms.ValidationError("This roll number has already been registered")
        if not re.match(r'^([0-9L]+)$', roll_no):
            raise forms.ValidationError("Enter a valid roll_number")
        return roll_no

class regstaff(forms.ModelForm):
    gender = forms.ChoiceField(
        widget=forms.RadioSelect(renderer=RadioFieldWithoutULRenderer),
        choices=staff.GENDER_CHOICES
    )
    dob = forms.DateField(
        widget=widgets.AdminDateWidget()
    )
    # avatar = forms.ImageField(label=('Profile Pic'), required=False, \
    #                           error_messages={'invalid': ("Image files only")}, \
    #                           widget=FileInput)

    def clean_first_name(self):
        cd = self.cleaned_data
        first_name = cd.get('first_name')
        first_name = first_name.title()
        if not re.match(r'^([a-zA-Z]+)$', first_name):
            raise forms.ValidationError("Enter a valid name")
        return first_name

    def clean_middle_name(self):
        cd = self.cleaned_data
        middle_name = cd.get('middle_name')
        middle_name = middle_name.title()
        if not re.match(r'^([a-zA-Z]*)$', middle_name):
            raise forms.ValidationError("Enter a valid name")
        return middle_name

    def clean_last_name(self):
        cd = self.cleaned_data
        last_name = cd.get('last_name')
        last_name = last_name.title()
        if not re.match(r'^([a-zA-Z ]+)$', last_name):
            raise forms.ValidationError("Enter a valid name")
        return last_name

    def clean_dob(self):
        cd = self.cleaned_data
        dob = cd.get('dob')
        if not datetime.datetime.strptime(str(dob), '%Y-%m-%d'):
            raise forms.ValidationError("Enter the date in the given format (YYYY-MM-DD)")
        return dob

    def clean_phone_number(self):
        cd = self.cleaned_data
        phone_number = cd.get('phone_number')
        if not re.match(r'^((\d+){10})$', phone_number):
            raise forms.ValidationError("Enter a valid mobile number")
        return phone_number

    def clean(self):
        cd = self.cleaned_data
        email = cd.get('email')
        password1 = cd.get('password1')
        password2 = cd.get('password2')
        staff_id = cd.get('staff_id')
        first_name = cd.get('first_name')
        middle_name = cd.get('middle_name')
        last_name = cd.get('last_name')
        dob = cd.get('dob')
        phone_number = cd.get('phone_number')
        return cd

    required_field = [
        'first_name',
        'last_name',
        'gender',
        'degree',
        # 'designation',
        'permanent_address',
        'temporary_address',
        'dob',
        'phone_number',
        # 'avatar',
    ]

    class Meta:
        model = staff
        fields = [
            'first_name',
            'middle_name',
            'last_name',
            'gender',
            'department',
            'qualification',
            'degree',
            'specialization',
            'designation',
            'permanent_address',
            'temporary_address',
            'dob',
            'phone_number',
            # 'avatar',
        ]
        exclude = ['user']

    def __init__(self, *args, **kwargs):
        super(regstaff, self).__init__(*args, **kwargs)
        for field in regstaff.required_field:
            self.fields[field].required = True

        self.fields['first_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'First Name'
        })
        self.fields['middle_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Can be blank'
        })
        self.fields['last_name'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Surname or Initials'
        })
        self.fields['department'].widget.attrs.update({
            'class': 'form-control'
        })
        self.fields['qualification'].widget.attrs.update({
            'class': 'form-control'
        })
        self.fields['phone_number'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Phone Number'
        })
        self.fields['dob'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['designation'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['dob'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'YYYY-MM-DD',
        })
        self.fields['temporary_address'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Room/Home address'
        })
        self.fields['permanent_address'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Home address'
        })
        self.fields['degree'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Degree Earned'
        })
        self.fields['specialization'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Specialization(Can be empty)'
        })


class LoginForm(forms.Form):
    email = forms.CharField(max_length=256)
    password = forms.CharField(widget=forms.PasswordInput())

    def clean_email(self):
        cd = self.cleaned_data
        email = cd.get('email')
        if not re.match(r'^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$', email):
            raise forms.ValidationError("Please enter a valid email id")
        if not CustomUser.objects.filter(email=email):
            raise forms.ValidationError("This email id has not been registered yet")
        return email

        # def clean_password(self):
        #    cd=self.cleaned_data
        #  if len(password)<8:
        #      raise forms.ValidationError("Password should contain atleast 8 characters")
        # return password

    def clean(self):
        cd = self.cleaned_data
        email = cd.get('email')
        return cd

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

        self.fields['email'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Email ID'
        })

        self.fields['password'].widget.attrs.update({
            'class': 'form-control',
            'placeholder': 'Password'
        })


class passforgot(forms.Form):
    email = forms.CharField(max_length=256)


class passchange(forms.Form):
    email = forms.CharField(max_length=256)
    oldpass = forms.CharField(widget=forms.PasswordInput())
    newpass = forms.CharField(widget=forms.PasswordInput())
    newpass_again = forms.CharField(widget=forms.PasswordInput())

    def clean_email(self):
        cd = self.cleaned_data
        email = cd.get('email')
        if not re.match(r'^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$', email):
            raise forms.ValidationError("Please enter a valid email id")
        if CustomUser.objects.filter(email=email):
            raise forms.ValidationError("This email id has already been registered")
        return email

    def clean_newpass(self):
        cd = self.cleaned_data
        password1 = cd.get('newpass')
        if len(password1) < 8:
            raise forms.ValidationError("Password should contain atleast 8 characters")
        return password1

    def clean_newpass_again(self):
        cd = self.cleaned_data
        password1 = cd.get('newpass')
        password2 = cd.get('newpass_again')
        if not password2:
            raise forms.ValidationError("You must confirm your password")
        if password1 != password2:
            raise forms.ValidationError("Your passwords do not match")
        return password2

    def clean(self):
        cd = self.cleaned_data
        email = cd.get('email')
        password1 = cd.get('newpass')
        password2 = cd.get('newpass_again')
        return cd


class confirmpass(forms.Form):
    passcode = forms.CharField(max_length=256)


class updateform(forms.ModelForm):
    class Meta:
        model = student
        fields = ['first_name', 'middle_name', 'last_name', 'gender', 'father_name', 'mother_name', 'department',
                  'batch', 'current_semester', 'qualification', 'temporary_address', 'permanent_address', 'dob',
                  'date_of_joining', 'phone_number']



class PhotoForm(forms.ModelForm):
    x = forms.FloatField(widget=forms.HiddenInput())
    y = forms.FloatField(widget=forms.HiddenInput())
    width = forms.FloatField(widget=forms.HiddenInput())
    height = forms.FloatField(widget=forms.HiddenInput())

    avatar = forms.ImageField(label=(''), required=True, error_messages={'invalid': ("Image files only")}, widget=FileInput)

    class Meta:
        model = CustomUser
        fields = ('avatar', 'x', 'y', 'width', 'height', )
        widgets = {
            'file': forms.FileInput(attrs={
                'accept': 'image/*'  # this is not an actual validation! don't rely on that!
            })
        }

    def save(self):
        photo = super(PhotoForm, self).save()

        x = self.cleaned_data.get('x')
        y = self.cleaned_data.get('y')
        w = self.cleaned_data.get('width')
        h = self.cleaned_data.get('height')

        image = Image.open(photo.avatar)
        cropped_image = image.crop((x, y, w+x, h+y))
        resized_image = cropped_image.resize((200, 200), Image.ANTIALIAS)
        resized_image.save(photo.avatar.path)
        return photo
