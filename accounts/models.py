from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.core.validators import RegexValidator
from django.db import models
# from simple_history.models import HistoricalRecords
from simple_history.models import HistoricalRecords


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password=None, is_staff_account=None, activation_key=None, key_expires=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        user = self.model(
            email=self.normalize_email(email),
            is_staff_account=is_staff_account,
            activation_key=activation_key,
            key_expires=key_expires
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, activation_key=None, key_expires=None):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password,
            activation_key=activation_key,
            key_expires=key_expires,
            is_staff_account=True
        )
        user.is_approved = True
        user.is_active = True
        user.is_superuser = True
        user.has_filled_data = True
        user.save(using=self._db)
        return user


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    is_staff_account = models.BooleanField(default=False)
    has_filled_data = models.BooleanField(default=False)
    has_profile_picture = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=False)
    activation_key = models.CharField(max_length=40)
    key_expires = models.DateTimeField()
    objects = CustomUserManager()
    avatar = models.ImageField('profile pic (1:1 square)', upload_to='profile-images', null=True, blank=True)
    history = HistoricalRecords()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def is_staff(self):
        return staff.objects.filter(user=self).exists()

    def get_staff_instance(self):
        if self.is_staff():
            return staff.objects.get(user=self)

    def is_student(self):
        return student.objects.filter(user=self).exists()

    def get_student_instance(self):
        if self.is_student():
            return student.objects.get(user=self)

    def is_principal(self):
        return self.groups.filter(name="principal").exists()

    def is_programme_coordinator(self):
        return self.groups.filter(name="programme_coordinator").exists()

    def is_faculty_advisor(self):
        return self.groups.filter(name="faculty_advisor").exists()

    def is_hod(self):
        return self.groups.filter(name="hod").exists()

    def is_coe_staff(self):
        return self.groups.filter(name="coe_staff").exists()


#################################################################################################

QUALIFICATION_CHOICES = (('UG', 'Under Graduate'),
                         ('PG', 'Post Graduate'),
                         ('Phd', 'Doctrate of Philosophy'),
                         ('PG PT', 'PG - Parttime'),
                         ('Other', 'Other')
                         )

YEAR_OF_STUDY = (
    ('1', '1st Year'),
    ('2', '2nd Year'),
    ('3', '3rd Year'),
    ('4', '4th Year'),
)

GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female'),
)

class programme(models.Model):
    name = models.CharField(max_length=50)
    acronym = models.CharField(max_length=6)

    def __str__(self):
        return str(self.name)


class department(models.Model):
    name = models.CharField(max_length=60)
    acronym = models.CharField(max_length=5)
    is_core = models.BooleanField()
    history = HistoricalRecords()

    def natural_key(self):
        return (self.acronym)

    def __str__(self):
        return str(self.name)

class sub_department(models.Model):
    main_department = models.ForeignKey(department,related_name='main_department')
    child_departments = models.ManyToManyField(department,related_name='child_departments')

    def __str__(self):
        return str(self.main_department.acronym)

class department_programmes(models.Model):
    department = models.ForeignKey(department)
    programme = models.ForeignKey(programme)

    def __str__(self):
        return str(self.department.acronym) + ' - ' + str(self.programme.acronym)

    class Meta:
        unique_together = (
            ('department', 'programme')
        )

class regulation(models.Model):
    start_year = models.IntegerField()
    end_year = models.IntegerField()
    history = HistoricalRecords()

    def __str__(self):
        return str(self.start_year) + '-' + str(self.end_year)


class batch(models.Model):
    PROGRAMME_CHOICES = (
        ('UG', 'Under Graduate'),
        ('PG', 'Post Graduate'),
        ('Phd', 'Doctrate of Philosophy'),
    )
    start_year = models.SmallIntegerField()
    end_year = models.SmallIntegerField()
    programme = models.CharField(max_length=3, choices=PROGRAMME_CHOICES)
    regulation = models.ForeignKey(regulation)
    history = HistoricalRecords()

    def __str__(self):
        programme_period = 0
        if (self.programme == 'UG'):
            programme_period = 4
        elif (self.programme == 'PG'):
            programme_period = 2
        elif (self.programme == 'Phd'):
            programme_period = 2
        start_year = self.start_year
        end_year = start_year + programme_period
        return str(start_year) + '-' + str(end_year) + '-' + str(self.programme)

    def get_start_year(self):
        return self.start_year


class active_batches(models.Model):
    PROGRAMME_CHOICES = (
        ('UG', 'Under Graduate'),
        ('PG', 'Post Graduate'),
        ('Phd', 'Doctrate of Philosophy'),
    )
    batch = models.ForeignKey(batch)
    department = models.ForeignKey(department)
    programme = models.CharField(max_length=3, choices=PROGRAMME_CHOICES)
    current_semester_number_of_this_batch = models.SmallIntegerField()
    history = HistoricalRecords()

    def __str__(self):
        return 'Acitve batch of ' + str(self.department) + ' ' + str(self.programme) + ' ' + str(
            self.current_semester_number_of_this_batch) + ' sem'


class staff(models.Model):
    DESIGNATION_CHOICES = (
        ('Principal', 'Principal'),
        ('Professor', 'Professor'),
        ('Associate Professor', 'Associate Professor'),
        ('Assistant Professor', 'Assistant Professor'),
        ('Network Admin', 'Network Admin'),
        ('Hostel Admin', 'Hostel Admin'),
        ('Alumni Staff', 'Alumni Staff'),
    )
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    staff_id = models.CharField(unique=True, max_length=10)
    first_name = models.CharField(max_length=20)
    middle_name = models.CharField(max_length=20, blank=True)
    last_name = models.CharField(max_length=20)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    dob = models.DateField(verbose_name='Date of Birth', null=True)
    department = models.ForeignKey(department, verbose_name='Department')
    designation = models.CharField(max_length=20, choices=DESIGNATION_CHOICES, )
    qualification = models.CharField(max_length=20, choices=QUALIFICATION_CHOICES, )
    degree = models.CharField(max_length=254, blank=True)
    specialization = models.CharField(max_length=30)
    temporary_address = models.TextField(max_length=300, blank=True)
    permanent_address = models.TextField(max_length=300, blank=True)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{10,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(max_length=10)
    avatar = models.ImageField('profile pic (1:1 square)', upload_to='staff-uploaded-images', null=True, blank=True)
    history = HistoricalRecords()

    def __str__(self):
        name = str(self.first_name)
        if self.middle_name != None:
            name += ' '
            name += self.middle_name
        name += ' '
        name += self.last_name
        return name

    def get_department(self):
        return department.objects.get(pk=self.department_id)

    def get_department_id(self):
        return self.department_id


class student(models.Model):
    DAYSCHOLAR_HOSTELLER = (
        ('dayscholar', 'Day Scholar'),
        ('hosteller', 'Hosteller'),
    )

    ENTRY_TYPE = (
        ('regular', 'Regular'),
        ('lateral', 'Lateral'),
        ('transfer', 'Transfer'),
    )

    COMMUNITY = (
        ('OC', 'OC'),
        ('BC', 'BC'),
        ('BCM', 'BCM'),
        ('MBC', 'MBC'),
        ('SC', 'SC'),
        ('ST', 'ST'),
    )

    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    roll_no = models.CharField(unique=True, max_length=7)
    first_name = models.CharField(max_length=20)
    middle_name = models.CharField(max_length=20, blank=True)
    last_name = models.CharField(max_length=20)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, null=False)
    father_name = models.CharField(max_length=254, blank=True)
    father_occupation = models.CharField(max_length=50, default=None)
    father_annual_income = models.FloatField(default=None)
    mother_name = models.CharField(max_length=254, blank=True)
    mother_occupation = models.CharField(max_length=50, default=None)
    mother_annual_income = models.FloatField(default=None)
    department = models.ForeignKey(department, verbose_name='Department')
    batch = models.ForeignKey(batch)
    current_semester = models.SmallIntegerField()
    qualification = models.CharField(max_length=20, choices=QUALIFICATION_CHOICES)
    temporary_address = models.TextField(max_length=300, blank=True)
    permanent_address = models.TextField(max_length=300, blank=True)
    dob = models.DateField(verbose_name='Date of Birth', null=True)
    date_of_joining = models.DateField(null=True)
    student_type = models.CharField(max_length=10, choices=DAYSCHOLAR_HOSTELLER, null=False)
    entry_type = models.CharField(max_length=10, choices=ENTRY_TYPE, null=False)
    # phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
    # message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(max_length=10)
    caste = models.CharField(max_length=50, default=None)
    community = models.CharField(max_length=3, choices=COMMUNITY, default=None)
    aadhaar_number = models.CharField(max_length=12, default=None, null=True, blank=True)
    avatar = models.ImageField('profile pic (1:1 square)', upload_to='student-uploaded-images', null=True, blank=True)
    
    Pending_GPA=models.BooleanField(default=False)

    history = HistoricalRecords()

    def __str__(self):
        name = str(self.first_name)
        if self.middle_name != None:
            name += ' '
            name += self.middle_name
        name += ' '
        name += self.last_name
        return name

    def get_roll_number(self):
        return self.roll_no

    def get_current_year(self):
        if (self.current_semester % 2 == 0):
            return int(self.current_semester / 2)
        else:
            return int((self.current_semester + 1) / 2)

    class Meta:
        permissions = (
            ('can_approve_student', 'Can approve student'),
            ('can_approve_staffs', 'Can approve staffs'),
        )
        unique_together = (
            ('roll_no', 'aadhaar_number')
        )


class site_settings(models.Model):
    key = models.CharField(max_length=30)
    value = models.CharField(max_length=30)
    history = HistoricalRecords()

    def __str__(self):
        return self.key + '=' + self.value


class staff_papers(models.Model):
    staff = models.ForeignKey(staff)
    paper_name = models.CharField(max_length=200)
    description = models.CharField(max_length=1000, null=True, blank=True)
    year_of_publish = models.IntegerField()
    history = HistoricalRecords()

    def __str__(self):
        return str(self.staff) + '-' + str(self.paper_name) + '-' + str(self.year_of_publish)


class staff_course_attended(models.Model):
    staff = models.ForeignKey(staff)
    course_name = models.CharField(max_length=200)
    description = models.CharField(max_length=1000, null=True, blank=True)
    date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.staff) + '-' + str(self.course_name) + '-' + str(self.date)


class issue_onduty(models.Model):
    staff = models.ForeignKey(staff)
    student = models.ForeignKey(student)
    reason = models.CharField(max_length=1000, null=True, blank=True)
    date = models.DateField()
    history = HistoricalRecords()

    def __str__(self):
        return str(self.student) + ' od on ' + str(self.date) + ' for ' + str(self.reason) + ' from ' + str(self.staff)

    class Meta:
        unique_together = (
            ('student', 'date')
        )


class onduty_request(models.Model):
    student = models.ForeignKey(student)
    staff = models.ForeignKey(staff)
    message = models.CharField(max_length=1000, null=True, blank=True)
    start_date = models.DateField()
    end_date = models.DateField()
    granted = models.BooleanField(default=False)
    staff_created = models.BooleanField(default=False)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.student) + ' request od from ' + str(self.start_date) + ' to ' + str(
            self.end_date) + ' for ' + str(self.message) + ' to ' + str(self.staff)


class hours(models.Model):
    hour = models.PositiveSmallIntegerField(unique=True)
    hour_name = models.CharField(max_length=10)
    history = HistoricalRecords()


class hourly_onduty_request(models.Model):
    student = models.ForeignKey(student)
    staff = models.ForeignKey(staff)
    message = models.CharField(max_length=1000)
    date = models.DateField()
    hour = models.ManyToManyField(hours)
    granted = models.BooleanField(default=False)
    staff_created = models.BooleanField(default=False)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.student) + '-' + str(self.staff) + '-' + str(self.message)

    class Meta:
        unique_together = (
            ('student', 'date')
        )


class student_achievements(models.Model):
    student = models.ForeignKey(student)
    description = models.CharField(max_length=1000)
    semester_number = models.PositiveSmallIntegerField()
    history = HistoricalRecords()


class outgoing_mail_counter(models.Model):
    from_email = models.CharField(max_length=30)
    date = models.DateField()
    email_count = models.SmallIntegerField(default=0)

class onesignal(models.Model):
    user = models.ForeignKey(CustomUser)
    onesignal_id = models.TextField()

    def __str__(self):
        return str(self.user) + ' - ' + str(self.onesignal_id)
