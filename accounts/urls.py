from django.conf.urls import url
from django.contrib.auth import views
from django.views.generic import TemplateView

from accounts.views import index, imagegallery, userlogin, getdetails, userlogout, homepagelogout, signup, changepass, \
    approve, deapprove, cancel, update_status, userprofile, viewprofile, get_current_batches_signup, lookup_student, \
    lookup_staff, whole_students_lookup, alumni_students_lookup, add_papers, view_papers, edit_papers, \
    add_courses_attended, view_courses_attended, edit_courses_attended, UpdateProfile, UpdateProfilePreApproval, \
    details_submit_success, request_on_duty, view_request_od, my_granted_ods, view_issued_ods, indiviual_od, \
    view_od_requests, hourly_indiviual_od, bulk_indiviual_od, bulk_hourly_od, activation, new_activation_link, \
    no_javascript_page, add_onesignal_id
from accounts.views.users import update_profile, UpdateImage, UpdateImageOnAccountCreation, profile_updated
from accounts.views.student_profile_addups import add_achievements, view_achievements, edit_achievements

urlpatterns = [
    url(r'^$', index, name="index"),
    url(r'^index/$', index, name="index"),
    url(r'^imagegallery/$', imagegallery, name="imagegallery"),
    url(r'^login', userlogin, name="login"),
    url(r'^details_verification', getdetails, name="getdetails"),
    url(r'^logout/$', userlogout, name="logout"),
    url(r'^homelogout/$', homepagelogout, name="homelogout"),
    url(r'^signup/$', signup, name="signup"),
    url(r'^changepass/$', changepass, name="changepass"),
    url(r'^approval$', approve, name="approve"),
    url(r'^deapproval$', deapprove, name="deapprove"),
    # url(r'^delete_unverified_users$', delete_unverified_users, name="delete_unverified_users"),
    url(r'^cancel', cancel, name="cancel"),
    url(r'^update_status/$', update_status, name="update_status"),
    url(r'^update_profile/$', update_profile, name="updateprofile"),
    url(r'^(?P<roll>[0-9L]+)$', userprofile, name="userprofile"),
    url(r'^viewprofile/(?P<staff_pk>[0-9]+)?$', viewprofile, name="viewprofile"),

    url(r'^image_cropper/$', UpdateImage.as_view(), name="image_cropper"),

    url(r'^upload_image/$', UpdateImageOnAccountCreation.as_view(), name="upload_image_account_creation"),

    url(r'^profile_updated/$', profile_updated, name="profile_updated"),

    url(r'^get_current_batches/$', get_current_batches_signup, name="getbatches"),

    # Lookups

    url(r'^lookup_students/$', lookup_student, name="lookupstudent"),
    url(r'^lookup_staffs/$', lookup_staff, name="lookupstaff"),
    url(r'^students_database/$', whole_students_lookup, name="wholestudentslookup"),
    url(r'^alumni_students_database/$', alumni_students_lookup, name="alumnistudentslookup"),

    url(r'^add_papers/$', add_papers, name="addpapers"),
    url(r'^view_my_papers/$', view_papers, name="viewpapers"),
    url(r'^edit_my_papers/$', edit_papers, name="editpapers"),
    url(r'^add_courses/$', add_courses_attended, name="addcourses"),
    url(r'^view_my_courses/$', view_courses_attended, name="viewcourses"),
    url(r'^edit_my_courses/$', edit_courses_attended, name="editcourses"),

    url(r'^edit_profile$', UpdateProfile.as_view(), name='update_user'),
    url(r'^update_profile_pre_login/$', UpdateProfilePreApproval.as_view(), name='update_profile_pre_login$'),
    url(r'^edit_profile_pre_approval$', UpdateProfilePreApproval.as_view(), name='edit_profile_pre_approval'),
    url(r'^details_submit_success$', details_submit_success, name='details_submit_success'),

    url(r'^request_onduty/$', request_on_duty, name='request_od'),
    url(r'^view_request_onduty/$', view_request_od, name="viewmyodrequest"),
    # url(r'^delete_request_onduty/$',delete_request_od, name="deletemyodrequest"),
    url(r'^my_granted_ods/$', my_granted_ods, name="mygrantedod"),
    # url(r'^edit_my_ondutys/$',edit_issued_ods, name="editondutys"),
    url(r'^view_my_ondutys/$', view_issued_ods, name="viewondutys"),
    url(r'^add_onduty/$', indiviual_od, name="grandod"),
    url(r'^view_od_requests/$', view_od_requests, name="viewodrequests"),
    url(r'^add_hourly_onduty/$', hourly_indiviual_od, name="grandhourlyod"),

    url(r'^grant_bulk_od/$', bulk_indiviual_od, name="bulkod"),
    url(r'^grant_bulk_hourly_od/$', bulk_hourly_od, name="bulk_hourly_od"),

    url(r'^activate/(?P<key>.+)$', activation, name='activation'),
    url(r'^new-activation-link/(?P<user_id>\d+)$', new_activation_link, name='new_activation_link'),

    url(r'^user/password/reset/$',
        views.password_reset,
        {'post_reset_redirect': '/user/password/reset/done/',
         'template_name': 'registration/password_reset_form.html'},
        name="password_reset"),
    url(r'^user/password/reset/done/$',
        views.password_reset_done),
    url(r'^user/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        views.password_reset_confirm,
        {'post_reset_redirect': '/user/password/done/'}, name='urlreset'),
    url(r'^user/password/done/$',
        views.password_reset_complete),

    url(r'^add_achievements/$', add_achievements, name="add_achievements"),
    url(r'^view_achievements/$', view_achievements, name="view_achievements"),
    url(r'^edit_achievements/$', edit_achievements, name="edit_achievements"),
    url(r'^no_js/$', no_javascript_page, name="no_js"),

    url(r'^add_onesignal_id/$', add_onesignal_id, name="add_onesignal_id"),

    url(r'^OneSignalSDKUpdaterWorker(.*.js)(?:/(?P<params>[a-zA-Z]+)/)?',
        TemplateView.as_view(template_name='OneSignalSDKUpdaterWorker.js', content_type='application/x-javascript')),
    url(r'^OneSignalSDKWorker(.*.js)(?:/(?P<params>[a-zA-Z]+)/)?',
        TemplateView.as_view(template_name='OneSignalSDKWorker.js', content_type='application/x-javascript')),
    url(r'^manifest.json',
        TemplateView.as_view(template_name='manifest.json')),
]
