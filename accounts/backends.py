from django.contrib.auth.backends import ModelBackend

from .models import CustomUser

from django.core.mail.backends.smtp import EmailBackend

class CustomUserAuth(ModelBackend):
    def authenticate(self, username=None, password=None):
        try:
            user = CustomUser.objects.get(email=username)
            if user.check_password(password):
                return user
        except CustomUser.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            user = CustomUser.objects.get(pk=user_id)
            if user.is_active and user.is_approved:
                return user
            return None
        except CustomUser.DoesNotExist:
            return None

import configparser
import os
from django.conf import settings
config = configparser.ConfigParser()
config.read(os.path.join(settings.BASE_DIR, 'config.ini'))
from accounts.models import outgoing_mail_counter
from django.utils import timezone

import logging

logger = logging.getLogger(__name__)

class MyEmailCounterBackend(EmailBackend):

    fromAddress = None

    def __init__(self, host=None, port=None, username=None, password=None,
                 use_tls=None, fail_silently=False, use_ssl=None, timeout=None,
                 ssl_keyfile=None, ssl_certfile=None,
                 **kwargs):
        newHost = config['EMAIL']['EMAIL_HOST']
        newPort = config['EMAIL']['EMAIL_PORT']
        newPassword = config['EMAIL']['EMAIL_HOST_PASSWORD']
        newUseTls = config['EMAIL']['EMAIL_USE_TLS']


        self.fromAddress = config['EMAIL']['EMAIL_HOST_USER']

        if outgoing_mail_counter.objects.filter(from_email=str(self.fromAddress),date= timezone.now().date()).exists():
            omc_inst = outgoing_mail_counter.objects.get(
                from_email = self.fromAddress,
                date = timezone.now().date()
            )
        else:
            omc_inst = outgoing_mail_counter.objects.create(
                from_email=self.fromAddress,
                date=timezone.now().date()
            )

        if omc_inst.email_count == 250:
            self.fromAddress = config['EMAIL']['EMAIL_HOST_USER_1']

            if outgoing_mail_counter.objects.filter(from_email=str(self.fromAddress),
                                                    date=timezone.now().date()).exists():
                new_omc_inst = outgoing_mail_counter.objects.get(
                    from_email=self.fromAddress,
                    date=timezone.now().date(),
                )
            else:
                new_omc_inst = outgoing_mail_counter.objects.create(
                    from_email=self.fromAddress,
                    date=timezone.now().date()
                )
            if new_omc_inst.email_count == 250:
                self.fromAddress = config['EMAIL']['EMAIL_HOST_USER_2']
            else:
                new_omc_inst.email_count = new_omc_inst.email_count + 1
                new_omc_inst.save()
        else:
            omc_inst.email_count = omc_inst.email_count + 1
            omc_inst.save()





        print('sending mail new way')

        super(MyEmailCounterBackend, self).__init__(newHost, newPort, self.fromAddress, newPassword,
                    newUseTls, fail_silently, use_ssl, timeout,
                 ssl_keyfile, ssl_certfile,
                 **kwargs)

    def send_messages(self, email_messages):
        try:
            for msg in email_messages:
                msg.from_email = self.fromAddress
                logger.info(u"Sending message '%s' to recipients: %s", msg.subject, msg.to)
        except:
            logger.exception("Problem logging recipients, ignoring")

        return super(MyEmailCounterBackend, self).send_messages(email_messages)
