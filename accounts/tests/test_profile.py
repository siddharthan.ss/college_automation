from datetime import timedelta

from django.test import TestCase
from django.utils import timezone

from accounts.models import staff, student
from accounts.views.profile import studentProfile
from curriculum.models import attendance, courses, semester


class studentProfileTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json', 'perms.json',
        'group.json', 'customuser.json', 'batch.json', 'active_batches.json',
        'department.json', 'staff.json', 'student.json',
        'courses.json'
    ]

    def setUp(self):
        self.choosen_course = courses.objects.order_by('?').first()
        self.choose_staff = staff.objects.order_by('?').first()
        self.choosen_student = student.objects.order_by('?').first()
        self.semester_plan = semester(
            department=self.choosen_course.department,
            batch=self.choosen_student.batch,
            semester_number=self.choosen_student.current_semester,
        )
        self.semester_plan.save()
        self.attendance_inst_today = attendance(
            course=self.choosen_course,
            date=timezone.now().date(),
            hour=1,
            staff=self.choose_staff,
            grant_period=timezone.now().date(),
            semester=self.semester_plan,
        )
        self.attendance_inst_today.save()

        self.attendance_inst_yesterday = attendance(
            course=self.choosen_course,
            date=timezone.now().date() - timedelta(days=1),
            hour=5,
            staff=self.choose_staff,
            grant_period=timezone.now().date(),
            semester=self.semester_plan,
        )
        self.attendance_inst_yesterday.save()

        self.attendance_inst_tomorrow = attendance(
            course=self.choosen_course,
            date=timezone.now().date() + timedelta(days=1),
            hour=7,
            staff=self.choose_staff,
            grant_period=timezone.now().date(),
            semester=self.semester_plan,
        )
        self.attendance_inst_tomorrow.save()

        self.studentProfile_inst = studentProfile()

    def test_attendance_status_absent(self):
        self.attendance_inst_today.absent_students.add(self.choosen_student)

        result = self.studentProfile_inst.attendance_status_single(self.semester_plan, self.choosen_course,
                                                                   timezone.now().date(), 1, self.choosen_student)

        self.assertEqual(result, 'A')

    def test_attendance_status_present(self):
        self.attendance_inst_today.present_students.add(self.choosen_student)

        result = self.studentProfile_inst.attendance_status_single(self.semester_plan, self.choosen_course,
                                                                   timezone.now().date(), 1, self.choosen_student)

        self.assertEqual(result, 'P')

    def test_attendance_status_on_duty(self):
        self.attendance_inst_today.onduty_students.add(self.choosen_student)

        result = self.studentProfile_inst.attendance_status_single(self.semester_plan, self.choosen_course,
                                                                   timezone.now().date(), 1, self.choosen_student)

        self.assertEqual(result, 'O')

    def test_attendance_status_not_marked(self):
        result = self.studentProfile_inst.attendance_status_single(self.semester_plan, self.choosen_course,
                                                                   timezone.now().date(), 1, self.choosen_student)

        self.assertIsNone(result)

    def test_attendance_status_range(self):
        self.attendance_inst_yesterday.absent_students.add(self.choosen_student)
        self.attendance_inst_today.present_students.add(self.choosen_student)
        self.attendance_inst_tomorrow.onduty_students.add(self.choosen_student)

        start_date = timezone.now().date() - timedelta(days=1)
        end_date = timezone.now().date() + timedelta(days=1)

        range_queryset = self.studentProfile_inst.get_attendance_queryset_for_given_range(self.semester_plan,
                                                                                          self.choosen_course,
                                                                                          start_date, end_date)
        result_list = self.studentProfile_inst.get_list_of_attendance_entries_for_student_in_range(range_queryset,
                                                                                                   self.choosen_student)

        expected_list = [
            {'hour': 5, 'status': 'A', 'date': start_date},
            {'hour': 1, 'status': 'P', 'date': timezone.now().date()},
            {'hour': 7, 'status': 'O', 'date': end_date}
        ]

        self.assertEqual(result_list, expected_list)

    def test_present_percentage(self):
        self.attendance_inst_yesterday.absent_students.add(self.choosen_student)
        self.attendance_inst_today.present_students.add(self.choosen_student)
        self.attendance_inst_tomorrow.onduty_students.add(self.choosen_student)

        start_date = timezone.now().date() - timedelta(days=1)
        end_date = timezone.now().date() + timedelta(days=1)

        range_queryset = self.studentProfile_inst.get_attendance_queryset_for_given_range(self.semester_plan,self.choosen_course,start_date,end_date)
        result_list = self.studentProfile_inst.get_list_of_attendance_entries_for_student_in_range(range_queryset,self.choosen_student)

        expected_result = format((1 / 3) * 100, '.2f')
        result = self.studentProfile_inst.get_present_percentage()

        self.assertEqual(result, expected_result)

    def test_onduty_percentage(self):
        self.attendance_inst_yesterday.absent_students.add(self.choosen_student)
        self.attendance_inst_today.present_students.add(self.choosen_student)
        self.attendance_inst_tomorrow.onduty_students.add(self.choosen_student)

        start_date = timezone.now().date() - timedelta(days=1)
        end_date = timezone.now().date() + timedelta(days=1)

        range_queryset = self.studentProfile_inst.get_attendance_queryset_for_given_range(self.semester_plan,
                                                                                          self.choosen_course,
                                                                                          start_date, end_date)
        result_list = self.studentProfile_inst.get_list_of_attendance_entries_for_student_in_range(range_queryset,
                                                                                                   self.choosen_student)

        expected_result = format((1 / 3) * 100, '.2f')
        result = self.studentProfile_inst.get_onduty_percentage()

        self.assertEqual(result, expected_result)
