import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import tag
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys


@tag('selenium')
class general(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super(general, cls).setUpClass()
        cls.browser = WebDriver()
        cls.browser.maximize_window()

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super(general, cls).tearDownClass()

    def loginpageheaderandlinks(self):
        self.browser.find_element_by_id('login').click()
        self.assertIn('GCT | Login', self.browser.title)
        self.browser.find_element_by_id('logo').click()
        self.assertIn('GCT', self.browser.title)
        self.browser.find_element_by_id('login').click()
        self.assertIn('GCT | Login', self.browser.title)
        self.browser.find_element_by_id('logotitle').click()
        self.assertIn('GCT', self.browser.title)
        self.browser.find_element_by_id('login').click()
        self.assertIn('GCT | Login', self.browser.title)
        self.browser.find_element_by_class_name('to_register').click()
        self.assertIn('GCT | Signup', self.browser.title)
        self.browser.back()
        self.browser.find_element_by_class_name('reset_pass').click()
        self.assertIn('GCT | Reset Password', self.browser.title)
        self.browser.back()
        self.browser.find_element_by_id('logotitle').click()
        self.assertIn('GCT', self.browser.title)

    def signuppageheader(self):
        self.browser.find_element_by_id('signup').click()
        self.assertIn('GCT | Signup', self.browser.title)
        self.browser.find_element_by_class_name('active').is_selected()
        username_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_email']")
        username_input.send_keys('testgctportal@gmail.com')
        password1_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_password1']")
        password1_input.send_keys('testgctportal123')
        password2_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_password2']")
        password2_input.send_keys('testgctportal123')
        roll_no_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_roll_no']")
        roll_no_input.send_keys('1111111')
        first_name_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_first_name']")
        first_name_input.send_keys('Sample')
        middle_name_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_middle_name']")
        middle_name_input.send_keys('Alais')
        last_name_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_last_name']")
        last_name_input.send_keys('User')
        gender_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_gender_0']")
        gender_input.click()
        father_name_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_father_name']")
        father_name_input.send_keys('Sample Father')
        mother_name_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_mother_name']")
        mother_name_input.send_keys('Sample Mother')
        department_input = self.browser.find_element_by_xpath(
            "//select[@name='department']/option[text()='Computer Science and Engineering']").click()
        qualification_input = self.browser.find_element_by_xpath(
            "//select[@name='qualification']/option[text()='Under Graduate']").click()
        batch_input = self.browser.find_element_by_xpath("//select[@name='batch']/option[text()='2016-2020']").click()
        date_of_joining_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_date_of_joining']")
        date_of_joining_input.send_keys('2016-07-25')
        permanent_address_input = self.browser.find_element_by_xpath("//form[1]//textarea[@id='id_permanent_address']")
        permanent_address_input.send_keys('My permanent address')
        temporary_address_input = self.browser.find_element_by_xpath("//form[1]//textarea[@id='id_temporary_address']")
        temporary_address_input.send_keys('My temporary address')
        dob_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_dob']")
        dob_input.send_keys('1998-07-25')
        phone_number_input = self.browser.find_element_by_xpath("//form[1]//input[@id='id_phone_number']")
        phone_number_input.send_keys('9876543210')
        # time.sleep(2)
        self.browser.find_element_by_name("student_register").click()
        time.sleep(5)

    def checksuccesspage(self):
        self.assertIn('GCT | Signup success', self.browser.title)

    def logincheck(self):
        self.browser.find_element_by_id('login').click()
        username_input = self.browser.find_element_by_name("email")
        username_input.send_keys('testgctportal@gmail.com')
        password_input = self.browser.find_element_by_name("password")
        password_input.send_keys('testgctportal')
        self.browser.find_element_by_name("login").click()
        self.assertIn('GCT | Login error', self.browser.title)
        self.browser.back()
        self.browser.refresh()
        username_input = self.browser.find_element_by_name("email")
        username_input.send_keys('testgctportal@gmail.com')
        password_input = self.browser.find_element_by_name("password")
        password_input.send_keys('testgctportal123')
        self.browser.find_element_by_name("login").click()
        self.assertIn('GCT | Verification error', self.browser.title)
        self.browser.back()

    def gmail(self):
        email_input = self.browser.find_element_by_name("Email")
        email_input.send_keys('testgctportal@gmail.com')
        self.browser.find_element_by_id("next").click()
        time.sleep(2)
        password_input = self.browser.find_element_by_name("Passwd")
        password_input.send_keys('testgctportal123')
        self.browser.find_element_by_id("signIn").click()
        time.sleep(2)

    def viewemail(self):
        self.browser.find_element_by_xpath(
            "/html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[2]/tbody/tr[1]/td[3]/a/span").click()
        time.sleep(3)
        elem = self.browser.find_element_by_partial_link_text("http://127.0.0.1:8000")
        time.sleep(10)
        elem.send_keys(Keys.CONTROL + Keys.RETURN + '2')
        time.sleep(5)

        for handle in self.browser.window_handles:
            self.browser.switch_to.window(handle)
        self.assertIn('GCT | Account activated', self.browser.title)
        self.browser.find_element_by_link_text('Login').click()

    def loginafterverification(self):

        time.sleep(2)

        username_input = self.browser.find_element_by_name("email")
        username_input.send_keys('testgctportal@gmail.com')
        password_input = self.browser.find_element_by_name("password")
        password_input.send_keys('testgctportal123')
        self.browser.find_element_by_name("login").click()
        time.sleep(2)
        self.assertIn('GCT | Not approved', self.browser.title)
        self.browser.back()

    def forgotpassword(self):
        time.sleep(2)
        self.browser.find_element_by_class_name("reset_pass").click()
        self.assertIn('GCT | Reset Password', self.browser.title)
        email_input = self.browser.find_element_by_name("email")
        email_input.send_keys('testgctportal@gmail.com')
        self.browser.find_element_by_name("submit").click()
        self.assertIn('GCT | Password reset successful', self.browser.title)

    def resetmail(self):
        self.browser.find_element_by_xpath(
            "/html/body/table[2]/tbody/tr/td[2]/table[1]/tbody/tr/td[2]/form/table[2]/tbody/tr[1]/td[3]/a/span").click()
        time.sleep(3)
        elem = self.browser.find_element_by_partial_link_text("http://127.0.0.1:8000")
        time.sleep(2)
        elem.send_keys(Keys.CONTROL + Keys.RETURN + '3')
        time.sleep(5)
        for handle in self.browser.window_handles:
            self.browser.switch_to.window(handle)
        self.assertIn('GCT | Setting New password', self.browser.title)
        password1_input = self.browser.find_element_by_name("new_password1")
        password1_input.send_keys('mypassword12345')
        password2_input = self.browser.find_element_by_name("new_password2")
        password2_input.send_keys('mypassword12345')
        self.browser.find_element_by_name("submit").click()
        time.sleep(3)
        self.assertIn('GCT | Password reset complete', self.browser.title)
        time.sleep(2)
        self.browser.find_element_by_link_text("Click here to Log in").click()
        time.sleep(2)

    def loginfinal(self):
        time.sleep(2)
        username_input = self.browser.find_element_by_name("email")
        username_input.send_keys('testgctportal@gmail.com')
        password_input = self.browser.find_element_by_name("password")
        password_input.send_keys('testgctportal123')
        self.browser.find_element_by_name("login").click()
        time.sleep(2)
        self.assertIn('GCT | Login error', self.browser.title)
        self.browser.back()
        self.browser.refresh()
        username_input = self.browser.find_element_by_name("email")
        username_input.send_keys('testgctportal@gmail.com')
        password_input = self.browser.find_element_by_name("password")
        password_input.send_keys('mypassword12345')
        self.browser.find_element_by_name("login").click()
        time.sleep(2)
        self.assertIn('GCT | Not approved', self.browser.title)

    def test_general_tasks(self):
        self.browser.get('http://127.0.0.1:8000')
        self.assertIn('GCT', self.browser.title)
        self.loginpageheaderandlinks()
        self.signuppageheader()
        self.checksuccesspage()
        self.browser.get('http://127.0.0.1:8000')
        self.logincheck()
        self.browser.get(
            'https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/mail/#identifier')
        self.gmail()
        self.viewemail()
        self.loginafterverification()
        self.forgotpassword()
        self.browser.get(
            'https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/mail/#identifier')
        # self.gmail()
        self.resetmail()
        self.loginfinal()

        time.sleep(10)
