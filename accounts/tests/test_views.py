"""
 This file is to test all views in accounts app
 checking the following
 * proper heading is rendrerd
 * proper template is rendered
 * appropritate content is there for appropriate user


 **Testing Guidelines**

    GET or POST by anonymous user (should redirect to login page)
    GET or POST by logged-in user with no profile (should raise a UserProfile.DoesNotExist exception)
    GET by logged-in user (should show the form)
    POST by logged-in user with blank data (should show form errors)
    POST by logged-in user with invalid data (should show form errors)
    POST by logged-in user with valid data (should redirect)

 *
"""
import random
import unittest
from datetime import datetime, timedelta

from io import StringIO
from django.http import Http404
from django.utils import timezone

from django.core.exceptions import ValidationError
from django.http import HttpRequest
from django.template.loader import render_to_string
from django.test import TestCase, Client
from django.urls import resolve

import curriculum
from accounts.models import staff, CustomUser, student, department, batch, active_batches
from accounts.views import index, userlogin, signup
from django.contrib.auth.models import Group

from curriculum.models import semester,semester_migration, department_sections, section_students
from accounts.forms import *
from django.core.files.uploadedfile import SimpleUploadedFile
from college_automation.settings import *


class indexPageTest(TestCase):
    # check if page url properly resolves
    def setUp(self):
        self.client = Client()

    def test_root_url_resolves_to_index_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_index_page_returns_correct_html(self):
        response = self.client.get('/')
        self.assertIn(b'<title>GCT Portal</title>', response.content)

        expected_html = render_to_string('index.html')
        self.assertEqual(response.content.decode(), expected_html)

    def test_index_page_contains_correct_data(self):
        response = self.client.get('/')
        self.assertContains(response, 'GCT Educational Management System')
        # self.assertContains(response, 'Welcome to GCT one-stop Portal')
        # self.assertContains(response, 'Manage all your academic tasks at one place')


class loginPageTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json', 'perms.json',
        'group.json', 'customuser.json', 'batch.json','active_batches.json',
        'department.json', 'staff.json', 'student.json'
    ]

    def setUp(self):
        self.client = Client()

    # check if page url properly resolves
    def test_root_url_resolves_to_login_page_view(self):
        found = resolve('/login/')
        self.assertEqual(found.func, userlogin)

    def test_login_page_returns_correct_html(self):
        response = self.client.get('/login/')
        self.assertTrue(response.content.startswith(b'<!DOCTYPE html>'))
        self.assertIn(b'<title>GCT | Login</title>', response.content)
        self.assertTrue(response.content.endswith(b'</html>'))

        self.assertTemplateUsed(response, template_name='accounts/login.html')
    @unittest.skip("to be fixed")
    def test_login_with_post_data(self):
        self.client.login(email='f1@gmail.com', password='123456')
        response = self.client.post('/dashboard')
        print(vars(response))
        self.assertTrue(response.context['user'].is_authenticated)
        self.assertEqual(response.status_code, 200)

    def test_call_login_fails_blank(self):
        response = self.client.post('/login/', {})  # blank data dictionary
        self.assertFormError(response, 'loginForm', 'email', 'This field is required.')
        self.assertFormError(response, 'loginForm', 'password', 'This field is required.')

    def test_call_login_fails_invalid(self):
        response = self.client.post('/login/', {u'email': [u'f@gmail.com'], u'password': [u'123456']})
        self.assertRaises(ValidationError)
        self.assertContains(response, 'Email ID is not registered!')

    def test_invalid_password(self):
        response = self.client.post('/login/', {u'email': [u'f1@gmail.com'], u'password': [u'12345']})

        msg = {
            'page_title': 'GCT | Login error',
            'title': 'Invalid account',
            'description': 'Email and password did not match!',
        }
        self.assertTemplateUsed(response, 'prompt_pages/invalid_account.html')
        self.assertEqual(response.context['message'], msg)

    def test_hod_session_user_type(self):
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_approved = True
        cust_inst.is_verified = True
        cust_inst.has_profile_picture = True
        cust_inst.save()
        response = self.client.post('/login/', {u'email': [u'hod@gmail.com'], u'password': [u'123456']})
        session = self.client.session
        staff_instance = staff.objects.get(user=cust_inst)
        self.assertEqual(session["user_type"], staff_instance.get_designation_display())

    def test_principal_session_user_type(self):
        cust_inst = CustomUser.objects.get(email='principal@gmail.com')
        cust_inst.is_approved = True
        cust_inst.is_verified = True
        cust_inst.has_profile_picture = True
        cust_inst.save()
        response = self.client.post('/login/', {u'email': [u'principal@gmail.com'], u'password': [u'123456']})
        session = self.client.session
        staff_instance = staff.objects.get(user=cust_inst)
        self.assertEqual(session["user_type"], staff_instance.get_designation_display())


    def test_student_session(self):
        cust_inst = CustomUser.objects.get(email='s4@gmail.com')
        cust_inst.is_approved = True
        cust_inst.is_verified = True
        cust_inst.has_profile_picture = True
        cust_inst.save()
        # assign student to a particular section
        student_inst = student.objects.get(user=cust_inst)
        department_section_inst = department_sections.objects.create(
            department = student_inst.department,
            batch = student_inst.batch,
            section_name = 'A',
        )
        section_students.objects.create(
            student = student_inst,
            section = department_section_inst,
        )
        response = self.client.post('/login/', {u'email': [u's4@gmail.com'], u'password': [u'123456']})
        session = self.client.session
        student_instance = student.objects.get(user=cust_inst)
        self.assertEqual(session["user_type"], 'student')
        self.assertEqual(session["user_name"], str(student_instance))
        # self.assertEqual(session["profile_pic"], student_instance.avatar)
        self.assertEqual(session["roll_no"], student_instance.roll_no)
        self.assertEqual(session["gender"], student_instance.gender)

    def test_assert_redirects_without_profile(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        response = self.client.post('/login/', {u'email': [u'f1@gmail.com'], u'password': [u'123456']})
        # print(vars(response))
        # same again, but with valid data, then
        self.assertRedirects(response, '/upload_image/')

    def test_call_login_valid(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.has_profile_picture = True
        cust_inst.save()
        response = self.client.post('/login/', {u'email': [u'f1@gmail.com'], u'password': [u'123456']})
        # print(vars(response))
        # same again, but with valid data, then
        self.assertRedirects(response, '/dashboard/')

        session = self.client.session

        self.assertEqual(session["last_visit"], 'welcome')
        self.assertIsNone(session["disp_batch"])
        self.assertFalse(session['logged_first'])

        staff_instance = staff.objects.get(user=cust_inst)

        self.assertEqual(session["profile_pic"], staff_instance.avatar)     
        self.assertEqual(session["gender"], staff_instance.gender)

    def test_login_not_approved(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = False
        cust_inst.has_profile_picture = True
        cust_inst.save()
        response = self.client.post('/login/', {u'email': [u'f1@gmail.com'], u'password': [u'123456']})

        msg = {
            'page_title': 'GCT | Not approved',
            'title': 'Account not approved'
        }
        self.assertTemplateUsed(response, 'prompt_pages/not_approved.html')
        self.assertEqual(response.context['message'], msg)

    def test_login_not_verified(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_verified = False
        cust_inst.has_profile_picture = True
        cust_inst.save()
        response = self.client.post('/login/', {u'email': [u'f1@gmail.com'], u'password': [u'123456']})

        msg = {
            'page_title': 'GCT | Verification error',
            'title': 'Account not verified',
            'description': 'Your email is not yet verified',
        }
        self.assertTemplateUsed(response, 'prompt_pages/not_verified.html')
        self.assertEqual(response.context['message'], msg)

        session = self.client.session

        self.assertEqual(session["delete_user"], cust_inst.email)

    def test_last_login_today(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_verified = True
        cust_inst.is_approved = True
        cust_inst.has_profile_picture = True
        cust_inst.last_login = timezone.now()

        last_login = timezone.now()
        cust_inst.save()
        response = self.client.post('/login/', {u'email': [u'f1@gmail.com'], u'password': [u'123456']})

        session = self.client.session

        last_login_time = str(last_login.strftime(" at %l:%M:%S %p"))

        self.assertEqual(session['last_visit'], 'Today' + last_login_time)

    def test_last_login_yesterday(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_verified = True
        cust_inst.is_approved = True
        cust_inst.has_profile_picture = True
        cust_inst.last_login = timezone.now() - timedelta(days=1)

        last_login = timezone.now() - timedelta(days=1)
        cust_inst.save()
        response = self.client.post('/login/', {u'email': [u'f1@gmail.com'], u'password': [u'123456']})

        session = self.client.session

        last_login_time = str(last_login.strftime(" at %l:%M:%S %p"))

        self.assertEqual(session['last_visit'], 'Yesterday' + last_login_time)


class signupPageTest(TestCase):
    # check if page url properly resolves
    def setUp(self):
        self.client = Client(enforce_csrf_checks=True)

    def test_root_url_resolves_to_signup_page_view(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signup)

    def test_signup_page_returns_correct_html(self):
        response = self.client.get('/signup/')
        self.assertTrue(response.content.startswith(b'<!DOCTYPE html>'))
        self.assertIn(b'<title>GCT | Signup</title>', response.content)
        self.assertTrue(response.content.endswith(b'</html>'))

        self.assertTemplateUsed(response, template_name='signup.html')


"""
class EmailTest(TestCase):

    def test_whether_email_works_perfectly(self):d
        datas = {}
        datas['activation_key'] = '123456'
        datas['email'] = 'siddharthan.ss@gmail.com'

        RegisterStudent().sendEmail(datas)

"""


class UserProfileTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_access_without_login(self):
        response = self.client.get('/1317148')
        self.assertTemplateNotUsed(response, template_name='profilepage.html')
    @unittest.skip('todo to inculude reports')
    def test_access_student_account_as_staff(self):
        logged = self.client.login(email='hod@gmail.com', password='123456')

        student_instance = student.objects.get(roll_no='1317148')
        custom_user_instance = student_instance.user
        custom_user_instance.is_approved = True
        custom_user_instance.save()
        self.assertTrue(logged)

        response = self.client.get('/1317148')
        # print(response)

        self.assertTemplateUsed(response, template_name='profilepage.html')

    @unittest.skip('todo to include reports')
    def test_correct_student_data(self):
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)
        student_instance = student.objects.get(roll_no='1317148')
        custom_user_instance = student_instance.user
        custom_user_instance.is_approved = True
        custom_user_instance.save()

        response = self.client.get('/1317148')

        self.assertContains(response, student_instance.roll_no, 2)
        # self.assertContains(response,str(student_instance),1)
        self.assertContains(response, student_instance.phone_number, 1)
        self.assertContains(response, student_instance.user.email, 2)
        if student_instance.gender == 'M':
            self.assertContains(response, 'Male', 1)
        elif student_instance.gender == 'F':
            self.assertContains(response, 'Female', 1)
        self.assertContains(response, student_instance.dob.strftime('%b. %-d, %Y'), 1)
        self.assertContains(response, student_instance.department, 1)
        self.assertContains(response, student_instance.batch, 1)
        self.assertContains(response, student_instance.qualification)
        self.assertContains(response, student_instance.permanent_address)
        self.assertContains(response, student_instance.temporary_address)
        # self.assertContains(response,'student alias  A')

    def test_invalid_roll_no(self):
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.get('/1317189')
        self.assertTemplateUsed(response, '404.html')

    def test_unauthorized_access_by_student(self):
        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.get('/1317138')
        self.assertTemplateUsed(response, '401.html')


class ViewProfileTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_access_without_login(self):
        response = self.client.get('/viewprofile/')
        self.assertTemplateNotUsed(response, template_name='viewprofile.html')

    def test_template_rendered(self):
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.get('/viewprofile/')

        # print(response)

        self.assertTemplateUsed(response, template_name='viewprofile.html')

    @unittest.skip('skipped due to redirected page asking for student sections')
    def test_student_profile(self):
        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)
        student_instance_1 = student.objects.get(roll_no='1317148')
        custom_user_inst = CustomUser.objects.get(email='s1@gmail.com')
        student_instance = student.objects.get(user=custom_user_inst)

        response = self.client.get('/viewprofile/')
        self.assertRedirects(response,'/' + student_instance.roll_no)

        # self.assertNotContains(response, student_instance_1.roll_no)
        # # self.assertContains(response, str(student_instance.roll_no))
        # # self.assertContains(response,str(student_instance),1)
        # self.assertContains(response, student_instance.phone_number, 1)
        # self.assertContains(response, student_instance.user.email, 2)
        # if student_instance.gender == 'M':
        #     self.assertContains(response, 'Male', 1)
        # elif student_instance.gender == 'F':
        #     self.assertContains(response, 'Female', 1)
        # self.assertContains(response, student_instance.dob.strftime('%b. %-d, %Y'), 1)
        # self.assertContains(response, student_instance.department, 1)
        # self.assertEqual(response.context['obj'], student_instance)
        # self.assertContains(response, student_instance.qualification)
        # self.assertContains(response, student_instance.permanent_address)
        # self.assertContains(response, student_instance.temporary_address)
        # self.assertEqual(response.context['custuser'], custom_user_inst)

    def test_staff_view_profile(self):
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)
        custom_user_inst = CustomUser.objects.get(email='hod@gmail.com')
        staff_instance = staff.objects.get(user=custom_user_inst)

        response = self.client.get('/viewprofile/')

        self.assertEqual(response.context['obj'], staff_instance)
        self.assertEqual(response.context['custuser'], custom_user_inst)


class LogoutTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_logout_url(self):
        response = self.client.get('/logout/')
        self.assertRedirects(response, '/')

    def test_homelogout_url(self):
        response = self.client.get('/homelogout/')
        self.assertRedirects(response, '/')


class ApprovalTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_get_access_by_anonymous(self):
        response = self.client.get('/approval')
        self.assertTemplateNotUsed(response, 'accounts/approval.html')

    def test_approval_access_by_student(self):
        self.client.login(email='s1@gmail.com', password='123456')
        response = self.client.get('/approval')
        self.assertTemplateNotUsed(response, 'accounts/approval.html')

    def test_approval_access_by_faculty(self):
        self.client.login(email='f1@gmail.com', password='123456')
        response = self.client.get('/approval')
        self.assertTemplateNotUsed(response, 'accounts/approval.html')

    def test_approval_access_by_faculty_advisor(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/approval')
        self.assertTemplateUsed(response, 'accounts/approval.html')

    def test_approval_access_by_hod(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/approval')
        self.assertTemplateUsed(response, 'accounts/approval.html')

    def test_approval_access_by_pricipal(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='principal')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/approval')
        self.assertTemplateUsed(response, 'accounts/approval.html')

    def test_valid_data_for_principal(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='principal')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        hod_customuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        hod_customuser_inst.is_approved = False
        hod_customuser_inst.save()


        response = self.client.get('/approval')

        user_list = []
        value = staff.objects.get(user__email=hod_customuser_inst)
        user_list.append(value)
        self.assertEqual(response.context['list_of_hods'], user_list)
        self.assertEqual(response.context['action'], 'Approve')

    def test_valid_users_for_hod(self):
        custuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)

        all_staffs = staff.objects.filter(department=staff.objects.get(user=custuser_inst).department)

        for each in all_staffs:
            inst = CustomUser.objects.get(pk=each.user_id)
            inst.is_approved = True
            inst.save()

        staff_customuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        staff_customuser_inst.is_approved = False
        staff_customuser_inst.save()

        response = self.client.get('/approval')

        user_list = []
        # print('user=' + str(user))
        value = staff.objects.get(user__email=staff_customuser_inst)
        user_list.append(value)

        self.assertEqual(response.context['list_of_staffs'], user_list)
        self.assertEqual(response.context['action'], 'Approve')

    def test_invalid_users_for_hod(self):
        custuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)

        staff_customuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        staff_inst = staff.objects.get(user=staff_customuser_inst)
        staff_inst.department = department.objects.get(acronym='IT')
        staff_inst.save()
        staff_customuser_inst.is_approved = False
        staff_customuser_inst.save()

        response = self.client.get('/approval')

        user_list = []
        # print('user=' + str(user))
        value = staff.objects.get(user__email=staff_customuser_inst)
        user_list.append(value)

        self.assertNotEqual(response.context['list_of_staffs'], user_list)
        self.assertEqual(response.context['action'], 'Approve')

    def test_valid_users_for_fa(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        custuser_inst.is_approved = True
        custuser_inst.save()
        g = Group.objects.get(name='faculty_advisor')
        student_customuser_inst = CustomUser.objects.get(email='s1@gmail.com')


        dept_sec_A = department_sections(
            department=staff.objects.get(user=custuser_inst).department,
            batch=student.objects.get(user__email=student_customuser_inst).batch,
            section_name='A'
        )
        dept_sec_A.save()

        sem_obj = semester.objects.create(
            department=staff.objects.get(user=custuser_inst).department,
            batch=student.objects.get(user__email=student_customuser_inst).batch,
            semester_number=student.objects.get(user__email=student_customuser_inst).current_semester,
            department_section = dept_sec_A
        )
        sem_obj.faculty_advisor.add(staff.objects.get(user=custuser_inst))
        g.user_set.add(custuser_inst)
        g.save()

        student_instance = student.objects.get(user__email=student_customuser_inst)

        sem_migration_inst = semester_migration(
            semester=sem_obj,
            department=staff.objects.get(user=custuser_inst).department,
            semester_number= active_batches.objects.filter(department=student_instance.department).get(batch=student_instance.batch).current_semester_number_of_this_batch,
        )
        sem_migration_inst.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        all_studs = student.objects.filter(department=staff.objects.get(user=custuser_inst).department)

        for each in all_studs:
            inst = CustomUser.objects.get(pk=each.user_id)
            inst.is_approved = True
            inst.save()


        student_customuser_inst.is_approved = False
        student_customuser_inst.save()

        response = self.client.get('/approval')
        # print('user=' + str(user))
        value = student.objects.get(user__email=student_customuser_inst)
        user_list = []
        user_list.append(value)

        self.assertEqual(response.context['list_of_students'], user_list)
        self.assertEqual(response.context['action'], 'Approve')

    def test_approving_users(self):
        custuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.post('/approval', {u'approved': [u'Approve'],u'approve': [u'f1@gmail.com'],})
        self.assertEqual(response.status_code, 200)

        self.assertTrue(CustomUser.objects.get(email='f1@gmail.com').is_approved)

        response = self.client.post('/approval', {u'delete': [u'Approve'], u'approve': [u'f1@gmail.com'], })
        self.assertEqual(response.status_code, 200)
        var = True
        try:
            CustomUser.objects.get(email='f1@gmail.com')
            var = False
        except:
            pass

        self.assertTrue(var)


class DeapprovalTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_get_access_by_anonymous(self):
        response = self.client.get('/deapproval')
        self.assertTemplateNotUsed(response, 'accounts/approval.html')

    def test_approval_access_by_student(self):
        self.client.login(email='s1@gmail.com', password='123456')
        response = self.client.get('/deapproval')
        self.assertTemplateNotUsed(response, 'accounts/approval.html')

    def test_approval_access_by_faculty(self):
        self.client.login(email='f1@gmail.com', password='123456')
        response = self.client.get('/deapproval')
        self.assertTemplateNotUsed(response, 'accounts/approval.html')

    def test_approval_access_by_faculty_advisor(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/deapproval')
        self.assertTemplateUsed(response, 'accounts/approval.html')

    def test_approval_access_by_hod(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/deapproval')
        self.assertTemplateUsed(response, 'accounts/approval.html')

    def test_approval_access_by_pricipal(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='principal')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/deapproval')
        self.assertTemplateUsed(response, 'accounts/approval.html')

    def test_valid_data_for_principal(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        custuser_inst.is_approved = True
        custuser_inst.save()
        g = Group.objects.get(name='principal')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        all_hod = CustomUser.objects.filter(staff__designation='Professor')

        for each in all_hod:
            each.is_approved = False
            each.save()

        hod_customuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        hod_customuser_inst.is_approved = True
        hod_customuser_inst.save()


        response = self.client.get('/deapproval')

        user_list = []
        # print('user=' + str(user))
        value = staff.objects.get(user__email=hod_customuser_inst)
        user_list.append(value)


        self.assertEqual(response.context['list_of_hods'], user_list)
        self.assertEqual(response.context['action'], 'Deapprove')

    def test_valid_users_for_hod(self):
        custuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)

        all_staffs = staff.objects.filter(department=staff.objects.get(user=custuser_inst).department)

        for each in all_staffs:
            inst = CustomUser.objects.get(pk=each.user_id)
            inst.is_approved = False
            inst.save()

        staff_customuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        staff_customuser_inst.is_approved = True
        staff_customuser_inst.save()

        response = self.client.get('/deapproval')

        user_list = []
        # print('user=' + str(user))
        value = staff.objects.get(user__email=staff_customuser_inst)
        user_list.append(value)

        self.assertEqual(response.context['list_of_staffs'], user_list)
        self.assertEqual(response.context['action'], 'Deapprove')

    def test_invalid_users_for_hod(self):
        custuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)

        staff_customuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        staff_inst = staff.objects.get(user=staff_customuser_inst)
        staff_inst.department = department.objects.get(acronym='IT')
        staff_inst.save()
        staff_customuser_inst.is_approved = True
        staff_customuser_inst.save()

        response = self.client.get('/deapproval')

        user_list = []
        # print('user=' + str(user))
        value = staff.objects.get(user__email=staff_customuser_inst.email)
        user_list.append(value)

        self.assertNotEqual(response.context['list_of_staffs'], user_list)
        self.assertEqual(response.context['action'], 'Deapprove')

    def test_valid_users_for_fa(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        custuser_inst.is_approved = True
        custuser_inst.save()
        g = Group.objects.get(name='faculty_advisor')
        student_customuser_inst = CustomUser.objects.get(email='s1@gmail.com')

        dept_sec_A = department_sections(
            department=staff.objects.get(user=custuser_inst).department,
            batch=student.objects.get(user__email=student_customuser_inst).batch,
            section_name='A'
        )
        dept_sec_A.save()

        sem_obj = semester.objects.create(
            department=staff.objects.get(user=custuser_inst).department,
            batch=student.objects.get(user__email=student_customuser_inst).batch,
            semester_number=student.objects.get(user__email=student_customuser_inst).current_semester,
            department_section = dept_sec_A
        )
        sem_obj.faculty_advisor.add(staff.objects.get(user=custuser_inst))
        g.user_set.add(custuser_inst)
        g.save()
        semester_migration_obj = semester_migration(
            semester=sem_obj,
            department=sem_obj.department,
            semester_number=sem_obj.semester_number,
        )
        semester_migration_obj.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        all_studs = student.objects.filter(department=staff.objects.get(user=custuser_inst).department)

        for each in all_studs:
            inst = CustomUser.objects.get(pk=each.user_id)
            inst.is_approved = False
            inst.save()


        student_customuser_inst.is_approved = True
        student_customuser_inst.save()

        response = self.client.get('/deapproval')
        # print('user=' + str(user))
        value = student.objects.get(user__email=student_customuser_inst)
        user_list = []
        user_list.append(value)

        self.assertEqual(response.context['list_of_students'], user_list)
        self.assertEqual(response.context['action'], 'Deapprove')

    def test_deapproving_unverified_users(self):
        custuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.post('/deapproval', {u'deapprove': [u'Deapprove'],u'approve': [u'f1@gmail.com'],})
        self.assertEqual(response.status_code, 200)

        self.assertFalse(CustomUser.objects.get(email='f1@gmail.com').is_approved)

"""
class DeleteUnverifiedTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_get_access_by_anonymous(self):
        response = self.client.get('/delete_unverified_users')
        self.assertTemplateNotUsed(response, 'accounts/approval.html')

    def test_approval_access_by_student(self):
        self.client.login(email='s1@gmail.com', password='123456')
        response = self.client.get('/delete_unverified_users')
        self.assertTemplateNotUsed(response, 'accounts/approval.html')

    def test_approval_access_by_faculty(self):
        self.client.login(email='f1@gmail.com', password='123456')
        response = self.client.get('/delete_unverified_users')
        self.assertTemplateNotUsed(response, 'accounts/approval.html')

    def test_approval_access_by_faculty_advisor(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/delete_unverified_users')
        self.assertTemplateUsed(response, 'accounts/approval.html')

    def test_approval_access_by_hod(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/delete_unverified_users')
        self.assertTemplateUsed(response, 'accounts/approval.html')

    def test_approval_access_by_pricipal(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='principal')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/delete_unverified_users')
        self.assertTemplateUsed(response, 'accounts/approval.html')

    def test_valid_data_for_principal(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        g = Group.objects.get(name='principal')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)





        hod_customuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        hod_customuser_inst.is_verified = False
        hod_customuser_inst.save()


        response = self.client.get('/delete_unverified_users')

        user_list = []

        temp_list = {}
        temp_list['whom'] = 'staff'
        temp_list['email'] = hod_customuser_inst
        # print('user=' + str(user))
        value = staff.objects.get(user__email=hod_customuser_inst)
        temp_list['id'] = getattr(value, 'staff_id')
        temp_list['first_name'] = getattr(value, 'first_name')
        temp_list['middle_name'] = getattr(value, 'middle_name')
        temp_list['last_name'] = getattr(value, 'last_name')
        temp_list['gender'] = getattr(value, 'gender')
        temp_list['avatar'] = getattr(value, 'avatar')
        temp_list['dob'] = getattr(value, 'dob')
        temp_list['department'] = getattr(value, 'department')
        temp_list['designation'] = getattr(value, 'designation')
        temp_list['qualification'] = getattr(value, 'qualification')
        temp_list['degree'] = getattr(value, 'degree')
        temp_list['specialization'] = getattr(value, 'specialization')
        temp_list['temporary_address'] = getattr(value, 'temporary_address')
        temp_list['permanent_address'] = getattr(value, 'permanent_address')
        temp_list['phone_number'] = getattr(value, 'phone_number')
        user_list.append(temp_list)

        self.assertEqual(response.context['user_list'], user_list)
        self.assertEqual(response.context['action'], 'Delete')

    def test_valid_users_for_hod(self):
        custuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)

        all_staffs = staff.objects.filter(department=staff.objects.get(user=custuser_inst).department)

        for each in all_staffs:
            inst = CustomUser.objects.get(pk=each.user_id)
            inst.is_verified = True
            inst.save()

        staff_customuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        staff_customuser_inst.is_verified = False
        staff_customuser_inst.save()

        response = self.client.get('/delete_unverified_users')

        user_list = []

        temp_list = {}
        temp_list['whom'] = 'staff'
        temp_list['email'] = staff_customuser_inst
        # print('user=' + str(user))
        value = staff.objects.get(user__email=staff_customuser_inst)
        temp_list['id'] = getattr(value, 'staff_id')
        temp_list['first_name'] = getattr(value, 'first_name')
        temp_list['middle_name'] = getattr(value, 'middle_name')
        temp_list['last_name'] = getattr(value, 'last_name')
        temp_list['gender'] = getattr(value, 'gender')
        temp_list['avatar'] = getattr(value, 'avatar')
        temp_list['dob'] = getattr(value, 'dob')
        temp_list['department'] = getattr(value, 'department')
        temp_list['designation'] = getattr(value, 'designation')
        temp_list['qualification'] = getattr(value, 'qualification')
        temp_list['degree'] = getattr(value, 'degree')
        temp_list['specialization'] = getattr(value, 'specialization')
        temp_list['temporary_address'] = getattr(value, 'temporary_address')
        temp_list['permanent_address'] = getattr(value, 'permanent_address')
        temp_list['phone_number'] = getattr(value, 'phone_number')
        user_list.append(temp_list)

        self.assertEqual(response.context['user_list'], user_list)
        self.assertEqual(response.context['action'], 'Delete')

    def test_invalid_users_for_hod(self):
        custuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)

        staff_customuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        staff_inst = staff.objects.get(user=staff_customuser_inst)
        staff_inst.department = department.objects.get(acronym='IT')
        staff_inst.save()
        staff_customuser_inst.is_approved = False
        staff_customuser_inst.save()

        response = self.client.get('/delete_unverified_users')

        user_list = []

        temp_list = {}
        temp_list['whom'] = 'staff'
        temp_list['email'] = staff_customuser_inst
        # print('user=' + str(user))
        value = staff.objects.get(user__email=staff_customuser_inst)
        temp_list['id'] = getattr(value, 'staff_id')
        temp_list['first_name'] = getattr(value, 'first_name')
        temp_list['middle_name'] = getattr(value, 'middle_name')
        temp_list['last_name'] = getattr(value, 'last_name')
        temp_list['gender'] = getattr(value, 'gender')
        temp_list['avatar'] = getattr(value, 'avatar')
        temp_list['dob'] = getattr(value, 'dob')
        temp_list['department'] = getattr(value, 'department')
        temp_list['designation'] = getattr(value, 'designation')
        temp_list['qualification'] = getattr(value, 'qualification')
        temp_list['degree'] = getattr(value, 'degree')
        temp_list['specialization'] = getattr(value, 'specialization')
        temp_list['temporary_address'] = getattr(value, 'temporary_address')
        temp_list['permanent_address'] = getattr(value, 'permanent_address')
        temp_list['phone_number'] = getattr(value, 'phone_number')
        user_list.append(temp_list)

        self.assertNotEqual(response.context['user_list'], user_list)
        self.assertEqual(response.context['action'], 'Delete')

    def test_valid_users_for_fa(self):
        custuser_inst = CustomUser.objects.get(email='f1@gmail.com')
        custuser_inst.is_approved = True
        custuser_inst.save()
        g = Group.objects.get(name='faculty_advisor')
        student_customuser_inst = CustomUser.objects.get(email='s1@gmail.com')
        sem_obj = semester.objects.create(
            department=staff.objects.get(user=custuser_inst).department,
            batch=student.objects.get(user__email=student_customuser_inst).batch,
            semester_number=student.objects.get(user__email=student_customuser_inst).current_semester,
        )
        sem_obj.faculty_advisor.add(staff.objects.get(user=custuser_inst))
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        all_studs = student.objects.filter(department=staff.objects.get(user=custuser_inst).department)

        for each in all_studs:
            inst = CustomUser.objects.get(pk=each.user_id)
            inst.is_verified = True
            inst.save()


        student_customuser_inst.is_verified = False
        student_customuser_inst.save()

        response = self.client.get('/delete_unverified_users')

        user_list = []

        temp_list = {}
        temp_list['whom'] = 'student'
        temp_list['email'] = student_customuser_inst.email
        # print('user=' + str(user))
        value = student.objects.get(user__email=student_customuser_inst)
        temp_list['id'] = getattr(value, 'roll_no')
        temp_list['first_name'] = getattr(value, 'first_name')
        temp_list['middle_name'] = getattr(value, 'middle_name')
        temp_list['last_name'] = getattr(value, 'last_name')
        temp_list['gender'] = getattr(value, 'gender')
        temp_list['avatar'] = getattr(value, 'avatar')
        temp_list['father_name'] = getattr(value, 'father_name')
        temp_list['mother_name'] = getattr(value, 'mother_name')
        temp_list['department'] = getattr(value, 'department')
        temp_list['batch'] = getattr(value, 'batch')
        temp_list['current_semester'] = getattr(value, 'current_semester')
        temp_list['qualification'] = getattr(value, 'qualification')
        temp_list['temporary_address'] = getattr(value, 'temporary_address')
        temp_list['permanent_address'] = getattr(value, 'permanent_address')
        temp_list['date_of_joining'] = getattr(value, 'date_of_joining')
        temp_list['dob'] = getattr(value, 'dob')
        temp_list['phone_number'] = getattr(value, 'phone_number')
        user_list.append(temp_list)

        self.assertEqual(response.context['user_list'], user_list)
        self.assertEqual(response.context['action'], 'Delete')

    def test_deleting_unverified_users(self):
        custuser_inst = CustomUser.objects.get(email='hod@gmail.com')
        g = Group.objects.get(name='hod')
        g.user_set.add(custuser_inst)
        g.save()
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.post('/delete_unverified_users', {u'delete': [u'Delete'],u'approve': [u'f1@gmail.com'],})
        self.assertEqual(response.status_code, 200)
        var = True
        try:
            CustomUser.objects.get(email='f1@gmail.com')
            var = False
        except:
            pass

        self.assertTrue(var)
"""

class CancelAccountTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_account_deleted(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_verified = False
        cust_inst.save()
        self.client.post('/login/', {u'email': [u'f1@gmail.com'], u'password': [u'123456']})

        response = self.client.get('/cancel/')
        #print(vars(response))

        var = True
        try:
            cst = CustomUser.objects.get(email='f1@gmail.com')
            print(vars(cst))
            var = False
        except:
            pass

        self.assertTrue(var)

class UpdateStatusTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]


    def setUp(self):
        self.client = Client()

    def test_anonymous_user_access(self):
        response = self.client.get('/update_status')
        self.assertTemplateNotUsed(response, 'prompt_pages/update_message.html')

    def test_return_msg(self):
        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)

        msg = {
            'page_title': 'Update success',
            'title': 'Profile updated',
            'description': 'Your profile has been successfully updated!'
        }

        response = self.client.get('/update_status/')
        self.assertTemplateUsed(response, 'prompt_pages/update_message.html')

        #print(vars(response))
        self.assertEqual(response.context['message'],msg)

class UpdateProfileTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_anonymous_user_access(self):
        response = self.client.get('/update_status')
        self.assertTemplateNotUsed(response, 'prompt_pages/update_message.html')

class SignupTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'active_batches.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_valid_template(self):
        response = self.client.get('/signup/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response,'signup.html')

    def test_signup_student_post(self):
        email_signup_dict = {
            u'student_register': [u'some'],
            u'email': [u'stud@gmail.com'],
            u'email_hidden': [u'stud@gmail.com'],
            u'password1': [u'abc123AB@'],
            u'password2': [u'abc123AB@'],
            u'acc_type': [u'student'],
        }

        response = self.client.post('/signup/', email_signup_dict)

        cust_inst = CustomUser.objects.get(email='stud@gmail.com')
        cust_inst.is_verified = True
        cust_inst.save()

        response = self.client.post('/login/', {u'email': [u'stud@gmail.com'], u'password': [u'abc123AB@']})
        #print(vars(response))


        img = open(MEDIA_ROOT + 'images/male.png', 'rb')
        uploaded = SimpleUploadedFile(img.name, img.read(),content_type='image/png')

        details_dict = {
            u'roll_no': [u'1317L03'],
            u'first_name': [u'stud'],
            u'middle_name': [u'alias'],
            u'last_name': [u'cse'],
            u'gender': [u'M'],
            u'father_name': [u'daddy'],
            u'mother_name': [u'mummy'],
            u'department': department.objects.get(acronym='IT').pk,
            u'qualification': [u'UG'],
            u'batch': batch.objects.filter(start_year=2016).get(end_year=2020).pk,
            u'date_of_joining': '2016-10-10',
            u'permanent_address': [u'abc123AB'],
            u'temporary_address': [u'abc123AB'],
            u'dob': '1995-10-10',
            u'phone_number': [u'9659988771'],
            u'student_type': [u'hosteller'],
            u'student': [u'abc'],
            u'avatar': uploaded,
            u'user_name': [u'stud@gmail.com'],
            u'entry_type': [u'regular'],
            u'father_occupation': [u'test'],
            u'mother_occupation': [u'test'],
            u'father_annual_income': [u'50000'],
            u'mother_annual_income': [u'20000'],
            u'caste': [u'xyz'],
            u'community': [u'BC'],
        }

        response = self.client.post('/details_verification/', details_dict)

        # print(vars(response))

        # self.assertTemplateUsed(response,'prompt_pages/details_submit_success.html')
        self.assertRedirects(response, '/upload_image/')

        cust_inst = CustomUser.objects.get(email='stud@gmail.com')

        student_inst = student.objects.get(user=cust_inst)

        self.assertEqual(student_inst.roll_no,'1317L03')

        self.assertFalse(cust_inst.groups.filter(name='hod').exists())
        self.assertFalse(cust_inst.groups.filter(name='principal').exists())
        self.assertTrue(cust_inst.groups.filter(name='student').exists())
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor').exists())
        self.assertFalse(cust_inst.groups.filter(name='faculty').exists())

    def test_signup_hod_post(self):
        email_signup_dict = {
            u'student_register': [u'some'],
            u'email': [u'staff@gmail.com'],
            u'email_hidden': [u'staff@gmail.com'],
            u'password1': [u'abc123AB@'],
            u'password2': [u'abc123AB@'],
            u'acc_type': [u'staff'],
        }

        response = self.client.post('/signup/', email_signup_dict)
        #print(vars(response))

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')
        cust_inst.is_verified = True
        cust_inst.has_profile_picture = True
        cust_inst.save()

        response = self.client.post('/login/', {u'email': [u'staff@gmail.com'], u'password': [u'abc123AB@']})
        #print(vars(response))


        img = open(MEDIA_ROOT + 'images/male.png', 'rb')
        uploaded = SimpleUploadedFile(img.name, img.read(), content_type='image/png')

        details_dict = {
            u'staff_id': [u'abcd123'],
            u'first_name': [u'staff'],
            u'middle_name': [u'alias'],
            u'last_name': [u'cse'],
            u'gender': [u'F'],
            u'department': department.objects.get(acronym='IT').pk,
            u'qualification': [u'UG'],
            u'degree': [u'MS'],
            u'specialization': [u'Cloud computing'],
            u'designation': [u'Professor'],
            u'permanent_address': [u'abc123AB'],
            u'temporary_address': [u'abc123AB'],
            u'dob': timezone.now().date(),
            u'phone_number': [u'9659988771'],
            u'staff': [u'abc'],
            u'user_name': [u'staff@gmail.com'],
            u'avatar': uploaded,
        }

        response = self.client.post('/details_verification/', details_dict)

        print(vars(response))

        # self.assertTemplateUsed(response,'prompt_pages/details_submit_success.html')
        self.assertRedirects(response,'/upload_image/')

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')

        staff_inst = staff.objects.get(user=cust_inst)

        self.assertEqual(staff_inst.staff_id,'abcd123')
        self.assertTrue(cust_inst.groups.filter(name='hod').exists())
        self.assertFalse(cust_inst.groups.filter(name='principal').exists())
        self.assertFalse(cust_inst.groups.filter(name='student').exists())
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor').exists())
        self.assertTrue(cust_inst.groups.filter(name='faculty').exists())

    def test_signup_staff_post(self):
        email_signup_dict = {
            u'student_register': [u'some'],
            u'email': [u'staff@gmail.com'],
            u'email_hidden': [u'staff@gmail.com'],
            u'password1': [u'abc123AB@'],
            u'password2': [u'abc123AB@'],
            u'acc_type': [u'staff'],
        }

        response = self.client.post('/signup/', email_signup_dict)
        # print(vars(response))

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')
        cust_inst.is_verified = True
        cust_inst.save()

        response = self.client.post('/login/', {u'email': [u'staff@gmail.com'], u'password': [u'abc123AB@']})
        #print(vars(response))


        img = open(MEDIA_ROOT + 'images/male.png', 'rb')
        uploaded = SimpleUploadedFile(img.name, img.read(), content_type='image/png')

        details_dict = {
            u'staff_id': [u'abcd123'],
            u'first_name': [u'staff'],
            u'middle_name': [u'alias'],
            u'last_name': [u'cse'],
            u'gender': [u'F'],
            u'department': department.objects.get(acronym='IT').pk,
            u'qualification': [u'UG'],
            u'degree': [u'MS'],
            u'specialization': [u'Cloud computing'],
            u'designation': [u'Associate Professor'],
            u'permanent_address': [u'abc123AB'],
            u'temporary_address': [u'abc123AB'],
            u'dob': timezone.now().date(),
            u'phone_number': [u'9659988771'],
            u'staff': [u'abc'],
            u'user_name': [u'staff@gmail.com'],
            u'avatar': uploaded,
        }
        response = self.client.post('/details_verification/', details_dict)

        #print(vars(response))

        # self.assertTemplateUsed(response, 'prompt_pages/details_submit_success.html')
        self.assertRedirects(response, '/upload_image/')

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')

        staff_inst = staff.objects.get(user=cust_inst)

        self.assertEqual(staff_inst.staff_id,'abcd123')
        self.assertFalse(cust_inst.groups.filter(name='hod').exists())
        self.assertFalse(cust_inst.groups.filter(name='principal').exists())
        self.assertFalse(cust_inst.groups.filter(name='student').exists())
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor').exists())
        self.assertTrue(cust_inst.groups.filter(name='faculty').exists())

    def test_signup_pricipal_post(self):
        email_signup_dict = {
            u'student_register': [u'some'],
            u'email': [u'staff@gmail.com'],
            u'email_hidden': [u'staff@gmail.com'],
            u'password1': [u'abc123AB@'],
            u'password2': [u'abc123AB@'],
            u'acc_type': [u'staff'],
        }

        response = self.client.post('/signup/', email_signup_dict)
        # print(vars(response))

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')
        cust_inst.is_verified = True
        cust_inst.save()

        response = self.client.post('/login/', {u'email': [u'staff@gmail.com'], u'password': [u'abc123AB@']})
        #print(vars(response))


        img = open(MEDIA_ROOT + 'images/male.png', 'rb')
        uploaded = SimpleUploadedFile(img.name, img.read(), content_type='image/png')

        details_dict = {
            u'staff_id': [u'abcd123'],
            u'first_name': [u'staff'],
            u'middle_name': [u'alias'],
            u'last_name': [u'cse'],
            u'gender': [u'F'],
            u'department': department.objects.get(acronym='IT').pk,
            u'qualification': [u'UG'],
            u'degree': [u'MS'],
            u'specialization': [u'Cloud computing'],
            u'designation': [u'Principal'],
            u'permanent_address': [u'abc123AB'],
            u'temporary_address': [u'abc123AB'],
            u'dob': timezone.now().date(),
            u'phone_number': [u'9659988771'],
            u'staff': [u'abc'],
            u'user_name': [u'staff@gmail.com'],
            u'avatar': uploaded,
        }

        response = self.client.post('/details_verification/', details_dict)

        # print(vars(response))

        # self.assertTemplateUsed(response, 'prompt_pages/details_submit_success.html')
        self.assertRedirects(response, '/upload_image/')

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')

        staff_inst = staff.objects.get(user=cust_inst)

        self.assertEqual(staff_inst.staff_id,'abcd123')
        self.assertFalse(cust_inst.groups.filter(name='hod').exists())
        self.assertTrue(cust_inst.groups.filter(name='principal').exists())
        self.assertFalse(cust_inst.groups.filter(name='student').exists())
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor').exists())
        self.assertFalse(cust_inst.groups.filter(name='faculty').exists())

class ActivationLinkTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_student_activation_link(self):

        signup_dict = {
            u'student_register': [u'some'],
            u'email': [u'stud@gmail.com'],
            u'email_hidden': [u'stud@gmail.com'],
            u'password1': [u'abc123AB@'],
            u'password2': [u'abc123AB@'],
            u'acc_type': [u'student'],
        }

        response = self.client.post('/signup/', signup_dict)

        #print(vars(response))

        self.assertTemplateUsed(response,'prompt_pages/signup_success.html')

        cust_inst = CustomUser.objects.get(email='stud@gmail.com')

        """

        student_inst = student.objects.get(user=cust_inst)

        self.assertEqual(student_inst.roll_no,'1317L03')

        self.assertFalse(cust_inst.groups.filter(name='hod').exists())
        self.assertFalse(cust_inst.groups.filter(name='principal').exists())
        self.assertTrue(cust_inst.groups.filter(name='student').exists())
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor').exists())
        self.assertFalse(cust_inst.groups.filter(name='faculty').exists())"""

        act_link= cust_inst.activation_key

        #print('\n\nactivation link = ' + act_link)

        self.assertFalse(cust_inst.is_verified)

        response = self.client.get('/activate/'+act_link)

        #print(vars(response))

        self.assertTemplateUsed(response,'prompt_pages/activated_mail.html')
        self.assertInHTML(' <h1>Congrats - Few more steps...</h1>',str(response.content))

        cust_inst = CustomUser.objects.get(email='stud@gmail.com')
        self.assertTrue(cust_inst.is_verified)

    def test_staff_activation_link(self):

        signup_dict = {
            u'student_register': [u'some'],
            u'email': [u'staff@gmail.com'],
            u'email_hidden': [u'staff@gmail.com'],
            u'password1': [u'abc123AB@'],
            u'password2': [u'abc123AB@'],
            u'acc_type': [u'staff'],
        }

        response = self.client.post('/signup/', signup_dict)

        self.assertTemplateUsed(response,'prompt_pages/signup_success.html')

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')

        """

        staff_inst = staff.objects.get(user=cust_inst)

        self.assertEqual(staff_inst.staff_id,'abcd123')
        self.assertFalse(cust_inst.groups.filter(name='hod').exists())
        self.assertFalse(cust_inst.groups.filter(name='principal').exists())
        self.assertFalse(cust_inst.groups.filter(name='student').exists())
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor').exists())
        self.assertTrue(cust_inst.groups.filter(name='faculty').exists())"""

        act_link = cust_inst.activation_key

        # print('\n\nactivation link = ' + act_link)

        self.assertFalse(cust_inst.is_verified)

        response = self.client.get('/activate/' + act_link)

        # print(vars(response))

        self.assertTemplateUsed(response, 'prompt_pages/activated_mail.html')
        self.assertInHTML(' <h1>Congrats - Few more steps...</h1>', str(response.content))

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')
        self.assertTrue(cust_inst.is_verified)

        response = self.client.get('/activate/' + act_link)
        self.assertTemplateUsed(response, 'prompt_pages/activated_mail.html')
        self.assertInHTML('<h2>Your account has been already activated</h2>', str(response.content))

    def test_invalid_link(self):
        signup_dict = {
            u'student_register': [u'some'],
            u'email': [u'staff@gmail.com'],
            u'email_hidden': [u'staff@gmail.com'],
            u'password1': [u'abc123AB@'],
            u'password2': [u'abc123AB@'],
            u'acc_type': [u'staff'],
        }

        response = self.client.post('/signup/', signup_dict)

        self.assertTemplateUsed(response, 'prompt_pages/signup_success.html')
        cust_inst = CustomUser.objects.get(email='staff@gmail.com')

        """

        staff_inst = staff.objects.get(user=cust_inst)

        self.assertEqual(staff_inst.staff_id, 'abcd123')
        self.assertFalse(cust_inst.groups.filter(name='hod').exists())
        self.assertFalse(cust_inst.groups.filter(name='principal').exists())
        self.assertFalse(cust_inst.groups.filter(name='student').exists())
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor').exists())
        self.assertTrue(cust_inst.groups.filter(name='faculty').exists())"""

        act_link = cust_inst.activation_key

        # print('\n\nactivation link = ' + act_link)

        self.assertFalse(cust_inst.is_verified)

        response = self.client.get('/activate/' + act_link +'some_garbage')

        # print(vars(response))

        self.assertTemplateUsed(response, 'prompt_pages/error_page_base.html')
        msg = {
            'page_title': 'Expired Link',
            'title': 'Link Expired',
            'description': 'Please check whether you are clicking the latest sent to your mail'
        }
        self.assertEqual(response.context['message'],msg)

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')
        self.assertFalse(cust_inst.is_verified)

    def test_expired_link(self):
        signup_dict = {
            u'student_register': [u'some'],
            u'email': [u'staff@gmail.com'],
            u'email_hidden': [u'staff@gmail.com'],
            u'password1': [u'abc123AB@'],
            u'password2': [u'abc123AB@'],
            u'acc_type': [u'staff'],
        }

        response = self.client.post('/signup/', signup_dict)

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')
        act_link = cust_inst.activation_key
        cust_inst.key_expires = timezone.now() - datetime.timedelta(days=3)
        cust_inst.save()
        self.assertFalse(cust_inst.is_verified)
        response = self.client.get('/activate/' + act_link)
        self.assertTemplateUsed(response, 'prompt_pages/activated_mail.html')

        cust_inst = CustomUser.objects.get(email='staff@gmail.com')
        self.assertFalse(cust_inst.is_verified)

        self.assertInHTML('<h2>Activation link expired</h2>', str(response.content))

class NewActivationLinkTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_invalid_request(self):
        response = self.client.get('/new-activation-link/65695161')
        self.assertEqual(response.status_code,404)
        self.assertRaises(Http404)

    def test_valid_student_request(self):
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_verified = False
        cust_inst.save()
        response = self.client.get('/new-activation-link/' + str(cust_inst.pk))

        #print(vars(response))

        self.assertTemplateUsed(response,'prompt_pages/signup_success.html')
        msg = {
            'page_title': 'GCT | Re-sent conformation mail',
            'title': 'Email re-sent',
            'description': 'An email has been sent your mail ID.Please verify it to proceed',
        }
        self.assertEquals(response.context['message'],msg)

    def test_valid_staff_request(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_verified = False
        cust_inst.save()
        response = self.client.get('/new-activation-link/' + str(cust_inst.pk))

        # print(vars(response))

        self.assertTemplateUsed(response, 'prompt_pages/signup_success.html')
        msg = {
            'page_title': 'GCT | Re-sent conformation mail',
            'title': 'Email re-sent',
            'description': 'An email has been sent your mail ID.Please verify it to proceed',
        }
        self.assertEquals(response.context['message'], msg)

class UpdateProfileTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_anonymous_access(self):
        response = self.client.get('/edit_profile')
        self.assertTemplateNotUsed(response, 'edit_form.html')

    def test_student_update_profile(self):

        img = open(MEDIA_ROOT + 'images/male.png', 'rb')
        uploaded = SimpleUploadedFile(img.name, img.read(), content_type='image/png')
        datas = {
            u'first_name': [u'stud'],
            u'middle_name': [u'alias'],
            u'last_name': [u'cse'],
            u'gender': [u'M'],
            u'student_type': [u'hosteller'],
            u'entry_type': [u'regular'],
            u'father_name': [u'father'],
            u'mother_name': [u'mommy'],
            u'department': department.objects.get(acronym='IT').pk,
            u'qualification': [u'UG'],
            u'batch': batch.objects.filter(start_year=2016).get(end_year=2020).pk,
            u'date_of_joining': timezone.now().date(),
            u'permanent_address': [u'abc123AB'],
            u'temporary_address': [u'abc123AB'],
            u'dob': datetime.datetime.strptime("09-10-1995","%d-%m-%Y").date(),
            u'phone_number': [u'9659988771'],
            u'avatar': uploaded,
            u'father_occupation': [u'test'],
            u'mother_occupation': [u'test'],
            u'father_annual_income': [u'50000'],
            u'mother_annual_income': [u'20000'],
            u'caste': [u'xyz'],
            u'community': [u'BC'],
        }
        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.post('/edit_profile', datas)

        print(vars(response))

        self.assertRedirects(response,'/update_status/')

        cust_inst = CustomUser.objects.get(email='s1@gmail.com')

        stud_inst = student.objects.get(user=cust_inst)
        self.assertEqual(stud_inst.father_name,'Father')
        self.assertEqual(stud_inst.mother_name,'Mommy')

    def test_staff_update_profile(self):

        img = open(MEDIA_ROOT + 'images/male.png', 'rb')
        uploaded = SimpleUploadedFile(img.name, img.read(), content_type='image/png')
        datas = {
            u'first_name': [u'staff'],
            u'middle_name': [u'alias'],
            u'last_name': [u'cse'],
            u'gender': [u'F'],
            u'department': department.objects.get(acronym='IT').pk,
            u'qualification': [u'UG'],
            u'degree': [u'MS'],
            u'specialization': [u'Cloud computing'],
            u'designation': [u'Principal'],
            u'permanent_address': [u'abc123AB'],
            u'temporary_address': [u'abc123AB'],
            u'dob': timezone.now().date(),
            u'phone_number': [u'9659988771'],
            u'avatar': uploaded,
        }
        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.post('/edit_profile', datas)

        #print(str(response))

        self.assertRedirects(response,'/update_status/')

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')

        staff_inst = staff.objects.get(user=cust_inst)
        self.assertEqual(staff_inst.specialization,'Cloud computing')



















