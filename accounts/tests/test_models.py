from django.test import TestCase
from django.utils import timezone

from accounts.models import *


class TestBatch(TestCase):
    fixtures = ['regulation.json', ]

    def test_batch_model_ug_string(self):
        batch.objects.create(
            start_year=2016,
            end_year=2020,
            programme='UG',
            regulation=regulation.objects.all()[0]
        )

        obj = batch.objects.get(start_year=2016)
        self.assertEqual(str(obj), '2016-2020')

    def test_batch_model_pg_string(self):
        batch.objects.create(
            start_year=2016,
            end_year=2018,
            programme='PG',
            regulation=regulation.objects.all()[0]
        )

        obj = batch.objects.get(end_year=2018)
        self.assertEqual(str(obj), '2016-2018')

    def test_batch_start_year(self):
        batch.objects.create(
            start_year=2016,
            end_year=2020,
            programme='UG',
            regulation=regulation.objects.all()[0]
        )

        start_year = batch.objects.get(start_year=2016).get_start_year()
        self.assertEqual(start_year, 2016)


class TestDepartment(TestCase):
    fixtures = ['department.json']

    def test_department_model_string(self):
        obj = department.objects.get(acronym='CSE')
        self.assertEqual(str(obj), 'Computer Science and Engineering')


class TestStaff(TestCase):
    fixtures = [
        'ct.json',
        'perms.json',
        'group.json', 'department.json',
        'customuser.json', 'staff.json'
    ]

    def test_staff_model_string(self):
        obj = staff.objects.filter(first_name='F1').get(last_name='cse')

        self.assertEqual(str(obj), 'F1 of cse')

    def test_get_department_function(self):
        department_instance = department.objects.get(acronym='IT')
        staff_instance = staff.objects.filter(first_name='F1').get(last_name='cse')
        self.assertNotEqual(department_instance, staff_instance.get_department())

        department_instance = department.objects.get(acronym='CSE')
        staff_instance = staff.objects.filter(first_name='F1').get(last_name='cse')
        self.assertEqual(department_instance, staff_instance.get_department())

    def test_get_department_id_function(self):
        department_instance = department.objects.get(acronym='CSE')
        department_id = getattr(department_instance, 'pk')
        staff_instance = staff.objects.filter(first_name='F1').get(last_name='cse')
        self.assertEqual(department_id, staff_instance.get_department_id())


class TestStudent(TestCase):
    fixtures = [
        'department.json',
        'regulation.json',
        'ct.json', 'perms.json',
        'batch.json', 'active_batches.json',
        'group.json', 'customuser.json',
        'student.json',
    ]

    def test_student_model_string(self):
        student_instance = student.objects.get(last_name="A")

        self.assertEqual(str(student_instance), 'student alias  A')

    def test_student_get_roll_number_funtion(self):
        student_instance = student.objects.get(last_name="A")
        student_reg_number = getattr(student_instance, 'roll_no')

        self.assertEqual(student_instance.get_roll_number(), student_reg_number)


class TestCustomUser(TestCase):
    fixtures = [
        'ct.json', 'perms.json',
        'group.json', 'customuser.json']

    def test_custom_user_model_string(self):
        custom_user_instance = CustomUser.objects.get(email='hod@gmail.com')
        self.assertEqual(str(custom_user_instance), 'hod@gmail.com')

    def test_get_full_name_function(self):
        super_user_instance = CustomUser.objects.create_superuser('admin@gmail.com', '123456',
                                                                  activation_key='abc',
                                                                  key_expires=timezone.now())
        self.assertEqual(super_user_instance.get_full_name(), 'admin@gmail.com')

    def test_get_short_name_function(self):
        super_user_instance = CustomUser.objects.create_superuser('admin@gmail.com', '123456',
                                                                  activation_key='abc',
                                                                  key_expires=timezone.now())
        self.assertEqual(super_user_instance.get_short_name(), 'admin@gmail.com')

    def test_email_exception(self):
        with self.assertRaises(TypeError):
            user_instance = CustomUser.objects.create_user(password='123456')
