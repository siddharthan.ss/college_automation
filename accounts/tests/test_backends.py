from django.test import TestCase

from accounts.backends import *


class TestCustomAuthBackend(TestCase):
    fixtures = [
        'ct.json',
        'perms.json',
        'group.json', 'customuser.json',
    ]

    def setUp(self):
        self.custom_auth_instance = CustomUserAuth()

    def test_authenticate_function(self):
        ret_val = self.custom_auth_instance.authenticate(username='sid@gmail.com', password='123456')
        self.assertIsNone(ret_val)

        ret_obj = self.custom_auth_instance.authenticate(username='hod@gmail.com', password='123456')
        self.assertIsNotNone(ret_obj)
        self.assertEqual(ret_obj, CustomUser.objects.get(email='hod@gmail.com'))

    def test_get_user_function(self):
        ret_val = self.custom_auth_instance.get_user('1234')
        self.assertIsNone(ret_val)

        custom_user_instance = CustomUser.objects.get(email='hod@gmail.com')
        primary_key = getattr(custom_user_instance, 'pk')

        ret_obj = self.custom_auth_instance.get_user(primary_key)
        self.assertIsNotNone(ret_obj)
        self.assertEqual(ret_obj, CustomUser.objects.get(email='hod@gmail.com'))

        custom_user_instance.is_active = False
        custom_user_instance.save()
        ret_obj = self.custom_auth_instance.get_user(primary_key)
        self.assertIsNone(ret_obj)
