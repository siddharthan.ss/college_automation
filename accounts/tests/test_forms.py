import copy
import unittest

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase

from accounts.forms import *
from accounts.models import department, batch
from college_automation.settings import *


class TestStudentRegistrationForm(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json', 'perms.json',
        'department.json', 'batch.json', 'active_batches.json',
        'group.json',
        'customuser.json', 'student.json',
    ]

    def setUp(self):
        department_instance = department.objects.get(acronym="IT")
        self.batch_instance = batch.objects.filter(end_year=2020).get(start_year=2016)
        img = open(MEDIA_ROOT + 'images/male.png', 'rb')
        self.uploaded = SimpleUploadedFile(img.name, img.read(), content_type='image/png')
        self.form_data = {
            'roll_no': '1317L01',
            'first_name': 'Aravind',
            'middle_name': '',
            'last_name': 'Raj',
            'gender': 'M',
            'father_name': 'father',
            'mother_name': 'Mother',
            'department': department_instance.pk,
            'qualification': 'UG',
            'batch': self.batch_instance.pk,
            'date_of_joining': '2016-06-06',
            'permanent_address': 'Cbe',
            'temporary_address': 'Cbe',
            'dob': '1995-09-09',
            'phone_number': '9659988574',
            'student_type': 'hosteller',
            'avatar': 'ABC',
            'entry_type': 'regular',
            'father_occupation': 'ABC',
            'mother_occupation': 'ABC',
            'mother_annual_income': '20000',
            'father_annual_income': '20000',
            'caste': 'ABC',
            'community': 'BC',
        }

    def test_stuent_reg_valid_form(self):
        s = student()
        form = RegisterStudent(data=self.form_data, instance=s, files={'avatar': self.uploaded})
        print(form.errors)
        required_field = ['email',
                          'password1',
                          'password2',
                          'roll_no',
                          'first_name',
                          'last_name',
                          'gender',
                          'department',
                          'batch',
                          'qualification',
                          'date_of_joining',
                          'permanent_address',
                          'temporary_address',
                          'dob',
                          'phone_number',
                          ]
        """
        if (form.errors):
            for field in required_field:
                if form[field].errors:
                    print(field + ' ' + form[field].errors)
        """
        self.assertTrue(form.is_valid())

    def test_required_fields(self):
        form_data_copy = copy.deepcopy(self.form_data)

        form_data_keys = self.form_data.keys()

        for key in form_data_keys:
            del form_data_copy[key]
            s = student()
            form = RegisterStudent(data=form_data_copy, instance=s)
            # print(form_data_copy)
            if key in RegisterStudent.required_field:
                self.assertIn('This field is required.', form.errors[key])

    def test_invalid_roll_no(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_roll_no = [
            '12345ab',
            '1317l02'
        ]

        for invalid_data in invalid_roll_no:
            form_data_copy['roll_no'] = invalid_data
            s = student()
            form = RegisterStudent(form_data_copy, instance=s)
            self.assertIn('Enter a valid roll_number', form.errors['roll_no'])

    def test_duplicate_roll_no(self):
        form_data_copy = copy.deepcopy(self.form_data)
        form_data_copy['roll_no'] = '1317148'
        s = student()
        form = RegisterStudent(form_data_copy, instance=s)
        self.assertIn('This roll number has already been registered', form.errors['roll_no'])

    def test_invalid_first_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_first_name = [
            'sid123',
            '1233',
            'Dr.',
            # 'sid dis',
        ]

        for invalid_data in invalid_first_name:
            form_data_copy['first_name'] = invalid_data
            s = student()
            form = RegisterStudent(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['first_name'])

    def test_invalid_middle_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_middle_name = [
            'sid123',
            '1233',
            'Dr.',
            'sid dis',
        ]

        for invalid_data in invalid_middle_name:
            form_data_copy['middle_name'] = invalid_data
            s = student()
            form = RegisterStudent(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['middle_name'])

    def test_invalid_last_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_last_name = [
            'sid123',
            '1233',
            # 'Dr.',
        ]

        for invalid_data in invalid_last_name:
            form_data_copy['last_name'] = invalid_data
            s = student()
            form = RegisterStudent(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['last_name'])

    def test_invalid_father_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_father_name = [
            'sid123',
            '1233',
        ]

        for invalid_data in invalid_father_name:
            form_data_copy['father_name'] = invalid_data
            s = student()
            form = RegisterStudent(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['father_name'])

    def test_invalid_mother_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_mother_name = [
            'sid123',
            '1233',
        ]

        for invalid_data in invalid_mother_name:
            form_data_copy['mother_name'] = invalid_data
            s = student()
            form = RegisterStudent(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['mother_name'])

    def test_invalid_phone_number(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_phone_number = [
            '1234abc',
            '123456789'
        ]

        for invalid_data in invalid_phone_number:
            form_data_copy['phone_number'] = invalid_data
            s = student()
            form = RegisterStudent(form_data_copy, instance=s)
            self.assertIn('Enter a valid mobile number', form.errors['phone_number'])

    @unittest.skip('todo')
    def test_invalid_date_of_joining_regular(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_date_of_joinig = [
            '1999-09-09',
        ]

        for invalid_data in invalid_date_of_joinig:
            form_data_copy['date_of_joining'] = invalid_data
            s = student()
            form = RegisterStudent(form_data_copy, instance=s)
            self.assertIn('Please enter a proper date of joining', form.errors['date_of_joining'])

    @unittest.skip('todo')
    def test_invalid_date_of_joining_others(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_date_of_joinig = [
            str(self.batch_instance.start_year) + '-09-09',
        ]

        for invalid_data in invalid_date_of_joinig:
            form_data_copy['date_of_joining'] = invalid_data
            form_data_copy['entry_type'] = 'lateral'
            s = student()
            form = RegisterStudent(form_data_copy, instance=s)
            self.assertIn('Please enter a proper date of joining', form.errors['date_of_joining'])

    def test_invalid_dob(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_dob = [
            str(self.batch_instance.start_year - 15) + '-09-09',
        ]

        for invalid_data in invalid_dob:
            form_data_copy['dob'] = invalid_data
            s = student()
            form = RegisterStudent(form_data_copy, instance=s)
            self.assertIn('Please enter proper date of birth', form.errors['dob'])


class TestUpdateStudentRegistrationForm(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json', 'perms.json',
        'department.json', 'batch.json', 'active_batches.json',
        'group.json',
        'customuser.json', 'student.json',
    ]

    def setUp(self):
        department_instance = department.objects.get(acronym="IT")
        batch_instance = batch.objects.filter(end_year=2020).get(start_year=2016)
        img = open(MEDIA_ROOT + 'images/male.png', 'rb')
        self.uploaded = SimpleUploadedFile(img.name, img.read(), content_type='image/png')
        self.form_data = {
            'email': 'sid@gmail.com',
            'password1': '12345678',
            'password2': '12345678',
            'roll_no': '1317L01',
            'first_name': 'Aravind',
            'middle_name': '',
            'last_name': 'Raj',
            'gender': 'M',
            'student_type': 'hosteller',
            'entry_type': 'regular',
            'father_name': 'father',
            'mother_name': 'Mother',
            'department': department_instance.pk,
            'qualification': 'UG',
            'batch': batch_instance.pk,
            'date_of_joining': '2016-06-06',
            'permanent_address': 'Cbe',
            'temporary_address': 'Cbe',
            'dob': '1995-09-09',
            'phone_number': '9659988574',
            'avatar': 'ABC',
            'father_occupation': 'ABC',
            'mother_occupation': 'ABC',
            'mother_annual_income': '20000',
            'father_annual_income': '20000',
            'caste': 'ABC',
            'community': 'BC',
        }

    def test_forms(self):
        s = student()
        form = regstud(data=self.form_data, instance=s, files={'avatar': self.uploaded})
        # print(form.errors)
        self.assertTrue(form.is_valid())

    def test_required_fields(self):
        form_data_copy = copy.deepcopy(self.form_data)

        form_data_keys = self.form_data.keys()

        for key in form_data_keys:
            del form_data_copy[key]
            s = student()
            form = regstud(data=form_data_copy, instance=s)
            # print(form_data_copy)
            if key in regstud.required_field:
                self.assertIn('This field is required.', form.errors[key])

    def test_invalid_first_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_first_name = [
            'sid123',
            '1233',
            'Dr.',
            'sid dis',
        ]

        for invalid_data in invalid_first_name:
            form_data_copy['first_name'] = invalid_data
            s = student()
            form = regstud(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['first_name'])

    def test_invalid_middle_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_middle_name = [
            'sid123',
            '1233',
            'Dr.',
            'sid dis',
        ]

        for invalid_data in invalid_middle_name:
            form_data_copy['middle_name'] = invalid_data
            s = student()
            form = regstud(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['middle_name'])

    def test_invalid_last_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_last_name = [
            'sid123',
            '1233',
            'Dr.',
        ]

        for invalid_data in invalid_last_name:
            form_data_copy['last_name'] = invalid_data
            s = student()
            form = regstud(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['last_name'])

    def test_invalid_father_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_father_name = [
            'sid123',
            '1233',
        ]

        for invalid_data in invalid_father_name:
            form_data_copy['father_name'] = invalid_data
            s = student()
            form = regstud(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['father_name'])

    def test_invalid_mother_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_mother_name = [
            'sid123',
            '1233',
        ]

        for invalid_data in invalid_mother_name:
            form_data_copy['mother_name'] = invalid_data
            s = student()
            form = regstud(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['mother_name'])

    def test_invalid_phone_number(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_phone_number = [
            '1234abc',
            '123456789'
        ]

        for invalid_data in invalid_phone_number:
            form_data_copy['phone_number'] = invalid_data
            s = student()
            form = regstud(form_data_copy, instance=s)
            self.assertIn('Enter a valid mobile number', form.errors['phone_number'])


class TestStaffRegistrationForm(TestCase):
    fixtures = [
        'ct.json',
        'department.json', 'perms.json',
        'group.json',
        'customuser.json', 'staff.json',
    ]

    def setUp(self):
        department_instance = department.objects.get(acronym="IT")
        img = open(MEDIA_ROOT + 'images/male.png', 'rb')
        self.uploaded = SimpleUploadedFile(img.name, img.read(), content_type='image/png')
        self.form_data = {
            'staff_id': '123456',
            'first_name': 'HOD',
            'middle_name': 'of',
            'last_name': 'cse',
            'gender': 'M',
            'qualification': 'PG',
            'degree': 'BE',
            'specialization': 'cloud',
            'designation': staff.DESIGNATION_CHOICES[0][0],
            'permanent_address': 'cbe',
            'temporary_address': 'cbe',
            'dob': '1954-06-06',
            'phone_number': '9632587410',
            'department': department_instance.pk,
            'avatar': 'ABC',
        }

    def test_forms(self):
        s = staff()
        form = RegisterStaff(data=self.form_data, instance=s, files={'avatar': self.uploaded})
        required_field = [
            'staff_id',
            'first_name',
            'last_name',
            'gender',
            'degree',
            'designation',
            'permanent_address',
            'temporary_address',
            'dob',
            'phone_number',
        ]
        """
        if(form.errors):
            for field in required_field:
                if form[field].errors:
                    print(field + ' ' + form[field].errors)
        """
        self.assertTrue(form.is_valid())

    def test_required_fields(self):
        form_data_copy = copy.deepcopy(self.form_data)

        form_data_keys = self.form_data.keys()

        for key in form_data_keys:
            del form_data_copy[key]
            s = staff()
            form = RegisterStudent(data=form_data_copy, instance=s)
            # print(form_data_copy)
            if key in RegisterStudent.required_field:
                self.assertIn('This field is required.', form.errors[key])

    def test_invalid_first_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_first_name = [
            'sid123',
            '1233',
            'Dr.',
            'sid dis',
        ]

        for invalid_data in invalid_first_name:
            form_data_copy['first_name'] = invalid_data
            s = staff()
            form = RegisterStaff(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['first_name'])

    def test_invalid_middle_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_middle_name = [
            'sid123',
            '1233',
            'Dr.',
            'sid dis',
        ]

        for invalid_data in invalid_middle_name:
            form_data_copy['middle_name'] = invalid_data
            s = staff()
            form = RegisterStaff(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['middle_name'])

    def test_invalid_last_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_last_name = [
            'sid123',
            '1233',
            'Dr.',
        ]

        for invalid_data in invalid_last_name:
            form_data_copy['last_name'] = invalid_data
            s = staff()
            form = RegisterStaff(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['last_name'])

    def test_invalid_phone_number(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_phone_number = [
            '1234abc',
            '123456789'
        ]

        for invalid_data in invalid_phone_number:
            form_data_copy['phone_number'] = invalid_data
            s = staff()
            form = RegisterStaff(form_data_copy, instance=s)
            self.assertIn('Enter a valid mobile number', form.errors['phone_number'])

    def test_duplicate_roll_no(self):
        form_data_copy = copy.deepcopy(self.form_data)
        form_data_copy['staff_id'] = '145209'
        s = staff()
        form = RegisterStaff(form_data_copy, instance=s)
        self.assertIn('This staff number has already been registered', form.errors['staff_id'])


class TestUpdateStaffRegistrationForm(TestCase):
    fixtures = [
        'ct.json',
        'department.json', 'perms.json',
        'group.json',
        'customuser.json', 'staff.json',
    ]

    def setUp(self):
        department_instance = department.objects.get(acronym="IT")

        img = open(MEDIA_ROOT + 'images/male.png', 'rb')
        self.uploaded = SimpleUploadedFile(img.name, img.read(), content_type='image/png')
        self.form_data = {
            'email': 'hod1@gmail.com',
            'password1': '12345678',
            'password2': '12345678',
            'staff_id': '123456',
            'first_name': 'HOD',
            'middle_name': 'of',
            'last_name': 'cse',
            'gender': 'M',
            'qualification': 'PG',
            'degree': 'BE',
            'specialization': 'cloud',
            'designation': staff.DESIGNATION_CHOICES[0][0],
            'permanent_address': 'cbe',
            'temporary_address': 'cbe',
            'dob': '1954-06-06',
            'phone_number': '9632587410',
            'department': department_instance.pk,
            'avatar': 'ABC',
        }

    def test_forms(self):
        s = staff()
        form = regstaff(data=self.form_data, instance=s, files={'avatar': self.uploaded})
        self.assertTrue(form.is_valid())

    def test_required_fields(self):
        form_data_copy = copy.deepcopy(self.form_data)

        form_data_keys = self.form_data.keys()

        for key in form_data_keys:
            del form_data_copy[key]
            s = staff()
            form = regstaff(data=form_data_copy, instance=s)
            # print(form_data_copy)
            if key in regstaff.required_field:
                self.assertIn('This field is required.', form.errors[key])

    def test_invalid_first_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_first_name = [
            'sid123',
            '1233',
            'Dr.',
            'sid dis',
        ]

        for invalid_data in invalid_first_name:
            form_data_copy['first_name'] = invalid_data
            s = staff()
            form = regstaff(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['first_name'])

    def test_invalid_middle_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_middle_name = [
            'sid123',
            '1233',
            'Dr.',
            'sid dis',
        ]

        for invalid_data in invalid_middle_name:
            form_data_copy['middle_name'] = invalid_data
            s = staff()
            form = regstaff(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['middle_name'])

    def test_invalid_last_name(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_last_name = [
            'sid123',
            '1233',
            'Dr.',
        ]

        for invalid_data in invalid_last_name:
            form_data_copy['last_name'] = invalid_data
            s = staff()
            form = regstaff(form_data_copy, instance=s)
            self.assertIn('Enter a valid name', form.errors['last_name'])

    def test_invalid_phone_number(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_phone_number = [
            '1234abc',
            '123456789'
        ]

        for invalid_data in invalid_phone_number:
            form_data_copy['phone_number'] = invalid_data
            s = staff()
            form = regstaff(form_data_copy, instance=s)
            self.assertIn('Enter a valid mobile number', form.errors['phone_number'])


class TestLoginForm(TestCase):
    fixtures = [
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
    ]

    def setUp(self):
        self.form_data = {}

    def test_invalid_email(self):
        invalid_emails = [
            'sid',
            'sid@',
            'sid@gmail',
            'sid@.com',
            '@',
            '@.com',
        ]

        for invalid_data in invalid_emails:
            self.form_data['email'] = invalid_data
            form = LoginForm(self.form_data)
            self.assertIn('Please enter a valid email id', form.errors['email'])

    def test_email_does_not_exists(self):
        self.form_data['email'] = 'doesnotexist@gmail.com'
        form = LoginForm(self.form_data)
        self.assertIn('This email id has not been registered yet', form.errors['email'])

        """
class SignupFormTest():


    def test_invalid_email(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_emails = [
            'sid',
            'sid@',
            'sid@gmail',
            'sid@.com',
            '@',
            '@.com',
        ]

        for invalid_data in invalid_emails:
            form_data_copy['email']  = invalid_data
            s = staff()
            form = RegisterStaff(form_data_copy, instance=s)
            self.assertIn('Please enter a valid email id', form.errors['email'])

    def test_email_already_exists(self):
        CustomUser.objects.create_user(email='siddy@gmail.com')
        form_data_copy = copy.deepcopy(self.form_data)
        form_data_copy['email'] = 'siddy@gmail.com'
        s = staff()
        form = RegisterStaff(form_data_copy, instance=s)
        self.assertIn('This email id has already been registered', form.errors['email'])

    def test_invalid_password(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_password = [
            '1234567',
        ]

        for invalid_data in invalid_password:
            form_data_copy['password1'] = invalid_data
            s = staff()
            form = RegisterStaff(form_data_copy, instance=s)
            self.assertIn('Password should contain atleast 8 characters', form.errors['password1'])

    def test_invalid_password_2(self):
        form_data_copy = copy.deepcopy(self.form_data)
        invalid_password = [
            '1234567',
        ]

        for invalid_data in invalid_password:
            form_data_copy['password2'] = invalid_data
            s = staff()
            form = RegisterStaff(form_data_copy, instance=s)
            self.assertIn('Your passwords do not match', form.errors['password2'])

"""
