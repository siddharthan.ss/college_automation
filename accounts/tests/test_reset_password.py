from django.test import Client
from django.test import TestCase


class ResetPasswordTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_reset_password_correct_html(self):
        response = self.client.get('/user/password/reset/')
        self.assertTemplateUsed(response, 'registration/password_reset_form.html')
        self.assertContains(response, 'GCT PORTAL')

    def test_reset_password_sent_correct_html(self):
        response = self.client.get('/user/password/reset/done/')
        self.assertTemplateUsed(response, 'registration/password_reset_done.html')
        self.assertContains(response, 'GCT PORTAL')
        self.assertContains(response, 'Password Reset Request sent!')

    def test_reset_password_done_correct_html(self):
        response = self.client.get('/user/password/done/')
        self.assertTemplateUsed(response, 'registration/password_reset_complete.html')
        self.assertContains(response, 'GCT PORTAL')
        self.assertContains(response, 'Password Reset Done!')
