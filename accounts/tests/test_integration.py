from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import tag
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait


@tag('selenium')
class HOD(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super(HOD, cls).setUpClass()
        cls.browser = WebDriver()
        cls.browser.maximize_window()

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super(HOD, cls).tearDownClass()

    def logoutHod(self):
        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="user-profile dropdown-toggle"]')))
        system.click()
        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@href="/logout/"]')))
        system.click()

    def loginIntoHod(self):
        self.browser.find_element_by_id('login').click()
        self.assertIn('GCT | Login', self.browser.title)
        username_input = self.browser.find_element_by_name("email")
        username_input.send_keys('hod@gmail.com')
        password_input = self.browser.find_element_by_name("password")
        password_input.send_keys('123456')
        self.browser.find_element_by_name("login").click()

    def assginProgrammmeCoordinator(self):
        self.browser.find_element_by_xpath("id('sidebar-menu')/div/ul/li[1]/a").click()
        self.browser.find_element_by_xpath("//a[@href='/assign_programme_coordinator']").click()

        select = Select(self.browser.find_element_by_name('programme_coordinator'))
        select.select_by_value("2")
        self.browser.find_element_by_name("pc").click()

        # ActionChains(self.browser).move_to_element(menu).click(hidden_submenu).perform()

    def approvalTesting(self):
        self.browser.find_element_by_xpath("id('sidebar-menu')/div/ul/li[4]/a").click()
        """
            Approve user and check whether it is in deapprove list
            Deapprove that user and check whether that is in approve list
        """

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.element_to_be_clickable((By.XPATH, "id('sidebar-menu')/div/ul/li[4]/ul/li[1]/a")))
        system.click()

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'F1 of cse')))
        system.click()

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@value='f1@gmail.com']")))
        system.click()

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'f1@gmail.com')))
        system.click()

        wait = WebDriverWait(self.browser, 30)
        system = wait.until(EC.element_to_be_clickable((By.ID, "approve_btn")))
        system.click()

    def deapprovalTesting(self):
        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located((By.XPATH, "//a[@href='/deapproval']")))
        system = wait.until(EC.element_to_be_clickable((By.XPATH, "//a[@href='/deapproval']")))
        system.click()

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.element_to_be_clickable((By.XPATH, "//input[@value='f1@gmail.com']")))
        system.click()

        wait = WebDriverWait(self.browser, 30)
        system = wait.until(EC.element_to_be_clickable((By.ID, "deapprove_btn")))
        system.click()

    def approvalTestAgain(self):
        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.element_to_be_clickable((By.XPATH, "id('sidebar-menu')/div/ul/li[4]/ul/li[1]/a")))
        system.click()

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.element_to_be_clickable((By.PARTIAL_LINK_TEXT, 'f1@gmail.com')))
        system.click()

    def test_hod_tasks(self):
        self.browser.get('http://127.0.0.1:8000')
        self.assertIn('GCT', self.browser.title)
        self.loginIntoHod()
        self.assginProgrammmeCoordinator()
        self.approvalTesting()
        self.deapprovalTesting()
        self.approvalTestAgain()
        self.logoutHod()
