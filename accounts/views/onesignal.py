from django.contrib.auth.decorators import login_required

from accounts.models import onesignal
from django.http import JsonResponse

@login_required
def add_onesignal_id(request):
    if request.is_ajax():
        onesignal_id = request.POST['onesignal_id']

        onesignal.objects.create(
            user = request.user,
            onesignal_id = onesignal_id,
        )

        return_data = {}
        return_data['message'] = 'onesignal success'

        return JsonResponse(return_data)