from django.utils import timezone

from accounts.models import student
from curriculum.models import attendance

from common.utils.studentUtil import getSemesterPlanofStudent

class studentProfile:
    def attendance_status(self, attendance_inst, student_inst):
        absent_list = attendance_inst.absent_students.all()
        if student_inst in absent_list:
            return 'A'

        present_list = attendance_inst.present_students.all()
        if student_inst in present_list:
            return 'P'

        od_list = attendance_inst.onduty_students.all()
        if student_inst in od_list:
            return 'O'

        return None

    def attendance_status_single(self, semester_inst, course_inst, date, hour, student_inst):
        attendance_inst = attendance.objects.filter(
            semester=semester_inst
        ).filter(
            date=date
        ).filter(
            course=course_inst
        ).get(
            hour=hour
        )
        return self.attendance_status(attendance_inst, student_inst)

    @staticmethod
    def get_attendance_queryset_for_given_range(semester_inst, course_inst, start_date, end_date):
        return attendance.objects.filter(
            semester=semester_inst
        ).filter(
            date__gte=start_date
        ).filter(
            date__lte=end_date
        ).filter(
            course=course_inst
        ).order_by('date')

    def get_list_of_attendance_entries_for_student_in_range(self, attendance_inst_range_queryset, student_inst):
        list_of_attendance_entries = []
        self.total_hours = 0
        self.present_hours = 0
        self.absent_hours = 0
        self.onduty_hours = 0
        for entry in attendance_inst_range_queryset:
            self.total_hours += 1
            temp = {}
            temp['date'] = entry.date
            temp['hour'] = entry.hour
            temp['status'] = self.attendance_status(entry, student_inst)
            if temp['status'] == 'P':
                self.present_hours += 1
            elif temp['status'] == 'O':
                self.onduty_hours += 1
            elif temp['status'] == 'A':
                self.absent_hours += 1
            else:
                self.total_hours -= 1

            list_of_attendance_entries.append(temp)

        return list_of_attendance_entries

    def get_total_working_hours(self):
        return self.total_hours

    def get_present_hours(self):
        return self.present_hours

    def get_absent_hours(self):
        return self.absent_hours

    def get_onduty_hours(self):
        return self.onduty_hours

    def get_present_percentage(self):
        if self.total_hours > 0:
            return format((self.present_hours / self.total_hours) * 100, '.2f')
        else:
            return 0

    def get_onduty_percentage(self):
        if self.total_hours > 0:
            return format((self.onduty_hours / self.total_hours) * 100, '.2f')
        else:
            return 0

    def get_total_present_percentage(self):
        if self.total_hours > 0:
            return format(((self.onduty_hours + self.present_hours) / self.total_hours) * 100, '.2f')
        else:
            return 0

    def get_internal_mark(self, course_instance, student_inst):
        current_semester_plan_of_student = getSemesterPlanofStudent(student_inst)
        start_date = current_semester_plan_of_student.start_term_1
        if current_semester_plan_of_student.end_term_3 != None:
            end_date = current_semester_plan_of_student.end_term_3
        else:
            end_date = timezone.now().date()
        self.get_attendance_queryset_for_given_range(current_semester_plan_of_student, course_instance, start_date, end_date)

        percentage = self.get_present_percentage()

        if percentage >= 95:
            return 10
        elif percentage >= 90:
            return 8
        elif percentage >= 85:
            return 6
        elif percentage >= 80:
            return 4
        elif percentage > 75:
            return 2
        else:
            return 0

    def get_internal_mark_for_student(self, course_instance,student_instance):
        current_semester_plan_of_student = getSemesterPlanofStudent(student_instance)
        start_date = current_semester_plan_of_student.start_term_1
        if current_semester_plan_of_student.end_term_3 != None:
            end_date = current_semester_plan_of_student.end_term_3
        else:
            end_date = timezone.now().date()
        self.get_attendance_queryset_for_given_range(current_semester_plan_of_student, course_instance, start_date, end_date)

        percentage = self.get_present_percentage()

        if percentage >= 95:
            return 10
        elif percentage >= 90:
            return 8
        elif percentage >= 85:
            return 6
        elif percentage >= 80:
            return 4
        elif percentage > 75:
            return 2
        else:
            return 0


