from django.core.mail import send_mail

from college_automation import settings
import urllib
import json

def validateEmailAddress(emailAddressString):
    lookupStringUrl = 'http://apilayer.net/api/check? access_key = 1fc9f875ef097c8476123855c0a9798a\
    & email = '+ emailAddressString +'\
    & smtp = 1'

    response = urllib.request.urlopen(lookupStringUrl)
    print(response)
    data = json.load(response)
    print(data)


def sendEmail(datas):
    subject = datas['subject']
    message = datas['message']
    from_email = 'no-reply@gctportal.com'
    send_mail(subject, message, from_email, [datas['email']], fail_silently=False)
