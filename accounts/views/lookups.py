from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import render

from accounts.models import CustomUser, student, staff, department, staff_papers, staff_course_attended, batch, \
    active_batches
from curriculum.views.common_includes import get_current_batches

from common.API.departmentAPI import getAllDepartments


@login_required
@permission_required('accounts.can_view_profiles')
def lookup_student(request):
    if request.method == 'POST':
        if request.is_ajax():

            dictionary = {}

            got_regno = str(request.POST.get('regno'))

            print('got_regno=' + str(got_regno))

            # list = []

            if student.objects.filter(roll_no=got_regno).exists():
                if CustomUser.objects.filter(student__roll_no=got_regno).filter(is_approved=True):
                    list = student.objects.get(roll_no=got_regno)
                    # print(list)
                    dictionary['name'] = str(list)
                    dictionary['rollno'] = str(list.roll_no)
                    dictionary['phone'] = str(list.phone_number)
                    dictionary['email'] = str(list.user)
                    dictionary['gender'] = str(list.gender)
                    dictionary['dob'] = str(list.dob)
                    dictionary['department'] = str(list.department)
                    dictionary['batch'] = str(list.batch)
                    if active_batches.objects.filter(department=list.department).filter(
                        batch=list.batch).exists():
                        curent_semester_number = active_batches.objects.filter(department=list.department).get(
                            batch=list.batch).current_semester_number_of_this_batch
                    else:
                        curent_semester_number = 'Passed out'
                    dictionary['sem'] = curent_semester_number
                    dictionary['qualification'] = str(list.qualification)
                    dictionary['paddr'] = str(list.permanent_address)
                    dictionary['taddr'] = str(list.temporary_address)
                    dictionary['avatar'] = str(list.avatar)
                    print(list.avatar)
            else:
                pass

            print(dictionary)
            return JsonResponse(dictionary)

    return render(request, 'lookups/lookup_students.html', {'pic1': 'student-uploaded-images/mt.jpg'})


@login_required
@permission_required('accounts.can_view_profiles')
def lookup_staff(request):
    list_of_staffs = []
    paper_list = []
    list_of_papers = []
    course_list = []
    list_of_courses = []
    combine_list = []

    department_list = getAllDepartments()

    staff_instance = staff.objects.get(user=request.user)
    current_department = staff_instance.department

    if request.method == 'POST':
        if 'department_selected' in request.POST:
            got_department = request.POST.get('department_selected')
            current_department = department.objects.get(id=got_department)

    for each in staff.objects.filter(department=current_department).filter(designation='Principal'):
        if CustomUser.objects.filter(email=each.user).filter(is_approved=True):
            list_of_staffs.append(each)
            paper_list = []
            for every in staff_papers.objects.filter(staff__user=each.user):
                paper_list.append(every.paper_name)
            list_of_papers.append(paper_list)
            course_list = []
            for every in staff_course_attended.objects.filter(staff__user=each.user):
                course_list.append(every.course_name)
            list_of_courses.append(course_list)

    for each in staff.objects.filter(department=current_department).filter(designation='Professor'):
        if CustomUser.objects.filter(email=each.user).filter(is_approved=True):
            list_of_staffs.append(each)
            paper_list = []
            for every in staff_papers.objects.filter(staff__user=each.user):
                paper_list.append(every.paper_name)
            list_of_papers.append(paper_list)
            course_list = []
            for every in staff_course_attended.objects.filter(staff__user=each.user):
                course_list.append(every.course_name)
            list_of_courses.append(course_list)

    for each in staff.objects.filter(department=current_department).filter(designation='Associate Professor').order_by(
            'first_name').order_by('last_name'):
        if CustomUser.objects.filter(email=each.user).filter(is_approved=True):
            list_of_staffs.append(each)
            paper_list = []
            for every in staff_papers.objects.filter(staff__user=each.user):
                paper_list.append(every.paper_name)
            list_of_papers.append(paper_list)
            course_list = []
            for every in staff_course_attended.objects.filter(staff__user=each.user):
                course_list.append(every.course_name)
            list_of_courses.append(course_list)

    for each in staff.objects.filter(department=current_department).filter(designation='Assistant Professor').order_by(
            'first_name').order_by('last_name'):
        if CustomUser.objects.filter(email=each.user).filter(is_approved=True):
            list_of_staffs.append(each)
            paper_list = []
            for every in staff_papers.objects.filter(staff__user=each.user):
                paper_list.append(every.paper_name)
            list_of_papers.append(paper_list)
            course_list = []
            for every in staff_course_attended.objects.filter(staff__user=each.user):
                course_list.append(every.course_name)
            list_of_courses.append(course_list)

    for each in staff.objects.filter(department=current_department).filter(designation='Network Admin').order_by(
            'first_name').order_by('last_name'):
        if CustomUser.objects.filter(email=each.user).filter(is_approved=True):
            list_of_staffs.append(each)
            paper_list = []
            for every in staff_papers.objects.filter(staff__user=each.user):
                paper_list.append(every.paper_name)
            list_of_papers.append(paper_list)
            course_list = []
            for every in staff_course_attended.objects.filter(staff__user=each.user):
                course_list.append(every.course_name)
            list_of_courses.append(course_list)

    for (staf, paper, course) in zip(list_of_staffs, list_of_papers, list_of_courses):
        temp = {}
        list = []
        mem = {}
        mem['pk'] = staf.pk
        mem['name'] = staf
        mem['staff_id'] = staf.staff_id
        mem['email'] = staf.user
        mem['gender'] = staf.gender
        mem['dob'] = staf.dob
        mem['department'] = staf.department
        mem['designation'] = staf.designation
        mem['qualification'] = staf.qualification
        mem['degree'] = staf.degree
        mem['specialization'] = staf.specialization
        mem['temporary_address'] = staf.temporary_address
        mem['permanent_address'] = staf.permanent_address
        mem['phone_number'] = staf.phone_number
        mem['avatar'] = staf.avatar
        list.append(mem)
        temp['list'] = list
        temp['paper'] = paper
        temp['course'] = course
        combine_list.append(temp)

        # for each in combine_list:
        #   for every in each['list']:
        #      print(every['email'])

        # for each in combine_list:
        #   for every in each['paper']:
        #      print(every)

    return render(request, 'lookups/lookup_staffs.html',
                  {'combine_list': combine_list, 'department_list': department_list,
                   'selected_department': current_department})


@login_required
@permission_required('accounts.can_view_profiles')
def whole_students_lookup(request):
    obj = staff.objects.get(user__email=request.user)
    dept = getattr(obj, 'department')
    if request.method == 'POST':
        if request.is_ajax():
            if 'department' in request.POST:
                dictionary = {}

                chosen_department = str(request.POST.get('department'))

                # print('chosen_department='+str(chosen_department))

                list = []

                for each in get_current_batches(chosen_department):
                    temp = {}
                    temp['display_batch'] = str(each['display_text'])
                    temp['batch_obj'] = str(each['batch_obj'])
                    list.append(temp)

                dictionary['list'] = list

                return JsonResponse(dictionary)

            if 'year' in request.POST:
                dictionary = {}

                chosen_year = str(request.POST.get('year'))
                chosen_dept = str(request.POST.get('dept'))

                # print('chosen_year=' + str(chosen_year))

                list = []

                for each in student.objects.filter(department_id=chosen_dept).filter(batch=chosen_year).order_by(
                        'roll_no'):
                    if CustomUser.objects.filter(email=each.user).filter(is_approved=True):
                        temp = {}
                        temp['name'] = str(each)
                        temp['avatar'] = str(each.avatar)
                        temp['gender'] = str(each.gender)
                        # print(each.avatar)
                        temp['regno'] = str(each.roll_no)
                        list.append(temp)

                dictionary['list'] = list

                return JsonResponse(dictionary)

    dept_list = []
    for each in getAllDepartments():
        dept_list.append(each)

    return render(request, 'lookups/whole_lookup_students.html', {'dept_list': dept_list})


@login_required
@permission_required('accounts.can_view_profiles')
def alumni_students_lookup(request):
    obj = staff.objects.get(user__email=request.user)
    dept = getattr(obj, 'department')
    if request.method == 'POST':
        if request.is_ajax():
            if 'department' in request.POST:
                dictionary = {}

                chosen_department = str(request.POST.get('department'))

                # print('chosen_department='+str(chosen_department))

                list = []

                for each in batch.objects.all():
                    temp = {}
                    temp['display_batch'] = str(each) + ' - ' + str(each.programme)
                    temp['batch_obj'] = str(each.id)
                    list.append(temp)

                dictionary['list'] = list

                return JsonResponse(dictionary)

            if 'year' in request.POST:
                dictionary = {}

                chosen_year = str(request.POST.get('year'))
                chosen_dept = str(request.POST.get('dept'))

                # print('chosen_year=' + str(chosen_year))

                list = []

                for each in student.objects.filter(department_id=chosen_dept).filter(batch=chosen_year).order_by(
                        'roll_no'):
                    if CustomUser.objects.filter(email=each.user).filter(is_approved=True):
                        temp = {}
                        temp['name'] = str(each)
                        temp['avatar'] = str(each.avatar)
                        temp['gender'] = str(each.gender)
                        # print(each.avatar)
                        temp['regno'] = str(each.roll_no)
                        list.append(temp)

                dictionary['list'] = list

                return JsonResponse(dictionary)

    dept_list = []
    for each in getAllDepartments():
        dept_list.append(each)

    return render(request, 'lookups/alumni_lookup_students.html', {'dept_list': dept_list})
