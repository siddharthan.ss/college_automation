from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, redirect

from accounts.models import student, student_achievements


@login_required
@permission_required('accounts.can_add_achievements')
def add_achievements(request):
    student_instance = student.objects.get(user=request.user)
    # except:
    #     msg = {
    #         'page_title': 'Access Denied',
    #         'title': 'No Permission',
    #         'description': 'This page is only accessible to students'
    #     }
    #     return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    if request.method == 'POST':
        got_description = request.POST.get('description')
        got_semester = request.POST.get('semester')

        instance = student_achievements(student=student_instance, description=got_description,
                                        semester_number=got_semester)
        instance.save()

    list_of_achievements = student_achievements.objects.filter(student=student_instance).order_by('semester_number')

    return render(request, 'student_profile_addups/add_achievements.html',
                  {'list_of_achievements': list_of_achievements})


@login_required
@permission_required('accounts.can_add_achievements')
def view_achievements(request):
    student_instance = student.objects.get(user=request.user)
    if request.method == 'POST':
        list_of_id = []
        for each in student_achievements.objects.filter(student=student_instance).order_by('semester_number'):
            list_of_id.append(str(each.id))

        required_edit = ''

        for each in list_of_id:
            if str(request.POST.get(each)) == 'Edit':
                required_edit = int(each)
        print('required_edit=' + str(required_edit))

        request.session['editable_achievement_id'] = required_edit
        return redirect('../edit_achievements')

    list_of_achievements = student_achievements.objects.filter(student=student_instance).order_by('semester_number')

    return render(request, 'student_profile_addups/view_achievements.html',
                  {'list_of_achievements': list_of_achievements})


@login_required
@permission_required('accounts.can_add_achievements')
def edit_achievements(request):
    student_instance = student.objects.get(user=request.user)

    if request.method == 'POST':
        if 'update' in request.POST:
            got_description = request.POST.get('description')
            got_semester = request.POST.get('semester')
            required_edit_id = int(request.session['editable_achievement_id'])

            del request.session['editable_achievement_id']

            instance = student_achievements.objects.get(id=required_edit_id)
            instance.description = got_description
            instance.semester_number = got_semester
            instance.save()

        if 'delete' in request.POST:
            required_edit_id = int(request.session['editable_achievement_id'])

            del request.session['editable_achievement_id']

            student_achievements.objects.filter(id=required_edit_id).delete()

        return redirect('../view_achievements')

    if not 'editable_achievement_id' in request.session:
        return redirect('../view_achievements')

    required_edit = int(request.session['editable_achievement_id'])

    achievement = student_achievements.objects.get(id=required_edit)

    return render(request, 'student_profile_addups/edit_achievements.html', {'achievement': achievement})


"""
@login_required
@permission_required('accounts.can_view_profiles')
def view_papers(request):
    if request.method == 'POST':
        list_of_id = []
        for each in staff_papers.objects.filter(staff__user=request.user).order_by('-year_of_publish'):
            list_of_id.append(str(each.id))

        required_edit = ''

        for each in list_of_id:
            if str(request.POST.get(each)) == 'Edit':
                #print('got_paper_id='+str(each))
                required_edit = int(each)

        request.session['editable_paper_id'] = required_edit
        return redirect('../edit_my_papers')

    list_of_papers = []

    for each in staff_papers.objects.filter(staff__user=request.user).order_by('-year_of_publish'):
        list_of_papers.append(each)

    return render(request, 'profile_addups/view_papers.html', { 'list_of_papers': list_of_papers })


@login_required
@permission_required('accounts.can_view_profiles')
def edit_papers(request):
    if request.method == 'POST':
        if 'update' in request.POST:
            got_paper_name = str(request.POST.get('paper_name'))
            got_description = str(request.POST.get('description'))
            got_year = int(request.POST.get('year'))
            required_edit_id = int(request.session['editable_paper_id'])

            del request.session['editable_paper_id']

            staff_papers.objects.filter(id=required_edit_id).update(paper_name=got_paper_name, description=got_description, year_of_publish=got_year)

        if 'delete' in request.POST:
            required_edit_id = int(request.session['editable_paper_id'])

            del request.session['editable_paper_id']

            staff_papers.objects.filter(id=required_edit_id).delete()

        return redirect('../view_my_papers')

    else:
        required_edit = int(request.session['editable_paper_id'])

        paper = staff_papers.objects.get(id=required_edit)

        current_year = datetime.now().year

        return render(request, 'profile_addups/edit_papers.html', { 'paper': paper, 'max_year': current_year })
"""
