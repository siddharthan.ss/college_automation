from datetime import timedelta, datetime

from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import render

from accounts.models import issue_onduty, onduty_request, hourly_onduty_request, staff, student, department
from curriculum.models import semester, day
from curriculum.views.common_includes import get_active_semesters_tabs_of_department, get_list_of_students_of_semester

from common.API.departmentAPI import getAllDepartments


def get_staffs_under_department(department_id):
    current_staff_objects = staff.objects.filter(department_id=department_id).order_by('first_name').exclude(
        designation='Network Admin')

    list_of_staffs = []

    for entry in current_staff_objects:
        if entry.user.is_approved:
            temp = {}
            temp['id'] = entry.id
            temp['name'] = str(entry)
            list_of_staffs.append(temp)

    return list_of_staffs


@login_required
@permission_required('accounts.can_request_onduty')
def view_request_od(request):
    if request.method == 'POST':
        list_of_id = []
        for each in onduty_request.objects.filter(student__user=request.user, granted=False).order_by('start_date'):
            list_of_id.append(str(each.id))

        required_edit = ''

        for each in list_of_id:
            if str(request.POST.get(each)) == 'Delete':
                # print('got_roll_no=' + str(each))
                required_edit = int(each)

        if required_edit != '':
            onduty_request.objects.filter(id=required_edit).delete()
        else:
            required_edit = ''

            list_of_id = []

            for each in hourly_onduty_request.objects.filter(student__user=request.user, granted=False,
                                                             staff_created=False).order_by('date'):
                list_of_id.append(str(each.id))

            for each in list_of_id:
                if str(request.POST.get('hour.' + each)) == 'Delete':
                    print('got_roll_no=' + str(each))
                    required_edit = int(each)

            hourly_onduty_request.objects.filter(id=required_edit).delete()

    list_of_ods = []

    for each in onduty_request.objects.filter(student__user=request.user, granted=False, staff_created=False).order_by(
            'start_date'):
        list_of_ods.append(each)

    hourly_list_of_ods = hourly_onduty_request.objects.filter(student__user=request.user, granted=False).order_by(
        'date')

    return render(request, 'onduty/view_requested_ondutys.html', {
        'list_of_ods': list_of_ods,
        'hourly_list_of_ods': hourly_list_of_ods
    })


@login_required
@permission_required('accounts.can_request_onduty')
def my_granted_ods(request):
    list_of_ods = onduty_request.objects.filter(student__user=request.user, granted=True).order_by('start_date')
    hourly_list_of_ods = hourly_onduty_request.objects.filter(student__user=request.user, granted=True).order_by('date')

    return render(request, 'onduty/my_grantedods.html', {
        'list_of_ods': list_of_ods,
        'hourly_list_of_ods': hourly_list_of_ods
    })


@login_required
@permission_required('accounts.can_issue_onduty')
def view_od_requests(request):
    selected_od_type = 'full'
    if request.method == 'POST':
        if 'selected_tab' in request.POST:
            selected_od_type = request.POST.get('selected_tab')
        if 'grant_id' in request.POST:
            required_id = int(request.POST.get('grant_id'))
            onduty_instance = onduty_request.objects.get(id=required_id)
            start_date = onduty_instance.start_date
            end_date = onduty_instance.end_date
            print(start_date)
            print(end_date)
            reason = onduty_instance.message
            student_instance = onduty_instance.student
            staff_instance = onduty_instance.staff

            d1 = datetime.strptime(str(start_date), "%Y-%m-%d")
            d2 = datetime.strptime(str(end_date), "%Y-%m-%d")
            no_of_days_od_granted = abs((d2 - d1).days)

            test_d1 = d1
            success_message = None
            error = 'success'
            # try:
            counter = 1
            while counter <= no_of_days_od_granted + 1:
                if issue_onduty.objects.filter(student=student_instance, date=test_d1):
                    print('exists')
                print(test_d1)
                test_d1 = test_d1 + timedelta(1)
                counter = counter + 1

            print('no_of_days_od_granted=' + str(no_of_days_od_granted))
            counter = 1
            while counter <= no_of_days_od_granted + 1:
                if not day.objects.filter(day_name=d1.strftime("%A")):
                    d1 = d1 + timedelta(1)
                    counter = counter + 1
                    continue
                print(d1)
                try:
                    issue_onduty.objects.create(staff=staff_instance,
                                                student=student_instance,
                                                reason=reason,
                                                date=d1
                                                )
                except:
                    pass
                print('created')
                d1 = d1 + timedelta(1)
                counter = counter + 1

            od_request_inst = onduty_request.objects.get(id=required_id)
            od_request_inst.granted = True
            od_request_inst.save()
            success_message = 'OD granted for ' + str(student_instance) + ' from ' + \
                              str(start_date) + ' to ' + str(end_date)
            # except:
            #   error = 'duplicate'

            print(error)
            full_od_requests = onduty_request.objects.filter(staff__user=request.user, granted=False).order_by(
                'start_date')
            full_od_requests = len(full_od_requests)
            hour_od_requests = hourly_onduty_request.objects.filter(staff__user=request.user, granted=False).order_by(
                'date')
            hour_od_requests = len(hour_od_requests)
            list_of_ods = onduty_request.objects.filter(staff__user=request.user, granted=False).order_by('start_date')
            return render(request, 'onduty/view_odrequests.html', {
                'list_of_ods': list_of_ods,
                'error': error,
                'success_message': success_message,
                'selected_tab': selected_od_type,
                'full_od_requests': full_od_requests,
                'hour_od_requests': hour_od_requests
            })
        if 'refuse_id' in request.POST:
            required_id = request.POST.get('refuse_id')
            required_id = str(required_id).split('-')
            print('required_id=' + str(required_id))
            deleted = onduty_request.objects.get(id=required_id[0])
            onduty_request.objects.filter(id=required_id[0]).delete()
            full_od_requests = onduty_request.objects.filter(staff__user=request.user, granted=False).order_by(
                'start_date')
            full_od_requests = len(full_od_requests)
            hour_od_requests = hourly_onduty_request.objects.filter(staff__user=request.user, granted=False).order_by(
                'date')
            hour_od_requests = len(hour_od_requests)
            list_of_ods = onduty_request.objects.filter(staff__user=request.user, granted=False).order_by('start_date')
            return render(request, 'onduty/view_odrequests.html',
                          {
                              'list_of_ods': list_of_ods,
                              'deleted_user': deleted,
                              'selected_tab': selected_od_type,
                              'full_od_requests': full_od_requests,
                              'hour_od_requests': hour_od_requests
                          })
        if 'grant_id_hour' in request.POST:
            selected_od_type = 'hour'
            required_id = int(request.POST.get('grant_id_hour'))
            onduty_instance = hourly_onduty_request.objects.get(id=required_id)
            start_date = onduty_instance.date
            student_instance = onduty_instance.student

            error = 'success'
            if issue_onduty.objects.filter(student=student_instance, date=start_date).exists():
                error = 'duplicate'

            if error == 'success':
                onduty_instance.granted = True
                onduty_instance.save()
            success_message = 'OD granted for ' + str(student_instance) + ' with specified hours'

            full_od_requests = onduty_request.objects.filter(staff__user=request.user, granted=False).order_by(
                'start_date')
            full_od_requests = len(full_od_requests)
            hour_od_requests = hourly_onduty_request.objects.filter(staff__user=request.user, granted=False).order_by(
                'date')
            hour_od_requests = len(hour_od_requests)
            list_of_ods = hourly_onduty_request.objects.filter(staff__user=request.user, granted=False).order_by('date')
            return render(request, 'onduty/view_odrequests.html', {
                'list_of_ods': list_of_ods,
                'error': error,
                'success_message': success_message,
                'selected_tab': selected_od_type,
                'full_od_requests': full_od_requests,
                'hour_od_requests': hour_od_requests
            })
        if 'refuse_id_hour' in request.POST:
            selected_od_type = 'hour'
            required_id = request.POST.get('refuse_id_hour')
            required_id = str(required_id).split('-')
            print('required_id=' + str(required_id))
            deleted = hourly_onduty_request.objects.get(id=required_id[0])
            hourly_onduty_request.objects.filter(id=required_id[0]).delete()

            full_od_requests = onduty_request.objects.filter(staff__user=request.user, granted=False).order_by(
                'start_date')
            full_od_requests = len(full_od_requests)
            hour_od_requests = hourly_onduty_request.objects.filter(staff__user=request.user, granted=False).order_by(
                'date')
            hour_od_requests = len(hour_od_requests)
            list_of_ods = hourly_onduty_request.objects.filter(staff__user=request.user, granted=False).order_by('date')
            return render(request, 'onduty/view_odrequests.html',
                          {
                              'list_of_ods': list_of_ods,
                              'deleted_user': deleted,
                              'selected_tab': selected_od_type,
                              'full_od_requests': full_od_requests,
                              'hour_od_requests': hour_od_requests
                          })

    if selected_od_type == 'full':
        list_of_ods = onduty_request.objects.filter(staff__user=request.user, granted=False).order_by('start_date')
    else:
        list_of_ods = hourly_onduty_request.objects.filter(staff__user=request.user, granted=False).order_by('date')
        for each in list_of_ods:
            for hour in each.hour.all():
                print(hour.hour)
    full_od_requests = onduty_request.objects.filter(staff__user=request.user, granted=False).order_by('start_date')
    full_od_requests = len(full_od_requests)
    hour_od_requests = hourly_onduty_request.objects.filter(staff__user=request.user, granted=False).order_by('date')
    hour_od_requests = len(hour_od_requests)
    return render(request, 'onduty/view_odrequests.html', {
        'list_of_ods': list_of_ods,
        'selected_tab': selected_od_type,
        'full_od_requests': full_od_requests,
        'hour_od_requests': hour_od_requests
    })


@login_required
@permission_required('accounts.can_request_onduty')
def request_on_duty(request, selected_od_type='full'):
    student_instance = student.objects.get(user=request.user)
    student_regulation = student_instance.batch.regulation
    department_list = getAllDepartments()
    if request.method == 'POST':
        if 'selected_tab' in request.POST:
            selected_od_type = request.POST.get('selected_tab')
        elif 'end_date' in request.POST:
            error = ''
            selected_od_type = 'full'
            chosen_staff = str(request.POST.get('staff'))
            got_reason = str(request.POST.get('reason'))
            requested_start_date = got_sdate = str(request.POST.get('start_date'))
            requested_end_date = got_edate = str(request.POST.get('end_date'))
            staff_instance = staff.objects.get(id=chosen_staff)
            got_sdate = datetime.strptime(got_sdate, "%d/%m/%Y").strftime('%Y-%m-%d')
            got_edate = datetime.strptime(got_edate, "%d/%m/%Y").strftime('%Y-%m-%d')

            d1 = datetime.strptime(got_sdate, "%Y-%m-%d")
            d2 = datetime.strptime(got_edate, "%Y-%m-%d")
            days_counted = abs((d2 - d1).days)

            day_already_granted = False
            counter = 1
            while counter <= days_counted + 1:
                # print(d1.strftime('%Y-%m-%d'))
                if issue_onduty.objects.filter(student=student_instance, date=d1):
                    day_already_granted = True
                    break
                d1 = d1 + timedelta(1)
                counter = counter + 1

            d1 = datetime.strptime(got_sdate, "%Y-%m-%d")
            d2 = datetime.strptime(got_edate, "%Y-%m-%d")
            days_counted = abs((d2 - d1).days)
            if day.objects.filter(day_name=d1.strftime("%A")) and day.objects.filter(day_name=d2.strftime("%A")):
                list_of_days_od_requested = []
                counter = 1
                while counter <= days_counted + 1:
                    list_of_days_od_requested.append(d1.strftime('%Y-%m-%d'))
                    d1 = d1 + timedelta(1)
                    counter = counter + 1

                # print(list_of_days_od_requested)

                day_already_requested = False
                days_in_requested = 0
                if day_already_granted == False:
                    for each in onduty_request.objects.filter(student=student_instance).filter(granted=False):
                        start_day = each.start_date.strftime('%Y-%m-%d')
                        end_day = each.end_date.strftime('%Y-%m-%d')
                        d1 = datetime.strptime(start_day, "%Y-%m-%d")
                        d2 = datetime.strptime(end_day, "%Y-%m-%d")
                        days_counted = abs((d2 - d1).days)

                        counter = 1
                        while counter <= days_counted + 1:
                            # print(d1.strftime('%Y-%m-%d'))
                            if d1.strftime('%Y-%m-%d') in list_of_days_od_requested:
                                day_already_requested = True
                                break
                            d1 = d1 + timedelta(1)
                            counter = counter + 1
                        if day_already_requested:
                            break

                request_success = False

                if day_already_granted == False and day_already_requested == False:
                    onduty_request.objects.create(staff=staff_instance,
                                                  student=student_instance,
                                                  message=got_reason,
                                                  start_date=got_sdate,
                                                  end_date=got_edate,
                                                  granted=False
                                                  )
                    request_success = True

                reason = ''
                after_request = True
                if not request_success:
                    if day_already_granted:
                        reason = 'Your requested date overlaps with your GRANTED ODs'
                    elif day_already_requested:
                        reason = 'Your requested date overlaps with your REQUESTED ODs'

                else:
                    reason = 'OD requested from ' + str(requested_start_date) + ' to ' + str(requested_end_date)

                return render(request, 'onduty/request_on_duty.html',
                              {
                                  'selected_tab': selected_od_type,
                                  'department_list': department_list,
                                  'after_request': after_request,
                                  'request_success': request_success,
                                  'reason': reason
                              })
            else:
                error = 'holiday'
                return render(request, 'onduty/request_on_duty.html',
                              {
                                  'selected_tab': selected_od_type,
                                  'department_list': department_list,
                                  'error': error
                              })

        elif 'date' in request.POST:
            error = ''
            selected_od_type = 'hour'
            chosen_staff = str(request.POST.get('staff'))
            got_reason = str(request.POST.get('reason'))
            got_date = str(request.POST.get('date'))
            staff_instance = staff.objects.get(id=chosen_staff)
            print('got_date=' + str(got_date))
            got_date = datetime.strptime(got_date, "%d/%m/%Y").strftime('%Y-%m-%d')
            hour_list = request.POST.getlist('hour')

            if day.objects.filter(day_name=datetime.strptime(got_date, "%Y-%m-%d").strftime("%A")):
                request_success = False
                after_request = True
                day_already_granted = False
                day_already_requested = False
                day_already_granted_hourly = False
                if issue_onduty.objects.filter(student=student_instance, date=got_date):
                    day_already_granted = True
                list_of_days = []
                if not day_already_granted:
                    for od_request in onduty_request.objects.filter(student=student_instance):
                        d1 = datetime.strptime(str(od_request.start_date), "%Y-%m-%d")
                        d2 = datetime.strptime(str(od_request.end_date), "%Y-%m-%d")
                        days_counted = abs((d2 - d1).days)

                        counter = 1
                        while counter <= days_counted + 1:
                            list_of_days.append(d1.strftime('%Y-%m-%d'))
                            d1 = d1 + timedelta(1)
                            counter = counter + 1

                    if got_date in list_of_days:
                        day_already_requested = True
                if not day_already_requested:
                    for od_request in hourly_onduty_request.objects.filter(student=student_instance, date=got_date,
                                                                           granted=True):
                        for each in hour_list:
                            if each in od_request.hour.all():
                                day_already_granted_hourly = True
                                break

                request_success = False
                if day_already_granted:
                    reason = 'Your requested date overlaps with your GRANTED ODs'
                elif day_already_requested:
                    reason = 'Your requested date overlaps with your REQUESTED ODs'
                elif day_already_granted_hourly:
                    reason = 'Your requested date overlaps with your GRANTED HOURLY ODs'
                else:
                    request_success = True
                    reason = 'OD requested successfully for specified hours'
                    if hourly_onduty_request.objects.filter(student=student_instance, date=got_date).exists():
                        hourly_onduty_request.objects.filter(student=student_instance, date=got_date).delete()
                    od_instance = hourly_onduty_request(student=student_instance,
                                                        staff=staff_instance,
                                                        message=got_reason,
                                                        date=got_date,
                                                        granted=False)
                    od_instance.save()
                    for each in hour_list:
                        od_instance.hour.add(each)
                    od_instance.save()
                    return render(request, 'onduty/request_on_duty.html',
                                  {
                                      'selected_tab': selected_od_type,
                                      'student_regulation': student_regulation,
                                      'department_list': department_list,
                                      'after_request': after_request,
                                      'request_success': request_success,
                                      'reason': reason,
                                  })
            else:
                error = 'holiday'
                return render(request, 'onduty/request_on_duty.html',
                              {
                                  'selected_tab': selected_od_type,
                                  'student_regulation': student_regulation,
                                  'department_list': department_list,
                                  'error': error
                              })

        if request.is_ajax():
            if 'department' in request.POST:
                dictionary = {}
                chosen_department = str(request.POST.get('department'))
                list = get_staffs_under_department(chosen_department)
                dictionary['list'] = list
                return JsonResponse(dictionary)
            elif 'chosen_date' in request.POST:
                dictionary = {}
                chosen_date = request.POST.get('chosen_date')
                already_requested_hour = []
                chosen_date = datetime.strptime(chosen_date, "%d/%m/%Y").strftime('%Y-%m-%d')
                if hourly_onduty_request.objects.filter(student=student_instance, date=chosen_date, granted=False):
                    od_instance = hourly_onduty_request.objects.get(student=student_instance, date=chosen_date,
                                                                    granted=False)
                    for each in od_instance.hour.all():
                        already_requested_hour.append(each.hour)
                    dictionary['hours'] = len(already_requested_hour)
                    dictionary['list'] = already_requested_hour
                    print(already_requested_hour)
                return JsonResponse(dictionary)

    return render(request, 'onduty/request_on_duty.html',
                  {
                      'selected_tab': selected_od_type,
                      'department_list': department_list,
                      'student_regulation': student_regulation
                  })


@login_required
@permission_required('accounts.can_issue_onduty')
def view_issued_ods(request):
    selected_od_type = 'full'
    if request.method == 'POST':
        if 'selected_tab' in request.POST:
            selected_od_type = request.POST.get('selected_tab')
        if 'delete_id' in request.POST:
            required_id = request.POST.get('delete_id')
            print('required_id=' + str(required_id))
            deleted = onduty_request.objects.get(id=required_id)
            student_instance = deleted.student
            print(deleted.start_date)
            print(deleted.end_date)
            d1 = datetime.strptime(str(deleted.start_date), "%Y-%m-%d")
            d2 = datetime.strptime(str(deleted.end_date), "%Y-%m-%d")
            no_of_days_od_granted = abs((d2 - d1).days)

            counter = 1
            while counter <= no_of_days_od_granted + 1:
                if not day.objects.filter(day_name=d1.strftime("%A")):
                    d1 = d1 + timedelta(1)
                    counter = counter + 1
                    continue
                print(d1)
                issue_onduty.objects.filter(student=student_instance, date=d1).delete()
                print('created')
                d1 = d1 + timedelta(1)
                counter = counter + 1
            onduty_request.objects.filter(id=required_id).delete()
            list_of_ods = onduty_request.objects.filter(staff__user=request.user, granted=True).order_by('start_date',
                                                                                                         'student__roll_no')
            return render(request, 'onduty/view_ondutys.html',
                          {
                              'list_of_ods': list_of_ods,
                              'deleted_user': deleted,
                              'selected_tab': selected_od_type
                          })
        if 'delete_id_hour' in request.POST:
            selected_od_type = 'hour'
            required_id = request.POST.get('delete_id_hour')
            print('required_id=' + str(required_id))
            deleted = hourly_onduty_request.objects.get(id=required_id)
            hourly_onduty_request.objects.filter(id=required_id).delete()
            list_of_ods = hourly_onduty_request.objects.filter(staff__user=request.user, granted=True).order_by('date',
                                                                                                                'student__roll_no')
            return render(request, 'onduty/view_ondutys.html',
                          {
                              'list_of_ods': list_of_ods,
                              'deleted_user': deleted,
                              'selected_tab': selected_od_type
                          })

    if selected_od_type == 'full':
        list_of_ods = onduty_request.objects.filter(staff__user=request.user, granted=True).order_by('start_date',
                                                                                                     'student__roll_no')
    else:
        list_of_ods = hourly_onduty_request.objects.filter(staff__user=request.user, granted=True).order_by('date',
                                                                                                            'student__roll_no')

    return render(request, 'onduty/view_ondutys.html', {
        'list_of_ods': list_of_ods,
        'selected_tab': selected_od_type
    })


@login_required
@permission_required('accounts.can_issue_onduty')
def indiviual_od(request):
    error = None
    success_message = ''
    crash_message = ''
    if request.method == 'POST':
        if request.is_ajax():
            if 'check_roll' in request.POST:
                dictionary = {}
                got_roll = str(request.POST.get('roll_no'))
                try:
                    student_instance = student.objects.get(roll_no=got_roll)
                    dictionary['error'] = 'false'
                    if student_instance.user.is_approved:
                        list = []

                        for each in onduty_request.objects.filter(student=student_instance, granted=True).order_by(
                                'start_date'):
                            temp = {}
                            temp['staff_name'] = str(each.staff)
                            temp['student_roll'] = str(each.student.roll_no)
                            temp['student_name'] = str(each.student)
                            temp['reason'] = str(each.message)
                            temp['start_date'] = str(each.start_date)
                            temp['end_date'] = str(each.end_date)
                            list.append(temp)

                        dictionary['list'] = list
                        dictionary['student_name'] = str(student_instance) + ' (' + str(student_instance.roll_no) + ')'

                        list = []
                        for each in hourly_onduty_request.objects.filter(student=student_instance,
                                                                         granted=True).order_by(
                            'date'):
                            temp = {}
                            temp['staff_name'] = str(each.staff)
                            temp['student_roll'] = str(each.student.roll_no)
                            temp['student_name'] = str(each.student)
                            temp['reason'] = str(each.message)
                            temp['date'] = str(each.date)
                            hour_list = []
                            for hour in each.hour.all():
                                hour_list.append(hour.hour)
                            temp['hours'] = hour_list
                            list.append(temp)

                        dictionary['hourly_list'] = list
                    else:
                        dictionary['error'] = 'true'
                except:
                    dictionary['error'] = 'true'
                return JsonResponse(dictionary)
        else:
            got_roll = str(request.POST.get('roll_no'))
            got_reason = str(request.POST.get('reason'))
            got_date = str(request.POST.get('date'))
            got_end_date = str(request.POST.get('enddate'))
            got_date = datetime.strptime(got_date, "%d/%m/%Y").strftime('%Y-%m-%d')
            got_end_date = datetime.strptime(got_end_date, "%d/%m/%Y").strftime('%Y-%m-%d')

            staff_instance = staff.objects.get(user=request.user)
            student_instance = student.objects.get(roll_no=got_roll)

            d1 = datetime.strptime(got_date, "%Y-%m-%d")
            print(d1)
            d2 = datetime.strptime(got_end_date, "%Y-%m-%d")
            print(d2)
            no_of_days_od_granted = abs((d2 - d1).days)
            if day.objects.filter(day_name=d1.strftime("%A")) and day.objects.filter(day_name=d2.strftime("%A")):
                test_d1 = d1
                error = 'success'
                crash_in = False
                try:
                    counter = 1
                    while counter <= no_of_days_od_granted + 1:
                        # print(test_d1.strftime('%Y-%m-%d'))
                        if issue_onduty.objects.filter(student=student_instance, date=test_d1):
                            crash_in = 'fullod'
                            raise Exception
                        if hourly_onduty_request.objects.filter(student=student_instance, date=test_d1):
                            crash_in = 'hourlyod'
                            raise Exception
                        test_d1 = test_d1 + timedelta(1)
                        counter = counter + 1

                    counter = 1
                    while counter <= no_of_days_od_granted + 1:
                        if not day.objects.filter(day_name=d1.strftime("%A")):
                            d1 = d1 + timedelta(1)
                            counter = counter + 1
                            continue
                        issue_onduty.objects.create(staff=staff_instance,
                                                    student=student_instance,
                                                    reason=got_reason,
                                                    date=d1
                                                    )
                        d1 = d1 + timedelta(1)
                        counter = counter + 1
                    onduty_request.objects.create(staff=staff_instance,
                                                  student=student_instance,
                                                  message=got_reason,
                                                  start_date=got_date,
                                                  end_date=got_end_date,
                                                  granted=True,
                                                  staff_created=True
                                                  )
                except:
                    error = 'duplicate'
                    if crash_in == 'fullod':
                        crash_message = 'Already Provided FULL DAY OD for this student'
                    if crash_in == 'hourlyod':
                        crash_message = 'HOURLY OD provided for this student'

                success_message = 'OD granted for ' + str(student_instance) + ' from ' + \
                                  str(got_date) + ' to ' + str(got_end_date)
            else:
                error = 'holiday'

            return render(request, 'onduty/add_onduty.html', {
                'error': error,
                'success_message': success_message,
                'crash_message': crash_message
            })

    return render(request, 'onduty/add_onduty.html', {})


@login_required
@permission_required('accounts.can_issue_onduty')
def hourly_indiviual_od(request):
    staff_instance = staff.objects.get(user=request.user)
    error = None
    success_message = ''
    if request.method == 'POST':
        if request.is_ajax():
            if 'check_roll' in request.POST:
                dictionary = {}
                got_roll = str(request.POST.get('roll_no'))
                try:
                    student_instance = student.objects.get(roll_no=got_roll)
                    dictionary['error'] = 'false'
                    if student_instance.user.is_approved:
                        dictionary['regulation'] = student_instance.batch.regulation.start_year
                        list = []
                        for each in hourly_onduty_request.objects.filter(student=student_instance,
                                                                         granted=True).order_by('date'):
                            temp = {}
                            temp['staff_name'] = str(each.staff)
                            temp['student_roll'] = str(each.student.roll_no)
                            temp['student_name'] = str(each.student)
                            temp['reason'] = str(each.message)
                            temp['date'] = str(each.date)
                            hour_list = []
                            for hour in each.hour.all():
                                hour_list.append(hour.hour)
                            temp['hours'] = hour_list
                            list.append(temp)

                        dictionary['hourly_list'] = list
                        dictionary['student_name'] = str(student_instance) + ' (' + str(student_instance.roll_no) + ')'

                        list = []

                        for each in onduty_request.objects.filter(student=student_instance, granted=True).order_by(
                                'start_date'):
                            temp = {}
                            temp['staff_name'] = str(each.staff)
                            temp['student_roll'] = str(each.student.roll_no)
                            temp['student_name'] = str(each.student)
                            temp['reason'] = str(each.message)
                            temp['start_date'] = str(each.start_date)
                            temp['end_date'] = str(each.end_date)
                            list.append(temp)

                        dictionary['list'] = list
                    else:
                        dictionary['error'] = 'true'
                except:
                    dictionary['error'] = 'true'
                return JsonResponse(dictionary)
        else:
            got_roll = str(request.POST.get('roll_no'))
            got_reason = str(request.POST.get('reason'))
            got_date = str(request.POST.get('date'))
            got_date = datetime.strptime(got_date, "%d/%m/%Y").strftime('%Y-%m-%d')
            hour_list = request.POST.getlist('hour')
            try:
                student_instance = student.objects.get(roll_no=got_roll)
            except:
                return render(request, 'onduty/add_hourlyonduty.html',
                              {
                                  'no_matching_rollno': True
                              })

            if day.objects.filter(day_name=datetime.strptime(got_date, "%Y-%m-%d").strftime("%A")):
                grand_success = False
                after_request = True
                day_already_granted = False
                day_already_granted_hourly = False
                if issue_onduty.objects.filter(student=student_instance, date=got_date).exists():
                    day_already_granted = True
                if not day_already_granted:
                    if hourly_onduty_request.objects.filter(student=student_instance, date=got_date,
                                                            granted=True).exists():
                        day_already_granted_hourly = True

                print(day_already_granted)
                print(day_already_granted_hourly)
                if day_already_granted:
                    reason = 'Given date overlaps with your GRANTED FULL DAY ODs'
                elif day_already_granted_hourly:
                    reason = 'Given date overlaps with your GRANTED HOURLY ODs'
                else:
                    grand_success = True
                    reason = 'OD requested successfully for specified hours'
                    od_instance = hourly_onduty_request(student=student_instance,
                                                        staff=staff_instance,
                                                        message=got_reason,
                                                        date=got_date,
                                                        granted=True,
                                                        staff_created=True
                                                        )
                    od_instance.save()
                    for each in hour_list:
                        od_instance.hour.add(each)
                    od_instance.save()
                return render(request, 'onduty/add_hourlyonduty.html',
                              {
                                  'after_request': after_request,
                                  'grand_success': grand_success,
                                  'reason': reason
                              })
            else:
                error = 'holiday'
                return render(request, 'onduty/add_hourlyonduty.html',
                              {
                                  'error': error
                              })

    return render(request, 'onduty/add_hourlyonduty.html', {})


@login_required
@permission_required('accounts.can_issue_onduty')
def bulk_indiviual_od(request):
    error = None
    success_message = ''
    crash_message = ''
    if request.method == 'POST':
        if request.is_ajax():
            if 'department' in request.POST:
                dictionary = {}
                chosen_department = str(request.POST.get('department'))
                department_instance = department.objects.get(id=chosen_department)
                sem_list = []
                for each in get_active_semesters_tabs_of_department(department_instance):
                    temp = {}
                    temp['display_text'] = each['display_text']
                    temp['semester_instance_pk'] = each['semester_instance_pk']
                    sem_list.append(temp)
                dictionary['list'] = sem_list
                return JsonResponse(dictionary)

        if 'enddate' in request.POST:
            got_department = str(request.POST.get('department_id'))
            got_semester = str(request.POST.get('batch'))
            got_reason = str(request.POST.get('reason'))
            got_date = str(request.POST.get('date'))
            got_end_date = str(request.POST.get('enddate'))
            got_date = datetime.strptime(got_date, "%d/%m/%Y").strftime('%Y-%m-%d')
            got_end_date = datetime.strptime(got_end_date, "%d/%m/%Y").strftime('%Y-%m-%d')

            d1 = datetime.strptime(got_date, "%Y-%m-%d")
            d2 = datetime.strptime(got_end_date, "%Y-%m-%d")
            no_of_days_od_granted = abs((d2 - d1).days)
            if day.objects.filter(day_name=d1.strftime("%A")) and day.objects.filter(day_name=d2.strftime("%A")):
                semester_instance = semester.objects.get(id=got_semester)
                department_instance = department.objects.get(id=got_department)

                list_of_students = get_list_of_students_of_semester(semester_instance)

                return render(request, 'onduty/grant_students.html', {
                    'list_of_students': list_of_students,
                    'reason': got_reason,
                    'no_of_days_od_granted': int(no_of_days_od_granted),
                    'semester': semester_instance,
                    'department': department_instance,
                    'start_date': got_date,
                    'end_date': got_end_date
                })
            else:
                department_list = department.objects.filter(is_core=True)

                return render(request, 'onduty/bulkod.html', {
                    'error': 'holiday',
                    'department_list': department_list
                })

        if 'semester' in request.POST:
            got_semester = str(request.POST.get('semester'))
            got_reason = str(request.POST.get('reason'))
            got_start_date = str(request.POST.get('start_date'))
            got_end_date = str(request.POST.get('end_date'))
            # got_start_date = datetime.strptime(got_start_date, "%d/%m/%Y").strftime('%Y-%m-%d')
            # got_end_date = datetime.strptime(got_end_date, "%d/%m/%Y").strftime('%Y-%m-%d')

            semester_instance = semester.objects.get(id=got_semester)

            list_of_students = get_list_of_students_of_semester(semester_instance)

            total_students = len(list_of_students)

            no_of_od_granted_students = 0
            od_grant_requests = 0

            staff_instance = staff.objects.get(user=request.user)

            for stud in list_of_students:
                od_granted_student = str(request.POST.get(stud['registration_number']))

                if od_granted_student == '1':
                    od_grant_requests = od_grant_requests + 1
                    student_instance = student.objects.get(roll_no=stud['registration_number'])

                    d1 = datetime.strptime(got_start_date, "%Y-%m-%d")
                    d2 = datetime.strptime(got_end_date, "%Y-%m-%d")
                    no_of_days_od_granted = abs((d2 - d1).days)
                    if day.objects.filter(day_name=d1.strftime("%A")) and day.objects.filter(
                            day_name=d2.strftime("%A")):
                        test_d1 = d1
                        error = 'success'
                        crash_in = False
                        try:
                            counter = 1
                            while counter <= no_of_days_od_granted + 1:
                                # print(test_d1.strftime('%Y-%m-%d'))
                                if issue_onduty.objects.filter(student=student_instance, date=test_d1):
                                    crash_in = 'fullod'
                                    raise Exception
                                if hourly_onduty_request.objects.filter(student=student_instance, date=test_d1):
                                    crash_in = 'hourlyod'
                                    raise Exception
                                test_d1 = test_d1 + timedelta(1)
                                counter = counter + 1

                            counter = 1
                            while counter <= no_of_days_od_granted + 1:
                                if not day.objects.filter(day_name=d1.strftime("%A")):
                                    d1 = d1 + timedelta(1)
                                    counter = counter + 1
                                    continue
                                issue_onduty.objects.create(staff=staff_instance,
                                                            student=student_instance,
                                                            reason=got_reason,
                                                            date=d1
                                                            )
                                d1 = d1 + timedelta(1)
                                counter = counter + 1
                            onduty_request.objects.create(staff=staff_instance,
                                                          student=student_instance,
                                                          message=got_reason,
                                                          start_date=got_start_date,
                                                          end_date=got_end_date,
                                                          granted=True,
                                                          staff_created=True
                                                          )
                            no_of_od_granted_students = no_of_od_granted_students + 1
                        except:
                            error = 'duplicate'
                            if crash_in == 'fullod':
                                crash_message = 'Already Provided FULL DAY OD for some student'
                            if crash_in == 'hourlyod':
                                crash_message = 'HOURLY OD provided for this student'

                        success_message = 'OD granted for students from ' + \
                                          str(got_start_date) + ' to ' + str(got_end_date)
                    else:
                        error = 'holiday'

            if od_grant_requests == no_of_od_granted_students:
                bulk_color = 'success'
                bulk_message_report = 'OD granted for ' + str(od_grant_requests) + ' students'
            else:
                bulk_color = 'danger'
                bulk_message_report = 'OD already provided for ' + str(
                    od_grant_requests - no_of_od_granted_students) + ' students'

            department_list = department.objects.filter(is_core=True)

            return render(request, 'onduty/bulkod.html', {
                'list_of_students': list_of_students,
                'semester': semester_instance,
                'start_date': got_start_date,
                'end_date': got_end_date,

                'bulk_message_report': bulk_message_report,
                'bulk_color': bulk_color,
                'error': error,
                'success_message': success_message,
                'crash_message': crash_message,
                'department_list': department_list
            })

    department_list = department.objects.filter(is_core=True)

    return render(request, 'onduty/bulkod.html', {
        'department_list': department_list
    })


@login_required
@permission_required('accounts.can_issue_onduty')
def bulk_hourly_od(request):
    error = None
    success_message = ''
    crash_message = ''
    if request.method == 'POST':
        if 'hour' in request.POST:
            got_department = str(request.POST.get('department_id'))
            got_semester = str(request.POST.get('batch'))
            got_reason = str(request.POST.get('reason'))
            got_date = str(request.POST.get('date'))
            got_date = datetime.strptime(got_date, "%d/%m/%Y").strftime('%Y-%m-%d')
            hour_list = request.POST.getlist('hour')

            hours_string = ''

            counter = 0
            while (counter < len(hour_list) - 1):
                hours_string += hour_list[counter] + ', '
                counter = counter + 1

            hours_string += hour_list[len(hour_list) - 1]

            if day.objects.filter(day_name=datetime.strptime(got_date, "%Y-%m-%d").strftime("%A")):
                semester_instance = semester.objects.get(id=got_semester)
                department_instance = department.objects.get(id=got_department)

                list_of_students = get_list_of_students_of_semester(semester_instance)

                return render(request, 'onduty/grant_hourly_od_students.html', {
                    'list_of_students': list_of_students,
                    'reason': got_reason,
                    'hour_list': hour_list,
                    'hours_string': hours_string,
                    'semester': semester_instance,
                    'department': department_instance,
                    'date': got_date,
                })
            else:
                department_list = department.objects.filter(is_core=True)

                return render(request, 'onduty/bulk_hourly_od.html', {
                    'error': 'holiday',
                    'department_list': department_list
                })

        if 'semester' in request.POST:
            got_semester = str(request.POST.get('semester'))
            got_reason = str(request.POST.get('reason'))
            got_date = str(request.POST.get('date'))
            got_hours = str(request.POST.get('hidden_hour_list'))

            hour_list = got_hours.split(', ')

            semester_instance = semester.objects.get(id=got_semester)

            list_of_students = get_list_of_students_of_semester(semester_instance)

            total_students = len(list_of_students)

            no_of_od_granted_students = 0
            od_grant_requests = 0

            staff_instance = staff.objects.get(user=request.user)

            for stud in list_of_students:
                od_granted_student = str(request.POST.get(stud['registration_number']))

                if od_granted_student == '1':
                    od_grant_requests = od_grant_requests + 1
                    student_instance = student.objects.get(roll_no=stud['registration_number'])

                    d1 = datetime.strptime(got_date, "%Y-%m-%d")
                    print(d1)
                    if day.objects.filter(day_name=datetime.strptime(got_date, "%Y-%m-%d").strftime("%A")):
                        grand_success = False
                        after_request = True
                        day_already_granted = False
                        day_already_granted_hourly = False
                        if issue_onduty.objects.filter(student=student_instance, date=got_date).exists():
                            day_already_granted = True
                        if not day_already_granted:
                            if hourly_onduty_request.objects.filter(student=student_instance, date=got_date,
                                                                    granted=True).exists():
                                day_already_granted_hourly = True

                        # print(day_already_granted)
                        # print(day_already_granted_hourly)
                        if day_already_granted:
                            reason = 'Given date overlaps with your GRANTED FULL DAY ODs'
                        elif day_already_granted_hourly:
                            reason = 'Given date overlaps with your GRANTED HOURLY ODs'
                        else:
                            grand_success = True
                            reason = 'OD requested successfully for specified hours'
                            od_instance = hourly_onduty_request(student=student_instance,
                                                                staff=staff_instance,
                                                                message=got_reason,
                                                                date=got_date,
                                                                granted=True,
                                                                staff_created=True
                                                                )
                            od_instance.save()
                            for each in hour_list:
                                od_instance.hour.add(each)
                            od_instance.save()

                            no_of_od_granted_students = no_of_od_granted_students + 1

                    else:
                        error = 'holiday'

            if od_grant_requests == no_of_od_granted_students:
                bulk_color = 'success'
                bulk_message_report = 'OD granted for ' + str(od_grant_requests) + ' students'
            else:
                bulk_color = 'danger'
                bulk_message_report = 'OD already provided for ' + str(
                    od_grant_requests - no_of_od_granted_students) + ' students'

            department_list = department.objects.filter(is_core=True)

            return render(request, 'onduty/bulk_hourly_od.html', {
                'list_of_students': list_of_students,
                'semester': semester_instance,
                'start_date': got_date,

                'bulk_message_report': bulk_message_report,
                'bulk_color': bulk_color,
                'error': error,
                'success_message': success_message,
                'crash_message': crash_message,
                'department_list': department_list
            })

    department_list = department.objects.filter(is_core=True)

    return render(request, 'onduty/bulk_hourly_od.html', {
        'department_list': department_list
    })
