from datetime import datetime
from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import render, redirect

from accounts.models import staff, staff_papers, staff_course_attended


@login_required
@permission_required('accounts.can_view_profiles')
def add_papers(request):
    if request.method == 'POST':
        if request.is_ajax():

            dictionary = {}

            got_paper = str(request.POST.get('paper_name'))
            got_description = str(request.POST.get('description'))
            got_date = int(request.POST.get('date'))

            # print('got_paper='+str(got_paper))
            # print('got_description='+str(got_description))
            # print('got_date='+str(got_date))

            staff_instance = staff.objects.get(user=request.user)

            if (got_description == ''):
                staff_papers.objects.create(staff=staff_instance,
                                            paper_name=got_paper.upper(),
                                            year_of_publish=got_date
                                            )
            else:
                staff_papers.objects.create(staff=staff_instance,
                                            paper_name=got_paper.upper(),
                                            description=got_description.title(),
                                            year_of_publish=got_date
                                            )

            list = []
            for each in staff_papers.objects.filter(staff__user=request.user).order_by('-year_of_publish'):
                temp = {}
                temp['paper_name'] = str(each.paper_name)
                temp['description'] = str(each.description)
                temp['year'] = str(each.year_of_publish)
                list.append(temp)

            dictionary['list'] = list

            return JsonResponse(dictionary)

    list_of_papers = []

    for each in staff_papers.objects.filter(staff__user=request.user).order_by('-year_of_publish'):
        list_of_papers.append(each)

    return render(request, 'profile_addups/add_papers.html', {'list_of_papers': list_of_papers})


@login_required
@permission_required('accounts.can_view_profiles')
def view_papers(request):
    if request.method == 'POST':
        list_of_id = []
        for each in staff_papers.objects.filter(staff__user=request.user).order_by('-year_of_publish'):
            list_of_id.append(str(each.id))

        required_edit = ''

        for each in list_of_id:
            if str(request.POST.get(each)) == 'Edit':
                # print('got_paper_id='+str(each))
                required_edit = int(each)

        request.session['editable_paper_id'] = required_edit
        return redirect('../edit_my_papers')

    list_of_papers = []

    for each in staff_papers.objects.filter(staff__user=request.user).order_by('-year_of_publish'):
        list_of_papers.append(each)

    return render(request, 'profile_addups/view_papers.html', {'list_of_papers': list_of_papers})


@login_required
@permission_required('accounts.can_view_profiles')
def edit_papers(request):
    if request.method == 'POST':
        if 'update' in request.POST:
            got_paper_name = str(request.POST.get('paper_name'))
            got_description = str(request.POST.get('description'))
            got_year = int(request.POST.get('year'))
            required_edit_id = int(request.session['editable_paper_id'])

            del request.session['editable_paper_id']

            staff_papers.objects.filter(id=required_edit_id).update(paper_name=got_paper_name,
                                                                    description=got_description,
                                                                    year_of_publish=got_year)

        if 'delete' in request.POST:
            required_edit_id = int(request.session['editable_paper_id'])

            del request.session['editable_paper_id']

            staff_papers.objects.filter(id=required_edit_id).delete()

        return redirect('../view_my_papers')

    else:
        required_edit = int(request.session['editable_paper_id'])

        paper = staff_papers.objects.get(id=required_edit)

        current_year = datetime.now().year

        return render(request, 'profile_addups/edit_papers.html', {'paper': paper, 'max_year': current_year})


@login_required
@permission_required('accounts.can_view_profiles')
def add_courses_attended(request):
    if request.method == 'POST':
        if request.is_ajax():
            dictionary = {}

            got_paper = str(request.POST.get('paper_name'))
            got_description = str(request.POST.get('description'))
            got_date = str(request.POST.get('date'))
            got_end_date = str(request.POST.get('end_date'))

            got_date = datetime.strptime(got_date, "%d/%m/%Y").strftime('%Y-%m-%d')
            got_end_date = datetime.strptime(got_end_date, "%d/%m/%Y").strftime('%Y-%m-%d')

            staff_instance = staff.objects.get(user=request.user)

            staff_course_attended.objects.create(staff=staff_instance,
                                                 course_name=got_paper.upper(),
                                                 description=got_description.title(),
                                                 date=got_date,
                                                 end_date=got_end_date
                                                 )

            list = []
            for each in staff_course_attended.objects.filter(staff__user=request.user).order_by('-date'):
                temp = {}
                temp['course_name'] = str(each.course_name)
                temp['description'] = str(each.description)
                if each.end_date:
                    if each.date == each.end_date:
                        temp['date'] = str(each.date.strftime('%d/%m/%Y'))
                    else:
                        temp['date'] = str(each.date.strftime('%d/%m/%Y')) + '  -  ' + str(each.end_date.strftime('%d/%m/%Y'))
                else:
                    temp['date'] = str(each.date.strftime('%d/%m/%Y'))
                list.append(temp)

            dictionary['list'] = list

            return JsonResponse(dictionary)

    list_of_courses = []

    for each in staff_course_attended.objects.filter(staff__user=request.user).order_by('-date'):
        temp = {}
        temp['course_name'] = str(each.course_name)
        temp['description'] = str(each.description)
        if each.end_date:
            if each.date == each.end_date:
                temp['date'] = str(each.date.strftime('%d/%m/%Y'))
            else:
                temp['date'] = str(each.date.strftime('%d/%m/%Y')) + ' - ' + str(each.end_date.strftime('%d/%m/%Y'))
        else:
            temp['date'] = str(each.date.strftime('%d/%m/%Y'))
        list_of_courses.append(temp)

    return render(request, 'profile_addups/add_courses.html', {'list_of_courses': list_of_courses})


@login_required
@permission_required('accounts.can_view_profiles')
def view_courses_attended(request):
    if request.method == 'POST':
        list_of_id = []
        for each in staff_course_attended.objects.filter(staff__user=request.user).order_by('-date'):
            list_of_id.append(str(each.id))

        pprint(list_of_id)

        required_edit = ''

        for each in list_of_id:
            if str(request.POST.get(each)) == 'Edit':
                print('got_paper_id=' + str(each))
                required_edit = int(each)

        request.session['editable_paper_id'] = required_edit
        return redirect('../edit_my_courses')

    list_of_courses = []

    for each in staff_course_attended.objects.filter(staff__user=request.user).order_by('-date'):
        temp = {}
        temp['id'] = str(each.id)
        temp['course_name'] = str(each.course_name)
        temp['description'] = str(each.description)
        if each.end_date:
            if each.date == each.end_date:
                temp['date'] = str(each.date.strftime('%d/%m/%Y'))
            else:
                temp['date'] = str(each.date.strftime('%d/%m/%Y')) + ' - ' + str(each.end_date.strftime('%d/%m/%Y'))
        else:
            temp['date'] = str(each.date.strftime('%d/%m/%Y'))
        list_of_courses.append(temp)

    return render(request, 'profile_addups/view_courses.html', {'list_of_courses': list_of_courses})


@login_required
@permission_required('accounts.can_view_profiles')
def edit_courses_attended(request):
    if request.method == 'POST':
        if 'year_end' in request.POST:
            print('done')
            got_paper_name = str(request.POST.get('paper_name'))
            got_description = str(request.POST.get('description'))
            got_year = str(request.POST.get('year'))
            got_end_year = str(request.POST.get('year_end'))
            pprint(got_end_year)
            required_edit_id = int(request.session['editable_paper_id'])

            got_date = datetime.strptime(got_year, "%d/%m/%Y").strftime('%Y-%m-%d')
            got_end_date = datetime.strptime(got_end_year, "%d/%m/%Y").strftime('%Y-%m-%d')
            del request.session['editable_paper_id']

            staff_course_attended.objects.filter(id=required_edit_id).update(course_name=got_paper_name.upper(),
                                                                             description=got_description.title(),
                                                                             date=got_date, end_date=got_end_date)

        if 'delete' in request.POST:
            required_edit_id = int(request.session['editable_paper_id'])

            del request.session['editable_paper_id']

            staff_course_attended.objects.filter(id=required_edit_id).delete()

        return redirect('../view_my_courses')

    else:
        required_edit = int(request.session['editable_paper_id'])

        instance = staff_course_attended.objects.get(id=required_edit)
        course_name = instance.course_name
        description = instance.description
        datestr = datetime.strptime(str(instance.date), '%Y-%m-%d').strftime('%d/%m/%Y')
        dateendstr = datetime.strptime(str(instance.date), '%Y-%m-%d').strftime('%d/%m/%Y')

        return render(request, 'profile_addups/edit_courses.html', {'course_name': course_name,
                                                                    'description': description,
                                                                    'datestr': datestr,
                                                                    'dateendstr': dateendstr
                                                                    })
