from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import render

from accounts.models import CustomUser, student, staff
from curriculum.models import semester_migration


def can_approve(user):
    if user.has_perm('accounts.can_approve_staffs') or \
            user.has_perm('accounts.can_approve_student') or \
            user.has_perm('accounts.can_allot_hod'):
        return True


@login_required()
@user_passes_test(can_approve)
def approve(request):
    no_of_users_approved = '0'
    no_of_users_deleted = '0'
    if request.method == 'POST':
        # from_email = settings.EMAIL_HOST_USER
        if 'approved' in request.POST:
            # subject = 'Congrats! Your account has been approved'
            # message = 'Welcome to GCT Portal. \nThank You for your patience until the approval. \nGo and Enjoy. Click here http://gctportal.com'
            userlist = request.POST.getlist('approve')
            no_of_users_approved = str(len(userlist))
            for user in userlist:
                CustomUser.objects.filter(email=user).update(is_approved=True)
                # send_mail(subject, message, from_email, userlist, fail_silently=False)

        elif 'delete' in request.POST:
            # subject = 'Oh Snap! Your account has been deleted'
            # message = 'Your account has been deleted by your concerned faculty advisor. \nTo know more meet the respective staff'
            userlist = request.POST.getlist('approve')
            no_of_users_deleted = str(len(userlist))
            for user in userlist:
                CustomUser.objects.filter(email=user).delete()
                # send_mail(subject, message, from_email, userlist, fail_silently=False)

    current_user = staff.objects.get(user=request.user)
    department_acronym = current_user.department.acronym
    is_cse_department = False
    if department_acronym == 'CSE':
        is_cse_department = True
    custom_list_of_hods = []
    custom_list_of_staffs_associates = []
    custom_list_of_staffs_assistants = []
    custom_list_of_staffs_network_admins = []
    custom_list_of_students = []
    list_of_hods = []
    list_of_staffs = []
    list_of_students = []
    if request.user.groups.filter(name='principal').exists():
        custom_list_of_hods = CustomUser.objects.filter(
            staff__designation='Professor',
            is_approved=False,
            is_verified=True,
            has_filled_data=True
        )
    if request.user.groups.filter(name='hod').exists():
        custom_list_of_staffs_associates = CustomUser.objects.filter(
            staff__department=current_user.department,
            staff__designation='Associate Professor',
            is_approved=False,
            is_verified=True,
            has_filled_data=True
        )
        custom_list_of_staffs_assistants = CustomUser.objects.filter(
            staff__department=current_user.department,
            staff__designation='Assistant Professor',
            is_approved=False,
            is_verified=True,
            has_filled_data=True
        )
        if is_cse_department:
            custom_list_of_staffs_network_admins = CustomUser.objects.filter(
                staff__designation='Network Admin',
                is_approved=False,
                is_verified=True,
                has_filled_data=True
            )
    if request.user.groups.filter(name='faculty_advisor').exists():
        list_of_batches = []
        custom_list_of_students = CustomUser.objects.none()
        for active_sem in semester_migration.objects.all():
            faculty_advisors = active_sem.semester.faculty_advisor.all()
            if current_user in faculty_advisors:
                list_of_batches.append(active_sem.semester.batch)
                custom_list_of_students |= CustomUser.objects.filter(
                    student__department=active_sem.department,
                    student__batch__in=list_of_batches,
                    is_approved=False,
                    is_verified=True,
                    has_filled_data=True
                )
    list3 = []

    list0 = staff.objects.filter(user__in=custom_list_of_hods).order_by('staff_id')

    list1 = staff.objects.filter(user__in=custom_list_of_staffs_associates).order_by('staff_id')
    list2 = staff.objects.filter(user__in=custom_list_of_staffs_assistants).order_by('staff_id')
    if is_cse_department:
        list3 = staff.objects.filter(user__in=custom_list_of_staffs_network_admins).order_by('staff_id')

    for each in list0:
        list_of_hods.append(each)
    for each in list1:
        list_of_staffs.append(each)
    for each in list2:
        list_of_staffs.append(each)
    if is_cse_department:
        for each in list3:
            list_of_staffs.append(each)

    list4 = student.objects.filter(user__in=custom_list_of_students).order_by('current_semester', 'roll_no')
    for each in list4:
        list_of_students.append(each)

    no_of_users_to_approve = len(list_of_hods) + len(list_of_staffs) + len(list_of_students)
    avail_for_selection = False
    if no_of_users_to_approve != 0:
        avail_for_selection = True

    return render(request, 'accounts/approval.html', {
        'list_of_hods': list_of_hods,
        'list_of_staffs': list_of_staffs,
        'list_of_students': list_of_students,
        'avail_for_selection': avail_for_selection,
        'action': 'Approve',
        'no_of_users_approved': no_of_users_approved,
        'no_of_users_deleted': no_of_users_deleted
    })


@login_required
@user_passes_test(can_approve)
def deapprove(request):
    no_of_users_deapproved = '0'
    if request.method == 'POST':
        # from_email = settings.EMAIL_HOST_USER
        # subject = 'Oh No! Your account has been unapproved'
        # message = 'Your account has been put on hold by your concerned faculty. \nMeet the respective staff to know more'
        if 'deapprove' in request.POST:
            userlist = request.POST.getlist('approve')
            no_of_users_deapproved = str(len(userlist))
            for user in userlist:
                CustomUser.objects.filter(email=user).update(is_approved=False)
                # send_mail(subject, message, from_email, userlist, fail_silently=False)

    current_user = staff.objects.get(user=request.user)
    department_acronym = current_user.department.acronym
    is_cse_department = False
    if department_acronym == 'CSE':
        is_cse_department = True
    custom_list_of_hods = []
    custom_list_of_staffs_associates = []
    custom_list_of_staffs_assistants = []
    custom_list_of_staffs_network_admins = []
    custom_list_of_students = []
    list_of_hods = []
    list_of_staffs = []
    list_of_students = []
    if request.user.groups.filter(name='principal').exists():
        custom_list_of_hods = CustomUser.objects.filter(
            staff__designation='Professor',
            is_approved=True,
            is_verified=True,
            # has_filled_data=True
        )
    if request.user.groups.filter(name='hod').exists():
        custom_list_of_staffs_associates = CustomUser.objects.filter(
            staff__department=current_user.department,
            staff__designation='Associate Professor',
            is_approved=True,
            is_verified=True,
            # has_filled_data=True
        )
        custom_list_of_staffs_assistants = CustomUser.objects.filter(
            staff__department=current_user.department,
            staff__designation='Assistant Professor',
            is_approved=True,
            is_verified=True,
            # has_filled_data=True
        )
        if is_cse_department:
            custom_list_of_staffs_network_admins = CustomUser.objects.filter(
                staff__designation='Network Admin',
                is_approved=True,
                is_verified=True,
                # has_filled_data=True
            )
    if request.user.groups.filter(name='faculty_advisor').exists():
        list_of_batches = []
        custom_list_of_students = CustomUser.objects.none()
        for active_sem in semester_migration.objects.all():
            faculty_advisors = active_sem.semester.faculty_advisor.all()
            if current_user in faculty_advisors:
                list_of_batches.append(active_sem.semester.batch)
                custom_list_of_students |= CustomUser.objects.filter(
                    student__department=active_sem.department,
                    student__batch__in=list_of_batches,
                    is_approved=True,
                    is_verified=True,
                    # has_filled_data=True
                )
    list3 = []

    list0 = staff.objects.filter(user__in=custom_list_of_hods).order_by('staff_id')

    list1 = staff.objects.filter(user__in=custom_list_of_staffs_associates).order_by('staff_id')
    list2 = staff.objects.filter(user__in=custom_list_of_staffs_assistants).order_by('staff_id')
    if is_cse_department:
        list3 = staff.objects.filter(user__in=custom_list_of_staffs_network_admins).order_by('staff_id')

    for each in list0:
        list_of_hods.append(each)
    for each in list1:
        list_of_staffs.append(each)
    for each in list2:
        list_of_staffs.append(each)
    if is_cse_department:
        for each in list3:
            list_of_staffs.append(each)

    list4 = student.objects.filter(user__in=custom_list_of_students).order_by('roll_no')
    for each in list4:
        list_of_students.append(each)

    no_of_users_to_deapprove = len(list_of_hods) + len(list_of_staffs) + len(list_of_students)
    avail_for_selection = False
    if no_of_users_to_deapprove != 0:
        avail_for_selection = True

    return render(request, 'accounts/approval.html', {
        'list_of_hods': list_of_hods,
        'list_of_staffs': list_of_staffs,
        'list_of_students': list_of_students,
        'avail_for_selection': avail_for_selection,
        'action': 'Deapprove',
        'no_of_users_deapproved': no_of_users_deapproved
    })


"""
@login_required
@user_passes_test(can_approve)
def delete_unverified_users(request):
    no_of_users_deleted_unverified = '0'
    if request.method == 'POST':
        if 'delete' in request.POST:
            userlist = request.POST.getlist('approve')
            no_of_users_deleted_unverified = str(len(userlist))
            for user in userlist:
                CustomUser.objects.filter(email=user).delete()

    current_user = staff.objects.get(user=request.user)
    custom_list_of_hods = []
    custom_list_of_staffs_associates = []
    custom_list_of_staffs_assistants = []
    custom_list_of_staffs_network_admins = []
    custom_list_of_students = []
    list_of_hods = []
    list_of_staffs = []
    list_of_students = []
    if request.user.groups.filter(name='principal').exists():
        custom_list_of_hods = CustomUser.objects.filter(
            staff__designation='Professor',
            is_verified=False
        )
    if request.user.groups.filter(name='hod').exists():
        custom_list_of_staffs_associates = CustomUser.objects.filter(
            staff__department=current_user.department,
            staff__designation='Associate Professor',
            is_verified=False
        )
        custom_list_of_staffs_assistants = CustomUser.objects.filter(
            staff__department=current_user.department,
            staff__designation='Assistant Professor',
            is_verified=False
        )
        custom_list_of_staffs_network_admins = CustomUser.objects.filter(
            staff__department=current_user.department,
            staff__designation='Network Admin',
            is_verified=False
        )
    if request.user.groups.filter(name='faculty_advisor').exists():
        list_of_batches = []
        for each in semester.objects.filter(faculty_advisor=current_user):
            list_of_batches.append(each.batch)
        custom_list_of_students = CustomUser.objects.filter(
            student__department=current_user.department,
            student__batch__in=list_of_batches,
            is_verified=False
        )

    list_of_hods = staff.objects.filter(user__in=custom_list_of_hods).order_by('staff_id')

    list1 = staff.objects.filter(user__in=custom_list_of_staffs_associates).order_by('staff_id')
    list2 = staff.objects.filter(user__in=custom_list_of_staffs_assistants).order_by('staff_id')
    list3 = staff.objects.filter(user__in=custom_list_of_staffs_network_admins).order_by('staff_id')

    for each in list1:
        list_of_staffs.append(each)
    for each in list2:
        list_of_staffs.append(each)
    for each in list3:
        list_of_staffs.append(each)

    list_of_students = student.objects.filter(user__in=custom_list_of_students).order_by('roll_no')

    no_of_users_to_delete = len(list_of_hods) + len(list_of_staffs) + len(list_of_students)
    avail_for_selection = False
    if no_of_users_to_delete != 0:
        avail_for_selection = True

    return render(request, 'accounts/approval.html', {
        'list_of_hods': list_of_hods,
        'list_of_staffs': list_of_staffs,
        'list_of_students': list_of_students,
        'avail_for_selection': avail_for_selection,
        'action': 'Delete',
        'no_of_users_deleted_unverified': no_of_users_deleted_unverified
    })

"""
