import hashlib
import re
from datetime import date, timedelta
from pprint import pprint

from django import forms
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.contrib.auth.views import redirect_to_login
from django.core.urlresolvers import reverse
from django.http import Http404
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.utils import timezone
from django.utils.crypto import random
from django.views.generic import UpdateView

from accounts.forms import RegisterStudent, RegisterStaff, LoginForm, passchange, regstud, regstaff, Signup_form, \
    PhotoForm
from accounts.models import CustomUser, student, staff, batch, staff_papers, staff_course_attended, active_batches, \
    department, student_achievements
from attendance.views.ConsolidatedAttendance2012API import ConsolidatedAttendance2012API
from attendance.views.SubjectConsolidatedAttendanceAPI import SubjectConsolidatedAttendanceAPI
from common.API.departmentAPI import getAllDepartments
from curriculum.models import department_sections
from curriculum.models import section_students
from curriculum.views.common_includes import get_current_batches, \
    get_current_semester_plan_of_student, get_courses_studied_by_student


def index(request):
    if request.session and 'remember_me' in request.session:
        return redirect('/dashboard/')
    return render(request, "index.html")


def imagegallery(request):
    return render(request, "image_gallery.html")


class UpdateImage(UpdateView):
    form_class = PhotoForm
    template_name = 'image_cropper.html'
    success_url = '/update_profile/'

    def user_passes_test(self, request):
        if request.user.is_authenticated():
            self.object = self.get_object()
            return self.object == request.user
        return False

    def dispatch(self, request, *args, **kwargs):
        if not self.user_passes_test(request):
            return redirect_to_login(request.get_full_path())
        return super(UpdateImage, self).dispatch(
            request, *args, **kwargs)

    def get_object(self):
        obj = CustomUser.objects.get(email=self.request.user)
        self.model = CustomUser.avatar
        return obj


class UpdateImageOnAccountCreation(UpdateView):
    form_class = PhotoForm
    template_name = 'image_cropper.html'
    success_url = '/profile_updated/'

    def user_passes_test(self, request):
        if 'image_required' in self.request.session:
            self.object = self.get_object()
            return True
        return False

    def dispatch(self, request, *args, **kwargs):
        if not self.user_passes_test(request):
            return redirect_to_login(request.get_full_path())
        return super(UpdateImageOnAccountCreation, self).dispatch(
            request, *args, **kwargs)

    def get_object(self):
        obj = CustomUser.objects.get(email=str(self.request.session['image_user_name']))
        self.model = CustomUser.avatar
        return obj


@login_required
def update_status(request):
    for each in student.objects.filter(user=request.user):
        request.session['profile_pic'] = str(each.user.avatar)
        request.session['user_name'] = str(each)

    for each in staff.objects.filter(user=request.user):
        request.session['profile_pic'] = str(each.user.avatar)
        request.session['user_name'] = str(each)
    msg = {
        'page_title': 'Update success',
        'title': 'Profile updated',
        'description': 'Your profile has been successfully updated!'
    }
    return render(request, 'prompt_pages/update_message.html', {'message': msg})


@login_required
def update_profile(request):
    for each in student.objects.filter(user=request.user):
        request.session['profile_pic'] = str(each.user.avatar)
        request.session['user_name'] = str(each)
        each.user.has_profile_picture = True
        each.user.save()

    for each in staff.objects.filter(user=request.user):
        request.session['profile_pic'] = str(each.user.avatar)
        request.session['user_name'] = str(each)
        each.user.has_profile_picture = True
        each.user.save()
    msg = {
        'page_title': 'Update success',
        'title': 'Profile Pic updated',
        'description': 'Your profile picture has been successfully updated!'
    }
    return render(request, 'prompt_pages/update_message.html', {'message': msg})


def profile_updated(request):
    if 'image_user_name' in request.session:
        user_instance = CustomUser.objects.get(email=str(request.session['image_user_name']))
        user_instance.has_profile_picture = True
        user_instance.save()
        request.session.clear()

    return render(request, 'prompt_pages/profile_updated.html')


@login_required
def userprofile(request, roll):
    # check if roll number is valid
    requested_student_query = student.objects.filter(roll_no=roll)
    if not requested_student_query.exists():
        return render(request, "404.html")
    requested_student_instance = requested_student_query.get()

    # check access if student account
    student_query = student.objects.filter(user=request.user)
    if (student_query.exists()):
        student_instance = student_query.get()
        if student_instance.roll_no != requested_student_instance.roll_no:
            return render(request, "401.html")

    idnumber = requested_student_instance.user_id
    custuser = CustomUser.objects.get(id=idnumber)

    if active_batches.objects.filter(department=requested_student_instance.department).filter(
            batch=requested_student_instance.batch).exists():
        current_semester_of_student = active_batches.objects.filter(
            department=requested_student_instance.department).get(
            batch=requested_student_instance.batch).current_semester_number_of_this_batch
    else:
        current_semester_of_student = 'Passed out'

    achievements = student_achievements.objects.filter(student=requested_student_instance)

    subject_consolidated_attendance_api_instance = SubjectConsolidatedAttendanceAPI()
    current_semester_plan_of_student = get_current_semester_plan_of_student(requested_student_instance)
    subject_consolidated_attendance_api_instance.semester_instance = current_semester_plan_of_student
    subject_consolidated_attendance_api_instance.start_date_instance = current_semester_plan_of_student.start_term_1
    if current_semester_plan_of_student.end_term_3:
        subject_consolidated_attendance_api_instance.end_date_instance = current_semester_plan_of_student.end_term_3
    else:
        subject_consolidated_attendance_api_instance.end_date_instance = timezone.now().date()
    consolidated_attendance_list = []
    for course_inst in get_courses_studied_by_student(
            requested_student_instance,
            current_semester_plan_of_student,
    ):
        temp = {}
        temp['course_instance'] = course_inst
        subject_consolidated_attendance_api_instance.course_instance = course_inst
        temp['subject_consolidated_result'] = \
            subject_consolidated_attendance_api_instance.get_report_as_dictionary(requested_student_instance.user)[0]
        temp['subject_absent_days'] = \
            subject_consolidated_attendance_api_instance.get_absent_date_and_hour_as_dictionary(
                requested_student_instance.user)[requested_student_instance.roll_no]
        consolidated_attendance_list.append(temp)

    overall_2012_attendance_result = None
    if requested_student_instance.batch.regulation.start_year == 2012:
        consolidated_attendance_2012_api_instance = ConsolidatedAttendance2012API()
        consolidated_attendance_2012_api_instance.semester_instance = current_semester_plan_of_student
        consolidated_attendance_2012_api_instance.start_date_instance = current_semester_plan_of_student.start_term_1
        if current_semester_plan_of_student.end_term_3:
            consolidated_attendance_2012_api_instance.end_date_instance = current_semester_plan_of_student.end_term_3
        else:
            consolidated_attendance_2012_api_instance.end_date_instance = timezone.now().date()
        overall_2012_attendance_result = \
            consolidated_attendance_2012_api_instance.get_report_as_dictionary(requested_student_instance.user)[0]

    pprint((consolidated_attendance_list))

    pprint(overall_2012_attendance_result)

    return render(request, "profilepage.html", {
        'user': requested_student_instance,
        'custuser': custuser,
        'achievements': achievements,
        'current_semester_of_student': current_semester_of_student,
        'consolidated_attendance_list': consolidated_attendance_list,
        'overall_2012_attendance_result': overall_2012_attendance_result,
    })


@login_required
def viewprofile(request, staff_pk=None):
    paper_list = []
    course_list = []
    current_semester_of_student = None

    try:
        obj = student.objects.get(user=request.user)

        current_semester_of_student = active_batches.objects.filter(department=obj.department).get(
            batch=obj.batch).current_semester_number_of_this_batch
        # staffobj=None
        idnumber = obj.user_id
        custuser = CustomUser.objects.get(id=idnumber)
        return redirect('/' + str(obj.roll_no))

    except:

        try:
            staff_instance = staff.objects.get(user=request.user)
        except:
            return render(request, "401.html")

        if staff_pk:
            if staff.objects.filter(pk=staff_pk):
                obj = staff.objects.get(pk=staff_pk)
            else:
                staff_pk = None
                obj = staff_instance

        else:
            obj = staff_instance

        if staff_pk and staff_instance.pk == int(staff_pk):
            staff_pk = None

        idnumber = obj.user_id
        custuser = CustomUser.objects.get(id=idnumber)

        for every in staff_papers.objects.filter(staff=obj).order_by('-year_of_publish'):
            paper_list.append(every)
        for each in staff_course_attended.objects.filter(staff=obj).order_by('-date'):
            temp = {}
            temp['course_name'] = str(each.course_name)
            temp['description'] = str(each.description)
            if each.end_date:
                if each.date == each.end_date:
                    temp['date'] = str(each.date.strftime('%d/%m/%Y'))
                else:
                    temp['date'] = str(each.date.strftime('%d/%m/%Y')) + ' - ' + str(each.end_date.strftime('%d/%m/%Y'))
            else:
                temp['date'] = str(each.date.strftime('%d/%m/%Y'))
            course_list.append(temp)

    return render(request, "viewprofile.html",
                  {
                      'obj': obj,
                      'staff_pk': staff_pk,
                      'custuser': custuser,
                      'paper_list': paper_list,
                      'course_list': course_list,
                      'current_semester_of_student': current_semester_of_student,
                  })


class UpdateProfile(UpdateView):
    form_class = regstaff
    template_name = 'edit_form.html'
    success_url = '/update_status/'
    slug = None

    def user_passes_test(self, request):
        if request.user.is_authenticated():
            self.object = self.get_object()
            return self.object.user == request.user
        return False

    def dispatch(self, request, *args, **kwargs):
        if not self.user_passes_test(request):
            return redirect_to_login(request.get_full_path())
        return super(UpdateProfile, self).dispatch(
            request, *args, **kwargs)

    def get_object(self):
        try:
            obj = student.objects.get(user=self.request.user)
            self.model = student
            self.form_class = regstud
        except student.DoesNotExist:
            obj = staff.objects.get(user=self.request.user)
            self.model = staff
        return obj


class UpdateProfilePreApproval(UpdateView):
    form_class = regstaff
    template_name = 'accounts/edit_profile_pre_approval.html'
    success_url = '/details_submit_success'
    slug = None

    def user_passes_test(self, request):
        if 'email' in request.session:
            self.object = self.get_object()
            return True
        return False

    def dispatch(self, request, *args, **kwargs):
        if not self.user_passes_test(request):
            return redirect_to_login(request.get_full_path())
        return super(UpdateProfilePreApproval, self).dispatch(
            request, *args, **kwargs)

    def get_object(self):
        requested_user = CustomUser.objects.get(email=self.request.session['email'])
        try:
            obj = student.objects.get(user=requested_user)
            self.model = student
            self.form_class = regstud
        except student.DoesNotExist:
            obj = staff.objects.get(user=requested_user)
            self.model = staff
        return obj


def getdetails(request):
    form_data = ''
    acc = ''
    error = False
    if request.method == 'POST':
        if 'staff' in request.POST:
            form2 = RegisterStaff(request.POST, request.FILES)
            if form2.is_valid():
                temp = form2.save(commit=False)
                designation = form2.cleaned_data.get("designation")
                # avatar = form2.cleaned_data['avatar']
                random_number_string = str(random.random())
                random_number_string = random_number_string.encode('utf-8')
                salt = hashlib.sha1(random_number_string).hexdigest()[:5]
                salt = salt.encode('utf-8')
                usernamesalt = str(request.POST.get('user_name'))
                usernamesalt = usernamesalt.encode('utf8')

                key = hashlib.sha1(salt + usernamesalt)
                key = key.hexdigest()
                temp.activation_key = key
                temp.key_expires = timezone.now() + timezone.timedelta(days=2)

                user = CustomUser.objects.get(email=str(request.POST.get('user_name')))

                if designation == 'Principal':
                    g = Group.objects.get(name='principal')
                elif designation == 'Professor':
                    faculty_group_obj = Group.objects.get(name='faculty')
                    faculty_group_obj.user_set.add(user)
                    g = Group.objects.get(name='hod')
                elif designation == 'Network Admin':
                    g = Group.objects.get(name='network_admin')
                elif designation == 'Hostel Admin':
                    g = Group.objects.get(name='hostel_admin')
                elif designation == 'Alumni Staff':
                    g = Group.objects.get(name='alumni_staff')
                else:
                    g = Group.objects.get(name='faculty')
                g.user_set.add(user)
                temp.user = user
                temp.save()
                user.has_filled_data = True
                user.save()

                request.session['image_required'] = True
                request.session['image_user_name'] = request.session['user_name']
                del request.session['user_name']
                return redirect(reverse('upload_image_account_creation'))


            else:
                form_data = form2
                acc = 'staff'

        elif 'student' in request.POST:
            form1 = RegisterStudent(request.POST, request.FILES)
            if form1.is_valid():
                temp = form1.save(commit=False)
                try:
                    batch_obj_posted = request.POST.get('batch')
                    batch_obj = batch.objects.get(id=int(batch_obj_posted))
                    got_department = request.POST.get('department')
                    has_roll_number = request.POST.get('has_roll_number')
                    print('has roll number')
                    print(has_roll_number)
                    department_instance = department.objects.get(id=got_department)
                    # avatar = form1.cleaned_data['avatar']
                    active_batch_obj = active_batches.objects.get(batch=batch_obj,
                                                                  department=department_instance)
                    sem = active_batch_obj.current_semester_number_of_this_batch
                    # print(sem)
                    temp.current_semester = sem
                    temp.batch_id = int(batch_obj_posted)
                    temp.qualification = str(request.POST.get('qualification'))
                    temp.entry_type = str(request.POST.get('entrytype'))
                    temp.department = department_instance

                    random_number_string = str(random.random())
                    random_number_string = random_number_string.encode('utf-8')
                    salt = hashlib.sha1(random_number_string).hexdigest()[:5]
                    salt = salt.encode('utf-8')
                    usernamesalt = str(request.POST.get('user_name'))
                    usernamesalt = usernamesalt.encode('utf8')

                    key = hashlib.sha1(salt + usernamesalt)
                    key = key.hexdigest()
                    temp.activation_key = key
                    temp.key_expires = timezone.now() + timezone.timedelta(days=2)

                    user = CustomUser.objects.get(email=str(request.POST.get('user_name')))
                    temp.user = user
                    g = Group.objects.get(name='student')
                    g.user_set.add(user)
                    temp.save()
                    user.has_filled_data = True
                    user.save()

                    # assign student to A section by default
                    new_user = CustomUser.objects.get(email=str(request.POST.get('user_name')))
                    student_instance = student.objects.get(user=new_user)
                    if student_instance.roll_no == '999999':
                        print('inside false')
                        student_instance.roll_no = 'T_' + str(temp.pk)
                        print(student_instance.roll_no)
                        student_instance.save()
                    department_instance = student_instance.department
                    batch_instance = student_instance.batch
                    department_sections_instance = department_sections.objects.filter(
                        department=department_instance
                    ).filter(
                        batch=batch_instance
                    ).get(
                        section_name='A'
                    )

                    section_students.objects.get_or_create(
                        section=department_sections_instance,
                        student=student_instance
                    )
                    print('Successfully assigned student to section A')

                    # print(vars(temp))

                    request.session['image_required'] = True
                    request.session['image_user_name'] = request.session['user_name']
                    del request.session['user_name']
                    return redirect(reverse('upload_image_account_creation'))

                except:
                    form_data = form1
                    acc = 'student'
                    error = 'datamiss'

            else:
                form_data = form1
                acc = 'student'

    else:
        try:
            is_staff_acc = bool(request.session['is_staff_acc'])
        except:
            msg = {
                'page_title': 'GCT | Unauthorized Access',
                'title': 'Unauthorized Access',
                'description': 'You don\'t have privilege to access this resourse!',
                'login': True
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

        if is_staff_acc:
            form_data = RegisterStaff()
            acc = 'staff'
        else:
            form_data = RegisterStudent()
            acc = 'student'

    try:
        user_name = request.session['user_name']
    except:
        msg = {
            'page_title': 'GCT | Resubmission',
            'title': 'Details Already Submitted',
            'description': 'You don\'t have privilege to access this resourse!',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    # print(user_name)

    department_list = getAllDepartments()

    return render(request, "fill_signup_details.html",
                  {'form1': form_data, 'acc_type': acc, 'user': user_name, 'error': error,
                   'department_list': department_list})


def get_current_batches_signup(request):
    if request.method == 'POST':
        if request.is_ajax():
            dictionary = {}
            chosen_programme = str(request.POST.get('qualification'))
            chosen_entry_type = str(request.POST.get('entry_type'))
            chosen_department = request.POST.get('department')
            print(chosen_department)
            department_instance = department.objects.get(id=chosen_department)

            print('chosen_entry_type=' + str(chosen_entry_type))
            print('chosen_programme_type=' + str(chosen_programme))

            if chosen_programme == 'Phd':
                batch_objects = get_current_batches(department_instance, get_phd=True)
            else:
                batch_objects = get_current_batches(department_instance)

            list = []
            for each in batch_objects:
                temp = {}
                temp['batch_obj'] = each['batch_obj']
                if chosen_entry_type != 'regular' and chosen_entry_type != 'transfer':
                    batch_start_year = int(each['start_year'])
                    batch_end_year = str(each['end_year'])
                    batch_start_year_plus_one = str(batch_start_year + 1)
                    temp['display_batch_text'] = batch_start_year_plus_one + '-' + batch_end_year
                else:
                    temp['display_batch_text'] = str(each['start_year']) + '-' + str(each['end_year'])
                if each['programme'] == chosen_programme:
                    list.append(temp)

            pprint(list)

            dictionary['list'] = list

            return JsonResponse(dictionary)


def has_blank_fields(email, customUser=None):
    required_field = [
        'first_name',
        'last_name',
        'gender',
        'qualification',
        'permanent_address',
        'temporary_address',
        'phone_number',
        'caste',
        'community',
        # 'aadhaar_number',
    ]

    student_query = student.objects.filter(user__email=email)
    if student_query.exists():
        student_instance = student_query.get()

        for field in required_field:
            field_value = getattr(student_instance, field)
            if field_value is None or len(field_value) == 0:
                return True

    return False


def userlogin(request):
    login_form = LoginForm()
    error = None
    if request.method == 'POST':
        request.session.clear()
        request.session['last_visit'] = None
        request.session['logged_first'] = True
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            try:
                email = login_form.cleaned_data['email']
                password = login_form.cleaned_data['password']
                if not CustomUser.objects.get(email=email):
                    raise forms.ValidationError("Email ID is not registered!")

                user = authenticate(email=email, password=password)

                if user is not None:
                    obj = CustomUser.objects.get(email=email)
                    # print(vars(obj))
                    is_approved_user = getattr(obj, 'is_approved')
                    is_verified_user = getattr(obj, 'is_verified')
                    is_filled_data = getattr(obj, 'has_filled_data')
                    has_profile_picture = obj.has_profile_picture
                    is_staff_acc = getattr(obj, 'is_staff_account')
                    print('is_verified_user=' + str(is_verified_user))
                    # app = CustomUser.objects.filter(email=user)
                    # print(app.values('is_approved') == False)
                    if is_verified_user:
                        if student.objects.filter(user=user).exists():
                            student_inst = student.objects.get(user=user)
                            # check if student batch is active
                            # if not active_batches.objects.filter(department=student_inst.department).filter(
                            #         batch=student_inst.batch
                            # ).exists():
                            #     msg = {
                            #         'page_title': 'Inactive batch',
                            #         'title': 'Batch not active',
                            #         # 'deleted_user': str(delete_user),
                            #         'description': 'Your batch seems to be inactive in your department. Please contact the developers through your Programme co-ordintaor/Faculty advisor to resolve this issue',
                            #         # 'createnew': True
                            #     }
                            #     return render(request, 'prompt_pages/general_error_msg.html', {'message': msg})
                            if not section_students.objects.filter(student=student_inst).exists():
                                msg = {
                                    'page_title': 'Section Error',
                                    'title': 'Section not assigned',
                                    # 'deleted_user': str(delete_user),
                                    'description': 'Your Programme Co-ordinator has not assigned you to particular section. Contact him/her immediately',
                                    # 'createnew': True
                                }
                                return render(request, 'prompt_pages/general_error_msg.html', {'message': msg})

                        if (not is_filled_data):
                            request.session['is_staff_acc'] = is_staff_acc
                            request.session['user_name'] = email
                            return redirect('getdetails')

                        if (not has_profile_picture):
                            request.session['is_staff_acc'] = is_staff_acc
                            request.session['image_user_name'] = email
                            request.session['image_required'] = True
                            return redirect(reverse('upload_image_account_creation'))



                        elif is_approved_user:
                            if student.objects.filter(user=user).exists():
                                student_inst = student.objects.get(user=user)
                                # check if student batch is active
                                if not active_batches.objects.filter(department=student_inst.department).filter(
                                        batch=student_inst.batch
                                ).exists():
                                    msg = {
                                        'page_title': 'Inactive batch',
                                        'title': 'Batch not active',
                                        # 'deleted_user': str(delete_user),
                                        'description': 'Your batch seems to be inactive in your department. Please contact the developers through your Programme co-ordintaor/Faculty advisor to resolve this issue',
                                        # 'createnew': True
                                    }
                                    return render(request, 'prompt_pages/general_error_msg.html', {'message': msg})
                            if student.objects.filter(user=user):

                                request.session['user_type'] = 'student'
                                request.session['user_name'] = str(student.objects.get(user=user))
                                for each in student.objects.filter(user=user):
                                    # request.session['profile_pic'] = str(each.avatar)
                                    request.session['roll_no'] = str(each.roll_no)
                                    request.session['gender'] = str(each.gender)
                                    request.session['profile_pic'] = str(each.user.avatar)
                            elif staff.objects.filter(user=user):
                                staff_user = staff.objects.get(user=user)
                                if staff_user.get_designation_display() == 'Principal' or staff_user.get_designation_display() == 'Professor':
                                    request.session['user_type'] = staff_user.get_designation_display()
                                else:
                                    request.session['user_type'] = 'staff'
                                request.session['user_name'] = str(staff.objects.get(user=user))
                            for each in staff.objects.filter(user=user):
                                request.session['profile_pic'] = str(each.user.avatar)
                                request.session['gender'] = str(each.gender)
                            # print('user=' + str(request.session['gender']))
                            # last_login = ''
                            for obj in CustomUser.objects.filter(email=str(user)):
                                last_login = obj.last_login
                            # print('last_visit=' + str(last_login))
                            if last_login:
                                request.session['last_visit'] = last_login.strftime("%d %B %Y at %l:%M:%S %p")
                                today = str(date.today())
                                # print('today='+str(today))
                                yesterday = str(date.today() - timedelta(1))
                                # print('yesterday='+str(yesterday))
                                last_login_date = str(last_login.strftime("%Y-%m-%d"))
                                # print('last_login_date='+str(last_login_date))
                                if last_login_date == today:
                                    last_login_time = str(last_login.strftime(" at %l:%M:%S %p"))
                                    request.session['last_visit'] = 'Today' + last_login_time
                                elif last_login_date == yesterday:
                                    last_login_time = str(last_login.strftime(" at %l:%M:%S %p"))
                                    request.session['last_visit'] = 'Yesterday' + last_login_time
                            else:
                                request.session['last_visit'] = 'welcome'
                            # print(request.session['last_visit'])
                            login(request, user)

                            if request.POST.get('remember_me'):
                                request.session['remember_me'] = True
                                print('remember me set')
                            else:
                                request.session.set_expiry(0)

                            request.session['disp_batch'] = None
                            if has_blank_fields(email):
                                request.session['email'] = email
                                return HttpResponseRedirect('/update_profile_pre_login/')
                            if 'next' in request.GET:
                                return redirect(request.GET['next'])
                            return redirect('/dashboard/')
                        else:
                            # print('not approved')
                            request.session['delete_user'] = str(user)
                            request.session['email'] = email
                            msg = {
                                'page_title': 'GCT | Not approved',
                                'title': 'Account not approved'
                            }
                            return render(request, 'prompt_pages/not_approved.html', {'message': msg})
                    else:
                        # print('not verified')
                        request.session['delete_user'] = str(user)
                        msg = {
                            'page_title': 'GCT | Verification error',
                            'title': 'Account not verified',
                            'description': 'Your email is not yet verified',
                        }
                        return render(request, 'prompt_pages/not_verified.html', {'message': msg, 'user': user})

                else:
                    # print('not valid')
                    msg = {
                        'page_title': 'GCT | Login error',
                        'title': 'Invalid account',
                        'description': 'Email and password did not match!',
                    }
                    return render(request, 'prompt_pages/invalid_account.html', {'message': msg})
            except forms.ValidationError:
                error = 'emailerror'
                # else:
        # login_form = LoginForm()
        error = 'emailerror'

    if request.session and 'remember_me' in request.session:
        return redirect('/dashboard/')
    return render(request, "accounts/login.html", {'loginForm': login_form, 'error': error})


@login_required
def changepass(request):
    change_pass = passchange()
    if request.method == 'POST':
        # emailid = request.POST['email']
        emailid = CustomUser.objects.get(email=request.user)
        oldpass = request.POST['oldpass']
        newpass1 = request.POST['newpass']
        newpass2 = request.POST['newpass_again']
        checker = authenticate(email=emailid, password=oldpass)
        if not re.match(r'(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*-+=_/;:~]).{8,}', newpass1):
            msg = {
                'page_title': 'Update failure',
                'title': 'Profile not updated',
                'description': 'Password should contain atleast 8 characters as well as a character,a digit and a special character',
            }
            return render(request, "changepass.html", {'message': msg})
        elif newpass1 and newpass2 and newpass1 != newpass2:
            msg = {
                'page_title': 'Update failure',
                'title': 'Profile not updated',
                'description': 'Your new passwords does not match!Try again..',
            }
            return render(request, "changepass.html", {'message': msg})
        elif newpass1 == oldpass:
            msg = {
                'page_title': 'Update failure',
                'title': 'Profile not updated',
                'description': 'Your have used the same password! Use a different password..',
            }
            return render(request, "changepass.html", {'message': msg})
        elif checker is None:
            msg = {
                'page_title': 'Update failure',
                'title': 'Profile not updated',
                'description': 'Your have entered wrong old password!',
            }
            return render(request, "changepass.html", {'message': msg})
        elif checker is not None:
            u = CustomUser.objects.get(email=emailid)
            u.set_password(newpass1)
            u.save()
            msg = {
                'page_title': 'Update success',
                'title': 'Profile updated',
                'description': 'Your password has been successfully changed! '
                               'Please login in to continue to the site!',
            }
            return render(request, "prompt_pages/update_message.html", {'forgotpass': change_pass, 'message': msg})
        else:
            msg = {
                'page_title': 'Update failure',
                'title': 'Profile not updated',
                'description': 'Username and email id mismatch! Forgot your password? Try resetting it..',
            }
            return render(request, "changepass.html", {'message': msg})
    else:
        return render(request, "changepass.html", {'changepass': change_pass})


# @login_required
def userlogout(request):
    request.session.clear()
    logout(request)
    return HttpResponseRedirect('/')


def cancel(request):
    delete_user = str(request.session['delete_user'])
    CustomUser.objects.filter(email=delete_user).delete()
    msg = {
        'page_title': 'Deleted Success',
        'title': 'Account Deleted',
        'deleted_user': str(delete_user),
        'description': '\'s request canceled successfully...',
        'createnew': True
    }
    return render(request, 'prompt_pages/error_page_base.html', {'message': msg})


def homepagelogout(request):
    request.session.clear()
    logout(request)
    return HttpResponseRedirect('/')


def signup(request):
    form1 = Signup_form()
    error = 'success'
    if request.method == 'POST':
        if request.POST.get('student_register'):
            form1 = Signup_form(request.POST, request.FILES)
            if form1.is_valid():
                try:
                    password1 = form1.cleaned_data['password1']
                    password2 = form1.cleaned_data['password2']
                    if password1 and password2 and password1 != password2:
                        raise forms.ValidationError("Passwords don't match")
                    # temp = form1.save(commit=False)
                    email = form1.cleaned_data['email']
                    password = form1.cleaned_data['password2']

                    print('acc_type=' + str(request.POST.get('acc_type')))

                    acc_type = str(request.POST.get('acc_type'))

                    if (acc_type == 'staff'):
                        is_staff_acc = True
                    else:
                        is_staff_acc = False

                    random_number_string = str(random.random())
                    random_number_string = random_number_string.encode('utf-8')
                    salt = hashlib.sha1(random_number_string).hexdigest()[:5]
                    salt = salt.encode('utf-8')
                    usernamesalt = email
                    usernamesalt = usernamesalt.encode('utf8')

                    key = hashlib.sha1(salt + usernamesalt)
                    key = key.hexdigest()
                    activation_key = key
                    key_expires = timezone.now() + timezone.timedelta(days=2)

                    user = CustomUser.objects.create_user(email=email,
                                                          password=password,
                                                          is_staff_account=is_staff_acc,
                                                          activation_key=activation_key,
                                                          key_expires=key_expires
                                                          )

                    datas = {}
                    datas['email'] = email
                    datas['activation_key'] = key

                    form1.sendEmail(datas)

                    msg = {
                        'page_title': 'GCT | Signup success',
                        'title': 'Please Verify Email-ID',
                        'description': 'An email has been sent your mail ID.Please verify it to proceed',
                    }
                    return render(request, 'prompt_pages/signup_success.html', {'message': msg})
                except forms.ValidationError:
                    error = 'pwd_student'
    return render(request, "signup.html", {'form1': form1})


# View called from activation email. Activate user if link didn't expire (48h default), or offer to
# send a second link if the first expired.
def activation(request, key):
    activation_expired = False
    already_active = False
    try:
        user = CustomUser.objects.get(activation_key=key)
    except CustomUser.DoesNotExist:
        msg = {
            'page_title': 'Expired Link',
            'title': 'Link Expired',
            'description': 'Please check whether you are clicking the latest sent to your mail'
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    print(vars(user))
    if user.is_verified == False:
        if timezone.now() > user.key_expires:
            activation_expired = True  # Display : offer to user to have another activation link (a link in template sending to the view new_activation_link)
            id_user = user.id
        else:  # Activation successful
            user.is_verified = True
            user.save()

    # If user is already active, simply display error message
    else:
        already_active = True  # Display : error message
    return render(request, 'prompt_pages/activated_mail.html', locals())


def new_activation_link(request, user_id):
    form = Signup_form()
    try:
        user = CustomUser.objects.get(id=user_id)
    except CustomUser.DoesNotExist:
        raise Http404
    if user is not None and not user.is_verified:
        random_number_string = str(random.random())
        random_number_string = random_number_string.encode('utf-8')
        salt = hashlib.sha1(random_number_string).hexdigest()[:5]
        salt = salt.encode('utf-8')
        usernamesalt = user.email
        usernamesalt = usernamesalt.encode('utf8')

        key = hashlib.sha1(salt + usernamesalt)
        key = key.hexdigest()
        user.activation_key = key
        user.key_expires = timezone.now() + timezone.timedelta(days=2)
        user.save()

        datas = {}
        datas['email'] = user.email
        datas['activation_key'] = key

        form.sendEmail(datas)
    msg = {
        'page_title': 'GCT | Re-sent conformation mail',
        'title': 'Email re-sent',
        'description': 'An email has been sent your mail ID.Please verify it to proceed',
    }
    return render(request, 'prompt_pages/signup_success.html', {'message': msg})


def details_submit_success(request):
    msg = {
        'page_title': 'GCT | Details confirmation',
        'title': 'Details submitted',
        'description': 'You have to be approved by an authorized person in order to proceed',
        'trylogin': True
    }
    return render(request, 'prompt_pages/details_submit_success.html', {'message': msg})





def no_javascript_page(request):
    return render(request, 'prompt_pages/enable_js.html')
