from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from simple_history.admin import SimpleHistoryAdmin

from accounts.models import CustomUser, department, department_programmes, sub_department, programme, active_batches, \
    staff, student, batch, staff_course_attended, staff_papers, onesignal


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = CustomUser
        fields = ('email',)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = CustomUser
        fields = ('email', 'password', 'is_active', 'is_superuser', 'is_approved', 'is_verified')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('id', 'email', 'is_active', 'is_superuser', 'is_approved', 'is_verified')
    list_filter = ('is_superuser',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Permissions', {'fields': ('is_approved', 'is_active', 'is_superuser', 'is_verified')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
         ),
    )
    search_fields = ('email', 'id')
    ordering = ('email', 'id')
    filter_horizontal = ()


class StaffAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'user', 'departments', 'designation')
    list_filter = ('department', 'gender', 'dob')
    search_fields = ('first_name', 'middle_name', 'last_name', 'user__email', 'id')

    ordering = ('first_name', 'middle_name', 'last_name', 'user__email', 'id')

    def name(self, obj):
        name = str(obj.first_name)
        if obj.middle_name != None:
            name += ' '
            name += obj.middle_name
        name += ' '
        name += obj.last_name
        return name

    def departments(self, obj):
        return obj.department.acronym


class StudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'roll_no', 'name', 'user', 'departments', 'batch')
    list_filter = ('department', 'gender', 'dob', 'batch')
    search_fields = ('roll_no', 'first_name', 'middle_name', 'last_name', 'user__email', 'id')

    ordering = ('roll_no', 'batch', 'first_name', 'middle_name', 'last_name', 'user__email', 'id')

    def name(self, obj):
        name = str(obj.first_name)
        if obj.middle_name != None:
            name += ' '
            name += obj.middle_name
        name += ' '
        name += obj.last_name
        return name

    def departments(self, obj):
        return obj.department.acronym


class StaffCourseAttendedAdmin(admin.ModelAdmin):
    list_display = ('id', 'staff', 'departments', 'course_name', 'date', 'end_date')
    list_filter = ('staff__department', 'date', 'end_date')
    search_fields = (
        'staff__first_name', 'staff__middle_name', 'staff__last_name', 'staff__user__email', 'course_name',
        'description', 'id')

    ordering = ('date', 'staff', 'id')

    def departments(self, obj):
        return obj.staff.department.acronym


class StaffPapersAdmin(admin.ModelAdmin):
    list_display = ('id', 'staff', 'departments', 'paper_name', 'year_of_publish')
    list_filter = ('staff__department', 'year_of_publish')
    search_fields = (
        'staff__first_name', 'staff__middle_name', 'staff__last_name', 'staff__user__email', 'year_of_publish',
        'paper_name', 'description', 'id')

    ordering = ('year_of_publish', 'staff', 'id')

    def departments(self, obj):
        return obj.staff.department.acronym


# Now register the new UserAdmin...
admin.site.register(CustomUser, UserAdmin)

admin.site.register(department, SimpleHistoryAdmin)
admin.site.register(department_programmes, SimpleHistoryAdmin)
admin.site.register(sub_department, SimpleHistoryAdmin)
admin.site.register(programme, SimpleHistoryAdmin)
admin.site.register(active_batches, SimpleHistoryAdmin)
admin.site.register(staff, StaffAdmin)
admin.site.register(student, StudentAdmin)
admin.site.register(batch, SimpleHistoryAdmin)
admin.site.register(staff_course_attended, StaffCourseAttendedAdmin)
admin.site.register(staff_papers, StaffPapersAdmin)
admin.site.register(onesignal)
