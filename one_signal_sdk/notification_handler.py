from django.conf import settings
from .one_signal_sdk import OneSignalSdk
from accounts.models import onesignal, CustomUser
from dashboard.notification import send_notification

def get_player_ids(custom_user_instances):
    return_list = []
    if type(custom_user_instances) is CustomUser:
        onesignal_queryset = onesignal.objects.filter(user=custom_user_instances)
        for onesignal_inst in onesignal_queryset:
            return_list.append(onesignal_inst.onesignal_id)
        return return_list
    for entry in custom_user_instances:
        onesignal_queryset = onesignal.objects.filter(user=entry)
        for onesignal_inst in onesignal_queryset:
            return_list.append(onesignal_inst.onesignal_id)
    return return_list


def handle_user_notification(contents, heading, custom_user_instances, url='https://gctportal.com'):
    oneSignalSdk = OneSignalSdk(settings.ONESIGNAL_REST_API_KEY, settings.ONESIGNAL_APP_API_ID)


    print(heading)
    print(contents)
    print(custom_user_instances)
    print(url)
    print()

    #write to built in notification table
    try:
        if type(custom_user_instances) is CustomUser:
            send_notification(custom_user_instances,str(heading) + ' - ' + str(contents),url)
        for entry in custom_user_instances:
            send_notification(entry, str(heading) + ' - ' + str(contents), url)
    except:
        pass


    # if settings.DEBUG == True:
    #     return

    if not settings.ENABLE_ONESIGNAL_NOTIFICATIONS:
        return

    """
    handler to deliver only 1000 notifications at a time
    """

    player_ids = get_player_ids(custom_user_instances)
    if not player_ids:
        return

    start_counter = 0
    end_counter = 999
    print(player_ids)

    while end_counter < len(player_ids):
        resp = oneSignalSdk.create_notification(
            contents=contents,
            heading=heading,
            url=url,
            player_ids=player_ids[start_counter:end_counter])
        print(player_ids[start_counter:end_counter])
        print(resp)
        start_counter = start_counter + 1000
        if (end_counter + 1000) > len(player_ids):
            end_counter = end_counter + (len(player_ids) - end_counter)
        else:
            end_counter = end_counter + 1000

    resp = oneSignalSdk.create_notification(
        contents=contents,
        heading=heading,
        url=url,
        player_ids=player_ids[start_counter:end_counter])
    print(resp)

    #todo handle response delete