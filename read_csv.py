"""
dept = raw_input('Enter dept acronym: ')
reg_strt_yr = raw_input('Enter reg start year: ')
reg_end_yr = raw_input('Enter reg end year: ')
programme = raw_input('Enter programme of these courses: ')

attrs = {
    'department':dept,
    'regulation_start_year':reg_strt_yr,
    'regulation_end_year':reg_end_yr,
    'programme':programme,
}

root = etree.Element('course_list',attrs)
with open('/home/sid/Downloads/courses.csv', newline='\r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:
        print(row)

sub_type = ''
with open('/home/sid/Downloads/courses.csv', newline='\r') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in spamreader:

        if row[2] == 'THEORY':
            sub_type = 'T'
        elif row[2] == 'PRACTICAL':
            sub_type = 'P'

        row[1] = row[1].replace(' ','')

        if row[1] != '' and re.match(r'^12',row[1]) and row[1][5] != 'x':

            print('semester = ' + row[1][3])

            sem_no = row[1][3]
            course_id = row[1].replace(' ','')
            course_name = row[2].replace('\r',' ')

            internal_marks = row[3]
            external_marks = row[4]

            if row[1][4] == 'E':
                is_elective = str(True)
            elif row[1][4] == 'I':
                is_elective = 'Industrial'
            else:
                is_elective = str(False)
            course_attrs = {
                'id':course_id,
                'semester':sem_no,
                'is_elective':is_elective,
                'subject_type':sub_type,
            }
            course = etree.SubElement(root,'course',course_attrs)
            c_name = etree.SubElement(course,'course_name')
            c_name.text = course_name

            i_marks = etree.SubElement(course,'internal')
            i_marks.text = internal_marks

            e_marks = etree.SubElement(course,'external')
            e_marks.text = external_marks

            l = etree.SubElement(course,'L')
            l.text = row[6]
            t = etree.SubElement(course,'T')
            t.text = row[7]
            p = etree.SubElement(course,'P')
            p.text = row[8]
            c = etree.SubElement(course,'C')
            c.text = row[9]

            print(row[1].replace(' ','') + ' - ' + row[2].replace('\r',' '))


s = etree.tostring(root)
print(s)

f = open('doc.xml', 'wb')
f.write(etree.tostring(root))
f.close()

"""
