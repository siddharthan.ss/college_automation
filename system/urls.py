from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from system.views.previous_timetable import view_previous_timetable
from system.views.previous_attendance_reports import view_previous_attendance_reports
from system.views.pastCourseOutcomes import pastCoMarksReport
from system.views.pastFeedback import pastFeedbackReport, PastFeedbackPdfReport

urlpatterns = [
    url(r'^view_previous_timetable/$', view_previous_timetable, name="view_previous_timetable"),

    url(r'^view_previous_co_marks/$', pastCoMarksReport, name="pastCoMarksReport"),

    url(r'^view_previous_feedback/$', pastFeedbackReport, name="pastFeedbackReport"),
    url(r'^view_previous_feedback_pdf/(?P<staffCoursePk>[0-9]+)/$', login_required(PastFeedbackPdfReport.as_view()),
        name='pastFeedbackPdfReport'),

    url(r'^previous_attendance_reports/$', view_previous_attendance_reports, name="previous_attendance_reports"),
]
