from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import render
from easy_pdf.views import PDFTemplateView

from accounts.models import department, batch
from feedback.views import GetFeedbackReport
from curriculum.models import semester, staff_course
from common.utils.ReportTabsUtil import getStaffCoursesForSemester
from common.API.departmentAPI import getDepartmentOfPk, getCoreDepartments


@login_required
@permission_required('curriculum.can_mark_attendance')
def pastFeedbackReport(request):
    departmentList = getCoreDepartments()
    batchList = batch.objects.all()
    if request.method == 'POST':
        if request.is_ajax():
            if request.POST.get('action') == 'getSemesterPlan':
                departmentPk = int(request.POST.get('departmentPk'))
                batchPk = int(request.POST.get('batchPk'))
                departmentInstance = getDepartmentOfPk(departmentPk)
                batchInstance = batch.objects.get(pk=batchPk)
                semesterList = []
                for eachSemester in semester.objects.filter(department=departmentInstance, batch=batchInstance):
                    temp = {}
                    temp['displayText'] = eachSemester.getDisplayTextForPastReport()
                    temp['pk'] = eachSemester.pk
                    semesterList.append(temp)

                dictionary = {}
                dictionary['semesterList'] = semesterList

                return JsonResponse(dictionary)

            if request.POST.get('action') == 'getCourses':
                semesterPk = request.POST.get('semesterPk')
                semesterInstance = semester.objects.get(pk=semesterPk)

                courseList = []

                for eachCourse in getStaffCoursesForSemester(semesterInstance):
                    staffCourseInstance = eachCourse['staffCourseInstance']
                    temp = {}
                    temp['pk'] = staffCourseInstance.pk
                    temp['course'] = str(staffCourseInstance.course)
                    courseList.append(temp)

                dictionary = {}
                dictionary['courseList'] = courseList

                return JsonResponse(dictionary)

        if 'staffCoursePkOnCoRequest' in request.POST:
            staffCoursePk = request.POST.get('staffCoursePkOnCoRequest')

            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            dictionary1 = GetFeedbackReport(staffCourseInstance)
            return render(request, 'pastFeedbackreport/pastFeedbackReport.html',
                          {
                              'staffCourseInstance': staffCourseInstance,
                              'departmentInstance': getDepartmentOfPk(int(request.POST.get('departmentPk'))),
                              'dictionary1': dictionary1,
                              'batchList': batchList,
                              'departmentList': departmentList
                          })

    return render(request, 'pastFeedbackreport/pastFeedbackReport.html',
                  {
                      'batchList': batchList,
                      'departmentList': departmentList
                  })


def getPastFeedback(request, staffCoursePk):
    if not staff_course.objects.filter(pk=staffCoursePk):
        return {'noPermission': True}
    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    dictionary = GetFeedbackReport(staffCourseInstance)

    context_data = {
        'staffCourseInstance': staffCourseInstance,
        'dictionary': dictionary
    }

    return context_data


class PastFeedbackPdfReport(PDFTemplateView):
    template_name = "pastFeedbackreport/pastFeedbackPdfReport.html"

    def get(self, request, *args, **kwargs):
        self.staffCoursePk = kwargs.pop('staffCoursePk')
        return super(PastFeedbackPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = getPastFeedback(self.request, staffCoursePk=self.staffCoursePk)
        return super(PastFeedbackPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )
