from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import render

from accounts.models import batch, staff, department
from curriculum.models import courses, semester, staff_course, time_table, elective_course_staff, \
    one_credit_course_staff
from common.API.departmentAPI import getCoreDepartments


# from read_csv import is_elective


@login_required
@permission_required('accounts.can_view_profiles')
def view_previous_timetable(request):
    staff_instance = staff.objects.get(user__email=request.user)
    department_list = getCoreDepartments()
    batch_list = batch.objects.all()
    if request.method == 'POST':
        if request.is_ajax():
            if 'batch' in request.POST:
                dictionary = {}
                chosen_batch = str(request.POST.get('batch'))
                department_id = request.POST.get('department')
                batch_instance = batch.objects.get(pk=chosen_batch)

                department_instance = department.objects.get(id=department_id)

                available_semesters = semester.objects.filter(
                    batch=batch_instance
                ).filter(
                    department=department_instance
                )
                serialized_available_semesters = []
                for sem_inst in available_semesters:
                    temp = {}
                    temp['semester_pk'] = sem_inst.pk
                    temp['semester_display_text'] = str(sem_inst.batch.programme) + ' SEM ' + str(
                        sem_inst.semester_number)
                    serialized_available_semesters.append(temp)
                dictionary['available_semesters'] = serialized_available_semesters
                return JsonResponse(dictionary)

        if 'semester_pk' in request.POST:
            department_id = request.POST.get('department')
            batch_id = request.POST.get('batch_id')
            semester_pk = request.POST.get('semester_pk')

            department_instance = department.objects.get(pk=department_id)

            batch_instance = batch.objects.get(pk=batch_id)
            batch_programme = batch_instance.programme
            regulation_instance = batch_instance.regulation

            semester_instance = semester.objects.get(pk=semester_pk)

            semester_number = semester_instance.semester_number


            list_of_courses = []

            for each in courses.objects.filter(department=department_instance, regulation=regulation_instance,
                                               programme=batch_programme, semester=semester_number,
                                               is_open=False, is_one_credit=False):
                temp_list = {}
                temp_list['course_text'] = each
                temp_list['course_id'] = each.course_id
                staff_list = []
                if each.is_elective:
                    for instance in elective_course_staff.objects.filter(course=each):
                        staff_list.append(instance.staff)
                    temp_list['staff_list'] = staff_list
                else:
                    if staff_course.objects.filter(course=each, batch=batch_instance,
                                                   semester=semester_instance,
                                                   department=department_instance):
                        for instance in staff_course.objects.filter(course=each, batch=batch_instance,
                                                                    semester=semester_instance,
                                                                    department=department_instance):
                            for each_staff in instance.staffs.all():
                                for id_instance in staff.objects.filter(staff_id=each_staff.staff_id):
                                    staff_id = id_instance.id
                                    staff_list.append(str(id_instance))
                            temp_list['staff_list'] = staff_list
                temp_list['course_name'] = each.course_name
                temp_list['code'] = each.code
                list_of_courses.append(temp_list)
            for each in courses.objects.filter(department=department_instance, regulation=regulation_instance,
                                               programme=batch_programme, semester=semester_number,
                                               is_one_credit=True):
                temp_list = {}
                temp_list['course_text'] = each
                temp_list['course_id'] = each.course_id
                staff_list = []
                for instance in one_credit_course_staff.objects.filter(course=each):
                    staff_list.append(instance.staff)
                temp_list['staff_list'] = staff_list
                temp_list['course_name'] = each.course_name
                temp_list['code'] = each.code
                list_of_courses.append(temp_list)

            list_of_days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
            available_hours = 8
            list_of_hours = []
            for each_day in list_of_days:
                day_list = []
                current_hour = 1
                while current_hour <= available_hours:
                    hour_list = []
                    for each_course_hour in time_table.objects.filter(department=department_instance, is_active=True,
                                                                      batch=batch_instance, semester=semester_instance,
                                                                      day__day_name=each_day, hour=current_hour):
                        course_instance_code = each_course_hour.course
                        if course_instance_code.code:
                            hour_list.append(course_instance_code.code)
                        else:
                            hour_list.append(each_course_hour.course)
                    day_list.append(hour_list)
                    current_hour = current_hour + 1
                temp = {}
                temp['day_name'] = each_day
                temp['hours'] = day_list
                list_of_hours.append(temp)

                # pprint(list_of_hours)

            faculty_advisors = semester_instance.faculty_advisor.all()

            # pprint(faculty_advisors)

            return render(request, 'timetable/view_previous_timetable.html',
                          {
                              'department_list': department_list,
                              'list_of_courses': list_of_courses,
                              'list_of_hours': list_of_hours,
                              'faculty_advisors': faculty_advisors,
                              'batch_list': batch_list,
                              'batch_instance': batch_instance,
                              'semester_number': semester_number
                          })

    return render(request, 'timetable/view_previous_timetable.html',
                  {
                      'batch_list': batch_list,
                      'department_list': department_list
                  })
