from django.http import JsonResponse
from django.shortcuts import render

from accounts.models import department, batch
from curriculum.models import semester, courses

from common.API.departmentAPI import getCoreDepartments


def view_previous_attendance_reports(request):
    department_list = getCoreDepartments()
    batch_list = batch.objects.all()
    if request.method == 'POST':
        if request.is_ajax():
            if request.POST.get('action') == 'get_semester_plans':
                dictionary = {}
                chosen_batch = str(request.POST.get('batch'))
                batch_instance = batch.objects.get(id=chosen_batch)
                department_id = request.POST.get('department')

                department_instance = department.objects.get(id=department_id)
                print(locals())
                available_semesters = semester.objects.filter(
                    batch=batch_instance
                ).filter(
                    department=department_instance
                )
                serialized_available_semesters = []
                for sem_inst in available_semesters:
                    temp = {}
                    temp['semester_pk'] = sem_inst.pk
                    temp['semester_display_text'] = str(sem_inst.batch.programme) + ' SEM ' + str(
                        sem_inst.semester_number)
                    serialized_available_semesters.append(temp)
                dictionary['available_semesters'] = serialized_available_semesters
                print(dictionary)
                return JsonResponse(dictionary)

            if request.POST.get('action') == 'get_courses':
                department_id = request.POST.get('department')
                batch_id = request.POST.get('batch')
                semester_pk = request.POST.get('semester_pk')
                department_instance = department.objects.get(id=department_id)

                print(locals())
                batch_instance = batch.objects.get(id=batch_id)
                batch_programme = batch_instance.programme
                regulation_instance = batch_instance.regulation

                semester_instance = semester.objects.get(pk=semester_pk)
                course_instances = courses.objects.filter(
                    department=semester_instance.department
                ).filter(
                    regulation=regulation_instance
                ).filter(
                    programme=batch_programme
                ).filter(
                    semester=semester_instance.semester_number
                )

                list_of_available_courses = []

                for course_entry in course_instances:
                    temp = {}
                    temp['course_pk'] = course_entry.pk
                    temp['course_id'] = course_entry.course_id
                    temp['course_name'] = course_entry.course_name
                    list_of_available_courses.append(temp)

                print(list_of_available_courses)

                data = {}
                data['list_of_courses'] = list_of_available_courses

                print(data)

                return JsonResponse(data)

        if 'semester_number' in request.POST:
            department_id = request.POST.get('department')
            batch_id = request.POST.get('batch_id')
            semester_number = request.POST.get('semester_number')

            department_instance = department.objects.get(id=department_id)

            batch_instance = batch.objects.get(id=batch_id)

            try:
                semester_instance = semester.objects.get(batch=batch_instance, semester_number=semester_number,
                                                         department=department_instance)


            except:
                return render(request, 'timetable/view_previous_attendance_report.html',
                              {
                                  'department_list': department_list,
                                  'department_instance': department_instance,
                                  'batch_instance': batch_instance,
                                  'semester_number': semester_number,
                                  'batch_list': batch_list,
                                  'no_attendance': True
                              })

    return render(request, 'timetable/view_previous_attendance_report.html',
                  {
                      'batch_list': batch_list,
                      'department_list': department_list
                  })
