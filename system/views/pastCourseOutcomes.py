from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import render
from googleapiclient.errors import BatchError

from accounts.models import department, batch
from courseOutcomes.views import getCoMarksOfUnitTest
from curriculum.models import semester, staff_course
from common.utils.ReportTabsUtil import getStaffCoursesForSemester
from common.API.departmentAPI import getDepartmentOfPk, getCoreDepartments


@login_required
@permission_required('curriculum.can_mark_attendance')
def pastCoMarksReport(request):
    departmentList = getCoreDepartments()
    batchList = batch.objects.all()
    if request.method == 'POST':
        if request.is_ajax():
            if request.POST.get('action') == 'getSemesterPlan':
                batchPk = int(request.POST.get('batchPk'))
                departmentPk = int(request.POST.get('departmentPk'))
                batchInstance = batch.objects.get(pk=batchPk)
                departmentInstance = getDepartmentOfPk(departmentPk)
                semesterList = []
                for eachSemester in semester.objects.filter(department=departmentInstance, batch=batchInstance):
                    temp = {}
                    temp['displayText'] = eachSemester.getDisplayTextForPastReport()
                    temp['pk'] = eachSemester.pk
                    semesterList.append(temp)

                dictionary = {}
                dictionary['semesterList'] = semesterList
                return JsonResponse(dictionary)

            if request.POST.get('action') == 'getSemesterPlan':
                departmentPk = int(request.POST.get('departmentPk'))
                departmentInstance = getDepartmentOfPk(departmentPk)
                semesterList = []
                for eachSemester in semester.objects.filter(department=departmentInstance):
                    temp = {}
                    temp['displayText'] = eachSemester.getDisplayTextForPastReport()
                    temp['pk'] = eachSemester.pk
                    semesterList.append(temp)

                dictionary = {}
                dictionary['semesterList'] = semesterList

                return JsonResponse(dictionary)

            if request.POST.get('action') == 'getCourses':
                semesterPk = request.POST.get('semesterPk')
                semesterInstance = semester.objects.get(pk=semesterPk)

                courseList = []

                for eachCourse in getStaffCoursesForSemester(semesterInstance):
                    staffCourseInstance = eachCourse['staffCourseInstance']
                    temp = {}
                    temp['pk'] = staffCourseInstance.pk
                    temp['course'] = str(staffCourseInstance.course)
                    courseList.append(temp)

                dictionary = {}
                dictionary['courseList'] = courseList

                return JsonResponse(dictionary)

        if 'staffCoursePkOnCoRequest' in request.POST:
            staffCoursePk = request.POST.get('staffCoursePkOnCoRequest')
            unitTestNumber = int(request.POST.get('unitTestNumberOnCoRequest'))

            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getCoMarksOfUnitTest(staffCourseInstance, unitTestNumber)

            return render(request, 'pastCourseOutcomes/pastCoMarksReport.html',
                          {
                              'staffCourseInstance': staffCourseInstance,
                              'unitTestNumber': unitTestNumber,
                              'dictionary': dictionary,
                              'batchList': batchList,
                              'departmentList': departmentList
                          })

    return render(request, 'pastCourseOutcomes/pastCoMarksReport.html',
                  {
                      'batchList': batchList,
                      'departmentList': departmentList
                  })
