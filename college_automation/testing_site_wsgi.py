import os
import site
import sys

site.addsitedir('/home/ubuntu/ca/envs/ca_testing_site/lib/python3.4/site-packages')

sys.path.append('/home/ubuntu/ca/ca_testing_site/college_automation/')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "college_automation.settings_testing_port")

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
