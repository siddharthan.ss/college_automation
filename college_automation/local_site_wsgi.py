import os
import site
import sys

site.addsitedir('/home/sid/envs/ca/lib/python3.5/site-packages')
sys.path.append('/home/sid/envs/ca/lib/python3.5/site-packages')
site.addsitedir('/home/sid/gctporatal_site/master_march_27/college_automation')
sys.path.append('/home/sid/gctporatal_site/master_march_27/college_automation')

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "college_automation.settings")

application = get_wsgi_application()
