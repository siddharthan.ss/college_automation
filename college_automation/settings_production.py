from .settings import *

DEBUG = False

ALLOWED_HOSTS = ['*']
ADMINS = (
    ('Siddharthan', 'siddharthan.ss@gmail.com'),
    ('Harsha Vardhan', 'harshaktg@gmail.com'),
    ('Muthu Bai', 'muthamil.ons@gmail.com'),
    ('Muthukumaran', 'muthukumaranvgct2016@gmail.com'),
)

MANAGERS = (
    ('Siddharthan', 'siddharthan.ss@gmail.com'),
    ('Harsha Vardhan', 'harshaktg@gmail.com'),
    ('Muthu Bai', 'muthamil.ons@gmail.com'),
    ('Muthukumaran', 'muthukumaranvgct2016@gmail.com'),
)

import configparser

config = configparser.ConfigParser()

config.read(os.path.join(BASE_DIR, 'config.ini'))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': config['PRODUCTION']['PRODUTION_DB_NAME'],
        'USER': config['PRODUCTION']['PRODUTION_DB_USER_NAME'],
        'PASSWORD': config['PRODUCTION']['PRODUTION_DB_PASSWORD'],
        'HOST': config['PRODUCTION']['PRODUTION_DB_HOST'],
        'PORT': config['PRODUCTION']['PRODUTION_DB_PORT'],
        'OPTIONS': {'init_command': "SET sql_mode='STRICT_TRANS_TABLES'"}
    }
}

CURRENT_HOST_NAME = 'https://gctportal.com/'
HIDE_THIS_FEATURE = False

EMAIL_BACKEND = 'accounts.backends.MyEmailCounterBackend'

ONESIGNAL_APP_API_ID = '5f004f57-c64f-48b1-b9ae-8e6cb2646953'
ONESIGNAL_REST_API_KEY = 'OTZmMTkwMTUtZmRjNy00NGJiLWE3ODItMGE5YzQxNDNmZGUy'