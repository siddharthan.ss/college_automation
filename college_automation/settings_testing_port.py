from .settings import *

DEBUG = True

ALLOWED_HOSTS = ['*']
ADMINS = (
    ('Siddharthan', 'siddharthan.ss@gmail.com'),
    ('Harsha Vardhan', 'harshaktg@gmail.com'),
    ('Muthu Bai', 'muthamil.ons@gmail.com'),
    ('Muthukumaran', 'muthukumaranvgct2016@gmail.com'),
)

MANAGERS = (
    ('Siddharthan', 'siddharthan.ss@gmail.com'),
    ('Harsha Vardhan', 'harshaktg@gmail.com'),
    ('Muthu Bai', 'muthamil.ons@gmail.com'),
    ('Muthukumaran', 'muthukumaranvgct2016@gmail.com'),
)

import configparser

config = configparser.ConfigParser()

config.read(os.path.join(BASE_DIR, 'config.ini'))

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': config['PRODUCTION']['PRODUTION_DB_NAME'],
        'USER': config['PRODUCTION']['PRODUTION_DB_USER_NAME'],
        'PASSWORD': config['PRODUCTION']['PRODUTION_DB_PASSWORD'],
        'HOST': config['PRODUCTION']['PRODUTION_DB_HOST'],
        'PORT': config['PRODUCTION']['PRODUTION_DB_PORT'],
        'OPTIONS': {'init_command': "SET sql_mode='STRICT_TRANS_TABLES'"}
    }
}

CURRENT_HOST_NAME = 'http://www.gctportal.tk:5000/'
EMAIL_BACKEND = 'accounts.backends.MyEmailCounterBackend'
