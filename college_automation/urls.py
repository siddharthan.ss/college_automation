"""college_automation URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

import notifications.urls
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include("accounts.urls")),
    url(r'^', include("curriculum.urls")),
    url(r'^', include("departmentManagement.urls")),
    url(r'^', include("messaging.urls")),
    url(r'^', include("marks.urls")),
    url(r'^', include("quiz.urls")),
    url(r'^', include("enrollment.urls")),
    url(r'^', include("assignment.urls")),
    url(r'^', include("feedback.urls")),
    url(r'^', include("courseOutcomes.urls")),
    url(r'^dashboard/', include("dashboard.urls")),
    url(r'^developers/', include("developers.urls")),
    url(r'^wifi/', include("wifi.urls")),
    url(r'^', include("hostel.urls")),
    url(r'^system/', include("system.urls")),
    url(r'^alumni/', include("alumni.urls")),
    url(r'^attendance/', include("attendance.urls")),
    url('^inbox/notifications/', include(notifications.urls, namespace='notifications')),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^integrations/', include('integrations.urls', namespace='integrations')),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
