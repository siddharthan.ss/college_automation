from django.db import models

from accounts.models import student
from curriculum.models import courses, semester


# class courses_offered(models.Model):
#     course = models.ForeignKey(courses)
#     semester_duration = models.ForeignKey(semester_duration)
#     is_active = models.BooleanField(default=True)
#
#     class Meta:
#         unique_together = (
#             'course', 'semester_duration'
#         )


class course_enrollment(models.Model):
    course = models.ForeignKey(courses)
    semester = models.ForeignKey(semester)
    student = models.ForeignKey(student)
    requested = models.BooleanField(default=False)
    enrolled_date = models.DateField()
    registered = models.BooleanField(default=False)
    registered_date = models.DateField(blank=True, null=True)
    approved = models.BooleanField(default=False)

    class Meta:
        unique_together = (
            'course', 'semester', 'student'
        )


class addup_courses(models.Model):
    course = models.ForeignKey(courses)
    semester = models.ForeignKey(semester)
    student = models.ForeignKey(student)
