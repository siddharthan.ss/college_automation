from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from enrollment.views.available_courses import courses_enrollment, enrollmentPDFreport, registrationPDFreport, enable_courses_for_student_enrollment
from enrollment.views.course_approval import approve_courses, deapprove_courses
from enrollment.views.enroll_students import enroll_students
from enrollment.views.enrollmentReports import enrollmentReport, registrationReport

urlpatterns = [
    # url(r'^courses_for_enrollment/$', select_courses_available_for_enrollment, name="courses_for_enrollment"),
    url(r'^course_enrollment/$', courses_enrollment, name="course_enrollment"),
    url(r'^approve_courses/$', approve_courses, name="approve_courses"),
    url(r'^deapprove_courses/$', deapprove_courses, name="deapprove_courses"),

    url(r'^enrollment_form_pdf_view/$', login_required(enrollmentPDFreport.as_view()), name='enrollment_pdf'),
    url(r'^registration_form_pdf_view/$', login_required(registrationPDFreport.as_view()), name='registration_pdf'),

    url(r'^enrollment_report/$', enrollmentReport, name='enrollmentReport'),
    url(r'^registration_report/$', registrationReport, name='registrationReport'),

    url(r'^enroll_students/$', enroll_students, name='enroll_students'),

    url(r'^enable_courses/$', enable_courses_for_student_enrollment, name='enable_courses'),
]
