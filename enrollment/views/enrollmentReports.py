from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from accounts.models import staff, department
from curriculum.models import semester, staff_course, student_enrolled_courses
from enrollment.models import course_enrollment

from common.utils.ReportTabsUtil import getActiveDepartmentsForStaff, getActiveSemesterTabsForStaff, \
    getActiveCoursesTabsForStaff

from common.API.departmentAPI import getDepartmentOfPk


@login_required
@permission_required('enrollment.can_view_enrollment_details')
def enrollmentReport(request):
    staffInstance = staff.objects.get(user=request.user)
    departmentList = getActiveDepartmentsForStaff(staffInstance)

    if not departmentList:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact Programme co-ordinator to ensure whether courses are properly alloted',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    departmentInstance = department.objects.get(pk=departmentList[0].pk)

    if request.method == 'POST':
        if 'departmentPk' in request.POST:
            departmentPk = int(request.POST.get('departmentPk'))
            departmentInstance = getDepartmentOfPk(departmentPk)

        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            departmentInstance = semesterInstance.department

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            departmentInstance = staffCourseInstance.department

    semesterList = getActiveSemesterTabsForStaff(staffInstance, departmentInstance)

    if not semesterList:
        return render(request, 'enrollment/enrollmentReports.html',
                      {
                          'noSemester': True,
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                      })

    semesterInstance = semesterList[0]['semesterInstance']

    if request.method == 'POST':
        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            departmentInstance = semesterInstance.department

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            semesterInstance = staffCourseInstance.semester
            departmentInstance = staffCourseInstance.department

    courseList = getActiveCoursesTabsForStaff(staffInstance, semesterInstance)

    oldRegulation = False
    if semesterInstance.batch.regulation.start_year == 2012:
        electiveCourseList = []
        for eachCourse in courseList:
            if eachCourse['staffCourseInstance'].course.is_elective or eachCourse[
                'staffCourseInstance'].course.is_one_credit or eachCourse['staffCourseInstance'].course.is_open:
                electiveCourseList.append(eachCourse)
        courseList = electiveCourseList
        oldRegulation = True

    try:
        if not 'staffCoursePk' in request.POST:
            staffCoursePk = courseList[0]['staffCourseInstance'].pk
    except:
        return render(request, 'enrollment/enrollmentReports.html',
                      {
                          'noCourses': True,
                          'oldRegulation': oldRegulation,
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                          'semesterInstance': semesterInstance,
                          'semesterList': semesterList,
                      })

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    semesterInstance = staffCourseInstance.semester
    courseInstance = staffCourseInstance.course

    oldRegulation = False

    if staffCourseInstance.course.regulation.start_year >= 2016:
        studentList = course_enrollment.objects.filter(semester=semesterInstance, course=courseInstance,
                                                       requested=True).order_by('student__roll_no')
    else:
        studentList = student_enrolled_courses.objects.filter(studied_semester=semesterInstance,
                                                              request_course=True, approved=False,
                                                              course=courseInstance).order_by('student__roll_no')

        oldRegulation = True

    return render(request, 'enrollment/enrollmentReports.html',
                  {
                      'departmentList': departmentList,
                      'departmentInstance': departmentInstance,
                      'semesterInstance': semesterInstance,
                      'semesterList': semesterList,
                      'staffCourseInstance': staffCourseInstance,
                      'courseList': courseList,
                      'studentList': studentList,
                      'oldRegulation': oldRegulation
                  })


@login_required
@permission_required('enrollment.can_view_enrollment_details')
def registrationReport(request):
    staffInstance = staff.objects.get(user=request.user)
    departmentList = getActiveDepartmentsForStaff(staffInstance)

    if not departmentList:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact Programme co-ordinator to ensure whether courses are properly alloted',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    departmentInstance = department.objects.get(pk=departmentList[0].pk)

    if request.method == 'POST':
        if 'departmentPk' in request.POST:
            departmentPk = int(request.POST.get('departmentPk'))
            departmentInstance = getDepartmentOfPk(departmentPk)

        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            departmentInstance = semesterInstance.department

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            departmentInstance = staffCourseInstance.department

    semesterList = getActiveSemesterTabsForStaff(staffInstance, departmentInstance)

    if not semesterList:
        return render(request, 'enrollment/registrationReports.html',
                      {
                          'noSemester': True,
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                      })

    semesterInstance = semesterList[0]['semesterInstance']

    if request.method == 'POST':
        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            departmentInstance = semesterInstance.department

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            semesterInstance = staffCourseInstance.semester
            departmentInstance = staffCourseInstance.department

    courseList = getActiveCoursesTabsForStaff(staffInstance, semesterInstance)

    oldRegulation = False
    if semesterInstance.batch.regulation.start_year == 2012:
        electiveCourseList = []
        for eachCourse in courseList:
            if eachCourse['staffCourseInstance'].course.is_elective or eachCourse[
                'staffCourseInstance'].course.is_one_credit or eachCourse['staffCourseInstance'].course.is_open:
                electiveCourseList.append(eachCourse)
        courseList = electiveCourseList
        oldRegulation = True

    try:
        if not 'staffCoursePk' in request.POST:
            staffCoursePk = courseList[0]['staffCourseInstance'].pk
    except:
        return render(request, 'enrollment/registrationReports.html',
                      {
                          'noCourses': True,
                          'oldRegulation': oldRegulation,
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                          'semesterInstance': semesterInstance,
                          'semesterList': semesterList,
                      })

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    semesterInstance = staffCourseInstance.semester
    courseInstance = staffCourseInstance.course

    oldRegulation = False

    if staffCourseInstance.course.regulation.start_year >= 2016:
        studentList = course_enrollment.objects.filter(semester=semesterInstance, course=courseInstance,
                                                       registered=True).order_by('student__roll_no')
    else:
        studentList = student_enrolled_courses.objects.filter(studied_semester=semesterInstance,
                                                              request_course=True, approved=True,
                                                              course=courseInstance).order_by('student__roll_no')

        oldRegulation = True

    return render(request, 'enrollment/registrationReports.html',
                  {
                      'departmentList': departmentList,
                      'departmentInstance': departmentInstance,
                      'semesterInstance': semesterInstance,
                      'semesterList': semesterList,
                      'staffCourseInstance': staffCourseInstance,
                      'courseList': courseList,
                      'studentList': studentList,
                      'oldRegulation': oldRegulation
                  })
