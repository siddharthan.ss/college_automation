from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from accounts.models import staff, student
from curriculum.models import staff_course, student_enrolled_courses
from enrollment.models import course_enrollment
from curriculum.views.common_includes import get_current_course_allotment_with_display_text


@login_required
@permission_required('curriculum.can_approve_students_for_courses')
def approve_courses(request):
    faculty_instance = staff.objects.get(user=request.user)
    course_list_objects = get_current_course_allotment_with_display_text(faculty_instance)
    # pprint(course_list_objects)

    course_list = []

    for each in course_list_objects:
        if each['staff_course_instance'].course.regulation.start_year >= 2016:
            course_list.append(each)
        elif each['staff_course_instance'].course.is_elective or each['staff_course_instance'].course.is_one_credit:
            course_list.append(each)

    no_of_students_approved = 0
    no_of_students_deleted = 0

    try:
        selected_course = course_list[0]['staff_course_instance'].pk
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course. For practicals click on Set Lab Pattern from the side pane to set pattern for practicals and then click on Add Lab Marks to add marks',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'subject_code' in request.POST:
            selected_course = request.POST.get('subject_code')

        if 'approve_students' in request.POST:
            students_approve_list = request.POST.getlist('approve')
            selected_course = request.POST.get('hidden_course')
            staff_course_instance = staff_course.objects.get(pk=selected_course)
            regulation_start_year = staff_course_instance.course.regulation.start_year
            if regulation_start_year >= 2016:
                for each_student in students_approve_list:
                    student_instance = student.objects.get(roll_no=each_student)
                    instance = course_enrollment.objects.get(course=staff_course_instance.course,
                                                             student=student_instance)
                    instance.approved = True
                    instance.save()
            else:
                for each_student in students_approve_list:
                    student_instance = student.objects.get(roll_no=each_student)
                    instance = student_enrolled_courses.objects.get(student=student_instance,
                                                                    course=staff_course_instance.course)
                    instance.approved = True
                    instance.save()

            no_of_students_approved = len(students_approve_list)

        if 'delete_students' in request.POST:
            students_approve_list = request.POST.getlist('approve')
            selected_course = request.POST.get('hidden_course')
            staff_course_instance = staff_course.objects.get(pk=selected_course)
            regulation_start_year = staff_course_instance.course.regulation.start_year
            if regulation_start_year >= 2016:
                for each_student in students_approve_list:
                    student_instance = student.objects.get(roll_no=each_student)
                    course_enrollment.objects.get(course=staff_course_instance.course,
                                                  semester=staff_course_instance.semester,
                                                  student=student_instance).delete()
            else:
                for each_student in students_approve_list:
                    student_instance = student.objects.get(roll_no=each_student)
                    student_enrolled_courses.objects.get(student=student_instance,
                                                         studied_semester=staff_course_instance.semester,
                                                         course=staff_course_instance.course).delete()

            no_of_students_deleted = len(students_approve_list)

    staff_course_instance = staff_course.objects.get(pk=selected_course)
    course_instance = staff_course_instance.course
    semester_instance = staff_course_instance.semester

    regulation_start_year = course_instance.regulation.start_year

    if regulation_start_year >= 2016:
        list_of_students = course_enrollment.objects.filter(semester=semester_instance, course=course_instance,
                                                            approved=False, requested=True,
                                                            registered=True).order_by('student__roll_no')
    else:
        list_of_students = student_enrolled_courses.objects.filter(studied_semester=semester_instance, approved=False,
                                                                   course=course_instance).order_by('student__roll_no')

    return render(request, 'enrollment/approve_courses.html',
                  {
                      'selected_course': int(selected_course),
                      'course_list': course_list,
                      'list_of_students': list_of_students,
                      'no_of_students_approved': no_of_students_approved,
                      'no_of_students_deleted': no_of_students_deleted
                      # 'subject_total_marks': subject_total_marks
                  })


@login_required
@permission_required('curriculum.can_approve_students_for_courses')
def deapprove_courses(request):
    faculty_instance = staff.objects.get(user=request.user)
    course_list_objects = get_current_course_allotment_with_display_text(faculty_instance)
    # pprint(course_list_objects)

    course_list = []

    for each in course_list_objects:
        if each['staff_course_instance'].course.regulation.start_year >= 2016:
            course_list.append(each)
        elif each['staff_course_instance'].course.is_elective or each['staff_course_instance'].course.is_one_credit:
            course_list.append(each)

    no_of_students_deapproved = 0

    try:
        selected_course = course_list[0]['staff_course_instance'].pk
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course. For practicals click on Set Lab Pattern from the side pane to set pattern for practicals and then click on Add Lab Marks to add marks',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'subject_code' in request.POST:
            selected_course = request.POST.get('subject_code')

        if 'deapprove_students' in request.POST:
            students_approve_list = request.POST.getlist('approve')
            selected_course = request.POST.get('hidden_course')
            staff_course_instance = staff_course.objects.get(pk=selected_course)
            regulation_start_year = staff_course_instance.course.regulation.start_year
            if regulation_start_year >= 2016:
                for each_student in students_approve_list:
                    student_instance = student.objects.get(roll_no=each_student)
                    instance = course_enrollment.objects.get(course=staff_course_instance.course,
                                                             semester=staff_course_instance.semester,
                                                             student=student_instance)
                    instance.approved = False
                    instance.save()
            else:
                for each_student in students_approve_list:
                    student_instance = student.objects.get(roll_no=each_student)
                    instance = student_enrolled_courses.objects.get(student=student_instance,
                                                                    studied_semester=staff_course_instance.semester,
                                                                    course=staff_course_instance.course)
                    instance.approved = False
                    instance.save()

            no_of_students_deapproved = len(students_approve_list)

    staff_course_instance = staff_course.objects.get(pk=selected_course)
    course_instance = staff_course_instance.course
    semester_instance = staff_course_instance.semester

    regulation_start_year = course_instance.regulation.start_year

    if regulation_start_year >= 2016:
        list_of_students = course_enrollment.objects.filter(semester=semester_instance, course=course_instance,
                                                            approved=True).order_by('student__roll_no')
    else:
        list_of_students = student_enrolled_courses.objects.filter(studied_semester=semester_instance, approved=True,
                                                                   course=course_instance).order_by('student__roll_no')

    return render(request, 'enrollment/deapprove_courses.html',
                  {
                      'selected_course': int(selected_course),
                      'course_list': course_list,
                      'list_of_students': list_of_students,
                      'no_of_students_deapproved': no_of_students_deapproved,
                      # 'subject_total_marks': subject_total_marks
                  })
