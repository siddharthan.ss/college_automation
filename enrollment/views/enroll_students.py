from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render
from django.utils import timezone

from accounts.models import department, batch, student
from curriculum.models import regulation, semester, courses
from enrollment.models import course_enrollment

from common.utils.studentUtil import getSemesterPlanofStudent


@login_required
@permission_required('enrollment.can_enroll_students')
def enroll_students(request):
    all_departments = department.objects.filter(is_core=True)
    semester_list = [1, 2]

    regulation_instance = regulation.objects.get(start_year=2016)
    batch_list = batch.objects.filter(regulation=regulation_instance)

    selected_department = all_departments.first()
    selected_department_pk = selected_department.pk

    selected_semester_number = semester_list[0]

    student_roll_no = 0

    if request.method == "POST":
        if 'selected_department_pk' in request.POST:
            selected_department_pk = int(request.POST.get('selected_department_pk'))

        if 'selected_semester_number' in request.POST:
            selected_semester_number = int(request.POST.get('selected_semester_number'))
            selected_department_pk = int(request.POST.get('selected_department_pk'))

        if 'student_regno' in request.POST:
            student_roll_no = request.POST.get('student_regno')
            selected_semester_number = int(request.POST.get('hidden_semester_number'))
            selected_department_pk = int(request.POST.get('hidden_department_pk'))

        if 'course_enroll' in request.POST:
            selected_semester_number = int(request.POST.get('hidden_semester_number'))
            selected_department_pk = int(request.POST.get('hidden_department_pk'))
            student_roll_no = request.POST.get('hidden_student_regno')
            student_instance = student.objects.get(roll_no=student_roll_no)
            selected_courses_list = request.POST.getlist('course_enroll')
            selected_course = selected_courses_list[0]
            semester_instance = semester.objects.get(batch=student_instance.batch,
                                                     department=student_instance.department,
                                                     semester_number=selected_semester_number)
            course_instance = courses.objects.get(pk=selected_course)

            if course_enrollment.objects.filter(semester=semester_instance, course=course_instance,
                                                student=student_instance):
                instance = course_enrollment.objects.get(semester=semester_instance,
                                                         course=course_instance, student=student_instance)
                instance.requested = True
                instance.enrolled_date = timezone.now()
                instance.save()
            else:
                instance = course_enrollment(semester=semester_instance, course=course_instance,
                                             student=student_instance, requested=True, enrolled_date=timezone.now())
                instance.save()

        if 'course_register' in request.POST:
            selected_semester_number = int(request.POST.get('hidden_semester_number'))
            selected_department_pk = int(request.POST.get('hidden_department_pk'))
            student_roll_no = request.POST.get('hidden_student_regno')
            student_instance = student.objects.get(roll_no=student_roll_no)
            selected_courses_list = request.POST.getlist('course_register')
            selected_course = selected_courses_list[0]
            semester_instance = semester.objects.get(batch=student_instance.batch,
                                                     department=student_instance.department,
                                                     semester_number=selected_semester_number)
            course_instance = courses.objects.get(pk=selected_course)

            if course_enrollment.objects.filter(semester=semester_instance, course=course_instance,
                                                student=student_instance, requested=True):
                instance = course_enrollment.objects.get(semester=semester_instance,
                                                         course=course_instance, student=student_instance,
                                                         requested=True)
                instance.registered = True
                instance.registered_date = timezone.now()

                instance.approved = True

                instance.save()

        if 'course_delete' in request.POST:
            selected_semester_number = int(request.POST.get('hidden_semester_number'))
            selected_department_pk = int(request.POST.get('hidden_department_pk'))
            student_roll_no = request.POST.get('hidden_student_regno')
            student_instance = student.objects.get(roll_no=student_roll_no)
            selected_courses_list = request.POST.get('course_delete')
            selected_course = selected_courses_list
            semester_instance = semester.objects.get(batch=student_instance.batch,
                                                     department=student_instance.department,
                                                     semester_number=selected_semester_number)
            course_instance = courses.objects.get(pk=selected_course)

            if course_enrollment.objects.filter(semester=semester_instance, course=course_instance,
                                                student=student_instance):
                course_enrollment.objects.get(semester=semester_instance,
                                              course=course_instance, student=student_instance).delete()

    department_instance = department.objects.get(pk=selected_department_pk)

    list_of_students = student.objects.filter(batch__in=batch_list, department=department_instance,
                                              user__is_approved=True).order_by('roll_no')

    list_of_courses = []

    student_instance = None

    selected_semester_list = [1]

    if selected_semester_number == 2:
        selected_semester_list.append(2)

    course_list = []
    available_courses = []
    list_of_requested_courses = []
    list_of_additional_courses = []

    list_of_enrolled_courses = []
    list_of_registered_courses = []
    list_of_approved_courses = []

    if student_roll_no != 0:
        student_instance = student.objects.get(roll_no=student_roll_no)

        semester_instance = getSemesterPlanofStudent(student_instance)

        for each in courses.objects.filter(semester__in=selected_semester_list,
                                           department=student_instance.department,
                                           regulation=regulation_instance,
                                           programme=student_instance.qualification).order_by(
            'course_id'):
            available_courses.append(each)

        for each in course_enrollment.objects.filter(semester=semester_instance, student=student_instance,
                                                     requested=True).order_by('-course__semester'):
            course_list.append(each.course)
            if each.registered:
                list_of_registered_courses.append(each.course)
            elif each.requested:
                list_of_enrolled_courses.append(each.course)

            if each.approved:
                list_of_approved_courses.append(each.course)

            list_of_requested_courses.append(each)

        available_courses.sort(key=lambda x: x.course_id[-1])

        for each in available_courses:
            temp = {}
            temp['data'] = each
            if each in list_of_registered_courses:
                temp['registered'] = True
                if each in list_of_approved_courses:
                    temp['approved'] = True
            elif each in list_of_enrolled_courses:
                temp['enrolled'] = True

            if selected_semester_number == 2:
                if each.semester == 2:
                    list_of_courses.append(temp)
                elif each.semester == 1:
                    list_of_additional_courses.append(temp)
            elif selected_semester_number == 1:
                list_of_courses.append(temp)


    return render(request, 'enrollment/enroll_students.html',
                  {
                      'all_departments': all_departments,
                      'selected_department_pk': int(selected_department_pk),
                      'semester_list': semester_list,
                      'selected_semester_number': selected_semester_number,
                      'list_of_students': list_of_students,
                      'student_roll_no': student_roll_no,
                      'student_instance': student_instance,
                      'list_of_courses': list_of_courses,
                      'list_of_additional_courses': list_of_additional_courses
                  })
