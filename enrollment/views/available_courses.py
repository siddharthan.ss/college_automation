from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, redirect
from django.utils import timezone
from django.http import JsonResponse
from easy_pdf.views import PDFTemplateView

from accounts.models import staff, student, department
from curriculum.models import semester_migration, semester, courses, department_sections, section_students
from enrollment.models import addup_courses, course_enrollment

from curriculum.views.common_includes import get_active_semesters_tabs
from common.utils.studentUtil import getSemesterPlanofStudent
from common.utils.ReportTabsUtil import getDepartmentsOfPc, getDepartmentsForFacultyAdvisor


@login_required
@permission_required('enrollment.can_enable_courses')
def enable_courses_for_student_enrollment(request):
    staff_instance = staff.objects.get(user__email=request.user)
    if request.method == 'POST':
        if request.is_ajax():
            if 'department' in request.POST:
                dictionary = {}

                chosen_department = str(request.POST.get('department'))

                department_instance = department.objects.get(pk=chosen_department)

                avaliable_semesters = get_active_semesters_tabs(request)

                if request.user.groups.filter(name='faculty_advisor').exists() and not \
                        request.user.groups.filter(name='hod').exists() and not \
                        request.user.groups.filter(
                            name='programme_coordinator').exists():
                    new_avaliable_semesters = []
                    semesters_as_fa = semester_migration.objects.filter(
                        semester__faculty_advisor=staff_instance
                    )
                    for entry in semesters_as_fa:
                        new_avaliable_semesters.append(entry.semester)

                    final_semesters = []

                    for entry in avaliable_semesters:
                        sem_inst = semester.objects.get(pk=entry['semester_instance'].pk)
                        if sem_inst in new_avaliable_semesters:
                            final_semesters.append(entry)

                    if final_semesters:
                        avaliable_semesters = final_semesters

                list = []

                for each in avaliable_semesters:
                    if each['semester_instance'].batch.regulation.start_year >= 2016:
                        temp = {}
                        temp['display_batch'] = str(each['display_text'])
                        temp['batch_obj'] = str(each['semester_instance'].batch.pk)
                        list.append(temp)

                dictionary['list'] = list

                return JsonResponse(dictionary)

            if 'year' in request.POST:
                dictionary = {}

                chosen_year = str(request.POST.get('year'))
                chosen_dept = str(request.POST.get('dept'))

                list = []

                for each in student.objects.filter(department_id=chosen_dept, batch=chosen_year,
                                                   user__is_approved=True).order_by(
                    'roll_no'):
                    temp = {}
                    temp['name'] = str(each)
                    temp['regno'] = str(each.roll_no)
                    list.append(temp)

                dictionary['list'] = list

                return JsonResponse(dictionary)

            if 'student' in request.POST:
                dictionary = {}

                chosen_year = str(request.POST.get('hidden_year'))
                chosen_dept = str(request.POST.get('hidden_dept'))
                chosen_student_rollno = str(request.POST.get('student'))

                # batch_instance = batch.objects.get(pk=chosen_year)
                department_instance = department.objects.get(pk=chosen_dept)
                student_instance = student.objects.get(roll_no=chosen_student_rollno)

                semester_instance = getSemesterPlanofStudent(student_instance)

                if not semester_instance:
                    dictionary['student'] = str(student_instance.roll_no) + ' - ' + str(student_instance)
                    dictionary['no_sem_plan'] = 'true'
                    return JsonResponse(dictionary)

                regulation_instance = student_instance.batch.regulation

                list = []
                list_offered = []

                for each in courses.objects.filter(common_course=False, department=department_instance,
                                                   regulation=regulation_instance,
                                                   semester__lt=semester_instance.semester_number).order_by(
                    'semester'):
                    temp = {}
                    temp['semester'] = str(each.semester)
                    temp['pk'] = each.pk
                    temp['course_id'] = str(each.course_id)
                    temp['course_name'] = str(each.course_name)
                    if not addup_courses.objects.filter(course=each, student=student_instance,
                                                        semester=semester_instance):
                        list.append(temp)
                    else:
                        list_offered.append(temp)

                dictionary['list'] = list
                dictionary['list_offered'] = list_offered
                dictionary['semester_pk'] = semester_instance.pk

                return JsonResponse(dictionary)

            if 'course_id' in request.POST:
                dictionary = {}

                chosen_year = str(request.POST.get('hidden_year'))
                chosen_dept = str(request.POST.get('hidden_dept'))
                chosen_student_rollno = str(request.POST.get('hidden_student'))
                chosen_course_id = str(request.POST.get('course_id'))

                course_instance = courses.objects.get(pk=chosen_course_id)

                # batch_instance = batch.objects.get(pk=chosen_year)
                department_instance = department.objects.get(pk=chosen_dept)
                student_instance = student.objects.get(roll_no=chosen_student_rollno)

                semester_instance = getSemesterPlanofStudent(student_instance)

                regulation_instance = student_instance.batch.regulation

                if not addup_courses.objects.filter(semester=semester_instance, student=student_instance,
                                                    course=course_instance):
                    addup_courses_instance = addup_courses(semester=semester_instance, student=student_instance,
                                                           course=course_instance)
                    addup_courses_instance.save()

                list = []
                list_offered = []

                for each in courses.objects.filter(common_course=False, department=department_instance,
                                                   regulation=regulation_instance,
                                                   semester__lt=semester_instance.semester_number).order_by('semester'):
                    temp = {}
                    temp['semester'] = str(each.semester)
                    temp['pk'] = each.pk
                    temp['course_id'] = str(each.course_id)
                    temp['course_name'] = str(each.course_name)
                    if not addup_courses.objects.filter(course=each, student=student_instance,
                                                        semester=semester_instance):
                        list.append(temp)
                    else:
                        list_offered.append(temp)

                dictionary['list'] = list
                dictionary['list_offered'] = list_offered
                dictionary['semester_pk'] = semester_instance.pk

                return JsonResponse(dictionary)

            if 'delete_course_id' in request.POST:
                dictionary = {}

                chosen_year = str(request.POST.get('hidden_year'))
                chosen_dept = str(request.POST.get('hidden_dept'))
                chosen_student_rollno = str(request.POST.get('hidden_student'))
                chosen_course_id = str(request.POST.get('delete_course_id'))

                course_instance = courses.objects.get(pk=chosen_course_id)

                # batch_instance = batch.objects.get(pk=chosen_year)
                department_instance = department.objects.get(pk=chosen_dept)
                student_instance = student.objects.get(roll_no=chosen_student_rollno)

                semester_instance = getSemesterPlanofStudent(student_instance)

                regulation_instance = student_instance.batch.regulation

                if addup_courses.objects.filter(semester=semester_instance, student=student_instance,
                                                course=course_instance):
                    addup_courses.objects.get(semester=semester_instance, student=student_instance,
                                              course=course_instance).delete()

                list = []
                list_offered = []

                for each in courses.objects.filter(common_course=False, department=department_instance,
                                                   regulation=regulation_instance,
                                                   semester__lt=semester_instance.semester_number).order_by('semester'):
                    temp = {}
                    temp['semester'] = str(each.semester)
                    temp['pk'] = each.pk
                    temp['course_id'] = str(each.course_id)
                    temp['course_name'] = str(each.course_name)
                    if not addup_courses.objects.filter(course=each, student=student_instance,
                                                        semester=semester_instance):
                        list.append(temp)
                    else:
                        list_offered.append(temp)

                dictionary['list'] = list
                dictionary['list_offered'] = list_offered
                dictionary['semester_pk'] = semester_instance.pk

                return JsonResponse(dictionary)

    if request.user.groups.filter(name='coe_staff').exists():
        all_departments = department.objects.filter(is_core=True)
    elif request.user.groups.filter(name='programme_coordinator').exists() or request.user.groups.filter(
            name='hod').exists():
        all_departments = getDepartmentsOfPc(staff_instance)
    elif request.user.groups.filter(name='faculty_advisor').exists():
        all_departments = getDepartmentsForFacultyAdvisor(staff_instance)

    return render(request, 'enrollment/enable_courses_for_student_enrollment.html', {'dept_list': all_departments})


@login_required
@permission_required('curriculum.can_choose_courses')
def courses_enrollment(request):
    student_instance = student.objects.get(user=request.user)
    regulation_instance = student_instance.batch.regulation
    programme = student_instance.qualification
    openCourseLimitExceed = False

    if regulation_instance.start_year < 2016:
        return redirect('/select_my_courses')

    section_instance = section_students.objects.get(student=student_instance).section

    semester_instance = getSemesterPlanofStudent(student_instance)

    no_of_sections = department_sections.objects.filter(department=student_instance.department,
                                                        batch=student_instance.batch).count()

    current_year = timezone.now().year

    if not semester_instance:
        msg = {
            'page_title': 'No Batch',
            'title': 'Batch not found!',
            'description': 'Contact System Admin or Programme Coordinator for more information!',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'course_enroll' in request.POST:
            selected_courses_list = request.POST.getlist('course_enroll')
            semester_pk = int(request.POST.get('hidden_semester_pk'))
            selected_course = selected_courses_list[0]
            semester_instance = semester.objects.get(pk=semester_pk)
            course_instance = courses.objects.get(pk=selected_course)

            if course_instance.is_open:
                openCoursesEnrolled = course_enrollment.objects.filter(semester=semester_instance, course__is_open=True,
                                                    student=student_instance).count()
                if not semester_instance.open_courses or openCoursesEnrolled >= semester_instance.open_courses:
                    openCourseLimitExceed = True


            if openCourseLimitExceed == False:
                if course_enrollment.objects.filter(semester=semester_instance, course=course_instance,
                                                    student=student_instance):
                    instance = course_enrollment.objects.get(semester=semester_instance,
                                                             course=course_instance, student=student_instance)
                    instance.requested = True
                    instance.enrolled_date = timezone.now()
                    instance.save()
                else:
                    instance = course_enrollment(semester=semester_instance, course=course_instance,
                                                 student=student_instance, requested=True, enrolled_date=timezone.now())
                    instance.save()

        if 'course_register' in request.POST:
            selected_courses_list = request.POST.getlist('course_register')
            semester_pk = int(request.POST.get('hidden_semester_pk'))
            selected_course = selected_courses_list[0]
            semester_instance = semester.objects.get(pk=semester_pk)
            course_instance = courses.objects.get(pk=selected_course)

            if course_enrollment.objects.filter(semester=semester_instance, course=course_instance,
                                                student=student_instance, requested=True):
                instance = course_enrollment.objects.get(semester=semester_instance,
                                                         course=course_instance, student=student_instance,
                                                         requested=True)
                instance.registered = True
                instance.registered_date = timezone.now()

                instance.approved = True

                instance.save()

    course_list = []
    available_courses = []
    list_of_requested_courses = []

    list_of_enrolled_courses = []
    list_of_registered_courses = []
    list_of_approved_courses = []

    list_of_courses = []

    for each in courses.objects.filter(semester=semester_instance.semester_number,
                                       department=student_instance.department, regulation=regulation_instance,
                                       programme=programme, is_open=False, common_course=False).order_by(
        'course_id'):
        available_courses.append(each)

    if semester_instance.open_courses and semester_instance.open_courses > 0:
        for each in courses.objects.filter(is_open=True, common_course=False, regulation=regulation_instance,
                                           programme=programme).exclude(
            department=student_instance.department).order_by('course_id'):
            available_courses.append(each)

    for each in course_enrollment.objects.filter(semester=semester_instance, student=student_instance,
                                                 requested=True).order_by('-course__semester'):
        course_list.append(each.course)
        if each.registered:
            list_of_registered_courses.append(each.course)
        elif each.requested:
            list_of_enrolled_courses.append(each.course)

        if each.approved:
            list_of_approved_courses.append(each.course)

        list_of_requested_courses.append(each)

    available_courses.sort(key=lambda x: x.course_id[-1])

    course_addups = []

    for each in addup_courses.objects.filter(semester=semester_instance, student=student_instance).order_by(
            'course__course_id'):
        course_addups.append(each.course)

    available_courses = available_courses + course_addups

    for each in available_courses:
        temp = {}
        temp['data'] = each
        if each in list_of_registered_courses:
            temp['registered'] = True
            if each in list_of_approved_courses:
                temp['approved'] = True
        elif each in list_of_enrolled_courses:
            temp['enrolled'] = True

        list_of_courses.append(temp)

    return render(request, 'enrollment/course_enrollment.html',
                  {
                      'list_of_courses': list_of_courses,
                      'semester_instance': semester_instance,
                      'section_instance': section_instance,
                      'no_of_sections': no_of_sections,
                       'openCourseLimitExceed': openCourseLimitExceed
                  })


def enrollment_pdf_data(request):
    dictionary = {}
    student_instance = student.objects.get(user=request.user)
    regulation_instance = student_instance.batch.regulation

    if regulation_instance.start_year < 2016:
        return redirect('/select_my_courses')

    semester_instance = getSemesterPlanofStudent(student_instance)

    if not semester_instance:
        msg = {
            'page_title': 'No Batch',
            'title': 'Batch not found!',
            'description': 'Contact System Admin or Programme Coordinator for more information!',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    list_of_enrolled_courses = []

    for each in course_enrollment.objects.filter(semester=semester_instance, student=student_instance,
                                                 requested=True).order_by('-course__semester'):
        temp = {}
        temp['course_id'] = each.course.course_id
        temp['course_name'] = each.course.course_name
        temp['category'] = each.course.CAT
        temp['semester'] = each.course.semester
        temp['credits'] = each.course.C_credits

        list_of_enrolled_courses.append(temp)

    dictionary['data'] = list_of_enrolled_courses
    dictionary['student_name'] = student_instance

    if semester_instance.semester_number > 1:
        dictionary['semester'] = semester_instance.semester_number - 1
    else:
        dictionary['semester'] = semester_instance.semester_number

    return dictionary


class enrollmentPDFreport(PDFTemplateView):
    template_name = "enrollment/enrollment_pdf.html"
    selected_course = None

    def get(self, request, *args, **kwargs):
        return super(enrollmentPDFreport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = enrollment_pdf_data(self.request)
        # print(context_data)
        return super(enrollmentPDFreport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )


def registration_pdf_data(request):
    dictionary = {}
    student_instance = student.objects.get(user=request.user)
    regulation_instance = student_instance.batch.regulation

    if regulation_instance.start_year < 2016:
        return redirect('/select_my_courses')

    semester_instance = getSemesterPlanofStudent(student_instance)

    if not semester_instance:
        msg = {
            'page_title': 'No Batch',
            'title': 'Batch not found!',
            'description': 'Contact System Admin or Programme Coordinator for more information!',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    list_of_enrolled_courses = []
    list_of_enrolled_courses_one_credit = []

    for each in course_enrollment.objects.filter(semester=semester_instance, student=student_instance,
                                                 registered=True).order_by('-course__semester'):

        temp = {}
        temp['course_id'] = each.course.course_id
        temp['course_name'] = each.course.course_name
        temp['category'] = each.course.CAT
        temp['semester'] = each.course.semester
        temp['credits'] = each.course.C_credits

        if each.course.is_one_credit:
            list_of_enrolled_courses_one_credit.append(temp)
        else:
            list_of_enrolled_courses.append(temp)

    dictionary['data_count'] = len(list_of_enrolled_courses)
    dictionary['data_one_credit_count'] = len(list_of_enrolled_courses_one_credit)

    # pprint(list_of_enrolled_courses)

    dictionary['courses_count'] = courses.objects.filter(semester=semester_instance.semester_number,
                                                         department=student_instance.department,
                                                         regulation=regulation_instance).count()

    dictionary['data'] = list_of_enrolled_courses
    dictionary['data_one_credit'] = list_of_enrolled_courses_one_credit
    dictionary['student_name'] = student_instance
    dictionary['semester'] = semester_instance.semester_number

    return dictionary


class registrationPDFreport(PDFTemplateView):
    template_name = "enrollment/registration_pdf.html"
    selected_course = None

    def get(self, request, *args, **kwargs):
        return super(registrationPDFreport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = registration_pdf_data(self.request)
        # print(context_data)
        return super(registrationPDFreport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )
