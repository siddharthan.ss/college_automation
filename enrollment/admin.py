# Register your models here.
from django.contrib import admin

from enrollment.models import course_enrollment, addup_courses


class CourseEnrollmentAdmin(admin.ModelAdmin):
    list_display = ('regno', 'student', 'course', 'semester', 'requested', 'registered', 'approved')
    list_filter = ('student__department', 'semester', 'course')
    search_fields = ('student__roll_no', 'course__course_id', 'course__course_name',)

    ordering = ('student__roll_no', 'course', 'semester',)

    def regno(self, obj):
        return obj.student.roll_no

class AddupCoursesAdmin(admin.ModelAdmin):
    list_display = ('regno', 'student', 'course', 'semester',)
    list_filter = ('student__department', 'semester', 'course')
    search_fields = ('student__roll_no', 'course__course_id',)

    ordering = ('student__roll_no', 'course__course_id', 'course__course_name',)

    def regno(self, obj):
        return obj.student.roll_no

admin.site.register(course_enrollment, CourseEnrollmentAdmin)
admin.site.register(addup_courses, AddupCoursesAdmin)