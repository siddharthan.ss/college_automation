from django.conf.urls import url

from developers.views import developers, reportbug, view_bug_reports, getWhatsNew, addNewUpdateInfo, changeUpdateStatus

urlpatterns = [
    url(r'^$', developers, name='developers'),
    url(r'^report_bug$', reportbug, name='reportbug'),
    url(r'^view_report_bug$', view_bug_reports, name='viewbugreport'),
    url(r'^getLatestUpdates$', getWhatsNew, name='getLatestUpdates'),
    url(r'^add_update_info', addNewUpdateInfo, name='addUpdateInfo'),
    url(r'^change_update_status', changeUpdateStatus, name='changeUpdateStatus'),
]
