from django.contrib import admin

from developers.models import DeveloperProfile, works

# Register your models here.

admin.site.register(DeveloperProfile)
admin.site.register(works)
