import random

from django.test import TestCase

from developers.models import *


class TestWorks(TestCase):
    def setUp(self):
        self.work_inst = works.objects.create(
            work_name='Testing works'
        )

    def test_works_string(self):
        self.assertEqual(str(self.work_inst), 'Testing works')


class TestDeveloperProfile(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'department.json',
        'batch.json',
        'active_batches.json',
        'student.json',
    ]

    def setUp(self):
        self.cust_inst = random.choice(CustomUser.objects.all())
        self.dev_prof_inst = DeveloperProfile.objects.create(
            user=self.cust_inst,
            current_designation='Nothing',
        )

    def test_dev_profile_string(self):
        self.assertEqual(str(self.cust_inst), str(self.dev_prof_inst))
