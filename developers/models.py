from django.db import models
from simple_history.models import HistoricalRecords

from accounts.models import student, CustomUser


# Create your models here.


class works(models.Model):
    work_name = models.CharField(max_length=30)
    history = HistoricalRecords()

    def __str__(self):
        return self.work_name


class DeveloperProfile(models.Model):
    user = models.ForeignKey(CustomUser)
    facebook_link = models.URLField(default=None, blank=True, null=True)
    google_link = models.URLField(default=None, blank=True, null=True)
    instagram_link = models.URLField(default=None, blank=True, null=True)
    linkedin_link = models.URLField(default=None, blank=True, null=True)
    twitter_link = models.URLField(default=None, blank=True, null=True)
    current_designation = models.CharField(default=None, max_length=40)
    core_developer = models.BooleanField(default=False)
    is_mentor = models.BooleanField(default=False)
    contributions = models.ManyToManyField(works)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.user.email)

    class Meta:
        permissions = (
            ('view_bug_reports', 'can view bug reports'),
        )


class BugReports(models.Model):
    user = models.ForeignKey(CustomUser)
    comment = models.CharField(max_length=7000)
    date = models.DateTimeField()
    send_success = models.BooleanField(default=False)
    history = HistoricalRecords()

    def __str__(self):
        return str(self.user)


class WhatsNew(models.Model):
    work = models.CharField(max_length=5000)
    link_to_use = models.CharField(max_length=1000, blank=True, null=True)
    date = models.DateField()
    show_status = models.BooleanField(default=False)

    def __str__(self):
        return str(self.work) + '-' + str(self.date)
