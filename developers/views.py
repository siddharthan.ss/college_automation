from datetime import timedelta, datetime

from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.http import JsonResponse
from django.shortcuts import render

from accounts.models import staff, student, CustomUser
from developers.models import DeveloperProfile, BugReports, WhatsNew


# Create your views here.

def developers(request):
    try:
        if request.user.is_authenticated():
            student_roll_no = student.objects.get(user=request.user).roll_no
        else:
            student_roll_no = None
    except student.DoesNotExist:
        student_roll_no = None
    core_developer_instances = DeveloperProfile.objects.filter(core_developer=True)

    core_dev_list = []
    for entry in core_developer_instances:
        stud_inst = student.objects.get(user=entry.user)
        temp = {}
        temp['student'] = stud_inst
        temp['dev_inst'] = entry
        core_dev_list.append(temp)

    # mentors

    mentor_instances = DeveloperProfile.objects.filter(is_mentor=True)

    mentor_list = []
    for entry in mentor_instances:
        staff_inst = staff.objects.get(user=entry.user)
        temp = {}
        temp['staff'] = staff_inst
        temp['dev_inst'] = entry
        mentor_list.append(temp)

    return render(request, 'developers/developers_page.html', {
        'core_developers': core_dev_list,
        'mentors': mentor_list,
        'roll_no': student_roll_no,
    })


# class editDevelperProfile(UpdateView):

@login_required
def reportbug(request):
    if request.method == 'POST':
        if request.is_ajax():
            dictionary = {}
            to_list = (
                ('siddharthan.ss@gmail.com'),
                ('harshaktg@gmail.com'),
                ('muthamil.ons@gmail.com'),
                ('muthukumaranvgct2016@gmail.com'),
            )

            comment = request.POST.get('comment')
            user_email = request.user
            try:
                user_name = student.objects.get(user=request.user)

                subject = 'FeedBack from student ' + str(user_name)

                content = 'Student Name: ' + str(user_name) + '\n' + \
                          'Student email: ' + str(user_email) + '\n' + \
                          'Register Number: ' + str(user_name.roll_no) + '\n' + \
                          'Department: ' + str(user_name.department) + '\n' + \
                          'Batch: ' + str(user_name.batch) + '\n' + \
                          'Semester: ' + str(user_name.current_semester) + '\n' + \
                          'Ph. No.: ' + str(user_name.phone_number) + '\n\n' + \
                          'Comment:\n\t' + str(comment)
            except:
                user_name = staff.objects.get(user=request.user)

                subject = 'FeedBack from Staff ' + str(user_name)

                content = 'Staff Name: ' + str(user_name) + '\n' + \
                          'Staff email: ' + str(user_email) + '\n' + \
                          'Designation: ' + str(user_name.designation) + '\n' + \
                          'Staff ID: ' + str(user_name.staff_id) + '\n' + \
                          'Department: ' + str(user_name.department) + '\n' + \
                          'Ph. No.: ' + str(user_name.phone_number) + '\n\n' + \
                          'Comment:\n\t' + str(comment)

            from_email = 'dummy@email.com'

            try:
                send_mail(subject, content, from_email, to_list, fail_silently=False)
                email_sent = True
                dictionary['sent'] = 'true'
                print('email sent')
            except:
                email_sent = False
                dictionary['sent'] = 'false'
                print('email not sent')

            BugReports.objects.create(
                user=request.user,
                comment=comment,
                date=datetime.now(),
                send_success=email_sent
            )

            return JsonResponse(dictionary)


@login_required
def view_bug_reports(request):
    list_of_comments_staff = []
    list_of_comments_student = []
    if request.method == 'POST':
        got_date = str(request.POST.get('selected_date'))
        converted_date = datetime.strptime(got_date, '%m/%d/%Y').strftime('%Y-%m-%d')
        next_day = datetime.strptime(got_date, '%m/%d/%Y')
        next_day = next_day + timedelta(1)
        for each in BugReports.objects.filter(date__gte=converted_date, date__lt=next_day).order_by('-date'):
            if staff.objects.filter(user=each.user):
                list_of_comments_staff.append(each)
        for each in BugReports.objects.filter(date__gte=converted_date, date__lt=next_day).order_by('-date'):
            if student.objects.filter(user=each.user):
                list_of_comments_student.append(each)

        return render(request, 'developers/view_bug_reports.html', {
            'list_of_comments_staff': list_of_comments_staff,
            'list_of_comments_student': list_of_comments_student,
            'staff_comments': len(list_of_comments_staff),
            'student_comments': len(list_of_comments_student),
            'display_date': got_date
        })

    else:
        for each in BugReports.objects.all().order_by('-date'):
            if staff.objects.filter(user=each.user):
                list_of_comments_staff.append(each)

        for each in BugReports.objects.all().order_by('-date'):
            if student.objects.filter(user=each.user):
                list_of_comments_student.append(each)

    return render(request, 'developers/view_bug_reports.html', {
        'list_of_comments_staff': list_of_comments_staff,
        'list_of_comments_student': list_of_comments_student,
        'staff_comments': len(list_of_comments_staff),
        'student_comments': len(list_of_comments_student)
    })


@login_required
def getWhatsNew(request):
    latestUpdates = []
    for newUpdate in WhatsNew.objects.filter(show_status=True).order_by('-date'):
        dictionary = {}
        dictionary['work'] = newUpdate.work
        dictionary['link_to_use'] = newUpdate.link_to_use
        dictionary['date'] = str(newUpdate.date)
        latestUpdates.append(dictionary)

    jsonDictionary = {}
    jsonDictionary['list'] = latestUpdates
    if len(latestUpdates) > 0:
        jsonDictionary['updatesAvailable'] = "true"
    else:
        jsonDictionary['updatesAvailable'] = "false"
    return JsonResponse(jsonDictionary)


@login_required
def addNewUpdateInfo(request):
    user_instance = CustomUser.objects.get(email=request.user)
    if not user_instance.is_superuser:
        msg = {
            'page_title': 'GCT | Unauthorized Access',
            'title': 'Unauthorized Access',
            'description': 'You don\'t have privilege to access this resourse!',
            'login': True
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'add' in request.POST:
            work = str(request.POST.get('work'))
            link_to_use = str(request.POST.get('link_to_use'))
            date = str(request.POST.get('date'))
            dateInstance = datetime.strptime(date,'%d/%m/%Y').strftime('%Y-%m-%d')

            whatsNewInstance = WhatsNew(work=work, link_to_use=link_to_use, date=dateInstance, show_status=True)
            whatsNewInstance.save()

        elif 'update' in request.POST:
            edit_pk = request.POST.get('edit_pk')
            work = str(request.POST.get('work'))
            link_to_use = str(request.POST.get('link_to_use'))
            status = int(request.POST.get('status'))
            date = str(request.POST.get('date'))
            dateInstance = datetime.strptime(date, '%d/%m/%Y').strftime('%Y-%m-%d')

            whatsNewInstance = WhatsNew.objects.get(pk=edit_pk)
            whatsNewInstance.work = work
            whatsNewInstance.link_to_use = link_to_use
            whatsNewInstance.date = dateInstance
            if status == 1:
                whatsNewInstance.show_status = True
            else:
                whatsNewInstance.show_status = False
            whatsNewInstance.save()

        elif 'delete' in request.POST:
            delete_pk = request.POST.get('delete_pk')

            WhatsNew.objects.get(pk=delete_pk).delete()

        else:
            updateInfoList = WhatsNew.objects.all()

            required_edit = ''

            for each in updateInfoList:
                if str(request.POST.get('edit-'+str(each.pk))) == 'Edit':
                    required_edit = int(each.pk)

            updateInfo = WhatsNew.objects.get(pk=required_edit)

            return render(request, 'developers/editUpdateInfo.html', {
                'updateInfo': updateInfo
            })

    updateInfoList = WhatsNew.objects.all().order_by('-date')

    return render(request, 'developers/addNewUpdateInfo.html', {
        'updateInfoList': updateInfoList
    })


@login_required
def changeUpdateStatus(request):
    user_instance = CustomUser.objects.get(email=request.user)
    if not user_instance.is_superuser:
        msg = {
            'page_title': 'GCT | Unauthorized Access',
            'title': 'Unauthorized Access',
            'description': 'You don\'t have privilege to access this resourse!',
            'login': True
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'hide' in request.POST:
            hide_pk = request.POST.get('hide')
            whatsNewInstance = WhatsNew.objects.get(pk=hide_pk)
            whatsNewInstance.show_status = False
            whatsNewInstance.save()

        if 'show' in request.POST:
            show_pk = request.POST.get('show')
            whatsNewInstance = WhatsNew.objects.get(pk=show_pk)
            whatsNewInstance.show_status = True
            whatsNewInstance.save()

    updateInfoList = WhatsNew.objects.all().order_by('-date')

    return render(request, 'developers/changeUpdateStatus.html', {
        'updateInfoList': updateInfoList
    })