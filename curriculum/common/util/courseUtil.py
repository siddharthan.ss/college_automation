from curriculum.views.common_includes import get_current_semester_plan_of_student
from curriculum.models import staff_course


def get_coures_currently_studying_by_student(student_instance):
    semester_instance = get_current_semester_plan_of_student(student_instance)

    list_of_courses = []

    for staff_course_entry in staff_course.objects.filter(semester = semester_instance):
        list_of_courses.append(staff_course_entry.course)

    return list_of_courses
