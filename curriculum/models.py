from django.contrib.auth.models import Group
from django.db import models
from simple_history.models import HistoricalRecords

from accounts.models import department, student, staff, batch, CustomUser, regulation, programme, department_programmes


# from simple_history.models import HistoricalRecords

class day(models.Model):
    day_name = models.CharField(max_length=10)
    slot_number = models.SmallIntegerField()
    history = HistoricalRecords()

    def __str__(self):
        return str(self.day_name)


class hod(models.Model):
    department = models.ForeignKey(department)
    staff = models.ForeignKey(staff, related_name='hod_staff')


class courses(models.Model):
    CAT_CHOICES = (
        ('HS', 'HS'),
        ('BS', 'BS'),
        ('ES', 'ES'),
        ('PC', 'PC'),
        ('PE', 'PE'),
        ('OE', 'OE'),
        ('FC', 'FC'),
        ('OC', 'OC'),
        ('EEC', 'EEC'),
    )
    SUBJECT_CHOICES = (
        ('T', 'Theory'),
        ('P', 'Practical'),
    )
    QUALIFICATION_CHOICES = (('UG', 'Under Graduate'),
                             ('PG', 'Post Graduate'),
                             ('Phd', 'Doctrate of Philosophy')
                             )
    department = models.ForeignKey(department, verbose_name='Department')
    semester = models.SmallIntegerField(blank=True, null=True)
    course_id = models.CharField(primary_key=True, max_length=8)
    course_name = models.CharField(max_length=100)
    subject_type = models.CharField(max_length=9, choices=SUBJECT_CHOICES)
    CAT = models.CharField(max_length=3, choices=CAT_CHOICES)
    CAT_marks = models.SmallIntegerField()
    end_semester_marks = models.SmallIntegerField()
    L_credits = models.SmallIntegerField()
    T_credits = models.SmallIntegerField()
    P_credits = models.SmallIntegerField()
    C_credits = models.SmallIntegerField()
    code = models.CharField(blank=True, max_length=10)
    programme = models.CharField(max_length=2, choices=QUALIFICATION_CHOICES)
    is_elective = models.BooleanField(default=False)
    is_open = models.BooleanField(default=False)
    is_one_credit = models.BooleanField(default=False)
    regulation = models.ForeignKey(regulation)
    common_course = models.BooleanField(default=False)
    isOpenElective = models.BooleanField(default=False)
    history = HistoricalRecords()

    class Meta:
        unique_together = ('course_id', 'programme')

    def __str__(self):
        if self.common_course:
            return str(self.course_name)
        return str(self.course_id) + '-' + str(self.course_name)


class department_sections(models.Model):
    department = models.ForeignKey(department)
    batch = models.ForeignKey(batch)
    section_name = models.CharField(max_length=1)

    class Meta:
        unique_together = ('department', 'batch', 'section_name')

    def __str__(self):
        return str(self.department.acronym) + ' section ' + str(self.section_name)


class section_students(models.Model):
    student = models.ForeignKey(student)
    section = models.ForeignKey(department_sections)


class semester(models.Model):
    department = models.ForeignKey(department)
    department_section = models.ForeignKey(department_sections, null=True)
    batch = models.ForeignKey(batch)
    semester_number = models.IntegerField()
    faculty_advisor = models.ManyToManyField(staff)
    start_term_1 = models.DateField(blank=True, default=None, null=True)
    end_term_1 = models.DateField(blank=True, default=None, null=True)
    start_term_2 = models.DateField(blank=True, default=None, null=True)
    end_term_2 = models.DateField(blank=True, default=None, null=True)
    start_term_3 = models.DateField(blank=True, default=None, null=True)
    end_term_3 = models.DateField(blank=True, default=None, null=True)
    open_courses = models.PositiveSmallIntegerField(blank=True, null=True)
    history = HistoricalRecords()

    def getDisplayTextForPastReport(self):
        if department_sections.objects.filter(department=self.department, batch=self.batch).count() > 1:
            return str(self.department.acronym) + ' - ' + str(self.batch) + ' - Sem ' + str(
                self.semester_number) + ' [ ' + str(self.batch.programme) + ' ] - Section ' + str(
                self.department_section.section_name)
        else:
            return str(self.department.acronym) + ' - ' + str(self.batch) + ' - Sem ' + str(
                self.semester_number) + ' [ ' + str(self.batch.programme) + ' ]'

    def getDisplayTextForAllotmentTab(self):
        if semester.objects.filter(department=self.department, batch=self.batch).count() > 1:
            return str(self.department.acronym) + ' S' + str(self.semester_number) + ' ( ' + str(
                self.batch.programme) + ' )' + '-' + str(
                self.department_section.section_name)
        else:
            return str(self.department.acronym) + ' S' + str(self.semester_number) + ' ( ' + str(
                self.batch.programme) + ' )'

    def getDisplayTextForFirstYearAllotmentTab(self):
        if semester.objects.filter(department=self.department, batch=self.batch).count() > 1:
            return str(self.department.acronym) + ' S' + str(self.semester_number) + '-' + str(
                self.department_section.section_name)
        else:
            return str(self.department.acronym) + ' S' + str(self.semester_number)

    def __str__(self):
        return str(self.department) + '_sem=' + str(self.semester_number) + '_' + str(self.batch.programme) + '_' + str(
            self.department_section.section_name)

    class Meta:
        unique_together = ('department', 'batch', 'semester_number', 'department_section')
        permissions = (
            ('can_create_semester_plan', 'Can create semester plan'),
        )


class attendance(models.Model):
    course = models.ForeignKey(courses)
    date = models.DateField()
    hour = models.PositiveSmallIntegerField()
    staff = models.ForeignKey(staff)
    absent_students = models.ManyToManyField(student, related_name='absent')
    present_students = models.ManyToManyField(student, related_name='present')
    onduty_students = models.ManyToManyField(student, related_name='onduty')
    granted_edit_access = models.BooleanField(default=False)
    grant_period = models.DateTimeField()
    semester = models.ForeignKey(semester)
    history = HistoricalRecords()

    class Meta:
        unique_together = ('course', 'semester', 'date', 'hour')


class time_table(models.Model):
    course = models.ForeignKey(courses)
    department = models.ForeignKey(department)
    day = models.ForeignKey(day)
    hour = models.PositiveIntegerField()
    semester = models.ForeignKey(semester)
    batch = models.ForeignKey(batch)
    is_active = models.BooleanField(default=True)
    created_date = models.DateField()
    modified_date = models.DateField()
    history = HistoricalRecords()

    @property
    def get_course(self):
        return courses.objects.get(pk=self.course_id)

    def __str__(self):
        return str(self.department) + '-' + str(self.course.course_id) + '-' + str(self.day) + '-' + str(self.hour)

    class Meta:
        unique_together = ('department', 'course', 'batch', 'semester', 'day', 'hour', 'is_active', 'created_date')
        permissions = (
            ('can_create_timetable', 'Can create timetable'),
        )


class staff_course(models.Model):
    course = models.ForeignKey(courses)
    staffs = models.ManyToManyField(staff)
    batch = models.ForeignKey(batch)
    semester = models.ForeignKey(semester)
    department = models.ForeignKey(department)
    history = HistoricalRecords()

    class Meta:
        unique_together = (('course', 'batch', 'semester', 'department'),)

    def __str__(self):
        return str(self.course) + '-' + str(self.semester) + '-' + str(self.batch)


class college_schedule(models.Model):
    YEAR_CHOICES = (
        ('first_year', 'First Year'),
        ('remaining_years', 'Remaining Years'),
    )
    year = models.CharField(choices=YEAR_CHOICES, max_length=20)
    hour = models.SmallIntegerField()
    start_of_hour = models.TimeField()
    end_of_hour = models.TimeField()
    history = HistoricalRecords()

    attendance_marking_time_limit = models.SmallIntegerField()
    LIMIT_CHOICES = (
        ('BOH', 'Beginning of Hour'),
        ('EOH', 'End of Hour'),
    )
    limit_starts_from = models.CharField(choices=LIMIT_CHOICES, max_length=3)
    regulation = models.ForeignKey(regulation)

    def __str__(self):
        return 'College Schedule for ' + self.year


class holidays(models.Model):
    date = models.DateField()
    description = models.CharField(max_length=50)
    history = HistoricalRecords()





class student_enrolled_courses(models.Model):
    student = models.ForeignKey(student)
    course = models.ForeignKey(courses)
    under_staff = models.ForeignKey(staff)
    studied_semester = models.ForeignKey(semester)
    request_course = models.BooleanField(default=False)
    approved = models.BooleanField(default=False)
    cleared = models.BooleanField(default=False)
    cleared_semester = models.SmallIntegerField(blank=True, null=True)
    history = HistoricalRecords()

    class Meta:
        unique_together = ('student', 'course')


class open_course_staff(models.Model):
    course = models.OneToOneField(courses)
    staffs = models.ManyToManyField(staff)
    history = HistoricalRecords()


class elective_course_staff(models.Model):
    course = models.OneToOneField(courses)
    staff = models.ForeignKey(staff)
    enabled = models.BooleanField(default=False)
    history = HistoricalRecords()


class one_credit_course_staff(models.Model):
    course = models.OneToOneField(courses)
    staff = models.ForeignKey(staff)
    enabled = models.BooleanField(default=False)
    history = HistoricalRecords()


class semester_migration(models.Model):
    semester = models.ForeignKey(semester)
    department = models.ForeignKey(department)
    semester_number = models.PositiveSmallIntegerField()
    history = HistoricalRecords()


class reallow_attendance_requests(models.Model):
    staff = models.ForeignKey(staff)
    date = models.DateField()
    hour = models.SmallIntegerField()
    course = models.ForeignKey(courses)
    is_allowed = models.BooleanField(default=False)
    semester = models.ForeignKey(semester)
    history = HistoricalRecords()


class batch_split(models.Model):
    semester = models.ForeignKey(semester)
    course = models.ForeignKey(courses)
    day = models.ForeignKey(day)
    start_student = models.ForeignKey(student, related_name='start_student')
    end_student = models.ForeignKey(student, related_name='end_student')
    timestamp = models.DateTimeField()
    is_active = models.BooleanField(default=True)
    batch_number = models.SmallIntegerField()
    history = HistoricalRecords()


class flexi_attendance(models.Model):
    marked_course = models.ForeignKey(courses, related_name='marked_course')
    attendance_date = models.DateField()
    attendance_hour = models.IntegerField()
    original_course = models.ForeignKey(courses, related_name='original_course')
    is_approved = models.BooleanField(default=False)
    is_marked = models.BooleanField(default=False)
    semester = models.ForeignKey(semester)
    history = HistoricalRecords()


class unit_test_timetable(models.Model):
    semester = models.ForeignKey(semester)
    UT_CHOICES = (
        (1, 'Unit Test 1'),
        (2, 'Unit Test 2'),
        (3, 'Unit Test 3'),
    )
    ut_number = models.SmallIntegerField(choices=UT_CHOICES)
    course = models.ForeignKey(courses)
    date = models.DateField()

    FN_OR_AN_CHOICES = (
        ('FN', 'FN'),
        ('AN', 'AN'),
    )
    fn_or_an = models.CharField(max_length=2, choices=FN_OR_AN_CHOICES)


    # class semester_duration(models.Model):
    #     department = models.ForeignKey(department)
    #     year = models.PositiveIntegerField()
    #     is_even_semester = models.BooleanField()
    #
    #     def __str__(self):
    #         return str(self.department.acronym) + '-' + str(self.year) + '-' + str(self.is_even_semester)


class programme_coordinator(models.Model):
    programme_coordinator_staffs = models.ManyToManyField(staff)
    department_programme = models.ForeignKey(department_programmes)


class staff_course_split(models.Model):
    staff_course = models.ForeignKey(staff_course)
    staff = models.ForeignKey(staff)
    student = models.ForeignKey(student)

    class Meta:
        unique_together = ('staff_course', 'staff', 'student')


class timeTableSlot(models.Model):
    date = models.DateField()
    slotday = models.ForeignKey(day)

    def __str__(self):
        return str(self.date.strftime("%d-%m-%Y")) + " Slot: " + str(self.slotday.slot_number)


class forceTimetableSlot(models.Model):
    date = models.DateField()
    slotNumber = models.SmallIntegerField()

    def __str__(self):
        return str(self.date.strftime("%d-%m-%Y")) + ' - ' + str(self.slotNumber)

    class Meta:
        unique_together = ('date', 'slotNumber')