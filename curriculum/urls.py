from django.conf.urls import url

from curriculum.views import *
from curriculum.views import CourseListView
from curriculum.views import mark_pending_attendandes, get_pending_hours_for_given_date
from curriculum.views.add_to_db import *
from curriculum.views.allot_coe_head import allot_coe_head
from curriculum.views.attendance_report import weekly_attendance_report, weeklyPdfReport, \
    consolidated_attendance_report, consolidatedPdfReport, customAttendanceReport, customPdfReport
from curriculum.views.attendance_lookup import *
from curriculum.views.department_section_split import *
from curriculum.views.edit_student_profile import UpdateStudentProfile
from curriculum.views.first_year_settings import *
from curriculum.views.flexi_attendance import *
from curriculum.views.forceTimetableSlot import forceTimeTableSlot
from curriculum.views.holidays import ListHolidays, HolidaysCreate
from curriculum.views.lab_batch_settings import *
from curriculum.views.migrate_semesters import migrate_semester_view
from curriculum.views.open_elective_courses import prepare_open_course_staffs, select_courses
from curriculum.views.overall_attendance_report import *
from curriculum.views.reallow_attendance_requests import *
from curriculum.views.semester_migrations import migrate_semesters, check_migration_consitency
from curriculum.views.semester_plan import *
from curriculum.views.unit_test_timetable import *
from curriculum.views.add_student_to_attendance import *
from curriculum.views.allot_staff_split import *

urlpatterns = [

    url(r'^assign_programme_coordinator/$', assign_programme_coordinator, name="assign_programme_ordinator"),
    url(r'^create_semester_plan/$', create_semester_plan, name="semester_plan"),
    url(r'^create_semester_plan_first_year/$', semester_plan_first_year, name="create_semester_plan_first_year"),

    # url(r'^view_semester_plan/$',view_semester_plan,name = "view_semester_plan"),

    url(
        r'^attendance/(?P<semester_pk>\d+)/(?P<course_id>[0-9a-zA-Z ]+)/(?P<attendance_date>\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01]))/(?P<hour>[1-8]{1}|xyz$)',
        mark_attendance, name="attendance"),
    url(
        r'^reallow_request/(?P<semester_pk>\d+)/(?P<course_id>[0-9a-zA-Z ]+)/(?P<attendance_date>\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01]))/(?P<hour>[1-8]{1}|xyz$)',
        request_reallow_already_marked, name="reallow_request"),
    url(
        r'^add_student_to_attendance/(?P<semester_pk>\d+)/(?P<course_id>[0-9a-zA-Z ]+)/(?P<attendance_date>\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01]))/(?P<hour>[1-8]{1}|xyz$)',
        add_student_to_attendance, name="add_student_to_attendance"),
    url(
        r'^bulk_attendance/(?P<semester_pk>\d+)/(?P<course_id>[0-9a-zA-Z ]+)/(?P<attendance_date>\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01]))/(?P<start_hour>[1-8]{1}|xyz$)/(?P<end_hour>[1-8]{1}|xyz$)',
        mark_bulk_attendance, name="bulk_attendance"),
    url(
        r'^reallow_bulk_request/(?P<semester_pk>\d+)/(?P<course_id>[0-9a-zA-Z ]+)/(?P<attendance_date>\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01]))/(?P<start_hour>[1-8]{1}|xyz$)/(?P<end_hour>[1-8]{1}|xyz$)',
        request_bulk_reallow_already_marked, name="reallow_bulk_request"),
    url(r'^weekly_attendance_report', weekly_attendance_report, name="weekly_attendance_report"),
    url(r'^weekly_attendance_lookup', weekly_attendance_lookup, name="weekly_attendance_lookup"),
    # url(r'^overall_weekly_attendance_report',overall_weekly_attendance_report,name = "overall_weekly_attendance_report"),
    url(r'^consolidated_attendance_report', consolidated_attendance_report, name="consolidated_attendance_report"),
    url(r'^consolidated_attendance_lookup', consolidated_attendance_lookup, name="consolidated_attendance_lookup"),
    url(r'^consolidated_attendance_conduct_report', consolidated_attendance_conduct_report,
        name="consolidated_attendance_conduct_report"),
    url(r'^custom_attendance_report', customAttendanceReport, name="custom_attendance_report"),
    url(r'^custom_attendance_lookup', custom_attendance_lookup, name="custom_attendance_lookup"),
    # url(r'^overall_custom_attendance_report',overall_custom_attendance_report,name = "overall_custom_attendance_report"),
    # url(r'^overall_consolidated_attendance_report',overall_consolidated_attendance_report,name = "overall_consolidated_attendance_report"),
    url(
        r'^weekly_attendance_pdf_report/(?P<allot_staff_pk>\d+)/(?P<start_date>[0-9]{4}-[0-9]{2}-[0-9]{2})/(?P<end_date>[0-9]{4}-[0-9]{2}-[0-9]{2})',
        weeklyPdfReport.as_view(), name="weekly_attendance_pdf_report"),
    url(
        r'^custom_attendance_pdf_report/(?P<allot_staff_pk>\d+)/(?P<start_date>[0-9]{4}-[0-9]{2}-[0-9]{2})/(?P<end_date>[0-9]{4}-[0-9]{2}-[0-9]{2})',
        customPdfReport.as_view(), name="custom_attendance_pdf_report"),
    url(r'^consolidated_attendance_pdf_report/(?P<allot_staff_pk>\d+)', consolidatedPdfReport.as_view(),
        name="consolidated_attendance_pdf_report"),
    url(r'^consolidated_attendance_conduct_pdf_report/(?P<allot_staff_pk>\d+)', consolidatedConductPdfReport.as_view(),
        name="consolidated_attendance_conduct_pdf_report"),
    # url(r'^overall_consolidated_attendance_pdf_report/(?P<semester_pk>\d+)',overallConsolidatedPdfReport.as_view(),name = "overall_consolidatd_attendance_pdf_report"),
    # url(r'^overall_weekly_attendance_pdf_report/(?P<semester_pk>\d+)/(?P<start_date>[0-9]{4}-[0-9]{2}-[0-9]{2})/(?P<end_date>[0-9]{4}-[0-9]{2}-[0-9]{2})',overallWeeklyPdfReport.as_view(),name = "overall_weekly_attendance_pdf_report"),
    # url(r'^overall_custom_attendance_pdf_report/(?P<semester_pk>\d+)/(?P<start_date>[0-9]{4}-[0-9]{2}-[0-9]{2})/(?P<end_date>[0-9]{4}-[0-9]{2}-[0-9]{2})',overallCustomPdfReport.as_view(),name = "overall_custom_attendance_pdf_report"),

    url(r'^pending_attendances', mark_pending_attendandes, name='mark_pending_attendances'),
    url(r'^pending_hours', get_pending_hours_for_given_date, name='get_pending_hours'),

    url(r'^reallow_attendance/$', reallow_attendance, name='reallow_attendance'),
    url(r'^create_timetable/$', create_timetable, name='create_timetable'),
    # url(r'^create_timetable_first_year/$', create_timetable_first_year, name='create_timetable_first_year'),

    url(r'^view_timetable/$', view_timetable, name="view_timetable"),
    # url(r'^view_timetable_first_year/$',view_timetable_first_year,name = "view_timetable_first_year"),

    url(r'^view_my_timetable/$', view_my_timetable, name="view_my_timetable"),

    url(r'^timetable_revision/(?P<semester_pk>[0-9]+)/$', timetable_revision, name='timetable_revision'),

    url(r'^timetable_slot/$', forceTimeTableSlot, name="forceTimetableSlot"),

    # ajax calls
    url(r'^get_courses', get_courses_alloted_to_faculty, name='get_courses'),
    url(r'^get_hour', get_hours_of_selected_date, name='get_hour'),
    url(r'^check_attendance_entry', check_attendance_entry, name='check_attendance_entry'),
    url(r'^current_facutly_advisors', get_current_semesters_faculty_advisors, name='current_fa'),

    url(r'^list_holidays', login_required(ListHolidays.as_view()), name='list_holidays'),
    url(r'^add_holidays', login_required(HolidaysCreate.as_view()), name='add_holidays'),

    url(r'^courses/$', CourseListView.as_view(), name='course_list'),

    # url(r'^split_staff_course_handling/$', split_staff_course_handling, name='split_staff_course'),
    # url(r'^allot_staff_first_year/$', allot_staff_first_year, name='allot_staff_first_year'),


    url(r'^prepare_open_course/$', prepare_open_course_staffs, name="prepare_open_course_staffs"),
    # url(r'^prepare_elective_course/$', prepare_elective_course_staffs, name = "prepare_elective_course_staffs"),
    # url(r'^prepare_one_credit_course/$', prepare_one_credit_course_staffs, name = "prepare_one_credit_course_staffs"),
    url(r'^select_my_courses/$', select_courses, name="select_courses"),
    # url(r'^select_my_next_semester_courses/$', select_courses_next_semester, name = "select_courses_next_semester"),


    url(r'^create_course_first_year/$', add_courses_to_db_first_year, name='add_course_to_db_first_year'),

    url(r'^create_course/$', add_courses_to_db, name='add_course_to_db'),
    url(r'^view_course/$', view_course_form_db, name='view_course_to_db'),
    # url(r'^delete_course/$', delete_course_from_db, name='delete_course_from_db'),
    url(r'^create_regulation/$', add_regulation_to_db, name='add_regulation_to_db'),
    url(r'^edit_regulation/$', edit_regulation, name='edit_regulation'),

    url(r'^migrate_semesters/$', migrate_semester_view, name='migrate_semesters'),
    url(r'^check_migration_consitency/$', check_migration_consitency, name='check_migration_consitency'),
    url(r'^reallow_attendance_request/$', reallow_attendance_request, name='reallow_attendance_requests'),

    url(r'^add_lab_batch', add_lab_batch, name="add_lab_batch"),
    url(r'^view_split_batches/(?P<semester_pk>\d+)', view_split_batches, name="view_split_batches"),

    url(r'^mark_flexi_attendance', mark_flexi_attendance, name="mark_flexi_attendance"),
    url(r'^view_flexi_marked_attendance', view_flexi_marked_attendance, name="view_flexi_marked_attendance"),
    url(r'^view_flexi_attendance_requests', view_flexi_attendance_requests, name="view_flexi_attendance_requests"),

    url(r'^first_year_settings', first_year_settings, name="first_year_settings"),
    url(r'^set_no_of_sections', set_no_of_sections, name="set_no_of_sections"),

    url(r'^create_unit_test_timetable', create_unit_test_timetable, name="create_unit_test_timetable"),
    url(r'^view_ut_timetable/(?P<semester_pk>\d+)', view_ut_timetable, name="view_ut_timetable"),

    url(r'^overall_consolidated_attendance_report', consolidated_overall_attendance_report,
        name="overall_consolidated_attendance_report"),
    url(r'^overall_weekly_attendance_report', weekly_overall_attendance_report,
        name="overall_weekly_attendance_report"),

    # url(r'^instant_reallow_attendance/(?P<semester_pk>\d+)/(?P<course_id>[0-9a-zA-Z]+)/(?P<attendance_date>\d{4}\-(0?[1-9]|1[012])\-(0?[1-9]|[12][0-9]|3[01]))/(?P<hour>[1-8]{1}|xyz$)',
    #     instant_reallow_attendance, name="instant_reallow_attendance"),
    url(r'^instant_reallow_attendance', instant_reallow_attendance, name="instant_reallow_attendance"),
    url(r'^instant_delete_attendance', instant_delete_attendance, name="instant_delete_attendance"),

    url(r'^allot_coe', allot_coe_head, name="allot_coe"),
    url(r'^edit_student_profile/(?P<roll_number>[0-9LT_]+)$', login_required(UpdateStudentProfile.as_view()),
        name="edit_student_profile"),



]
