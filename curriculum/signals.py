from datetime import datetime

from django.db.models.signals import post_save, pre_save, pre_delete, m2m_changed
from django.dispatch import receiver

from accounts.models import issue_onduty, onduty_request
from curriculum.models import *
from curriculum.views.common_includes import get_course_of_student_given_date_and_hour, \
    get_current_semester_plan_of_student
from dashboard.notification import send_notification
from django.conf import settings
from one_signal_sdk.notification_handler import handle_user_notification
from django.urls import reverse
from curriculum.views.common_includes import get_first_year_managing_department


@receiver(pre_save, sender=issue_onduty)
def mark_od_for_past_hours_full_day_od(sender, instance, **kwargs):
    print('inside mark past full day od')
    print(vars(instance))
    list_of_courses = get_course_of_student_given_date_and_hour(
        student_inst=instance.student,
        date=instance.date,
    )
    print('list of courses')
    print(list_of_courses)
    current_semester_plan_of_student = get_current_semester_plan_of_student(instance.student)
    for course in list_of_courses:
        print(instance.date)
        attendance_instances = attendance.objects.filter(
            course=course,
            date=instance.date,
            semester=current_semester_plan_of_student
        )
        print('list of attendance entries')
        print(vars(attendance_instances))
        for entries in attendance_instances:
            print('each entry')
            print(vars(entries))
            absent_list = entries.absent_students.all()
            # fix - to handle improper OD marking of hours that the student was not part of
            # parallel lab hours
            attendance_marked = False
            if instance.student in absent_list:
                entries.absent_students.remove(instance.student)
                attendance_marked = True
            present_list = entries.present_students.all()
            if instance.student in present_list:
                entries.present_students.remove(instance.student)
                attendance_marked = False
            onduty_list = entries.onduty_students.all()
            if instance.student not in onduty_list and attendance_marked:
                entries.onduty_students.add(instance.student)
            entries.save()


# @receiver(post_save, sender=onduty_request)
# def notify_od_granted(sender, instance, **kwargs):
#     if instance.granted:
#         #print(vars(instance))
#         #print(str(instance.staff.designation).upper())
#         datas = {}
#         datas['subject'] = 'GCT Portal - Granted OD'
#         datas['message'] = 'As per your request to ' + str(instance.staff.designation) + ' ' + str(instance.staff).upper() + ',' + 'you have been granted on-duty from ' + \
#             str(instance.start_date) + ' to ' +str(instance.end_date)
#         datas['email'] = instance.student.user.email
#         #print(instance.student.user.email)
#         receiver_list = []
#         receiver_list.append(instance.student.user)
#         content = 'OD granted from ' + str(instance.start_date) + ' to ' +str(instance.end_date)
#         url = 'my_granted_ods'
#         #print(datas['message'])
#         send_message_notifications(instance.student.department, receiver_list, content, url, False, False, False)
#         for each in receiver_list:
#             send_notification(recipient=each, message=content, url=url)
#         sendEmail(datas)


@receiver(post_save, sender=student_enrolled_courses)
def approved_enrolled_course(sender, instance, **kwargs):
    if instance.approved == True:
        datas = {}
        datas['subject'] = 'GCT Portal - Course Approved'
        datas['message'] = 'As per your request to study ' + str(instance.course).upper() + ' has been approved'
        datas['email'] = instance.student.user.email
        content = str(instance.course).upper() + ' course approved'
        url = 'select_my_courses'
        send_notification(recipient=instance.student.user, message=content, url=url)
        # sendEmail(datas)
    if kwargs['created']:
        datas = {}
        datas['subject'] = 'GCT Portal - Course Requested'
        datas['message'] = 'Your request to study ' + str(instance.course).upper() + ' has been submitted successfully'
        datas['email'] = instance.student.user.email
        content = str(instance.course).upper() + ' course requested'
        url = 'select_my_courses'
        send_notification(recipient=instance.student.user, message=content, url=url)
        #   sendEmail(datas)

    elif instance.approved == False and instance.request_course == True:
        datas = {}
        datas['subject'] = 'GCT Portal - Course Disapproved'
        datas['message'] = 'As per your request to study ' + str(
            instance.course).upper() + ' has been disapproved. Contact the respective faculty to know more'
        datas['email'] = instance.student.user.email
        content = str(instance.course).upper() + ' course disapproved'
        url = 'select_my_courses'
        send_notification(recipient=instance.student.user, message=content, url=url)
        # sendEmail(datas)


@receiver(pre_delete, sender=student_enrolled_courses)
def rejected_enrolled_course(sender, instance, **kwargs):
    datas = {}
    datas['subject'] = 'GCT Portal - Course Rejected'
    datas['message'] = 'Your request to study ' + str(instance.course).upper() +' has been rejected.'
    datas['email'] = instance.student.user.email
    content = str(instance.course).upper() + ' course rejected'
    url = 'select_courses'
    send_notification(recipient=instance.student.user, message=content, url=url)
    # sendEmail(datas)


def marked_flexi_is_marked_true(instance):
    if instance.present_students.count() > 0 or \
            instance.absent_students.count() > 0 or \
            instance.onduty_students.count() > 0:
        flexi_attendance_query = flexi_attendance.objects.filter(
            semester=instance.semester
        ).filter(
            marked_course=instance.course
        ).filter(
            attendance_date=instance.date
        ).filter(
            attendance_hour=instance.hour
        ).filter(
            is_marked=False
        )

        print('inside flexi marked signal')

        if flexi_attendance_query.exists():
            flexi_attendance_instance = flexi_attendance_query.first()
            flexi_attendance_instance.is_marked = True
            flexi_attendance_instance.save()


@receiver(m2m_changed, sender=attendance.present_students.through)
def present_students_handler(sender, **kwargs):
    instance = kwargs.pop('instance', None)
    action = kwargs.pop('action', None)

    if action == "post_add":
        marked_flexi_is_marked_true(instance)

        # notification
        # if settings.ENABLE_ONESIGNAL_NOTIFICATIONS:
        #     customUserInstances = []
        #     for entry in instance.present_students.all():
        #         customUserInstances.append(entry.user)
        #     contents = ''
        #     if instance.course.code != '':
        #         contents += str(instance.course.code)
        #     else:
        #         contents += str(instance.course.course_id)
        #     contents += ' - '
        #     contents += instance.date.strftime("%d/%m/%Y")
        #     contents += ' - '
        #     contents += 'hour ' + str(instance.hour)
        #     handle_user_notification(
        #         heading='Present Marked',
        #         contents= contents,
        #         url=settings.CURRENT_HOST_NAME[:-1] + reverse('viewprofile'),
        #         custom_user_instances=customUserInstances,
        #     )


@receiver(m2m_changed, sender=attendance.absent_students.through)
def absent_students_handler(sender, **kwargs):
    instance = kwargs.pop('instance', None)
    action = kwargs.pop('action', None)

    if action == "post_add":
        marked_flexi_is_marked_true(instance)

        # notification
        # if settings.ENABLE_ONESIGNAL_NOTIFICATIONS:
        #     customUserInstances = []
        #     for entry in instance.absent_students.all():
        #         customUserInstances.append(entry.user)
        #     contents = ''
        #     if instance.course.code != '':
        #         contents += str(instance.course.code)
        #     else:
        #         contents += str(instance.course.course_id)
        #     contents += ' - '
        #     contents += instance.date.strftime("%d/%m/%Y")
        #     contents += ' - '
        #     contents += 'hour ' + str(instance.hour)
        #     handle_user_notification(
        #         heading='Absent Marked',
        #         contents=contents,
        #         url=settings.CURRENT_HOST_NAME[:-1] + reverse('viewprofile'),
        #         custom_user_instances=customUserInstances,
        #     )


@receiver(m2m_changed, sender=attendance.onduty_students.through)
def onduty_students_handler(sender, **kwargs):
    instance = kwargs.pop('instance', None)
    action = kwargs.pop('action', None)

    if action == "post_add":
        marked_flexi_is_marked_true(instance)

        # notification
        # if settings.ENABLE_ONESIGNAL_NOTIFICATIONS:
        #     customUserInstances = []
        #     for entry in instance.onduty_students.all():
        #         customUserInstances.append(entry.user)
        #     contents = ''
        #     if instance.course.code != '':
        #         contents += str(instance.course.code)
        #     else:
        #         contents += str(instance.course.course_id)
        #     contents += ' - '
        #     contents += instance.date.strftime("%d/%m/%Y")
        #     contents += ' - '
        #     contents += 'hour ' + str(instance.hour)
        #     handle_user_notification(
        #         heading='Onduty Marked',
        #         contents=contents,
        #         url=settings.CURRENT_HOST_NAME[:-1] + reverse('viewprofile'),
        #         custom_user_instances=customUserInstances,
        #     )


@receiver(post_save, sender=section_students)
def updated_enrolled_courses_semester_for_student(sender, instance, **kwargs):
    pass


@receiver(m2m_changed, sender=programme_coordinator.programme_coordinator_staffs.through)
def manage_pc_group(sender, **kwargs):
    instance = kwargs.pop('instance', None)
    pk_set = kwargs.pop('pk_set', None)
    action = kwargs.pop('action', None)

    print(instance.programme_coordinator_staffs.all())

    """
        for every staff to be remove from facutly advisor group,
        check whether the faculty is not in any of the active semester faculty
    """

    g = Group.objects.get(name='programme_coordinator')

    if action == "pre_remove":

        list_of_pcs = []
        for programme_coordinator_inst in programme_coordinator.objects.all():
            if programme_coordinator_inst != instance:
                for entry in programme_coordinator_inst.programme_coordinator_staffs.all():
                    list_of_pcs.append(entry)
            else:
                for pc_staff in programme_coordinator_inst.programme_coordinator_staffs.all():
                    if pc_staff in instance.programme_coordinator_staffs.all():
                        list_of_pcs.append(pc_staff)

        print("list of pc")
        print(list_of_pcs)

        for faculty in instance.programme_coordinator_staffs.all():
            if faculty not in list_of_pcs:
                g.user_set.remove(faculty.user)
                g.save()

    if action == "post_add":
        list_of_pcs = []
        for faculty in instance.programme_coordinator_staffs.all():
            print("staff = " + str(faculty))
            for programme_coordinator_inst in programme_coordinator.objects.all():
                if programme_coordinator_inst != instance:
                    for entry in programme_coordinator_inst.programme_coordinator_staffs.all():
                        list_of_pcs.append(entry)
        print("list of pc")
        print(list_of_pcs)
        for faculty in instance.programme_coordinator_staffs.all():
            if faculty not in list_of_pcs:
                g.user_set.add(faculty.user)
                g.save()

    print(locals())


@receiver(post_save, sender=hod)
def add_to_hod_group(sender, instance, **kwargs):
    staff_inst = instance.staff
    custom_user_inst = staff_inst.user

    if not custom_user_inst.groups.filter(name="hod").exists():
        g = Group.objects.get(name="hod")
        g.user_set.add(custom_user_inst)
        g.save()
        print(str(custom_user_inst) + 'added to hod group')
    else:
        print(str(custom_user_inst) + 'not added to hod group')


@receiver(pre_save, sender=hod)
def safe_remove_from_hod_group(sender, instance, **kwargs):
    print(instance.staff)

    if not hod.objects.filter(department=instance.department).exists():
        return
    current_hod_inst = hod.objects.get(department=instance.department)
    print(current_hod_inst.staff)
    is_another_dept_hod = False
    for inst in hod.objects.all():
        if inst != instance:
            if inst.staff == current_hod_inst.staff:
                is_another_dept_hod = True

    if not is_another_dept_hod:
        g = Group.objects.get(name="hod")
        g.user_set.remove(current_hod_inst.staff.user)
        g.save()
        print(str(current_hod_inst.staff.user) + 'removed from hod group')
    else:
        print(str(current_hod_inst.staff.user) + 'not removed from hod group')


@receiver(post_save, sender=reallow_attendance_requests)
def notifiy_attendance_reallowed(sender, instance, **kwargs):
    contents = ''
    if instance.course.code != '':
        contents += str(instance.course.code)
    else:
        contents += str(instance.course.course_id)
    contents += ' - '
    if type(instance.date) is str:
        contents += datetime.strptime(instance.date, "%Y-%m-%d").strftime("%d/%m/%Y")
    else:
        contents += instance.date.strftime("%d/%m/%Y")
    contents += ' - '
    contents += 'hour ' + str(instance.hour)
    if instance.is_allowed:
        handle_user_notification(
            heading='Attendance re-enabled',
            contents=contents,
            url=settings.CURRENT_HOST_NAME[:-1] + reverse('mark_pending_attendances'),
            custom_user_instances=instance.staff.user,
        )
    else:
        hod_query = hod.objects.filter(department=instance.course.department)
        if hod_query.exists():
            handle_user_notification(
                heading='Attendance re-enable Request',
                contents=contents,
                url=settings.CURRENT_HOST_NAME[:-1] + reverse('reallow_attendance_requests'),
                custom_user_instances=hod_query.get().staff.user,
            )
        if instance.course.semester <= 2:
            hod_query = hod.objects.filter(department=get_first_year_managing_department())
            if hod_query.exists():
                handle_user_notification(
                    heading='Attendance re-enable Request',
                    contents=contents,
                    url=settings.CURRENT_HOST_NAME[:-1] + reverse('reallow_attendance_requests'),
                    custom_user_instances=hod_query.get().staff.user,
                )


# test and enable
"""
@receiver(post_save, sender=onduty_request)
def notify_od_request(sender, instance, **kwargs):
    if not instance.staff_created:
        contents = str(instance.student) + '(' + str(instance.student.roll_no) +') requested onduty'
        handle_user_notification(
            heading='Fullday Onduty request',
            contents=contents,
            url=settings.CURRENT_HOST_NAME[:-1] + reverse('viewodrequests'),
            custom_user_instances=instance.staff.user,
        )
        
@receiver(post_save, sender=issue_onduty)
def notify_granted_od(sender, instance, **kwargs):
    contents = ''
    if type(instance.date) is str:
        contents += datetime.strptime(instance.date,"%Y-%m-%d").strftime("%d/%m/%Y")
    else:
        contents += instance.date.strftime("%d/%m/%Y")
    contents += ' issued by ' + str(instance.staff)
    handle_user_notification(
        heading='Fullday Onduty Granted',
        contents=contents,
        url=settings.CURRENT_HOST_NAME[:-1] + reverse('mygrantedod'),
        custom_user_instances=instance.student.user,
    )
"""
