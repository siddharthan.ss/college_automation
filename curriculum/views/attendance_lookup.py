from django.http import JsonResponse
from accounts.models import department, batch
from common.utils.ReportTabsUtil import getStaffCoursesForSemester
from common.API.departmentAPI import getDepartmentOfPk, getCoreDepartments
from datetime import timedelta
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.shortcuts import render
from django.utils import timezone
from accounts.views.profile import studentProfile
from curriculum.models import attendance, semester,staff_course
from curriculum.views.common_includes import *
from curriculum.views.attendance_report import *


@login_required
@permission_required('curriculum.can_mark_attendance')
def custom_attendance_lookup(request, st_date=None, ed_date=None, data_for_pdf=False):
    departmentList = getCoreDepartments()
    batchList = batch.objects.all()
    if request.method == 'POST':
        if request.is_ajax():
            if request.POST.get('action') == 'getSemesterPlan':
                departmentPk = int(request.POST.get('departmentPk'))
                departmentInstance = getDepartmentOfPk(departmentPk)
                semesterList = []
                for eachSemester in semester.objects.filter(department=departmentInstance):
                    temp = {}
                    temp['displayText'] = eachSemester.getDisplayTextForPastReport()
                    temp['pk'] = eachSemester.pk
                    semesterList.append(temp)

                dictionary = {}
                dictionary['semesterList'] = semesterList

                return JsonResponse(dictionary)

            if request.POST.get('action') == 'getCourses':
                semesterPk = request.POST.get('semesterPk')
                semesterInstance = semester.objects.get(pk=semesterPk)

                courseList = []

                for eachCourse in getStaffCoursesForSemester(semesterInstance):
                    staffCourseInstance = eachCourse['staffCourseInstance']
                    temp = {}
                    temp['pk'] = staffCourseInstance.pk
                    temp['course'] = str(staffCourseInstance.course)
                    courseList.append(temp)

                dictionary = {}
                dictionary['courseList'] = courseList

                return JsonResponse(dictionary)
        
        if 'staffCoursePkOnCmalRequest' in request.POST:
            staffCoursePkOnCmalRequest = request.POST.get('staffCoursePkOnCmalRequest')
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePkOnCmalRequest)
            if (request.POST.get('start_date') and request.POST.get('end_date')) or data_for_pdf:
                start_date = datetime.strptime(request.POST.get('start_date'), "%Y-%m-%d").date()
                end_date = datetime.strptime(request.POST.get('end_date'), "%Y-%m-%d").date()

                if st_date:
                    start_date = datetime.strptime(st_date, "%Y-%m-%d").date()
                if ed_date:
                    end_date = datetime.strptime(ed_date, "%Y-%m-%d").date()

                print('selected_course = ' + str(staffCourseInstance.course))
                course_instance = staffCourseInstance.course
    
                semester_instance = staffCourseInstance.semester

                if semester_instance.start_term_1 is None:
                    msg = {
                        'page_title': 'Date Error',
                        'title': 'Start Date Not Found',
                        'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
                    }
                    return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

                if semester_instance.start_term_1 > start_date:
                    print('start date error')
                    modal = {
                        'heading': 'Date Error',
                        'body': 'The start date seems to be out of range for this subject',
                    }
                    return render(request, 'curriculum/custom_attendance_lookup.html', {
                        'staffCoursePkOnCmalRequest': int(staffCoursePkOnCmalRequest),
                        'toggle_model': 'true', 'modal': modal})
    
                if semester_instance.end_term_3 and semester_instance.end_term_3 < end_date or (
                            datetime.today().date() < end_date):
                    print('end date error')
                    modal = {
                        'heading': 'Date Error',
                        'body': 'The end date seems to be out of range for this subject',
                    }
                    return render(request, 'curriculum/custom_attendance_lookup.html', {
                        'staffCoursePkOnCmalRequest': int(staffCoursePkOnCmalRequest),
                        'toggle_model': 'true', 'modal': modal})
    
                result_dict = overall_attendance_report(start_date, end_date, course_instance, semester_instance)

                if data_for_pdf:
                    context_data = {
                        'final_result': result_dict,
                        'selected_course': course_instance,
                        'current_start_date': start_date, 'current_end_date': end_date,
                    }
                    return context_data
    
                return render(request, 'curriculum/custom_attendance_lookup.html',
                            {
                            'staffCoursePkOnCmalRequest': int(staffCoursePkOnCmalRequest),
                            'final_result': result_dict,
                            'current_start_date': start_date, 'current_end_date': end_date,
                            'selected_course_name': staffCourseInstance.course,
                            })

            return render(request, 'curriculum/custom_attendance_lookup.html',
                              {
                                  'staffCoursePkOnCmalRequest': int(staffCoursePkOnCmalRequest),
                                  'selected_course_name': staffCourseInstance.course,
                                  'batchList': batchList,
                                  'departmentList': departmentList,
                            })
        
    return render(request, 'curriculum/custom_attendance_lookup.html',
                  {
                      'batchList': batchList,
                      'departmentList': departmentList,
                  })



@login_required
@permission_required('curriculum.can_mark_attendance')


def consolidated_attendance_lookup(request, data_for_pdf=False):
    departmentList = getCoreDepartments()
    batchList = batch.objects.all()
    if request.method == 'POST':
        if request.is_ajax():
            if request.POST.get('action') == 'getSemesterPlan':
                departmentPk = int(request.POST.get('departmentPk'))
                departmentInstance = getDepartmentOfPk(departmentPk)
                semesterList = []
                for eachSemester in semester.objects.filter(department=departmentInstance):
                    temp = {}
                    temp['displayText'] = eachSemester.getDisplayTextForPastReport()
                    temp['pk'] = eachSemester.pk
                    semesterList.append(temp)

                dictionary = {}
                dictionary['semesterList'] = semesterList

                return JsonResponse(dictionary)

            if request.POST.get('action') == 'getCourses':
                semesterPk = request.POST.get('semesterPk')
                semesterInstance = semester.objects.get(pk=semesterPk)

                courseList = []

                for eachCourse in getStaffCoursesForSemester(semesterInstance):
                    staffCourseInstance = eachCourse['staffCourseInstance']
                    temp = {}
                    temp['pk'] = staffCourseInstance.pk
                    temp['course'] = str(staffCourseInstance.course)
                    courseList.append(temp)

                dictionary = {}
                dictionary['courseList'] = courseList

                return JsonResponse(dictionary)

        if 'staffCoursePkOnCsalRequest' in request.POST:
            staffCoursePkOnCsalRequest = request.POST.get('staffCoursePkOnCsalRequest')
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePkOnCsalRequest)
            course_instance = staffCourseInstance.course
            timetable_instances = time_table.objects.filter(course=course_instance)

            pprint(timetable_instances)

            if not timetable_instances:
                msg = {
                    'page_title': 'Allotment Error',
                    'title': 'Timetable not Found',
                    'description': 'No timetable has been/ assigned to this course',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

            semester_plan_instance = staffCourseInstance.semester

            start_date = getattr(semester_plan_instance, 'start_term_1')
            if start_date is None:
                msg = {
                    'page_title': 'Date Error',
                    'title': 'Start Date Not Found',
                    'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

            end_date = getattr(semester_plan_instance, 'end_term_3')
            if end_date is None:
                end_date = timezone.now().date()

            result_dict = overall_attendance_report(start_date, end_date, course_instance, semester_plan_instance)

            print('start_date = ' + str(start_date))

            if data_for_pdf:
                context_data = {
                    'final_result': result_dict,
                    'selected_course': course_instance,
                    'current_start_date': start_date, 'current_end_date': end_date,
                }
                return context_data
        
            return render(request, 'curriculum/consolidated_attendance_lookup.html',
                  {
                   'final_result': result_dict, 'staffCoursePkOnCsalRequest': int(staffCoursePkOnCsalRequest),
                   'current_start_date': start_date, 'current_end_date': end_date,
                   'selected_course_name':staffCourseInstance.course
                   })


    return render(request, 'curriculum/consolidated_attendance_lookup.html',
              {
                  'batchList': batchList,
                  'departmentList': departmentList
              })
                  
@login_required
@permission_required('curriculum.can_mark_attendance')

def weekly_attendance_lookup(request, start_date=None, end_date=None, data_for_pdf=False):
    departmentList = getCoreDepartments()
    batchList = batch.objects.all()
    if request.method == 'POST':
        if request.is_ajax():
            if request.POST.get('action') == 'getSemesterPlan':
                departmentPk = int(request.POST.get('departmentPk'))
                departmentInstance = getDepartmentOfPk(departmentPk)
                semesterList = []
                for eachSemester in semester.objects.filter(department=departmentInstance):
                    temp = {}
                    temp['displayText'] = eachSemester.getDisplayTextForPastReport()
                    temp['pk'] = eachSemester.pk
                    semesterList.append(temp)

                dictionary = {}
                dictionary['semesterList'] = semesterList

                return JsonResponse(dictionary)

            if request.POST.get('action') == 'getCourses':
                semesterPk = request.POST.get('semesterPk')
                semesterInstance = semester.objects.get(pk=semesterPk)

                courseList = []

                for eachCourse in getStaffCoursesForSemester(semesterInstance):
                    staffCourseInstance = eachCourse['staffCourseInstance']
                    temp = {}
                    temp['pk'] = staffCourseInstance.pk
                    temp['course'] = str(staffCourseInstance.course)
                    courseList.append(temp)

                dictionary = {}
                dictionary['courseList'] = courseList

                return JsonResponse(dictionary)

        if 'staffCoursePkOnWalRequest' in request.POST:
            staffCoursePkOnWalRequest = request.POST.get('staffCoursePkOnWalRequest')
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePkOnWalRequest)
            weeks_so_far = form_weeks(staffCourseInstance.semester, staffCourseInstance.course)
            if weeks_so_far is None:
                msg = {
                    'page_title': 'Date Error',
                    'title': 'Start Date Not Found',
                    'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

            if start_date is None and end_date is None:
                start_date = weeks_so_far[-1]['start_day']
                end_date = weeks_so_far[-1]['end_day']
            if request.POST.get('date_range'):
                dates = [value for value in request.POST.get('date_range').split('_')]
                start_date = datetime.strptime(dates[0], "%Y-%m-%d").date()
                end_date = datetime.strptime(dates[1], "%Y-%m-%d").date()
                pprint(dates)
                print((request.POST.get('date_range')))
        # Wpass

            result_dict = attendance_report(start_date, end_date, staffCourseInstance.course,
                                    staffCourseInstance.semester)

            print('start_date = ' + str(start_date))
            print(staffCourseInstance.semester)

            if data_for_pdf:
                context_data = {
                    'date_and_hour_list': result_dict['date_and_hour_list'],
                    'final_result': result_dict['processed_result'],
                    'selected_course': staffCourseInstance.course,
                    'current_start_date': datetime.strptime(start_date, "%Y-%m-%d"),
                    'current_end_date': datetime.strptime(end_date, "%Y-%m-%d"),
                }
                return context_data

            return render(request, 'curriculum/weekly_attendance_lookup.html',
                          {
                           'date_and_hour_list': result_dict['date_and_hour_list'],
                           'final_result': result_dict['processed_result'],
                           'staffCoursePkOnWalRequest': int(staffCoursePkOnWalRequest),
                           'selected_semester_pk': int(staffCourseInstance.semester.pk),
                           'selected_course': (staffCourseInstance.course.pk),
                           'selected_course_name': (staffCourseInstance.course),
                           'weeks': weeks_so_far, 'current_start_date': start_date,
                           'current_end_date': end_date,
                        })
            

    return render(request, 'curriculum/weekly_attendance_lookup.html',
                  {
                      'batchList': batchList,
                      'departmentList': departmentList
                  })
