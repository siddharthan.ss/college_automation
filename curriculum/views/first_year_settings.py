from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from accounts.models import department, site_settings


@login_required
@permission_required('accounts.can_allot_first_year_managing_department')
def first_year_settings(request):
    selected_department_id = None

    if request.method == "POST":
        selected_department_id = request.POST.get('selected_department')
        query = site_settings.objects.filter(key="FIRST_YEAR_MANAGING_DEPARTMENT")
        if query.exists():
            required_instance = query.get()
            required_instance.value = str(selected_department_id)
            required_instance.save()
        else:
            site_settings.objects.create(
                key="FIRST_YEAR_MANAGING_DEPARTMENT",
                value=str(selected_department_id),
            )

    query = site_settings.objects.filter(key="FIRST_YEAR_MANAGING_DEPARTMENT")
    if query.exists():
        selected_department_id = query.get().value

    print(selected_department_id)

    if selected_department_id:
        selected_department_id = int(selected_department_id)
    else:
        selected_department_id = None

    data = {
        'all_non_core_departments': department.objects.filter(is_core=False),
        'selected_department': selected_department_id,
    }

    return render(
        request,
        'first_year_settings/first_year_settings.html',
        data
    )
