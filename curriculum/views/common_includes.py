from datetime import datetime
from datetime import date
from pprint import pprint

from django.db.models import Q
from django.http import JsonResponse

from accounts.models import batch, staff, department, student, active_batches, site_settings, department_programmes, \
    sub_department, CustomUser
from curriculum.models import staff_course, semester_migration, student_enrolled_courses, \
    time_table, day, batch_split, \
    department_sections, section_students, programme_coordinator, hod, unit_test_timetable, \
    courses, semester
from curriculum.views.holidays import is_holiday
from enrollment.models import course_enrollment

from common.API.departmentAPI import getAllDepartments

def is_ut_date(semester, date, hour):
    if hour < 5:
        fn_or_an = 'FN'
    else:
        fn_or_an = 'AN'

    print('is_ut = ' + str(date) + ' ' + str(hour) + str(
        unit_test_timetable.objects.filter(date=date).filter(fn_or_an=fn_or_an).exists()))
    if unit_test_timetable.objects.filter(semester=semester).filter(date=date).filter(fn_or_an=fn_or_an).exists():
        return True

    return False


def get_active_batches_for_section_split(user_instance):
    staff_query = staff.objects.filter(user=user_instance)
    if staff_query.exists():
        staff_instance = staff_query.get()
    else:
        return None

    pc_departments = get_departments_of_pc(staff_instance)

    # for core staffs(PC)
    active_batches_query = active_batches.objects.filter(
        department__in=pc_departments
    ).exclude(
        batch__programme='Phd'
    ).order_by(
        '-batch__programme', '-batch__start_year',
    )

    list_of_batches = []
    for entry in active_batches_query:
        temp = {}
        temp['active_batch_instance'] = entry
        temp['display_text'] = str(entry.department.acronym) + ' S' + str(
            entry.current_semester_number_of_this_batch) + ' (' + str(
            entry.batch.programme) + ')'
        list_of_batches.append(temp)

    # for non-core staffs(PC)
    if staffBelongsToNonCore(staff_instance):
        if staffNotEligible(staff_instance):
            return None
        else:
            active_batches_query = active_batches.objects.filter(
                current_semester_number_of_this_batch__lte=2
            ).exclude(
                batch__programme='Phd'
            ).exclude(
                batch__programme='PG'
            ).order_by(
                '-batch__programme', '-batch__start_year',
            )

            for entry in active_batches_query:
                temp = {}
                temp['active_batch_instance'] = entry
                temp['display_text'] = str(entry.department.acronym) + ' S' + str(
                    entry.current_semester_number_of_this_batch)
                list_of_batches.append(temp)

    return list_of_batches


def get_departments_of_fa(staff_instance):
    list_of_fa_departments = []

    for entry in semester_migration.objects.filter(semester__faculty_advisor=staff_instance):
        if entry.semester.department not in list_of_fa_departments:
            list_of_fa_departments.append(entry.semester.department)

    return list_of_fa_departments


def get_departments_of_hod(staff_instance):
    list_of_hod_departments = []

    for entry in hod.objects.filter(staff=staff_instance):
        if entry.department not in list_of_hod_departments:
            list_of_hod_departments.append(entry.department)

    return list_of_hod_departments


def get_departments_of_pc(pc_staff_instance):
    pc_table_query = programme_coordinator.objects.filter(programme_coordinator_staffs=pc_staff_instance)

    list_of_pc_departments = []
    for entry in pc_table_query:
        list_of_pc_departments.append(entry.department_programme.department)

    return list_of_pc_departments


def get_department_programmes_of_pc(pc_staff_instance):
    pc_table_query = programme_coordinator.objects.filter(programme_coordinator_staffs=pc_staff_instance)

    list_of_pc_departments_programmes = []
    for entry in pc_table_query:
        list_of_pc_departments_programmes.append(entry.department_programme)

    return list_of_pc_departments_programmes


def get_active_batch_tabs(user_instance):
    staff_query = staff.objects.filter(user=user_instance)
    if staff_query.exists():
        staff_instance = staff_query.get()
    else:
        return None

    list_of_pc_department_prgrammes = get_department_programmes_of_pc(staff_instance)
    print('list of pc departments programmes')
    print(list_of_pc_department_prgrammes)

    department_sections_query = department_sections.objects.none()
    for entry in list_of_pc_department_prgrammes:
        active_batches_query = active_batches.objects.filter(
            department=entry.department,
            programme=entry.programme.acronym
        ).values_list('batch')

        print('active batches query')
        print(active_batches_query)

        additional_department_section_query = department_sections.objects.filter(
            department=entry.department
        ).filter(
            batch__in=active_batches_query
        ).filter(
            batch__programme=entry.programme.acronym
        ).order_by(
            '-batch__programme', '-batch__start_year',
        )
        department_sections_query |= additional_department_section_query

    flag_non_core_staff = False

    if staffBelongsToNonCore(staff_instance):
        if staffNotEligible(staff_instance):
            return None
        else:
            active_batches_query = active_batches.objects.filter(
                current_semester_number_of_this_batch__lte=2
            ).values_list('batch')
            department_sections_query = department_sections.objects.filter(
                batch__in=active_batches_query
            ).exclude(
                batch__programme='PG'
            ).exclude(
                batch__programme='Phd'
            ).order_by(
                'department__acronym'
            )
            flag_non_core_staff = True

    print('department sections query')
    print(department_sections_query)

    list_of_department_sections = []

    for entry in department_sections_query:
        temp = {}
        temp['department_section_instance'] = entry
        active_batches_inst = active_batches.objects.filter(
            department=entry.department
        ).get(
            batch=entry.batch
        )
        # if not flag_non_core_staff:
        #     if check_department_batch_has_multiple_sections(entry.department, entry.batch):
        #         temp['display_text'] = 'S' + str(active_batches_inst.current_semester_number_of_this_batch) \
        #                                + '-' + str(active_batches_inst.programme) + '-' + str(entry.section_name)
        #     else:
        #         temp['display_text'] = 'S' + str(active_batches_inst.current_semester_number_of_this_batch) \
        #                                + '-' + str(active_batches_inst.programme)
        # else:
        if check_department_batch_has_multiple_sections(entry.department, entry.batch):
            temp['display_text'] = str(entry.department.acronym) + ' ' + str(entry.section_name) + ' S' + str(
                active_batches_inst.current_semester_number_of_this_batch) + '-' + str(active_batches_inst.programme)
        else:
            temp['display_text'] = str(entry.department.acronym) + ' S' + str(
                active_batches_inst.current_semester_number_of_this_batch) + '-' + str(active_batches_inst.programme)

        list_of_department_sections.append(temp)

    pprint(list_of_department_sections)
    return list_of_department_sections


def get_active_semesters_tabs_of_department(department_instance):
    active_semester_query = semester_migration.objects.filter(department=department_instance).order_by(
        '-semester__batch__programme', '-semester__batch__start_year',
    )

    list_of_active_semesters = []
    for entry in active_semester_query:
        temp = {}
        temp['semester_instance_pk'] = str(entry.semester.pk)
        temp['semester_instance'] = entry.semester
        temp['display_text'] = 'SEM ' + str(entry.semester_number) + ' ( ' + str(
            entry.semester.batch.programme) + ' )'
        if check_department_batch_has_multiple_sections(entry.department, entry.semester.batch):
            temp['display_text'] += '-' + str(entry.semester.department_section.section_name)
        list_of_active_semesters.append(temp)

    return list_of_active_semesters


def get_active_semesters_tabs_for_overall_reports(request):
    staff_query = staff.objects.filter(user=request.user)
    if staff_query.exists():
        staff_instance = staff_query.get()
    else:
        return None

    flag_first_year_staff = False

    if staffBelongsToNonCore(staff_instance):
        if staffNotEligible(staff_instance) and not request.user.groups.filter(name="faculty_advisor").exists():
            return None
        else:
            active_semester_query = semester_migration.objects.filter(semester__batch__programme='UG').filter(
                semester__semester_number__lte=2)
            flag_first_year_staff = True
    else:
        department_instance = None
        if request.user.groups.filter(name="hod").exists():
            department_instance = get_departments_of_hod(staff_instance)
        elif request.user.groups.filter(name="faculty_advisor").exists():
            department_instance = get_departments_of_fa(staff_instance)
        # returns active semesters for the corresponding department
        active_semester_query = semester_migration.objects.filter(department__in=department_instance)

        active_semester_query = active_semester_query.order_by(
            '-semester__batch__programme', '-semester__batch__start_year',
        )

    list_of_active_semesters = []
    for entry in active_semester_query:
        temp = {}
        temp['semester_instance'] = entry.semester
        if flag_first_year_staff:
            temp['display_text'] = str(entry.department.acronym)
            temp['display_text'] += ' S' + str(entry.semester_number)
            if check_department_batch_has_multiple_sections(entry.department, entry.semester.batch):
                temp['display_text'] += '-' + str(entry.semester.department_section.section_name)
        else:
            temp['display_text'] = str(entry.department.acronym) + ' '
            temp['display_text'] += 'S' + str(entry.semester_number) + ' ( ' + str(
                entry.semester.batch.programme) + ' )'
            if check_department_batch_has_multiple_sections(entry.department, entry.semester.batch):
                temp['display_text'] += '-' + str(entry.semester.department_section.section_name)
        list_of_active_semesters.append(temp)

    return list_of_active_semesters


def get_active_semesters_tabs(request):
    staff_query = staff.objects.filter(user=request.user)
    if staff_query.exists():
        staff_instance = staff_query.get()
    else:
        return None

    flag_first_year_staff = False

    if staffBelongsToNonCore(staff_instance):
        if staffNotEligible(staff_instance) and not request.user.groups.filter(name="faculty_advisor").exists():
            return None
        else:
            active_semester_query = semester_migration.objects.filter(semester__batch__programme='UG').filter(
                semester__semester_number__lte=2)
            flag_first_year_staff = True
    else:
        department_instance = get_departments_of_pc(staff_instance)
        # returns active semesters for the corresponding department
        active_semester_query = semester_migration.objects.filter(department__in=department_instance).order_by(
            '-semester__batch__programme', '-semester__batch__start_year',
        )

    list_of_active_semesters = []
    for entry in active_semester_query:
        temp = {}
        temp['semester_instance'] = entry.semester
        if flag_first_year_staff:
            temp['display_text'] = str(entry.department.acronym)
            temp['display_text'] += ' S' + str(entry.semester_number)
            if check_department_batch_has_multiple_sections(entry.department, entry.semester.batch):
                temp['display_text'] += '-' + str(entry.semester.department_section.section_name)
        else:
            temp['display_text'] = str(entry.department.acronym) + ' '
            temp['display_text'] += 'S' + str(entry.semester_number) + ' ( ' + str(
                entry.semester.batch.programme) + ' )'
            if check_department_batch_has_multiple_sections(entry.department, entry.semester.batch):
                temp['display_text'] += '-' + str(entry.semester.department_section.section_name)
        list_of_active_semesters.append(temp)

    return list_of_active_semesters


# this function is used in testing since no request object is avaialble
# all core logic depends on the previous function
def get_test_active_semesters_tabs(user_instance):
    staff_query = staff.objects.filter(user=user_instance)
    if staff_query.exists():
        staff_instance = staff_query.get()
    else:
        return None

    flag_first_year_staff = False

    if staffBelongsToNonCore(staff_instance):
        if staffNotEligible(staff_instance) and not user_instance.groups.filter(name="faculty_advisor").exists():
            return None
        else:
            active_semester_query = semester_migration.objects.filter(semester__batch__programme='UG').filter(
                semester__semester_number__lte=2)
            flag_first_year_staff = True
    else:
        department_instance = get_departments_of_pc(staff_instance)
        # returns active semesters for the corresponding department
        active_semester_query = semester_migration.objects.filter(department__in=department_instance).order_by(
            '-semester__batch__programme', '-semester__batch__start_year',
        )

    list_of_active_semesters = []
    for entry in active_semester_query:
        temp = {}
        temp['semester_instance'] = entry.semester
        if flag_first_year_staff:
            temp['display_text'] = str(entry.department.acronym)
            temp['display_text'] += ' S' + str(entry.semester_number)
            if check_department_batch_has_multiple_sections(entry.department, entry.semester.batch):
                temp['display_text'] += '-' + str(entry.semester.department_section.section_name)
        else:
            temp['display_text'] = str(entry.department.acronym) + ' '
            temp['display_text'] += 'S' + str(entry.semester_number) + ' ( ' + str(
                entry.semester.batch.programme) + ' )'
            if check_department_batch_has_multiple_sections(entry.department, entry.semester.batch):
                temp['display_text'] += '-' + str(entry.semester.department_section.section_name)
        list_of_active_semesters.append(temp)

    return list_of_active_semesters


# currently not in use except signup and faculty advisor assignment
def get_current_batches(department, get_phd=False):
    """
    The oldest batch currently in college will be of the final year BE/B.Tech students
    Their end of batch year is calculated below
    """
    current_active_batches = active_batches.objects.filter(department=department)
    current_batch_objects = current_active_batches.filter(programme='UG').order_by('-batch__start_year')

    list_of_batches = []

    for entry in current_batch_objects:
        # print(vars(entry))
        batchinstance = entry.batch
        thisbatch = {}
        thisbatch['instance'] = batchinstance
        thisbatch['programme'] = getattr(batchinstance, 'programme')
        thisbatch['start_year'] = batchinstance.get_start_year()
        thisbatch['end_year'] = getattr(batchinstance, 'end_year')
        thisbatch['batch_obj'] = getattr(batchinstance, 'pk')
        # if (entry.current_semester_number_of_this_batch == 7 or
        #             entry.current_semester_number_of_this_batch == 8):
        #     text = 'IV Year'
        # elif (entry.current_semester_number_of_this_batch == 5 or
        #               entry.current_semester_number_of_this_batch == 6):
        #     text = 'III Year'
        # elif (entry.current_semester_number_of_this_batch == 4 or
        #               entry.current_semester_number_of_this_batch == 3):
        #     text = 'II Year'
        # elif (entry.current_semester_number_of_this_batch == 1 or
        #               entry.current_semester_number_of_this_batch == 2):
        #     text = 'I Year'

        thisbatch['display_text'] = 'SEM ' + str(entry.current_semester_number_of_this_batch) + ' ( ' + thisbatch[
            'programme'] + ' )'
        list_of_batches.append(thisbatch)

    current_active_batches = active_batches.objects.filter(department=department)
    current_batch_objects = current_active_batches.filter(programme='PG').order_by('-batch__start_year')

    for entry in current_batch_objects:
        batchinstance = entry.batch
        thisbatch = {}
        thisbatch['instance'] = batchinstance
        thisbatch['programme'] = getattr(batchinstance, 'programme')
        start_year_of_this_batch = batchinstance.get_start_year()
        thisbatch['start_year'] = start_year_of_this_batch
        thisbatch['end_year'] = getattr(batchinstance, 'end_year')
        thisbatch['programme'] = getattr(batchinstance, 'programme')
        thisbatch['batch_obj'] = getattr(batchinstance, 'pk')
        # if (entry.current_semester_number_of_this_batch == 3 or
        #             entry.current_semester_number_of_this_batch == 4):
        #     text = 'II Year'
        # elif (entry.current_semester_number_of_this_batch == 2 or
        #               entry.current_semester_number_of_this_batch == 1):
        #     text = 'I Year'
        thisbatch['display_text'] = 'S' + str(entry.current_semester_number_of_this_batch) + ' ( ' + thisbatch[
            'programme'] + ' )'
        list_of_batches.append(thisbatch)

    if get_phd:
        current_active_batches = active_batches.objects.filter(department=department)
        current_batch_objects = current_active_batches.filter(programme='Phd').order_by('-batch__start_year')

        for entry in current_batch_objects:
            batchinstance = entry.batch
            thisbatch = {}
            thisbatch['programme'] = getattr(batchinstance, 'programme')
            start_year_of_this_batch = batchinstance.get_start_year()
            thisbatch['start_year'] = start_year_of_this_batch
            thisbatch['end_year'] = getattr(batchinstance, 'end_year')
            thisbatch['programme'] = getattr(batchinstance, 'programme')
            thisbatch['batch_obj'] = getattr(batchinstance, 'pk')
            # if (entry.current_semester_number_of_this_batch == 3 or
            #             entry.current_semester_number_of_this_batch == 4):
            #     text = 'II Year'
            # elif (entry.current_semester_number_of_this_batch == 2 or
            #               entry.current_semester_number_of_this_batch == 1):
            #     text = 'I Year'
            thisbatch['display_text'] = 'S' + str(entry.current_semester_number_of_this_batch) + ' ( ' + \
                                        thisbatch['programme'] + ' )'
            list_of_batches.append(thisbatch)
    # print(list_of_batches)
    return list_of_batches


def get_current_semester_of_given_batch(dept_obj, section_obj):
    batch_object = section_obj.batch
    acitve_batch_inst = active_batches.objects.filter(department=dept_obj).get(batch=batch_object)
    current_sem_of_this_batch = acitve_batch_inst.current_semester_number_of_this_batch
    # print('current_sem_of_this_batch='+str(current_sem_of_this_batch))
    try:
        sem_migraton_obj = semester_migration.objects.filter(semester__batch=batch_object).filter(
            department=dept_obj).filter(
            semester__department_section=section_obj
        ).get(semester_number=current_sem_of_this_batch)
        # print(sem_migraton_obj.semester)
        return sem_migraton_obj.semester
    except semester_migration.DoesNotExist:
        return None


def get_current_semesters_faculty_advisors(request):
    if request.method == 'POST':
        if request.is_ajax():

            dictionary = {}  # to send return data
            got_programme = request.POST.get('programme')
            got_start_year = request.POST.get('start_year')
            got_end_year = request.POST.get('end_year')

            # print(got_programme)
            # print(got_start_year)
            # print(got_end_year)

            batch_instance = batch.objects.filter(programme=got_programme).filter(start_year=got_start_year).get(
                end_year=got_end_year)
            print(vars(batch_instance))

            current_user_instance = staff.objects.all().get(user=request.user)

            department_instance = getattr(current_user_instance, 'department')
            print(department_instance)

            current_semester_inst = get_current_semester_of_given_batch(department_instance, batch_instance)
            # print(current_semester)


            current_semester_plan_instance = current_semester_inst
            print((current_semester_plan_instance))
            if current_semester_plan_instance is None:
                return JsonResponse(dictionary)
            print(vars(current_semester_plan_instance))
            # print(vars(current_semester_plan_instance.faculty_advisor.all()[0]))

            faculty_advisors = current_semester_plan_instance.faculty_advisor.all()

            list = []

            for faculty in faculty_advisors:
                pk = getattr(faculty, 'pk')
                # print(pk)
                list.append(pk)

            dictionary['list'] = list

            start_term_1 = getattr(current_semester_plan_instance, 'start_term_1')
            if start_term_1:
                dictionary['start_term_1'] = start_term_1.strftime('%d/%m/%Y')
            end_term_1 = getattr(current_semester_plan_instance, 'end_term_1')
            if end_term_1:
                dictionary['end_term_1'] = end_term_1.strftime('%d/%m/%Y')
            start_term_2 = getattr(current_semester_plan_instance, 'start_term_2')
            if start_term_2:
                dictionary['start_term_2'] = start_term_2.strftime('%d/%m/%Y')
            end_term_2 = getattr(current_semester_plan_instance, 'end_term_2')
            if end_term_2:
                dictionary['end_term_2'] = end_term_2.strftime('%d/%m/%Y')
            start_term_3 = getattr(current_semester_plan_instance, 'start_term_3')
            if start_term_3:
                dictionary['start_term_3'] = start_term_3.strftime('%d/%m/%Y')
            end_term_3 = getattr(current_semester_plan_instance, 'end_term_3')
            if end_term_3:
                dictionary['end_term_3'] = end_term_3.strftime('%d/%m/%Y')
            return JsonResponse(dictionary)


def get_facutly_advisors(department_instance, section_instance, semester_number=None):
    current_semester_plan_instance = get_current_semester_of_given_batch(dept_obj=department_instance,
                                                                         section_obj=section_instance)
    if not current_semester_plan_instance:
        return None

    faculty_advisors = current_semester_plan_instance.faculty_advisor.all()

    fa_obj_list = []

    for faculty in faculty_advisors:
        fa_obj_list.append(faculty)

    return fa_obj_list


def get_staff_course_allotment_instances_for_staff(faculty_instance):
    list_of_active_semesters = semester_migration.objects.all().values_list('semester')

    staff_course_instances = staff_course.objects.filter(staffs=faculty_instance).filter(
        semester__in=list_of_active_semesters
    )

    """
    # elective courses
    elective_course_instances = elective_course_staff.objects.filter(staff=faculty_instance)
    for entry in elective_course_instances:
        # print(entry)
        list_of_course_instances.append(entry.course)

    # one credit courses
    one_credit_course_instances = one_credit_course_staff.objects.filter(staff=faculty_instance)
    for entry in one_credit_course_instances:
        list_of_course_instances.append(entry.course)
    """

    pprint(staff_course_instances)

    return staff_course_instances


def check_department_batch_has_multiple_sections(department_instance, batch_instance):
    if department_sections.objects.filter(
            department=department_instance
    ).filter(
        batch=batch_instance
    ).count() > 1:
        return True

    else:
        return False


def get_course_allotments_for_overall_report(staff_course_instances):
    final_list = []
    for entry in staff_course_instances:
        temp = {}
        temp['staff_course_instance'] = entry
        # pprint(temp['staff_course_instance'].course.subject_type)

        temp['display_text'] = str(entry.semester.department.acronym) + '-'

        department_instance = entry.semester.department
        batch_instance = entry.semester.batch
        if check_department_batch_has_multiple_sections(department_instance, batch_instance):
            temp['display_text'] += str(entry.semester.department_section.section_name) + '-'
            temp['section'] = str(entry.semester.department_section.section_name)

        if entry.course.code != '':
            temp['display_text'] += str(entry.course.code)
        else:
            temp['display_text'] += str(entry.course.course_id)

        final_list.append(temp)

    return final_list


def get_current_course_allotment_with_display_text(faculty_instance):
    staff_course_instances = get_staff_course_allotment_instances_for_staff(faculty_instance)

    final_list = []
    for entry in staff_course_instances:
        temp = {}
        temp['staff_course_instance'] = entry
        # pprint(temp['staff_course_instance'].course.subject_type)

        temp['display_text'] = str(entry.semester.department.acronym) + '-'

        department_instance = entry.semester.department
        batch_instance = entry.semester.batch
        if check_department_batch_has_multiple_sections(department_instance, batch_instance):
            temp['display_text'] += str(entry.semester.department_section.section_name) + '-'
            temp['section'] = str(entry.semester.department_section.section_name)

        if entry.course.code != '':
            temp['display_text'] += str(entry.course.code)
        else:
            temp['display_text'] += str(entry.course.course_id)

        final_list.append(temp)

    return final_list


# def get_coureses_handled_by_faculty_this_sem(faculty_instance):
#     """
#     currently we dont have current semester duration recorded
#     once it is recorded refer that table to limit results
#     """
#
#     list_of_courses_handled_by_factuly = []
#
#     list_of_course_instances = daf(faculty_instance)
#     # pprint(list_of_course_instances)
#     for entry in list_of_course_instances:
#         # print(entry.subject_type)
#         temp = {}
#         course_instance = entry
#         subject_code = getattr(course_instance, 'course_id')
#         department_instance = getattr(course_instance, 'department')
#         # print(subject_code)
#         temp['subject_code'] = subject_code
#         temp['category'] = entry.subject_type
#         temp['department'] = getattr(department_instance, 'acronym')
#         temp['short_code'] = getattr(course_instance, 'code')
#         temp['regulation'] = getattr(course_instance, 'regulation')
#         temp['subject_type'] = getattr(course_instance, 'subject_type')
#         list_of_courses_handled_by_factuly.append(temp)
#
#     # print(list_of_courses_handled_by_factuly)
#     return list_of_courses_handled_by_factuly


def get_list_of_students_of_semester_queryset(semester_instance):
    student_query = section_students.objects.filter(
        section=semester_instance.department_section
    ).filter(
        student__user__is_approved=True
    ).order_by(
        'student__roll_no'
    )
    student_list = []
    print('student_list')
    pprint(student_list)
    for entry in student_query:
        if entry.student not in student_list:
            student_list.append(entry.student)
    return student_list


def get_list_of_students_of_semester(semester_instance):
    list_of_students = []
    student_query = get_list_of_students_of_semester_queryset(semester_instance)

    for stud in student_query:
        temp_list = {}
        temp_list['student_obj'] = stud
        temp_list['registration_number'] = stud.roll_no
        list_of_students.append(temp_list)

    return list_of_students


def get_list_of_student_queryset(semester_instance, course_instance):
    student_list = []

    if course_instance.regulation.start_year < 2016:
        course_type = course_instance.is_elective | course_instance.is_open | course_instance.is_one_credit
        if not course_type:

            student_query = section_students.objects.filter(
                section=semester_instance.department_section
            ).filter(
                student__user__is_approved=True
            ).order_by(
                'student__roll_no'
            )

            for entry in student_query:
                student_list.append(entry.student)


                # print('Student list after query :')
                # print(vars(student_list))
        else:
            enrolled_student_query = student_enrolled_courses.objects.filter(
                studied_semester=semester_instance
            ).filter(
                course=course_instance
            ).filter(
                approved=True
            ).order_by(
                'student__roll_no'
            )
            for entry in enrolled_student_query:
                student_list.append(entry.student)
    else:
        enrolled_student_query = course_enrollment.objects.filter(
            semester=semester_instance,
            course=course_instance,
            approved=True
        ).order_by(
            'student__roll_no'
        )
        pprint(enrolled_student_query)
        for entry in enrolled_student_query:
            student_list.append(entry.student)

    pprint(student_list)
    return student_list


def get_list_of_students(semester_instance, course_instance):
    list_of_students = []

    student_list = get_list_of_student_queryset(semester_instance, course_instance)

    for stud in student_list:
        temp_list = {}
        temp_list['student_obj'] = stud
        # print(stud)
        student_register_number = getattr(stud, 'roll_no')
        temp_list['registration_number'] = student_register_number
        list_of_students.append(temp_list)
        # print(student_register_number)
    # print('Each student list : ')
    # print(list_of_students)

    return list_of_students


def get_departments():
    dept_avail = []
    for each in getAllDepartments():
        temp = {}
        temp['name'] = str(each)
        temp['acronym'] = str(each.acronym)
        temp['dept_id'] = str(each.id)
        dept_avail.append(temp)
    return dept_avail


def get_dispaly_text_for_batch_id(batch_id, department_inst):
    batch_instance = batch.objects.get(pk=batch_id)

    active_batch_instance = active_batches.objects.filter(
        department=department_inst
    ).get(
        batch=batch_instance
    )

    current_sem_of_batch = active_batch_instance.current_semester_number_of_this_batch

    if current_sem_of_batch <= 2:
        text = 'I Year '
    elif current_sem_of_batch <= 4:
        text = 'II Year '
    elif current_sem_of_batch <= 6:
        text = 'III Year '
    elif current_sem_of_batch <= 8:
        text = 'IV Year '

    text = text + (batch_instance.programme)

    return text


def get_weekday_of_day(day_instance):
    weekday_name = getattr(day_instance, 'day_name')
    if weekday_name == 'Monday':
        return 0
    elif weekday_name == 'Tuesday':
        return 1
    elif weekday_name == 'Wednesday':
        return 2
    elif weekday_name == 'Thursday':
        return 3
    elif weekday_name == 'Friday':
        return 4


def get_current_active_semester_of_this_dept(dept_inst):
    current_active_semester_of_this_dept = semester_migration.objects.filter(department=dept_inst)
    list = []

    for each in current_active_semester_of_this_dept:
        list.append(each.semester)

    return list


def get_all_current_active_semester():
    current_active_semester_of_this_dept = semester_migration.objects.all()
    list = []

    for each in current_active_semester_of_this_dept:
        list.append(each.semester)

    return list


def get_current_semester_plan_of_course(staff_instance, course_instance):
    semseter_of_course = course_instance.semester
    programme_of_course = course_instance.programme
    department_of_course = course_instance.department

    active_batches_instance = active_batches.objects.filter(
        department=department_of_course
    ).filter(
        programme=programme_of_course
    ).get(
        current_semester_number_of_this_batch=semseter_of_course
    )

    pprint(active_batches_instance.batch)

    semester_migration_instance = semester_migration.objects.filter(department=department_of_course).get(
        semester__batch=active_batches_instance.batch
    )
    print(semester_migration_instance)

    semester_plan_instance = semester_migration_instance.semester

    return semester_plan_instance


def get_current_semester_plan_of_student(student_inst):
    department_inst = student_inst.department
    section_students_instance = section_students.objects.get(
        student=student_inst
    )
    active_batches_instance = active_batches.objects.filter(
        department=department_inst
    ).get(
        batch=student_inst.batch
    )
    current_semester = active_batches_instance.current_semester_number_of_this_batch

    pprint(locals())

    semester_migration_inst = semester_migration.objects.filter(
        semester_number=current_semester
    ).get(
        semester__department_section=section_students_instance.section
    )

    return semester_migration_inst.semester


def get_course_of_student_given_date_and_hour(student_inst, date):
    current_semester_object = get_current_semester_plan_of_student(student_inst)
    day_inst = day.objects.get(day_name=date.strftime("%A"))
    timetable_instances = time_table.objects.filter(
        is_active=True
    ).filter(
        semester=current_semester_object
    ).filter(
        day=day_inst
    )

    list_of_courses = []
    for entry in timetable_instances:
        list_of_courses.append(entry.course)

    print(locals())

    return list_of_courses


# def get_todays_timetable(staff_inst):
#     if is_holiday(datetime.now().date()):
#         return None
#
#     today_weekday = datetime.now().strftime("%A")
#
#     try:
#         day_obj = day.objects.get(day_name=today_weekday)
#     except day.DoesNotExist:
#         return None
#
#     courses_allotment_inst = get_staff_course_allotment_instances_for_staff(staff_inst)
#     courses_handled_by_factulty_this_sem = []
#     for entry in courses_allotment_inst:
#         courses_handled_by_factulty_this_sem.append(entry.course)
#     # list_of_active_semesters = semester_migration.objects.all().values_list('semester')
#     list_of_active_semesters = []
#     for entry in courses_allotment_inst:
#         list_of_active_semesters.append(entry.semester)
#     list_of_timetables = []
#     for semester_entry in list_of_active_semesters:
#         time_table_slot_day_instance = getDayInstanceForSemesterInstance(semester_entry, datetime.now().date())
#         for time_table_entry in time_table.objects.filter(is_active=True).filter(semester=semester_entry).filter(
#                 course__in=courses_handled_by_factulty_this_sem).filter(day=time_table_slot_day_instance):
#             list_of_timetables.append(time_table_entry)
#
#     print('list of timetables')
#     pprint(locals())
#
#     ut_timetable_filtering = []
#     for tts in list_of_timetables:
#         if not is_ut_date(tts.semester, datetime.now().date(), tts.hour):
#             ut_timetable_filtering.append(tts)
#
#     list_of_timetables = ut_timetable_filtering
#
#     return list_of_timetables


def keyfn(x):
    return x['start_hour']


def format_discrete_hour_list(discrete_list):
    return_list = []

    for hour_entry in discrete_list:
        course_instance = hour_entry['course']
        temp = {}
        temp['course_id'] = course_instance.course_id
        if course_instance.code:
            temp['course_name'] = course_instance.code
        else:
            temp['course_name'] = course_instance.course_name
        temp['start_hour'] = hour_entry['hour']
        temp['end_hour'] = hour_entry['hour']
        if 'grant_period' in hour_entry:
            temp['grant_period'] = hour_entry['grant_period']
        temp['bulk'] = False
        temp['semester'] = hour_entry['semester'].pk
        temp['department_acronym'] = hour_entry['semester'].department.acronym
        temp['date'] = hour_entry['date']
        temp['display_date'] = hour_entry['date'].strftime("%d-%m-%Y")
        return_list.append(temp)

    return return_list


def split_continuous_hour_list(continuous_list):
    pprint(continuous_list)

    temp_dict = {}
    temp_list = []
    return_list = []

    saved_entry = None
    # saved_hour_query = None

    for hour_entries in continuous_list:
        if saved_entry is not None:
            if int(saved_entry['hour']) != 4 and int(hour_entries['hour']) - 1 == int(saved_entry['hour']) \
                    and saved_entry['course'] == hour_entries['course'] \
                    and saved_entry['semester'] == hour_entries['semester'] \
                    and saved_entry['date'] == hour_entries['date']:
                print('inside')
                if saved_entry not in temp_list:
                    temp_list.append(saved_entry)
                temp_list.append(hour_entries)
                print(temp_list)
            elif len(temp_list) > 0:
                print('temp list')
                print(temp_list)
                temp_dict[saved_entry['course'].course_id] = temp_list
                return_list.append(temp_dict)
                temp_list = []
                temp_dict = {}

        saved_entry = hour_entries
        print('saved hour')
        print(saved_entry)

    if len(temp_list) > 0:
        temp_dict[saved_entry['course'].course_id] = temp_list
        return_list.append(temp_dict)

    return return_list


def format_continuous_hour_list(splitted_list):
    pprint(splitted_list)

    return_list = []
    for each_group_list in splitted_list:
        temp_dict = {}
        for each_list in each_group_list.values():
            temp = {}
            temp['course_id'] = each_list[0]['course'].course_id
            if each_list[0]['course'].code:
                temp['course_name'] = each_list[0]['course'].code
            else:
                temp['course_name'] = each_list[0]['course'].course_name
            temp['start_hour'] = each_list[0]['hour']
            temp['end_hour'] = each_list[-1]['hour']

            lowest_reallow_time = None
            for each_hour in each_list:
                if 'grant_period' in each_hour:
                    if type(each_hour['grant_period']) is date:
                        print('instance of date ')
                        each_hour['grant_period'] = datetime.combine(each_hour['grant_period'], datetime.min.time())
            if 'grant_period' in each_list[0]:
                lowest_reallow_time = each_list[0]['grant_period']
            for each_hour in each_list:
                if 'grant_period' in each_hour:
                    if each_hour['grant_period'] < lowest_reallow_time:
                        lowest_reallow_time = each_hour['grant_period']
            if lowest_reallow_time is not None:
                temp['grant_period'] = lowest_reallow_time
            temp['bulk'] = True
            temp['semester'] = each_list[0]['semester'].pk
            temp['department_acronym'] = each_list[0]['semester'].department.acronym
            temp['date'] = each_list[0]['date']
            temp['display_date'] = each_list[0]['date'].strftime("%d-%m-%Y")
            return_list.append(temp)

    pprint(return_list)

    return return_list


def get_hours_grouped_for_day(list_of_date_hours):
    saved_entry = list_of_date_hours[0]
    pprint(saved_entry)

    discrete_hour_list = []
    continuous_hour_list = []
    for each_entry in list_of_date_hours:
        print('entry')
        print((each_entry))
        if int(saved_entry['hour']) != 4 and int(each_entry['hour']) - 1 == int(saved_entry['hour']) \
                and saved_entry['course'] == each_entry['course'] \
                and saved_entry['semester'] == each_entry['semester'] \
                and saved_entry['date'] == each_entry['date']:
            if saved_entry not in continuous_hour_list:
                discrete_hour_list.remove(saved_entry)
                continuous_hour_list.append(saved_entry)
            continuous_hour_list.append(each_entry)
        else:
            discrete_hour_list.append(each_entry)

        saved_entry = each_entry

    return_dict = {}
    return_dict['discrete_hour_list'] = format_discrete_hour_list(discrete_hour_list)
    return_dict['continuous_hour_list'] = format_continuous_hour_list(split_continuous_hour_list(continuous_hour_list))

    return_list = []
    for entry in return_dict['discrete_hour_list']:
        return_list.append(entry)

    for entry in return_dict['continuous_hour_list']:
        return_list.append(entry)

    return return_list


def get_list_of_hostel_students(department_inst, batch_inst, gender):
    student_list_query = student.objects.filter(department=department_inst,
                                                batch=batch_inst,
                                                student_type='hosteller',
                                                gender=gender).order_by('roll_no')

    return student_list_query


def get_filtered_students_for_lab(semester_inst, student_list, course_instane, attendance_date):
    day_name = attendance_date.strftime("%A")
    print(day_name)
    day_instance = day.objects.get(day_name=day_name)

    batch_split_query = batch_split.objects.filter(
        semester=semester_inst
    ).filter(
        course=course_instane
    ).filter(
        day=day_instance
    )
    filtered_list_of_students = []
    if batch_split_query.count() > 0:

        for batch_split_inst in batch_split_query:

            flag = False
            for entry in student_list:
                if entry['student_obj'] == batch_split_inst.start_student:
                    flag = True
                if flag:
                    filtered_list_of_students.append(entry)

                if entry['student_obj'] == batch_split_inst.end_student:
                    break

            return batch_split_inst, filtered_list_of_students
    else:
        batch_split_query = batch_split.objects.filter(
            semester=semester_inst,
            course=course_instane,
        )
        batch_split_inst = batch_split_query.first()
        flag = False
        for entry in student_list:
            if entry['student_obj'] == batch_split_inst.start_student:
                flag = True
            if flag:
                filtered_list_of_students.append(entry)

            if entry['student_obj'] == batch_split_inst.end_student:
                break

        return batch_split_inst, filtered_list_of_students


def get_batch_split_list(batch_split_pk, semester_inst, course_instane):
    filtered_list_of_students = []
    batch_split_query = batch_split.objects.filter(pk=batch_split_pk)
    if batch_split_query.exists():
        batch_split_inst = batch_split_query.get()
        flag = False
        student_list = get_list_of_students(semester_inst, course_instane)
        for entry in student_list:
            if entry['student_obj'] == batch_split_inst.start_student:
                flag = True
            if flag:
                filtered_list_of_students.append(entry)

            if entry['student_obj'] == batch_split_inst.end_student:
                break

        return filtered_list_of_students


def get_staff_handling_course(course_instance, semester_instance):
    staff_course_instance = staff_course.objects.filter(
        semester=semester_instance
    ).filter(
        course=course_instance
    )

    if staff_course_instance.count() < 1:
        return None
    else:
        staff_course_instance = staff_course_instance.get()

    list_of_staffs = []

    for entry in staff_course_instance.staffs.all():
        list_of_staffs.append(entry)

    return list_of_staffs


def staffBelongsToNonCore(staff_instance):
    if staff_instance.department.is_core == False:
        return True
    else:
        return False


def get_first_year_managing_department():
    managing_department_instance = None
    query = site_settings.objects.filter(key="FIRST_YEAR_MANAGING_DEPARTMENT")
    if query.exists():
        managing_department_id = query.get().value
        managing_department_instance = department.objects.get(pk=managing_department_id)
    return managing_department_instance


def staffNotEligible(staff_instance):
    managing_department = get_first_year_managing_department()
    if managing_department is not None:
        if staff_instance.department != managing_department:
            return True
        else:
            return False
    else:
        return True


def is_staff_first_year_chief(staff_instance):
    return not staffNotEligible(staff_instance)


def get_first_year_batch_of_department(department_instance, programme):
    try:
        return active_batches.objects.filter(
            department=department_instance
        ).filter(
            programme=programme
        ).get(
            current_semester_number_of_this_batch__lte=2
        )
    except active_batches.MultipleObjectsReturned:
        print('multiple objects')
        return None
    except active_batches.DoesNotExist:
        print('not exist')
        return None


def get_eligible_departments_for_pc(pc_staff_instance):
    pc_instances = programme_coordinator.objects.filter(programme_coordinator_staffs=pc_staff_instance)
    list_of_departments = []
    for entry in pc_instances:
        if entry.department_programme.department not in list_of_departments:
            list_of_departments.append(entry.department_programme.department)

    return list_of_departments


def get_eligible_programmes_for_pc(pc_staff_instance, department_inst):
    pc_instances = programme_coordinator.objects.filter(programme_coordinator_staffs=pc_staff_instance,
                                                        department_programme__department=department_inst)
    list_of_programmes = []
    for entry in pc_instances:
        if entry.department_programme.programme not in list_of_programmes:
            list_of_programmes.append(entry.department_programme.programme)

    return list_of_programmes


def get_eligible_department_programmes_for_hod(hod_staff_instance):
    hod_table_instances = hod.objects.filter(staff=hod_staff_instance)

    hod_departments = []
    for entry in hod_table_instances:
        hod_departments.append(entry.department)

    department_programmes_query = department_programmes.objects.filter(department__in=hod_departments).order_by(
        '-programme__acronym')

    return department_programmes_query


def get_department_programmes(department_instance):
    department_programmes_query = department_programmes.objects.filter(department=department_instance).order_by(
        '-programme__acronym')

    return department_programmes_query


def get_parent_department(department_instance):
    child_department_query = sub_department.objects.filter(child_departments=department_instance)
    if child_department_query.exists():
        return child_department_query.get().main_department
    else:
        return None


def get_child_departments(department_instance):
    main_department_query = sub_department.objects.filter(main_department=department_instance)
    if main_department_query.exists():
        return main_department_query.get().child_departments.all()
    else:
        return None


def get_all_eligible_staffs(department_instance):
    final_eligible_staff_queryset = None

    q1 = staff.objects.filter(department=department_instance, user__is_approved=True)
    final_eligible_staff_queryset = q1

    parent_department = get_parent_department(department_instance)
    if parent_department is not None:
        q2 = staff.objects.filter(department=parent_department, user__is_approved=True)
        final_eligible_staff_queryset |= q2

    child_departments = get_child_departments(department_instance)

    if child_departments is not None:
        for child_dept in child_departments:
            q3 = staff.objects.filter(department=child_dept, user__is_approved=True)
            final_eligible_staff_queryset |= q3

    return final_eligible_staff_queryset


def get_students_under_staff(faculty_instance):
    list_of_batches = []
    custom_list_of_students = CustomUser.objects.none()
    for active_sem in semester_migration.objects.all():
        faculty_advisors = active_sem.semester.faculty_advisor.all()
        print(faculty_advisors)
        if faculty_instance in faculty_advisors:
            list_of_batches.append(active_sem.semester.batch)
            custom_list_of_students |= CustomUser.objects.filter(
                student__department=active_sem.department,
                student__batch__in=list_of_batches,
                is_verified=True,
                has_filled_data=True
            )
    return student.objects.filter(user__in=custom_list_of_students)


def get_students_under_staffPrev(faculty_instance, date):
    list_of_batches = []
    custom_list_of_students = CustomUser.objects.none()
    for active_sem in semester.objects.filter(start_term_1__lt=date,end_term_3__gt=date):
        faculty_advisors = active_sem.faculty_advisor.all()
        print(faculty_advisors)
        if faculty_instance in faculty_advisors:
            list_of_batches.append(active_sem.batch)
            custom_list_of_students |= CustomUser.objects.filter(
                student__department=active_sem.department,
                student__batch__in=list_of_batches,
                is_verified=True,
                has_filled_data=True
            )
    return student.objects.filter(user__in=custom_list_of_students)


def get_courses_studied_by_student(student_instance, semester_instance):
    course_list = []
    if student_instance.batch.regulation.start_year == 2016:
        course_enrollment_instances = course_enrollment.objects.filter(
            student=student_instance,
            semester=semester_instance,
            approved=True,
        )
        for entry in course_enrollment_instances:
            course_list.append(entry.course)
    elif student_instance.batch.regulation.start_year == 2012:
        return courses.objects.filter(
            department=semester_instance.department,
            semester=semester_instance.semester_number,
        )
    return course_list
