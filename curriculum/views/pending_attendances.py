from datetime import timedelta, datetime

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render
from django.utils import timezone

from common.utils.TimeTableUtil import getDayInstanceForGivenDate
from curriculum.models import attendance, flexi_attendance
from curriculum.views.attendance import check_if_flexi_marked_by_another_staff, get_next_tuesday
from curriculum.views.common_includes import *



def get_weekday_of_day_custom(day_instance):
    weekday_name = getattr(day_instance, 'day_name')
    if weekday_name == 'Monday':
        return 0
    elif weekday_name == 'Tuesday':
        return 1
    elif weekday_name == 'Wednesday':
        return 2
    elif weekday_name == 'Thursday':
        return 3
    elif weekday_name == 'Friday':
        return 4


def is_weekend(dateInstance):
    if dateInstance.weekday() == 5 or dateInstance.weekday() == 6:
        return True
    return False



def get_pending_attendance_periods(semester_inst, staff_instance, course_instance):
    pending_dates_for_this_subject = []

    all_timetable_versions = time_table.objects.filter(course=course_instance).filter(semester=semester_inst)
    print('timetables')
    pprint(all_timetable_versions)

    # all_timetable_versions = sorted(all_timetable_versions, key=lambda x: (x.created_date))


    # get valid weekday numbers to be searched
    list_of_weekday_numbers = []
    for timetable_entry in all_timetable_versions:
        timetable_day = timetable_entry.day
        print('timetable day')
        pprint(timetable_day)
        weekday_number = get_weekday_of_day(timetable_day)
        if weekday_number not in list_of_weekday_numbers:
            list_of_weekday_numbers.append(weekday_number)

    # get valid start and end date
    start_date_of_sem = semester_inst.start_term_1
    ending_date = semester_inst.end_term_3
    if ending_date is None or ending_date > timezone.now().date():
        ending_date = timezone.now().date()

    delta = ending_date - start_date_of_sem
    print('start date = ' + str(start_date_of_sem))
    print('end date = ' + str(ending_date))
    pprint('delta = ' + str(delta))

    # find corresponding date and hours in each date , use available weekday numbers to reduce loop count

    current_check_date = start_date_of_sem
    list_of_date_hours = []
    for i in range(delta.days + 1):

        current_day_instance = getDayInstanceForGivenDate(semester_inst, current_check_date)
        print("current day instance " + str(current_day_instance))
        if not is_holiday(current_check_date) and not is_weekend(current_check_date):


            print('checking for date = ' + str(current_check_date))

            active_timetable_query = all_timetable_versions.filter(
                course=course_instance
            ).filter(
                semester=semester_inst
            ).filter(
                day=current_day_instance
            ).filter(
                is_active=True
            ).filter(
                created_date__lte=current_check_date
            )

            print('active tt query')

            if active_timetable_query.exists():
                for tt_hours in active_timetable_query:
                    print(vars(tt_hours))
                    if not is_ut_date(tt_hours.semester, current_check_date, tt_hours.hour):
                        temp = {}
                        temp['date'] = current_check_date
                        temp['hour'] = tt_hours.hour
                        temp['semester'] = tt_hours.semester
                        temp['course'] = tt_hours.course
                        list_of_date_hours.append(temp)

            inactive_timetable_query = all_timetable_versions.filter(
                course=course_instance
            ).filter(
                semester=semester_inst
            ).filter(
                day=current_day_instance
            ).filter(
                is_active=False
            ).filter(
                created_date__lte=current_check_date
            ).filter(
                modified_date__gt=current_check_date
            )

            if inactive_timetable_query.exists():
                for tt_hours in inactive_timetable_query:
                    if not is_ut_date(tt_hours.semester, current_check_date, tt_hours.hour):
                        temp = {}
                        temp['date'] = current_check_date
                        temp['hour'] = tt_hours.hour
                        temp['semester'] = tt_hours.semester
                        temp['course'] = tt_hours.course
                        list_of_date_hours.append(temp)

        current_check_date = current_check_date + timedelta(days=1)


    for entry in flexi_attendance.objects.filter(semester=semester_inst,marked_course=course_instance):
        att_query = attendance.objects.filter(
            semester=entry.semester,
            course=entry.marked_course,
            date=entry.attendance_date,
            hour=entry.attendance_hour,
            grant_period__gte=timezone.now(),
            granted_edit_access=True,
        )
        print('flexi attendance menu')
        print(att_query)
        if att_query.exists():
            att_inst = att_query.get()
            temp = {}
            temp['date'] = att_inst.date
            temp['hour'] = att_inst.hour
            temp['semester'] = att_inst.semester
            temp['course'] = att_inst.course
            list_of_date_hours.append(temp)

    print('list_of_date_hours')
    list_of_date_hours = sorted(list_of_date_hours, key=lambda x: (x['date'], x['hour']))
    pprint(list_of_date_hours)
    #
    # for timetable_instances  in all_timetable_versions:
    #     start_date_of_sem = timetable_instances.created_date
    #
    #
    #     if not timetable_instances.is_active:
    #         ending_date = timetable_instances.modified_date - timedelta(days=1)
    #     else:
    #
    #
    #
    #
    #
    #
    #     list_of_valid_dates = []
    #     for i in range(delta.days + 1):
    #         if not is_holiday(start_date_of_sem + timedelta(days=i)):
    #             calculated_date = start_date_of_sem + timedelta(days=i)
    #             if calculated_date.weekday() in list_of_weekday_numbers:
    #                 list_of_valid_dates.append(calculated_date)
    #
    #     list_of_date_hours = []
    #     for date_entry in list_of_valid_dates:
    #         for day_hours_entry in list_of_day_hours:
    #             if date_entry.weekday() == day_hours_entry['weekday_number'] and not \
    #                 is_ut_date(date_entry,day_hours_entry['hour']):
    #                 temp = {}
    #                 temp['date'] = date_entry
    #                 temp['hour'] = day_hours_entry['hour']
    #                 temp['semester'] = day_hours_entry['semester']
    #                 temp['course'] = day_hours_entry['course']
    #                 list_of_date_hours.append(temp)
    #
    #     list_of_date_hours = sorted(list_of_date_hours, key=lambda x: (x['date'], x['hour']))
    #
    #     pprint(list_of_date_hours)


    final_list_of_date_hours = []

    required_attendance_queryset = attendance.objects.filter(
        semester=semester_inst,
        course=course_instance,
        date__gte=start_date_of_sem,
        date__lte=ending_date
    )
    for entry in list_of_date_hours:
        print(entry['date'])
        print(entry['hour'])
        print(semester_inst.pk)
        print(course_instance.pk)
        # print(staff_instance)
        attendance_query = required_attendance_queryset.filter(semester=semester_inst).filter(course=course_instance).filter(
            date=entry['date']).filter(hour=int(entry['hour']))

        print(attendance_query.exists())

        if not attendance_query.exists():
            if not check_if_flexi_marked_by_another_staff(semester_inst, course_instance, entry['date'], entry['hour']):
                entry['grant_period'] = get_next_tuesday(entry['date'])
                final_list_of_date_hours.append(entry)
        elif attendance_query.get().granted_edit_access:
            entry['grant_period'] = attendance_query.get().grant_period
            final_list_of_date_hours.append(entry)

    print('list of date hours')
    print()
    print()
    print()
    pprint(final_list_of_date_hours)
    if len(final_list_of_date_hours) > 0:
        pending_dates_for_this_subject = get_hours_grouped_for_day(final_list_of_date_hours)
        print('pending att')
        pprint(pending_dates_for_this_subject)

    return pending_dates_for_this_subject



    #
    #
    #
    # for i in range(delta.days + 1):
    #     calculated_date = start_date_of_sem + timedelta(days=i)
    #     if not is_holiday(calculated_date) and not is_ut_date(calculated_date,timetable_hour):
    #         print(is_holiday(calculated_date))
    #         print(is_ut_date(calculated_date,timetable_hour))
    #         if calculated_date.weekday() == weekday_number:
    #             pprint('cal date weekday = ' + str(calculated_date) + " " +str(calculated_date.weekday()))
    #             try:
    #                 attendance_instance = attendance.objects.filter(staff=staff_instance).filter(course=course_instance).filter(
    #                     date=calculated_date).get(hour=timetable_hour)
    #                 if attendance_instance.granted_edit_access and not attendance_instance.grant_period < datetime.now():
    #                     day_inst = day.objects.get(day_name=calculated_date.strftime("%A"))
    #                     current_attendance_day_query = timetable_instances.filter(day=day_inst)
    #                     grouping = get_hours_grouped_for_day(current_attendance_day_query)
    #                     pprint(grouping)
    #
    #                     for grouped_hour in grouping:
    #                         grouped_hour['date'] = calculated_date.strftime("%Y-%m-%d")
    #                         grouped_hour['display_date'] = calculated_date.strftime("%d-%m-%Y")
    #                         flag = True
    #                         for pending_entry in pending_dates_for_this_subject:
    #                             if pending_entry['date'] == grouped_hour['date'] and \
    #                                             pending_entry['course_id'] == grouped_hour['course_id'] and \
    #                                             pending_entry['start_hour'] == grouped_hour['start_hour'] and \
    #                                             pending_entry['end_hour'] == grouped_hour['end_hour'] and \
    #                                             pending_entry['semester'] == grouped_hour['semester'] and \
    #                                             pending_entry['bulk'] == grouped_hour['bulk']:
    #                                 flag = False
    #
    #                         if flag:
    #                             pending_dates_for_this_subject.append(grouped_hour)
    #             except attendance.DoesNotExist:
    #                 day_inst = day.objects.get(day_name=calculated_date.strftime("%A"))
    #                 current_attendance_day_query = timetable_instances.filter(day=day_inst)
    #                 # to remove ut hour entry
    #                 for entry in current_attendance_day_query:
    #                     if attendance.objects.filter(staff=staff_instance).filter(course=course_instance).filter(date=calculated_date).filter(hour=entry.hour).exists():
    #                         current_attendance_day_query = current_attendance_day_query.exclude(hour=entry.hour)
    #                     if is_ut_date(calculated_date,entry.hour):
    #                         current_attendance_day_query = current_attendance_day_query.exclude(hour=entry.hour)
    #                 grouping = get_hours_grouped_for_day(current_attendance_day_query)
    #                 pprint(grouping)
    #                 for grouped_hour in grouping:
    #                     grouped_hour['date'] = calculated_date.strftime("%Y-%m-%d")
    #                     grouped_hour['display_date'] = calculated_date.strftime("%d-%m-%Y")
    #
    #                     flag = True
    #                     for pending_entry in pending_dates_for_this_subject:
    #                         if pending_entry['date'] == grouped_hour['date'] and \
    #                             pending_entry['course_id'] == grouped_hour['course_id'] and \
    #                             pending_entry['start_hour'] == grouped_hour['start_hour'] and \
    #                             pending_entry['end_hour'] == grouped_hour['end_hour'] and \
    #                             pending_entry['semester'] == grouped_hour['semester'] and \
    #                             pending_entry['bulk'] == grouped_hour['bulk']:
    #                             flag = False
    #
    #                     if flag:
    #                         pending_dates_for_this_subject.append(grouped_hour)
    #
    # pprint(pending_dates_for_this_subject)
    # return pending_dates_for_this_subject


@login_required
@permission_required('curriculum.can_mark_attendance')
def mark_pending_attendandes(request):
    try:
        staff_instance = staff.objects.get(user=request.user)
    except staff.DoesNotExist:
        return render(request, '404.html')
    course_allotment_list = get_current_course_allotment_with_display_text(staff_instance)

    try:
        selected_allotment_pk = course_allotment_list[0]['staff_course_instance'].pk
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    print(selected_allotment_pk)

    if request.method == 'POST':
        selected_allotment_pk = request.POST.get('selected_allotment_pk')
    print('selected_allotment_pk = ' + str(selected_allotment_pk))
    staff_course_instance = staff_course.objects.get(pk=int(selected_allotment_pk))
    course_instance = staff_course_instance.course
    semester_inst = staff_course_instance.semester
    pending_attendance_periods = get_pending_attendance_periods(semester_inst, staff_instance, course_instance)

    if pending_attendance_periods == 'date_error':
        msg = {
            'page_title': 'Date Error',
            'title': 'Start Date Not Found',
            'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    return render(request, 'curriculum/pending_attendances.html',
                  {'course_allotment_list': course_allotment_list,
                   'selected_allotment_pk': int(selected_allotment_pk),
                   'selected_semester_pk': int(staff_course_instance.semester.pk),
                   'pending_periods': pending_attendance_periods,
                   })


def get_pending_hours_for_given_date(request):
    print('Method called hours')

    if request.method == 'POST':
        print('POST called')
        # POST goes here . is_ajax is must to capture ajax requests. Beginner's pit.
        if request.is_ajax():
            got_course = request.POST.get('course')
            print(got_course)
            got_date = request.POST.get('date')
            print(got_date)
            date_instance = datetime.strptime(got_date, '%Y-%m-%d')
            day_name = date_instance.strftime("%A")
            print(day_name)
            staff_instance = staff.objects.get(user=request.user)

            current_active_semesters = get_all_current_active_semester()
            timetable_instances = time_table.objects.filter(sem__in=current_active_semesters).filter(
                staff=staff_instance).filter(course=got_course).filter(
                day__day_name=day_name)

            pprint(timetable_instances)

            data = {}
            list = []

            for instance in timetable_instances:
                hour = getattr(instance, 'hour')
                list.append(hour)

            print(list)

            data['hours'] = list

        return JsonResponse(data)
