from datetime import datetime
from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import render

from accounts.models import staff, active_batches
from curriculum.models import semester, courses, unit_test_timetable
from curriculum.views import staffBelongsToNonCore, staffNotEligible, get_first_year_managing_department, \
    get_active_semesters_tabs


@login_required
@permission_required('curriculum.can_create_ut_timetable')
def create_unit_test_timetable(request):
    staff_query = staff.objects.filter(user=request.user)
    staff_instance = staff_query.get()
    # santity check for first year
    if staffBelongsToNonCore(staff_instance):
        print(staffBelongsToNonCore(staff_instance))
        if staffNotEligible(staff_instance):
            msg = {
                'page_title': 'Access Denied',
                'title': 'No permission',
                'description': 'Your department does not have this permission. Contact ' + str(
                    get_first_year_managing_department()) + ' department for this settings',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    available_semsters = get_active_semesters_tabs(request)

    if available_semsters:
        selected_semester_pk = available_semsters[0]['semester_instance'].pk
    else:
        modal = {
            'heading': 'Error',
            'body': 'No semester plan has been created for your access',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    if 'selected_semester' in request.POST:
        selected_semester_pk = request.POST['selected_semester']

    selected_semester_instance = semester.objects.get(pk=selected_semester_pk)
    active_batch_instance = active_batches.objects.filter(
        department=selected_semester_instance.department
    ).get(
        batch=selected_semester_instance.batch
    )

    courses_query = courses.objects.filter(
        department=selected_semester_instance.department
    ).filter(
        regulation=selected_semester_instance.batch.regulation
    ).filter(
        semester=active_batch_instance.current_semester_number_of_this_batch
    ).filter(
        programme=selected_semester_instance.batch.programme
    )

    list_of_courses = []
    for entry in courses_query:
        list_of_courses.append(entry)

    if request.method == 'POST':
        if request.is_ajax():

            dictionary = {}

            got_course_pk = request.POST.get('course_pk')
            got_ut_number = request.POST.get('ut_number')
            got_date = request.POST.get('date')
            got_fn_or_an = request.POST.get('fn_or_an')
            selected_semester_pk = request.POST.get('selected_semester_pk')

            pprint(locals())

            selected_course_instance = courses.objects.get(pk=got_course_pk)
            selected_semester_instance = semester.objects.get(pk=selected_semester_pk)
            selected_date_instance = datetime.strptime(got_date, "%d-%m-%Y")

            unit_test_timetable.objects.get_or_create(
                semester=selected_semester_instance,
                ut_number=int(got_ut_number),
                course=selected_course_instance,
                date=selected_date_instance,
                fn_or_an=got_fn_or_an
            )

            list = []
            for each in unit_test_timetable.objects.filter(semester=selected_semester_instance):
                temp = {}
                temp['ut_number'] = str(each.ut_number)
                temp['course_name'] = str(each.course.course_name)
                temp['date'] = str(each.date.strftime("%d-%m-%Y"))
                temp['fn_or_an'] = str(each.fn_or_an)

                list.append(temp)

            dictionary['list'] = list

            return JsonResponse(dictionary)

    list_of_ut_timetable = []

    for each in unit_test_timetable.objects.filter(semester=selected_semester_instance):
        temp = {}
        temp['ut_number'] = str(each.ut_number)
        temp['course_name'] = str(each.course.course_name)
        temp['date'] = str(each.date.strftime("%d-%m-%Y"))
        temp['fn_or_an'] = str(each.fn_or_an)

        list_of_ut_timetable.append(temp)

    return render(request, 'unit_test_timetable/create_unit_test_timetable.html', {
        'list_of_semesters': available_semsters,
        'selected_semester': int(selected_semester_pk),
        'list_of_ut_timetable': list_of_ut_timetable,
        'list_of_courses': list_of_courses,
    })


@login_required
@permission_required('curriculum.can_create_ut_timetable')
def view_ut_timetable(request, semester_pk):
    print(semester_pk)

    semester_instance = semester.objects.filter(pk=semester_pk)

    if request.method == 'POST':
        got_ut_timetable_pk = request.POST.get('ut_timetable_pk')
        print(got_ut_timetable_pk)
        unit_test_timetable.objects.get(pk=got_ut_timetable_pk).delete()

    list_of_ut_timetable = []

    for each in unit_test_timetable.objects.filter(semester=semester_instance):
        list_of_ut_timetable.append(each)

    pprint(locals())

    return render(request, 'unit_test_timetable/view_ut_timetable.html', {'list_of_ut_timetable': list_of_ut_timetable})
