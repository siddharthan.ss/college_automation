from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from accounts.models import staff
from curriculum.models import staff_course, staff_course_split, semester
from curriculum.views.common_includes import staffBelongsToNonCore, staffNotEligible, \
    get_first_year_managing_department, get_active_semesters_tabs, get_course_allotments_for_overall_report, \
    get_list_of_students


@login_required
@permission_required('curriculum.can_allot_staffs')
def split_staff_course_handling(request):
    staff_instance = staff.objects.get(user=request.user)
    delete_status = False

    # santity check for first year
    if staffBelongsToNonCore(staff_instance):
        if staffNotEligible(staff_instance):
            msg = {
                'page_title': 'Access Denied',
                'title': 'No permission',
                'description': 'Your department does not have this permission. Contact ' + str(
                    get_first_year_managing_department()) + ' department for this settings',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    available_semsters = get_active_semesters_tabs(request)

    if available_semsters:
        selected_semester_pk = available_semsters[0]['semester_instance'].pk
    else:
        msg = {
            'page_title': 'No Semester Plan',
            'title': 'No Semester Plan',
            'description': 'Create Semester Plan first, in order to create timetable',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'selected_semester_tab' in request.POST:
            selected_semester_pk = request.POST['selected_semester_tab']

        if request.POST.get('subject_code'):
            selected_course = request.POST.get('subject_code')
            selected_semester_pk = int(request.POST.get('inherit_batch'))

        if 'assign' in request.POST:
            selected_course = request.POST.get('hidden_selected_course')
            selected_semester_pk = int(request.POST.get('hidden_selected_semester'))

        if 'delete_split' in request.POST:
            selected_course = request.POST.get('hidden_selected_course')
            selected_semester_pk = int(request.POST.get('hidden_selected_semester'))

            staff_course_instance = staff_course.objects.get(pk=selected_course)

            staff_course_split.objects.filter(staff_course=staff_course_instance).delete()

            delete_status = True

    selected_semester = semester.objects.get(pk=selected_semester_pk)

    alloted_course_instances_of_selected_semester = staff_course.objects.filter(
        semester=selected_semester
    )

    course_allotment_list = get_course_allotments_for_overall_report(alloted_course_instances_of_selected_semester)

    try:
        selected_course = course_allotment_list[0]['staff_course_instance'].pk
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if request.POST.get('subject_code'):
            selected_course = request.POST.get('subject_code')
            selected_semester_pk = int(request.POST.get('inherit_batch'))

    if request.method == 'POST':
        if 'assign' in request.POST:
            selected_course = request.POST.get('hidden_selected_course')
            selected_semester_pk = int(request.POST.get('hidden_selected_semester'))

    selected_semester_instance = semester.objects.get(pk=selected_semester_pk)
    # print(semester_instance)
    staff_course_instance = staff_course.objects.get(pk=selected_course)
    list_of_staffs = staff_course_instance.staffs.all()

    available_students = get_list_of_students(selected_semester_instance, staff_course_instance.course)

    if request.method == 'POST':
        if 'assign' in request.POST:
            for each_student in available_students:
                selected_staff_pk = request.POST.get('select_student-' + str(each_student['student_obj'].pk))
                selected_staff_instance = staff.objects.get(pk=selected_staff_pk)
                if staff_course_split.objects.filter(student=each_student['student_obj'],
                                                     staff_course=staff_course_instance):
                    staff_course_split_instance = staff_course_split.objects.get(student=each_student['student_obj'],
                                                                                 staff_course=staff_course_instance)
                    staff_course_split_instance.staff = selected_staff_instance
                    staff_course_split_instance.save()
                else:
                    staff_course_split_instance = staff_course_split(student=each_student['student_obj'],
                                                                     staff_course=staff_course_instance,
                                                                     staff=selected_staff_instance)
                    staff_course_split_instance.save()

    list_of_students = []

    delete_possible = False

    for each_student in available_students:
        temp = {}
        temp['student_obj'] = each_student['student_obj']
        if staff_course_split.objects.filter(student=each_student['student_obj'], staff_course=staff_course_instance):
            temp['staff_instance'] = staff_course_split.objects.get(student=each_student['student_obj'],
                                                                    staff_course=staff_course_instance).staff
            delete_possible = True
        list_of_students.append(temp)

    return render(request, 'allot_staff/allot_staff_split.html',
                  {
                      'selected_course': int(selected_course),
                      'course_list': course_allotment_list,
                      'list_of_semesters': available_semsters,
                      'selected_semester': int(selected_semester_pk),
                      'list_of_staffs': list_of_staffs,
                      'list_of_students': list_of_students,
                      'no_of_staffs': len(list_of_staffs),
                      'no_of_students': len(list_of_students),
                      'delete_possible': delete_possible,
                      'delete_status': delete_status
                  })
