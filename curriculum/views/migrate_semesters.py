"""


"""
from django.shortcuts import render
from feedback.models import *
from accounts.models import staff, active_batches, batch, regulation
from curriculum.models import semester_migration, department_sections
from curriculum.views.common_includes import get_departments_of_pc


class GctportalException(Exception):
    pass


def get_latest_regulation():
    return regulation.objects.order_by('-start_year').first()


def check_migration(currently_migrating_instance):
    # semester_check_query = semester.objects.filter(
    #     department = currently_migrating_instance.semester.department,
    #     programme = currently_migrating_instance.semester.programme,
    #     semsester_number = currently_migrating_instance.semester_number + 1
    # )
    batch_check_query = active_batches.objects.filter(
        department=currently_migrating_instance.semester.department,
        programme=currently_migrating_instance.semester.batch.programme,
        current_semester_number_of_this_batch=currently_migrating_instance.semester_number + 1
    )
    if batch_check_query.exists():
        return False
    return True


def create_new_active_batch(currently_migrating_instance):
    batch_check_query = active_batches.objects.filter(
        department=currently_migrating_instance.semester.department,
        programme=currently_migrating_instance.semester.batch.programme,
        current_semester_number_of_this_batch=1
    )
    if batch_check_query.exists():
        raise GctportalException(str(currently_migrating_instance.semester.department.acronym) + ' - ' +
                        str(
                            currently_migrating_instance.semester.batch.programme) + ' - Semester 1 is still active. Please handle it')
    batch_increment_count = None
    active_batches_instance = active_batches.objects.filter(
        department=currently_migrating_instance.semester.department).filter(
        current_semester_number_of_this_batch__lte=2).filter(
        programme=currently_migrating_instance.semester.batch.programme)
    if active_batches_instance.exists():
        active_batches_instance = active_batches_instance.get()
        batch_increment_count = 1
    else:
        active_batches_instance = active_batches.objects.filter(
            department=currently_migrating_instance.semester.department).filter(
            current_semester_number_of_this_batch__gt=2).filter(current_semester_number_of_this_batch__lte=4).filter(
            programme=currently_migrating_instance.semester.batch.programme)
        if active_batches_instance.exists():
            active_batches_instance = active_batches_instance.get()
            batch_increment_count = 2
        else:
            raise GctportalException("No first year batch found. Please create semester plan for first year")
    new_batch_obj = batch.objects.get_or_create(
        start_year=active_batches_instance.batch.start_year + batch_increment_count,
        end_year=active_batches_instance.batch.end_year + batch_increment_count,
        programme=active_batches_instance.batch.programme,
        regulation=get_latest_regulation()
    )
    new_batch_obj = new_batch_obj[0]
    print('created ' + str(new_batch_obj))
    # add new active batch obj
    new_active_batch_obj = active_batches.objects.create(
        batch=new_batch_obj,
        department=currently_migrating_instance.semester.department,
        current_semester_number_of_this_batch=1,
        programme=new_batch_obj.programme,
    )
    print('Created  + ' + str(new_active_batch_obj))
    # create new department section
    new_department_section_obj = department_sections.objects.create(
        batch=new_batch_obj,
        department=currently_migrating_instance.semester.department,
        section_name='A',
    )
    print('Created + ' + str(new_department_section_obj))


# gets semester_migration_pk (active semester) and performs migration for it
def migrate_semester(semester_migration_pk):
    """
    checks whether current semester can be migrated

    :param semester_migration_pk:
    :return: boolean
    """
    currently_migrating_instance = semester_migration.objects.get(pk=semester_migration_pk)
    if check_migration(currently_migrating_instance):
        regul=currently_migrating_instance.semester.batch.regulation__start_year
        if regul==2016:
            courses=course_enrollment.objects.filter(semester=currently_migrating_instance.semester,department=currently_migrating_instance.department)
            notFilledLis=[]
            for student in section_students.objects.filter(section=currently_migrating_instance.semester.department_section):
                courses=course_enrollment.objects.filter(student=student,semester=currently_migrating_instance.semester,approved=True)
                if FeedbackByStudents.filter(student=student,staffCourse__courses__in=courses).count()<len(courses)*60:
                    notFilledLis.append(student)
            if len(notFiled)>0:
                raise GctportalException('Please ask your students to fill the feedback before migrating the semester.')
        if ((
                            currently_migrating_instance.semester.semester_number == 8 and currently_migrating_instance.semester.batch.programme == "UG")
            or (
                            currently_migrating_instance.semester_number == 4 and currently_migrating_instance.semester.batch.programme == "PG")):
            create_new_active_batch(currently_migrating_instance)
            active_batches_instance = active_batches.objects.get(
                department=currently_migrating_instance.department,
                programme=currently_migrating_instance.semester.batch.programme,
                current_semester_number_of_this_batch=currently_migrating_instance.semester_number)
            active_batches_instance.delete()
        else:
            active_batches_instance = active_batches.objects.get(
                department=currently_migrating_instance.department,
                programme=currently_migrating_instance.semester.batch.programme,
                current_semester_number_of_this_batch=currently_migrating_instance.semester_number)
            active_batches_instance.current_semester_number_of_this_batch += 1
            active_batches_instance.save()

            # semester_instance = currently_migrating_instance.semester
            # semester_instance.semester_number += 1
            # semester_instance.save()

        currently_migrating_instance.delete()
    else:
        raise GctportalException('There is already another batch with the same semester number. Migrate it first')


def migrate_semester_view(request):
    staff_inst = staff.objects.get(user=request.user)
    modal = None

    department_instance_list = get_departments_of_pc(staff_inst)

    semester_migration_instances = semester_migration.objects.filter(department__in=department_instance_list).order_by(
        'semester_number')

    if request.method == "POST":
        got_semester_migration_pk = request.POST.get('semester_migration_pk')
        try:
            migrate_semester(got_semester_migration_pk)
            modal = {
                'heading': 'Migration Done',
                'body': 'The selected migration has been applied  successfully',
            }
        except GctportalException as e:
            modal = {
                'heading': 'Error',
                'body': str(e),
            }

    if modal is not None:
        toggle_modal = True
    else:
        toggle_modal = False

    return render(request, 'semester_migration/semester_migration.html',
                  {'active_semesters': semester_migration_instances, 'toggle_model': toggle_modal, 'modal': modal})