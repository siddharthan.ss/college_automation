from datetime import datetime
from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.http import Http404
from django.shortcuts import render

from accounts.models import staff, batch, active_batches, department
from curriculum.models import semester, semester_migration, department_sections
from curriculum.views.common_includes import get_facutly_advisors, \
    get_current_semester_of_given_batch, \
    get_first_year_batch_of_department
from curriculum.views.common_includes import (
    get_first_year_managing_department,
    staffBelongsToNonCore,
    staffNotEligible,
    get_active_batch_tabs,
)

from common.API.departmentAPI import getAllDepartments


@login_required
@permission_required('curriculum.can_create_semester_plan')
def create_semester_plan(request):
    # print('inside create semster plan')
    ############################################################################################
    ## handle first year ##

    staff_query = staff.objects.filter(user=request.user)
    if staff_query.exists():
        staff_instance = staff_query.get()
    else:
        return 403

    # santity check for first year
    if staffBelongsToNonCore(staff_instance):
        # print(staffBelongsToNonCore(staff_instance))
        if staffNotEligible(staff_instance):
            msg = {
                'page_title': 'Access Denied',
                'title': 'No permission',
                'description': 'Your department does not have this permission. Contact ' + str(
                    get_first_year_managing_department()) + ' department for this settings',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    ###############################################################################################

    all_batches = get_active_batch_tabs(request.user)

    selected_departments = []

    selected_section_pk = None
    if len(all_batches) > 0:
        selected_section_pk = all_batches[0]['department_section_instance'].pk
    else:
        msg = {
            'page_title': 'No eligible batches',
            'title': 'No eligible batches',
            'description': 'There is no eligible batch for you to create semsester plan',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'selected_section_tab' in request.POST:
            selected_section_pk = int(request.POST.get('selected_section_tab'))

        if 'chosen_section_from_select_department' in request.POST:
            selected_section_pk = int(request.POST.get('chosen_section_from_select_department'))
            selected_departments = request.POST.getlist('select_department')
            # print('selected_dept')
            # print(selected_departments)

        if 'semester_plan' in request.POST:
            choosen_faculty_advisors_id = request.POST.getlist('choosen_faculty_advisors')
            # print('choosen_fa')
            # print(choosen_faculty_advisors_id)

            selected_section_pk = int(request.POST.get('selected_tab'))
            selected_section_instance = department_sections.objects.get(pk=selected_section_pk)
            selected_batch_instance = selected_section_instance.batch
            department_instance = selected_section_instance.department

            got_start_term_1 = request.POST.get('start_term_1') if request.POST.get('start_term_1') != '' else None
            got_end_term_1 = request.POST.get('end_term_1') if request.POST.get('end_term_1') != '' else None
            got_start_term_2 = request.POST.get('start_term_2') if request.POST.get('start_term_2') != '' else None
            got_end_term_2 = request.POST.get('end_term_2') if request.POST.get('end_term_2') != '' else None
            got_start_term_3 = request.POST.get('start_term_3') if request.POST.get('start_term_3') != '' else None
            got_end_term_3 = request.POST.get('end_term_3') if request.POST.get('end_term_3') != '' else None
            openElectiveCount = request.POST.get('openElectiveCourse')

            if got_start_term_1:
                got_start_term_1 = datetime.strptime(got_start_term_1, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_start_term_2:
                got_start_term_2 = datetime.strptime(got_start_term_2, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_start_term_3:
                got_start_term_3 = datetime.strptime(got_start_term_3, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_end_term_1:
                got_end_term_1 = datetime.strptime(got_end_term_1, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_end_term_2:
                got_end_term_2 = datetime.strptime(got_end_term_2, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_end_term_3:
                got_end_term_3 = datetime.strptime(got_end_term_3, "%d/%m/%Y").strftime('%Y-%m-%d')

            current_sem_number = active_batches.objects.filter(
                department=department_instance
            ).get(
                batch=selected_batch_instance
            ).current_semester_number_of_this_batch

            new_semester_plan_instance = semester.objects.get_or_create(
                department=department_instance,
                batch=selected_batch_instance,
                semester_number=current_sem_number,
                department_section=selected_section_instance,
            )
            if new_semester_plan_instance[1] == False:
                # print('deleting')
                for faculty_advisors_inst in new_semester_plan_instance[0].faculty_advisor.all():
                    new_semester_plan_instance[0].faculty_advisor.remove(faculty_advisors_inst)
            new_semester_plan_instance = new_semester_plan_instance[0]
            new_semester_plan_instance.start_term_1 = got_start_term_1
            new_semester_plan_instance.start_term_2 = got_start_term_2
            new_semester_plan_instance.start_term_3 = got_start_term_3
            new_semester_plan_instance.end_term_1 = got_end_term_1
            new_semester_plan_instance.end_term_2 = got_end_term_2
            new_semester_plan_instance.end_term_3 = got_end_term_3
            new_semester_plan_instance.open_courses = openElectiveCount
            new_semester_plan_instance.save()

            for id in choosen_faculty_advisors_id:
                new_semester_plan_instance.faculty_advisor.add(staff.objects.get(pk=id))

            semester_migration.objects.get_or_create(
                semester=new_semester_plan_instance,
                department=department_instance,
                semester_number=current_sem_number
            )

    staff_from_departments = []

    selected_section_instance = department_sections.objects.get(pk=selected_section_pk)
    selected_batch_instance = selected_section_instance.batch
    department_instance = selected_section_instance.department

    available_departments = []
    for each in getAllDepartments():
        temp = {}
        temp['name'] = str(each)
        temp['acronym'] = str(each.acronym)
        temp['dept_id'] = str(each.id)
        available_departments.append(temp)

    semester_instance = get_current_semester_of_given_batch(department_instance, selected_section_instance)
    # pprint(department_instance)
    # pprint(selected_batch_instance)
    # pprint(semester_instance)

    semester_plan_dates = {}
    semester_plan_dates['start_term_1'] = ''
    semester_plan_dates['start_term_2'] = ''
    semester_plan_dates['start_term_3'] = ''
    semester_plan_dates['end_term_1'] = ''
    semester_plan_dates['end_term_2'] = ''
    semester_plan_dates['end_term_3'] = ''
    semester_plan_dates['openElective'] = 0
    faculty_advisors_list = []
    if semester_instance:
        if semester_instance.start_term_1:
            semester_plan_dates['start_term_1'] = semester_instance.start_term_1.strftime("%d/%m/%Y")
        if semester_instance.start_term_2:
            semester_plan_dates['start_term_2'] = semester_instance.start_term_2.strftime("%d/%m/%Y")
        if semester_instance.start_term_3:
            semester_plan_dates['start_term_3'] = semester_instance.start_term_3.strftime("%d/%m/%Y")
        if semester_instance.end_term_1:
            semester_plan_dates['end_term_1'] = semester_instance.end_term_1.strftime("%d/%m/%Y")
        if semester_instance.end_term_2:
            semester_plan_dates['end_term_2'] = semester_instance.end_term_2.strftime("%d/%m/%Y")
        if semester_instance.end_term_3:
            semester_plan_dates['end_term_3'] = semester_instance.end_term_3.strftime("%d/%m/%Y")
        if semester_instance.open_courses:
            semester_plan_dates['openElective'] = semester_instance.open_courses

        faculty_advisors_instances_list = get_facutly_advisors(department_instance, selected_section_instance)

        faculty_advisors_list = []
        for entry in faculty_advisors_instances_list:
            faculty_advisors_list.append(str(entry.pk))

        for staff_inst in faculty_advisors_instances_list:
            staff_from_departments.append(staff_inst.department)

    # print(staff_from_departments)
    for each in staff_from_departments:
        if each.pk != department_instance.id:
            if str(each.pk) not in selected_departments:
                selected_departments.append(str(each.pk))
    # print(selected_departments)
    list_of_staffs = []
    for each in staff.objects.filter(department=department_instance, user__is_approved=True).exclude(
            designation='Network Admin').order_by('first_name'):
        temp = {}
        temp['acronym'] = each.department.acronym
        temp['staff_name'] = str(each)
        temp['id'] = str(each.id)
        list_of_staffs.append(temp)

    for each_department in selected_departments:
        for each in staff.objects.filter(department_id=each_department, user__is_approved=True).exclude(
                designation='Network Admin').order_by('first_name'):
            temp = {}
            temp['acronym'] = each.department.acronym
            temp['staff_name'] = str(each)
            temp['id'] = str(each.id)
            list_of_staffs.append(temp)

    context_data = {
        'all_sections': all_batches,
        'selected_section': int(selected_section_pk),
        'available_departments': available_departments,
        'current_department': str(department_instance.id),
        'selected_departments': selected_departments,
        'semester_plan_dates': semester_plan_dates,
        'faculty_advisors_list': faculty_advisors_list,
        'list_of_staffs': list_of_staffs,

    }

    # pprint(context_data)

    return render(request, 'curriculum/semester_plan.html', context_data)


"""


    pc_object = staff.objects.get(user=request.user)
    #print('test')


    department_instances = []
    selected_department_id_list = []
    selected_department_id_list.append(pc_object.department.pk)
    for department_obj in department_instances:
        selected_department_id_list.append(department_obj.pk)
    if request.method == 'POST':
        got_selected_department_id_list = request.POST.getlist("select_department")
        print(got_selected_department_id_list)
        print('selected dept list')
        for entry in got_selected_department_id_list:
            selected_department_id_list.append(int(entry))
        pprint(selected_department_id_list)

    queries = [Q(pk=dept_id) for dept_id in selected_department_id_list]

    query = queries.pop()
    for item in queries:
        query |= item

    print('query ')
    print(query)

    department_insts = department.objects.filter(query)

    queryset = staff.objects.all().filter(user__is_approved=True).filter(department__in=department_insts)
    available_batches = get_current_batches(department=pc_object.get_department())
    semester_plan = semester_plan_form(queryset=queryset)

    if request.method == 'POST' and request.POST.get("semester_plan"):
        semester_plan = semester_plan_form(request.POST,queryset=queryset)
        #print(semester_plan.is_valid())
        if semester_plan.is_valid():
            got_batch = request.POST.get('batch')
            batch_instance = batch.objects.get(pk=got_batch)

            #dates
            got_start_term_1 = request.POST.get('start_term_1') if request.POST.get('start_term_1')!='' else None
            got_end_term_1 = request.POST.get('end_term_1') if request.POST.get('end_term_1')!='' else None
            got_start_term_2 = request.POST.get('start_term_2') if request.POST.get('start_term_2')!='' else None
            got_end_term_2 = request.POST.get('end_term_2') if request.POST.get('end_term_2')!='' else None
            got_start_term_3 = request.POST.get('start_term_3') if request.POST.get('start_term_3')!='' else None
            got_end_term_3 = request.POST.get('end_term_3') if request.POST.get('end_term_3')!='' else None

            if got_start_term_1:
                got_start_term_1 = datetime.strptime(got_start_term_1, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_start_term_2:
                got_start_term_2 = datetime.strptime(got_start_term_2, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_start_term_3:
                got_start_term_3 = datetime.strptime(got_start_term_3, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_end_term_1:
                got_end_term_1 = datetime.strptime(got_end_term_1, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_end_term_2:
                got_end_term_2 = datetime.strptime(got_end_term_2, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_end_term_3:
                got_end_term_3 = datetime.strptime(got_end_term_3, "%d/%m/%Y").strftime('%Y-%m-%d')

            #print('start term 1 : ' + str(got_start_term_1))
            #print('end term 1 : ' + str(got_end_term_1))
            #print('start term 2 : ' + str(got_start_term_2))
            #print('end term 2 : ' + str(got_end_term_2))
            #print('start term 3 : ' + str(got_start_term_3))
            #print('end term 3 : ' + str(got_end_term_3))

            #request_user_staff_obj = staff.objects.all().get(user=request.user)

            active_batch_inst = active_batches.objects.filter(department=department_obj).get(batch=batch_instance)
            sem = active_batch_inst.current_semester_number_of_this_batch

            g = Group.objects.get(name='faculty_advisor')
            curent_faculty_advisors = get_facutly_advisors(department_obj, batch_instance, sem)
            #print('Current FA : ' + str(curent_faculty_advisors))

            staff_obj = semester_plan.cleaned_data['faculty_advisor']


            if curent_faculty_advisors is not None:
                semester_instance = get_current_semester_of_given_batch(dept_obj=department_obj,batch_object=batch_instance)
                for obj in curent_faculty_advisors:
                    user = getattr(obj,'user')
                    g.user_set.remove(user)
                    semester_instance.faculty_advisor.remove(obj)
                for fa in staff_obj:
                    semester_instance.faculty_advisor.add(fa)

                semester_instance.start_term_1 = got_start_term_1
                semester_instance.end_term_1 = got_end_term_1
                semester_instance.start_term_2 = got_start_term_2
                semester_instance.end_term_2 = got_end_term_2
                semester_instance.start_term_3 = got_start_term_3
                semester_instance.end_term_3 = got_end_term_3

                semester_instance.save()
            else:
                #print('No Fa previously assinged')
                temp = semester_plan.save(commit=False)
                temp.batch = batch_instance
                temp.department = department_obj
                temp.semester_number = sem

                temp.start_term_1 = got_start_term_1
                temp.end_term_1 = got_end_term_1
                temp.start_term_2 = got_start_term_2
                temp.end_term_2 = got_end_term_2
                temp.start_term_3 = got_start_term_3
                temp.end_term_3 = got_end_term_3

                temp.save()
                semester_plan.save_m2m()

                #create entry in semester migration
                new_curr_sem = semester_migration.objects.create(
                    semester=temp,
                    department = department_obj,
                    semester_number = sem,
                )
                #print('created ' + str(new_curr_sem))


            for fa in staff_obj:
                user = fa.user
                g.user_set.add(user)

    context = {
        'semester_plan_form': semester_plan,
        'batches': available_batches,
        'available_departments':getAllDepartments(),
        'selected_departments_id':selected_department_id_list,
        'current_department':pc_object.department,
    }

    pprint(context)

    return render(request, 'curriculum/semester_plan.html', context)

"""


@login_required
@permission_required('curriculum.can_create_semester_plan')
def semester_plan_first_year(request):
    staff_instance = staff.objects.get(user=request.user)
    department_instance = staff_instance.department
    current_department = str(staff_instance.department.id)

    staff_from_departments = []
    # fetching all departmentslist_of_staffs
    available_departments = []
    for each in getAllDepartments().exclude(name='Hostel'):
        temp = {}
        temp['name'] = str(each)
        temp['acronym'] = str(each.acronym)
        temp['dept_id'] = str(each.id)
        available_departments.append(temp)

    selected_departments = []
    # fetching all core departments
    all_core_departments = department.objects.filter(is_core=True)
    selected_department = None
    selected_department_id = 0
    if len(all_core_departments) > 0:
        selected_department = all_core_departments[0]

    if request.method == "POST":
        if 'selected_department_acronym' in request.POST:
            selected_department_acronym = request.POST.get("selected_department_acronym")
            selected_department = department.objects.get(acronym=selected_department_acronym)

        if 'chosen_dept_from_select_department' in request.POST:
            selected_department_id = request.POST.get('chosen_dept_from_select_department')
            selected_department = department.objects.get(id=selected_department_id)
            # print(selected_department)
            selected_departments = request.POST.getlist('select_department')
            # print('selected_dept')
            # pprint(selected_departments)

        if 'semester_plan' in request.POST:
            choosen_faculty_advisors_id = request.POST.getlist('choosen_faculty_advisors')
            # print('choosen_fa')
            # print(choosen_faculty_advisors_id)

            got_department = request.POST.get('post_select_department')
            selected_department = department.objects.get(id=got_department)
            # print('selected_department='+str(selected_department))

            selected_batch_instance = get_first_year_batch_of_department(selected_department, 'UG').batch
            # selected_batch_instance = batch.objects.get(pk=selected_batch)

            got_start_term_1 = request.POST.get('start_term_1') if request.POST.get('start_term_1') != '' else None
            got_end_term_1 = request.POST.get('end_term_1') if request.POST.get('end_term_1') != '' else None
            got_start_term_2 = request.POST.get('start_term_2') if request.POST.get('start_term_2') != '' else None
            got_end_term_2 = request.POST.get('end_term_2') if request.POST.get('end_term_2') != '' else None
            got_start_term_3 = request.POST.get('start_term_3') if request.POST.get('start_term_3') != '' else None
            got_end_term_3 = request.POST.get('end_term_3') if request.POST.get('end_term_3') != '' else None

            if got_start_term_1:
                got_start_term_1 = datetime.strptime(got_start_term_1, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_start_term_2:
                got_start_term_2 = datetime.strptime(got_start_term_2, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_start_term_3:
                got_start_term_3 = datetime.strptime(got_start_term_3, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_end_term_1:
                got_end_term_1 = datetime.strptime(got_end_term_1, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_end_term_2:
                got_end_term_2 = datetime.strptime(got_end_term_2, "%d/%m/%Y").strftime('%Y-%m-%d')
            if got_end_term_3:
                got_end_term_3 = datetime.strptime(got_end_term_3, "%d/%m/%Y").strftime('%Y-%m-%d')

            current_sem_number = active_batches.objects.filter(
                department=selected_department
            ).get(
                batch=selected_batch_instance
            ).current_semester_number_of_this_batch

            new_semester_plan_instance = semester.objects.get_or_create(
                department=selected_department,
                batch=selected_batch_instance,
                semester_number=current_sem_number,
            )
            if new_semester_plan_instance[1] == False:
                print('deleting')
                for faculty_advisors_inst in new_semester_plan_instance[0].faculty_advisor.all():
                    new_semester_plan_instance[0].faculty_advisor.remove(faculty_advisors_inst)
            new_semester_plan_instance = new_semester_plan_instance[0]
            new_semester_plan_instance.start_term_1 = got_start_term_1
            new_semester_plan_instance.start_term_2 = got_start_term_2
            new_semester_plan_instance.start_term_3 = got_start_term_3
            new_semester_plan_instance.end_term_1 = got_end_term_1
            new_semester_plan_instance.end_term_2 = got_end_term_2
            new_semester_plan_instance.end_term_3 = got_end_term_3
            new_semester_plan_instance.save()

            for id in choosen_faculty_advisors_id:
                new_semester_plan_instance.faculty_advisor.add(staff.objects.get(pk=id))

            semester_migration.objects.get_or_create(
                semester=new_semester_plan_instance,
                department=selected_department,
                semester_number=current_sem_number
            )

    selected_department_id = selected_department.id

    selected_batch = get_first_year_batch_of_department(selected_department, 'UG').batch
    # print(selected_batch)
    selected_batch_instance = batch.objects.get(pk=selected_batch.id)
    # print(batch_instance)
    # print(selected_department)

    semester_instance = get_current_semester_of_given_batch(selected_department, selected_batch)
    # print(b)
    # semester_instance = b.batch
    # pprint(department_instance)
    # pprint(selected_batch)
    # pprint(semester_instance)

    semester_plan_dates = {}
    semester_plan_dates['start_term_1'] = ''
    semester_plan_dates['start_term_2'] = ''
    semester_plan_dates['start_term_3'] = ''
    semester_plan_dates['end_term_1'] = ''
    semester_plan_dates['end_term_2'] = ''
    semester_plan_dates['end_term_3'] = ''
    faculty_advisors_list = []
    if semester_instance:
        if semester_instance.start_term_1:
            semester_plan_dates['start_term_1'] = semester_instance.start_term_1.strftime("%d/%m/%Y")
        if semester_instance.start_term_2:
            semester_plan_dates['start_term_2'] = semester_instance.start_term_2.strftime("%d/%m/%Y")
        if semester_instance.start_term_3:
            semester_plan_dates['start_term_3'] = semester_instance.start_term_3.strftime("%d/%m/%Y")
        if semester_instance.end_term_1:
            semester_plan_dates['end_term_1'] = semester_instance.end_term_1.strftime("%d/%m/%Y")
        if semester_instance.end_term_2:
            semester_plan_dates['end_term_2'] = semester_instance.end_term_2.strftime("%d/%m/%Y")
        if semester_instance.end_term_3:
            semester_plan_dates['end_term_3'] = semester_instance.end_term_3.strftime("%d/%m/%Y")

        faculty_advisors_instances_list = get_facutly_advisors(selected_department, selected_batch)

        faculty_advisors_list = []
        for entry in faculty_advisors_instances_list:
            faculty_advisors_list.append(str(entry.pk))

        for staff_inst in faculty_advisors_instances_list:
            staff_from_departments.append(staff_inst.department)

    # print(staff_from_departments)
    for each in staff_from_departments:
        if each.id != department_instance.id:
            if str(each.id) not in selected_departments:
                selected_departments.append(str(each.id))

    pprint(selected_departments)

    list_of_staffs = []
    for each in staff.objects.filter(department=department_instance, user__is_approved=True).exclude(
            designation='Network Admin').order_by('first_name'):
        temp = {}
        temp['designation'] = each.designation
        temp['acronym'] = each.department.acronym
        temp['staff_name'] = str(each)
        temp['id'] = str(each.id)
        list_of_staffs.append(temp)

    for each_department in selected_departments:
        for each in staff.objects.filter(department_id=each_department, user__is_approved=True).exclude(
                designation='Network Admin').order_by('first_name'):
            temp = {}
            temp['designation'] = each.designation
            temp['acronym'] = each.department.acronym
            temp['staff_name'] = str(each)
            temp['id'] = str(each.id)
            list_of_staffs.append(temp)

    if not selected_department:
        selected_department = selected_department.acronym

    # print('selected_department='+str(selected_department))

    data = {
        'available_departments': available_departments,
        'all_core_departments': all_core_departments,
        'selected_department': selected_department,
        'selected_departments': selected_departments,
        'selected_department_id': selected_department_id,
        'list_of_staffs': list_of_staffs,
        # 'selected_batch': selected_batch,
        'current_department': current_department,
        'semester_plan_dates': semester_plan_dates,
        'faculty_advisors_list': faculty_advisors_list
    }

    return render(
        request,
        'curriculum/semester_plan_first_year.html',
        data
    )
