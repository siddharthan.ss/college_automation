from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from accounts.models import student, active_batches
from curriculum.models import department_sections, section_students
from curriculum.views.common_includes import get_active_batches_for_section_split


@login_required
@permission_required('curriculum.can_split_sections')
def set_no_of_sections(request):
    list_of_active_batches = get_active_batches_for_section_split(request.user)
    choosen_active_batch_pk = list_of_active_batches[0]['active_batch_instance'].pk

    active_batch_instance = active_batches.objects.get(pk=choosen_active_batch_pk)

    student_list = student.objects.filter(
        department=active_batch_instance.department
    ).filter(
        batch_id=active_batch_instance.batch
    ).filter(
        user__is_approved=True
    ).order_by(
        'roll_no'
    )

    if request.method == "POST":
        choosen_active_batch_pk = int(request.POST.get('selected_active_batch_pk'))
        active_batch_instance = active_batches.objects.get(pk=choosen_active_batch_pk)

        student_list = student.objects.filter(
            department=active_batch_instance.department
        ).filter(
            batch_id=active_batch_instance.batch
        ).filter(
            user__is_approved=True
        ).order_by(
            'roll_no'
        )

        if request.POST.get('no_of_section_submit'):

            no_of_sections = request.POST.get('no_of_sections')
            if int(no_of_sections) > 0:
                department_sections.objects.filter(
                    department=active_batch_instance.department
                ).filter(
                    batch=active_batch_instance.batch
                ).all().delete()
                i = 0
                while (i < int(no_of_sections)):
                    department_sections.objects.create(
                        department=active_batch_instance.department,
                        batch=active_batch_instance.batch,
                        section_name=chr(65 + i)
                    )
                    i = i + 1

        if request.POST.get('split_student_submit'):
            for entry in student_list:
                section_pk = request.POST.get('section_' + str(entry.roll_no))
                if section_pk != "None":
                    department_sections_instance = department_sections.objects.get(pk=section_pk)
                    already_alloted_query = section_students.objects.filter(student=entry)
                    if already_alloted_query.exists():
                        already_alloted_instance = already_alloted_query.get()
                        already_alloted_instance.section = department_sections_instance
                        already_alloted_instance.save()
                    else:
                        section_students.objects.create(
                            student=entry,
                            section=department_sections_instance
                        )

    list_of_department_sections = department_sections.objects.filter(
        department=active_batch_instance.department
    ).filter(
        batch=active_batch_instance.batch
    )

    no_of_sections = list_of_department_sections.count()

    student_list_with_section = []

    for stud in student_list:
        temp = {}
        temp['roll_no'] = stud.roll_no
        temp['name'] = str(stud)
        section_name = None
        if section_students.objects.filter(section__in=list_of_department_sections).filter(student=stud).exists():
            section_students_inst = section_students.objects.filter(section__in=list_of_department_sections).get(student=stud)
            section_name = section_students_inst.section.section_name
        # for entry in list_of_department_sections:
        #     section_students_query = section_students.objects.filter(section=entry)
        #     list_of_students_in_this_section = []
        #     for stud_section_inst in section_students_query:
        #         list_of_students_in_this_section.append(stud_section_inst.student)
        #
        #     pprint(list_of_students_in_this_section)
        #     if stud in list_of_students_in_this_section:
        #         section_name = entry.section_name

        temp['section_name'] = section_name
        student_list_with_section.append(temp)

    pprint(student_list_with_section)

    context_data = {
        'no_of_sections': no_of_sections,
        'student_list_with_section': student_list_with_section,
        'list_of_active_batches': list_of_active_batches,
        'choosen_active_batch_pk': int(choosen_active_batch_pk),
        'list_of_department_sections': list_of_department_sections,
    }

    return render(
        request,
        'section_split/section_split.html',
        context_data
    )
