import hashlib
from datetime import datetime

from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponseRedirect
from django.shortcuts import render

from curriculum.models import attendance, semester, courses
from curriculum.views.common_includes import get_list_of_students, get_filtered_students_for_lab

from curriculum.views.attendance import mark_attendance


@login_required
@permission_required('curriculum.can_mark_attendance')
def add_student_to_attendance(request, semester_pk, course_id, attendance_date, hour):
    attendance_date_inst = datetime.strptime(attendance_date, "%Y-%m-%d")
    attendance_query = attendance.objects.filter(
        semester_id=semester_pk,
        course_id=course_id,
        date= attendance_date_inst,
        hour=hour
    )

    if not attendance_query.exists():
        msg = {
            'page_title': 'Entry not found',
            'title': 'Not already Marked',
            'description': 'You should have already marked attendance to use this option',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    attendance_instance = attendance_query.get()

    semester_instance = semester.objects.get(pk=semester_pk)
    course_instance = courses.objects.get(pk=course_id)

    current_list_of_students = get_list_of_students(semester_instance,course_instance)

    if course_instance.subject_type == "P":
        batch_split_inst,current_list_of_students = get_filtered_students_for_lab(semester_instance,current_list_of_students,course_instance,attendance_date_inst)

    temp_student_list = []
    for entry in current_list_of_students:
        stud = entry['student_obj']
        print(stud)
        if stud.date_of_joining <= attendance_date_inst.date():
            temp_student_list.append(entry)

    current_list_of_students = temp_student_list

    final_current_list_of_student_instances = []
    for student_entry in current_list_of_students:
        final_current_list_of_student_instances.append(student_entry['student_obj'])

    existing_list_of_student_instances = []
    for student_entry in attendance_instance.present_students.all():
        existing_list_of_student_instances.append(student_entry)
    for student_entry in attendance_instance.absent_students.all():
        existing_list_of_student_instances.append(student_entry)
    for student_entry in attendance_instance.onduty_students.all():
        existing_list_of_student_instances.append(student_entry)

    if len(final_current_list_of_student_instances) == len(existing_list_of_student_instances):
        msg = {
            'page_title': 'No students',
            'title': 'No extra students',
            'description': 'You have already marked attendance for all enrolled students',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    else:
        request.session['can_add_student_to_attendance'] = (hashlib.sha1(str(semester_pk).encode('utf8') + str(course_id).encode('utf8') + str(attendance_date).encode('utf8') + str(hour).encode('utf8'))).hexdigest()
        # return mark_attendance(request,semester_pk,course_id,attendance_date,hour)
        return HttpResponseRedirect('/attendance/' + str(semester_pk) + '/' + str(course_id) + '/' + str(attendance_date) + '/' + str(hour) + '/' )

