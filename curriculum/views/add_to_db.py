from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect

from accounts.models import regulation
from curriculum.models import courses
from curriculum.views.common_includes import *


@login_required
@permission_required('curriculum.can_view_enrollment_details')
def add_regulation_to_db(request):
    if request.method == 'POST':
        if 'startyear' in request.POST:
            startyear = int(request.POST.get('startyear'))
            endyear = int(request.POST.get('endyear'))

            print('startyear=' + str(startyear))
            print('endyear=' + str(endyear))

            regulation.objects.create(start_year=startyear, end_year=endyear)

        else:
            list_of_id = []
            for each in regulation.objects.all():
                list_of_id.append(str(each.id))

            required_edit = ''

            for each in list_of_id:
                if str(request.POST.get(each)) == 'Edit':
                    # print('got_paper_id='+str(each))
                    required_edit = int(each)

            request.session['editable_regulation_id'] = required_edit
            return redirect('../edit_regulation')

    list_of_regulations = []
    for each in regulation.objects.all().order_by('start_year'):
        list_of_regulations.append(each)

    return render(request, 'add_to_db/add_regulation_to_db.html', {'list_of_regulations': list_of_regulations})


@login_required
@permission_required('curriculum.can_view_enrollment_details')
def edit_regulation(request):
    if request.method == 'POST':
        if 'startyear' in request.POST:
            got_startyear = int(request.POST.get('startyear'))
            got_endyear = int(request.POST.get('endyear'))
            required_edit_id = int(request.session['editable_regulation_id'])

            del request.session['editable_regulation_id']

            regulation.objects.filter(id=required_edit_id).update(start_year=got_startyear, end_year=got_endyear)

        return redirect('../create_regulation')

    else:
        required_edit = int(request.session['editable_regulation_id'])

        regulation_var = regulation.objects.get(id=required_edit)

        return render(request, 'add_to_db/edit_regulation_to_db.html', {'regulation': regulation_var})


@login_required
@permission_required('curriculum.can_create_semester_plan')
def add_courses_to_db_first_year(request):
    user_instance = staff.objects.get(user=request.user)

    if not staffBelongsToNonCore(user_instance):
        return HttpResponseRedirect('/create_course')

    all_core_departments = department.objects.filter(is_core=True)
    selected_department = None
    if len(all_core_departments) > 0:
        selected_department = all_core_departments[0]

    if request.method == 'POST':
        if 'selected_department_acronym' in request.POST:
            selected_department_acronym = request.POST.get("selected_department_acronym")
            selected_department = department.objects.get(acronym=selected_department_acronym)

        if 'addcourse' in request.POST:
            got_courseid = str(request.POST.get('courseid'))
            got_department = str(request.POST.get('course_department'))
            selected_department = department.objects.get(id=got_department)
            programme = str(request.POST.get('programme'))
            if courses.objects.filter(course_id=got_courseid, programme=programme).exists():
                instance = courses.objects.get(course_id=got_courseid, programme=programme)
                msg = {
                    'page_title': 'Course Id Exists',
                    'title': 'Course Id Exists ! ',
                    'description': 'Course ID already exists for ' + str(instance.course_name) + ' of ' + str(
                        instance.department) + ' department.',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
            got_regulation_pk = str(request.POST.get('regulation'))
            # start_year = got_regulation.split('-')
            got_course_type = str(request.POST.get('coursetype'))
            # print('got_course_type=' + str(got_course_type))

            got_semester = int(request.POST.get('semester'))

            elective = False
            open_course = False
            one_credit = False
            if got_course_type == 'elective':
                elective = True
            elif got_course_type == 'open':
                open_course = True
            elif got_course_type == 'onecredit':
                one_credit = True

            regulation_instance = regulation.objects.get(pk=got_regulation_pk)

            courses.objects.create(
                department=selected_department,
                semester=int(got_semester),
                course_id=got_courseid.upper(),
                course_name=str(request.POST.get('coursename')),
                subject_type=str(request.POST.get('subjecttype')),
                CAT=str(request.POST.get('cat')),
                CAT_marks=int(request.POST.get('catmarks')),
                end_semester_marks=int(request.POST.get('endsemmarks')),
                L_credits=int(request.POST.get('lcredits')),
                T_credits=int(request.POST.get('tcredits')),
                P_credits=int(request.POST.get('pcredits')),
                C_credits=int(request.POST.get('ccredits')),
                code=str(request.POST.get('code')),
                programme=str(request.POST.get('programme')),
                is_elective=elective,
                is_open=open_course,
                is_one_credit=one_credit,
                regulation=regulation_instance
            )

        if request.is_ajax():
            if 'view' in request.POST:
                got_courseid = str(request.POST.get('courseid'))
                request.session['view_course_id'] = got_courseid
                # print('got_courseid=' + str(got_courseid))
                dictionary = {}
                return JsonResponse(dictionary)

    list_of_courses_all = []
    for each in courses.objects.filter(department=selected_department, programme='UG', semester__lte=2).order_by(
            'regulation').order_by(
            'semester'):
        list_of_courses_all.append(each)

    list_of_regulations = []
    for each in regulation.objects.all():
        list_of_regulations.append(each)

    available_CAT = []

    for each in courses.CAT_CHOICES:
        available_CAT.append(each[0])

    return render(request, 'add_to_db/add_courses_to_db_first_year.html',
                  {
                      'list_of_courses': list_of_courses_all,
                      'list_of_regulations': list_of_regulations,
                      'all_core_departments': all_core_departments,
                      'selected_department': selected_department,
                      'available_CAT': available_CAT
                  })


@login_required
@permission_required('curriculum.can_create_semester_plan')
def add_courses_to_db(request):
    user_instance = staff.objects.get(user=request.user)

    currrent_department = get_eligible_departments_for_pc(user_instance)

    # currrent_department = user_instance.department

    # santity check for first year
    if staffBelongsToNonCore(user_instance):
        if staffNotEligible(user_instance):
            msg = {
                'page_title': 'Access Denied',
                'title': 'No permission',
                'description': 'Your department does not have this permission. Contact ' + str(
                    get_first_year_managing_department()) + ' department for this settings',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
        return HttpResponseRedirect('/create_course_first_year')

    department_instance = user_instance.department
    if not department_instance.is_core:
        msg = {
            'page_title': 'Access Denied',
            'title': 'No permission',
            'description': 'Your department does not have this permission',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'coursename' in request.POST:
            got_courseid = str(request.POST.get('courseid'))
            department_pk = request.POST.get('department')
            department_instance = department.objects.get(pk=department_pk)
            programme = str(request.POST.get('programme'))
            if courses.objects.filter(course_id=got_courseid, programme=programme).exists():
                msg = {
                    'page_title': 'Course Id Exists',
                    'title': 'Course Id Exists ! ',
                    'description': 'Course ID already exists for ' + str(got_courseid) + '.',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
            got_regulation_pk = str(request.POST.get('regulation'))
            # start_year = got_regulation.split('-')
            got_course_type = str(request.POST.get('coursetype'))
            # print('got_course_type=' + str(got_course_type))

            got_semester = int(request.POST.get('semester'))

            elective = False
            open_course = False
            one_credit = False
            if got_course_type == 'elective':
                elective = True
            elif got_course_type == 'open':
                open_course = True
            elif got_course_type == 'onecredit':
                one_credit = True

            regulation_instance = regulation.objects.get(pk=got_regulation_pk)

            courses.objects.create(
                department=department_instance,
                semester=int(got_semester),
                course_id=got_courseid.upper(),
                course_name=str(request.POST.get('coursename')),
                subject_type=str(request.POST.get('subjecttype')),
                CAT=str(request.POST.get('cat')),
                CAT_marks=int(request.POST.get('catmarks')),
                end_semester_marks=int(request.POST.get('endsemmarks')),
                L_credits=int(request.POST.get('lcredits')),
                T_credits=int(request.POST.get('tcredits')),
                P_credits=int(request.POST.get('pcredits')),
                C_credits=int(request.POST.get('ccredits')),
                code=str(request.POST.get('code')),
                programme=str(request.POST.get('programme')),
                is_elective=elective,
                is_open=open_course,
                is_one_credit=one_credit,
                regulation=regulation_instance
            )

        if request.is_ajax():
            if 'department' in request.POST:
                dictionary = {}
                department_pk = request.POST.get('department')
                department_instance = department.objects.get(pk=department_pk)
                list_of_programmes = get_eligible_programmes_for_pc(user_instance, department_instance)

                list = []

                for each in list_of_programmes:
                    list.append(each.acronym)

                dictionary['list'] = list

                return JsonResponse(dictionary)

            if 'view' in request.POST:
                got_courseid = str(request.POST.get('courseid'))
                request.session['view_course_id'] = got_courseid
                # print('got_courseid=' + str(got_courseid))
                dictionary = {}
                return JsonResponse(dictionary)

            else:
                got_programme = str(request.POST.get('programme'))
                got_regulation = str(request.POST.get('regulation'))
                # print('got_programme=' + str(got_programme))
                # print('got_regulation=' + str(got_regulation))

                if got_regulation != 'all':
                    start_year = got_regulation.split('-')
                    regulation_instance = regulation.objects.get(start_year=start_year[0], end_year=start_year[1])

                dictionary = {}
                list_of_courses = []

                if got_programme == 'all' and got_regulation == 'all':
                    for each in courses.objects.filter(department__in=currrent_department):
                        temp = {}
                        if each.semester == 0:
                            temp['semester'] = 'Exceptional'
                        else:
                            temp['semester'] = each.semester
                        temp['course_id'] = each.course_id
                        temp['pk'] = each.pk
                        temp['course_name'] = each.course_name
                        temp['subject_type'] = each.subject_type
                        temp['CAT'] = each.CAT
                        temp['CAT_marks'] = each.CAT_marks
                        temp['end_semester_marks'] = each.end_semester_marks
                        temp['L_credits'] = each.L_credits
                        temp['T_credits'] = each.T_credits
                        temp['P_credits'] = each.P_credits
                        temp['C_credits'] = each.C_credits
                        temp['code'] = each.code
                        temp['programme'] = each.programme
                        if each.is_elective == True:
                            temp['is_elective'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_elective'] = ''
                        if each.is_open == True:
                            temp['is_open'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_open'] = ''
                        if each.is_one_credit == True:
                            temp['is_one_credit'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_one_credit'] = ''
                        temp['regulation'] = str(each.regulation)
                        list_of_courses.append(temp)
                elif got_programme == 'all' and got_regulation != 'all':
                    for each in courses.objects.filter(department__in=currrent_department,
                                                       regulation=regulation_instance):
                        temp = {}
                        if each.semester == 0:
                            temp['semester'] = 'Exceptional'
                        else:
                            temp['semester'] = each.semester
                        temp['course_id'] = each.course_id
                        temp['pk'] = each.pk
                        temp['course_name'] = each.course_name
                        temp['subject_type'] = each.subject_type
                        temp['CAT'] = each.CAT
                        temp['CAT_marks'] = each.CAT_marks
                        temp['end_semester_marks'] = each.end_semester_marks
                        temp['L_credits'] = each.L_credits
                        temp['T_credits'] = each.T_credits
                        temp['P_credits'] = each.P_credits
                        temp['C_credits'] = each.C_credits
                        temp['code'] = each.code
                        temp['programme'] = each.programme
                        if each.is_elective == True:
                            temp['is_elective'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_elective'] = ''
                        if each.is_open == True:
                            temp['is_open'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_open'] = ''
                        if each.is_one_credit == True:
                            temp['is_one_credit'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_one_credit'] = ''
                        temp['regulation'] = str(each.regulation)
                        list_of_courses.append(temp)
                elif got_programme != 'all' and got_regulation == 'all':
                    for each in courses.objects.filter(department__in=currrent_department, programme=got_programme):
                        temp = {}
                        if each.semester == 0:
                            temp['semester'] = 'Exceptional'
                        else:
                            temp['semester'] = each.semester
                        temp['course_id'] = each.course_id
                        temp['pk'] = each.pk
                        temp['course_name'] = each.course_name
                        temp['subject_type'] = each.subject_type
                        temp['CAT'] = each.CAT
                        temp['CAT_marks'] = each.CAT_marks
                        temp['end_semester_marks'] = each.end_semester_marks
                        temp['L_credits'] = each.L_credits
                        temp['T_credits'] = each.T_credits
                        temp['P_credits'] = each.P_credits
                        temp['C_credits'] = each.C_credits
                        temp['code'] = each.code
                        temp['programme'] = each.programme
                        if each.is_elective == True:
                            temp['is_elective'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_elective'] = ''
                        if each.is_open == True:
                            temp['is_open'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_open'] = ''
                        if each.is_one_credit == True:
                            temp['is_one_credit'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_one_credit'] = ''
                        temp['regulation'] = str(each.regulation)
                        list_of_courses.append(temp)
                else:
                    for each in courses.objects.filter(department__in=currrent_department, programme=got_programme,
                                                       regulation=regulation_instance).order_by('semester'):
                        temp = {}
                        if each.semester == 0:
                            temp['semester'] = 'Exceptional'
                        else:
                            temp['semester'] = each.semester
                            temp['pk'] = each.pk
                        temp['course_id'] = each.course_id
                        temp['course_name'] = each.course_name
                        temp['subject_type'] = each.subject_type
                        temp['CAT'] = each.CAT
                        temp['CAT_marks'] = each.CAT_marks
                        temp['end_semester_marks'] = each.end_semester_marks
                        temp['L_credits'] = each.L_credits
                        temp['T_credits'] = each.T_credits
                        temp['P_credits'] = each.P_credits
                        temp['C_credits'] = each.C_credits
                        temp['code'] = each.code
                        temp['programme'] = each.programme
                        if each.is_elective == True:
                            temp['is_elective'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_elective'] = ''
                        if each.is_open == True:
                            temp['is_open'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_open'] = ''
                        if each.is_one_credit == True:
                            temp['is_one_credit'] = '<i class="glyphicon glyphicon-ok"></i>'
                        else:
                            temp['is_one_credit'] = ''
                        temp['regulation'] = str(each.regulation)
                        list_of_courses.append(temp)

                dictionary['list'] = list_of_courses

                return JsonResponse(dictionary)

    list_of_courses_all = []
    for each in courses.objects.filter(department__in=currrent_department):
        list_of_courses_all.append(each)

    list_of_regulations = []
    for each in regulation.objects.all():
        list_of_regulations.append(each)

    available_CAT = []

    for each in courses.CAT_CHOICES:
        available_CAT.append(each[0])

    list_of_departments = get_eligible_departments_for_pc(user_instance)

    return render(request, 'add_to_db/add_courses_to_db.html',
                  {'list_of_courses': list_of_courses_all, 'list_of_regulations': list_of_regulations,
                   'available_CAT': available_CAT,
                   'list_of_departments': list_of_departments
                   })


@login_required
@permission_required('curriculum.can_create_semester_plan')
def view_course_form_db(request):
    user_instance = staff.objects.get(user=request.user)

    if request.method == 'POST':
        if request.is_ajax():
            if 'department' in request.POST:
                dictionary = {}
                department_pk = request.POST.get('department')
                department_instance = department.objects.get(pk=department_pk)
                list_of_programmes = get_eligible_programmes_for_pc(user_instance, department_instance)

                list = []

                for each in list_of_programmes:
                    list.append(each.acronym)

                dictionary['list'] = list

                return JsonResponse(dictionary)

        elif 'originalid' in request.POST:
            original_course_pk = str(request.POST.get('originalid'))
            got_courseid = str(request.POST.get('courseid'))
            got_course_type = str(request.POST.get('coursetype'))
            got_regulation = str(request.POST.get('regulation'))
            department_pk = request.POST.get('department')

            regulation_instance = regulation.objects.get(pk=got_regulation)

            department_instance = department.objects.get(pk=department_pk)

            got_semester = int(request.POST.get('semester'))

            elective = False
            open_course = False
            one_credit = False
            if got_course_type == 'elective':
                elective = True
            elif got_course_type == 'open':
                open_course = True
            elif got_course_type == 'onecredit':
                one_credit = True

            courses.objects.filter(pk=original_course_pk).update(
                department=department_instance,
                semester=int(got_semester),
                # course_id=got_courseid.upper(),
                course_name=str(request.POST.get('coursename')),
                subject_type=str(request.POST.get('subjecttype')),
                CAT=str(request.POST.get('cat')),
                CAT_marks=int(request.POST.get('catmarks')),
                end_semester_marks=int(request.POST.get('endsemmarks')),
                L_credits=int(request.POST.get('lcredits')),
                T_credits=int(request.POST.get('tcredits')),
                P_credits=int(request.POST.get('pcredits')),
                C_credits=int(request.POST.get('ccredits')),
                code=str(request.POST.get('code')),
                programme=str(request.POST.get('programme')),
                is_elective=elective,
                is_open=open_course,
                is_one_credit=one_credit,
                regulation=regulation_instance
            )

    if not 'view_course_id' in request.session:
        return redirect('/create_course')

    view_course = str(request.session['view_course_id'])
    course = courses.objects.get(pk=view_course)

    list_of_regulations = []
    for each in regulation.objects.all():
        list_of_regulations.append(each)

    available_CAT = []

    for each in courses.CAT_CHOICES:
        available_CAT.append(each[0])

    list_of_departments = get_eligible_departments_for_pc(user_instance)

    return render(request, 'add_to_db/view_course_from_db.html',
                  {'course': course, 'list_of_regulations': list_of_regulations, 'available_CAT': available_CAT,
                   'list_of_departments': list_of_departments})
