from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Group
from django.shortcuts import render

from accounts.models import site_settings, staff


@login_required
@permission_required('accounts.can_allot_coe')
def allot_coe_head(request):
    selected_staff_id = None

    if request.method == "POST":
        selected_staff_id = request.POST.get('selected_staff_id')
        query = site_settings.objects.filter(key="COE_HEAD")

        g = Group.objects.get(name='coe_staff')
        if query.exists():
            required_instance = query.get()

            # remove old user from coe group if he is been already alloted
            old_coe_head_staff = staff.objects.get(pk=required_instance.value)
            old_coe_customUser_inst = old_coe_head_staff.user
            g.user_set.remove(old_coe_customUser_inst)
            g.save()

            required_instance.value = str(selected_staff_id)
            required_instance.save()
        else:
            site_settings.objects.create(
                key="COE_HEAD",
                value=str(selected_staff_id),
            )

        # add new coe head to the coe_staff group
        new_coe_head_staff = staff.objects.get(pk=selected_staff_id)
        new_coe_customUser_inst = new_coe_head_staff.user
        g.user_set.add(new_coe_customUser_inst)
        g.save()

    query = site_settings.objects.filter(key="COE_HEAD")
    if query.exists():
        selected_staff_id = query.get().value

    print(selected_staff_id)

    if selected_staff_id:
        selected_staff_id = int(selected_staff_id)
    else:
        selected_staff_id = None

    data = {
        'all_staffs': staff.objects.filter(user__is_approved=True),
        'selected_staff_id': selected_staff_id,
    }

    return render(
        request,
        'coe/allot_coe_head.html',
        data
    )
