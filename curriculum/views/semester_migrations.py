from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Group
from django.shortcuts import render
from django_common.http import JsonResponse

from accounts.models import staff, student, active_batches, batch, regulation
from curriculum.models import semester_migration, department_sections


@login_required
@permission_required('curriculum.can_create_semester_plan')
def migrate_semesters(request):
    staff_inst = staff.objects.get(user=request.user)
    user_department = staff_inst.department
    modal = None

    semester_migration_instances = semester_migration.objects.filter(department=user_department).order_by(
        'semester_number')

    if request.method == 'POST' and request.POST.get('migration_group'):
        group_to_be_migrated = request.POST.get('migration_group')

        if group_to_be_migrated == 'UG_1':
            """
            student_instances = student.objects.filter(qualification='UG').filter(current_semester__lte=2)
            for entry in student_instances:
                print(entry.current_semester)
                current_sem_number = entry.current_semester
                entry.current_semester += 1
                entry.save()
            """

            act_batch_obj = active_batches.objects.filter(department=user_department).filter(
                current_semester_number_of_this_batch__lte=2).get(
                programme='UG')
            act_batch_obj.current_semester_number_of_this_batch += 1
            act_batch_obj.save()
            if request.POST.get('selected_regulation'):
                # creating new UG batch
                start_year = act_batch_obj.batch.start_year + 1
                end_year = start_year + 4
                try:
                    got_regulation = request.POST.get('selected_regulation')
                    regulation_inst = regulation.objects.get(pk=got_regulation)
                except regulation.DoesNotExist:
                    regulation_inst = None
                new_batch_obj = batch.objects.get_or_create(
                    start_year=start_year,
                    end_year=end_year,
                    programme='UG',
                    regulation=regulation_inst
                )
                new_batch_obj = new_batch_obj[0]
                print('created ' + str(new_batch_obj))
                # add new active batch obj
                new_active_batch_obj = active_batches.objects.create(
                    batch=new_batch_obj,
                    department=user_department,
                    current_semester_number_of_this_batch=1,
                    programme=new_batch_obj.programme,
                )
                print('Created  + ' + str(new_active_batch_obj))
                # create new department section
                new_department_section_obj = department_sections.objects.create(
                    batch=new_batch_obj,
                    department=user_department,
                    section_name='A',
                )
                print('Created + ' + str(new_department_section_obj))
                flag = False
            try:
                semester_migration_instances = semester_migration_instances.filter(semester__batch__programme='UG')
                semester_migration_instances = semester_migration_instances.filter(semester_number__lte=2)
                for semester_migration_inst in semester_migration_instances:
                    fa_group = Group.objects.get(name='faculty_advisor')
                    list_of_faculty_advisors = []
                    for faculty in semester_migration_inst.semester.faculty_advisor.all():
                        print("staff = " + str(faculty))
                        for active_semester in semester_migration.objects.all():
                            print("active semester = " + str(active_semester))
                            semester_instance = active_semester.semester
                            if semester_instance != semester_migration_inst.semester:
                                for entry in semester_instance.faculty_advisor.all():
                                    list_of_faculty_advisors.append(entry)
                    print("list of fa")
                    print(list_of_faculty_advisors)
                    for faculty in semester_migration_inst.semester.faculty_advisor.all():
                        if faculty not in list_of_faculty_advisors:
                            fa_group.user_set.remove(faculty.user)
                            fa_group.save()
                semester_migration_instances.delete()
            except semester_migration.DoesNotExist:
                print('No semester plans hav been configured for this batch yet')
        elif group_to_be_migrated == 'UG_other':
            """
            student_instances = student.objects.filter(qualification='UG').filter(current_semester__gt=2).filter(
                current_semester__lte=8)
            for entry in student_instances:
                current_sem_number = entry.current_semester
                print('Curr sem num ' + str(current_sem_number))
                entry.current_semester += 1
                entry.save()
            """
            act_batch_obj = active_batches.objects.filter(department=user_department).filter(
                current_semester_number_of_this_batch__gt=2).filter(
                programme='UG')  # increment current smester number of all batches
            for each in act_batch_obj:
                each.current_semester_number_of_this_batch += 1
                print(each.current_semester_number_of_this_batch)
                if each.current_semester_number_of_this_batch > 8:
                    each.delete()
                else:
                    each.save()
            semester_migration_instances = semester_migration_instances.filter(semester__batch__programme='UG')
            semester_migration_instances = semester_migration_instances.filter(semester_number__gt=2).filter(
                semester_number__lte=8)
            for semester_migration_inst in semester_migration_instances:
                fa_group = Group.objects.get(name='faculty_advisor')
                list_of_faculty_advisors = []
                for faculty in semester_migration_inst.semester.faculty_advisor.all():
                    print("staff = " + str(faculty))
                    for active_semester in semester_migration.objects.all():
                        print("active semester = " + str(active_semester))
                        semester_instance = active_semester.semester
                        if semester_instance != semester_migration_inst.semester:
                            for entry in semester_instance.faculty_advisor.all():
                                list_of_faculty_advisors.append(entry)
                print("list of fa")
                print(list_of_faculty_advisors)
                for faculty in semester_migration_inst.semester.faculty_advisor.all():
                    if faculty not in list_of_faculty_advisors:
                        fa_group.user_set.remove(faculty.user)
                        fa_group.save()
                # semester_migration_inst.semester.faculty_advisor.remove(fa)
            semester_migration_instances.delete()
        elif group_to_be_migrated == 'PG_1':
            """
            student_instances = student.objects.filter(qualification='PG').filter(current_semester__lte=2)
            for entry in student_instances:
                entry.current_semester += 1
                entry.save()
            """

            act_batch_obj = active_batches.objects.filter(department=user_department).filter(
                current_semester_number_of_this_batch__lte=2).get(
                programme='PG')
            act_batch_obj.current_semester_number_of_this_batch += 1
            act_batch_obj.save()
            if request.POST.get('selected_regulation'):
                # creating new UG batch
                start_year = act_batch_obj.batch.start_year + 1
                end_year = start_year + 4
                try:
                    got_regulation = request.POST.get('selected_regulation')
                    regulation_inst = regulation.objects.get(pk=got_regulation)
                except regulation.DoesNotExist:
                    regulation_inst = None
                new_batch_obj = batch.objects.get_or_create(
                    start_year=start_year,
                    end_year=end_year,
                    programme='PG',
                    regulation=regulation_inst
                )
                new_batch_obj = new_batch_obj[0]
                print('created ' + str(new_batch_obj))
                # add new active batch obj
                new_active_batch_obj = active_batches.objects.create(
                    batch=new_batch_obj,
                    department=user_department,
                    current_semester_number_of_this_batch=1,
                    programme=new_batch_obj.programme,
                )
                print('Created  + ' + str(new_active_batch_obj))
                # create new department section
                new_department_section_obj = department_sections.objects.create(
                    batch=new_batch_obj,
                    department=user_department,
                    section_name='A',
                )
                print('Created + ' + str(new_department_section_obj))
            try:
                semester_migration_instances = semester_migration_instances.filter(semester__batch__programme='PG')
                semester_migration_instances = semester_migration_instances.filter(semester_number__lte=2)
                for semester_migration_inst in semester_migration_instances:
                    fa_group = Group.objects.get(name='faculty_advisor')
                    list_of_faculty_advisors = []
                    for faculty in semester_migration_inst.semester.faculty_advisor.all():
                        print("staff = " + str(faculty))
                        for active_semester in semester_migration.objects.all():
                            print("active semester = " + str(active_semester))
                            semester_instance = active_semester.semester
                            if semester_instance != semester_migration_inst.semester:
                                for entry in semester_instance.faculty_advisor.all():
                                    list_of_faculty_advisors.append(entry)
                    print("list of fa")
                    print(list_of_faculty_advisors)
                    for faculty in semester_migration_inst.semester.faculty_advisor.all():
                        if faculty not in list_of_faculty_advisors:
                            fa_group.user_set.remove(faculty.user)
                            fa_group.save()
                semester_migration_instances.delete()
            except semester_migration.DoesNotExist:
                print('No semester plans hav been configured for this batch yet')
        elif group_to_be_migrated == 'PG_other':
            """
            student_instances = student.objects.filter(qualification='PG').filter(current_semester__gt=2).filter(
                current_semester__lte=8)
            for entry in student_instances:
                entry.current_semester += 1
                entry.save()
            """

            act_batch_obj = active_batches.objects.filter(department=user_department).filter(
                current_semester_number_of_this_batch__gt=2).filter(
                programme='PG')  # increment current smester number of all batches
            for each in act_batch_obj:
                each.current_semester_number_of_this_batch += 1
                if each.current_semester_number_of_this_batch > 4:
                    each.delete()
                else:
                    each.save()

            semester_migration_instances = semester_migration_instances.filter(semester__batch__programme='PG')
            semester_migration_instances = semester_migration_instances.filter(semester_number__gt=2).filter(
                semester_number__lte=8)
            for semester_migration_inst in semester_migration_instances:
                fa_group = Group.objects.get(name='faculty_advisor')
                list_of_faculty_advisors = []
                for faculty in semester_migration_inst.semester.faculty_advisor.all():
                    print("staff = " + str(faculty))
                    for active_semester in semester_migration.objects.all():
                        print("active semester = " + str(active_semester))
                        semester_instance = active_semester.semester
                        if semester_instance != semester_migration_inst.semester:
                            for entry in semester_instance.faculty_advisor.all():
                                list_of_faculty_advisors.append(entry)
                print("list of fa")
                print(list_of_faculty_advisors)
                for faculty in semester_migration_inst.semester.faculty_advisor.all():
                    if faculty not in list_of_faculty_advisors:
                        fa_group.user_set.remove(faculty.user)
                        fa_group.save()
            semester_migration_instances.delete()

        modal = {
            'heading': 'Migration Done',
            'body': 'The selected migration has been applied  successfully',
        }

    semester_migration_instances = semester_migration.objects.filter(department=user_department).order_by(
        'semester_number')

    if modal is not None:
        toggle_modal = True
    else:
        toggle_modal = False

    return render(request, 'curriculum/current_semester_plans.html',
                  {'active_semesters': semester_migration_instances, 'toggle_model': toggle_modal, 'modal': modal})


@login_required
@permission_required('curriculum.can_create_semester_plan')
def check_migration_consitency(request):
    if request.method == 'POST' and request.POST.get('selected_group'):
        got_group = request.POST.get('selected_group')
        print(got_group)
        staff_inst = staff.objects.get(user=request.user)
        dept_obj = staff_inst.department
        data = {}
        data['code'] = 1
        data['msg'] = ''

        if got_group == 'UG_1':
            # check for first sem
            # will be handled by consitency check mechanisms

            # check if current student are in second semester

            ongoing_first_year_semester = active_batches.objects.filter(
                department=dept_obj
            ).filter(
                programme='UG'
            ).get(
                current_semester_number_of_this_batch__lte=2
            ).current_semester_number_of_this_batch

            conflicting_sem_to_check = ongoing_first_year_semester + 2

            no_of_second_year_students = student.objects.filter(
                department=dept_obj
            ).filter(
                qualification='UG'
            ).filter(
                current_semester=conflicting_sem_to_check
            ).count()

            active_second_year_batch = active_batches.objects.filter(
                department=dept_obj
            ).filter(
                programme='UG'
            ).filter(
                current_semester_number_of_this_batch=conflicting_sem_to_check
            ).exists()

            active_second_year_semesters = semester_migration.objects.filter(
                department=dept_obj
            ).filter(
                semester_number=conflicting_sem_to_check
            ).exists()

            if active_second_year_batch or active_second_year_semesters:
                msg = '<p>Please migrate 2nd,3rd and 4th Year students first</p>'
                data['msg'] = msg
                data['code'] = 2

                return JsonResponse(data)

            active_second_year_batch = active_batches.objects.filter(
                department=dept_obj
            ).filter(
                programme='UG'
            ).filter(
                current_semester_number_of_this_batch=3
            ).exists()

            if not active_second_year_batch and ongoing_first_year_semester != 1:
                data['code'] = 3
                regulation_instances = regulation.objects.all().order_by('-start_year')
                list = []
                for each in regulation_instances:
                    temp = {}
                    temp['pk'] = each.pk
                    temp['string'] = str(each)
                    list.append(temp)
                data['reg_list'] = list
                return JsonResponse(data)

        elif got_group == 'PG_1':
            # check for first sem
            # will be handled by consitency check mechanisms

            # check if current student are in second semester

            ongoing_first_year_semester = active_batches.objects.filter(
                department=dept_obj
            ).filter(
                programme='PG'
            ).get(
                current_semester_number_of_this_batch__lte=2
            ).current_semester_number_of_this_batch

            conflicting_sem_to_check = ongoing_first_year_semester + 2

            no_of_second_year_students = student.objects.filter(
                department=dept_obj
            ).filter(
                qualification='PG'
            ).filter(
                current_semester=conflicting_sem_to_check
            ).count()

            active_second_year_batch = active_batches.objects.filter(
                department=dept_obj
            ).filter(
                programme='PG'
            ).filter(
                current_semester_number_of_this_batch=conflicting_sem_to_check
            ).exists()

            active_second_year_semesters = semester_migration.objects.filter(
                department=dept_obj
            ).filter(
                semester_number=conflicting_sem_to_check
            ).exists()

            if active_second_year_batch or active_second_year_semesters:
                msg = '<p>Please migrate 2nd Year students first</p>'
                data['msg'] = msg
                data['code'] = 2

                return JsonResponse(data)

            active_second_year_batch = active_batches.objects.filter(
                department=dept_obj
            ).filter(
                programme='PG'
            ).filter(
                current_semester_number_of_this_batch=3
            ).exists()

            if not active_second_year_batch and ongoing_first_year_semester != 1:
                data['code'] = 3
                regulation_instances = regulation.objects.all().order_by('-start_year')
                list = []
                for each in regulation_instances:
                    temp = {}
                    temp['pk'] = each.pk
                    temp['string'] = str(each)
                    list.append(temp)
                data['reg_list'] = list
                return JsonResponse(data)

        return JsonResponse(data)

    else:
        return render(request, '403.html')
