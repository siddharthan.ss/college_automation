from django.views.generic import ListView
from django.views.generic.edit import FormMixin

from accounts.models import staff
from curriculum.forms import course_filter_form
from curriculum.models import courses


class CourseListView(ListView, FormMixin):
    model = courses
    form_class = course_filter_form
    template_name = 'curriculum/courseList.html'
    context_object_name = 'courses'
    programme = 'UG'

    def get(self, request, *args, **kwargs):
        self.object = None
        self.form = self.get_form(self.form_class)
        print(self.form)
        if self.form.is_valid():
            print(self.form_class.cleaned_data['programme'])
            self.programme = self.form_class.clean_data['programme']
        # Explicitly states what get to call:
        # return self.get_queryset()
        return ListView.get(self, request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        # Just include the form
        context = super(CourseListView, self).get_context_data(*args, **kwargs)
        context['form'] = course_filter_form()
        return context

    def get_queryset(self):
        staff_instance = staff.objects.get(user=self.request.user)
        department_instance = getattr(staff_instance, 'department')
        return courses.objects.filter(department=department_instance).filter(programme=self.programme)
