from datetime import datetime
from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render
from django.utils import timezone

from accounts.models import staff, department
from curriculum.models import semester, semester_migration, staff_course, time_table
from curriculum.views import attendance_report
from curriculum.views import form_weeks
from curriculum.views import get_active_semesters_tabs_of_department
from curriculum.views import get_active_semesters_tabs_for_overall_reports
from curriculum.views import get_course_allotments_for_overall_report
from curriculum.views import overall_attendance_report


@login_required
@permission_required('curriculum.can_view_overall_reports')
def consolidated_overall_attendance_report(request, semester_pk=None, selected_allotment_pk=False, data_for_pdf=False):
    staff_instance = staff.objects.get(user=request.user)

    all_departments = department.objects.filter(is_core=True)
    user_is_pricipal_or_coe = False

    selected_department_pk = None
    if request.method == "POST" and 'selected_department_pk' in request.POST:
        selected_department_pk = int(request.POST.get('selected_department_pk'))
    if request.user.groups.filter(name='principal').exists() or request.user.groups.filter(name='coe_staff').exists():
        if not selected_department_pk:
            selected_department = all_departments.first()
            selected_department_pk = selected_department.pk
        else:
            selected_department = department.objects.get(pk=selected_department_pk)
        avaliable_semesters = get_active_semesters_tabs_of_department(selected_department)
        user_is_pricipal_or_coe = True
    else:
        selected_department = staff_instance.department
        selected_department_pk = selected_department.pk
        avaliable_semesters = get_active_semesters_tabs_for_overall_reports(request)

    if not avaliable_semesters:
        modal = {
            'heading': 'Error',
            'body': 'No semester plan has been created for your access',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    pprint(avaliable_semesters)

    if request.user.groups.filter(name='faculty_advisor').exists() and not \
            request.user.groups.filter(name='hod').exists() and not user_is_pricipal_or_coe:
        new_avaliable_semesters = []
        semesters_as_fa = semester_migration.objects.filter(
            semester__faculty_advisor=staff_instance
        )
        for entry in semesters_as_fa:
            new_avaliable_semesters.append(entry.semester)

        pprint('new semesters ' + str(new_avaliable_semesters))

        final_semesters = []
        for entry in avaliable_semesters:
            print(entry)
            sem_inst = semester.objects.get(pk=entry['semester_instance'].pk)
            if sem_inst in new_avaliable_semesters:
                print((sem_inst))
                final_semesters.append(entry)

        pprint('final batches = ' + str(final_semesters))
        if final_semesters:
            avaliable_semesters = final_semesters

    selected_semester_pk = avaliable_semesters[0]['semester_instance'].pk

    if request.method == "POST" and 'semester_pk' in request.POST:
        selected_semester_pk = int(request.POST.get('semester_pk'))

    if semester_pk:
        selected_semester_pk = int(semester_pk)

    print(selected_semester_pk)

    selected_semester = semester.objects.get(pk=selected_semester_pk)

    alloted_course_instances_of_selected_semester = staff_course.objects.filter(
        semester=selected_semester
    )

    course_allotment_list = get_course_allotments_for_overall_report(alloted_course_instances_of_selected_semester)

    if not selected_allotment_pk:
        try:
            selected_allotment_pk = course_allotment_list[0]['staff_course_instance'].pk
        except:
            msg = {
                'page_title': 'No courses',
                'title': 'No courses alloted',
                'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    print(selected_allotment_pk)

    if request.method == 'POST' and request.POST.get('selected_allotment_pk'):
        selected_allotment_pk = request.POST.get('selected_allotment_pk')
    # print('selected_course = ' + selected_course)
    selected_allotment_instance = staff_course.objects.get(pk=selected_allotment_pk)

    course_instance = selected_allotment_instance.course
    timetable_instances = time_table.objects.filter(course=course_instance)

    pprint(timetable_instances)

    if not timetable_instances:
        msg = {
            'page_title': 'Allotment Error',
            'title': 'Timetable not Found',
            'description': 'No timetable has been assigned to this course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    semester_plan_instance = selected_allotment_instance.semester

    start_date = getattr(semester_plan_instance, 'start_term_1')
    if start_date is None:
        msg = {
            'page_title': 'Date Error',
            'title': 'Start Date Not Found',
            'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    end_date = getattr(semester_plan_instance, 'end_term_3')
    if end_date is None:
        end_date = timezone.now().date()

    result_dict = overall_attendance_report(start_date, end_date, course_instance, semester_plan_instance)

    print('start_date = ' + str(start_date))

    return render(request, 'overall_reports/consolidated_overall_attendance_report.html',
                  {
                      'all_departments': all_departments,
                      'selected_department_pk': selected_department_pk,
                      'semesters': avaliable_semesters,
                      'selected_semester_pk': selected_semester_pk,
                      'course_allotment_list': course_allotment_list,
                      'final_result': result_dict, 'selected_allotment_pk': int(selected_allotment_pk),
                      'current_start_date': start_date, 'current_end_date': end_date,
                      'selected_course_name':selected_allotment_instance.course,
                  })


@login_required
@permission_required('curriculum.can_view_overall_reports')
def weekly_overall_attendance_report(request, semester_pk=None, selected_allotment_pk=False, start_date=None,
                                     end_date=None, data_for_pdf=False):
    staff_instance = staff.objects.get(user=request.user)

    all_departments = department.objects.filter(is_core=True)
    user_is_pricipal_or_coe = False

    selected_department_pk = None
    if request.method == "POST" and 'selected_department_pk' in request.POST:
        selected_department_pk = int(request.POST.get('selected_department_pk'))
    if request.user.groups.filter(name='principal').exists() or request.user.groups.filter(name='coe_staff').exists():
        if not selected_department_pk:
            selected_department = all_departments.first()
            selected_department_pk = selected_department.pk
        else:
            selected_department = department.objects.get(pk=selected_department_pk)
        avaliable_semesters = get_active_semesters_tabs_of_department(selected_department)
        user_is_pricipal_or_coe = True
    else:
        selected_department = staff_instance.department
        selected_department_pk = selected_department.pk
        avaliable_semesters = get_active_semesters_tabs_for_overall_reports(request)

    if not avaliable_semesters:
        modal = {
            'heading': 'Error',
            'body': 'No semester plan has been created for your access',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    pprint(avaliable_semesters)

    if request.user.groups.filter(name='faculty_advisor').exists() and not \
            request.user.groups.filter(name='hod').exists() and not user_is_pricipal_or_coe:
        new_avaliable_semesters = []
        semesters_as_fa = semester_migration.objects.filter(
            semester__faculty_advisor=staff_instance
        )
        for entry in semesters_as_fa:
            new_avaliable_semesters.append(entry.semester)

        pprint('new semesters ' + str(new_avaliable_semesters))

        final_semesters = []
        for entry in avaliable_semesters:
            print(entry)
            sem_inst = semester.objects.get(pk=entry['semester_instance'].pk)
            if sem_inst in new_avaliable_semesters:
                print((sem_inst))
                final_semesters.append(entry)

        pprint('final batches = ' + str(final_semesters))
        if final_semesters:
            avaliable_semesters = final_semesters

    selected_semester_pk = avaliable_semesters[0]['semester_instance'].pk

    if request.method == "POST" and 'semester_pk' in request.POST:
        selected_semester_pk = int(request.POST.get('semester_pk'))

    if semester_pk:
        selected_semester_pk = int(semester_pk)

    print(selected_semester_pk)

    selected_semester = semester.objects.get(pk=selected_semester_pk)

    alloted_course_instances_of_selected_semester = staff_course.objects.filter(
        semester=selected_semester
    )

    course_allotment_list = get_course_allotments_for_overall_report(alloted_course_instances_of_selected_semester)

    if not selected_allotment_pk:
        try:
            selected_allotment_pk = course_allotment_list[0]['staff_course_instance'].pk
        except:
            msg = {
                'page_title': 'No courses',
                'title': 'No courses alloted',
                'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    print(selected_allotment_pk)

    if request.method == 'POST' and request.POST.get('selected_allotment_pk'):
        selected_allotment_pk = request.POST.get('selected_allotment_pk')
    # print('selected_course = ' + selected_course)
    selected_allotment_instance = staff_course.objects.get(pk=selected_allotment_pk)

    course_instance = selected_allotment_instance.course
    timetable_instances = time_table.objects.filter(course=course_instance)

    pprint(timetable_instances)

    if not timetable_instances:
        msg = {
            'page_title': 'Allotment Error',
            'title': 'Timetable not Found',
            'description': 'No timetable has been assigned to this course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    semester_plan_instance = selected_allotment_instance.semester

    weeks_so_far = form_weeks(selected_allotment_instance.semester, selected_allotment_instance.course)
    if weeks_so_far is None:
        msg = {
            'page_title': 'Date Error',
            'title': 'Start Date Not Found',
            'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if start_date is None and end_date is None:
        start_date = weeks_so_far[-1]['start_day']
        end_date = weeks_so_far[-1]['end_day']
    if request.method == 'POST' and request.POST.get('date_range'):
        dates = [value for value in request.POST.get('date_range').split('_')]
        start_date = datetime.strptime(dates[0], "%Y-%m-%d").date()
        end_date = datetime.strptime(dates[1], "%Y-%m-%d").date()
        pprint(dates)
        print((request.POST.get('date_range')))
        # Wpass

    result_dict = attendance_report(start_date, end_date, selected_allotment_instance.course,
                                    selected_allotment_instance.semester)

    print('start_date = ' + str(start_date))
    print(selected_allotment_instance.semester)

    return render(request, 'overall_reports/weekly_overall_attendance_report.html',
                  {
                      'all_departments': all_departments,
                      'selected_department_pk': selected_department_pk,
                      'semesters': avaliable_semesters,
                      'selected_semester_pk': selected_semester_pk,
                      'course_allotment_list': course_allotment_list,
                      'selected_allotment_pk': int(selected_allotment_pk),
                      'selected_course': (selected_allotment_instance.course.pk),
                      'current_start_date': start_date,
                      'current_end_date': end_date,
                      'date_and_hour_list': result_dict['date_and_hour_list'],
                      'final_result': result_dict['processed_result'],
                      'weeks': weeks_so_far,
                      'selected_course_name' : selected_allotment_instance.course
                  })
