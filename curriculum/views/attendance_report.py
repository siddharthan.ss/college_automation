from datetime import timedelta

from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.shortcuts import render
from django.utils import timezone

from accounts.views.profile import studentProfile
from curriculum.models import attendance, semester
from curriculum.views.common_includes import *


def get_recent_monday_date():
    current_weekday_number = timezone.datetime.now().weekday()
    recent_monday = timezone.datetime.now().date() - timedelta(days=current_weekday_number)
    print('current weekday number = ' + str(current_weekday_number))
    print('recent monday = ' + str(recent_monday))

    return recent_monday


def form_weekly_report_result(student_list, date_and_hour_list, individual_attendance_list):
    processed_data_list = []
    for student_entry in student_list:
        temp_dictionary = {}
        temp_dictionary['roll_no'] = student_entry['registration_number']
        temp_dictionary['name'] = student_entry['student_name']

        present_hours = 0
        absent_hours = 0
        present_list = []

        for attendance_entry in individual_attendance_list:
            for date_and_hour_entry in date_and_hour_list:
                if attendance_entry['date'] == date_and_hour_entry['attendance_date']:
                    if attendance_entry['hour'] == date_and_hour_entry['hour']:
                        if attendance_entry['registration_number'] == student_entry['registration_number']:
                            if attendance_entry['present'] == True:
                                present_list.append(True)
                                present_hours += 1
                            elif attendance_entry['absent'] == True:
                                present_list.append(False)
                                absent_hours += 1
        temp_dictionary['present'] = present_list
        temp_dictionary['total_hours'] = present_hours + absent_hours
        temp_dictionary['present_hours'] = present_hours
        temp_dictionary['absent_hours'] = absent_hours
        try:
            temp_dictionary['percentage'] = format((present_hours / temp_dictionary['total_hours']) * 100, '.2f')
        except:
            temp_dictionary['percentage'] = 0
        processed_data_list.append(temp_dictionary)

    print('\nProcessed data list')
    pprint(processed_data_list)
    return processed_data_list


"""
def attendance_report(start_date, end_date,course_instance):
    result_dict = {}

    timetable_instances = time_table.objects.filter(course=course_instance)

    date_and_hour_list = []
    list_of_students = []
    indvidual_attendance_list = []
    total_hours_this_week = 0

    for timetable_entry in timetable_instances:
        batch_instance = getattr(timetable_entry, 'batch')
        current_semeseter_number = batch_instance.get_current_semester_of_this_batch()
        print('current sem of this batch = ' + str(current_semeseter_number))
        list_of_students = get_list_of_students(course_instance, current_semeseter_number)
        break

    for timetable_entry in timetable_instances:

        timetable_day = getattr(timetable_entry, 'day')
        print('timetable_day = ' + str(timetable_day))
        weekday_of_timetable_day = get_weekday_of_day(timetable_day)
        print(weekday_of_timetable_day)
        timetable_hour = getattr(timetable_entry, 'hour')
        print('timetable_hour ' + str(timetable_hour))
        try:
            attendance_instance = attendance.objects.filter(
                course=course_instance).filter(date__gte=start_date).filter(date__lte=end_date).filter(date__week_day=get_weekday_of_day(timetable_day)).filter(
                hour=timetable_hour)
        except attendance.DoesNotExist:
            print('no attendance entry for ' + str(timetable_day) + str(timetable_hour))
            attendance_instance = None

        if attendance_instance:
            for entry in attendance_instance:
                temp = {}
                date = getattr(entry, 'date')
                this_hour = getattr(entry, 'hour')
                present_list = entry.present_students.all()
                absent_list = entry.absent_students.all()
                print('present list = ' + str(present_list))
                print('absent list = ' + str(absent_list))

                for student_entry in list_of_students:
                    temp_dict = {}
                    student_instance = student.objects.get(roll_no=student_entry['registration_number'])
                    temp_dict['registration_number'] = student_entry['registration_number']
                    temp_dict['date'] = date
                    temp_dict['hour'] = this_hour
                    if student_instance in present_list:
                        temp_dict['present'] = True
                        temp_dict['absent'] = False
                    else:
                        temp_dict['present'] = False
                        temp_dict['absent'] = True
                    indvidual_attendance_list.append(temp_dict)

                print('student_list after adding present = ' + str(list_of_students))

                temp['attendance_date'] = date
                temp['hour'] = this_hour
                total_hours_this_week += 1
                date_and_hour_list.append(temp)

    pprint(date_and_hour_list)
    pprint(list_of_students)
    pprint(indvidual_attendance_list)
    processed_result = form_weekly_report_result(list_of_students, date_and_hour_list, indvidual_attendance_list)

    result_dict['processed_result'] = processed_result
    result_dict['date_and_hour_list'] = date_and_hour_list

    return result_dict
"""


def overall_attendance_report(start_date, end_date, course_instance, semester_inst):
    attendance_entries = attendance.objects.filter(
        course=course_instance
    ).filter(
        semester=semester_inst
    ).filter(
        date__gte=start_date
    ).filter(
        date__lte=end_date
    ).order_by(
        'date'
    )

    student_instances = get_list_of_students(semester_inst, course_instance)

    processed_result = []
    for stud in student_instances:
        entry = stud['student_obj']
        temp = {}
        temp['roll_no'] = entry.roll_no
        temp['name'] = str(entry)

        present_hours_count = attendance_entries.filter(present_students=entry).count()
        onduty_hours_count = attendance_entries.filter(onduty_students=entry).count()
        absent_hours_count = attendance_entries.filter(absent_students=entry).count()
        temp['total_hours'] = present_hours_count + onduty_hours_count + absent_hours_count
        if temp['total_hours'] == 0:
            continue
        temp['present_hours'] = int(present_hours_count + onduty_hours_count)
        temp['absent_hours'] = absent_hours_count
        try:
            temp['percentage'] = format((temp['present_hours'] / temp['total_hours']) * 100, '.2f')
        except ZeroDivisionError:
            temp['percentage'] = 0
        processed_result.append(temp)

    return processed_result


def attendance_report(start_date, end_date, course_instance, semester_inst):
    # prepare date and hour list
    date_and_hour_list = []
    attendance_instances = attendance.objects.filter(
        semester=semester_inst
    ).filter(
        course=course_instance
    ).filter(
        date__gte=start_date
    ).filter(
        date__lte=end_date
    ).order_by(
        'date'
    )

    for entry in attendance_instances:
        temp = {}
        temp['attendance_date'] = entry.date
        temp['hour'] = entry.hour
        date_and_hour_list.append(temp)

    # get the list of students
    # handle is_open,is_elecitive and is_one_credit

    course_type = course_instance.is_elective | course_instance.is_open | course_instance.is_one_credit

    student_instances = get_list_of_students(semester_inst, course_instance)

    processed_result = []

    range_queryset = studentProfile.get_attendance_queryset_for_given_range(semester_inst,course_instance,start_date, end_date)
    for stud in student_instances:
        studentProfileInstance = studentProfile()
        entry = stud['student_obj']
        temp = {}
        temp['roll_no'] = entry.roll_no
        temp['name'] = str(entry)
        individual_attendance_list = studentProfileInstance.get_list_of_attendance_entries_for_student_in_range(range_queryset,entry)
        temp['attendance_list'] = individual_attendance_list
        temp['total_hours'] = studentProfileInstance.get_total_working_hours()
        if temp['total_hours'] == 0:
            continue
        temp['present_hours'] = studentProfileInstance.get_present_hours() + studentProfileInstance.get_onduty_hours()
        temp['absent_hours'] = studentProfileInstance.get_absent_hours()
        temp['percentage'] = studentProfileInstance.get_total_present_percentage()
        studentProfileInstance = None
        processed_result.append(temp)

    result_dict = {}
    result_dict['processed_result'] = processed_result
    result_dict['date_and_hour_list'] = date_and_hour_list

    pprint(result_dict)

    return result_dict


# def get_next_friday(date_instance):
#     current_weekday_number = date_instance.weekday()
#     no_of_days_to_next_friday = 3 - current_weekday_number
#     next_friday = date_instance + timedelta(days=no_of_days_to_next_friday)
#
#     if next_friday.weekday() != 3:
#         raise ValueError(str(next_friday) + 'is not friday')
#
#     print('current_day = ' + str(date_instance))
#     print('next_friday = ' + str(next_friday))
#
#     return next_friday

def get_next_saturday(date_instance):
    current_weekday_number = date_instance.weekday()
    no_of_days_to_next_friday = 5 - current_weekday_number
    next_friday = date_instance + timedelta(days=no_of_days_to_next_friday)

    if next_friday.weekday() != 5:
        raise ValueError(str(next_friday) + 'is not Saturday')

    print('current_day = ' + str(date_instance))
    print('next_saturday = ' + str(next_friday))

    return next_friday

def get_next_monday(date_instance):
    if date_instance.weekday() == 4:
        next_monday = date_instance + timedelta(days=3)

        if next_monday.weekday() != 0:
            raise ValueError(str(next_monday) + 'is not monday')

        print('current_day = ' + str(date_instance))
        print('next_monday = ' + str(next_monday))

        return next_monday
    else:
        raise ValueError(str(date_instance) + 'is not friday')


def form_weeks(semester_plan_instance, course_instance):
    result_list = []

    start_date_of_sem = semester_plan_instance.start_term_1
    if not start_date_of_sem:
        return None

    pprint('start of sem data : ' + str(start_date_of_sem))

    ending_date = getattr(semester_plan_instance, 'end_term_3')
    if ending_date is None:
        ending_date = timezone.now().date()

    # next_friday = get_next_friday(start_date_of_sem)
    next_saturday = get_next_saturday(start_date_of_sem)
    temp = {}

    temp['start_day'] = start_date_of_sem
    temp['end_day'] = next_saturday

    result_list.append(temp)

    while next_saturday < ending_date:
        temp = {}
        next_sunday = next_saturday + timedelta(days=1)
        if next_sunday > datetime.today().date():
            break
        next_saturday = next_sunday + timedelta(days=6)
        temp['start_day'] = next_sunday
        if next_saturday < ending_date:
            temp['end_day'] = next_saturday
        else:
            temp['end_day'] = ending_date
        result_list.append(temp)

    pprint(result_list)

    return result_list


def form_overall_final_result(overall_result_list):
    course_and_faculty_list = []
    student_list = []

    student_dictionary = {}

    for entry in overall_result_list:
        temp = {}
        temp['course_obj'] = entry['course_obj']
        temp['faculty_obj'] = entry['faculty_obj']
        course_and_faculty_list.append(temp)

    for entry in overall_result_list:
        for student_entry in entry['attendance_result']:
            templist = []
            temp2 = {}
            temp2['name'] = student_entry['name']
            temp2['roll_no'] = student_entry['roll_no']
            templist.append(temp2)
            student_dictionary[student_entry['roll_no']] = templist
        break

    for entry in student_dictionary:
        data_list = []

        entry = data_list

    for entry in overall_result_list:
        for student_entry in entry['attendance_result']:
            if len(student_entry) < 0:
                data_list.append(0)
                data_list.append(0)
                data_list.append(0)
                data_list.append(0)
            else:
                data_list.append(student_entry['total_hours'])
                data_list.append(student_entry['present_hours'])
                data_list.append(student_entry['absent_hours'])
                data_list.append(student_entry['percentage'])
            student_dictionary[student_entry['roll_no']][0]['list'] = data_list

    for entry in overall_result_list:
        for student_entry in entry['attendance_result']:
            student_list.append(student_dictionary[student_entry['roll_no']][0])
        break

    # for entry in overall_result_list:
    #     temp = {}
    #     temp['course_obj'] = entry['course_obj']
    #     temp['faculty_obj'] = entry['faculty_obj']
    #     course_and_faculty_list.append(temp)
    #
    #     for student_entry in entry['attendance_result']:
    #         flag = True
    #         for final_entries in student_list:
    #             if final_entries['name'] == student_entry['name']:
    #                 final_entries['list'].append(student_entry['total_hours'])
    #                 final_entries['list'].append(student_entry['present_hours'])
    #                 final_entries['list'].append(student_entry['absent_hours'])
    #                 final_entries['list'].append(student_entry['percentage'])
    #                 flag = False
    #         if flag:
    #             temp2 = {}
    #             temp2['name'] = student_entry['name']
    #             temp2['roll_no'] = student_entry['roll_no']
    #             templist = []
    #             templist.append(student_entry['total_hours'])
    #             templist.append(student_entry['present_hours'])
    #             templist.append(student_entry['absent_hours'])
    #             templist.append(student_entry['percentage'])
    #             temp2['list'] = templist
    #             student_list.append(temp2)
    pprint(course_and_faculty_list)

    pprint(student_list)

    result_dictionary = {
        'courses_and_faculty_list': course_and_faculty_list,
        'student_list': student_list,
    }

    return result_dictionary


def can_view_attendance_marks(user):
    if user.has_perm('curriculum.can_mark_attendance') or \
            user.has_perm('curriculum.can_view_overall_reports'):
        return True


@login_required
@user_passes_test(can_view_attendance_marks)
def weekly_attendance_report(request, selected_allotment_pk=False, start_date=None, end_date=None, data_for_pdf=False):
    faculty_instance = staff.objects.get(user=request.user)
    course_allotment_list = get_current_course_allotment_with_display_text(faculty_instance)
    if not selected_allotment_pk:
        try:
            selected_allotment_pk = course_allotment_list[0]['staff_course_instance'].pk
        except:
            msg = {
                'page_title': 'No courses',
                'title': 'No courses alloted',
                'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    print(selected_allotment_pk)

    if request.method == 'POST':
        selected_allotment_pk = request.POST.get('selected_allotment_pk')
    # print('selected_course = ' + selected_course)
    selected_allotment_instance = staff_course.objects.get(pk=selected_allotment_pk)
    weeks_so_far = form_weeks(selected_allotment_instance.semester, selected_allotment_instance.course)
    if weeks_so_far is None:
        msg = {
            'page_title': 'Date Error',
            'title': 'Start Date Not Found',
            'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if start_date is None and end_date is None:
        start_date = weeks_so_far[-1]['start_day']
        end_date = weeks_so_far[-1]['end_day']
    if request.method == 'POST' and request.POST.get('date_range'):
        dates = [value for value in request.POST.get('date_range').split('_')]
        start_date = datetime.strptime(dates[0], "%Y-%m-%d").date()
        end_date = datetime.strptime(dates[1], "%Y-%m-%d").date()
        pprint(dates)
        print((request.POST.get('date_range')))
        # Wpass

    result_dict = attendance_report(start_date, end_date, selected_allotment_instance.course,
                                    selected_allotment_instance.semester)

    print('start_date = ' + str(start_date))
    print(selected_allotment_instance.semester)

    if data_for_pdf:
        context_data = {
            'date_and_hour_list': result_dict['date_and_hour_list'],
            'final_result': result_dict['processed_result'],
            'selected_course': selected_allotment_instance.course,
            'current_start_date': datetime.strptime(start_date, "%Y-%m-%d"),
            'current_end_date': datetime.strptime(end_date, "%Y-%m-%d"),
        }
        return context_data

    return render(request, 'curriculum/weekly_attendance_report.html',
                  {'course_allotment_list': course_allotment_list,
                   'date_and_hour_list': result_dict['date_and_hour_list'],
                   'final_result': result_dict['processed_result'],
                   'selected_allotment_pk': int(selected_allotment_pk),
                   'selected_semester_pk': int(selected_allotment_instance.semester.pk),
                   'selected_course': (selected_allotment_instance.course.pk),
                   'selected_course_name': (selected_allotment_instance.course),
                   'weeks': weeks_so_far, 'current_start_date': start_date,
                   'current_end_date': end_date,
                   })


@login_required
@user_passes_test(can_view_attendance_marks)
def consolidated_attendance_report(request, selected_allotment_pk=False, data_for_pdf=False):
    faculty_instance = staff.objects.get(user=request.user)
    course_allotment_list = get_current_course_allotment_with_display_text(faculty_instance)
    if not selected_allotment_pk:
        try:
            selected_allotment_pk = course_allotment_list[0]['staff_course_instance'].pk
        except:
            msg = {
                'page_title': 'No courses',
                'title': 'No courses alloted',
                'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    print(selected_allotment_pk)

    if request.method == 'POST':
        selected_allotment_pk = request.POST.get('selected_allotment_pk')
    # print('selected_course = ' + selected_course)
    selected_allotment_instance = staff_course.objects.get(pk=selected_allotment_pk)

    course_instance = selected_allotment_instance.course
    timetable_instances = time_table.objects.filter(course=course_instance)

    pprint(timetable_instances)

    if not timetable_instances:
        msg = {
            'page_title': 'Allotment Error',
            'title': 'Timetable not Found',
            'description': 'No timetable has been assigned to this course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    semester_plan_instance = selected_allotment_instance.semester

    start_date = getattr(semester_plan_instance, 'start_term_1')
    if start_date is None:
        msg = {
            'page_title': 'Date Error',
            'title': 'Start Date Not Found',
            'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    end_date = getattr(semester_plan_instance, 'end_term_3')
    if end_date is None:
        end_date = timezone.now().date()

    result_dict = overall_attendance_report(start_date, end_date, course_instance, semester_plan_instance)

    print('start_date = ' + str(start_date))

    if data_for_pdf:
        context_data = {
            'final_result': result_dict,
            'selected_course': course_instance,
            'current_start_date': start_date, 'current_end_date': end_date,
        }
        return context_data

    return render(request, 'curriculum/consolidated_attendance_report.html',
                  {'course_allotment_list': course_allotment_list,
                   'final_result': result_dict, 'selected_allotment_pk': int(selected_allotment_pk),
                   'current_start_date': start_date, 'current_end_date': end_date,
                   'selected_course_name':selected_allotment_instance.course
                   })


def group_consolidated_for_conduct(processed_list, programme):
    group_1 = []
    group_2 = []
    group_3 = []

    print(programme)

    if programme == 'PG':
        for entry in processed_list:
            if float(entry['percentage']) >= 80:
                group_1.append(entry)
            elif float(entry['percentage']) >= 70:
                group_2.append(entry)
            else:
                group_3.append(entry)
        group_1_description = '(Attendance 80% and above)'
        group_2_description = '(Attendance 70% and above but less than 80%)'
        group_3_description = '(Attendance less than 70%)'

    elif programme == 'UG':
        for entry in processed_list:
            if float(entry['percentage']) >= 75:
                group_1.append(entry)
            elif float(entry['percentage']) >= 65:
                group_2.append(entry)
            else:
                group_3.append(entry)
        group_1_description = '(Attendance 75% and above)'
        group_2_description = '(Attendance 65% and above but less than 75%)'
        group_3_description = '(Attendance less than 65%)'

    temp = {}
    temp['group_1'] = group_1
    temp['group_1_description'] = group_1_description
    temp['group_2'] = group_2
    temp['group_2_description'] = group_2_description
    temp['group_3'] = group_3
    temp['group_3_description'] = group_3_description

    return temp


@login_required
@permission_required('curriculum.can_mark_attendance')
def consolidated_attendance_conduct_report(request, selected_allotment_pk=False, data_for_pdf=False):
    faculty_instance = staff.objects.get(user=request.user)
    course_allotment_list = get_current_course_allotment_with_display_text(faculty_instance)

    temp_course_allotment_list = []
    for course_allotment in course_allotment_list:
        if course_allotment['staff_course_instance'].course.regulation.start_year >= 2016:
            temp_course_allotment_list.append(course_allotment)
    course_allotment_list = temp_course_allotment_list

    if not selected_allotment_pk:
        try:
            selected_allotment_pk = course_allotment_list[0]['staff_course_instance'].pk
        except:
            msg = {
                'page_title': 'No courses',
                'title': 'No courses alloted',
                'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    print(selected_allotment_pk)

    if request.method == 'POST':
        selected_allotment_pk = request.POST.get('selected_allotment_pk')
    # print('selected_course = ' + selected_course)
    selected_allotment_instance = staff_course.objects.filter(course__regulation__start_year=2016).get(
        pk=selected_allotment_pk)

    course_instance = selected_allotment_instance.course

    timetable_instances = time_table.objects.filter(course=course_instance)

    pprint(timetable_instances)

    if not timetable_instances:
        msg = {
            'page_title': 'Allotment Error',
            'title': 'Timetable not Found',
            'description': 'No timetable has been assigned to this course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    semester_plan_instance = selected_allotment_instance.semester

    start_date = getattr(semester_plan_instance, 'start_term_1')
    if start_date is None:
        msg = {
            'page_title': 'Date Error',
            'title': 'Start Date Not Found',
            'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    end_date = getattr(semester_plan_instance, 'end_term_3')
    if end_date is None:
        end_date = timezone.now().date()

    result_dict = overall_attendance_report(start_date, end_date, course_instance, semester_plan_instance)

    print('start_date = ' + str(start_date))

    print('processed result')
    print(result_dict)

    if course_instance.programme == "PG":
        grouped_list = group_consolidated_for_conduct(result_dict, "PG")
    else:
        grouped_list = group_consolidated_for_conduct(result_dict, "UG")

    if end_date.month > 6:
        semester_string = 'Nov/Dec ' + str(end_date.year)
    else:
        semester_string = 'Apr/May ' + str(end_date.year)
    if data_for_pdf:
        context_data = {
            'final_result': grouped_list,
            'selected_course': course_instance,
            'current_start_date': start_date, 'current_end_date': end_date,
            'semester_string': semester_string,
        }
        return context_data

    return render(request, 'curriculum/attendance_conduct.html',
                  {'course_allotment_list': course_allotment_list,
                   'final_result': grouped_list, 'selected_allotment_pk': int(selected_allotment_pk),
                   'current_start_date': start_date, 'current_end_date': end_date,
                   })


@login_required
@permission_required('curriculum.can_mark_attendance')
def customAttendanceReport(request, selected_allotment_pk=False, st_date=None, ed_date=None, data_for_pdf=False):
    faculty_instance = staff.objects.get(user=request.user)
    course_allotment_list = get_current_course_allotment_with_display_text(faculty_instance)
    if not selected_allotment_pk:
        try:
            selected_allotment_pk = course_allotment_list[0]['staff_course_instance'].pk
        except:
            msg = {
                'page_title': 'No courses',
                'title': 'No courses alloted',
                'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    print(selected_allotment_pk)

    # print('selected_course = ' + selected_course)
    selected_allotment_instance = staff_course.objects.get(pk=selected_allotment_pk)
    # selected_course = selected_allotment_instance.course

    if request.method == 'POST' or data_for_pdf:
        if request.method == 'POST':
            selected_allotment_pk = request.POST.get('selected_allotment_pk')
            selected_allotment_instance = staff_course.objects.get(pk=selected_allotment_pk)
            if not request.POST.get('start_date') and not request.POST.get('end_date'):
                return render(request, 'curriculum/custom_attendance_report.html',
                              {'course_allotment_list': course_allotment_list,
                               'selected_allotment_pk': int(selected_allotment_pk),
                               })
            start_date = datetime.strptime(request.POST.get('start_date'), "%Y-%m-%d").date()
            end_date = datetime.strptime(request.POST.get('end_date'), "%Y-%m-%d").date()

        if st_date:
            start_date = datetime.strptime(st_date, "%Y-%m-%d").date()
        if ed_date:
            end_date = datetime.strptime(ed_date, "%Y-%m-%d").date()

        print('selected_course = ' + str(selected_allotment_instance.course))
        course_instance = selected_allotment_instance.course

        semester_instance = selected_allotment_instance.semester

        if semester_instance.start_term_1 is None:
            msg = {
                'page_title': 'Date Error',
                'title': 'Start Date Not Found',
                'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

        if semester_instance.start_term_1 > start_date:
            print('start date error')
            modal = {
                'heading': 'Date Error',
                'body': 'The start date seems to be out of range for this subject',
            }
            return render(request, 'curriculum/custom_attendance_report.html', {
                'course_allotment_list': course_allotment_list, 'selected_allotment_pk': int(selected_allotment_pk),
                'toggle_model': 'true', 'modal': modal})

        if semester_instance.end_term_3 and semester_instance.end_term_3 < end_date or (
                    datetime.today().date() < end_date):
            print('end date error')
            modal = {
                'heading': 'Date Error',
                'body': 'The end date seems to be out of range for this subject',
            }
            return render(request, 'curriculum/custom_attendance_report.html', {
                'course_allotment_list': course_allotment_list, 'selected_allotment_pk': int(selected_allotment_pk),
                'toggle_model': 'true', 'modal': modal})

        result_dict = overall_attendance_report(start_date, end_date, course_instance, semester_instance)

        if data_for_pdf:
            context_data = {
                'final_result': result_dict,
                'selected_course': course_instance,
                'current_start_date': start_date, 'current_end_date': end_date,
            }
            return context_data

        return render(request, 'curriculum/custom_attendance_report.html',
                      {'course_allotment_list': course_allotment_list,
                       'selected_allotment_pk': int(selected_allotment_pk),
                       'final_result': result_dict,
                       'current_start_date': start_date, 'current_end_date': end_date,
                       'selected_course_name': selected_allotment_instance.course,
                       })

    return render(request, 'curriculum/custom_attendance_report.html',
                  {
                      'course_allotment_list': course_allotment_list,
                      'selected_allotment_pk': int(selected_allotment_pk),
                      'selected_course_name': selected_allotment_instance.course,
                   })


from easy_pdf.views import PDFTemplateView


class weeklyPdfReport(PDFTemplateView):
    template_name = "curriculum/weekly_pdf_report.html"
    selected_allotment_pk = None
    start_date = None
    end_date = None

    def get(self, request, *args, **kwargs):
        self.selected_allotment_pk = kwargs.pop('allot_staff_pk')
        self.start_date = kwargs.pop('start_date')
        self.end_date = kwargs.pop('end_date')
        return super(weeklyPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = weekly_attendance_report(self.request,
                                                selected_allotment_pk=self.selected_allotment_pk,
                                                start_date=self.start_date,
                                                end_date=self.end_date,
                                                data_for_pdf=True
                                                )
        print(context_data)
        return super(weeklyPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )


class consolidatedPdfReport(PDFTemplateView):
    template_name = "curriculum/consolidated_pdf_report.html"
    selected_allotment_pk = None

    def get(self, request, *args, **kwargs):
        self.selected_allotment_pk = kwargs.pop('allot_staff_pk')
        print(self.selected_allotment_pk)
        return super(consolidatedPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = consolidated_attendance_report(self.request,
                                                      selected_allotment_pk=self.selected_allotment_pk,
                                                      data_for_pdf=True
                                                      )
        print(context_data)
        return super(consolidatedPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )


class consolidatedConductPdfReport(PDFTemplateView):
    template_name = "curriculum/attendance_conduct_pdf.html"
    selected_allotment_pk = None

    def get(self, request, *args, **kwargs):
        self.selected_allotment_pk = kwargs.pop('allot_staff_pk')
        print(self.selected_allotment_pk)
        return super(consolidatedConductPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = consolidated_attendance_conduct_report(self.request,
                                                              selected_allotment_pk=self.selected_allotment_pk,
                                                              data_for_pdf=True
                                                              )
        print(context_data)
        return super(consolidatedConductPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )


class customPdfReport(PDFTemplateView):
    template_name = "curriculum/custom_pdf_report.html"
    selected_allotment_pk = None
    start_date = None
    end_date = None

    def get(self, request, *args, **kwargs):
        self.selected_allotment_pk = kwargs.pop('allot_staff_pk')
        self.start_date = kwargs.pop('start_date')
        self.end_date = kwargs.pop('end_date')
        print(self.selected_allotment_pk)
        return super(customPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = customAttendanceReport(self.request,
                                              selected_allotment_pk=self.selected_allotment_pk,
                                              st_date=self.start_date,
                                              ed_date=self.end_date,
                                              data_for_pdf=True
                                              )
        print(context_data)
        return super(customPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )


@login_required
@permission_required('curriculum.can_mark_attendance')
def overall_consolidated_attendance_report(request, semester_pk=None, data_for_pdf=False):
    staff_instance = staff.objects.get(user=request.user)

    avaliable_semesters = get_active_semesters_tabs_for_overall_reports(request)
    if not avaliable_semesters:
        modal = {
            'heading': 'Error',
            'body': 'No semester plan has been created for your access',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    pprint(avaliable_semesters)

    if request.user.groups.filter(name='faculty_advisor').exists() and not \
            request.user.groups.filter(name='hod').exists():
        new_avaliable_semesters = []
        semesters_as_fa = semester_migration.objects.filter(
            semester__faculty_advisor=staff_instance
        )
        for entry in semesters_as_fa:
            new_avaliable_semesters.append(entry.semester)

        pprint('new semesters ' + str(new_avaliable_semesters))

        final_semesters = []
        for entry in avaliable_semesters:
            print(entry)
            sem_inst = semester.objects.get(pk=entry['semester_instance'].pk)
            if sem_inst in new_avaliable_semesters:
                print((sem_inst))
                final_semesters.append(entry)

        pprint('final batches = ' + str(final_semesters))
        if final_semesters:
            avaliable_semesters = final_semesters

    selected_semester_pk = avaliable_semesters[0]['semester_instance'].pk

    if request.method == "POST":
        selected_semester_pk = int(request.POST.get('semester_pk'))

    if semester_pk:
        selected_semester_pk = int(semester_pk)

    print(selected_semester_pk)

    selected_semester = semester.objects.get(pk=selected_semester_pk)

    alloted_course_instances_of_selected_semester = staff_course.objects.filter(
        semester=selected_semester
    )

    course_instances_of_selected_semester = []
    for entry in alloted_course_instances_of_selected_semester:
        course_instances_of_selected_semester.append(entry.course)

    course_instances_of_selected_semester.sort(key=lambda x: x.course_id[-1])

    pprint('courese list ' + str(course_instances_of_selected_semester))

    fetched_result_list = []

    start_date = None
    end_date = None

    for course_instance in course_instances_of_selected_semester:
        temp = {}
        staff_course_instance = staff_course.objects.filter(semester=selected_semester).filter(course=course_instance)
        for entry in staff_course_instance:
            faculty_instance = entry.staffs.all()

            semester_plan_instance = selected_semester

            start_date = getattr(semester_plan_instance, 'start_term_1')
            if start_date is None:
                msg = {
                    'page_title': 'Date Error',
                    'title': 'Start Date Not Found',
                    'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

            end_date = getattr(semester_plan_instance, 'end_term_3')
            if end_date is None:
                end_date = timezone.now().date()
            temp['course_obj'] = course_instance
            temp['faculty_obj'] = faculty_instance
            temp['attendance_result'] = overall_attendance_report(
                course_instance=course_instance,
                start_date=start_date,
                end_date=end_date,
                semester_inst=semester_plan_instance,
            )
            fetched_result_list.append(temp)

    print('fetched result list')
    pprint(fetched_result_list)

    ovreall_result_list = form_overall_final_result(fetched_result_list)

    pprint(ovreall_result_list)

    length = len(ovreall_result_list['courses_and_faculty_list'])

    percentage_width = rest_width = 0
    if length > 0:
        percentage_width = 25 / len(ovreall_result_list['courses_and_faculty_list'])
        rest_width = 17 / len(ovreall_result_list['courses_and_faculty_list'])

    if data_for_pdf:
        context_data = {
            'title_text': 'OVERALL CONSOLIDATED ATTENDANCE REPORT',
            'percentage_width': percentage_width,
            'rest_width': rest_width,
            'selected_batch_text': get_dispaly_text_for_batch_id(selected_semester.batch.pk,
                                                                 selected_semester.department),
            'overall_result': ovreall_result_list,
            'current_start_date': start_date, 'current_end_date': end_date,
        }
        return context_data

    return render(request, 'curriculum/overall_consolidated_attendance_report.html', {
        'semesters': avaliable_semesters,
        'selected_semester_pk': selected_semester_pk,
        'percentage_width': percentage_width,
        'rest_width': rest_width,
        'overall_result': ovreall_result_list,
        'current_start_date': start_date,
        'current_end_date': end_date
    })


@login_required
@permission_required('curriculum.can_mark_attendance')
def overall_weekly_attendance_report(request, semester_pk=None, start_date=None, end_date=None, data_for_pdf=False):
    staff_instance = staff.objects.get(user=request.user)

    avaliable_semesters = get_active_semesters_tabs_for_overall_reports(request)
    if not avaliable_semesters:
        modal = {
            'heading': 'Error',
            'body': 'No semester plan has been created for your access',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    pprint(avaliable_semesters)

    if request.user.groups.filter(name='faculty_advisor').exists() and not \
            request.user.groups.filter(name='hod').exists():
        new_avaliable_semesters = []
        semesters_as_fa = semester_migration.objects.filter(
            semester__faculty_advisor=staff_instance
        )
        for entry in semesters_as_fa:
            new_avaliable_semesters.append(entry.semester)

        pprint('new semesters ' + str(new_avaliable_semesters))

        final_semesters = []
        for entry in avaliable_semesters:
            print(entry)
            sem_inst = semester.objects.get(pk=entry['semester_instance'].pk)
            if sem_inst in new_avaliable_semesters:
                print((sem_inst))
                final_semesters.append(entry)

        pprint('final batches = ' + str(final_semesters))
        if final_semesters:
            avaliable_semesters = final_semesters

    selected_semester_pk = avaliable_semesters[0]['semester_instance'].pk

    if request.method == "POST":
        selected_semester_pk = int(request.POST.get('semester_pk'))

    if semester_pk:
        selected_semester_pk = int(semester_pk)

    print(selected_semester_pk)

    selected_semester = semester.objects.get(pk=selected_semester_pk)
    alloted_course_instances_of_selected_semester = staff_course.objects.filter(
        semester=selected_semester
    )

    course_instances_of_selected_semester = []
    for entry in alloted_course_instances_of_selected_semester:
        course_instances_of_selected_semester.append(entry.course)

    course_instances_of_selected_semester.sort(key=lambda x: x.course_id[-1])

    pprint('courese list ' + str(course_instances_of_selected_semester))

    fetched_result_list = []
    weeks_so_far = None

    for course_instance in course_instances_of_selected_semester:
        temp = {}
        staff_course_instance = staff_course.objects.filter(semester=selected_semester).filter(course=course_instance)
        for entry in staff_course_instance:
            faculty_instance = entry.staffs.all()
            current_semester_of_course = selected_semester
            weeks_so_far = form_weeks(selected_semester, course_instance)
            if weeks_so_far is None:
                msg = {
                    'page_title': 'Date Error',
                    'title': 'Start Date Not Found',
                    'description': 'Your Programme co-ordinator has not supplied the start date of this semester',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

            if start_date is None and end_date is None:
                start_date = weeks_so_far[-1]['start_day']
                end_date = weeks_so_far[-1]['end_day']
            if request.method == 'POST' and request.POST.get('date_range'):
                dates = [value for value in request.POST.get('date_range').split('_')]
                start_date = datetime.strptime(dates[0], "%Y-%m-%d").date()
                end_date = datetime.strptime(dates[1], "%Y-%m-%d").date()
                pprint(dates)
                print((request.POST.get('date_range')))

            temp['course_obj'] = course_instance
            temp['faculty_obj'] = faculty_instance
            temp['attendance_result'] = overall_attendance_report(
                course_instance=course_instance,
                start_date=start_date,
                end_date=end_date,
                semester_inst=current_semester_of_course,
            )
            fetched_result_list.append(temp)

    pprint(fetched_result_list)

    ovreall_result_list = form_overall_final_result(fetched_result_list)

    pprint(ovreall_result_list)

    length = len(ovreall_result_list['courses_and_faculty_list'])
    percentage_width = rest_width = 0
    if length > 0:
        percentage_width = 25 / len(ovreall_result_list['courses_and_faculty_list'])
        rest_width = 17 / len(ovreall_result_list['courses_and_faculty_list'])

    if data_for_pdf:
        context_data = {
            'title_text': 'OVERALL WEEKLY ATTENDANCE REPORT',
            'percentage_width': percentage_width,
            'rest_width': rest_width,
            'selected_batch_text': get_dispaly_text_for_batch_id(selected_semester.batch.pk,
                                                                 selected_semester.department),
            'overall_result': ovreall_result_list,
            'current_start_date': datetime.strptime(start_date, "%Y-%m-%d"),
            'current_end_date': datetime.strptime(end_date, "%Y-%m-%d"),
        }
        return context_data

    return render(request, 'curriculum/overall_weekly_attendance_report.html', {
        'semesters': avaliable_semesters,
        'selected_semester_pk': selected_semester_pk,
        'overall_result': ovreall_result_list,
        'current_start_date': start_date,
        'current_end_date': end_date,
        'weeks': weeks_so_far,
        'percentage_width': percentage_width,
        'rest_width': rest_width,
    })


@login_required
@permission_required('curriculum.can_mark_attendance')
def overall_custom_attendance_report(request, semester_pk=None, st_date=None, ed_date=None, data_for_pdf=False):
    staff_instance = staff.objects.get(user=request.user)

    avaliable_semesters = get_active_semesters_tabs_for_overall_reports(request)
    if not avaliable_semesters:
        modal = {
            'heading': 'Error',
            'body': 'No semester plan has been created for your access',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    pprint(avaliable_semesters)

    if request.user.groups.filter(name='faculty_advisor').exists() and not \
            request.user.groups.filter(name='hod').exists():
        new_avaliable_semesters = []
        semesters_as_fa = semester_migration.objects.filter(
            semester__faculty_advisor=staff_instance
        )
        for entry in semesters_as_fa:
            new_avaliable_semesters.append(entry.semester)

        pprint('new semesters ' + str(new_avaliable_semesters))

        final_semesters = []
        for entry in avaliable_semesters:
            print(entry)
            sem_inst = semester.objects.get(pk=entry['semester_instance'].pk)
            if sem_inst in new_avaliable_semesters:
                print((sem_inst))
                final_semesters.append(entry)

        pprint('final batches = ' + str(final_semesters))
        if final_semesters:
            avaliable_semesters = final_semesters

    selected_semester_pk = avaliable_semesters[0]['semester_instance'].pk

    if request.method == "POST":
        selected_semester_pk = int(request.POST.get('semester_pk'))

    if semester_pk:
        selected_semester_pk = int(semester_pk)

    print(selected_semester_pk)

    selected_semester = semester.objects.get(pk=selected_semester_pk)
    alloted_course_instances_of_selected_semester = staff_course.objects.filter(
        semester=selected_semester
    )

    course_instances_of_selected_semester = []
    for entry in alloted_course_instances_of_selected_semester:
        course_instances_of_selected_semester.append(entry.course)

    course_instances_of_selected_semester.sort(key=lambda x: x.course_id[-1])

    pprint('courese list ' + str(course_instances_of_selected_semester))

    fetched_result_list = []

    start_date = None
    end_date = None

    for course_instance in course_instances_of_selected_semester:
        temp = {}
        try:
            staff_course_instance = staff_course.objects.filter(semester=selected_semester).filter(
                course=course_instance)
            for entry in staff_course_instance:
                faculty_instance = entry.staffs.all()
                current_semester_of_course = selected_semester
                if request.method == 'POST' or data_for_pdf:
                    if request.method == 'POST':
                        if not request.POST.get('start_date') and not request.POST.get('end_date'):
                            return render(request, 'curriculum/overall_custom_attendance_report.html', {
                                'semesters': avaliable_semesters,
                                'selected_semester_pk': selected_semester_pk,
                            })
                        start_date = datetime.strptime(request.POST.get('start_date'), "%Y-%m-%d").date()
                        end_date = datetime.strptime(request.POST.get('end_date'), "%Y-%m-%d").date()

                    if st_date:
                        start_date = datetime.strptime(st_date, "%Y-%m-%d").date()
                    if ed_date:
                        end_date = datetime.strptime(ed_date, "%Y-%m-%d").date()

                    semester_instance = current_semester_of_course
                    if semester_instance.start_term_1 > start_date:
                        print('start date error')
                        modal = {
                            'heading': 'Date Error',
                            'body': 'The start date seems to be out of range for this subject',
                        }
                        return render(request, 'curriculum/overall_custom_attendance_report.html', {
                            'semesters': avaliable_semesters,
                            'selected_semester_pk': selected_semester_pk,
                            'toggle_model': 'true',
                            'modal': modal})

                    if semester_instance.end_term_3 and semester_instance.end_term_3 < end_date or (
                                datetime.today().date() < end_date):
                        print('end date error')
                        modal = {
                            'heading': 'Date Error',
                            'body': 'The end date seems to be out of range for this subject',
                        }
                        return render(request, 'curriculum/overall_custom_attendance_report.html', {
                            'semesters': avaliable_semesters,
                            'selected_semester_pk': selected_semester_pk,
                            'toggle_model': 'true',
                            'modal': modal})

                temp['course_obj'] = course_instance
                temp['faculty_obj'] = faculty_instance
                temp['attendance_result'] = overall_attendance_report(
                    course_instance=course_instance,
                    start_date=start_date,
                    end_date=end_date,
                    semester_inst=current_semester_of_course,
                )
                fetched_result_list.append(temp)
        except:
            print('course not handled by any faculty')
            pass

    pprint(fetched_result_list)

    ovreall_result_list = form_overall_final_result(fetched_result_list)

    pprint(ovreall_result_list)

    length = len(ovreall_result_list['courses_and_faculty_list'])
    percentage_width = rest_width = 0
    if length > 0:
        percentage_width = 25 / len(ovreall_result_list['courses_and_faculty_list'])
        rest_width = 17 / len(ovreall_result_list['courses_and_faculty_list'])

    if data_for_pdf:
        context_data = {
            'title_text': 'OVERALL CUSTOM ATTENDANCE REPORT',
            'percentage_width': percentage_width,
            'rest_width': rest_width,
            'selected_batch_text': get_dispaly_text_for_batch_id(selected_semester.batch.pk,
                                                                 selected_semester.department),
            'overall_result': ovreall_result_list,
            'current_start_date': start_date,
            'current_end_date': end_date,
        }
        return context_data

    return render(request, 'curriculum/overall_custom_attendance_report.html', {
        'semesters': avaliable_semesters,
        'selected_semester_pk': selected_semester_pk,
        'overall_result': ovreall_result_list,
        'current_start_date': start_date,
        'current_end_date': end_date,
        'percentage_width': percentage_width,
        'rest_width': rest_width,
    })


class overallCustomPdfReport(PDFTemplateView):
    template_name = "curriculum/overall_pdf_report.html"
    semester_pk = None
    start_date = None
    end_date = None

    def get(self, request, *args, **kwargs):
        self.start_date = kwargs.pop('start_date')
        self.end_date = kwargs.pop('end_date')
        self.semester_pk = kwargs.pop('semester_pk')
        return super(overallCustomPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = overall_custom_attendance_report(self.request,
                                                        semester_pk=self.semester_pk,
                                                        st_date=self.start_date,
                                                        ed_date=self.end_date,
                                                        data_for_pdf=True
                                                        )
        print(context_data)
        return super(overallCustomPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )
