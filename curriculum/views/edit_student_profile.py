from django.contrib.auth.views import redirect_to_login
from django.http import Http404
from django.views.generic import UpdateView

from accounts.models import student, staff
from accounts.forms import updateStudentByStaff
from curriculum.views.common_includes import get_students_under_staff


class UpdateStudentProfile(UpdateView):
    form_class = updateStudentByStaff
    template_name = 'edit_form.html'
    success_url = '/deapproval'
    roll_number = None

    def user_passes_test(self, request):
        if request.user.is_authenticated() and request.user.is_staff_account:
            staff_instance = staff.objects.get(user=request.user)
            student_instance = student.objects.get(roll_no=self.roll_number)
            print(get_students_under_staff(staff_instance))
            if student_instance in get_students_under_staff(staff_instance):
                return True
        return False

    def dispatch(self, request, *args, **kwargs):
        self.roll_number = kwargs.pop('roll_number')
        if not self.user_passes_test(request):
            return redirect_to_login(request.get_full_path())
        return super(UpdateStudentProfile, self).dispatch(
            request, *args, **kwargs)

    def get_object(self):
        try:
            obj = student.objects.get(roll_no=self.roll_number)
        except student.DoesNotExist:
            raise Http404()
        return obj