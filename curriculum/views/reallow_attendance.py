from datetime import datetime, timedelta

from django.contrib.auth.decorators import login_required, permission_required
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone

from accounts.models import staff
from curriculum.forms import reallow_attendance_form
from curriculum.models import attendance, time_table, day, staff_course
from  curriculum.views.common_includes import get_current_course_allotment_with_display_text


@login_required
@permission_required('curriculum.can_reallow_attendance')
def reallow_attendance(request):
    hod_object = staff.objects.get(user=request.user)
    dept = hod_object.get_department()
    queryset = staff.objects.all().filter(department=dept).filter(user__is_approved=True)
    print(vars(queryset))
    reallow_form = reallow_attendance_form(queryset=queryset)
    if request.method == 'POST':
        faculty = request.POST.get('faculty')
        staff_course_pk = request.POST.get('course')
        date = request.POST.get('date')
        hour = request.POST.get('hour')
        allow = request.POST.get('allow')
        update = request.POST.get('update')
        print(faculty)
        # print(course)
        print(date)
        print(hour)
        print(allow)
        print(update)

        current_date = timezone.now().date()
        staff_instance = staff.objects.get(pk=faculty)
        staff_course_inst = staff_course.objects.get(pk=int(staff_course_pk))
        if allow == 'on':
            if update == 'true':
                attendance_instance = attendance.objects.filter(semester=staff_course_inst.semester).filter(
                    date=date).filter(staff=staff_instance).filter(
                    course=staff_course_inst.course).filter(hour=hour)
                attendance_instance.update(granted_edit_access=True)
                attendance_instance.update(grant_period=datetime.now() + timedelta(hours=2))
                print(attendance_instance)
            else:
                attentance_obj = attendance.objects.create(
                    date=date,
                    hour=hour,
                    course=staff_course_inst.course,
                    staff=staff_instance,
                    semester=staff_course_inst.semester,
                    granted_edit_access=True,
                    grant_period=datetime.now() + timedelta(hours=2)
                )
            modal = {
                'heading': 'Allowed to mark attendance',
                'body': 'Successfully allowed the requested faculty to mark attendance for two hours',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    return render(request, 'curriculum/reallow_attendance.html', {'reallow_attendance_form': reallow_form})


def get_courses_alloted_to_faculty(request):
    print('Method called')

    if request.method == 'POST':
        print('POST called')
        # POST goes here . is_ajax is must to capture ajax requests. Beginner's pit.
        if request.is_ajax():
            got_faculty = request.POST.get('faculty')
            print(got_faculty)
            staff_instance = staff.objects.get(pk=got_faculty)
            list_of_allotments = get_current_course_allotment_with_display_text(staff_instance)

            final_list = []
            for entry in list_of_allotments:
                temp = {}
                temp['staff_course_pk'] = entry['staff_course_instance'].pk
                temp['display_text'] = entry['display_text']
                final_list.append(temp)

            data = {}

            data['courses'] = final_list

        return JsonResponse(data)


def get_hours_of_selected_date(request):
    print('Method called')

    if request.method == 'POST':
        print('POST called')
        # POST goes here . is_ajax is must to capture ajax requests. Beginner's pit.
        if request.is_ajax():
            got_date = request.POST.get('date')
            print(got_date)
            got_allotment_pk = request.POST.get('staff_course_pk')
            print(got_allotment_pk)
            got_faculty = request.POST.get('faculty')
            print(got_faculty)

            day_name = datetime.strptime(got_date, '%Y-%m-%d').strftime('%A')
            print(day_name)
            day_obj = day.objects.get(day_name=day_name)
            print(vars(day_obj))

            staff_course_instance = staff_course.objects.get(pk=int(got_allotment_pk))
            # staff_instance = staff.objects.get(pk=got_faculty)
            list_of_hours_in_given_day = time_table.objects.filter(semester=staff_course_instance.semester).filter(
                is_active=True).filter(
                day=day_obj).filter(course=staff_course_instance.course).values('hour')

            print(list_of_hours_in_given_day)
            data = {}
            list = []
            for obj in list_of_hours_in_given_day:
                list.append(obj['hour'])

            print(list)
            data['hours'] = list

        return JsonResponse(data)


def check_attendance_entry(request):
    print('Method called')

    if request.method == 'POST':
        print('POST called')
        # POST goes here . is_ajax is must to capture ajax requests. Beginner's pit.
        if request.is_ajax():
            got_date = request.POST.get('date')
            print(got_date)
            got_staff_course_pk = request.POST.get('staff_course_pk')
            print(got_staff_course_pk)
            got_faculty = request.POST.get('faculty')
            print(got_faculty)
            got_hour = request.POST.get('hour')
            print(got_hour)

            staff_course_instance = staff_course.objects.get(pk=int(got_staff_course_pk))
            staff_instance = staff.objects.get(pk=got_faculty)

            try:
                required_attendance = attendance.objects.filter(semester=staff_course_instance.semester).filter(
                    course=staff_course_instance.course).filter(
                    staff=staff_instance).filter(date=got_date).get(hour=got_hour)
                update_attendance = True
                print(required_attendance)
            except attendance.DoesNotExist:
                update_attendance = False
                print('not found')

            data = {}
            data['update_attendance'] = update_attendance

        return JsonResponse(data)
