from accounts.models import student,active_batches
from curriculum.models import semester,course,attendance
from alumni.models import *





def semesters_studied(request):
    semes_list=attendance.objects.filter(Q(present_students=student_instance)|Q(absent_students=student_instance)|Q           (onduty_students=student_instance)).values_list('semester',flat=True).distinct()
    semester_lis=list(semes_list)
    for inst in semes_list:
        sem_inst=semester.objects.get(id=inst)
        sem_nol.append(sem_inst.semester_number)
    dup_seml=set([x for x in sem_nol if sem_nol.count(x)>1])
    for duplicat_num in dup_seml:
        dup_sem_cur=[]
        def get_start_time(elem):
            return elem.start_term_1
        for inst in semes_list:
            sem_inst=semester.objects.get(id=inst)
            if sem_inst.semester_number==duplicat_num:
                dup_sem_cur.append(sem_inst)
        dup_sem_cur.sort(key=get_start_time,reverse=True)
        for i in range(1,len(dup_sem_cur)):
            semester_lis.remove(i.id)
    def get_sem_num(elem):
        return elem.semester_number
    semester_lis.sort(key=get_sem_num)
    return semester_lis

def fill_sem_marks(request):
    exis_list=[]
    sem_inst=-1
    if request.method==POST:
        if 'semester' in request.POST:
            sem_inst=request.POST['semester']
            for each in GradePoints.objects.filter(semester=each):
                gp=request.POST[each.course_id]
                gpInst=GradePoint.objects.get(student=request.user,semester=sem_inst,course=each.course)
                gpInst.GradePt=gp
                if gp==0:
                    exis_list.append(each.course)
                gpInst.save()
    semester_lis=semesters_studied()
    if len(exis_list)>0:
        for i in semester_lis:
            if i.semester_number==sem_inst.semester_number+1:
                for each in exis_list:
                    Gradepoints.objects.create(student=request.user,semester=sem_inst,course=each)
                course_list=attendance.objects.filter(Q(present_students=student_instance)|Q(absent_students=student_instance)|Q           (onduty_students=student_instance)|Q(semester=each)).values_list('course',flat=True).distinct()
                for each in course_list:
                    GradePoints.objects.get_or_create(student=request.user,semester=sem_inst,course=courses.objects.get(pk=each))
            break
        active_batches_instance = active_batches.objects.filter(
            department=student_instance.department
        ).get(
            batch=student_instance.batch
        )
        if i!=active_batches_instance.current_semester_number_of_this_batch:
            return render(request,'alumni/GPAfilling.html',{'course':course_list+exis_list,'semester':i})
        else:
            message={}
            message['title']="success in filling marks"
            message['description']="End Semester marks filled successfully"
            return render(request,'message':message,'dashboard/signup_success.html')

                
    for each in semester_lis:
        try:
            student_arr=GradePoints.objects.filter(student=request.user,semester=each)
            if student_arr[0].GradePt==-1:
                student_arr1=GradePoints.objects.filter(student=request.user,semester=each).course
                student_arr1+=exis_list
                return render(request,'alumni/GPAfilling.html',{'course':student_arr1,'semester':each)
            continue
        except:
            cour_lis=[]
            course_list=attendance.objects.filter(Q(present_students=student_instance)|Q(absent_students=student_instance)|Q           (onduty_students=student_instance)|Q(semester=each)).values_list('course',flat=True).distinct()
            for each in course_list:
                cour_lis.append(courses.objects.get(pk=each))
            cour_lis+=exis_list
            for each1 in cour_lis:
                GradePoints.objects.create(student=request.user,semester=each,course=each1)
            return render(request,'alumni/GPAfilling.html',{'course':cour_lis,'semester':each})
