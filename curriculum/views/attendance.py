import hashlib
from datetime import datetime, timedelta
from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from accounts.models import staff, student, issue_onduty
from college_automation import settings
from curriculum.models import time_table, courses, attendance, college_schedule, flexi_attendance, semester, batch_split
from curriculum.views.common_includes import get_list_of_students, \
    get_staff_course_allotment_instances_for_staff, get_filtered_students_for_lab, get_batch_split_list
from django.utils import timezone

import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


def get_next_tuesday(date_inst):
    offset_value = 8 - date_inst.weekday()
    next_tuesday = date_inst + timedelta(days=offset_value)
    return next_tuesday


"""
monday - +8
tuesday - +7
wed - +6

"""


def is_allowed_attendance_threshold(date_time_instance):
    return True
    date_instance = date_time_instance.date()
    offset_value = 8 - date_instance.weekday()
    next_tuesday = date_instance + timedelta(offset_value)
    if (timezone.now().date() <= next_tuesday):
        return True
    else:
        return False


def allowed_course(course_inst, faculty_inst):
    alloted_instances_for_faculty = get_staff_course_allotment_instances_for_staff(faculty_inst)
    list_of_handled_courses = alloted_instances_for_faculty.values_list('course')

    print('alloted instances')
    pprint(alloted_instances_for_faculty)
    print('list of handled courses')
    pprint(list_of_handled_courses)

    if alloted_instances_for_faculty.filter(course=course_inst).exists():
        return True

    return False


def allowed_date_hour(course_inst, attendance_date, hour):
    time_table_query = time_table.objects.filter(
        course=course_inst,
        created_date__lte=attendance_date,
        modified_date__gte=attendance_date,
        hour=hour
    )
    if time_table_query.exists():
        return True

    return False


def check_if_attendance_already_marked(semester_inst, courese_obj, attendance_date, hour):
    current_datetime = datetime.now()
    # print(current_datetime)

    attendance_already_marked_query = attendance.objects.filter(semester=semester_inst).filter(
        course=courese_obj).filter(
        date=attendance_date).filter(hour=hour)

    temp = {}

    if attendance_already_marked_query.exists():
        attendance_already_marked_object = attendance_already_marked_query.get()
        print(vars(attendance_already_marked_object))

        already_absent_roll_number = []
        already_absent_students_list = attendance_already_marked_object.absent_students.all()
        for stud in already_absent_students_list:
            roll_num = getattr(stud, 'roll_no')
            already_absent_roll_number.append(roll_num)

        already_onduty_roll_number = []
        already_onduty_students_list = attendance_already_marked_object.onduty_students.all()
        for stud in already_onduty_students_list:
            roll_num = getattr(stud, 'roll_no')
            already_onduty_roll_number.append(roll_num)

        if attendance_already_marked_object.granted_edit_access == False:
            print('inside grant edit access')
            temp['already_absent_list'] = already_absent_roll_number
            temp['already_onduty_list'] = already_onduty_roll_number
            temp['already_marked_obj'] = attendance_already_marked_object
            temp['status'] = 'no_access'
            return temp

        if current_datetime > attendance_already_marked_object.grant_period:
            print(current_datetime)
            print(attendance_already_marked_object.grant_period)
            print('inside grant period')
            temp['already_absent_list'] = already_absent_roll_number
            temp['already_onduty_list'] = already_onduty_roll_number
            temp['already_marked_obj'] = attendance_already_marked_object
            temp['status'] = 'no_access'
            return temp

        temp['already_absent_list'] = already_absent_roll_number
        temp['already_onduty_list'] = already_onduty_roll_number
        temp['already_marked_obj'] = attendance_already_marked_object
        temp['status'] = 'has_access'
        return temp

    else:
        temp['already_absent_list'] = None
        temp['already_onduty_list'] = None
        temp['already_marked_obj'] = None
        temp['status'] = 'not_already_marked'
        return temp


def check_if_flexi_marked_by_another_staff(semeter_inst, courese_obj, attendance_date, hour):
    flexi_attendance_query = flexi_attendance.objects.filter(
        semester=semeter_inst
    ).filter(
        original_course=courese_obj
    ).filter(
        attendance_date=attendance_date
    ).filter(
        attendance_hour=hour
    )

    for entry in flexi_attendance_query:
        attendance_query = attendance.objects.filter(
            date=entry.attendance_date,
            hour=entry.attendance_hour,
            course=entry.marked_course,
            semester=semeter_inst,
        )
        if attendance_query.exists():
            return True

    # additional attendance table check
    additional_attendance_query = attendance.objects.filter(
        date=attendance_date,
        hour=hour,
        semester=semeter_inst,
    ).exclude(
        course=courese_obj
    ).exclude(
        course__subject_type='P'
    )

    if additional_attendance_query.exists():
        return True

    return False


def check_if_flexi_marked(semeter_inst, courese_obj, attendance_date, hour):
    flexi_attendance_query = flexi_attendance.objects.filter(
        semester=semeter_inst
    ).filter(
        original_course=courese_obj
    ).filter(
        attendance_date=attendance_date
    ).filter(
        attendance_hour=hour
    )

    for entry in flexi_attendance_query:
        attendance_query = attendance.objects.filter(
            date=entry.attendance_date,
            hour=entry.attendance_hour,
            course=entry.marked_course,
            semester=semeter_inst,
            grant_period__lt=timezone.now()
        )
        if attendance_query.exists():
            attendance_instance = attendance_query.get()
            if attendance_instance.present_students.count() != 0 and \
                            attendance_instance.absent_students.count() != 0 and \
                            attendance_instance.onduty_students.count() != 0:
                return True

    return False


def validate_attendance_not_already_marked_for_students(semester_inst, course_obj, attendance_date, hour,
                                                        combined_list):
    attendance_querysets = attendance.objects.filter(
        semester=semester_inst,
        date=attendance_date,
        hour=hour
    ).exclude(
        course=course_obj
    )
    for entry in combined_list:
        student_obj = student.objects.get(roll_no=entry)
        if attendance_querysets.filter(absent_students=student_obj).exists() or \
                attendance_querysets.filter(present_students=student_obj).exists() or \
                attendance_querysets.filter(onduty_students=student_obj).exists():
            return False
    return True


def create_attendance_entry(semester_inst, attendance_date, hour, course_obj, staff_obj, absent_list, present_list,
                            od_list):
    if validate_attendance_not_already_marked_for_students(semester_inst, course_obj, attendance_date, hour,
                                                           (absent_list + present_list +
                                                                od_list)) == False:
        return False
    attentance_obj = attendance.objects.create(
        date=attendance_date,
        hour=hour,
        course=course_obj,
        staff=staff_obj,
        granted_edit_access=False,
        grant_period=datetime.now(),
        semester=semester_inst,
    )
    for absentee in absent_list:
        student_obj = student.objects.get(roll_no=absentee)
        attentance_obj.absent_students.add(student_obj)

    for present_students_roll_no in present_list:
        student_obj = student.objects.get(roll_no=present_students_roll_no)
        attentance_obj.present_students.add(student_obj)

    for onduty_students_roll_no in od_list:
        student_obj = student.objects.get(roll_no=onduty_students_roll_no)
        attentance_obj.onduty_students.add(student_obj)

    return True


def permitted_time(course_obj, attendance_date, hour, modification_time):
    print(vars(course_obj))
    current_datetime = datetime.now().replace(microsecond=0)
    print(current_datetime)
    semester_number = course_obj.semester
    if (semester_number == 1 or semester_number == 2):
        schedule_year = 'first_year'
    else:
        schedule_year = 'remaining_years'
    try:
        print(course_obj.regulation)
        for entry in college_schedule.objects.all():
            print(vars(entry))
            print(schedule_year)
            print(hour)
        college_schedule_hour_obj = college_schedule.objects.filter(regulation=course_obj.regulation).filter(
            year=schedule_year).get(hour=hour)
    except college_schedule.DoesNotExist:
        return 'no_schedule'
    start_time_of_hour = getattr(college_schedule_hour_obj, 'start_of_hour')
    start_hour_date_time = datetime.combine(attendance_date, start_time_of_hour)
    print(start_hour_date_time)

    if (start_hour_date_time > current_datetime):
        return 'early'

    allowed_time = None
    if modification_time:
        allowed_time = modification_time

    else:

        if (getattr(college_schedule_hour_obj, 'limit_starts_from') == 'BOH'):
            start_time = getattr(college_schedule_hour_obj, 'start_of_hour')
            # print(start_time)
            start_date_time = datetime.combine(attendance_date, start_time)
            # print(start_date_time)
            allowed_duration = getattr(college_schedule_hour_obj, 'attendance_marking_time_limit')
            allowed_time = start_date_time + timedelta(hours=allowed_duration)
        elif (getattr(college_schedule_hour_obj, 'limit_starts_from') == 'EOH'):
            start_time = getattr(college_schedule_hour_obj, 'end_of_hour')
            # print(start_time)
            start_date_time = datetime.combine(attendance_date, start_time)
            # print(start_date_time)
            allowed_duration = getattr(college_schedule_hour_obj, 'attendance_marking_time_limit')
            allowed_time = start_date_time + timedelta(hours=allowed_duration)

    print(allowed_time)

    if (current_datetime > allowed_time and not settings.HIDE_THIS_FEATURE):
        return 'late'


@login_required
@permission_required('curriculum.can_mark_attendance')
def mark_bulk_attendance(request, semester_pk, course_id, attendance_date, start_hour, end_hour):
    current_user = request.user
    staff_obj = staff.objects.get(user=current_user)

    attendance_date = datetime.strptime(attendance_date, "%Y-%m-%d")
    course_obj = courses.objects.get(pk=course_id)
    semester_instance = semester.objects.get(pk=semester_pk)
    batch_split_instances = None
    selected_batch_split = None

    if course_obj.subject_type == "P":
        batch_split_instances = batch_split.objects.filter(
            semester=semester_instance,
            course=course_obj,
        )
        if batch_split_instances.count() == 0:
            msg = {
                'page_title': 'No batches',
                'title': 'No batches have been split',
                'description': 'Please ask your programme co-ordinator to split batches for this lab',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if not 'flexi_marking' in request.session:
        if not allowed_course(course_obj, staff_obj):
            modal = {
                'heading': 'Error',
                'body': 'Your are not allowed to mark attendance for this course',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
    start_hour = int(start_hour)
    end_hour = int(end_hour)

    hour = start_hour
    while (hour <= end_hour):
        if check_if_flexi_marked(semester_instance, course_obj, attendance_date, hour):
            modal = {
                'heading': 'Error',
                'body': 'Somebody has alredy flexi-marked attendance for this hour.Please check under "Flexi-attenance requests"',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

        hour += 1

    hour = start_hour
    while (hour <= end_hour):
        already_marked = check_if_attendance_already_marked(semester_instance, course_obj, attendance_date, hour)

        if already_marked['status'] == 'no_access':
            msg = {
                'page_title': 'Already Marked',
                'title': 'Attendance already Marked ! ',
                'description': 'You have already marked attendance for this hour.If you need to make any changes,contact HOD of corresponding subject',
            }
            already_marked_obj = already_marked['already_marked_obj']
            if already_marked_obj.present_students.count() == 0 and \
                            already_marked_obj.absent_students.count() == 0 and \
                            already_marked_obj.onduty_students.count() == 0 and \
                            already_marked_obj.grant_period < timezone.now():
                msg = {
                    'page_title': 'Time Exceeded',
                    'title': 'Why so late?',
                    'description': 'You cannot mark attnedance now.Contact HOD of corresponding subject to regain access to mark attendance',
                }
            reallow_parameter = {}
            reallow_parameter['attendance_date'] = attendance_date.strftime("%Y-%m-%d")
            reallow_parameter['course_id'] = course_id
            reallow_parameter['start_hour'] = start_hour
            reallow_parameter['end_hour'] = end_hour
            reallow_parameter['semester_pk'] = semester_instance.pk
            return render(request, 'prompt_pages/already_marked_bulk_attendance.html',
                          {'message': msg, 'reallow_parameter': reallow_parameter})

        hour += 1

    # handle change batch
    if request.method == "POST" and 'batch_split_pk' in request.POST:
        batch_split_pk = request.POST.get('batch_split_pk')
        selected_batch_split = batch_split.objects.get(pk=batch_split_pk)
        student_list = get_batch_split_list(batch_split_pk, semester_instance, course_obj)
        temp_student_list = []
        for entry in student_list:
            stud = entry['student_obj']
            print(stud)
            if stud.date_of_joining <= attendance_date.date():
                temp_student_list.append(entry)

        list_of_students = []
        student_list = temp_student_list
        for entry in student_list:
            stud = entry['student_obj']
            temp_list = {}
            temp_list['student_name'] = stud
            # print(stud)
            student_register_number = getattr(stud, 'roll_no')
            temp_list['registration_number'] = student_register_number
            if already_marked['already_absent_list'] is not None and student_register_number in already_marked[
                'already_absent_list']:
                temp_list['already_absent'] = True
            else:
                temp_list['already_absent'] = False

            if issue_onduty.objects.filter(student=stud).filter(date=attendance_date).exists() or \
                    (already_marked['already_onduty_list'] is not None and student_register_number in
                        already_marked[
                            'already_onduty_list']):
                temp_list['granted_od'] = True
            else:
                temp_list['granted_od'] = False
            list_of_students.append(temp_list)

        modal = {
            'heading': 'Info',
            'body': 'You are about to mark attendance for hours ' + str(start_hour) + ' to ' + str(end_hour),
        }

        return render(request, 'curriculum/attendance.html', {'student_list': list_of_students,
                                                              'course_id': course_id,
                                                              'toggle_model': 'true', 'modal': modal,
                                                              'batch_split_instances': batch_split_instances,
                                                              'selected_batch_split': selected_batch_split
                                                              })

    flag = False
    hour = start_hour
    while (hour <= end_hour):
        already_marked = check_if_attendance_already_marked(semester_instance, course_obj, attendance_date, hour)
        if request.method == 'POST':
            absent_list = request.POST.getlist('absent')
            present_list = request.POST.getlist('present')
            od_list = request.POST.getlist('onduty')

            if already_marked['status'] == 'has_access':
                already_marked_obj = already_marked['already_marked_obj']
                already_marked_obj.delete()

            if create_attendance_entry(semester_instance, attendance_date, hour, course_obj, staff_obj, absent_list,
                                       present_list,
                                       od_list):
                flag = True

        hour += 1

    if request.method == 'POST':
        if flag:
            modal = {
                'heading': 'Success',
                'body': 'Your have successfully markked attendance.\nIncase if you need to change your attendance contact HOD of corresponding subject',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
        else:
            modal = {
                'heading': 'Error',
                'body': 'Attendanace already marked for some of the students',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
    else:
        if already_marked['already_marked_obj']:
            already_marked_obj = already_marked['already_marked_obj']
            grant_period = already_marked_obj.grant_period

            if already_marked_obj.present_students.count() == 0 and \
                            already_marked_obj.absent_students.count() == 0 and \
                            already_marked_obj.onduty_students.count() == 0 and \
                            already_marked_obj.grant_period < timezone.now():
                msg = {
                    'page_title': 'Time Exceeded',
                    'title': 'Why so late?',
                    'description': 'You cannot mark attnedance now.Contact HOD of corresponding subject to regain access to mark attendance',
                }
                reallow_parameter = {}
                reallow_parameter['attendance_date'] = attendance_date.strftime("%Y-%m-%d")
                reallow_parameter['course_id'] = course_id
                reallow_parameter['start_hour'] = start_hour
                reallow_parameter['end_hour'] = end_hour
                reallow_parameter['semester_pk'] = semester_instance.pk
                return render(request, 'prompt_pages/already_marked_bulk_attendance.html',
                              {'message': msg, 'reallow_parameter': reallow_parameter})
        else:
            grant_period = None
            if not is_allowed_attendance_threshold(attendance_date):
                msg = {
                    'page_title': 'Time Exceeded',
                    'title': 'Why so late?',
                    'description': 'You cannot mark attnedance now.Contact HOD of corresponding subject to regain access to mark attendance',
                }
                reallow_parameter = {}
                reallow_parameter['attendance_date'] = attendance_date.strftime("%Y-%m-%d")
                reallow_parameter['course_id'] = course_id
                reallow_parameter['start_hour'] = start_hour
                reallow_parameter['end_hour'] = end_hour
                reallow_parameter['semester_pk'] = semester_instance.pk
                return render(request, 'prompt_pages/already_marked_bulk_attendance.html',
                              {'message': msg, 'reallow_parameter': reallow_parameter})
        if permitted_time(course_obj, attendance_date, start_hour, grant_period) == 'early':
            msg = {
                'page_title': 'Time Early',
                'title': 'Why so early?',
                'description': 'You cannot mark attendance now.Please wait untill you hour commences',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
        elif permitted_time(course_obj, attendance_date, end_hour, grant_period) == 'late':
            msg = {
                'page_title': 'Time Exceeded',
                'title': 'Why so late?',
                'description': 'You cannot mark attnedance now.Contact HOD of subject to regain access to mark attendance',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
        elif permitted_time(course_obj, attendance_date, end_hour, grant_period) == 'no_schedule':
            msg = {
                'page_title': 'No schedule found',
                'title': 'No Schedule Found',
                'description': 'Start and end time for this regulation and hour not found.Please update from admin site',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

        list_of_students = []
        student_list = get_list_of_students(semester_instance, course_obj)

        if course_obj.subject_type == 'P':
            selected_batch_split, student_list = get_filtered_students_for_lab(semester_instance, student_list,
                                                                               course_obj, attendance_date)
            if student_list == -1:
                msg = {
                    'page_title': 'No batch',
                    'title': 'No Batch Assigned',
                    'description': 'Your Programme Coordinator has not assingned any batches for this Practical course.Please contact him/her to do so',
                }
                return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

        # print('Student list after query :')
        # print((student_list))

        temp_student_list = []
        for entry in student_list:
            stud = entry['student_obj']
            print(stud)
            if stud.date_of_joining <= attendance_date.date():
                temp_student_list.append(entry)

        student_list = temp_student_list
        for entry in student_list:
            stud = entry['student_obj']
            temp_list = {}
            temp_list['student_name'] = stud
            # print(stud)
            student_register_number = getattr(stud, 'roll_no')
            temp_list['registration_number'] = student_register_number
            if already_marked['already_absent_list'] is not None and student_register_number in already_marked[
                'already_absent_list']:
                temp_list['already_absent'] = True
            else:
                temp_list['already_absent'] = False

            if issue_onduty.objects.filter(student=stud).filter(date=attendance_date).exists() or \
                    (already_marked['already_onduty_list'] is not None and student_register_number in already_marked[
                        'already_onduty_list']):
                temp_list['granted_od'] = True
            else:
                temp_list['granted_od'] = False
            list_of_students.append(temp_list)
            # print(student_register_number)
        # print('last Each student list : ')
        # print(list_of_students)

        modal = {
            'heading': 'Info',
            'body': 'You are about to mark attendance for hours ' + str(start_hour) + ' to ' + str(end_hour),
        }

        return render(request, 'curriculum/attendance.html', {'student_list': list_of_students,
                                                              'course_id': course_id,
                                                              'toggle_model': 'true', 'modal': modal,
                                                              'batch_split_instances': batch_split_instances,
                                                              'selected_batch_split': selected_batch_split
                                                              })


@login_required
@permission_required('curriculum.can_mark_attendance')
def mark_attendance(request, semester_pk, course_id, attendance_date, hour):
    current_user = request.user
    staff_obj = staff.objects.get(user=current_user)

    attendance_date = datetime.strptime(attendance_date, "%Y-%m-%d")
    course_obj = courses.objects.get(pk=course_id)
    semester_instance = semester.objects.get(pk=semester_pk)
    batch_split_instances = None
    selected_batch_split = None
    if course_obj.subject_type == "P":
        batch_split_instances = batch_split.objects.filter(
            semester=semester_instance,
            course=course_obj,
        )
        if batch_split_instances.count() == 0:
            msg = {
                'page_title': 'No batches',
                'title': 'No batches have been split',
                'description': 'Please ask your programme co-ordinator to split batches for this lab',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if not 'flexi_marking' in request.session:
        if not allowed_course(course_obj, staff_obj):
            modal = {
                'heading': 'Error',
                'body': 'Your are not allowed to mark attendance for this course',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    if check_if_flexi_marked(semester_instance, course_obj, attendance_date, hour):
        modal = {
            'heading': 'Error',
            'body': 'Somebody has alredy flexi-marked attendance for this hour.Please check under "Flexi-attenance requests"',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    if not allowed_date_hour(course_obj, attendance_date, hour) and False:
        modal = {
            'heading': 'Error',
            'body': 'No matching timetable found for this date',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    already_marked = check_if_attendance_already_marked(semester_instance, course_obj, attendance_date, hour)

    add_student_access = False
    if 'can_add_student_to_attendance' in request.session:
        print('session value  = ' + str(request.session['can_add_student_to_attendance']))
        print((hashlib.sha1(str(semester_pk).encode('utf8') + str(course_id).encode('utf8') + str(
            attendance_date.strftime("%Y-%m-%d")).encode('utf8') + str(hour).encode('utf8'))).hexdigest())
        if request.session['can_add_student_to_attendance'] == (hashlib.sha1(
                            str(semester_pk).encode('utf8') + str(course_id).encode('utf8') + str(
                            attendance_date.strftime("%Y-%m-%d")).encode('utf8') + str(hour).encode(
                        'utf8'))).hexdigest():
            already_marked['status'] = 'has_access'
            add_student_access = True

    if already_marked['status'] == 'no_access':
        msg = {
            'page_title': 'Already Marked',
            'title': 'Attendance already Marked ! ',
            'description': 'You have already marked attendance for this hour.HOD of subject has to grant permission to make changes',
        }
        already_marked_obj = already_marked['already_marked_obj']
        if already_marked_obj.present_students.count() == 0 and \
                        already_marked_obj.absent_students.count() == 0 and \
                        already_marked_obj.onduty_students.count() == 0 and \
                        already_marked_obj.grant_period < timezone.now():
            msg = {
                'page_title': 'Time Exceeded',
                'title': 'Why so late?',
                'description': 'You cannot mark attnedance now.Contact HOD of corresponding subject to regain access to mark attendance',
            }
        reallow_parameter = {}
        reallow_parameter['attendance_date'] = attendance_date.strftime("%Y-%m-%d")
        reallow_parameter['semester_pk'] = semester_instance.pk
        reallow_parameter['course_id'] = course_id
        reallow_parameter['hour'] = hour
        return render(request, 'prompt_pages/already_marked_attendance.html',
                      {'message': msg, 'reallow_parameter': reallow_parameter})
        # print(already_absent_roll_number)

        # handle change batch
    if request.method == "POST" and 'batch_split_pk' in request.POST:
        batch_split_pk = request.POST.get('batch_split_pk')
        selected_batch_split = batch_split.objects.get(pk=batch_split_pk)
        student_list = get_batch_split_list(batch_split_pk, semester_instance, course_obj)
        temp_student_list = []
        for entry in student_list:
            stud = entry['student_obj']
            print(stud)
            if stud.date_of_joining <= attendance_date.date():
                temp_student_list.append(entry)

        list_of_students = []
        student_list = temp_student_list
        for entry in student_list:
            stud = entry['student_obj']
            temp_list = {}
            temp_list['student_name'] = stud
            # print(stud)
            student_register_number = getattr(stud, 'roll_no')
            temp_list['registration_number'] = student_register_number
            if already_marked['already_absent_list'] is not None and student_register_number in already_marked[
                'already_absent_list']:
                temp_list['already_absent'] = True
            else:
                temp_list['already_absent'] = False

            if issue_onduty.objects.filter(student=stud).filter(date=attendance_date).exists() or \
                    (already_marked['already_onduty_list'] is not None and student_register_number in
                        already_marked[
                            'already_onduty_list']):
                temp_list['granted_od'] = True
            else:
                temp_list['granted_od'] = False
            list_of_students.append(temp_list)

        return render(request, 'curriculum/attendance.html', {'student_list': list_of_students,
                                                              'course_id': course_id,
                                                              'batch_split_instances': batch_split_instances,
                                                              'selected_batch_split': selected_batch_split,
                                                              })

    if request.method == 'POST':
        absent_list = request.POST.getlist('absent')
        present_list = request.POST.getlist('present')
        od_list = request.POST.getlist('onduty')

        if already_marked['status'] == 'has_access':
            already_marked_obj = already_marked['already_marked_obj']
            already_marked_obj.delete()

        if create_attendance_entry(semester_instance, attendance_date, hour, course_obj, staff_obj, absent_list,
                                   present_list, od_list):
            modal = {
                'heading': 'Success',
                'body': 'Your have successfully markked attendance.\nIncase if you need to change your attendance contact HOD of corresponding subject',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
        else:
            modal = {
                'heading': 'Error',
                'body': 'Attendanace already marked for some of the students',
            }
            return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})
    else:
        if already_marked['already_marked_obj']:
            already_marked_obj = already_marked['already_marked_obj']
            grant_period = already_marked_obj.grant_period

            if already_marked_obj.present_students.count() == 0 and \
                            already_marked_obj.absent_students.count() == 0 and \
                            already_marked_obj.onduty_students.count() == 0 and \
                            already_marked_obj.grant_period < timezone.now():
                msg = {
                    'page_title': 'Time Exceeded',
                    'title': 'Why so late?',
                    'description': 'You cannot mark attnedance now.Contact HOD of corresponding subject to regain access to mark attendance',
                }
                reallow_parameter = {}
                reallow_parameter['attendance_date'] = attendance_date.strftime("%Y-%m-%d")
                reallow_parameter['course_id'] = course_id
                reallow_parameter['hour'] = hour
                reallow_parameter['semester_pk'] = semester_instance.pk
                return render(request, 'prompt_pages/already_marked_attendance.html',
                              {'message': msg, 'reallow_parameter': reallow_parameter})
        else:
            grant_period = None
            if not is_allowed_attendance_threshold(attendance_date) and not add_student_access:
                msg = {
                    'page_title': 'Time Exceeded',
                    'title': 'Why so late?',
                    'description': 'You cannot mark attnedance now.Contact HOD of corresponding subject to regain access to mark attendance',
                }
                reallow_parameter = {}
                reallow_parameter['attendance_date'] = attendance_date.strftime("%Y-%m-%d")
                reallow_parameter['course_id'] = course_id
                reallow_parameter['hour'] = hour
                reallow_parameter['semester_pk'] = semester_instance.pk
                return render(request, 'prompt_pages/already_marked_attendance.html',
                              {'message': msg, 'reallow_parameter': reallow_parameter})

        if permitted_time(course_obj, attendance_date, hour, grant_period) == 'early':
            msg = {
                'page_title': 'Time Early',
                'title': 'Why so early?',
                'description': 'You cannot mark attendance now.Please wait untill you hour commences',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
        elif permitted_time(course_obj, attendance_date, hour, grant_period) == 'late' and not add_student_access:
            msg = {
                'page_title': 'Time Exceeded',
                'title': 'Why so late?',
                'description': 'You cannot mark attnedance now.Contact HOD of corresponding subject to regain access to mark attendance',
            }
            reallow_parameter = {}
            reallow_parameter['attendance_date'] = attendance_date.strftime("%Y-%m-%d")
            reallow_parameter['course_id'] = course_id
            reallow_parameter['hour'] = hour
            reallow_parameter['semester_pk'] = semester_instance.pk
            return render(request, 'prompt_pages/already_marked_attendance.html',
                          {'message': msg, 'reallow_parameter': reallow_parameter})
        elif permitted_time(course_obj, attendance_date, hour, grant_period) == 'no_schedule':
            msg = {
                'page_title': 'No schedule found',
                'title': 'No Schedule Found',
                'description': 'Start and end time for this regulation and hour not found.Please update from admin site',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

        list_of_students = []
        student_list = get_list_of_students(semester_instance, course_obj)

        if course_obj.subject_type == 'P':
            selected_batch_split, student_list = get_filtered_students_for_lab(semester_instance, student_list,
                                                                               course_obj, attendance_date)

        if student_list == -1:
            msg = {
                'page_title': 'No batch',
                'title': 'No Batch Assigned',
                'description': 'Your Programme Coordinator has not assingned any batches for this Practical course on this day.Please contact him/her to do so',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
        # print('Student list after query :')
        # print((student_list))
        temp_student_list = []
        for entry in student_list:
            stud = entry['student_obj']
            print(stud)
            if stud.date_of_joining <= attendance_date.date():
                temp_student_list.append(entry)

        student_list = temp_student_list

        print('already marked obj')
        print((already_marked['already_absent_list']))
        for entry in student_list:
            stud = entry['student_obj']
            print(stud)
            temp_list = {}
            temp_list['student_name'] = stud
            # print(stud)
            student_register_number = getattr(stud, 'roll_no')

            temp_list['registration_number'] = student_register_number
            if already_marked['already_absent_list'] is not None and student_register_number in already_marked[
                'already_absent_list']:
                temp_list['already_absent'] = True
            else:
                temp_list['already_absent'] = False

            if issue_onduty.objects.filter(student=stud).filter(date=attendance_date).exists() or \
                    (already_marked['already_onduty_list'] is not None and student_register_number in already_marked[
                        'already_onduty_list']):
                temp_list['granted_od'] = True
            else:
                temp_list['granted_od'] = False
            list_of_students.append(temp_list)
            print(student_register_number)
        # print('last Each student list : ')
        print(list_of_students)

        print("batch_split_instances " + str(batch_split_instances))

        return render(request, 'curriculum/attendance.html', {'student_list': list_of_students,
                                                              'course_id': course_id,
                                                              'attendance_date': attendance_date.strftime("%d-%m-%Y"),
                                                              'hour': hour,
                                                              'batch_split_instances': batch_split_instances,
                                                              'selected_batch_split': selected_batch_split
                                                              })
