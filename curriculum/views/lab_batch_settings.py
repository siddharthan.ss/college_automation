from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render
from django.utils import timezone

from curriculum.models import courses, semester
from curriculum.views.common_includes import *


@login_required
@permission_required('curriculum.can_split_batch')
def add_lab_batch(request):
    staff_query = staff.objects.filter(user=request.user)
    staff_instance = staff_query.get()
    # santity check for first year
    if staffBelongsToNonCore(staff_instance):
        print(staffBelongsToNonCore(staff_instance))
        if staffNotEligible(staff_instance):
            msg = {
                'page_title': 'Access Denied',
                'title': 'No permission',
                'description': 'Your department does not have this permission. Contact ' + str(
                    get_first_year_managing_department()) + ' department for this settings',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    available_semsters = get_active_semesters_tabs(request)

    if available_semsters:
        selected_semester_pk = available_semsters[0]['semester_instance'].pk
    else:
        modal = {
            'heading': 'Error',
            'body': 'No semester plan has been created for your access',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    if 'selected_semester' in request.POST:
        selected_semester_pk = request.POST['selected_semester']

    selected_semester_instance = semester.objects.get(pk=selected_semester_pk)
    active_batch_instance = active_batches.objects.filter(
        department=selected_semester_instance.department
    ).get(
        batch=selected_semester_instance.batch
    )

    practical_courses_query = courses.objects.filter(
        department=selected_semester_instance.department
    ).filter(
        regulation=selected_semester_instance.batch.regulation
    ).filter(
        semester=active_batch_instance.current_semester_number_of_this_batch
    ).filter(
        subject_type='P'
    ).filter(
        programme=selected_semester_instance.batch.programme
    )

    list_of_practical_courses = []
    for entry in practical_courses_query:
        list_of_practical_courses.append(entry)

    if request.method == 'POST':
        if request.is_ajax():

            dictionary = {}

            if request.POST.get('fetch_students'):
                selected_semester_pk = request.POST.get('selected_semester_pk')
                got_course_pk = request.POST.get('selected_course_pk')
                print(got_course_pk)

                selected_course_instance = courses.objects.get(pk=got_course_pk)
                selected_semester_instance = semester.objects.get(pk=int(selected_semester_pk))
                print('current semester of course')
                print((selected_semester_instance.pk))

                list_of_students = get_list_of_students(selected_semester_instance, selected_course_instance)

                list_of_student_roll_numbers = []
                for each in list_of_students:
                    list_of_student_roll_numbers.append(each['registration_number'])

                time_table_query = time_table.objects.filter(
                    semester=selected_semester_instance
                ).filter(
                    course=selected_course_instance
                ).filter(
                    is_active=True
                )

                list_of_day_instances = []
                for tt in time_table_query:
                    if tt.day.day_name not in list_of_day_instances:
                        list_of_day_instances.append(tt.day.day_name)

                dictionary['list_of_students'] = list_of_student_roll_numbers
                dictionary['list_of_day_instances'] = list_of_day_instances

                return JsonResponse(dictionary)

            got_course_pk = request.POST.get('course_pk')
            got_start_student_roll_no = request.POST.get('start_student')
            got_end_student_roll_no = request.POST.get('end_student')
            got_day = request.POST.get('day')
            got_batch_number = request.POST.get('batch_number')
            selected_semester_pk = request.POST.get('selected_semester_pk')

            selected_course_instance = courses.objects.get(pk=got_course_pk)
            selected_semester_instance = semester.objects.get(pk=selected_semester_pk)

            day_instance = day.objects.get(day_name=got_day)

            start_student_instance = student.objects.get(roll_no=got_start_student_roll_no)
            end_student_instance = student.objects.get(roll_no=got_end_student_roll_no)

            # check batch split exists for the specified start and end student on the same day
            all_roll_numbers_in_selected_range = []

            list_of_students = get_list_of_students(selected_semester_instance, selected_course_instance)
            flag = False
            for each in list_of_students:
                if each['registration_number'] == start_student_instance.roll_no:
                    flag = True
                if each['registration_number'] == end_student_instance.roll_no:
                    flag = False
                if flag == True:
                    all_roll_numbers_in_selected_range.append(each['registration_number'])

            base_check_query = batch_split.objects.filter(
                semester=selected_semester_instance
            ).filter(
                day=day_instance
            ).filter(
                is_active=True
            )

            start_student_range = base_check_query.filter(
                start_student__roll_no__in=all_roll_numbers_in_selected_range
            )

            end_student_range = base_check_query.filter(
                end_student__roll_no__in=all_roll_numbers_in_selected_range
            )

            if start_student_range.count() > 0 or end_student_range.count() > 0:
                list_of_hours = []
                flag = False
                for entry in base_check_query:
                    time_table_inst = time_table.objects.filter(
                        is_active=True
                    ).filter(
                        semester=selected_semester_instance
                    ).filter(
                        course=entry.course
                    ).filter(
                        day=entry.day
                    )
                    for time_table_entries in time_table_inst:
                        if time_table_entries.hour not in list_of_hours:
                            list_of_hours.append(time_table_entries.hour)
                            print('list of hours')
                            pprint(list_of_hours)
                time_table_inst_curr_course = time_table.objects.filter(
                    is_active=True
                ).filter(
                    semester=selected_semester_instance
                ).filter(
                    course=selected_course_instance
                ).filter(
                    day=day_instance
                )
                for time_table_entries in time_table_inst_curr_course:
                    if time_table_entries.hour not in list_of_hours:
                        list_of_hours.append(time_table_entries.hour)
                        print('list of hours')
                        pprint(list_of_hours)
                    else:
                        flag = True
                        break
                if flag:
                    modal = {
                        'heading': 'Error',
                        'body': 'Batch for specified students already exists for the same day. Please delete the existing one and try again',
                    }
                    dictionary['modal'] = modal
                    return JsonResponse(dictionary)

            course_check_query = base_check_query.filter(
                course=selected_course_instance
            )

            for entry in course_check_query:
                if str(entry.batch_number) == got_batch_number:
                    modal = {
                        'heading': 'Error',
                        'body': 'Already an batch with same batch number for this course exists',
                    }
                    dictionary['modal'] = modal
                    return JsonResponse(dictionary)

            batch_split.objects.get_or_create(
                semester=selected_semester_instance,
                course=selected_course_instance,
                day=day_instance,
                start_student=start_student_instance,
                end_student=end_student_instance,
                timestamp=timezone.now(),
                batch_number=got_batch_number,
            )

            list = []
            for each in batch_split.objects.filter(semester=selected_semester_instance).filter(is_active=True):
                temp = {}
                temp['batch_number'] = str(each.batch_number)
                temp['start_student'] = str(each.start_student.roll_no)
                temp['end_student'] = str(each.end_student.roll_no)
                temp['day'] = str(each.day.day_name)
                temp['course'] = str(each.course)

                list.append(temp)

            dictionary['list'] = list

            return JsonResponse(dictionary)

    list_of_lab_batches = []

    for each in batch_split.objects.filter(semester=selected_semester_instance).filter(is_active=True):
        list_of_lab_batches.append(each)

    return render(request, 'batch_settings/add_lab_batch.html', {
        'list_of_semesters': available_semsters,
        'selected_semester': int(selected_semester_pk),
        'list_of_lab_batches': list_of_lab_batches,
        'list_of_practical_courses': list_of_practical_courses,
    })


@login_required
@permission_required('curriculum.can_split_batch')
def view_split_batches(request, semester_pk):
    print(semester_pk)
    staff_query = staff.objects.filter(user=request.user)
    staff_instance = staff_query.get()

    semester_instance = semester.objects.filter(pk=semester_pk)

    if request.method == 'POST':
        got_split_batch_pk = request.POST.get('delete')
        print(got_split_batch_pk)
        batch_split.objects.get(pk=got_split_batch_pk).delete()

    list_of_lab_batches = []

    for each in batch_split.objects.filter(semester=semester_instance).filter(is_active=True):
        list_of_lab_batches.append(each)

    pprint(locals())

    return render(request, 'batch_settings/view_lab_batches.html', {'list_of_lab_batches': list_of_lab_batches})


"""
@login_required
@permission_required('accounts.can_view_profiles')
def edit_papers(request):
    if request.method == 'POST':
        if 'update' in request.POST:
            got_paper_name = str(request.POST.get('paper_name'))
            got_description = str(request.POST.get('description'))
            got_year = int(request.POST.get('year'))
            required_edit_id = int(request.session['editable_paper_id'])

            del request.session['editable_paper_id']

            staff_papers.objects.filter(id=required_edit_id).update(paper_name=got_paper_name, description=got_description, year_of_publish=got_year)

        if 'delete' in request.POST:
            required_edit_id = int(request.session['editable_paper_id'])

            del request.session['editable_paper_id']

            staff_papers.objects.filter(id=required_edit_id).delete()

        return redirect('../view_my_papers')

    else:
        required_edit = int(request.session['editable_paper_id'])

        paper = staff_papers.objects.get(id=required_edit)

        current_year = datetime.now().year

        return render(request, 'profile_addups/edit_papers.html', { 'paper': paper, 'max_year': current_year })
"""
