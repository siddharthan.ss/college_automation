from datetime import datetime
from itertools import groupby
from operator import itemgetter
from pprint import pprint

from django.utils import timezone

from accounts.models import staff
from common.utils.TimeTableUtil import getTodayTimetable
from curriculum.models import courses
from curriculum.views.common_includes import get_hours_grouped_for_day


def keyfn(x):
    return x['course_id']


def hourkey(x):
    return x['hour']


def check_for_sequence(list_of_numbers):
    ranges = []
    for k, g in groupby(enumerate(list_of_numbers), lambda x: x[0] - x[1]):
        group = map(itemgetter(1), g)
        ranges.append((group[0], group[-1]))
    print(ranges)


def add_courses_offered_to_session(request):
    request.session['courses_offered'] = []
    request.session['current_date'] = None
    current_user = request.user
    try:
        staff_obj = staff.objects.all().get(user=current_user)
    except staff.DoesNotExist:
        return None

    courses_offered = []

    final_list_of_courses = []

    list_of_timetables = getTodayTimetable(staff_obj)

    if not list_of_timetables:
        return None

    print('list_of_courses')

    print((list_of_timetables))

    list_of_date_hours = []

    for entry in list_of_timetables:
        if entry.semester.end_term_3 is not None:
            if entry.semester.end_term_3 < timezone.now().date():
                continue
        temp = {}
        temp['date'] = timezone.now().date()
        temp['hour'] = entry.hour
        temp['semester'] = entry.semester
        temp['course'] = entry.course
        list_of_date_hours.append(temp)

    if len(list_of_date_hours) <= 0:
        return

    list_of_date_hours = sorted(list_of_date_hours, key=lambda x: (x['date'], x['hour']))

    final_list_of_timetables = get_hours_grouped_for_day(list_of_date_hours)

    for entry in final_list_of_timetables:
        entry['date'] = entry['date'].strftime("%Y-%m-%d")

    print('final list of courses ')
    pprint(final_list_of_timetables)

    request.session['courses_offered'] = final_list_of_timetables

    return

    for obj in list_of_courses:
        course_list = {}
        courses_id = getattr(obj, 'course_id')
        hour = getattr(obj, 'hour')
        course_obj = courses.objects.get(pk=courses_id)
        course_name = getattr(course_obj, 'course_name')
        course_list['course_id'] = courses_id
        if course_obj.code:
            course_list['course_name'] = course_obj.code
        else:
            course_list['course_name'] = course_name
        course_list['hour'] = hour
        course_list['bulk'] = False
        courses_offered.append(course_list)

    tuple = ([(k, list(g)) for k, g in groupby(sorted(courses_offered, key=keyfn), keyfn)])

    print('group new')
    for item in tuple:
        temp = {}
        temp['course_id'] = item[0]
        course_inst = courses.objects.get(course_id=item[0])
        if course_obj.code:
            temp['course_name'] = course_inst.code
        else:
            temp['course_name'] = course_inst.course_name
        temp['bulk'] = True
        groped_list = item[1]
        if len(groped_list) > 1:
            print(groped_list)
            groped_list = sorted(groped_list, key=hourkey)
            print(groped_list)
            start_hour = int(groped_list[0]['hour'])
            end_hour = start_hour
            flag = False
            for each in groped_list:
                if flag:
                    start_hour = int(each['hour'])
                    end_hour = start_hour
                    flag = False
                    continue
                if int(each['hour']) == 5:
                    print(start_hour)
                    print(end_hour)
                    temp['start_hour'] = str(start_hour)
                    temp['end_hour'] = str(end_hour)
                    final_list_of_courses.append(temp)
                    temp = {}
                    temp['course_id'] = item[0]
                    if course_obj.code:
                        temp['course_name'] = course_inst.code
                    else:
                        temp['course_name'] = course_inst.course_name
                    temp['bulk'] = True
                    start_hour = int(each['hour'])
                    print(start_hour)
                    end_hour = start_hour
                    print(end_hour)
                if int(each['hour']) - 1 == end_hour:
                    end_hour = int(each['hour'])
                else:
                    start_hour = int(each['hour'])
                    temp['bulk'] = False
                    temp['start_hour'] = start_hour
                    temp['end_hour'] = start_hour
                    final_list_of_courses.append(temp)
                    flag = True

            temp['start_hour'] = str(start_hour)
            temp['end_hour'] = str(end_hour)

            final_list_of_courses.append(temp)
        else:
            start_hour = int(groped_list[0]['hour'])
            temp['bulk'] = False
            temp['start_hour'] = start_hour
            temp['end_hour'] = start_hour
            final_list_of_courses.append(temp)
    # print(course_name)
    print(courses_offered)

    print('final list of courses ')
    pprint(final_list_of_courses)
    request.session['courses_offered'] = final_list_of_courses
    now = datetime.now()
    request.session['current_date'] = now.strftime("%Y-%m-%d")
