from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, redirect

from curriculum.models import flexi_attendance, attendance
from curriculum.views.common_includes import *


@login_required
@permission_required('curriculum.can_mark_attendance')
def mark_flexi_attendance(request, selected_allotment_pk=False):
    faculty_instance = staff.objects.get(user=request.user)
    course_allotment_list = get_current_course_allotment_with_display_text(faculty_instance)
    if not selected_allotment_pk:
        try:
            selected_allotment_pk = course_allotment_list[0]['staff_course_instance'].pk
        except:
            msg = {
                'page_title': 'No courses',
                'title': 'No courses alloted',
                'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    print(selected_allotment_pk)

    if request.method == 'POST':
        selected_allotment_pk = request.POST.get('selected_allotment_pk')
    print('selected_allotment_pk = ' + str(selected_allotment_pk))

    selected_allotment_instance = staff_course.objects.get(pk=selected_allotment_pk)
    selected_course_instance = selected_allotment_instance.course

    if request.method == 'POST' and request.POST.get('hour') and request.POST.get('date'):
        selected_date = request.POST.get('date')
        selected_hour = request.POST.get('hour')

        # validation parts
        # see currently alloted courses if any for the specifed batch in givn date and hour
        current_semester_plan = selected_allotment_instance.semester
        attendance_date = datetime.strptime(selected_date, "%Y-%m-%d")
        day_name = attendance_date.strftime("%A")
        print(day_name)
        time_table_query = time_table.objects.filter(
            semester=current_semester_plan
        ).filter(
            day=day.objects.get(day_name=day_name)
        ).filter(
            hour=selected_hour
        )

        print(time_table_query.count())

        if time_table_query.count() > 0:
            for entry in time_table_query:
                list_of_handling_faculty = get_staff_handling_course(entry.course, current_semester_plan)
                # if list_of_handling_faculty is not None:
                #     for each in list_of_handling_faculty:
                #         print(each)
                #         print('and')
                #         print(faculty_instance)
                #         if each == faculty_instance:
                #             modal = {
                #                 'heading': 'Error',
                #                 'body': 'You cannot flexi-mark attendance for your own hour',
                #             }
                #             return render(request, 'dashboard/dashboard_content.html',
                #                           {'toggle_model': 'true', 'modal': modal})
                print(entry.course.subject_type)
                if not selected_course_instance.subject_type == "P":
                    if attendance.objects.filter(
                        semester = current_semester_plan,
                        course = entry.course,
                        date = attendance_date,
                        hour = selected_hour
                    ).exists():
                        msg = {
                            'page_title': 'Error',
                            'title': 'Already marked',
                            'description': 'This hour was already marked by the original subject staff',
                        }
                        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

                flexi_attendance.objects.get_or_create(
                    marked_course=selected_course_instance,
                    attendance_date=attendance_date,
                    attendance_hour=selected_hour,
                    original_course=entry.course,
                    semester=current_semester_plan,
                )
        else:
            flexi_attendance.objects.get_or_create(
                marked_course=selected_course_instance,
                attendance_date=attendance_date,
                attendance_hour=selected_hour,
                original_course=selected_course_instance,
                semester=current_semester_plan,
            )

        request.session['flexi_marking'] = True

        return redirect('attendance/' + str(
            current_semester_plan.pk) + '/' + selected_course_instance.course_id + '/' + selected_date + '/' + selected_hour)

    return render(request, 'flexi_attendance/flexi_attendance.html', {
        'course_allotment_list': course_allotment_list,
        'selected_allotment_pk': int(selected_allotment_pk),
        'selected_course_id': selected_course_instance.course_id,
    })


@login_required
@permission_required('curriculum.can_mark_attendance')
def view_flexi_marked_attendance(request):
    faculty_instance = staff.objects.get(user=request.user)
    course_allotment_list = get_current_course_allotment_with_display_text(faculty_instance)

    if request.method == 'POST' and request.POST.get('delete'):
        flexi_attendance_instance = flexi_attendance.objects.get(pk=request.POST.get('delete'))
        attendance.objects.filter(
            semester=flexi_attendance_instance.semester
        ).filter(
            course=flexi_attendance_instance.marked_course
        ).filter(
            date=flexi_attendance_instance.attendance_date
        ).filter(
            hour=flexi_attendance_instance.attendance_hour
        ).delete()
        flexi_attendance_instance.delete()

    list_of_flexi_attendance_instances = []

    for entry in course_allotment_list:
        course_instance = entry['staff_course_instance'].course
        current_semester_plan = entry['staff_course_instance'].semester

        flexi_attendance_query = flexi_attendance.objects.filter(
            semester=current_semester_plan
        ).filter(
            marked_course=course_instance
        ).filter(
            is_marked=True
        )

        for entry in flexi_attendance_query:
            list_of_flexi_attendance_instances.append(entry)

    return render(request, 'flexi_attendance/view_flexi_attendance.html', {
        'list_of_flexi_attendance_instances': list_of_flexi_attendance_instances
    })


@login_required
@permission_required('curriculum.can_mark_attendance')
def view_flexi_attendance_requests(request):
    faculty_instance = staff.objects.get(user=request.user)
    course_allotment_list = get_current_course_allotment_with_display_text(faculty_instance)

    if request.method == 'POST' and (request.POST.get('approve') or request.POST.get('delete')):
        if request.POST.get('approve'):
            flexi_attendance_instance = flexi_attendance.objects.get(pk=request.POST.get('approve'))
            flexi_attendance_instance.is_approved = True
            flexi_attendance_instance.save()
        elif request.POST.get('delete'):
            flexi_attendance_instance = flexi_attendance.objects.get(pk=request.POST.get('delete'))
            attendance.objects.filter(
                semester=flexi_attendance_instance.semester
            ).filter(
                course=flexi_attendance_instance.marked_course
            ).filter(
                date=flexi_attendance_instance.attendance_date
            ).filter(
                hour=flexi_attendance_instance.attendance_hour
            ).delete()
            flexi_attendance_instance.delete()

    list_of_flexi_attendance_instances = []
    for entry in course_allotment_list:
        course_instance = entry['staff_course_instance'].course
        current_semester_plan = entry['staff_course_instance'].semester

        flexi_attendance_query = flexi_attendance.objects.filter(
            semester=current_semester_plan
        ).filter(
            original_course=course_instance
        ).filter(
            is_marked=True
        )

        for entry in flexi_attendance_query:
            list_of_flexi_attendance_instances.append(entry)

    return render(request, 'flexi_attendance/view_flexi_attendance_requests.html', {
        'list_of_flexi_attendance_instances': list_of_flexi_attendance_instances
    })
