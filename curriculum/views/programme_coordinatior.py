from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import Group
from django.shortcuts import render

from accounts.models import staff
from curriculum.forms import assign_programme_coordinator_form
from curriculum.views.common_includes import staffBelongsToNonCore, get_first_year_managing_department, staffNotEligible, \
    get_department_programmes, get_eligible_department_programmes_for_hod, get_all_eligible_staffs
from curriculum.models import programme_coordinator


@login_required
@permission_required('curriculum.can_assign_programme_coordinator')
def assign_programme_coordinator(request):
    # get staff instance or basic sanity check
    staff_query = staff.objects.filter(user=request.user)
    if staff_query.exists():
        staff_instance = staff_query.get()
    else:
        return 403



    # santity check for first year
    if staffBelongsToNonCore(staff_instance):
        print(staffBelongsToNonCore(staff_instance))
        if staffNotEligible(staff_instance):
            msg = {
                'page_title': 'Access Denied',
                'title': 'No permission',
                'description': 'Your department does not have this permission. Contact ' + str(
                    get_first_year_managing_department()) + ' department for this settings',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    hod_object = staff.objects.get(user=request.user)
    all_department_programmes = get_eligible_department_programmes_for_hod(hod_object)
    selected_department_programme = all_department_programmes.first()
    if selected_department_programme is None:
        msg = {
            'page_title': 'No programmes',
            'title': 'No Programmes Found',
            'description': 'No programmes are found in your department'
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    # all_staffs = staff.objects.all().filter(department=selected_department_programme.department).filter(user__is_approved=True)
    # current_ug_pc_query = programme_coordinator.objects.filter(department=hod_object.department,programme="UG")
    all_staffs = get_all_eligible_staffs(hod_object.department)


    if request.method == "POST" and request.POST.get('selected_programme'):
        # pprint(request.POST.get('selected_programme'))
        selected_department_programme = all_department_programmes.get(pk=int(request.POST.get('selected_programme')))

    if request.method == "POST" and request.POST.getlist('select_pc'):
        selected_department_programme = all_department_programmes.get(pk=int(request.POST.get('selected_programme')))
        programme_coordinator_query = programme_coordinator.objects.filter(department_programme=selected_department_programme)

        pprint(request.POST.getlist('select_pc'))
        for entry in request.POST.getlist('select_pc'):
            staff_instance = staff.objects.get(pk=int(entry))
            if programme_coordinator_query.exists():
                programme_coordinator_inst = programme_coordinator_query.get()
                for staff_inst in programme_coordinator_inst.programme_coordinator_staffs.all():
                    programme_coordinator_inst.programme_coordinator_staffs.remove(staff_inst)
                programme_coordinator_inst.save()
                programme_coordinator_inst.programme_coordinator_staffs.add(staff_instance)
                programme_coordinator_inst.save()
            else:
                programme_coordinator_inst = programme_coordinator.objects.create(
                    department_programme=selected_department_programme
                )
                programme_coordinator_inst.programme_coordinator_staffs.add(staff_instance)
                programme_coordinator_inst.save()


    programme_coordinator_query = programme_coordinator.objects.filter(department_programme=selected_department_programme)
    list_of_pcs = None
    if programme_coordinator_query.exists():
        list_of_pcs = programme_coordinator_query.get().programme_coordinator_staffs.all()

    pprint(locals())
    return render(request, 'programme_coordinator/assign_programme_coordinator.html', {
        'all_department_programmes': all_department_programmes,
        'selected_department_programme': int(selected_department_programme.pk),
        'all_staffs': all_staffs,
        'list_of_pc': list_of_pcs
    })
