from datetime import timedelta, datetime

from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone

from accounts.models import student, staff
from common.utils.ReportTabsUtil import getActiveSemesterTabsForAllotment
from curriculum.models import semester, courses, forceTimetableSlot, attendance


def isAttendanceMarked(semesterInstance, dateInstance):
    if attendance.objects.filter(date=dateInstance):
        return True
    return False


def forceTimeTableSlot(request):
    staffInstance = staff.objects.get(user=request.user)
    duplicateDate = False
    isWeekend = False
    startTermInvalid = False
    startTermNotFound = False
    attendanceAlreadyMarked = False

    semesterList = getActiveSemesterTabsForAllotment(staffInstance)

    if not semesterList:
        msg = {
            'page_title': 'No Semester Plan',
            'title': 'No Semester Plan',
            'description': 'Create Semester Plan first, in order to create timetable',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    semesterInstance = semesterList[0]['semesterInstance']

    if request.method == 'POST':
        
        if 'addSlot' in request.POST:
            date = str(request.POST.get('date'))
            dateInstance = datetime.strptime(date, "%d/%m/%Y")
            slotNumber = int(request.POST.get('slotNumber'))
            elif dateInstance.weekday() >= 5:
                isWeekend = True
            elif isAttendanceMarked(semesterInstance, dateInstance.date()):
                attendanceAlreadyMarked = True
            else:
                if forceTimetableSlot.objects.filter(date=dateInstance):
                    duplicateDate = True
                else:
                    timetableSlotInstance = forceTimetableSlot(semester=semesterInstance, date=dateInstance,
                                                               slotNumber=slotNumber)
                    timetableSlotInstance.save()

    timetableSlot = forceTimetableSlot.objects.filter(semester=semesterInstance).order_by('date')

    return render(request, 'forceTimetableSlot.html',
                  {
                      'timetableSlot': timetableSlot,
                      'duplicateDate': duplicateDate,
                      'isWeekend': isWeekend,
                      'attendanceAlreadyMarked': attendanceAlreadyMarked
                  })