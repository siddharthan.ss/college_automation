from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, redirect

from accounts.models import student, staff
from curriculum.models import staff_course, department_sections, section_students
from curriculum.models import student_enrolled_courses, open_course_staff
from curriculum.views import courses

from common.utils.studentUtil import getSemesterPlanofStudent


@login_required
@permission_required('curriculum.can_choose_courses')
def select_courses(request):
    error = False
    student_obj = student.objects.get(user=request.user)
    instance = student_obj
    student_department = instance.department
    section_instance = section_students.objects.get(student=student_obj).section
    no_of_sections = department_sections.objects.filter(department=student_department, batch=student_obj.batch).count()
    semester_instance = getSemesterPlanofStudent(student_obj)
    if not semester_instance:
        msg = {
            'page_title': 'No Semester Plan',
            'title': 'No Semester Plan',
            'description': 'You can choose courses after the semester plan has created.',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
    current_semester = semester_instance.semester_number
    batch_start_year = instance.batch.get_start_year()

    if batch_start_year >= 2016:
        return redirect('/course_enrollment')
    # print(batch_start_year)

    select_tab = 'mycourse'

    if request.method == 'POST':
        if 'request_course_one_credit' in request.POST:
            requested_courses = request.POST.getlist('course')
            for each in requested_courses:
                staff_course_instance = staff_course.objects.get(pk=each)
                if staff_course_instance.staffs.count() > 0:
                    staff_instance = staff_course_instance.staffs.first()
                else:
                    staff_instance = staff.objects.all().first()
                course_instance = staff_course_instance.course
                try:
                    student_enrolled_courses.objects.create(
                        student=student_obj,
                        course=course_instance,
                        under_staff=staff_instance,
                        studied_semester=semester_instance,
                        request_course=True,
                        approved=False,
                        cleared=False,
                        registered=True
                    )
                except:
                    error = True
            select_tab = 'onecreditcourse'

        if 'request_course_elective' in request.POST:
            requested_courses = request.POST.getlist('course')
            for each in requested_courses:
                staff_course_instance = staff_course.objects.get(pk=each)
                if staff_course_instance.staffs.count() > 0:
                    staff_instance = staff_course_instance.staffs.first()
                else:
                    staff_instance = staff.objects.all().first()
                course_instance = staff_course_instance.course
                try:
                    student_enrolled_courses.objects.create(
                        student=student_obj,
                        course=course_instance,
                        under_staff=staff_instance,
                        studied_semester=semester_instance,
                        request_course=True,
                        approved=False,
                        cleared=False,
                        registered=True
                    )
                except:
                    error = True
            select_tab = 'electivecourse'

        if 'unrequest_course' in request.POST:
            unrequested_courses = request.POST.getlist('course')
            for each in unrequested_courses:
                staff_course_instance = staff_course.objects.get(pk=each)
                course_instance = staff_course_instance.course
                if student_enrolled_courses.objects.filter(student=student_obj, course=course_instance,
                                                           studied_semester=semester_instance, approved=False):
                    student_enrolled_courses.objects.filter(student=student_obj, course=course_instance,
                                                            studied_semester=semester_instance, approved=False).delete()
            select_tab = 'unrequestcourse'

    # print(current_semester)
    # print(select_tab)
    temp_elective_courses = []
    temp_open_courses = []
    temp_one_credit_courses = []
    elective_courses = []
    open_courses = []
    one_credit_courses = []
    requested_elective_courses = []
    requested_open_courses = []
    requested_one_credit_courses = []
    my_elective_courses = []
    my_open_courses = []
    my_one_credit_courses = []

    """one credit courses"""

    # for each in courses.objects.filter(semester=current_semester, programme=student_programme,
    #                                    department=student_department, is_one_credit=True):
    for each in staff_course.objects.filter(semester=semester_instance, course__is_one_credit=True):
        temp = {}
        temp['staff_course_pk'] = each.pk
        temp['course_code'] = each.course.course_id
        temp['requested'] = False
        temp['approved'] = False
        if student_enrolled_courses.objects.filter(student=student_obj, course__course_id=each.course.course_id,
                                                   studied_semester=semester_instance):
            for stu_instance in student_enrolled_courses.objects.filter(student=student_obj,
                                                                        course__course_id=each.course.course_id,
                                                                        studied_semester=semester_instance):
                if stu_instance.request_course:
                    temp['requested'] = True
                if stu_instance.approved:
                    temp['approved'] = True
        temp['course'] = each.course.course_name
        temp['staff'] = each.staffs.all()
        temp_one_credit_courses.append(temp)

    """elective courses"""

    for each in staff_course.objects.filter(semester=semester_instance, course__is_elective=True):
        temp = {}
        temp['staff_course_pk'] = each.pk
        temp['course_code'] = each.course.course_id
        temp['requested'] = False
        temp['approved'] = False
        if student_enrolled_courses.objects.filter(student=student_obj, course__course_id=each.course.course_id,
                                                   studied_semester=semester_instance):
            for stu_instance in student_enrolled_courses.objects.filter(student=student_obj,
                                                                        course__course_id=each.course.course_id,
                                                                        studied_semester=semester_instance):
                if stu_instance.request_course:
                    temp['requested'] = True
                if stu_instance.approved:
                    temp['approved'] = True
        temp['course'] = each.course.course_name
        temp['staff'] = each.staffs.all()
        temp_elective_courses.append(temp)
        # course_codes.append(each.course.course_id)

    for each in temp_one_credit_courses:
        if each['approved']:
            my_one_credit_courses.append(each)
        elif each['requested']:
            requested_one_credit_courses.append(each)
        else:
            one_credit_courses.append(each)
    # print(one_credit_courses)

    for each in temp_elective_courses:
        if each['approved']:
            my_elective_courses.append(each)
        elif each['requested']:
            requested_elective_courses.append(each)
        else:
            elective_courses.append(each)
    # print(elective_courses)

    requested_courses_count = len(requested_elective_courses) + len(requested_one_credit_courses) + len(
        requested_open_courses)
    one_credit_courses_count = len(one_credit_courses)
    elective_courses_count = len(elective_courses)
    # open_courses_count = len(open_courses)

    return render(request, 'open_elective_courses/select_courses.html', {
        'error': error,
        'one_credit_courses': one_credit_courses,
        'elective_courses': elective_courses,
        'open_courses': open_courses,
        'requested_one_credit_courses': requested_one_credit_courses,
        'requested_elective_courses': requested_elective_courses,
        'requested_open_courses': requested_open_courses,
        'my_one_credit_courses': my_one_credit_courses,
        'my_elective_courses': my_elective_courses,
        'my_open_courses': my_open_courses,
        'select_tab': select_tab,
        'requested_courses_count': requested_courses_count,
        'one_credit_courses_count': one_credit_courses_count,
        'elective_courses_count': elective_courses_count,
        'section_instance': section_instance,
        'no_of_sections': no_of_sections
        # 'open_courses_count': open_courses_count

    })


@login_required
@permission_required('curriculum.can_allot_open_courses_staffs')
def prepare_open_course_staffs(request):
    if request.method == 'POST':
        if 'assign' in request.POST:
            no_of_courses = int(request.POST.get('no_of_courses'))
            inc = 1
            while inc <= no_of_courses:
                staff_list = []
                staff_list = request.POST.getlist('select' + str(inc))
                course_pk = request.POST.get('course' + str(inc))
                short_code = request.POST.get('shortcode' + str(inc))
                # print(staff_list)
                course_instance = courses.objects.get(pk=course_pk)
                if open_course_staff.objects.filter(course=course_instance):
                    # print('deleting')
                    open_course_staff.objects.filter(course=course_instance).delete()

                if staff_list:
                    temp = open_course_staff(course=course_instance)
                    temp.save()
                    for each_staff in staff_list:
                        temp.staffs.add(each_staff)
                    temp.save()

                short_code = str(short_code).upper()
                courses.objects.filter(pk=course_pk).update(code=short_code)

                inc = inc + 1

    staff_instance = staff.objects.get(user=request.user)
    current_department = staff_instance.department
    list_of_courses = []
    list_of_staffs = []
    for each in courses.objects.filter(department=current_department, is_open=True):
        temp_list = {}
        temp_list['pk'] = each.pk
        temp_list['course_id'] = each.course_id
        staff_list = []
        if open_course_staff.objects.filter(course=each):
            for instance in open_course_staff.objects.filter(course=each):
                for each_staff in instance.staffs.all():
                    for id_instance in staff.objects.filter(staff_id=each_staff.staff_id):
                        staff_id = id_instance.id
                        staff_list.append(str(staff_id))
                # print(staff_list)
                temp_list['staff_list'] = staff_list
        temp_list['course_name'] = each.course_name
        temp_list['code'] = each.code
        list_of_courses.append(temp_list)

    no_of_courses = len(list_of_courses)

    for each in staff.objects.filter(user__is_approved=True).filter(department=current_department).exclude(
            designation='Network Admin'):
        temp = {}
        temp['desig'] = each.designation
        temp['staff_name'] = str(each)
        temp['id'] = str(each.id)
        list_of_staffs.append(temp)

    return render(request, 'open_elective_courses/allot_open_course_staff.html',
                  {
                      'no_of_courses': no_of_courses,
                      'list_of_courses': list_of_courses,
                      'list_of_staffs': list_of_staffs,
                  })