from datetime import timedelta, datetime

from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.http import JsonResponse
from django.shortcuts import render
from django.utils import timezone

from accounts.models import student, staff
from curriculum.models import semester, open_course_staff, courses, time_table, day, student_enrolled_courses, \
    staff_course
from enrollment.models import course_enrollment

from curriculum.views.common_includes import staffBelongsToNonCore, staffNotEligible, \
    get_first_year_managing_department, get_active_semesters_tabs, get_current_batches, \
    get_current_course_allotment_with_display_text, check_department_batch_has_multiple_sections

from common.utils.studentUtil import getSemesterPlanofStudent


def get_courses_for_timetable(department_instance, regulation_instance, batch_programme, semester_instance,
                              batch_instance):
    current_semester_number = semester_instance.semester_number
    list_of_courses = []

    # normal and elective courses
    for each in courses.objects.filter(department=department_instance, regulation=regulation_instance,
                                       programme=batch_programme, semester=current_semester_number,
                                       is_open=False, common_course=False):
        temp_list = {}
        temp_list['course_text'] = str(each)
        temp_list['pk'] = each.pk
        temp_list['course_id'] = each.course_id
        staff_list = []
        if each.is_elective:
            temp_list['course_type'] = '(Elective)'
        if each.is_one_credit:
            temp_list['course_type'] = '(One Credit)'
        if staff_course.objects.filter(course=each, batch=batch_instance, semester=semester_instance,
                                       department=department_instance):
            for instance in staff_course.objects.filter(course=each, batch=batch_instance,
                                                        semester=semester_instance,
                                                        department=department_instance):
                for each_staff in instance.staffs.all():
                    staff_list.append(str(each_staff))
                temp_list['staff_list'] = staff_list
        temp_list['course_name'] = each.course_name
        temp_list['code'] = each.code
        list_of_courses.append(temp_list)

    if semester_instance.open_courses and semester_instance.open_courses > 0:
        i = 1
        while i <= semester_instance.open_courses:
            courseInstance = courses.objects.get(course_id="OE" + str(i))
            temp_list = {}
            temp_list['course_text'] = str(courseInstance)
            temp_list['pk'] = courseInstance.pk
            temp_list['course_id'] = courseInstance.course_id
            temp_list['course_name'] = courseInstance.course_name
            temp_list['code'] = courseInstance.course_id
            list_of_courses.append(temp_list)
            i = i + 1

    # # one credit courses
    # for each in courses.objects.filter(department=department_instance, regulation=regulation_instance,
    #                                    programme=batch_programme, semester=current_semester_number,
    #                                    is_one_credit=True, common_course=False):
    #     temp_list = {}
    #     temp_list['course_text'] = str(each)
    #     temp_list['course_id'] = each.course_id
    #     temp_list['course_type'] = '(One Credit)'
    #     if staff_course.objects.filter(course=each, batch=batch_instance, semester=semester_instance,
    #                                    department=department_instance):
    #         for instance in staff_course.objects.filter(course=each, batch=batch_instance,
    #                                                     semester=semester_instance,
    #                                                     department=department_instance):
    #             for each_staff in instance.staffs.all():
    #                 for id_instance in staff.objects.filter(staff_id=each_staff.staff_id):
    #                     staff_list.append(str(id_instance))
    #             temp_list['staff_list'] = staff_list
    #     temp_list['course_name'] = each.course_name
    #     temp_list['code'] = each.code
    #     list_of_courses.append(temp_list)

    # open courses
    # for each in courses.objects.filter(regulation=regulation_instance,
    #                                    programme=batch_programme,
    #                                    is_open=True, common_course=False).exclude(department=department_instance):
    #     temp_list = {}
    #     temp_list['course_text'] = str(each)
    #     temp_list['pk'] = each.pk
    #     temp_list['course_id'] = each.course_id
    #     temp_list['course_type'] = '(Open)'
    #     staff_list = []
    #     for instance in open_course_staff.objects.filter(course=each):
    #         for each_staff in instance.staffs.all():
    #             for id_instance in staff.objects.filter(staff_id=each_staff.staff_id):
    #                 staff_id = id_instance.id
    #                 staff_list.append(str(id_instance))
    #     temp_list['staff_list'] = staff_list
    #     temp_list['course_name'] = each.course_name
    #     temp_list['code'] = each.code
    #     list_of_courses.append(temp_list)

    # common courses
    # for each in courses.objects.filter(common_course=True):
    #     temp_list = {}
    #     temp_list['common'] = True
    #     temp_list['pk'] = each.pk
    #     temp_list['course_text'] = str(each)
    #     temp_list['course_id'] = each.course_id
    #     temp_list['course_name'] = each.course_name
    #     temp_list['code'] = each.code
    #     list_of_courses.append(temp_list)

    return list_of_courses


@login_required
@permission_required('curriculum.can_create_semester_plan')
def create_timetable(request):
    date_fails_start_date = False
    date_fails_end_date = False
    duplicate_date = False
    date_today = timezone.now().date()
    user_instance = staff.objects.get(user=request.user)

    # santity check for first year
    if staffBelongsToNonCore(user_instance):
        print(staffBelongsToNonCore(user_instance))
        if staffNotEligible(user_instance):
            msg = {
                'page_title': 'Access Denied',
                'title': 'No permission',
                'description': 'Your department does not have this permission. Contact ' + str(
                    get_first_year_managing_department()) + ' department for this settings',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    available_semsters = get_active_semesters_tabs(request)

    if available_semsters:
        selected_semester_pk = available_semsters[0]['semester_instance'].pk
    else:
        msg = {
            'page_title': 'No Semester Plan',
            'title': 'No Semester Plan',
            'description': 'Create Semester Plan first, in order to create timetable',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    # active_batch_instance = active_batches.objects.filter(
    #     department=selected_semester_instance.department
    # ).get(
    #     batch=selected_semester_instance.batch
    # )

    if request.method == 'POST':
        if request.is_ajax():
            if 'add_course' in request.POST:
                selected_semester_pk = int(request.POST.get('selected_semester'))
                selected_semester_instance = semester.objects.get(id=selected_semester_pk)
                department_instance = selected_semester_instance.department
                selected_hour = request.POST.get('selected_hour')
                selected_day = request.POST.get('selected_day')
                day_instance = day.objects.get(day_name=selected_day)

                batch_instance = selected_semester_instance.batch
                regulation_instance = batch_instance.regulation
                batch_programme = batch_instance.programme

                dictionary = {}

                total_list_courses = get_courses_for_timetable(department_instance, regulation_instance,
                                                               batch_programme,
                                                               selected_semester_instance, batch_instance)

                time_table_data = time_table.objects.filter(department=department_instance, is_active=True,
                                                            batch=batch_instance, semester=selected_semester_instance,
                                                            day=day_instance, hour=selected_hour)

                already_added_courses = []
                for each in time_table_data:
                    already_added_courses.append(each.course.pk)

                list_of_courses = []
                for course in total_list_courses:
                    if course['pk'] not in already_added_courses:
                        list_of_courses.append(course)

                dictionary['list'] = list_of_courses

                return JsonResponse(dictionary)

            if 'remove_course' in request.POST:
                selected_semester_pk = int(request.POST.get('selected_semester'))
                selected_semester_instance = semester.objects.get(id=selected_semester_pk)
                department_instance = selected_semester_instance.department
                selected_hour = request.POST.get('selected_hour')
                selected_day = request.POST.get('selected_day')
                day_instance = day.objects.get(day_name=selected_day)

                batch_instance = selected_semester_instance.batch
                regulation_instance = batch_instance.regulation
                batch_programme = batch_instance.programme

                dictionary = {}

                total_list_courses = get_courses_for_timetable(department_instance, regulation_instance,
                                                               batch_programme,
                                                               selected_semester_instance, batch_instance)

                time_table_data = time_table.objects.filter(department=department_instance, is_active=True,
                                                            batch=batch_instance, semester=selected_semester_instance,
                                                            day=day_instance, hour=selected_hour)

                already_added_courses = []
                for each in time_table_data:
                    already_added_courses.append(each.course.pk)

                list_of_courses = []
                for course in total_list_courses:
                    if course['pk'] in already_added_courses:
                        list_of_courses.append(course)

                # pprint(list_of_courses)

                dictionary['list'] = list_of_courses

                return JsonResponse(dictionary)

        if 'selected_semester_tab' in request.POST:
            selected_semester_pk = request.POST['selected_semester_tab']

        if 'selected_course' in request.POST:
            selected_semester_pk = int(request.POST.get('selected_semester'))
            selected_semester_instance = semester.objects.get(id=selected_semester_pk)
            department_instance = selected_semester_instance.department
            selected_course = request.POST.get('selected_course')
            got_date = str(request.POST.get('selected_date'))
            selected_date = datetime.strptime(got_date, "%d/%m/%Y").strftime('%Y-%m-%d')
            selected_hour = request.POST.get('selected_hour')
            selected_day = request.POST.get('selected_day')
            batch_instance = selected_semester_instance.batch
            day_instance = day.objects.get(day_name=selected_day)

            course_instance = courses.objects.get(pk=selected_course)

            if not selected_semester_instance.start_term_1.strftime('%Y-%m-%d') > selected_date:
                if not time_table.objects.filter(department=department_instance, course=course_instance,
                                                 batch=batch_instance,
                                                 semester=selected_semester_instance, day=day_instance,
                                                 hour=selected_hour, is_active=True).exists():
                    if time_table.objects.filter(department=department_instance, course=course_instance,
                                                 batch=batch_instance,
                                                 semester=selected_semester_instance, day=day_instance,
                                                 hour=selected_hour):
                        timetable_instance = time_table.objects.get(department=department_instance,
                                                                    course=course_instance,
                                                                    batch=batch_instance,
                                                                    semester=selected_semester_instance,
                                                                    day=day_instance,
                                                                    hour=selected_hour)

                        created_date = datetime.strptime(str(timetable_instance.created_date), '%Y-%m-%d').strftime(
                            '%Y-%m-%d')
                        modified_date = datetime.strptime(str(timetable_instance.modified_date), '%Y-%m-%d').strftime(
                            '%Y-%m-%d')
                        if created_date <= selected_date < modified_date:
                            duplicate_date = True

                        else:
                            time_table_instance = time_table(department=department_instance, course=course_instance,
                                                             batch=batch_instance, semester=selected_semester_instance,
                                                             day=day_instance,
                                                             hour=selected_hour, is_active=True,
                                                             created_date=selected_date, modified_date=selected_date)
                            time_table_instance.save()

                    else:
                        time_table_instance = time_table(department=department_instance, course=course_instance,
                                                         batch=batch_instance, semester=selected_semester_instance,
                                                         day=day_instance,
                                                         hour=selected_hour, is_active=True, created_date=selected_date,
                                                         modified_date=selected_date)
                        time_table_instance.save()
                else:

                    timetable_instance = time_table.objects.get(department=department_instance, course=course_instance,
                                                                batch=batch_instance,
                                                                semester=selected_semester_instance, day=day_instance,
                                                                hour=selected_hour,
                                                                is_active=True)
                    date_from_table = timetable_instance.created_date
                    today_date = selected_date
                    if str(date_from_table) == str(today_date):
                        timetable_instance.delete()
                    else:
                        timetable_instance.is_active = False
                        created_date = datetime.strptime(str(timetable_instance.created_date), '%Y-%m-%d').strftime(
                            '%Y-%m-%d')
                        if created_date > selected_date:
                            date_fails_end_date = True
                        else:
                            timetable_instance.modified_date = selected_date
                            timetable_instance.save()

            else:
                date_fails_start_date = True

    selected_semester_instance = semester.objects.get(pk=selected_semester_pk)

    department_instance = selected_semester_instance.department

    if selected_semester_instance:
        if not selected_semester_instance.start_term_1:
            return render(request, 'timetable/create_timetable.html',
                          {
                              'list_of_semesters': available_semsters,
                              'selected_semester': int(selected_semester_pk),
                              # 'list_of_batches': list_of_batches,
                              # 'selected_batch': selected_batch,
                              'no_start_date': True
                          })
        current_semester_number = selected_semester_instance.semester_number
        batch_instance = selected_semester_instance.batch
        regulation_instance = batch_instance.regulation
        batch_programme = batch_instance.programme

        list_of_courses = get_courses_for_timetable(department_instance, regulation_instance, batch_programme,
                                                    selected_semester_instance, batch_instance)

        # regulation_start_year = batch_instance.regulation.start_year

        list_of_days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
        # if regulation_start_year >= 2016:
        available_hours = 8
        # else:
        #   available_hours = 7
        list_of_hours = []
        for each_day in list_of_days:
            day_list = []
            current_hour = 1
            while current_hour <= available_hours:
                hour_list = []
                for each_course_hour in time_table.objects.filter(department=department_instance, is_active=True,
                                                                  batch=batch_instance,
                                                                  semester=selected_semester_instance,
                                                                  day__day_name=each_day, hour=current_hour):
                    course_instance_code = courses.objects.get(pk=each_course_hour.course.pk)
                    if course_instance_code.code:
                        hour_list.append(course_instance_code.code)
                    else:
                        hour_list.append(each_course_hour.course)
                day_list.append(hour_list)
                current_hour = current_hour + 1
            temp = {}
            temp['day_name'] = each_day
            temp['hours'] = day_list
            list_of_hours.append(temp)

        faculty_advisors = selected_semester_instance.faculty_advisor.all()

        # for each in list_of_hours:
        #   print('next')
        #  for every in each:
        #     print(every)

        return render(request, 'timetable/create_timetable.html',
                      {
                          'list_of_semesters': available_semsters,
                          'selected_semester': int(selected_semester_pk),
                          # 'list_of_batches': list_of_batches,
                          # 'selected_batch': selected_batch,
                          'list_of_courses': list_of_courses,
                          'no_of_courses': len(list_of_courses),
                          'list_of_hours': list_of_hours,
                          'faculty_advisors': faculty_advisors,
                          'date_today': date_today,
                          'date_fails_start_date': date_fails_start_date,
                          'date_fails_end_date': date_fails_end_date,
                          'duplicate_date': duplicate_date
                          # 'regulation_start_year': regulation_start_year
                      })

    return render(request, 'timetable/create_timetable.html',
                  {
                      'list_of_semesters': available_semsters,
                      'selected_semester': int(selected_semester_pk),
                      # 'list_of_batches': list_of_batches,
                      # 'selected_batch': selected_batch,
                      'no_faculty_advisor': True,
                      'date_today': date_today
                  })


@login_required
@permission_required('curriculum.can_create_semester_plan')
def timetable_revision(request, semester_pk):
    try:
        semester_instance = semester.objects.get(pk=semester_pk)
    except:
        msg = {
            'page_title': 'Access Denied',
            'title': 'No permission',
            'description': 'You are not allowed to this url.',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'get_timetable_of_hour' in request.POST:
            selected_day = request.POST.get('day')
            selected_hour = request.POST.get('hour')
            day_instance = day.objects.get(day_name=selected_day)
            timetable_list = []
            for entry in time_table.objects.filter(semester=semester_instance, day=day_instance,
                                                   hour=selected_hour).order_by('created_date'):
                temp = {}
                temp['id'] = entry.id
                temp['is_active'] = entry.is_active
                temp['course'] = entry.course
                temp['created_date'] = entry.created_date
                got_date = str(entry.modified_date)
                modified_date = datetime.strptime(got_date, "%Y-%m-%d")
                temp['modified_date'] = modified_date - timedelta(1)
                timetable_list.append(temp)

            batch_instance = semester_instance.batch
            regulation_instance = batch_instance.regulation
            batch_programme = batch_instance.programme
            department_instance = semester_instance.department

            list_of_courses = get_courses_for_timetable(department_instance, regulation_instance, batch_programme,
                                                        semester_instance, batch_instance)

            return render(request, 'timetable/revision.html',
                          {
                              'list_of_courses': list_of_courses,
                              'selected_hour': selected_hour,
                              'selected_day': selected_day,
                              'timetable_list': timetable_list,
                              'semester_instance': semester_instance
                          })

        if 'delete_id' in request.POST:
            delete_id = request.POST.get('delete_id')
            selected_day = request.POST.get('hidden_day')
            selected_hour = request.POST.get('hidden_hour')
            day_instance = day.objects.get(day_name=selected_day)

            time_table_instance = time_table.objects.get(id=delete_id)

            time_table.objects.get(id=delete_id).delete()

            timetable_list = []

            for entry in time_table.objects.filter(semester=semester_instance, day=day_instance,
                                                   hour=selected_hour).order_by('created_date'):
                temp = {}
                temp['id'] = entry.id
                temp['is_active'] = entry.is_active
                temp['course'] = entry.course
                temp['created_date'] = entry.created_date
                got_date = str(entry.modified_date)
                modified_date = datetime.strptime(got_date, "%Y-%m-%d")
                temp['modified_date'] = modified_date - timedelta(1)
                timetable_list.append(temp)

            batch_instance = semester_instance.batch
            regulation_instance = batch_instance.regulation
            batch_programme = batch_instance.programme
            department_instance = semester_instance.department

            list_of_courses = get_courses_for_timetable(department_instance, regulation_instance, batch_programme,
                                                        semester_instance, batch_instance)

            return render(request, 'timetable/revision.html',
                          {
                              'deleted': True,
                              'time_table_instance': time_table_instance,
                              'list_of_courses': list_of_courses,
                              'selected_hour': selected_hour,
                              'selected_day': selected_day,
                              'timetable_list': timetable_list,
                              'semester_instance': semester_instance
                          })

        if 'add_revision' in request.POST:
            selected_day = request.POST.get('hidden_day')
            selected_hour = request.POST.get('hidden_hour')
            day_instance = day.objects.get(day_name=selected_day)
            course_pk = request.POST.get('course')
            got_start_date = str(request.POST.get('start_date'))
            got_end_date = str(request.POST.get('end_date'))
            got_start_date = datetime.strptime(got_start_date, "%d/%m/%Y")
            got_end_date = datetime.strptime(got_end_date, "%d/%m/%Y")

            # push_end_date = datetime.strptime(str(got_end_date), "%Y-%m-%d").strftime('%Y-%m-%d')
            push_end_date = got_end_date + timedelta(1)

            course_instance = courses.objects.get(pk=course_pk)

            timetable_list = time_table.objects.filter(semester=semester_instance, day=day_instance, hour=selected_hour,
                                                       course=course_instance)

            batch_instance = semester_instance.batch
            regulation_instance = batch_instance.regulation
            batch_programme = batch_instance.programme
            department_instance = semester_instance.department

            list_of_courses = get_courses_for_timetable(department_instance, regulation_instance, batch_programme,
                                                        semester_instance, batch_instance)

            if got_start_date > got_end_date:
                timetable_list = []
                for entry in time_table.objects.filter(semester=semester_instance, day=day_instance,
                                                       hour=selected_hour).order_by('created_date'):
                    temp = {}
                    temp['id'] = entry.id
                    temp['is_active'] = entry.is_active
                    temp['course'] = entry.course
                    temp['created_date'] = entry.created_date
                    got_date = str(entry.modified_date)
                    modified_date = datetime.strptime(got_date, "%Y-%m-%d")
                    temp['modified_date'] = modified_date - timedelta(1)
                    timetable_list.append(temp)

                return render(request, 'timetable/revision.html',
                              {
                                  'date_error': True,
                                  'list_of_courses': list_of_courses,
                                  'selected_hour': selected_hour,
                                  'selected_day': selected_day,
                                  'timetable_list': timetable_list,
                                  'semester_instance': semester_instance
                              })
            if semester_instance.start_term_1.strftime('%Y-%m-%d') > got_start_date.strftime('%Y-%m-%d'):
                timetable_list = []
                for entry in time_table.objects.filter(semester=semester_instance, day=day_instance,
                                                       hour=selected_hour).order_by('created_date'):
                    temp = {}
                    temp['id'] = entry.id
                    temp['is_active'] = entry.is_active
                    temp['course'] = entry.course
                    temp['created_date'] = entry.created_date
                    got_date = str(entry.modified_date)
                    modified_date = datetime.strptime(got_date, "%Y-%m-%d")
                    temp['modified_date'] = modified_date - timedelta(1)
                    timetable_list.append(temp)

                return render(request, 'timetable/revision.html',
                              {
                                  'start_date_fails': True,
                                  'list_of_courses': list_of_courses,
                                  'selected_hour': selected_hour,
                                  'selected_day': selected_day,
                                  'timetable_list': timetable_list,
                                  'semester_instance': semester_instance
                              })

            if got_start_date.strftime('%Y-%m-%d') >= timezone.now().strftime('%Y-%m-%d'):
                timetable_list = []
                for entry in time_table.objects.filter(semester=semester_instance, day=day_instance,
                                                       hour=selected_hour).order_by('created_date'):
                    temp = {}
                    temp['id'] = entry.id
                    temp['is_active'] = entry.is_active
                    temp['course'] = entry.course
                    temp['created_date'] = entry.created_date
                    got_date = str(entry.modified_date)
                    modified_date = datetime.strptime(got_date, "%Y-%m-%d")
                    temp['modified_date'] = modified_date - timedelta(1)
                    timetable_list.append(temp)

                return render(request, 'timetable/revision.html',
                              {
                                  'future_date': True,
                                  'list_of_courses': list_of_courses,
                                  'selected_hour': selected_hour,
                                  'selected_day': selected_day,
                                  'timetable_list': timetable_list,
                                  'semester_instance': semester_instance
                              })

            crash = False

            for entry in timetable_list:
                start_date = datetime.strptime(str(entry.created_date), "%Y-%m-%d")
                take_date = datetime.strptime(str(entry.modified_date), "%Y-%m-%d")
                end_date = take_date - timedelta(1)

                if entry.is_active:
                    if start_date == got_start_date:
                        crash = True

                if start_date <= got_start_date <= end_date:
                    crash = True

                if start_date <= got_end_date <= end_date:
                    crash = True

                if crash:
                    break

            if not crash:
                time_table_instance = time_table.objects.create(department=department_instance, course=course_instance,
                                                                batch=batch_instance,
                                                                semester=semester_instance, day=day_instance,
                                                                hour=selected_hour, is_active=False,
                                                                created_date=got_start_date,
                                                                modified_date=push_end_date)

                timetable_list = []

                for entry in time_table.objects.filter(semester=semester_instance, day=day_instance,
                                                       hour=selected_hour).order_by('created_date'):
                    temp = {}
                    temp['id'] = entry.id
                    temp['is_active'] = entry.is_active
                    temp['course'] = entry.course
                    temp['created_date'] = entry.created_date
                    got_date = str(entry.modified_date)
                    modified_date = datetime.strptime(got_date, "%Y-%m-%d")
                    temp['modified_date'] = modified_date - timedelta(1)
                    timetable_list.append(temp)

                return render(request, 'timetable/revision.html',
                              {
                                  'date_started': got_start_date,
                                  'date_ended': push_end_date - timedelta(1),
                                  'crash': crash,
                                  'time_table_instance': time_table_instance,
                                  'list_of_courses': list_of_courses,
                                  'selected_hour': selected_hour,
                                  'selected_day': selected_day,
                                  'timetable_list': timetable_list,
                                  'semester_instance': semester_instance
                              })

            timetable_list = []

            for entry in time_table.objects.filter(semester=semester_instance, day=day_instance,
                                                   hour=selected_hour).order_by('created_date'):
                temp = {}
                temp['id'] = entry.id
                temp['is_active'] = entry.is_active
                temp['course'] = entry.course
                temp['created_date'] = entry.created_date
                got_date = str(entry.modified_date)
                modified_date = datetime.strptime(got_date, "%Y-%m-%d")
                temp['modified_date'] = modified_date - timedelta(1)
                timetable_list.append(temp)

            return render(request, 'timetable/revision.html',
                          {
                              'crash': crash,
                              'list_of_courses': list_of_courses,
                              'selected_hour': selected_hour,
                              'selected_day': selected_day,
                              'timetable_list': timetable_list,
                              'semester_instance': semester_instance
                          })

    return render(request, 'timetable/revision.html',
                  {
                      'semester_instance': semester_instance
                  })


def can_view(user):
    if user.has_perm('curriculum.can_view_timetable') or \
            user.has_perm('curriculum.can_view_timetable_of_his_batch'):
        return True


def view_timetable_get_courses_function(department_instance, regulation_instance, batch_programme, semester_instance,
                                        batch_instance, current_semester_number):
    list_of_courses = []
    for each in courses.objects.filter(department=department_instance, regulation=regulation_instance,
                                       programme=batch_programme, semester=current_semester_number,
                                       is_open=False, is_one_credit=False):
        temp_list = {}
        temp_list['course_text'] = each
        temp_list['pk'] = each.pk
        temp_list['course_id'] = each.course_id
        staff_list = []
        if staff_course.objects.filter(course=each, batch=batch_instance,
                                       semester=semester_instance,
                                       department=department_instance):
            for instance in staff_course.objects.filter(course=each, batch=batch_instance,
                                                        semester=semester_instance,
                                                        department=department_instance):
                for each_staff in instance.staffs.all():
                    staff_list.append(str(each_staff))
                temp_list['staff_list'] = staff_list
        temp_list['course_name'] = each.course_name
        temp_list['code'] = each.code
        list_of_courses.append(temp_list)
    for each in courses.objects.filter(department=department_instance, regulation=regulation_instance,
                                       programme=batch_programme, semester=current_semester_number,
                                       is_one_credit=True):
        temp_list = {}
        temp_list['course_text'] = each
        temp_list['pk'] = each.pk
        temp_list['course_id'] = each.course_id
        staff_list = []
        if staff_course.objects.filter(course=each, batch=batch_instance,
                                       semester=semester_instance,
                                       department=department_instance):
            for instance in staff_course.objects.filter(course=each, batch=batch_instance,
                                                        semester=semester_instance,
                                                        department=department_instance):
                for each_staff in instance.staffs.all():
                    staff_list.append(str(each_staff))
                temp_list['staff_list'] = staff_list
        temp_list['course_name'] = each.course_name
        temp_list['code'] = each.code
        list_of_courses.append(temp_list)

    return list_of_courses


def view_timetable_get_hours_function(batch_instance, department_instance, semester_instance):
    list_of_days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
    # if regulation_start_year >= 2016:
    available_hours = 8
    # else:
    #   available_hours = 7
    list_of_hours = []
    for each_day in list_of_days:
        day_list = []
        current_hour = 1
        while current_hour <= available_hours:
            hour_list = []
            for each_course_hour in time_table.objects.filter(department=department_instance, is_active=True,
                                                              batch=batch_instance, semester=semester_instance,
                                                              day__day_name=each_day, hour=current_hour):
                course_instance_code = courses.objects.get(pk=each_course_hour.course.pk)
                if course_instance_code.code:
                    hour_list.append(course_instance_code.code)
                else:
                    hour_list.append(each_course_hour.course)
            day_list.append(hour_list)
            current_hour = current_hour + 1
        temp = {}
        temp['day_name'] = each_day
        temp['hours'] = day_list
        list_of_hours.append(temp)

    return list_of_hours


@login_required
@user_passes_test(can_view)
def view_timetable(request):
    if request.user.groups.filter(name='faculty').exists():
        user_instance = staff.objects.get(user=request.user)

        # # santity check for first year
        # if staffBelongsToNonCore(user_instance):
        #     print(staffBelongsToNonCore(user_instance))
        #     # if staffNotEligible(user_instance):
        #     #     msg = {
        #     #         'page_title': 'Access Denied',
        #     #         'title': 'No permission',
        #     #         'description': 'Your department does not have this permission. Contact ' + str(
        #     #             get_first_year_managing_department()) + ' department for this settings',
        #     #     }
        #     #     return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
        #     return HttpResponseRedirect('/view_timetable_first_year')

        available_semsters = get_active_semesters_tabs(request)

        if available_semsters:
            selected_semester_pk = available_semsters[0]['semester_instance'].pk
        else:
            msg = {
                'page_title': 'No Semester Plan',
                'title': 'No Semester Plan',
                'description': 'Semester Plan is not yet created, contact your programme coordinator for more information.',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

        if request.method == 'POST':
            if 'selected_semester_tab' in request.POST:
                selected_semester_pk = request.POST['selected_semester_tab']

        selected_semester_instance = semester.objects.get(pk=selected_semester_pk)

        department_instance = selected_semester_instance.department

        if selected_semester_instance:
            current_semester_number = selected_semester_instance.semester_number
            batch_instance = selected_semester_instance.batch
            regulation_instance = batch_instance.regulation
            batch_programme = batch_instance.programme

            list_of_courses = view_timetable_get_courses_function(department_instance, regulation_instance,
                                                                  batch_programme, selected_semester_instance,
                                                                  batch_instance, current_semester_number)

            list_of_hours = view_timetable_get_hours_function(batch_instance, department_instance,
                                                              selected_semester_instance)

            faculty_advisors = selected_semester_instance.faculty_advisor.all()

            return render(request, 'timetable/view_timetable.html',
                          {
                              'list_of_semesters': available_semsters,
                              'selected_semester': int(selected_semester_pk),
                              'list_of_courses': list_of_courses,
                              'no_of_courses': len(list_of_courses),
                              'list_of_hours': list_of_hours,
                              'faculty_advisors': faculty_advisors,
                          })

        return render(request, 'timetable/view_timetable.html',
                      {
                          'list_of_semesters': available_semsters,
                          'selected_semester': int(selected_semester_pk),
                          'no_faculty_advisor': True
                      })
    elif request.user.groups.filter(name='student').exists():
        student_instance = student.objects.get(user=request.user)
        department_instance = student_instance.department
        batch_instance = student_instance.batch

        list_of_batches = get_current_batches(department_instance)

        semester_instance = getSemesterPlanofStudent(student_instance)
        if semester_instance:
            current_semester_number = semester_instance.semester_number
            regulation_instance = batch_instance.regulation
            batch_programme = batch_instance.programme

            list_of_courses = []

            for each in courses.objects.filter(department=department_instance, regulation=regulation_instance,
                                               programme=batch_programme, semester=current_semester_number,
                                               is_open=False, common_course=False):
                if each.is_elective and regulation_instance.start_year <= 2012:
                    if student_enrolled_courses.objects.filter(course=each, student=student_instance, approved=True):
                        temp_list = {}
                        temp_list['course_text'] = each
                        temp_list['course_id'] = each.course_id
                        if staff_course.objects.filter(course=each, batch=batch_instance,
                                                       semester=semester_instance,
                                                       department=department_instance):
                            staff_course_instance = staff_course.objects.get(course=each, batch=batch_instance,
                                                                             semester=semester_instance,
                                                                             department=department_instance)

                            temp_list['staff_list'] = staff_course_instance.staffs.all()
                        temp_list['course_name'] = each.course_name
                        temp_list['code'] = each.code
                        list_of_courses.append(temp_list)
                elif regulation_instance.start_year <= 2012:
                    temp_list = {}
                    temp_list['course_text'] = each
                    temp_list['course_id'] = each.course_id
                    if staff_course.objects.filter(course=each, batch=batch_instance,
                                                   semester=semester_instance,
                                                   department=department_instance):
                        staff_course_instance = staff_course.objects.get(course=each, batch=batch_instance,
                                                                         semester=semester_instance,
                                                                         department=department_instance)

                        temp_list['staff_list'] = staff_course_instance.staffs.all()
                    temp_list['course_name'] = each.course_name
                    temp_list['code'] = each.code
                    list_of_courses.append(temp_list)
                else:
                    if course_enrollment.objects.filter(course=each, semester=semester_instance,
                                                        student=student_instance, registered=True):
                        temp_list = {}
                        temp_list['course_text'] = each
                        temp_list['course_id'] = each.course_id
                        if staff_course.objects.filter(course=each, batch=batch_instance,
                                                       semester=semester_instance,
                                                       department=department_instance):
                            staff_course_instance = staff_course.objects.get(course=each, batch=batch_instance,
                                                                             semester=semester_instance,
                                                                             department=department_instance)

                            temp_list['staff_list'] = staff_course_instance.staffs.all()
                        temp_list['course_name'] = each.course_name
                        temp_list['code'] = each.code
                        list_of_courses.append(temp_list)

            for each in courses.objects.filter(regulation=regulation_instance,
                                               programme=batch_programme, is_open=True, common_course=False).exclude(
                department=department_instance):
                if course_enrollment.objects.filter(course=each, semester=semester_instance,
                                                    student=student_instance, registered=True):
                    temp_list = {}
                    temp_list['course_text'] = each
                    temp_list['course_id'] = each.course_id
                    if staff_course.objects.filter(course=each, batch=batch_instance,
                                                   semester=semester_instance,
                                                   department=department_instance):
                        staff_course_instance = staff_course.objects.get(course=each, batch=batch_instance,
                                                                         semester=semester_instance,
                                                                         department=department_instance)

                        temp_list['staff_list'] = staff_course_instance.staffs.all()
                    temp_list['course_name'] = each.course_name
                    temp_list['code'] = each.code
                    list_of_courses.append(temp_list)

            list_of_days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
            available_hours = 8
            list_of_hours = []
            for each_day in list_of_days:
                day_list = []
                current_hour = 1
                while current_hour <= available_hours:
                    hour_list = []
                    for each_course_hour in time_table.objects.filter(department=department_instance, is_active=True,
                                                                      batch=batch_instance, semester=semester_instance,
                                                                      day__day_name=each_day, hour=current_hour):
                        course_instance = each_course_hour.course
                        if course_instance.isOpenElective:
                            if course_instance.code:
                                hour_list.append(course_instance.code)
                            else:
                                hour_list.append(course_instance)

                        elif course_instance.regulation.start_year <= 2012:
                            if course_instance.is_elective or course_instance.is_open or course_instance.is_one_credit:
                                if student_enrolled_courses.objects.filter(course=course_instance,
                                                                           student=student_instance, approved=True):
                                    if course_instance.code:
                                        hour_list.append(course_instance.code)
                                    else:
                                        hour_list.append(course_instance)
                            else:
                                if course_instance.code:
                                    hour_list.append(course_instance.code)
                                else:
                                    hour_list.append(course_instance)

                        else:
                            if course_enrollment.objects.filter(course=course_instance, semester=semester_instance,
                                                                student=student_instance, registered=True):
                                if course_instance.code:
                                    hour_list.append(course_instance.code)
                                else:
                                    hour_list.append(course_instance)

                    day_list.append(hour_list)
                    current_hour = current_hour + 1
                temp = {}
                temp['day_name'] = each_day
                temp['hours'] = day_list
                list_of_hours.append(temp)

            faculty_advisors = semester_instance.faculty_advisor.all()

            return render(request, 'timetable/view_timetable.html',
                          {
                              'list_of_batches': list_of_batches,
                              'selected_batch': batch_instance,
                              'list_of_courses': list_of_courses,
                              'no_of_courses': len(list_of_courses),
                              'list_of_hours': list_of_hours,
                              'faculty_advisors': faculty_advisors,
                          })

        return render(request, 'timetable/view_timetable.html',
                      {
                          'list_of_batches': list_of_batches,
                          'selected_batch': batch_instance,
                          'no_faculty_advisor': True
                      })


@login_required
@permission_required('curriculum.can_view_my_timetable')
def view_my_timetable(request):
    staff_instance = staff.objects.get(user=request.user)
    # department_instance = staff_instance.department
    list_of_staff_handling_courses = get_current_course_allotment_with_display_text(staff_instance)

    list_of_courses = []
    list_of_active_semesters = []

    for course in list_of_staff_handling_courses:
        list_of_courses.append(course['staff_course_instance'].course)

    for course in list_of_staff_handling_courses:
        list_of_active_semesters.append(course['staff_course_instance'].semester)

    list_of_hours = view_my_timetable_function(list_of_courses, list_of_active_semesters)

    # pprint(list_of_staff_handling_courses)

    return render(request, 'timetable/view_my_timetable.html',
                  {
                      'list_of_hours': list_of_hours,
                      'list_of_courses': list_of_staff_handling_courses,
                      # 'new_regulation_avail': new_regulation_avail
                  })


def view_my_timetable_function(list_of_staff_handling_courses, list_of_active_semesters):
    # new_regulation_avail = False

    # for each in list_of_staff_handling_courses:
    #     if each.regulation.start_year >= 2016:
    #         new_regulation_avail = True
    #         break

    list_of_days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
    # if new_regulation_avail:
    available_hours = 8
    # else:
    #     available_hours = 7
    list_of_hours = []
    for each_day in list_of_days:
        day_list = []
        current_hour = 1
        while current_hour <= available_hours:
            hour_list = []
            for each_course_hour in time_table.objects.filter(is_active=True, semester__in=list_of_active_semesters,
                                                              day__day_name=each_day, hour=current_hour):
                if each_course_hour.course in list_of_staff_handling_courses:
                    if check_department_batch_has_multiple_sections(department_instance=each_course_hour.department,
                                                                    batch_instance=each_course_hour.batch):
                        section_string = '(' + str(each_course_hour.department.acronym) + '-B)'
                    else:
                        section_string = '(' + str(each_course_hour.department.acronym) + ')'
                    course_instance_code = courses.objects.get(pk=each_course_hour.course.pk)
                    if course_instance_code.code:
                        hour_list.append(course_instance_code.code)
                    else:
                        hour_list.append(each_course_hour.course)
            day_list.append(hour_list)
            current_hour = current_hour + 1
        temp = {}
        temp['day_name'] = each_day
        temp['hours'] = day_list
        list_of_hours.append(temp)

    return list_of_hours
