from datetime import timedelta

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render
from django.utils import timezone

from accounts.models import staff
from curriculum.models import reallow_attendance_requests, courses, attendance, semester
from curriculum.views.common_includes import *


def is_hod_of_department(user):
    staff_instance = staff.objects.filter(user=user)
    if staff_instance.exists():
        if user.groups.filter(name='hod').exists():
            return True

    return False


@login_required
@permission_required('curriculum.can_reallow_attendance')
def reallow_attendance_request(request):
    if request.method == "POST":
        selected_requests = request.POST.getlist('selected_requests')
        for req in selected_requests:
            reallow_inst = reallow_attendance_requests.objects.get(pk=req)
            attendance_inst = attendance.objects.filter(
                course=reallow_inst.course,
                date=reallow_inst.date,
                hour=reallow_inst.hour,
                semester=reallow_inst.semester,
            )
            if attendance_inst.exists():
                attendance_inst = attendance_inst.get()
                attendance_inst.granted_edit_access = True
                attendance_inst.grant_period = datetime.now() + timedelta(hours=6)
                attendance_inst.save()
            else:
                attendance_ins = attendance.objects.create(
                    course=reallow_inst.course,
                    date=reallow_inst.date,
                    hour=reallow_inst.hour,
                    staff=reallow_inst.staff,
                    semester=reallow_inst.semester,
                    granted_edit_access=True,
                    grant_period=datetime.now() + timedelta(hours=6),
                )
            reallow_inst.is_allowed = True
            reallow_inst.save()


    list_of_departments = get_departments_of_hod(staff.objects.get(user= request.user))

    # superuser will get all department requests
    if request.user.is_superuser:
        list_of_departments = department.objects.filter(is_core=True)

    list_of_entries = []

    for dept_instance in list_of_departments:
        active_semesters_this_sem = get_current_active_semester_of_this_dept(dept_instance)
        reallow_requests_query = reallow_attendance_requests.objects.filter(
            semester__in=active_semesters_this_sem
        ).filter(
            is_allowed=False
        ).order_by(
            'date', 'hour'
        )
        print(reallow_requests_query)


        for request_entry in reallow_requests_query:
            temp = {}
            temp['staff_instance'] = request_entry.staff
            temp['date'] = request_entry.date
            temp['hour'] = request_entry.hour
            temp['course'] = request_entry.course
            temp['reallow_inst_id'] = request_entry.pk

            list_of_entries.append(temp)

    if is_staff_first_year_chief(staff.objects.get(user= request.user)):
        active_semester_migrations_this_sem = semester_migration.objects.filter(semester__batch__programme='UG').filter(
                semester__semester_number__lte=2)
        active_semesters_this_sem = []
        for entry in active_semester_migrations_this_sem:
            active_semesters_this_sem.append(entry.semester)
        reallow_requests_query = reallow_attendance_requests.objects.filter(
            semester__in=active_semesters_this_sem
        ).filter(
            is_allowed=False
        ).order_by(
            'date', 'hour'
        )
        print(reallow_requests_query)

        for request_entry in reallow_requests_query:
            temp = {}
            temp['staff_instance'] = request_entry.staff
            temp['date'] = request_entry.date
            temp['hour'] = request_entry.hour
            temp['course'] = request_entry.course
            temp['reallow_inst_id'] = request_entry.pk

            list_of_entries.append(temp)

    return render(request, 'reallow_attendance/reallow_requests.html', {'list_of_requests': list_of_entries})


@login_required
def request_reallow_already_marked(request, semester_pk, course_id, attendance_date, hour):
    print(locals())
    staff_inst = staff.objects.get(user=request.user)
    course_inst = courses.objects.get(course_id=course_id)
    semester_inst = semester.objects.get(pk=semester_pk)
    reallow_attendance_requests.objects.get_or_create(
        staff=staff_inst,
        course=course_inst,
        hour=hour,
        date=attendance_date,
        semester=semester_inst,
        is_allowed=False,
    )
    modal = {
        'heading': 'Success',
        'body': 'Reallow attenance marking request successfully submitted to HOD.Please inform him personally to approve your request',
    }
    return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})


@login_required
def request_bulk_reallow_already_marked(request,semester_pk, course_id, attendance_date, start_hour, end_hour):
    print(locals())
    staff_inst = staff.objects.get(user=request.user)
    course_inst = courses.objects.get(course_id=course_id)
    semester_inst = semester.objects.get(pk=semester_pk)
    start_hour = int(start_hour)
    end_hour = int(end_hour)
    hour = start_hour
    while hour <= end_hour:
        if not reallow_attendance_requests.objects.filter(
            course=course_inst,
            hour=hour,
            date=attendance_date,
            semester=semester_inst,
            is_allowed=False,
        ).exists():
            reallow_attendance_requests.objects.get_or_create(
                staff=staff_inst,
                course=course_inst,
                hour=hour,
                date=attendance_date,
                semester=semester_inst,
                is_allowed=False,
            )
        hour += 1
    modal = {
        'heading': 'Success',
        'body': 'Reallow attenance marking request successfully submitted to HOD.Please inform him personally to approve your request',
    }
    return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})


@login_required
@permission_required('curriculum.can_reallow_attendance')
def instant_reallow_attendance(request):
    if request.is_ajax():
        semester_pk = request.POST.get("semester_pk")
        course_id = request.POST.get("course_pk")
        attendance_date = request.POST.get("date")
        hour = request.POST.get("hour")

        attendance_query = attendance.objects.filter(
            semester=int(semester_pk)
        ).filter(
            course_id=course_id
        ).filter(
            date=datetime.strptime(attendance_date, "%Y-%m-%d")
        ).filter(
            hour=int(hour)
        )

        pprint(locals())

        if attendance_query.exists():
            attendance_instance = attendance_query.get()
            attendance_instance.grant_period = timezone.now() + timedelta(hours=6)
            attendance_instance.granted_edit_access = True
            attendance_instance.save()

            modal = {
                'heading': 'Success',
                'body': 'Reallowed attendance for this hour successfully',
            }
            dictionary = {}
            dictionary['modal'] = modal
            return JsonResponse(dictionary)

    return render(request, '404.html')


@login_required
@permission_required('curriculum.can_mark_attendance')
def instant_delete_attendance(request):
    if request.is_ajax():
        semester_pk = request.POST.get("semester_pk")
        course_id = request.POST.get("course_pk")
        attendance_date = request.POST.get("date")
        hour = request.POST.get("hour")

        attendance_query = attendance.objects.filter(
            semester=int(semester_pk)
        ).filter(
            course_id=course_id
        ).filter(
            date=datetime.strptime(attendance_date, "%Y-%m-%d")
        ).filter(
            hour=int(hour)
        )

        pprint(locals())

        if attendance_query.exists():
            attendance_instance = attendance_query.get()
            attendance_instance.delete()

            modal = {
                'heading': 'Success',
                'body': 'Attendance deleted successfully',
            }
            dictionary = {}
            dictionary['modal'] = modal
            return JsonResponse(dictionary)

    return render(request, '404.html')
