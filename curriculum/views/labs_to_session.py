from curriculum.views.common_includes import *


def add_labs_to_session(request):
    staff_query = staff.objects.filter(user=request.user)
    if not staff_query.exists():
        return None
    else:
        staff_instance = staff_query.get()
    allot_staff_instances = get_staff_course_allotment_instances_for_staff(staff_instance)
    list_of_courses_handled_by_faculty = []

    for entry in allot_staff_instances:
        list_of_courses_handled_by_faculty.append(entry.course)

    list_of_labs = []
    for course in list_of_courses_handled_by_faculty:
        # print(vars(course))
        # print(course.subject_type)
        if course.subject_type == 'P':
            list_of_labs.append(course)

    # print('list of labs')
    # pprint(list_of_labs)

    final_list_of_labs = []

    for obj in list_of_labs:
        course_list = {}
        course_list['course_id'] = obj.course_id
        if obj.code:
            course_list['course_name'] = obj.code
        else:
            course_list['course_name'] = obj.course_name
        final_list_of_labs.append(course_list)

    request.session['list_of_labs'] = final_list_of_labs
