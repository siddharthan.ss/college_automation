from django.http import HttpResponseRedirect
from django.views.generic import CreateView
from django.views.generic import ListView

from accounts.models import staff, student
from curriculum.forms import holiday_form
from curriculum.models import holidays


class ListHolidays(ListView):
    template_name = 'curriculum/holidays.html'
    context_object_name = 'holidays'

    def get_queryset(self):
        return holidays.objects.all()


class HolidaysCreate(CreateView):
    template_name = 'curriculum/holidays_create_form.html'
    form_class = holiday_form
    model = holidays
    success_url = '/list_holidays'

    def form_valid(self, form):
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())


def is_holiday(date):
    try:
        if holidays.objects.filter(date=date):
            return True
    except holidays.DoesNotExist:
        return False