from pprint import pprint

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from accounts.models import staff, department
from curriculum.models import semester, staff_course
from curriculum.views import (
    get_active_semesters_tabs_of_department,
    get_active_semesters_tabs_for_overall_reports,
    get_list_of_students
)

from attendance.views.SubjectConsolidatedAttendanceAPI import SubjectConsolidatedAttendanceAPI


@login_required
@permission_required('curriculum.can_view_overall_reports')
def consolidated_ut_attendance_report(request, semester_pk=None):
    staff_instance = staff.objects.get(user=request.user)

    all_departments = department.objects.filter(is_core=True)
    user_is_pricipal_or_coe = False

    selected_department_pk = None
    if request.method == "POST" and 'selected_department_pk' in request.POST:
        selected_department_pk = int(request.POST.get('selected_department_pk'))
    if request.user.groups.filter(name='principal').exists() or request.user.groups.filter(name='coe_staff').exists():
        if not selected_department_pk:
            selected_department = all_departments.first()
            selected_department_pk = selected_department.pk
        else:
            selected_department = department.objects.get(pk=selected_department_pk)
        avaliable_semesters = get_active_semesters_tabs_of_department(selected_department)
        user_is_pricipal_or_coe = True
    else:
        selected_department = staff_instance.department
        selected_department_pk = selected_department.pk
        avaliable_semesters = get_active_semesters_tabs_for_overall_reports(request)

    if not avaliable_semesters:
        modal = {
            'heading': 'Error',
            'body': 'No semester plan has been created for your access',
        }
        return render(request, 'dashboard/dashboard_content.html', {'toggle_model': 'true', 'modal': modal})

    pprint(avaliable_semesters)

    selected_semester_pk = avaliable_semesters[0]['semester_instance'].pk

    if request.method == "POST" and 'semester_pk' in request.POST:
        selected_semester_pk = int(request.POST.get('semester_pk'))

    if semester_pk:
        selected_semester_pk = int(semester_pk)

    print(selected_semester_pk)

    selected_semester = semester.objects.get(pk=selected_semester_pk)

    alloted_course_instances_of_selected_semester = staff_course.objects.filter(
        semester=selected_semester
    )

    subjectConsolidatedAttendanceAPIInstance = SubjectConsolidatedAttendanceAPI()
    temp = {}
    for staff_course_instance in alloted_course_instances_of_selected_semester:
        list_of_students = get_list_of_students(staff_course_instance.semester, staff_course_instance.course)
        # setting start and end dates, course
        subjectConsolidatedAttendanceAPIInstance.semester_instance = staff_course_instance.semester
        subjectConsolidatedAttendanceAPIInstance.course_instance = staff_course_instance.course
        subjectConsolidatedAttendanceAPIInstance.start_date_instance = staff_course_instance.semester.start_term_1
        if staff_course_instance.semester.end_term_3:
            subjectConsolidatedAttendanceAPIInstance.start_date_instance = None
        for student_instance  in list_of_students:
            temp2 = {}
            temp3 = {}
            attendance_queryset = None
            temp3[staff_course_instance.course.pk] = None
            temp2['attendance'] = None
            temp[student_instance.pk] = None