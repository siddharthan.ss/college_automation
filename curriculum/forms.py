from django import forms

from accounts.models import staff
from curriculum.models import semester, time_table, holidays


class assign_programme_coordinator_form(forms.Form):
    programme_coordinator = forms.ModelChoiceField(queryset=staff.objects.none())

    def __init__(self, *args, **kwargs):
        self.queryset = kwargs.pop('queryset')
        super(assign_programme_coordinator_form, self).__init__(*args, **kwargs)
        self.fields["programme_coordinator"].help_text = "Assign a programme coordinator"
        self.fields["programme_coordinator"].queryset = self.queryset
        self.fields["programme_coordinator"].empty_label = "select a programme co-ordinator"
        self.fields["programme_coordinator"].widget.attrs.update({
            'class': 'form-control',
        })


class reallow_attendance_form(forms.Form):
    faculty = forms.ModelChoiceField(queryset=staff.objects.none())

    def __init__(self, *args, **kwargs):
        self.queryset = kwargs.pop('queryset')
        super(reallow_attendance_form, self).__init__(*args, **kwargs)
        self.fields["faculty"].help_text = "Select the faculty to whom access should be allowed"
        self.fields["faculty"].queryset = self.queryset
        self.fields["faculty"].empty_label = "select a faculty"
        self.fields["faculty"].widget.attrs.update({
            'class': 'form-control',
        })


class semester_plan_form(forms.ModelForm):
    required_field = [
        'faculty_advisor',
    ]

    class Meta:
        model = semester
        fields = ['faculty_advisor']
        exclude = ['semester_number', 'batch', 'department']

    def __init__(self, *args, **kwargs):
        self.queryset = kwargs.pop('queryset')
        super(semester_plan_form, self).__init__(*args, **kwargs)
        self.fields["faculty_advisor"].help_text = "Select a faculty advisor for this batch"
        self.fields["faculty_advisor"].queryset = self.queryset

        for field in semester_plan_form.required_field:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })


class create_timetable_form(forms.ModelForm):
    required_field = [
        'course',
        'day',
        'hour',
        'staff',
        'sem',
    ]

    class Meta:
        model = time_table
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(create_timetable_form, self).__init__(*args, **kwargs)
        for field in create_timetable_form.required_field:
            self.fields[field].required = True


class course_filter_form(forms.Form):
    PROGRAMME_CHOICES = (('UG', 'Under Graduate'),
                         ('PG', 'Post Graduate'),
                         ('Phd', 'Doctrate of Philosophy')
                         )

    programme = forms.ChoiceField(choices=PROGRAMME_CHOICES)

    def __init__(self, *args, **kwargs):
        super(course_filter_form, self).__init__(*args, **kwargs)


class holiday_form(forms.ModelForm):
    class Meta:
        model = holidays
        fields ='__all__'
    def __init__(self, *args, **kwargs):
        super(holiday_form, self).__init__(*args, **kwargs)

        self.fields['date'].widget.attrs.update({
            'class': 'form-control',
        })
        self.fields['description'].widget.attrs.update({
            'class': 'form-control',
        })