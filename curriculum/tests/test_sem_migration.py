import factory
from django.contrib.auth.models import Group
from django.db.models import signals
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import CustomUser, student, department, batch, active_batches, staff, regulation
from curriculum.models import semester, semester_migration, department_sections


class MigrateSemTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def setUp(self):
        self.Client = Client()
        batch_list = []
        for stud in student.objects.all():
            batch_list.append(stud.batch)

        for act_batch_inst in active_batches.objects.all():
            if act_batch_inst.batch not in batch_list:
                act_batch_inst.delete()

    def test_access_without_login(self):
        response = self.client.get('/migrate_semesters/')
        self.assertTemplateNotUsed(response, template_name='current_semester_plans.html')

    def test_access_without_permission(self):
        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        response = self.client.get('/migrate_semesters/')
        self.assertTemplateNotUsed(response, template_name='current_semester_plans.html')

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        response = self.client.get('/migrate_semesters/')
        self.assertTemplateNotUsed(response, template_name='current_semester_plans.html')

    def test_access_with_permission(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/migrate_semesters/')
        self.assertTemplateUsed(response, template_name='curriculum/current_semester_plans.html')

    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def test_migrate_ug_1_1(self):
        # apply migration on odd sem
        # check student current_sem
        # check batch current_sem
        # check whether entry removed in semester_migrations table

        # init students odd sem
        dept_inst = department.objects.get(acronym='CSE')
        batch_inst = batch.objects.filter(programme='UG').get(start_year=2016)
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(
            batch=batch_inst).all()
        for entry in student_inst:
            entry.current_semester = 1
            entry.save()

        dept_sec_A = department_sections(
            department=dept_inst,
            batch=batch_inst,
            section_name='A'
        )
        dept_sec_A.save()

        sem_inst = semester(
            department=dept_inst,
            batch=batch_inst,
            semester_number=1,
            department_section=dept_sec_A
        )
        sem_inst.save()

        # add one customuser as faculty advisor of this semester plan
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')

        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(cust_inst)
        g.save()

        staff_inst = staff.objects.get(user=cust_inst)
        sem_inst.faculty_advisor.add(staff_inst)

        sem_mig_inst = semester_migration(
            semester=sem_inst,
            department=dept_inst,
            semester_number=sem_inst.semester_number
        )
        sem_mig_inst.save()
        active_batch_inst = active_batches.objects.get_or_create(
            batch=batch_inst,
            department=dept_inst,
            programme=batch_inst.programme,
            current_semester_number_of_this_batch=sem_inst.semester_number
        )

        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.post('/migrate_semesters/', {u'migration_group': [u'UG_1']})
        self.assertEqual(response.status_code, 200)
        """
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(batch=batch_inst).all()
        for entry in student_inst:
            self.assertEqual(entry.current_semester,2)
        """
        active_batch_inst = active_batches.objects.filter(batch=batch_inst).filter(department=dept_inst).get(
            programme=batch_inst.programme)
        self.assertEqual(active_batch_inst.current_semester_number_of_this_batch, 2)
        self.assertFalse(semester_migration.objects.filter(semester=sem_inst).exists())

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor'))

    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def test_migrate_ug_1_2(self):
        # apply migration on odd sem
        # check student current_sem
        # check batch current_sem
        # check whether entry removed in semester_migrations table

        # init students odd sem
        dept_inst = department.objects.get(acronym='CSE')
        batch_inst = batch.objects.filter(programme='UG').get(start_year=2016)
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(
            batch=batch_inst).all()
        for entry in student_inst:
            entry.current_semester = 2
            entry.save()

        dept_sec_A = department_sections(
            department=dept_inst,
            batch=batch_inst,
            section_name='A'
        )
        dept_sec_A.save()

        sem_inst = semester(
            department=dept_inst,
            batch=batch_inst,
            semester_number=2,
            department_section=dept_sec_A
        )
        sem_inst.save()

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')

        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(cust_inst)
        g.save()

        staff_inst = staff.objects.get(user=cust_inst)
        sem_inst.faculty_advisor.add(staff_inst)

        sem_mig_inst = semester_migration(
            semester=sem_inst,
            department=dept_inst,
            semester_number=sem_inst.semester_number
        )
        sem_mig_inst.save()
        active_batches.objects.filter(
            batch=batch_inst,
            department=dept_inst,
            programme=batch_inst.programme,
            current_semester_number_of_this_batch=sem_inst.semester_number - 1
        ).delete()
        active_batches.objects.get_or_create(
            batch=batch_inst,
            department=dept_inst,
            programme=batch_inst.programme,
            current_semester_number_of_this_batch=sem_inst.semester_number
        )

        # delete to be created batch if already exists
        try:
            batch.objects.filter(programme=batch_inst.programme).get(start_year=batch_inst.start_year + 1).delete()
        except:
            # print('Does not exist')
            pass

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        reg_obj = regulation.objects.get(start_year=2016)

        response = self.client.post('/migrate_semesters/',
                                    {u'migration_group': [u'UG_1'], u'selected_regulation': [reg_obj.pk]})
        self.assertEqual(response.status_code, 200)
        """
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(batch=batch_inst).all()
        for entry in student_inst:
            self.assertEqual(entry.current_semester,3)
            #print(entry)
        """

        active_batch_inst = active_batches.objects.filter(batch=batch_inst).filter(department=dept_inst).get(
            programme=batch_inst.programme)
        self.assertEqual(active_batch_inst.current_semester_number_of_this_batch, 3)
        new_batch_inst = batch.objects.filter(programme=batch_inst.programme).get(start_year=batch_inst.start_year + 1)

        self.assertTrue(active_batches.objects.filter(batch=new_batch_inst).filter(department=dept_inst).filter(
            programme=new_batch_inst.programme).exists())

        self.assertTrue(
            department_sections.objects.filter(department=dept_inst).filter(batch=new_batch_inst).exists()
        )

        self.assertFalse(semester_migration.objects.filter(semester=sem_inst).exists())

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor'))

    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def test_migrate_ug_others_lower(self):
        # apply migration on odd sem
        # check student current_sem
        # check batch current_sem
        # check whether entry removed in semester_migrations table

        # init students odd sem
        dept_inst = department.objects.get(acronym='CSE')
        batch_inst = batch.objects.filter(programme='UG').get(start_year=2016)
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(
            batch=batch_inst).all()
        for entry in student_inst:
            entry.current_semester = 3
            entry.save()

        dept_sec_A = department_sections(
            department=dept_inst,
            batch=batch_inst,
            section_name='A'
        )
        dept_sec_A.save()

        sem_inst = semester(
            department=dept_inst,
            batch=batch_inst,
            semester_number=3,
            department_section=dept_sec_A
        )
        sem_inst.save()

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')

        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(cust_inst)
        g.save()

        staff_inst = staff.objects.get(user=cust_inst)
        sem_inst.faculty_advisor.add(staff_inst)

        sem_mig_inst = semester_migration(
            semester=sem_inst,
            department=dept_inst,
            semester_number=sem_inst.semester_number
        )
        sem_mig_inst.save()

        active_batches.objects.filter(batch=batch_inst).filter(department=dept_inst).filter(
            programme=batch_inst.programme).get(
            current_semester_number_of_this_batch=1
        ).delete()

        active_batches.objects.get_or_create(
            batch=batch_inst,
            department=dept_inst,
            programme=batch_inst.programme,
            current_semester_number_of_this_batch=sem_inst.semester_number
        )

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.post('/migrate_semesters/', {u'migration_group': [u'UG_other']})
        self.assertEqual(response.status_code, 200)
        """
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(batch=batch_inst).all()
        for entry in student_inst:
            print(vars(entry))
            self.assertEqual(entry.current_semester,4)
        """

        active_batch_inst = active_batches.objects.filter(batch=batch_inst).filter(department=dept_inst).get(
            programme=batch_inst.programme)
        self.assertEqual(active_batch_inst.current_semester_number_of_this_batch, 4)
        self.assertFalse(semester_migration.objects.filter(semester=sem_inst).exists())

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor'))

    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def test_migrate_pg_others_lower(self):
        # apply migration on odd sem
        # check student current_sem
        # check batch current_sem
        # check whether entry removed in semester_migrations table

        # init students odd sem
        dept_inst = department.objects.get(acronym='CSE')
        batch_inst = batch.objects.filter(programme='UG').get(start_year=2016)
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(
            batch=batch_inst).all()
        for entry in student_inst:
            entry.qualification = 'PG'
            entry.current_semester = 3
            entry.save()
        batch_inst = batch.objects.filter(programme='PG').get(start_year=2016)

        dept_sec_A = department_sections(
            department=dept_inst,
            batch=batch_inst,
            section_name='A'
        )
        dept_sec_A.save()
        sem_inst = semester(
            department=dept_inst,
            batch=batch_inst,
            semester_number=3,
            department_section=dept_sec_A
        )
        sem_inst.save()

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')

        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(cust_inst)
        g.save()

        staff_inst = staff.objects.get(user=cust_inst)
        sem_inst.faculty_advisor.add(staff_inst)

        sem_mig_inst = semester_migration(
            semester=sem_inst,
            department=dept_inst,
            semester_number=sem_inst.semester_number
        )
        sem_mig_inst.save()

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')

        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(cust_inst)
        g.save()

        staff_inst = staff.objects.get(user=cust_inst)
        sem_inst.faculty_advisor.add(staff_inst)

        active_batch_inst = active_batches.objects.get_or_create(
            batch=batch_inst,
            department=dept_inst,
            programme=batch_inst.programme,
            current_semester_number_of_this_batch=sem_inst.semester_number
        )
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.post('/migrate_semesters/', {u'migration_group': [u'PG_other']})
        self.assertEqual(response.status_code, 200)

        student_inst = student.objects.filter(department=dept_inst).filter(qualification='PG').filter(
            batch=batch_inst).all()
        for entry in student_inst:
            # print(vars(entry))
            self.assertEqual(entry.current_semester, 4)

        active_batch_inst = active_batches.objects.filter(batch=batch_inst).filter(department=dept_inst).get(
            programme=batch_inst.programme)
        self.assertEqual(active_batch_inst.current_semester_number_of_this_batch, 4)
        self.assertFalse(semester_migration.objects.filter(semester=sem_inst).exists())

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor'))

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor'))

    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def test_migrate_ug_others_higher(self):
        # apply migration on odd sem
        # check student current_sem
        # check batch current_sem
        # check whether entry removed in semester_migrations table

        # init students odd sem
        dept_inst = department.objects.get(acronym='CSE')
        batch_inst = batch.objects.filter(programme='UG').get(start_year=2016)
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(
            batch=batch_inst).all()
        for entry in student_inst:
            entry.current_semester = 8
            entry.save()

        dept_sec_A = department_sections(
            department=dept_inst,
            batch=batch_inst,
            section_name='A'
        )
        dept_sec_A.save()

        sem_inst = semester(
            department=dept_inst,
            batch=batch_inst,
            semester_number=8,
            department_section=dept_sec_A
        )
        sem_inst.save()

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')

        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(cust_inst)
        g.save()

        staff_inst = staff.objects.get(user=cust_inst)
        sem_inst.faculty_advisor.add(staff_inst)

        sem_mig_inst = semester_migration(
            semester=sem_inst,
            department=dept_inst,
            semester_number=sem_inst.semester_number
        )
        sem_mig_inst.save()

        active_batches.objects.get_or_create(
            batch=batch_inst,
            department=dept_inst,
            programme=batch_inst.programme,
            current_semester_number_of_this_batch=sem_inst.semester_number
        )

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.post('/migrate_semesters/', {u'migration_group': [u'UG_other']})
        self.assertEqual(response.status_code, 200)

        """
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(batch=batch_inst).all()
        for entry in student_inst:
            #print(vars(entry))
            self.assertEqual(entry.current_semester,9)
        """

        self.assertFalse(active_batches.objects.filter(batch=batch_inst).filter(department=dept_inst).filter(
            programme=batch_inst.programme).filter(current_semester_number_of_this_batch=8).exists())
        # new_batch_inst = batch.objects.filter(programme=batch_inst.programme).get(start_year=batch_inst.start_year+1)

        # self.assertTrue(active_batches.objects.filter(batch=new_batch_inst).filter(department=dept_inst).filter(programme=new_batch_inst.programme).exists())
        self.assertFalse(semester_migration.objects.filter(semester=sem_inst).exists())

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor'))

    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def test_migrate_pg_others_higher(self):
        # apply migration on odd sem
        # check student current_sem
        # check batch current_sem
        # check whether entry removed in semester_migrations table

        # init students odd sem
        dept_inst = department.objects.get(acronym='CSE')
        batch_inst = batch.objects.filter(programme='UG').get(start_year=2016)
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(
            batch=batch_inst).all()
        for entry in student_inst:
            entry.qualification = 'PG'
            entry.current_semester = 4
            entry.save()
        batch_inst = batch.objects.filter(programme='PG').get(start_year=2016)

        dept_sec_A = department_sections(
            department=dept_inst,
            batch=batch_inst,
            section_name='A'
        )
        dept_sec_A.save()
        sem_inst = semester(
            department=dept_inst,
            batch=batch_inst,
            semester_number=4,
            department_section=dept_sec_A
        )
        sem_inst.save()

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')

        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(cust_inst)
        g.save()

        staff_inst = staff.objects.get(user=cust_inst)
        sem_inst.faculty_advisor.add(staff_inst)

        sem_mig_inst = semester_migration(
            semester=sem_inst,
            department=dept_inst,
            semester_number=sem_inst.semester_number
        )
        sem_mig_inst.save()
        active_batch_inst = active_batches(
            batch=batch_inst,
            department=dept_inst,
            programme=batch_inst.programme,
            current_semester_number_of_this_batch=sem_inst.semester_number
        )
        active_batch_inst.save()

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.post('/migrate_semesters/', {u'migration_group': [u'PG_other']})
        self.assertEqual(response.status_code, 200)

        student_inst = student.objects.filter(department=dept_inst).filter(qualification='PG').filter(
            batch=batch_inst).all()
        for entry in student_inst:
            # print(vars(entry))
            self.assertEqual(entry.current_semester, 5)

        # print(vars(active_batches.objects.filter(batch=batch_inst).filter(department=dept_inst).get(programme=batch_inst.programme)))

        self.assertFalse(active_batches.objects.filter(batch=batch_inst).filter(department=dept_inst).filter(
            programme=batch_inst.programme).exists())
        # new_batch_inst = batch.objects.filter(programme=batch_inst.programme).get(start_year=batch_inst.start_year+1)

        # self.assertTrue(active_batches.objects.filter(batch=new_batch_inst).filter(department=dept_inst).filter(programme=new_batch_inst.programme).exists())
        self.assertFalse(semester_migration.objects.filter(semester=sem_inst).exists())

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor'))

    # @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def test_migrate_pg_1(self):
        # apply migration on odd sem
        # check student current_sem
        # check batch current_sem
        # check whether entry removed in semester_migrations table

        # init students odd sem
        dept_inst = department.objects.get(acronym='CSE')
        batch_inst = batch.objects.filter(programme='UG').get(start_year=2016)
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(
            batch=batch_inst).all()
        self.assertGreater(student_inst.count(), 0, 'No student found')
        for entry in student_inst:
            entry.qualification = 'PG'
            entry.current_semester = 1
            entry.save()

        batch_inst = batch.objects.filter(programme='PG').get(start_year=2016)

        dept_sec_A = department_sections(
            department=dept_inst,
            batch=batch_inst,
            section_name='A'
        )
        dept_sec_A.save()

        sem_inst = semester(
            department=dept_inst,
            batch=batch_inst,
            semester_number=1,
            department_section=dept_sec_A
        )
        sem_inst.save()

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')

        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(cust_inst)
        g.save()

        staff_inst = staff.objects.get(user=cust_inst)
        sem_inst.faculty_advisor.add(staff_inst)

        sem_mig_inst = semester_migration(
            semester=sem_inst,
            department=dept_inst,
            semester_number=sem_inst.semester_number
        )
        sem_mig_inst.save()
        active_batch_inst = active_batches.objects.get_or_create(
            batch=batch_inst,
            department=dept_inst,
            programme=batch_inst.programme,
            current_semester_number_of_this_batch=sem_inst.semester_number
        )

        print(active_batch_inst)

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        response = self.client.post('/migrate_semesters/', {u'migration_group': [u'PG_1']})
        self.assertEqual(response.status_code, 200)

        student_inst = student.objects.filter(department=dept_inst).filter(qualification='PG').filter(
            batch=batch_inst).all()
        for entry in student_inst:
            self.assertEqual(entry.current_semester, 2)

        active_batch_inst = active_batches.objects.filter(batch=batch_inst).filter(department=dept_inst).get(
            programme=batch_inst.programme)
        self.assertEqual(active_batch_inst.current_semester_number_of_this_batch, 2)
        self.assertFalse(semester_migration.objects.filter(semester=sem_inst).exists())

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor'))

    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def test_migrate_pg_2(self):
        # apply migration on odd sem
        # check student current_sem
        # check batch current_sem
        # check whether entry removed in semester_migrations table

        # init students odd sem
        dept_inst = department.objects.get(acronym='CSE')
        batch_inst = batch.objects.filter(programme='UG').get(start_year=2016)
        student_inst = student.objects.filter(department=dept_inst).filter(qualification='UG').filter(
            batch=batch_inst).all()
        self.assertGreater(student_inst.count(), 0, 'No student found')
        for entry in student_inst:
            entry.qualification = 'PG'
            entry.current_semester = 2
            entry.save()

        batch_inst = batch.objects.filter(programme='PG').get(start_year=2016)

        dept_sec_A = department_sections(
            department=dept_inst,
            batch=batch_inst,
            section_name='A'
        )
        dept_sec_A.save()

        sem_inst = semester(
            department=dept_inst,
            batch=batch_inst,
            semester_number=2,
            department_section=dept_sec_A
        )
        sem_inst.save()

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')

        g = Group.objects.get(name='faculty_advisor')
        g.user_set.add(cust_inst)
        g.save()

        staff_inst = staff.objects.get(user=cust_inst)
        sem_inst.faculty_advisor.add(staff_inst)

        sem_mig_inst = semester_migration(
            semester=sem_inst,
            department=dept_inst,
            semester_number=sem_inst.semester_number
        )
        sem_mig_inst.save()
        active_batches.objects.filter(
            batch=batch_inst,
            department=dept_inst,
            programme=batch_inst.programme,
            current_semester_number_of_this_batch=sem_inst.semester_number - 1
        ).delete()
        active_batch_inst = active_batches.objects.get_or_create(
            batch=batch_inst,
            department=dept_inst,
            programme=batch_inst.programme,
            current_semester_number_of_this_batch=sem_inst.semester_number
        )
        # print('before calling')

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        reg_obj = regulation.objects.get(start_year=2016)

        response = self.client.post('/migrate_semesters/',
                                    {u'migration_group': [u'PG_1'], u'selected_regulation': [reg_obj.pk]})
        self.assertEqual(response.status_code, 200)

        student_inst = student.objects.filter(department=dept_inst).filter(qualification='PG').filter(
            batch=batch_inst).all()
        for entry in student_inst:
            self.assertEqual(entry.current_semester, 3)

        active_batch_inst = active_batches.objects.filter(batch=batch_inst).filter(department=dept_inst).get(
            programme=batch_inst.programme)
        self.assertEqual(active_batch_inst.current_semester_number_of_this_batch, 3)
        new_batch_inst = batch.objects.filter(programme=batch_inst.programme).get(start_year=batch_inst.start_year + 1)

        self.assertTrue(active_batches.objects.filter(batch=new_batch_inst).filter(department=dept_inst).filter(
            programme=new_batch_inst.programme).exists())

        self.assertTrue(
            department_sections.objects.filter(department=dept_inst).filter(batch=new_batch_inst).exists()
        )
        self.assertFalse(semester_migration.objects.filter(semester=sem_inst).exists())

        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        self.assertFalse(cust_inst.groups.filter(name='faculty_advisor'))


def load_active_batches():
    core_dept_list = ['CSE', 'IT', 'MECH', 'CIVIL', 'PROD', 'IBT', 'EEE', 'ECE', 'EIE']

    for acronym in core_dept_list:
        try:
            dept_inst = department.objects.get(acronym=acronym)
            st_yr = 2013
            end_yr = 2016

            while (st_yr <= end_yr):
                batch_obj = batch.objects.filter(start_year=st_yr).get(programme='UG')

                no_of_years = timezone.now().year - st_yr
                if timezone.now().month > 5 and timezone.now().month <= 12:
                    sem = no_of_years * 2 + 1
                else:
                    sem = no_of_years * 2

                active_batch_obj = active_batches.objects.create(
                    batch=batch_obj,
                    department=dept_inst,
                    programme='UG',
                    current_semester_number_of_this_batch=sem,
                )
                print('Created ' + str(active_batch_obj))
                st_yr += 1
            st_yr = 2015
            while (st_yr <= end_yr):
                batch_obj = batch.objects.filter(start_year=st_yr).get(programme='PG')

                no_of_years = timezone.now().year - st_yr
                if timezone.now().month > 5 and timezone.now().month <= 12:
                    sem = no_of_years * 2 + 1
                else:
                    sem = no_of_years * 2

                active_batch_obj = active_batches.objects.create(
                    batch=batch_obj,
                    department=dept_inst,
                    programme='PG',
                    current_semester_number_of_this_batch=sem,
                )
                print('Created ' + str(active_batch_obj))
                st_yr += 1
        except department.DoesNotExist:
            print('Could not find dept ' + acronym)


class MigrationConsistencyTest(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'student.json',
        'staff.json',
    ]

    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def setUp(self):
        self.Client = Client()
        # sload_active_batches()

        batch_list = []
        for stud in student.objects.all():
            batch_list.append(stud.batch)

        for act_batch_inst in active_batches.objects.all():
            if act_batch_inst.batch not in batch_list:
                act_batch_inst.delete()

    def test_access_without_login(self):
        response = self.client.get('/check_migration_consitency/')
        self.assertTemplateNotUsed(response, template_name='current_semester_plans.html')

    def test_access_without_permission(self):
        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        response = self.client.get('/check_migration_consitency/')
        self.assertRedirects(response, '/login?next=/check_migration_consitency/')

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        response = self.client.get('/check_migration_consitency/')
        self.assertRedirects(response, '/login?next=/check_migration_consitency/')

    def test_access_with_permission_without_post_data(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)
        response = self.client.get('/check_migration_consitency/')
        self.assertTemplateUsed(response, '403.html')

    """

    def test_some_1_some_2_ug(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 1
        stud_inst.qualification = 'UG'
        stud_inst.save()

        cust_inst = CustomUser.objects.get(email='s2@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 2
        stud_inst.qualification = 'UG'
        stud_inst.save()

        no_of_1st_sem_studs = student.objects.filter(qualification='UG').filter(current_semester=1).count()
        no_of_2nd_sem_studs = student.objects.filter(qualification='UG').filter(current_semester=2).count()

        response = self.client.post('/check_migration_consitency',{u'selected_group': [u'UG_1']})

        #print(vars(response))

        self.assertEqual(response.status_code, 200)

        json_string = response.content.decode("utf-8")

        data = json.loads(json_string)

        msg = '<p>There is a serious conflict in your department</p>' \
                      '<p>' + str(no_of_1st_sem_studs) + ' students belong to 1st semsester and ' + \
                      str(no_of_2nd_sem_studs) + ' belong to 2nd semester. ' + '</p>' \
                      '<p>This can never be possible. Please Contact site administrator immediately</p>',
        code = 2

        json_msg = json.dumps(msg)
        got_json_msg = json.dumps(data['msg'])

        self.assertEqual(got_json_msg, json_msg.strip('[').strip(']'))
        self.assertEqual(data['code'],code)

    def test_some_1_some_2_pg(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 1
        stud_inst.qualification = 'PG'
        stud_inst.save()

        cust_inst = CustomUser.objects.get(email='s2@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 2
        stud_inst.qualification = 'PG'
        stud_inst.save()

        no_of_1st_sem_studs = student.objects.filter(qualification='PG').filter(current_semester=1).count()
        no_of_2nd_sem_studs = student.objects.filter(qualification='PG').filter(current_semester=2).count()

        response = self.client.post('/check_migration_consitency', {u'selected_group': [u'PG_1']})

        # print(vars(response))

        self.assertEqual(response.status_code, 200)

        json_string = response.content.decode("utf-8")

        data = json.loads(json_string)

        msg = '<p>There is a serious conflict in your department</p>' \
              '<p>' + str(no_of_1st_sem_studs) + ' students belong to 1st semsester and ' + \
              str(no_of_2nd_sem_studs) + ' belong to 2nd semester. ' + '</p>' \
                                                                       '<p>This can never be possible. Please Contact site administrator immediately</p>',
        code = 2

        json_msg = json.dumps(msg)
        got_json_msg = json.dumps(data['msg'])

        self.assertEqual(got_json_msg, json_msg.strip('[').strip(']'))
        self.assertEqual(data['code'], code)


    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def test_all_2_some_3_some_4_ug(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 2
        stud_inst.qualification = 'UG'
        stud_inst.save()

        cust_inst = CustomUser.objects.get(email='s2@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 3
        stud_inst.qualification = 'UG'
        stud_inst.save()

        cust_inst = CustomUser.objects.get(email='s3@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 4
        stud_inst.qualification = 'UG'
        stud_inst.save()

        cust_inst = CustomUser.objects.get(email='s4@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 2
        stud_inst.qualification = 'UG'
        stud_inst.save()

        no_of_3rd_sem_studs = student.objects.filter(qualification='UG').filter(current_semester=3).count()
        no_of_4th_sem_studs = student.objects.filter(qualification='UG').filter(current_semester=4).count()

        response = self.client.post('/check_migration_consitency', {u'selected_group': [u'UG_1']})

        # print(vars(response))

        self.assertEqual(response.status_code, 200)

        json_string = response.content.decode("utf-8")

        data = json.loads(json_string)

        msg = '<p>There are some student still in 2nd semester</p>'
        code = 2

        json_msg = json.dumps(msg)
        got_json_msg = json.dumps(data['msg'])

        self.assertEqual(got_json_msg, json_msg.strip('[').strip(']'))
        self.assertEqual(data['code'], code)

    @factory.django.mute_signals(signals.pre_save, signals.post_save)
    def test_all_2_some_3_some_4_pg(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(cust_inst)
        g.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 2
        stud_inst.qualification = 'PG'
        stud_inst.save()

        active_batches.objects.get_or_create(
            batch = stud_inst.batch,
            programme = stud_inst.qualification,
            current_semester_number_of_this_batch = stud_inst.current_semester,
            department = stud_inst.department
        )
        cust_inst = CustomUser.objects.get(email='s2@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 3
        stud_inst.qualification = 'PG'
        stud_inst.save()

        cust_inst = CustomUser.objects.get(email='s3@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 4
        stud_inst.qualification = 'PG'
        stud_inst.save()

        cust_inst = CustomUser.objects.get(email='s4@gmail.com')
        stud_inst = student.objects.get(user=cust_inst)
        stud_inst.current_semester = 2
        stud_inst.qualification = 'PG'
        stud_inst.save()

        no_of_3rd_sem_studs = student.objects.filter(qualification='PG').filter(current_semester=3).count()
        no_of_4th_sem_studs = student.objects.filter(qualification='PG').filter(current_semester=4).count()



        response = self.client.post('/check_migration_consitency', {u'selected_group': [u'PG_1']})

        # print(vars(response))

        self.assertEqual(response.status_code, 200)

        json_string = response.content.decode("utf-8")

        data = json.loads(json_string)

        msg = '<p>Please migrate 2nd Year students first</p>'
        code = 2

        self.maxDiff = None

        json_msg = json.dumps(msg)
        got_json_msg = json.dumps(data['msg'])

        self.assertEqual(got_json_msg, json_msg.strip('[').strip(']'))
        self.assertEqual(data['code'], code)


    """
