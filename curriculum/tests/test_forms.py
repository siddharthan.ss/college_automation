from django.test import TestCase

from accounts.models import department
from curriculum.forms import *


class TestAssginProgrammeCoordiatorForm(TestCase):
    fixtures = [
        'ct.json', 'perms.json',
        'department.json',
        'group.json',
        'customuser.json', 'staff.json',
    ]

    def test_valid_form(self):
        dept = department.objects.get(acronym='CSE')
        queryset = staff.objects.all().filter(department=dept)
        test_staff = staff.objects.filter(first_name='F1').get(last_name='cse')
        form_data = {
            'programme_coordinator': test_staff.pk,
        }
        programme_coordinator_form = assign_programme_coordinator_form(data=form_data, queryset=queryset)
        self.assertTrue(programme_coordinator_form.is_valid())

        test_staff = staff.objects.filter(first_name='F1').get(last_name='it')
        form_data = {
            'programme_coordinator': test_staff.pk,
        }
        programme_coordinator_form = assign_programme_coordinator_form(data=form_data, queryset=queryset)
        self.assertFalse(programme_coordinator_form.is_valid())


class TestReallowAttendanceMarkingForm(TestCase):
    fixtures = [
        'ct.json', 'perms.json',
        'department.json',
        'group.json',
        'customuser.json', 'staff.json',
    ]

    def test_valid_form(self):
        dept = department.objects.get(acronym='CSE')
        queryset = staff.objects.all().filter(department=dept)
        test_staff_1 = staff.objects.filter(first_name='F1').get(last_name='cse')
        form_data = {
            'faculty': test_staff_1.pk,
        }
        form = reallow_attendance_form(data=form_data, queryset=queryset)

        self.assertTrue(form.is_valid())


class TestCreateSemesterPlanForm(TestCase):
    fixtures = [
        'ct.json', 'perms.json',
        'department.json',
        'group.json',
        'customuser.json', 'staff.json',
    ]

    def test_valid_form(self):
        dept = department.objects.get(acronym='CSE')
        queryset = staff.objects.all().filter(department=dept)
        test_staff_1 = staff.objects.filter(first_name='F1').get(last_name='cse')
        test_staff_2 = staff.objects.filter(first_name='FA2').get(last_name='cse')
        form_data = {
            'faculty_advisor': [test_staff_1.pk, test_staff_2.pk],
        }
        form = semester_plan_form(data=form_data, queryset=queryset)

        self.assertTrue(form.is_valid())
