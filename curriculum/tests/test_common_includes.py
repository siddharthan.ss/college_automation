import unittest

from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import student, staff
from curriculum.models import courses


@unittest.skip('yet to implement')
class ProperStaffAllotmentReturn(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'regulation.json',
        'college_schedule.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'student.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choosen_course = courses.objects.get(course_id='16SBS103')
        self.assertIsNotNone(self.choosen_course, 'Empty course ...!')
        self.choose_student = student.objects.get(user__email='s1@gmail.com')
        self.choose_staff = staff.objects.get(user__email='fa1@gmail.com')
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(self.choose_staff.user)
        g.save()
        self.choosen_date = timezone.now().date()
