from django.test import Client
from django.test import TestCase
from accounts.models import department
from datetime import datetime

from common.utils.slotNumberUtil import getSlotNumber
from curriculum.models import holidays


class SlotNumberTest(TestCase):

    def setUp(self):
        self.department_instance = department.objects.create(
            name = "Computer Science and Engineering",
            acronym = "CSE",
            is_core = True
        )

        self.start_date = datetime.strptime("22-01-2017","%d-%m-%Y") # monday
        self.end_date = datetime.strptime("31-01-2017","%d-%m-%Y") # wednesday - next week
        self.holiday_1 = datetime.strptime("26-01-2017","%d-%m-%Y")
        self.holiday_2 = datetime.strptime("29-01-2017","%d-%m-%Y")

    def test_slot_number_without_holidays(self):
        self.assertEqual(
            getSlotNumber(
                self.start_date,
                self.end_date,
                self.department_instance,
                1
            ),
            3
        )
        self.assertEqual(
            getSlotNumber(
                self.start_date,
                self.end_date,
                self.department_instance,
                3
            ),
            5
        )

    def create_first_holiday(self):
        holidays.objects.create(
            date = self.holiday_1,
            description = 'test 1'
        )

    def create_second_holiday(self):
        holidays.objects.create(
            date = self.holiday_2,
            description = 'test 2'
        )

    def test_slot_number_with_single_holiday(self):
        self.create_first_holiday()
        self.assertEqual(
            getSlotNumber(
                self.start_date,
                self.end_date,
                self.department_instance,
                1
            ),
            2
        )
        self.assertEqual(
            getSlotNumber(
                self.start_date,
                self.end_date,
                self.department_instance,
                3
            ),
            4
        )

    def test_slot_number_with_dual_holiday(self):
        self.create_first_holiday()
        self.create_second_holiday()
        self.assertEqual(
            getSlotNumber(
                self.start_date,
                self.end_date,
                self.department_instance,
                1
            ),
            1
        )
        self.assertEqual(
            getSlotNumber(
                self.start_date,
                self.end_date,
                self.department_instance,
                3
            ),
            3
        )