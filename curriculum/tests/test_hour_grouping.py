from django.test import TestCase, Client
from django.utils import timezone

from curriculum.models import *
from curriculum.views.common_includes import *


class HourGroupingTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choose_staff = staff.objects.get(user__email='f1@gmail.com')

        programme_inst = programme.objects.create(
            name="Under Graduate",
            acronym="UG"
        )
        department_programmes_inst = department_programmes.objects.create(
            department=self.choose_staff.department,
            programme=programme_inst
        )
        pc_table_inst = programme_coordinator.objects.create(
            department_programme=department_programmes_inst
        )
        pc_table_inst.programme_coordinator_staffs.add(self.choose_staff)
        pc_table_inst.save()

        self.choosen_date = timezone.now().date()

        list_of_batches = get_current_batches(self.choose_staff.department)
        self.choosen_batch = batch.objects.get(pk=(list_of_batches[0]['batch_obj']))

        self.choosen_course = courses.objects.get(course_id='16SBS103')
        self.choosen_course.code = 'TEST'
        self.choosen_course.save()

        self.another_course = courses.objects.get(course_id='12S105')
        self.another_course.code = 'TEST2'
        self.another_course.save()

        # create two sections
        self.dept_sec_A = department_sections.objects.get(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='A'
        )
        self.dept_sec_B = department_sections(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='B'
        )
        self.dept_sec_B.save()

    def login_staff(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def create_semester_plan(self):
        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.dept_sec_A.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        # print(vars(response))
        self.sem_inst = semester.objects.get(batch=self.choosen_batch)

    def create_timetable_entries(self):
        timetable_obj = time_table(
            course=self.choosen_course,
            department=self.choosen_course.department,
            day=day.objects.get(day_name="Monday"),
            hour=1,
            semester=self.sem_inst,
            batch=self.choosen_batch,
            created_date=timezone.now(),
            modified_date=timezone.now(),

        )
        timetable_obj.save()

        timetable_obj = time_table(
            course=self.choosen_course,
            department=self.choosen_course.department,
            day=day.objects.get(day_name="Monday"),
            hour=6,
            semester=self.sem_inst,
            batch=self.choosen_batch,
            created_date=timezone.now(),
            modified_date=timezone.now(),
        )
        timetable_obj.save()

        timetable_obj = time_table(
            course=self.choosen_course,
            department=self.choosen_course.department,
            day=day.objects.get(day_name="Monday"),
            hour=7,
            semester=self.sem_inst,
            batch=self.choosen_batch,
            created_date=timezone.now(),
            modified_date=timezone.now(),
        )
        timetable_obj.save()

        timetable_obj = time_table(
            course=self.another_course,
            department=self.choosen_course.department,
            day=day.objects.get(day_name="Monday"),
            hour=5,
            semester=self.sem_inst,
            batch=self.choosen_batch,
            created_date=timezone.now(),
            modified_date=timezone.now(),
        )
        timetable_obj.save()

        timetable_obj = time_table(
            course=self.another_course,
            department=self.choosen_course.department,
            day=day.objects.get(day_name="Monday"),
            hour=3,
            semester=self.sem_inst,
            batch=self.choosen_batch,
            created_date=timezone.now(),
            modified_date=timezone.now(),
        )
        timetable_obj.save()

        timetable_obj = time_table(
            course=self.another_course,
            department=self.choosen_course.department,
            day=day.objects.get(day_name="Monday"),
            hour=4,
            semester=self.sem_inst,
            batch=self.choosen_batch,
            created_date=timezone.now(),
            modified_date=timezone.now(),
        )
        timetable_obj.save()

    def test_hour_grouping_function(self):
        self.login_staff()
        self.create_semester_plan()
        self.create_timetable_entries()
        time_table_query = time_table.objects.filter(batch=self.choosen_batch)
        # print(vars(time_table_query))

        list_of_date_hours = []

        date_inst = timezone.now().date()

        for entry in time_table_query:
            temp = {}
            temp['date'] = date_inst
            temp['hour'] = entry.hour
            temp['semester'] = entry.semester
            temp['course'] = entry.course
            list_of_date_hours.append(temp)

        list_of_date_hours = sorted(list_of_date_hours, key=lambda x: (x['date'], x['hour']))
        return_list = get_hours_grouped_for_day(list_of_date_hours)

        expected_list = []

        temp = {}
        temp['course_id'] = self.choosen_course.course_id
        temp['course_name'] = self.choosen_course.code
        temp['start_hour'] = 1
        temp['end_hour'] = 1
        temp['bulk'] = False
        temp['semester'] = self.sem_inst.pk
        temp['department_acronym'] = self.sem_inst.department.acronym
        temp['date'] = date_inst
        temp['display_date'] = date_inst.strftime("%d-%m-%Y")
        expected_list.append(temp)

        temp = {}
        temp['course_id'] = self.another_course.course_id
        temp['course_name'] = self.another_course.code
        temp['start_hour'] = 5
        temp['end_hour'] = 5
        temp['bulk'] = False
        temp['semester'] = self.sem_inst.pk
        temp['department_acronym'] = self.sem_inst.department.acronym
        temp['date'] = date_inst
        temp['display_date'] = date_inst.strftime("%d-%m-%Y")
        expected_list.append(temp)

        temp = {}
        temp['course_id'] = self.another_course.course_id
        temp['course_name'] = self.another_course.code
        temp['start_hour'] = 3
        temp['end_hour'] = 4
        temp['bulk'] = True
        temp['semester'] = self.sem_inst.pk
        temp['department_acronym'] = self.sem_inst.department.acronym
        temp['date'] = date_inst
        temp['display_date'] = date_inst.strftime("%d-%m-%Y")
        expected_list.append(temp)

        temp = {}
        temp['course_id'] = self.choosen_course.course_id
        temp['course_name'] = self.choosen_course.code
        temp['start_hour'] = 6
        temp['end_hour'] = 7
        temp['bulk'] = True
        temp['semester'] = self.sem_inst.pk
        temp['department_acronym'] = self.sem_inst.department.acronym
        temp['date'] = date_inst
        temp['display_date'] = date_inst.strftime("%d-%m-%Y")
        expected_list.append(temp)

        print(return_list)

        print()
        print()
        print(expected_list)

        self.maxDiff = None
        self.assertEqual(return_list, expected_list)
