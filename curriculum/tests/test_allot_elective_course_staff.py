import unittest

from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import staff, CustomUser, active_batches
from curriculum.models import courses, semester, semester_migration, elective_course_staff


@unittest.skip('enrollment in next update')
class allotElectiveCourseStaffTest(TestCase):
    fixtures = [
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choosen_elective_course = courses.objects.filter(department__acronym='CSE').filter(programme='UG').filter(
            semester=1).filter(is_elective=True).order_by('?').first()
        course_instance = courses.objects.get(pk=self.choosen_elective_course.pk)
        course_instance.semester += 1
        course_instance.save()
        active_batch_inst = active_batches.objects.filter(department__acronym='CSE').filter(programme='UG').get(
            current_semester_number_of_this_batch=1)
        self.choosen_batch = active_batch_inst.batch
        self.choose_staff = staff.objects.filter(department__acronym='CSE').order_by('?').first()
        self.another_staff = staff.objects.filter(department__acronym='CSE').exclude(pk=self.choose_staff.pk).order_by(
            '?').first()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(self.choose_staff.user)
        g.save()
        self.choosen_date = timezone.now().date()

    def create_semester_plan(self):
        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.choosen_batch.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        print(vars(response))
        self.expected_semester = semester.objects.filter(batch=self.choosen_batch).filter(
            faculty_advisor=self.choose_staff)
        self.assertTrue(
            self.expected_semester.exists()
        )

        self.assertTrue(
            semester_migration.objects.filter(semester=self.expected_semester).exists()
        )

    def test_access_without_login(self):
        url_string = '/prepare_elective_course/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='open_elective_courses/allot_elective_course_staff.html')

    def test_access_without_permission(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/prepare_elective_course/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='open_elective_courses/allot_elective_course_staff.html')

    def test_access_with_permission(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        self.create_semester_plan()

        url_string = '/prepare_elective_course/'
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='open_elective_courses/allot_elective_course_staff.html')

    def test_normal_get(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)
        self.create_semester_plan()
        list_of_courses = []
        list_of_staffs = []
        for each in courses.objects.filter(department=self.choose_staff.department, is_elective=True,
                                           semester=self.expected_semester.get().semester_number + 1,
                                           programme=self.choosen_batch.programme):
            temp_list = {}
            temp_list['course_id'] = each.course_id
            if elective_course_staff.objects.filter(course=each.course_id):
                for instance in elective_course_staff.objects.filter(course=each.course_id):
                    temp_list['staff_id'] = instance.staff_id
            temp_list['course_name'] = each.course_name
            temp_list['code'] = each.code
            temp_list['semester'] = each.semester
            list_of_courses.append(temp_list)

        self.no_of_courses = len(list_of_courses)

        for each in staff.objects.filter(user__is_approved=True).filter(
                department=self.choose_staff.department).exclude(
            designation='Network Admin'):
            list_of_staffs.append(each)
        for each in staff.objects.filter(user__is_approved=True).exclude(
                department=self.choose_staff.department).exclude(
            designation='Network Admin'):
            list_of_staffs.append(each)

        url_string = '/prepare_elective_course/'
        response = self.client.get(url_string)
        print()
        print()
        print("response")
        print(vars(response))
        #
        # self.assertEqual(
        #     response.context['no_of_courses'],self.no_of_courses
        # )

        self.assertEqual(
            response.context['list_of_courses'], list_of_courses
        )

        self.assertEqual(
            response.context['list_of_staffs'], list_of_staffs
        )

    def test_post_with_data(self):

        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)
        self.create_semester_plan()

        url_string = '/prepare_elective_course/'
        post_data = {
            u'assign': [u'abc'],
            u'no_of_courses': int(1),
            u'select1': [self.another_staff.pk],
            u'course1': [self.choosen_elective_course.pk],
            u'shortcode1': ['test'],
            u'selected_semester': [self.choosen_batch.pk],
        }
        response = self.client.post(url_string, post_data)

        list_of_courses = []
        list_of_staffs = []
        for each in courses.objects.filter(department=self.choose_staff.department, is_elective=True,
                                           semester=self.expected_semester.get().semester_number + 1,
                                           programme=self.choosen_batch.programme):
            temp_list = {}
            temp_list['course_id'] = each.course_id
            if elective_course_staff.objects.filter(course=each.course_id):
                for instance in elective_course_staff.objects.filter(course=each.course_id):
                    temp_list['staff_id'] = instance.staff_id
                    if instance.enabled:
                        temp_list['enabled'] = True
            temp_list['course_name'] = each.course_name
            temp_list['code'] = each.code
            temp_list['semester'] = each.semester
            list_of_courses.append(temp_list)

        print(list_of_courses)

        self.no_of_courses = len(list_of_courses)

        for each in staff.objects.filter(user__is_approved=True).filter(
                department=self.choose_staff.department).exclude(
            designation='Network Admin'):
            list_of_staffs.append(each)
        for each in staff.objects.filter(user__is_approved=True).exclude(
                department=self.choose_staff.department).exclude(
            designation='Network Admin'):
            list_of_staffs.append(each)
        self.assertEqual(
            response.context['no_of_courses'], self.no_of_courses
        )

        self.assertEqual(
            response.context['list_of_courses'], list_of_courses
        )

        self.assertEqual(
            response.context['list_of_staffs'], list_of_staffs
        )

        updated_course_inst = courses.objects.get(pk=self.choosen_elective_course.pk)

        self.assertEqual(
            updated_course_inst.code, 'TEST'
        )

        self.assertTrue(
            elective_course_staff.objects.filter(course=self.choosen_elective_course).filter(
                staff=self.another_staff).exists()
        )

        # check reassigns properly

        url_string = '/prepare_elective_course/'
        post_data = {
            u'assign': [u'abc'],
            u'no_of_courses': int(1),
            u'select1': [self.choose_staff.pk],
            u'course1': [self.choosen_elective_course.pk],
            u'shortcode1': ['src'],
            u'selected_semester': [self.choosen_batch.pk],
        }
        response = self.client.post(url_string, post_data)

        list_of_courses = []
        list_of_staffs = []
        for each in courses.objects.filter(department=self.choose_staff.department, is_elective=True,
                                           semester=self.expected_semester.get().semester_number + 1,
                                           programme=self.choosen_batch.programme):
            temp_list = {}
            temp_list['course_id'] = each.course_id
            if elective_course_staff.objects.filter(course=each.course_id):
                for instance in elective_course_staff.objects.filter(course=each.course_id):
                    temp_list['staff_id'] = instance.staff_id
                    if instance.enabled:
                        temp_list['enabled'] = True
            temp_list['course_name'] = each.course_name
            temp_list['code'] = each.code
            temp_list['semester'] = each.semester
            list_of_courses.append(temp_list)

        self.no_of_courses = len(list_of_courses)

        for each in staff.objects.filter(user__is_approved=True).filter(
                department=self.choose_staff.department).exclude(
            designation='Network Admin'):
            list_of_staffs.append(each)
        for each in staff.objects.filter(user__is_approved=True).exclude(
                department=self.choose_staff.department).exclude(
            designation='Network Admin'):
            list_of_staffs.append(each)

        self.assertEqual(
            response.context['no_of_courses'], self.no_of_courses
        )
        self.assertEqual(
            response.context['list_of_courses'], list_of_courses
        )

        self.assertEqual(
            response.context['list_of_staffs'], list_of_staffs
        )

        updated_course_inst = courses.objects.get(pk=self.choosen_elective_course.pk)

        self.assertEqual(
            updated_course_inst.code, 'SRC'
        )

        self.assertFalse(
            elective_course_staff.objects.filter(course=self.choosen_elective_course).filter(
                staff=self.another_staff).exists()
        )
        self.assertTrue(
            elective_course_staff.objects.filter(course=self.choosen_elective_course).filter(
                staff=self.choose_staff).exists()
        )
