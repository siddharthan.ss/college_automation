import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.test import tag
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait


@tag('selenium')
class PC(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super(PC, cls).setUpClass()
        cls.browser = WebDriver()
        cls.browser.maximize_window()

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super(PC, cls).tearDownClass()

    def logoutFA1(self):
        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@class="user-profile dropdown-toggle"]')))
        system.click()
        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located((By.XPATH, '//a[@href="/logout/"]')))
        system.click()

    def loginIntoFA1(self):
        self.browser.find_element_by_id('login').click()
        self.assertIn('GCT | Login', self.browser.title)
        username_input = self.browser.find_element_by_name("email")
        username_input.send_keys('fa1@gmail.com')
        password_input = self.browser.find_element_by_name("password")
        password_input.send_keys('123456')
        self.browser.find_element_by_name("login").click()

    def assignFA(self):
        self.browser.find_element_by_xpath("id('sidebar-menu')/div/ul/li[1]/a").click()
        time.sleep(3)
        self.browser.find_element_by_xpath("id('sidebar-menu')/div/ul/li[1]/ul/li[1]/a").click()
        time.sleep(3)
        # self.browser.find_element_by_xpath("id('UG-2016-2020-6')").click()
        # time.sleep(5)
        self.browser.find_element_by_xpath("id('create_semester')/div[1]/div/span/span[1]/span/ul").click()
        time.sleep(5)
        self.browser.find_element_by_xpath("//li[contains(@id, 'faculty_advisor-result')][text()='FA1 of cse']").click()
        time.sleep(5)
        self.browser.find_element_by_name("semester_plan").click()
        time.sleep(5)

    def availforapproval(self):
        self.browser.find_element_by_xpath("id('sidebar-menu')/div/ul/li[5]/a").click()

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.element_to_be_clickable((By.XPATH, "id('sidebar-menu')/div/ul/li[5]/ul/li[1]/a")))
        system.click()

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, 'Student Alias A')))

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, 'student alias D')))

    def assigntimetable(self):
        self.browser.find_element_by_xpath("id('sidebar-menu')/div/ul/li[3]/a").click()
        self.browser.find_element_by_xpath("id('sidebar-menu')/div/ul/li[3]/ul/li[1]/a").click()

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located(
            (By.XPATH, "/html/body/div[1]/div[1]/div[3]/div[1]/div/div[2]/div/form/ul/li[2]/input")))
        system = wait.until(EC.element_to_be_clickable(
            (By.XPATH, "/html/body/div[1]/div[1]/div[3]/div[1]/div/div[2]/div/form/ul/li[2]/input")))
        system.click()

        # wait = WebDriverWait(self.browser, 15)
        # system = wait.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, 'Allot Faculty Advisor to the batch in order to create timetable')))
        # system.click()

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located(
            (By.XPATH, "/html/body/div[1]/div[1]/div[3]/div[1]/div/div[2]/div/form/ul/li[1]/input")))
        system = wait.until(EC.element_to_be_clickable(
            (By.XPATH, "/html/body/div[1]/div[1]/div[3]/div[1]/div/div[2]/div/form/ul/li[1]/input")))
        system.click()

        # self.browser.find_element_by_xpath("/html/body/div[1]/div[1]/div[3]/div[1]/div/div[2]/div/form/ul/li[2]/input").click()
        # self.browser.find_elements_by_partial_link_text('Allot Faculty Advisor to the batch in order to create timetable')
        '''assign chemistry lab hour'''
        # self.browser.find_element_by_xpath("/html/body/div[1]/div[1]/div[3]/div[1]/div/div[2]/div/form/ul/li[1]/input").click()

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located((By.XPATH, "id('1')/div/table/tbody/tr[3]/td[7]")))
        system = wait.until(EC.element_to_be_clickable((By.XPATH, "id('1')/div/table/tbody/tr[3]/td[7]")))
        system.click()

        # self.browser.find_element_by_xpath("id('1')/div/table/tbody/tr[3]/td[7]").click()
        select = Select(self.browser.find_element_by_id('selected_course'))
        select.select_by_value("16SBS106")
        self.browser.find_element_by_id('conform_period').click()
        time.sleep(10)

        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.presence_of_element_located(
            (By.XPATH, "/html/body/div[1]/div[1]/div[3]/div[1]/div/div[2]/div/form/ul/li[1]/input")))
        # system = wait.until(EC.element_to_be_visible((By.XPATH, "/html/body/div[1]/div[1]/div[3]/div[1]/div/div[2]/div/form/ul/li[1]/input")))
        system.click()

        '''assign engg. phy hour'''
        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.element_to_be_clickable((By.XPATH, "id('1')/div/table/tbody/tr[5]/td[5]")))
        system.click()
        # self.browser.find_element_by_xpath("id('1')/div/table/tbody/tr[5]/td[5]").click()
        select = Select(self.browser.find_element_by_id('selected_course'))
        select.select_by_value("16SBS103")
        self.browser.find_element_by_id('conform_period').click()
        time.sleep(10)
        # wait = WebDriverWait(self.browser, 15)
        # sel = wait.until(EC.visibility_of_element_located((By.XPATH, "id('1')/div/table/tbody/tr[3]/td[7]")))

    def assignstaff(self):
        # self.browser.find_element_by_xpath("id('sidebar-menu')/div/ul/li[2]/a").click()
        wait = WebDriverWait(self.browser, 15)
        system = wait.until(EC.visibility_of_element_located((By.XPATH, "id('sidebar-menu')/div[1]/ul/li[3]/a")))
        system = wait.until(EC.element_to_be_clickable((By.XPATH, "id('sidebar-menu')/div[1]/ul/li[3]/ul/li[2]/a")))
        system.click()
        # self.browser.find_element_by_xpath("id('sidebar-menu')/div/ul/li[2]/ul/li[2]/a").click()
        # wait = WebDriverWait(self.browser, 15)
        # system = wait.until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div[3]/div/div/div[2]/div/form[1]/ul/li[1]/input")))
        # system = wait.until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div/div[3]/div/div/div[2]/div/form[1]/ul/li[1]/input")))
        # system.click()
        # self.browser.find_element_by_xpath("/html/body/div[1]/div/div[3]/div/div/div[2]/div/form[1]/ul/li[1]/input").click()
        '''Staff assigned for Engg. Phy'''
        select = Select(self.browser.find_element_by_id('16SBS103'))
        select.select_by_visible_text("FA1 of cse")
        input_code = self.browser.find_element_by_id('code-16SBS103')
        input_code.clear()
        input_code.send_keys('PHY')
        self.browser.find_element_by_id('assign').click()
        '''Staff assigned for Che. Lab'''
        self.browser.find_element_by_id('CHE').click()
        select = Select(self.browser.find_element_by_id('16SBS106'))
        select.select_by_visible_text("F2 of cse")
        input_code = self.browser.find_element_by_id('code-16SBS106')
        input_code.clear()
        input_code.send_keys('CHEM LAB')
        self.browser.find_element_by_id('assign').click()

    def checktimetable(self):
        wait = WebDriverWait(self.browser, 15)
        # time.sleep(3)
        system = wait.until(EC.visibility_of_element_located((By.XPATH, "id('sidebar-menu')/div[1]/ul/li[3]/a")))
        # time.sleep(3)
        system = wait.until(EC.element_to_be_clickable((By.XPATH, "id('sidebar-menu')/div[1]/ul/li[3]/ul/li[3]/a")))
        system.click()
        # self.browser.find_element_by_xpath("/html/body/div[1]/div/div[3]/div/div/div[2]/ul/li[1]/a").click()
        time.sleep(10)
        text = self.browser.find_element_by_xpath("id('1')/div/table/tbody/tr[3]/td[7]").text
        self.assertIn('CHEM LAB', text)
        text = self.browser.find_element_by_xpath("id('1')/div/table/tbody/tr[5]/td[5]").text
        self.assertIn('PHY', text)

    def test_hod_tasks(self):
        self.browser.get('http://127.0.0.1:8000')
        self.assertIn('GCT', self.browser.title)
        self.loginIntoFA1()
        self.assignFA()
        self.availforapproval()
        self.assigntimetable()
        self.assignstaff()
        self.checktimetable()
        self.logoutFA1()
