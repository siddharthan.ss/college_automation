from datetime import timedelta

from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import staff, CustomUser, batch, student, programme, department_programmes
from curriculum.models import courses, semester, elective_course_staff, time_table, day, \
    student_enrolled_courses, staff_course, one_credit_course_staff, department_sections, programme_coordinator
from curriculum.views import get_current_batches
from enrollment.models import course_enrollment


class ViewTimeTableTest(TestCase):
    fixtures = [
        'day.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'active_batches.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choose_staff = staff.objects.get(user__email='f1@gmail.com')
        self.choose_student = student.objects.get(user__email='s1@gmail.com')
        self.another_staff = staff.objects.filter(department__acronym='CSE').exclude(pk=self.choose_staff.pk).order_by(
            '?').first()
        self.choosen_date = timezone.now().date()

        programme_inst = programme.objects.create(
            name="Under Graduate",
            acronym="UG"
        )
        department_programmes_inst = department_programmes.objects.create(
            department = self.choose_staff.department,
            programme = programme_inst
        )
        pc_table_inst = programme_coordinator.objects.create(
            department_programme = department_programmes_inst
        )
        pc_table_inst.programme_coordinator_staffs.add(self.choose_staff)
        pc_table_inst.save()
        list_of_batches = get_current_batches(self.choose_staff.department)
        self.choosen_batch = batch.objects.get(pk=(list_of_batches[0]['batch_obj']))

        self.choosen_course = courses.objects.get(course_id='16SBS103')
        self.choosen_course.code = 'TEST'
        self.choosen_course.save()
        # create two sections
        self.dept_sec_A = department_sections.objects.get(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='A'
        )
        self.dept_sec_B = department_sections(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='B'
        )
        self.dept_sec_B.save()

    def login_staff(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def login_student(self):
        cust_inst = self.choose_student.user
        cust_inst.is_approved = True
        cust_inst.save()

        self.choose_student.batch = self.choosen_batch
        self.choose_student.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def create_semester_plan(self):
        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.dept_sec_A.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        """
        print('creating sem plan')
        print(self.choose_staff.department)
        print(self.choosen_batch)
        self.sem_inst = semester(
            department=self.choose_staff.department,
            batch = self.choosen_batch,
            semester_number = get_current_semester_of_given_batch(self.choose_staff.department,self.choosen_batch).semester_number
        )
        self.sem_inst.save()"""
        self.sem_inst = semester.objects.get(batch=self.choosen_batch)

    def create_timetable_entries(self):

        hr_counter = 1
        for days in ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']:
            timetable_obj = time_table(
                course=self.choosen_course,
                department=self.choosen_course.department,
                day=day.objects.get(day_name=days),
                hour=hr_counter,
                semester=self.sem_inst,
                batch=self.choosen_batch,
                created_date=timezone.now().date() - timedelta(days=1),
                modified_date=timezone.now().date()
            )
            timetable_obj.save()
            # print(vars(timetable_obj))
            hr_counter += 1
        while hr_counter < 8:
            timetable_obj = time_table(
                course=self.choosen_course,
                department=self.choosen_course.department,
                day=day.objects.get(day_name='Friday'),
                hour=hr_counter,
                semester=self.sem_inst,
                batch=self.choosen_batch,
                created_date=timezone.now(),
                modified_date=timezone.now().date()
            )
            timetable_obj.save()
            # print(vars(timetable_obj))
            hr_counter += 1

    def test_access_without_login(self):
        url_string = '/view_timetable/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='timetable/view_timetable.html')

    def test_access_without_permission(self):
        cust_inst = CustomUser.objects.get(email='net_admin@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='net_admin@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/view_timetable/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='timetable/view_timetable.html')

    def test_access_with_permission(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)
        self.create_semester_plan()

        url_string = '/view_timetable/'
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='timetable/view_timetable.html')

    def test_get_without_loaded_data(self):
        self.login_staff()
        url_string = '/view_timetable/'
        response = self.client.get(url_string)

        # self.assertIsNone(response.context['list_of_entries'])


        msg = {
            'page_title': 'No Semester Plan',
            'title': 'No Semester Plan',
            'description': 'Semester Plan is not yet created, contact your programme coordinator for more information.',
        }
        # expected_display_text = expected_semster_list[0]['display_text']

        self.assertEqual(response.context['message'], msg)

    def test_get_with_one_batch_loaded_tt(self):
        self.login_staff()
        self.create_semester_plan()
        self.create_timetable_entries()

        self.assertEqual(time_table.objects.all().count(), 7)
        self.assertTrue(time_table.objects.filter(day__day_name='Monday', hour=1, semester=self.sem_inst,
                                                  department=self.choose_staff.department).exists())
        # print(self.sem_inst)
        # print(self.choose_staff.department)
        url_string = '/view_timetable/'
        response = self.client.get(url_string)

        list_of_courses = []
        for each in courses.objects.filter(department=self.choose_staff.department,
                                           regulation=self.choosen_batch.regulation,
                                           programme=self.choosen_course.programme,
                                           semester=self.sem_inst.semester_number,
                                           is_open=False, is_one_credit=False):
            temp_list = {}
            temp_list['course_text'] = each
            temp_list['course_id'] = each.course_id
            temp_list['pk'] = each.pk
            staff_list = []
            if each.is_elective:
                for instance in elective_course_staff.objects.filter(course=each):
                    staff_list.append(instance.staff)
                temp_list['staff_list'] = staff_list
            else:
                if staff_course.objects.filter(course=each.course_id, batch=self.choosen_batch, semester=self.sem_inst,
                                               department=self.choose_staff.department):
                    for instance in staff_course.objects.filter(course=each.course_id, batch=self.choosen_batch,
                                                                semester=self.sem_inst,
                                                                department=self.choose_staff.department):
                        for each_staff in instance.staffs.all():
                            for id_instance in staff.objects.filter(staff_id=each_staff.staff_id):
                                staff_id = id_instance.id
                                staff_list.append(str(id_instance))
                        temp_list['staff_list'] = staff_list
            temp_list['course_name'] = each.course_name
            temp_list['code'] = each.code
            list_of_courses.append(temp_list)

        list_of_days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
        available_hours = 8
        list_of_hours = []
        for each_day in list_of_days:
            day_list = []
            current_hour = 1
            while current_hour <= available_hours:
                hour_list = []
                for each_course_hour in time_table.objects.filter(department=self.choose_staff.department,
                                                                  is_active=True,
                                                                  batch=self.choosen_batch, semester=self.sem_inst,
                                                                  day__day_name=each_day, hour=current_hour):
                    course_instance_code = courses.objects.get(course_id=each_course_hour.course_id)
                    if course_instance_code.code:
                        hour_list.append(course_instance_code.code)
                    else:
                        hour_list.append(each_course_hour.course)
                day_list.append(hour_list)
                current_hour = current_hour + 1
            temp = {}
            temp['day_name'] = each_day
            temp['hours'] = day_list
            list_of_hours.append(temp)
        self.assertEqual(response.context['list_of_hours'], list_of_hours)
        self.assertEqual(response.context['list_of_courses'], list_of_courses)
        # self.assertIsNone(response.context['list_of_entries'])

    def enroll_students(self):
        instance = course_enrollment(semester=self.sem_inst,
                                     course=self.choosen_course,
                                     student=self.choose_student,
                                     requested=True,
                                     enrolled_date=timezone.now().date(),
                                     registered=True,
                                    registered_date = timezone.now().date(),
                                    approved = True,
                                     )
        instance.save()

    def test_get_for_student_loaded_tt(self):
        self.login_staff()
        self.create_semester_plan()
        self.create_timetable_entries()
        self.enroll_students()

        self.assertEqual(time_table.objects.all().count(), 7)

        self.login_student()

        url_string = '/view_timetable/'
        response = self.client.get(url_string)

        print(vars(response))

        list_of_courses = []
        for each in courses.objects.filter(department=self.choosen_course.department, regulation=self.choosen_course.regulation,
                                           programme=self.choosen_batch, semester=self.sem_inst.semester_number ):
            if each.is_elective and self.choose_student.batch.regulation.start_year <= 2012:
                if student_enrolled_courses.objects.filter(course=each, student=self.choose_student, approved=True):
                    temp_list = {}
                    temp_list['course_text'] = each
                    temp_list['course_id'] = each.course_id
                    if staff_course.objects.filter(course=each, batch=self.choosen_batch,
                                                   semester=self.sem_inst,
                                                   department=self.choosen_course.department):
                        staff_course_instance = staff_course.objects.get(course=each, batch=self.choosen_batch,
                                                                         semester=self.sem_inst,
                                                                         department=self.choosen_course.department)

                        temp_list['staff_list'] = staff_course_instance.staffs.all()
                    temp_list['course_name'] = each.course_name
                    temp_list['code'] = each.code
                    list_of_courses.append(temp_list)
            elif self.choose_student.batch.regulation.start_year <= 2012:
                temp_list = {}
                temp_list['course_text'] = each
                temp_list['course_id'] = each.course_id
                if staff_course.objects.filter(course=each, batch=self.choosen_batch,
                                               semester=self.sem_inst,
                                               department=self.choosen_course.department):
                    staff_course_instance = staff_course.objects.get(course=each, batch=self.choosen_batch,
                                                                     semester=self.sem_inst,
                                                                     department=self.choosen_course.department)

                    temp_list['staff_list'] = staff_course_instance.staffs.all()
                temp_list['course_name'] = each.course_name
                temp_list['code'] = each.code
                list_of_courses.append(temp_list)
            else:
                if course_enrollment.objects.filter(course=each, semester=self.sem_inst, student=self.choose_student,
                                                    registered=True):
                    temp_list = {}
                    temp_list['course_text'] = each
                    temp_list['course_id'] = each.course_id
                    if staff_course.objects.filter(course=each, batch=self.choosen_batch,
                                                   semester=self.sem_inst,
                                                   department=self.choosen_course.department):
                        staff_course_instance = staff_course.objects.get(course=each, batch=self.choosen_batch,
                                                                         semester=self.sem_inst,
                                                                         department=self.choosen_course.department)

                        temp_list['staff_list'] = staff_course_instance.staffs.all()
                    temp_list['course_name'] = each.course_name
                    temp_list['code'] = each.code
                    list_of_courses.append(temp_list)

        list_of_days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
        available_hours = 8
        list_of_hours = []
        for each_day in list_of_days:
            day_list = []
            current_hour = 1
            while current_hour <= available_hours:
                hour_list = []
                for each_course_hour in time_table.objects.filter(department=self.choose_staff.department,
                                                                  is_active=True,
                                                                  batch=self.choosen_batch, semester=self.sem_inst,
                                                                  day__day_name=each_day, hour=current_hour):
                    print('each_course_hour')
                    print(each_course_hour.pk)
                    course_instance_code = courses.objects.get(pk=each_course_hour.course.pk)
                    if course_instance_code.code:
                        hour_list.append(course_instance_code.code)
                    else:
                        hour_list.append(each_course_hour.course)
                day_list.append(hour_list)
                current_hour = current_hour + 1
            temp = {}
            temp['day_name'] = each_day
            temp['hours'] = day_list
            list_of_hours.append(temp)
        self.maxDiff = None
        self.assertEqual(response.context['list_of_hours'], list_of_hours)
        self.assertEqual(response.context['list_of_courses'], list_of_courses)
        self.assertEqual(list(response.context['faculty_advisors']), list(self.sem_inst.faculty_advisor.all()))
