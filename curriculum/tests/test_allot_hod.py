import unittest

from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import staff, CustomUser, department
from curriculum.views import get_departments


class allotHodTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'student.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choose_principal = staff.objects.get(designation='Principal')
        g = Group.objects.get(name='principal')
        g.user_set.add(self.choose_principal.user)
        g.save()
        self.choosen_date = timezone.now().date()

    def list_of_staff(self, curr_dept):
        list_of_staffs = []
        for each in staff.objects.filter(department_id=curr_dept).filter(user__is_approved=True):
            temp = {}
            temp['staff_name'] = each
            temp['designation'] = each.designation
            temp['department'] = each.department.acronym
            temp['id'] = each.staff_id
            list_of_staffs.append(temp)
        return list_of_staffs

    def login_pri(self):
        cust_inst = self.choose_principal.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def test_access_without_login(self):
        url_string = '/allot_hod/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='allot_hod/allot_hod.html')

    def test_access_student(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/allot_hod/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='allot_hod/allot_hod.html')

    def test_access_hod(self):
        cust_inst = CustomUser.objects.get(email='hod@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='hod@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/allot_hod/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='allot_hod/allot_hod.html')

    def test_access_faculty(self):
        cust_inst = CustomUser.objects.get(email='f1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='f1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/allot_hod/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='allot_hod/allot_hod.html')

    def test_access_faculty_advisor(self):
        cust_inst = CustomUser.objects.get(email='fa1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='fa1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/allot_hod/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='allot_hod/allot_hod.html')

    def test_access_principal(self):
        cust_inst = self.choose_principal.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        url_string = '/allot_hod/'
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='allot_hod/allot_hod.html')

    @unittest.skip("to be fixed")
    def test_get_method(self):
        self.login_pri()
        url_string = '/allot_hod/'
        response = self.client.get(url_string)
        expected_dept_list = get_departments()
        expected_display_name = "Department of " + expected_dept_list[0]['name']
        list_of_stafffs = self.list_of_staff(expected_dept_list[0]['dept_id'])
        self.assertEqual(response.context['department_list'], expected_dept_list)
        self.assertEqual(response.context['display_dept_name'], expected_display_name)
        self.assertEqual(response.context['list_of_staffs'], list_of_stafffs)

    @unittest.skip("to be fixed")
    def test_post_select_dept(self):
        self.login_pri()
        response = self.client.post('/allot_hod/', {u'selected_dept': [u'MECH']})
        self.assertEqual(response.status_code, 200)
        real_dept_obj = department.objects.get(acronym='MECH')
        expected_display_name = "Department of " + real_dept_obj.name
        list_of_stafffs = self.list_of_staff(real_dept_obj.id)
        self.assertEqual(response.context['display_dept_name'], expected_display_name)
        self.assertEqual(response.context['list_of_staffs'], list_of_stafffs)

    def test_(self):
        self.login_pri()
        response = self.client.post('/allot_hod/', {u'selected_dept': [u'IT']})
        self.assertEqual(response.status_code, 200)
        real_dept_obj = department.objects.get(acronym='IT')
        list_of_stafffs = self.list_of_staff(real_dept_obj.id)
        staff_id = list_of_stafffs[0]['id']
        staff_ins = staff.objects.get(staff_id=staff_id)
        group_obj = CustomUser.objects.get(id=staff_ins.user_id)
        response = self.client.post('/allot_hod/', {u'assign': [u'Assign'], u'set_hod': [staff_id]})
        g = Group.objects.get(name='hod')
        g.user_set.add(group_obj)
        g.save()
        checker = CustomUser.objects.get(id=staff_ins.user_id)
        self.assertTrue(checker.groups.filter(name='hod').exists())
        response = self.client.post('/allot_hod/', {u'assign': [u'Assign'], u'set_hod': [u'Not']})
        g = Group.objects.get(name='hod')
        g.user_set.remove(group_obj)
        g.save()
        self.assertFalse(checker.groups.filter(name='hod').exists())
