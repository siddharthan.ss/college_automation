import unittest

from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import staff, active_batches, batch, student
from curriculum.models import courses, student_enrolled_courses
from curriculum.models import one_credit_course_staff, elective_course_staff
from curriculum.views import get_current_batches


@unittest.skip('skipped temporarily')
class selectCoursesTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choose_staff = staff.objects.filter(department__acronym='CSE').order_by('?').first()
        self.choosen_student = student.objects.get(user__email='s1@gmail.com')
        self.another_staff = staff.objects.filter(department__acronym='CSE').exclude(pk=self.choose_staff.pk).order_by(
            '?').first()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(self.choose_staff.user)
        g.save()
        self.choosen_date = timezone.now().date()

        list_of_batches = get_current_batches(self.choose_staff.department)
        self.choosen_batch = batch.objects.get(pk=(list_of_batches[0]['batch_obj']))

        self.choosen_course = courses.objects.get(course_id='12S104')

        # match course semester with current_semester_number_of_this_batch of student

        active_batch_instance = active_batches.objects.filter(
            department=self.choosen_student.department
        ).filter(
            programme=self.choosen_student.qualification
        ).get(
            batch=self.choosen_student.batch
        )

        active_batch_instance.current_semester_number_of_this_batch = self.choosen_course.semester
        active_batch_instance.save()

    def login_pc(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def login_student(self):
        cust_inst = self.choosen_student.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def create_semester_plan(self):
        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.choosen_batch.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)

    def create_one_credit_course_and_staff(self):
        self.choosen_course.is_open = False
        self.choosen_course.is_elective = False
        self.choosen_course.is_one_credit = True
        self.choosen_course.save()

        allotment_inst = one_credit_course_staff(
            course=self.choosen_course,
            staff=self.choose_staff,
            enabled=True
        )
        allotment_inst.save()

    def create_elective_course_and_staff(self):
        self.choosen_course.is_open = False
        self.choosen_course.is_elective = True
        self.choosen_course.is_one_credit = False
        self.choosen_course.save()

        allotment_inst = elective_course_staff(
            course=self.choosen_course,
            staff=self.choose_staff,
            enabled=True
        )
        allotment_inst.save()
        self.assertTrue(self.choosen_course.is_elective)

    def test_access_without_login(self):
        url_string = '/select_my_courses/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)

    def test_access_without_permission(self):
        self.login_pc()

        url_string = '/select_my_courses/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)

    def test_access_with_permission(self):
        self.login_student()

        url_string = '/select_my_courses/'
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='open_elective_courses/select_courses.html')

    def test_blank_get(self):
        self.login_student()

        url_string = '/select_my_courses/'
        response = self.client.get(url_string)
        print(vars(response))
        expected_one_credit_courses = []
        expected_elective_courses = []
        expected_open_courses = []
        expected_requested_one_credit_courses = []
        expected_requested_elective_courses = []
        expected_requested_open_courses = []
        expected_my_one_credit_courses = []
        expected_my_elective_courses = []
        expected_my_open_courses = []

        self.assertEqual(response.context['one_credit_courses'], expected_one_credit_courses)
        self.assertEqual(response.context['elective_courses'], expected_elective_courses)
        self.assertEqual(response.context['open_courses'], expected_open_courses)
        self.assertEqual(response.context['requested_one_credit_courses'], expected_requested_one_credit_courses)
        self.assertEqual(response.context['requested_elective_courses'], expected_requested_elective_courses)
        self.assertEqual(response.context['requested_open_courses'], expected_requested_open_courses)
        self.assertEqual(response.context['my_one_credit_courses'], expected_my_one_credit_courses)
        self.assertEqual(response.context['my_elective_courses'], expected_my_elective_courses)
        self.assertEqual(response.context['my_open_courses'], expected_my_open_courses)

    def test_one_credit_courese_visibility(self):
        self.login_student()
        self.create_one_credit_course_and_staff()

        url_string = '/select_my_courses/'
        response = self.client.get(url_string)
        expected_one_credit_courses = []
        expected_elective_courses = []
        expected_open_courses = []
        expected_requested_one_credit_courses = []
        expected_requested_elective_courses = []
        expected_requested_open_courses = []
        expected_my_one_credit_courses = []
        expected_my_elective_courses = []
        expected_my_open_courses = []

        temp = {}
        temp['course_code'] = self.choosen_course.course_id
        temp['requested'] = False
        temp['approved'] = False
        temp['course'] = self.choosen_course.course_name
        temp['staff'] = self.choose_staff
        temp['staff_id'] = self.choose_staff.staff_id
        expected_one_credit_courses.append(temp)

        self.assertEqual(response.context['one_credit_courses'], expected_one_credit_courses)
        self.assertEqual(response.context['elective_courses'], expected_elective_courses)
        self.assertEqual(response.context['open_courses'], expected_open_courses)
        self.assertEqual(response.context['requested_one_credit_courses'], expected_requested_one_credit_courses)
        self.assertEqual(response.context['requested_elective_courses'], expected_requested_elective_courses)
        self.assertEqual(response.context['requested_open_courses'], expected_requested_open_courses)
        self.assertEqual(response.context['my_one_credit_courses'], expected_my_one_credit_courses)
        self.assertEqual(response.context['my_elective_courses'], expected_my_elective_courses)
        self.assertEqual(response.context['my_open_courses'], expected_my_open_courses)

    def apply_one_credit_course(self):
        url_string = '/select_my_courses/'
        post_data = {
            'request_course_one_credit': 'abc',
            'course': str(self.choosen_course.course_id) + '-' + str(self.choose_staff.staff_id),
        }
        response = self.client.post(url_string, post_data)

        return response

    def test_one_credit_courese_apply(self):
        self.login_student()
        self.create_one_credit_course_and_staff()
        response = self.apply_one_credit_course()

        expected_one_credit_entry = student_enrolled_courses.objects.filter(
            student=self.choosen_student,
            course=self.choosen_course,
            under_staff=self.choose_staff,
            studied_semester=self.choosen_student.current_semester,
            request_course=True,
            approved=False,
            cleared=False
        )
        self.assertTrue(expected_one_credit_entry.exists())

        expected_one_credit_courses = []
        expected_elective_courses = []
        expected_open_courses = []
        expected_requested_one_credit_courses = []
        expected_requested_elective_courses = []
        expected_requested_open_courses = []
        expected_my_one_credit_courses = []
        expected_my_elective_courses = []
        expected_my_open_courses = []

        temp = {}
        temp['course_code'] = self.choosen_course.course_id
        temp['requested'] = True
        temp['approved'] = False
        temp['course'] = self.choosen_course.course_name
        temp['staff'] = self.choose_staff
        temp['staff_id'] = self.choose_staff.staff_id
        expected_requested_one_credit_courses.append(temp)

        self.assertEqual(response.context['one_credit_courses'], expected_one_credit_courses)
        self.assertEqual(response.context['elective_courses'], expected_elective_courses)
        self.assertEqual(response.context['open_courses'], expected_open_courses)
        self.assertEqual(response.context['requested_one_credit_courses'], expected_requested_one_credit_courses)
        self.assertEqual(response.context['requested_elective_courses'], expected_requested_elective_courses)
        self.assertEqual(response.context['requested_open_courses'], expected_requested_open_courses)
        self.assertEqual(response.context['my_one_credit_courses'], expected_my_one_credit_courses)
        self.assertEqual(response.context['my_elective_courses'], expected_my_elective_courses)
        self.assertEqual(response.context['my_open_courses'], expected_my_open_courses)

    def test_one_credit_course_approved(self):
        self.login_student()
        self.create_one_credit_course_and_staff()
        self.maxDiff = None
        response = self.apply_one_credit_course()

        expected_one_credit_entry = student_enrolled_courses.objects.filter(
            student=self.choosen_student,
            course=self.choosen_course,
            under_staff=self.choose_staff,
            studied_semester=self.choosen_student.current_semester,
            request_course=True,
            approved=False,
            cleared=False
        )

        self.assertTrue(expected_one_credit_entry.exists())

        expected_one_credit_entry = expected_one_credit_entry.get()

        expected_one_credit_entry.request_course = False
        expected_one_credit_entry.approved = True
        expected_one_credit_entry.save()

        self.assertTrue(expected_one_credit_entry.approved)

        url_string = '/select_my_courses/'
        response = self.client.get(url_string)
        expected_one_credit_courses = []
        expected_elective_courses = []
        expected_open_courses = []
        expected_requested_one_credit_courses = []
        expected_requested_elective_courses = []
        expected_requested_open_courses = []
        expected_my_one_credit_courses = []
        expected_my_elective_courses = []
        expected_my_open_courses = []

        temp = {}
        temp['course_code'] = self.choosen_course.course_id
        temp['requested'] = False
        temp['approved'] = True
        temp['course'] = self.choosen_course.course_name
        temp['staff'] = self.choose_staff
        temp['staff_id'] = self.choose_staff.staff_id
        expected_my_one_credit_courses.append(temp)

        self.assertEqual(response.context['one_credit_courses'], expected_one_credit_courses)
        self.assertEqual(response.context['elective_courses'], expected_elective_courses)
        self.assertEqual(response.context['open_courses'], expected_open_courses)
        self.assertEqual(response.context['requested_one_credit_courses'], expected_requested_one_credit_courses)
        self.assertEqual(response.context['requested_elective_courses'], expected_requested_elective_courses)
        self.assertEqual(response.context['requested_open_courses'], expected_requested_open_courses)
        self.assertEqual(response.context['my_one_credit_courses'], expected_my_one_credit_courses)
        self.assertEqual(response.context['my_elective_courses'], expected_my_elective_courses)
        self.assertEqual(response.context['my_open_courses'], expected_my_open_courses)

    def test_elective_courese_visibility(self):
        self.login_student()
        self.create_elective_course_and_staff()

        url_string = '/select_my_courses/'
        response = self.client.get(url_string)
        expected_one_credit_courses = []
        expected_elective_courses = []
        expected_open_courses = []
        expected_requested_one_credit_courses = []
        expected_requested_elective_courses = []
        expected_requested_open_courses = []
        expected_my_one_credit_courses = []
        expected_my_elective_courses = []
        expected_my_open_courses = []

        temp = {}
        temp['course_code'] = self.choosen_course.course_id
        temp['requested'] = False
        temp['approved'] = False
        temp['course'] = self.choosen_course.course_name
        temp['staff'] = self.choose_staff
        temp['staff_id'] = self.choose_staff.staff_id
        expected_elective_courses.append(temp)

        self.assertEqual(response.context['one_credit_courses'], expected_one_credit_courses)
        self.assertEqual(response.context['elective_courses'], expected_elective_courses)
        self.assertEqual(response.context['open_courses'], expected_open_courses)
        self.assertEqual(response.context['requested_one_credit_courses'], expected_requested_one_credit_courses)
        self.assertEqual(response.context['requested_elective_courses'], expected_requested_elective_courses)
        self.assertEqual(response.context['requested_open_courses'], expected_requested_open_courses)
        self.assertEqual(response.context['my_one_credit_courses'], expected_my_one_credit_courses)
        self.assertEqual(response.context['my_elective_courses'], expected_my_elective_courses)
        self.assertEqual(response.context['my_open_courses'], expected_my_open_courses)

    def apply_elecitve_course(self):
        url_string = '/select_my_courses/'
        post_data = {
            'request_course_elective': 'abc',
            'course': str(self.choosen_course.course_id) + '-' + str(self.choose_staff.staff_id),
        }
        response = self.client.post(url_string, post_data)

        return response

    def test_elective_courese_apply(self):
        self.login_student()
        self.create_elective_course_and_staff()
        response = self.apply_elecitve_course()

        expected_one_credit_entry = student_enrolled_courses.objects.filter(
            student=self.choosen_student,
            course=self.choosen_course,
            under_staff=self.choose_staff,
            studied_semester=self.choosen_student.current_semester,
            request_course=True,
            approved=False,
            cleared=False
        )
        self.assertTrue(expected_one_credit_entry.exists())

        expected_one_credit_courses = []
        expected_elective_courses = []
        expected_open_courses = []
        expected_requested_one_credit_courses = []
        expected_requested_elective_courses = []
        expected_requested_open_courses = []
        expected_my_one_credit_courses = []
        expected_my_elective_courses = []
        expected_my_open_courses = []

        temp = {}
        temp['course_code'] = self.choosen_course.course_id
        temp['requested'] = True
        temp['approved'] = False
        temp['course'] = self.choosen_course.course_name
        temp['staff'] = self.choose_staff
        temp['staff_id'] = self.choose_staff.staff_id
        expected_requested_elective_courses.append(temp)

        self.assertEqual(response.context['one_credit_courses'], expected_one_credit_courses)
        self.assertEqual(response.context['elective_courses'], expected_elective_courses)
        self.assertEqual(response.context['open_courses'], expected_open_courses)
        self.assertEqual(response.context['requested_one_credit_courses'], expected_requested_one_credit_courses)
        self.assertEqual(response.context['requested_elective_courses'], expected_requested_elective_courses)
        self.assertEqual(response.context['requested_open_courses'], expected_requested_open_courses)
        self.assertEqual(response.context['my_one_credit_courses'], expected_my_one_credit_courses)
        self.assertEqual(response.context['my_elective_courses'], expected_my_elective_courses)
        self.assertEqual(response.context['my_open_courses'], expected_my_open_courses)

    def test_elective_course_approved(self):
        self.login_student()
        self.create_elective_course_and_staff()
        response = self.apply_elecitve_course()
        self.maxDiff = None

        expected_one_credit_entry = student_enrolled_courses.objects.filter(
            student=self.choosen_student,
            course=self.choosen_course,
            under_staff=self.choose_staff,
            studied_semester=self.choosen_student.current_semester,
            request_course=True,
            approved=False,
            cleared=False
        )

        self.assertTrue(expected_one_credit_entry.exists())

        expected_one_credit_entry = expected_one_credit_entry.get()

        expected_one_credit_entry.request_course = False
        expected_one_credit_entry.approved = True
        expected_one_credit_entry.save()

        self.assertTrue(expected_one_credit_entry.approved)

        url_string = '/select_my_courses/'
        response = self.client.get(url_string)
        expected_one_credit_courses = []
        expected_elective_courses = []
        expected_open_courses = []
        expected_requested_one_credit_courses = []
        expected_requested_elective_courses = []
        expected_requested_open_courses = []
        expected_my_one_credit_courses = []
        expected_my_elective_courses = []
        expected_my_open_courses = []

        temp = {}
        temp['course_code'] = self.choosen_course.course_id
        temp['requested'] = False
        temp['approved'] = True
        temp['course'] = self.choosen_course.course_name
        temp['staff'] = self.choose_staff
        temp['staff_id'] = self.choose_staff.staff_id
        expected_my_elective_courses.append(temp)

        self.assertEqual(response.context['one_credit_courses'], expected_one_credit_courses)
        self.assertEqual(response.context['elective_courses'], expected_elective_courses)
        self.assertEqual(response.context['open_courses'], expected_open_courses)
        self.assertEqual(response.context['requested_one_credit_courses'], expected_requested_one_credit_courses)
        self.assertEqual(response.context['requested_elective_courses'], expected_requested_elective_courses)
        self.assertEqual(response.context['requested_open_courses'], expected_requested_open_courses)
        self.assertEqual(response.context['my_one_credit_courses'], expected_my_one_credit_courses)
        self.assertEqual(response.context['my_elective_courses'], expected_my_elective_courses)
        self.assertEqual(response.context['my_open_courses'], expected_my_open_courses)
