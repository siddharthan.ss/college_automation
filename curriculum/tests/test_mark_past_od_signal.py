from django.contrib.auth.models import Group
from django.test import TestCase
from django.utils import timezone

from accounts.models import student, staff, issue_onduty, programme, department_programmes, hourly_onduty_request, hours
from curriculum.models import courses, attendance, semester, semester_migration, time_table, day, department_sections, \
    programme_coordinator
from curriculum.views import get_current_batches


class MarkPastOdSignalTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json',
    ]

    def login_staff(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def create_semester_plan(self):
        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.dept_sec_A.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        self.expected_semester = semester.objects.filter(batch=self.choosen_batch).filter(
            faculty_advisor=self.choose_staff)
        self.assertTrue(
            self.expected_semester.exists()
        )

        self.assertTrue(
            semester_migration.objects.filter(semester=self.expected_semester).exists()
        )

    def setUp(self):
        self.choosen_course = courses.objects.get(course_id='16SBS103')
        self.assertIsNotNone(self.choosen_course, 'Empty course ...!')
        self.choose_student = student.objects.get(user__email='s1@gmail.com')
        self.choose_staff = staff.objects.get(user__email='fa1@gmail.com')

        programme_inst = programme.objects.create(
            name="Under Graduate",
            acronym="UG"
        )
        department_programmes_inst = department_programmes.objects.create(
            department=self.choose_staff.department,
            programme=programme_inst
        )
        pc_table_inst = programme_coordinator.objects.create(
            department_programme=department_programmes_inst
        )
        pc_table_inst.programme_coordinator_staffs.add(self.choose_staff)
        pc_table_inst.save()

        self.choosen_date = timezone.now().date()

        list_of_batches = get_current_batches(self.choose_staff.department)
        self.choosen_batch = self.choose_student.batch

        # create second sections
        self.dept_sec_A = department_sections.objects.get(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='A'
        )
        self.dept_sec_B = department_sections(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='B'
        )
        self.dept_sec_B.save()

        self.login_staff()
        self.create_semester_plan()

        timetable_inst = time_table(
            course=self.choosen_course,
            department=self.choose_student.department,
            day=day.objects.get(day_name=self.choosen_date.strftime("%A")),
            hour=1,
            semester=self.expected_semester.get(),
            batch=self.choosen_batch,
            created_date=timezone.now(),
            modified_date=timezone.now(),
        )
        timetable_inst.save()

        self.attendacance_inst = attendance(
            course=self.choosen_course,
            date=self.choosen_date,
            hour=1,
            staff=self.choose_staff,
            grant_period=self.choosen_date,
            semester=self.expected_semester.get(),
        )
        self.attendacance_inst.save()
        self.attendacance_inst.absent_students.add(self.choose_student)
        self.attendacance_inst.save()

        absent_studs_list = self.attendacance_inst.absent_students.all()
        self.assertIn(self.choose_student, absent_studs_list)

        self.hour_obj = hours(
            hour=1,
            hour_name='First',
        )
        self.hour_obj.save()

    def test_post_hourly_od_marking(self):
        hourly_onduty_request_inst = hourly_onduty_request.objects.create(
            staff=self.choose_staff,
            student=self.choose_student,
            message='src',
            date=self.choosen_date,
        )
        hour_inst = hours.objects.get(hour=1)
        hourly_onduty_request_inst.hour.add(hour_inst)
        hourly_onduty_request_inst.granted = True
        hourly_onduty_request_inst.save()
        print(vars(hourly_onduty_request_inst))

        od_stud_list = self.attendacance_inst.onduty_students.all()

        self.assertIn(self.choose_student, od_stud_list, 'No od has been marked')

    def test_post_od_marks(self):
        issue_onduty_inst = issue_onduty(
            staff=self.choose_staff,
            student=self.choose_student,
            reason='src',
            date=self.choosen_date,
        )
        issue_onduty_inst.save()
        print(vars(issue_onduty_inst))

        od_stud_list = self.attendacance_inst.onduty_students.all()

        self.assertIn(self.choose_student, od_stud_list, 'No od has been marked')
