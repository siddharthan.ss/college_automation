from django.test import TestCase
from django.utils import timezone

from accounts.models import *
from curriculum.models import *


class TestDay(TestCase):
    def test_day_model_string_value(self):
        day.objects.create(day_name='Sunday')
        obj = day.objects.get(day_name='Sunday')
        self.assertEqual(str(obj), 'Sunday')
        self.assertNotEqual(str(obj), 'Monday')


class TestSemester(TestCase):
    fixtures = ['regulation.json', 'department.json', 'batch.json']

    def test_semester_string_value(self):
        department_instance = department.objects.get(acronym='CSE')
        choosen_batch = batch.objects.filter(start_year='2016').filter(end_year='2018').get(
            programme='PG'
        )
        dept_sec_A = department_sections(
            department=department_instance,
            batch=choosen_batch,
            section_name='A'
        )
        dept_sec_A.save()
        semester.objects.create(
            department=department_instance,
            semester_number=1,
            batch=choosen_batch,
            department_section=dept_sec_A
        )
        obj = semester.objects.get(department=department_instance)
        expected_string = str(department_instance) + '_sem=' + str(1) + '_' + str(choosen_batch.programme) + '_' + str(
            dept_sec_A.section_name)
        self.assertEqual(str(obj), expected_string)


class TestCoursesTable(TestCase):
    fixtures = ['department.json']

    def setUp(self):
        self.reg_obj = regulation.objects.create(
            start_year=2016,
            end_year=2019,
        )

    def test_coureses_string_value(self):
        department_instance = department.objects.get(acronym='IT')
        courses.objects.create(
            department=department_instance,
            semester=2,
            course_id='12S603',
            course_name='Testing Django',
            subject_type='T',
            CAT='HS',
            CAT_marks=50,
            end_semester_marks=50,
            L_credits=2,
            T_credits=3,
            P_credits=2,
            C_credits=1,
            regulation=self.reg_obj
        )

        obj = courses.objects.get(course_id='12S603')

        self.assertEqual(str(obj), '12S603-Testing Django')


class TestTimetable(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'department.json',
        'staff.json',
        'batch.json',
        'day.json',
    ]

    def setUp(self):
        dept_inst = department.objects.get(acronym="CSE")
        batch_inst = batch.objects.filter(start_year=2016).get(end_year=2020)
        self.choose_staff = staff.objects.get(user__email='fa1@gmail.com')
        reg_obj = regulation.objects.create(
            start_year=2016,
            end_year=2019,
        )
        self.course_obj = courses.objects.create(
            department=dept_inst,
            semester=2,
            course_id='12S603',
            course_name='Testing Django',
            subject_type='T',
            CAT='HS',
            CAT_marks=50,
            end_semester_marks=50,
            L_credits=2,
            T_credits=3,
            P_credits=2,
            C_credits=1,
            regulation=reg_obj
        )
        dept_sec_A = department_sections(
            department=dept_inst,
            batch=batch_inst,
            section_name='A'
        )
        dept_sec_A.save()
        semester_inst = semester(
            department=dept_inst,
            batch=batch_inst,
            semester_number=1,
            department_section=dept_sec_A
        )
        semester_inst.save()
        self.sem_inst = semester_inst
        self.tt = time_table(
            course=self.course_obj,
            department=dept_inst,
            day=day.objects.get(day_name='Monday'),
            hour=1,
            semester=self.sem_inst,
            batch=batch_inst,
            created_date=timezone.now(),
            modified_date=timezone.now(),
        )
        self.tt.save()

    def test_timetable_get_course(self):
        course_instance = self.course_obj
        time_table_instance = self.tt
        self.assertEqual(course_instance, time_table_instance.get_course)


class TestCollegeSchedule(TestCase):
    fixtures = ['regulation.json', 'college_schedule.json']

    def test_college_schedule_text_value(self):
        obj = college_schedule.objects.filter(year="first_year").get(hour=1)

        self.assertEqual(str(obj), 'College Schedule for first_year')


class TestStaffCourse(TestCase):
    fixtures = [
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'department.json',
        'staff.json',
        'batch.json',
    ]

    def setUp(self):
        self.dept_inst = department.objects.get(acronym="CSE")
        self.batch_inst = batch.objects.filter(start_year=2016).get(end_year=2020)
        reg_obj = regulation.objects.create(
            start_year=2016,
            end_year=2019,
        )
        self.course_inst = courses.objects.create(
            department=self.dept_inst,
            semester=2,
            course_id='12S603',
            course_name='Testing Django',
            subject_type='T',
            CAT='HS',
            CAT_marks=50,
            end_semester_marks=50,
            L_credits=2,
            T_credits=3,
            P_credits=2,
            C_credits=1,
            regulation=reg_obj
        )
        self.staff_inst = staff.objects.filter(first_name='F1').get(last_name='cse')
        self.department_inst = self.staff_inst.department

        dept_sec_A = department_sections(
            department=self.dept_inst,
            batch=self.batch_inst,
            section_name='A'
        )
        dept_sec_A.save()
        semester_inst = semester(
            department=self.department_inst,
            batch=self.batch_inst,
            semester_number=1,
            department_section=dept_sec_A
        )
        semester_inst.save()
        self.sem_inst = semester_inst

        staff_course_inst = staff_course(
            course=self.course_inst,
            batch=self.batch_inst,
            semester=self.sem_inst,
            department=self.dept_inst
        )

        staff_course_inst.save()
        staff_course_inst.staffs.add(self.staff_inst)
        staff_course_inst.save()

    def test_staff_course_string(self):
        expected_string =  str(self.course_inst) + '-' + str(self.sem_inst) + '-' + str(self.batch_inst)

        staff_course_inst = staff_course.objects.filter(
            course=self.course_inst
        ).filter(
            staffs=self.staff_inst
        ).get(
            semester=self.sem_inst
        )

        self.assertEqual(expected_string, str(staff_course_inst))
