import unittest
from datetime import timedelta

from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import staff, CustomUser, student, batch, issue_onduty, programme, department_programmes
from curriculum.models import courses, attendance, semester, semester_migration, staff_course, department_sections, \
    programme_coordinator
from curriculum.views import get_current_batches, get_list_of_students

"""
* present normally marks
* absent normally marks
* od normally marks
* reallowed normally works
* reallow contains already marked list
* student list correctly fetches
* attendance marked date is always same not current date
"""


class attendanceTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'regulation.json',
        'college_schedule.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'student.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choosen_course = courses.objects.get(course_id='16SBS103')
        self.assertIsNotNone(self.choosen_course, 'Empty course ...!')
        self.choose_student = student.objects.get(user__email='s1@gmail.com')
        self.choose_staff = staff.objects.get(user__email='fa1@gmail.com')

        programme_inst = programme.objects.create(
            name="Under Graduate",
            acronym="UG"
        )
        department_programmes_inst = department_programmes.objects.create(
            department=self.choose_staff.department,
            programme=programme_inst
        )
        pc_table_inst = programme_coordinator.objects.create(
            department_programme=department_programmes_inst
        )
        pc_table_inst.programme_coordinator_staffs.add(self.choose_staff)
        pc_table_inst.save()

        self.choosen_date = timezone.now().date()

        list_of_batches = get_current_batches(self.choose_staff.department)
        self.choosen_batch = batch.objects.get(pk=(list_of_batches[0]['batch_obj']))

        # create two sections
        self.dept_sec_A = department_sections(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='A'
        )
        self.dept_sec_A.save()
        self.dept_sec_B = department_sections(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='B'
        )
        self.dept_sec_B.save()

    def login_staff(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def create_semester_plan(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.dept_sec_A.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        self.expected_semester = semester.objects.filter(batch=self.choosen_batch).filter(
            faculty_advisor=self.choose_staff)
        self.assertTrue(
            self.expected_semester.exists()
        )

        self.assertTrue(
            semester_migration.objects.filter(semester=self.expected_semester).exists()
        )

    def allot_staff(self):
        allot_staff_inst = staff_course(
            course=self.choosen_course,
            batch=self.choosen_batch,
            semester=self.expected_semester.get(),
            department=self.choosen_course.department
        )

        allot_staff_inst.save()
        allot_staff_inst.staffs.add(self.choose_staff)
        allot_staff_inst.save()

    """
    def test_reallow_correctly_returns_absent_od(self):
        self.login_staff()
        self.create_semester_plan()
        self.allot_staff()
        student_custint = self.choose_student.user
        student_custint.is_approved = True
        student_custint.save()
        self.choose_student.current_semester = self.choosen_course.semester
        self.choose_student.qualification = self.choosen_course.programme
        self.choose_student.save()
        att_obj = attendance(
            course=self.choosen_course,
            date=self.choosen_date,
            hour=1,
            staff=self.choose_staff,
            granted_edit_access=True,
            grant_period=timezone.now() + timedelta(hours=2),
            semester=self.expected_semester.get(),
        )
        att_obj.save()
        att_obj.absent_students.add(self.choose_student)
        att_obj.save()

        another_student = student.objects.get(user__email='s2@gmail.com')
        cust_inst = another_student.user
        cust_inst.is_approved =True
        cust_inst.save()

        another_student.current_semester = self.choosen_course.semester
        another_student.qualification = self.choosen_course.programme
        another_student.save()

        att = attendance.objects.filter(
            course=self.choosen_course
        ).filter(
            date=self.choosen_date
        ).filter(
            staff=self.choose_staff
        ).get(
            hour=1
        )
        att.onduty_students.add(self.choose_student)
        print(vars(att.onduty_students))
        """

    def test_access_without_login(self):

        self.create_semester_plan()
        self.client.logout()
        url_string = '/attendance/' + str(self.expected_semester.get().pk) + '/' + str(
            self.choosen_course.course_id) + '/' + str(self.choosen_date) + '/' + '1'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='curriculum/attendance.html')

    def test_access_without_permission(self):
        self.create_semester_plan()
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/attendance/' + str(self.expected_semester.get().pk) + '/' + str(
            self.choosen_course.course_id) + '/' + str(self.choosen_date) + '/' + '1'

        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='curriculum/attendance.html')

    def test_access_with_permission(self):
        self.login_staff()
        self.create_semester_plan()
        self.allot_staff()

        url_string = '/attendance/' + str(self.expected_semester.get().pk) + '/' + str(
            self.choosen_course.course_id) + '/' + str(self.choosen_date) + '/' + '1'
        response = self.client.get(url_string)
        print()
        print()
        print()
        print("wout perm")
        print(vars(response))
        self.assertTemplateUsed(response, template_name='curriculum/attendance.html')

    @unittest.skip('currently commented out')
    def test_access_with_invalid_course(self):
        self.create_semester_plan()
        cust_inst = CustomUser.objects.get(email='f2@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        url_string = '/attendance/' + str(self.expected_semester.get().pk) + '/' + str(
            self.choosen_course.course_id) + '/' + str(self.choosen_date) + '/' + '1'
        response = self.client.get(url_string)

        modal = {
            'heading': 'Error',
            'body': 'Your are not allowed to mark attendance for this course',
        }

        self.assertTrue(response.context['toggle_model'])
        self.assertEqual(response.context['modal'], modal)
        self.assertTemplateUsed(response, 'dashboard/dashboard_content.html')

    def test_absent_normally_marks(self):
        self.login_staff()
        self.create_semester_plan()
        self.allot_staff()
        url_string = '/attendance/' + str(self.expected_semester.get().pk) + '/' + str(
            self.choosen_course.course_id) + '/' + str(self.choosen_date) + '/' + '1'
        post_data = {
            u'absent': [self.choose_student.roll_no],
        }
        response = self.client.post(url_string, post_data)

        modal = {
            'heading': 'Success',
            'body': 'Your have successfully markked attendance.\nIncase if you need to change your attendance contact HOD of corresponding subject',
        }

        # print(vars(response))

        self.assertTrue(response.context['toggle_model'])
        self.assertEqual(response.context['modal'], modal)
        self.assertTemplateUsed(response, 'dashboard/dashboard_content.html')

        expected_attendance_obj = attendance.objects.filter(
            date=self.choosen_date, hour=1, course=self.choosen_course, staff=self.choose_staff
        )
        self.assertTrue(expected_attendance_obj.exists())

        absent_students = expected_attendance_obj.get().absent_students.all()
        self.assertIn(self.choose_student, absent_students)

    def test_present_normally_marks(self):
        self.login_staff()
        self.create_semester_plan()
        self.allot_staff()
        url_string = '/attendance/' + str(self.expected_semester.get().pk) + '/' + str(
            self.choosen_course.course_id) + '/' + str(self.choosen_date) + '/' + '1'
        post_data = {
            u'present': [self.choose_student.roll_no],
        }
        response = self.client.post(url_string, post_data)

        modal = {
            'heading': 'Success',
            'body': 'Your have successfully markked attendance.\nIncase if you need to change your attendance contact HOD of corresponding subject',
        }

        # print(vars(response))

        self.assertTrue(response.context['toggle_model'])
        self.assertEqual(response.context['modal'], modal)
        self.assertTemplateUsed(response, 'dashboard/dashboard_content.html')

        expected_attendance_obj = attendance.objects.filter(
            date=self.choosen_date, hour=1, course=self.choosen_course, staff=self.choose_staff
        )
        self.assertTrue(expected_attendance_obj.exists())

        present_students = expected_attendance_obj.get().present_students.all()
        self.assertIn(self.choose_student, present_students)

    def test_attendance_already_marked(self):
        self.login_staff()
        self.create_semester_plan()
        self.allot_staff()
        att_obj = attendance(
            course=self.choosen_course,
            date=self.choosen_date,
            hour=1,
            staff=self.choose_staff,
            granted_edit_access=False,
            grant_period=timezone.now() + timedelta(hours=1),
            semester=self.expected_semester.get(),
        )
        att_obj.save()

        url_string = '/attendance/' + str(self.expected_semester.get().pk) + '/' + str(
            self.choosen_course.course_id) + '/' + str(self.choosen_date) + '/' + '1'
        response = self.client.get(url_string)

        msg = {
            'page_title': 'Already Marked',
            'title': 'Attendance already Marked ! ',
            'description': 'You have already marked attendance for this hour.HOD of subject has to grant permission to make changes',
        }
        self.assertTemplateUsed('prompt_pages/error_page_base.html')
        self.assertEqual(response.context['message'], msg)

    def test_attedance_expired_grant_period(self):
        self.login_staff()
        self.create_semester_plan()
        self.allot_staff()
        att_obj = attendance(
            course=self.choosen_course,
            date=self.choosen_date,
            hour=1,
            staff=self.choose_staff,
            granted_edit_access=False,
            grant_period=timezone.now() - timedelta(hours=1),
            semester=self.expected_semester.get(),
        )
        att_obj.save()

        url_string = '/attendance/' + str(self.expected_semester.get().pk) + '/' + str(
            self.choosen_course.course_id) + '/' + str(self.choosen_date) + '/' + '1'
        response = self.client.get(url_string)

        msg = {
            'page_title': 'Time Exceeded',
            'title': 'Why so late?',
            'description': 'You cannot mark attnedance now.Contact HOD of corresponding subject to regain access to mark attendance',
        }
        self.assertTemplateUsed('prompt_pages/error_page_base.html')
        self.assertEqual(response.context['message'], msg)

    def test_normal_attendance_list_get(self):
        self.login_staff()
        self.create_semester_plan()
        self.allot_staff()

        student_custint = self.choose_student.user
        student_custint.is_approved = True
        student_custint.save()

        url_string = '/attendance/' + str(self.expected_semester.get().pk) + '/' + str(
            self.choosen_course.course_id) + '/' + str(self.choosen_date) + '/' + '1'
        response = self.client.get(url_string)

        list_of_students = []
        student_list = get_list_of_students(self.expected_semester.get(), self.choosen_course)
        for entry in student_list:
            stud = entry['student_obj']
            temp_list = {}
            temp_list['student_name'] = stud
            student_register_number = getattr(stud, 'roll_no')
            temp_list['registration_number'] = student_register_number
            temp_list['already_absent'] = False

            if issue_onduty.objects.filter(student=stud).filter(date=self.choosen_date).exists():
                temp_list['granted_od'] = True
            else:
                temp_list['granted_od'] = False
            list_of_students.append(temp_list)

        # print('testing student list ')
        # print(list_of_students)

        print(vars(response))
        self.maxDiff = None
        self.assertEqual(response.context['student_list'], list_of_students)
        self.assertEqual(response.context['course_id'], self.choosen_course.pk)
