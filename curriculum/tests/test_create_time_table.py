from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import staff, CustomUser, active_batches, batch, programme, department_programmes
from curriculum.models import courses, semester, elective_course_staff, time_table, day, \
    staff_course, department_sections, programme_coordinator
from curriculum.views import get_current_batches, get_test_active_semesters_tabs


class createTimeTableTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choose_staff = staff.objects.get(user__email='f1@gmail.com')
        self.another_staff = staff.objects.filter(department__acronym='CSE').exclude(pk=self.choose_staff.pk).order_by(
            '?').first()

        programme_inst = programme.objects.create(
            name="Under Graduate",
            acronym="UG"
        )
        department_programmes_inst = department_programmes.objects.create(
            department=self.choose_staff.department,
            programme=programme_inst
        )
        pc_table_inst = programme_coordinator.objects.create(
            department_programme=department_programmes_inst
        )
        pc_table_inst.programme_coordinator_staffs.add(self.choose_staff)
        pc_table_inst.save()

        self.choosen_date = timezone.now().date()

        print('department  = ' + str(self.choose_staff.department))
        print(active_batches.objects.all())

        list_of_batches = get_current_batches(self.choose_staff.department)
        self.choosen_batch = batch.objects.get(pk=(list_of_batches[0]['batch_obj']))

        self.choosen_course = courses.objects.get(course_id='16SBS103')

        # create two sections
        self.dept_sec_A = department_sections.objects.get(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='A'
        )
        self.dept_sec_A.save()
        self.dept_sec_B = department_sections(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='B'
        )
        self.dept_sec_B.save()

    def login_pc(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def create_semester_plan(self):
        url_string = '/create_semester_plan/'

        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.dept_sec_A.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        self.sem_inst = semester.objects.get(batch=self.choosen_batch)
        # print(vars(self.sem_inst))

    def test_access_without_login(self):
        url_string = '/create_timetable/'
        # print(vars(self.choosen_course  ))
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='timetable/create_timetable.html')

    def test_access_without_permission(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/create_timetable/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)

    def test_access_with_permission(self):
        # cust_inst = self.choose_staff.user
        # cust_inst.is_approved = True
        # cust_inst.save()
        #
        # logged = self.client.login(email=cust_inst.email, password='123456')
        # self.assertTrue(logged)
        self.login_pc()
        self.create_semester_plan()

        url_string = '/create_timetable/'
        response = self.client.get(url_string)
        print('response')
        print(vars(response))
        self.assertTemplateUsed(response, template_name='timetable/create_timetable.html')

    def test_get_without_semester_plan(self):
        self.login_pc()
        url_string = '/create_timetable/'
        response = self.client.get(url_string)

        msg = {
            'page_title': 'No Semester Plan',
            'title': 'No Semester Plan',
            'description': 'Create Semester Plan first, in order to create timetable',
        }

        # expected_display_text = expected_semster_list[0]['display_text']

        self.assertEqual(response.context['message'], msg)

    def test_get_with_semester_plan(self):
        self.login_pc()
        self.create_semester_plan()

        url_string = '/create_timetable/'
        response = self.client.get(url_string)

        expected_semster_list = get_test_active_semesters_tabs(self.choose_staff.user)

        self.assertEqual(response.context['list_of_semesters'], expected_semster_list)

        regulation_intance = self.choosen_batch.regulation
        active_batch_inst = active_batches.objects.filter(
            department=self.choose_staff.department
        ).get(
            batch=self.choosen_batch
        )

        list_of_courses = []
        for each in courses.objects.filter(department=self.choose_staff.department,
                                           regulation=self.choosen_batch.regulation,
                                           programme=self.choosen_course.programme,
                                           semester=self.sem_inst.semester_number):
            temp_list = {}
            temp_list['course_text'] = str(each)
            temp_list['pk'] = each.pk
            temp_list['course_id'] = each.course_id
            staff_list = []
            if each.is_elective:
                for instance in elective_course_staff.objects.filter(course=each):
                    staff_list.append(instance.staff)
                temp_list['staff_list'] = staff_list
                temp_list['course_type'] = '(Open)'
            else:
                if staff_course.objects.filter(course=each.course_id, batch=self.choosen_batch, semester=self.sem_inst,
                                               department=self.choose_staff.department):
                    for instance in staff_course.objects.filter(course=each.course_id, batch=self.choosen_batch,
                                                                semester=self.sem_inst.semester_number,
                                                                department=self.choosen_course.department):
                        for each_staff in instance.staffs.all():
                            for id_instance in staff.objects.filter(staff_id=each_staff.staff_id):
                                staff_id = id_instance.id
                                staff_list.append(str(id_instance))
                        temp_list['staff_list'] = staff_list
            temp_list['course_name'] = each.course_name
            temp_list['code'] = each.code
            list_of_courses.append(temp_list)

        self.assertEqual(list(response.context['list_of_courses']), list(list_of_courses))

        # current_fa = get_facutly_advisors(self.choose_staff.department,self.sem_inst.department_section)

        self.assertEqual(list(response.context['faculty_advisors']), list(self.sem_inst.faculty_advisor.all()))

        self.assertEqual(response.context['list_of_courses'], list_of_courses)
        list_of_days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
        available_hours = 8
        list_of_hours = []
        for each_day in list_of_days:
            day_list = []
            current_hour = 1
            while current_hour <= available_hours:
                hour_list = []
                for each_course_hour in time_table.objects.filter(department=self.choose_staff.department,
                                                                  is_active=True,
                                                                  batch=self.choosen_batch, semester=self.sem_inst,
                                                                  day__day_name=each_day, hour=current_hour):
                    course_instance_code = courses.objects.get(course_id=each_course_hour.course_id)
                    if course_instance_code.code:
                        hour_list.append(course_instance_code.code)
                    else:
                        hour_list.append(each_course_hour.course)
                day_list.append(hour_list)
                current_hour = current_hour + 1
            temp = {}
            temp['day_name'] = each_day
            temp['hours'] = day_list
            list_of_hours.append(temp)
        self.assertEqual(response.context['list_of_hours'], list_of_hours)

    def create_timetable(self):
        url_string = '/create_timetable/'
        post_data = {
            u'selected_semester': [int(self.sem_inst.pk)],
            u'selected_course': [self.choosen_course.course_id],
            u'selected_hour': [u'2'],
            u'selected_day': [u'Monday'],
            u'selected_date': timezone.now().strftime("%d/%m/%Y"),
        }
        response = self.client.post(url_string, post_data)

        return response

    def test_create_timetable_post(self):
        self.login_pc()
        self.create_semester_plan()
        response = self.create_timetable()

        list_of_days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
        available_hours = 8
        list_of_hours = []
        for each_day in list_of_days:
            day_list = []
            current_hour = 1
            while current_hour <= available_hours:
                hour_list = []
                for each_course_hour in time_table.objects.filter(department=self.choose_staff.department,
                                                                  is_active=True,
                                                                  batch=self.choosen_batch, semester=self.sem_inst,
                                                                  day__day_name=each_day, hour=current_hour):
                    course_instance_code = courses.objects.get(course_id=each_course_hour.course_id)
                    if course_instance_code.code:
                        hour_list.append(course_instance_code.code)
                    else:
                        hour_list.append(each_course_hour.course)
                day_list.append(hour_list)
                current_hour = current_hour + 1
            temp = {}
            temp['day_name'] = each_day
            temp['hours'] = day_list
            list_of_hours.append(temp)
        print(vars(response))
        self.assertEqual(response.context['list_of_hours'], list_of_hours)

        sem_obj = semester.objects.filter(department=self.choose_staff.department).get(batch=self.choosen_batch)

        self.assertTrue(
            time_table.objects.filter(course=self.choosen_course, department=self.choosen_course.department,
                                      day=day.objects.get(day_name='Monday'), hour=2, semester=sem_obj,
                                      batch=self.choosen_batch, is_active=True).exists()
        )

    def test_timetable_deletion(self):
        self.login_pc()
        self.create_semester_plan()

        response1 = self.create_timetable()
        response2 = self.create_timetable()

        self.assertFalse(
            time_table.objects.filter(course=self.choosen_course, department=self.choosen_course.department,
                                      day=day.objects.get(day_name='Monday'), hour=2, semester=self.sem_inst,
                                      batch=self.choosen_batch, is_active=False).exists()
        )
