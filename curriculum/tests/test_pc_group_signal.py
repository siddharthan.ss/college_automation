from django.test import TestCase
from django.test import Client

from accounts.models import staff, department_programmes, programme
from curriculum.models import programme_coordinator

class TestPCGropup(TestCase):

    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choose_staff = staff.objects.get(user__email='f1@gmail.com')
        self.another_staff = staff.objects.filter(department__acronym='CSE').exclude(pk=self.choose_staff.pk).order_by(
            '?').first()

        programme_inst = programme.objects.create(
            name="Under Graduate",
            acronym="UG"
        )

        self.department_programmes_inst = department_programmes.objects.create(
            department=self.choose_staff.department,
            programme=programme_inst
        )

    def test_pc_group_add(self):

        pc_inst = programme_coordinator.objects.create(
            department_programme = self.department_programmes_inst
        )
        pc_inst.programme_coordinator_staffs.add(self.choose_staff)
        pc_inst.save()

        self.assertTrue(self.choose_staff.user.groups.filter(name="programme_coordinator").exists())

        pc_inst.programme_coordinator_staffs.add(self.another_staff)
        pc_inst.save()

        self.assertTrue(self.choose_staff.user.groups.filter(name="programme_coordinator").exists())
        self.assertTrue(self.another_staff.user.groups.filter(name="programme_coordinator").exists())

        pc_inst.programme_coordinator_staffs.remove(self.another_staff)
        self.assertTrue(self.choose_staff.user.groups.filter(name="programme_coordinator").exists())

