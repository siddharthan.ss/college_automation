import unittest

from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase

from accounts.models import staff, CustomUser, department, site_settings
from curriculum.views import get_departments, get_first_year_batch_of_department


@unittest.skip('no allot first year')
class allotFirstYearTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'student.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choose_pc = staff.objects.filter(designation='Associate Professor').first()
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(self.choose_pc.user)
        g.save()

    def list_of_staff(self, curr_dept):
        list_of_staffs = []
        for each in staff.objects.filter(department_id=curr_dept).filter(user__is_approved=True):
            temp = {}
            temp['staff_name'] = each
            temp['designation'] = each.designation
            temp['department'] = each.department.acronym
            temp['id'] = each.staff_id
            list_of_staffs.append(temp)
        return list_of_staffs

    def login_pc(self):
        cust_inst = self.choose_pc.user
        cust_inst.is_approved = True
        cust_inst.save()
        query = site_settings.objects.create(key="FIRST_YEAR_MANAGING_DEPARTMENT", value=self.choose_pc.department.pk)

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def test_access_without_login(self):
        url_string = '/allot_staff_first_year/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='allot_staff/allot_staff_first_year.html')

    def test_access_stud(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/allot_staff_first_year/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='allot_staff/allot_staff_first_year.html')

    def test_access_no_permission(self):
        cust_inst = CustomUser.objects.get(email='net_admin@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        url_string = '/allot_staff_first_year/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='allot_staff/allot_staff_first_year.html')

    def test_access_a00dmin(self):
        cust_inst = CustomUser.objects.get(email='itf1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        url_string = '/allot_staff_first_year/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='allot_staff/allot_staff_first_year.html')

    def test_get_prop(self):
        self.login_pc()
        url_string = '/allot_staff_first_year/'
        response = self.client.get(url_string)
        expected_dept_list = get_departments()

        all_core_departments = department.objects.filter(is_core=True)
        if len(all_core_departments) > 0:
            selected_department = all_core_departments[0]
        selected_batch = get_first_year_batch_of_department(selected_department, 'UG').batch
        self.maxDiff = True
        expected_display_name = "Department of " + expected_dept_list[0]['name']
        list_of_stafffs = self.list_of_staff(expected_dept_list[0]['dept_id'])
        # print(response.context['all_core_departments'])
        # print(all_core_departments)
        self.assertEqual(list(response.context['all_core_departments']), list(all_core_departments))
        self.assertEqual(response.context['selected_department'], selected_department)
        self.assertEqual(response.context['selected_batch'], selected_batch)
        self.assertEqual(response.context['course_not_avail'], True)

    def test_post_selected_department_acr(self):
        self.login_pc()
        response = self.client.post('/allot_staff_first_year/', {u'selected_dept_acronym': [u'MECH']})
        self.assertEqual(response.status_code, 200)

    def test_post_choosen_department(self):
        self.login_pc()
        response = self.client.post('/allot_staff_first_year/', {u'chosen_dept_from_select_department': [u'MECH']})
        self.assertEqual(response.status_code, 200)

    def test_post_assign(self):
        self.login_pc()
        response = self.client.post('/allot_staff_first_year/', {u'selected_dept': [u'IT']})
        self.assertEqual(response.status_code, 200)
        real_dept_obj = department.objects.get(acronym='IT')
        list_of_stafffs = self.list_of_staff(real_dept_obj.id)
        staff_id = list_of_stafffs[0]['id']
        staff_ins = staff.objects.get(staff_id=staff_id)
        group_obj = CustomUser.objects.get(id=staff_ins.user_id)
        response = self.client.post('/allot_hod/', {u'assign': [u'Assign'], u'set_hod': [staff_id]})
        g = Group.objects.get(name='hod')
        g.user_set.add(group_obj)
        g.save()
        checker = CustomUser.objects.get(id=staff_ins.user_id)
        self.assertTrue(checker.groups.filter(name='hod').exists())
        response = self.client.post('/allot_hod/', {u'assign': [u'Assign'], u'set_hod': [u'Not']})
        g = Group.objects.get(name='hod')
        g.user_set.remove(group_obj)
        g.save()
        self.assertFalse(checker.groups.filter(name='hod').exists())
