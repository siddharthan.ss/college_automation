from datetime import timedelta

from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import staff, CustomUser, active_batches, department_programmes, programme
from curriculum.models import semester, semester_migration, department_sections, programme_coordinator
from curriculum.views import get_active_batch_tabs


class semesterPlanTest(TestCase):
    fixtures = [
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        active_batch_inst = active_batches.objects.filter(department__acronym='CSE').order_by('?').first()
        self.choosen_batch = active_batch_inst.batch
        self.choose_staff = staff.objects.filter(department__acronym='CSE').order_by('?').first()
        self.another_staff = staff.objects.filter(department__acronym='CSE').exclude(pk=self.choose_staff.pk).order_by(
            '?').first()
        programme_inst = programme.objects.create(
            name="Under Graduate",
            acronym="UG"
        )
        department_programmes_inst = department_programmes.objects.create(
            department = active_batch_inst.department,
            programme = programme_inst
        )
        pc_table_inst = programme_coordinator.objects.create(
            department_programme = department_programmes_inst
        )
        pc_table_inst.programme_coordinator_staffs.add(self.choose_staff)
        pc_table_inst.save()
        self.choosen_date = timezone.now().date()

        # create two sections
        self.dept_sec_A = department_sections.objects.get(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='A'
        )
        self.dept_sec_B = department_sections(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='B'
        )
        self.dept_sec_B.save()



    def test_access_without_login(self):
        url_string = '/create_semester_plan/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='curriculum/semester_plan.html')

    def test_access_without_permission(self):
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/create_semester_plan/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='curriculum/semester_plan.html')

    def test_access_with_permission(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        url_string = '/create_semester_plan/'
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='curriculum/semester_plan.html')

    def test_simple_get_context(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        url_string = '/create_semester_plan/'
        response = self.client.get(url_string)

        queryset = staff.objects.all().filter(department=self.choose_staff.department).filter(user__is_approved=True)
        expected_batches = get_active_batch_tabs(cust_inst)
        # expected_form = semester_plan_form(instance=None,queryset=queryset)


        self.assertEqual(response.context['all_sections'], expected_batches)
        # self.assertEqual(response.context['semester_plan_form'],expected_form)

    def test_post_with_fa_and_st_date(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        print(cust_inst.email)

        # first creation of fa

        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.dept_sec_A.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        post_data_2 = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.dept_sec_B.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response_2 = self.client.post(url_string, post_data_2)
        # print(vars(response))
        self.assertTemplateUsed(response, template_name='curriculum/semester_plan.html')
        self.assertTemplateUsed(response_2, template_name='curriculum/semester_plan.html')

        expected_semester_plan = semester.objects.filter(batch=self.choosen_batch).filter(
            faculty_advisor=self.choose_staff).filter(
            department_section=self.dept_sec_A
        )
        expected_semester_plan_2 = semester.objects.filter(batch=self.choosen_batch).filter(
            faculty_advisor=self.choose_staff).filter(
            department_section=self.dept_sec_B
        )

        self.assertTrue(
            expected_semester_plan.exists()
        )

        self.assertTrue(
            expected_semester_plan_2.exists()
        )

        self.assertEqual(
            expected_semester_plan.get().start_term_1, timezone.now().date()
        )

        expected_semester_migration = semester_migration.objects.filter(semester=expected_semester_plan)
        expected_semester_migration_2 = semester_migration.objects.filter(semester=expected_semester_plan_2)

        self.assertTrue(
            expected_semester_migration.exists()
        )

        self.assertTrue(
            expected_semester_migration_2.exists()
        )

        updated_custom_user_inst = CustomUser.objects.get(email=cust_inst.email)
        print((updated_custom_user_inst.groups.all()))

        self.assertTrue(
            updated_custom_user_inst.groups.filter(name='faculty_advisor').exists()
        )

        # testing adding of fa
        cust_inst_2 = self.another_staff.user
        cust_inst_2.is_approved = True
        cust_inst_2.save()

        yesterday = (timezone.now().date() - timedelta(days=1))

        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.dept_sec_A.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk, self.another_staff.pk],
            u'start_term_1': [yesterday.strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)

        expected_semester_plan = semester.objects.filter(batch=self.choosen_batch).filter(
            faculty_advisor=self.choose_staff).filter(faculty_advisor=self.another_staff).filter(
            department_section=self.dept_sec_A
        )

        self.assertTrue(
            expected_semester_plan.exists()
        )

        self.assertEqual(
            expected_semester_plan.get().start_term_1, yesterday
        )

        updated_custom_user_inst_2 = CustomUser.objects.get(email=self.another_staff.user.email)
        print(updated_custom_user_inst_2.email)
        self.assertTrue(
            updated_custom_user_inst_2.groups.filter(name='faculty_advisor').exists()
        )

    def test_post_with_fa_and_all_dates(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        print(cust_inst.email)

        # first creation of fa

        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.choosen_batch.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
            u'start_term_2': [timezone.now().date().strftime("%d/%m/%Y")],
            u'start_term_3': [timezone.now().date().strftime("%d/%m/%Y")],
            u'end_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
            u'end_term_2': [timezone.now().date().strftime("%d/%m/%Y")],
            u'end_term_3': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        # print(vars(response))
        self.assertTemplateUsed(response, template_name='curriculum/semester_plan.html')

        expected_semester_plan = semester.objects.filter(batch=self.choosen_batch).filter(
            faculty_advisor=self.choose_staff)

        self.assertTrue(
            expected_semester_plan.exists()
        )

        self.assertEqual(
            expected_semester_plan.get().start_term_1, timezone.now().date()
        )
        self.assertEqual(
            expected_semester_plan.get().start_term_2, timezone.now().date()
        )
        self.assertEqual(
            expected_semester_plan.get().start_term_3, timezone.now().date()
        )
        self.assertEqual(
            expected_semester_plan.get().end_term_1, timezone.now().date()
        )
        self.assertEqual(
            expected_semester_plan.get().end_term_2, timezone.now().date()
        )
        self.assertEqual(
            expected_semester_plan.get().end_term_3, timezone.now().date()
        )

        expected_semester_migration = semester_migration.objects.filter(semester=expected_semester_plan)

        self.assertTrue(
            expected_semester_migration.exists()
        )

        updated_custom_user_inst = CustomUser.objects.get(email=cust_inst.email)
        print(updated_custom_user_inst.email)

        self.assertTrue(
            updated_custom_user_inst.groups.filter(name='faculty_advisor').exists()
        )
