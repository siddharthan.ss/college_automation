from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import staff, batch, student, programme, department_programmes
from curriculum.models import courses, semester, time_table, day, \
    reallow_attendance_requests, department_sections, programme_coordinator, hod
from curriculum.views import get_current_batches


class ReallowAttendanceTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json',
    ]

    def setUp(self):
        self.client = Client()
        self.choose_staff = staff.objects.get(user__email='f1@gmail.com')
        self.choose_student = student.objects.get(user__email='s1@gmail.com')
        self.another_staff = staff.objects.filter(department__acronym='CSE').exclude(pk=self.choose_staff.pk).order_by(
            '?').first()
        self.choosen_date = timezone.now().date()

        programme_inst = programme.objects.create(
            name="Under Graduate",
            acronym="UG"
        )
        department_programmes_inst = department_programmes.objects.create(
            department=self.choose_staff.department,
            programme=programme_inst
        )
        pc_table_inst = programme_coordinator.objects.create(
            department_programme=department_programmes_inst
        )
        pc_table_inst.programme_coordinator_staffs.add(self.choose_staff)
        pc_table_inst.save()

        list_of_batches = get_current_batches(self.choose_staff.department)
        self.choosen_batch = batch.objects.get(pk=(list_of_batches[0]['batch_obj']))

        self.choosen_course = courses.objects.get(course_id='16SBS103')
        self.choosen_course.code = 'TEST'
        self.choosen_course.save()

        # create two sections
        self.dept_sec_A = department_sections.objects.get(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='A'
        )
        self.dept_sec_B = department_sections(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='B'
        )
        self.dept_sec_B.save()

    def login_staff(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def login_student(self):
        cust_inst = self.choose_student.user
        cust_inst.is_approved = True
        cust_inst.save()

        self.choose_student.batch = self.choosen_batch
        self.choose_student.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def create_semester_plan(self):
        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.dept_sec_A.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        """
        print('creating sem plan')
        print(self.choose_staff.department)
        print(self.choosen_batch)
        self.sem_inst = semester(
            department=self.choose_staff.department,
            batch = self.choosen_batch,
            semester_number = get_current_semester_of_given_batch(self.choose_staff.department,self.choosen_batch).semester_number
        )
        self.sem_inst.save()"""
        self.sem_inst = semester.objects.get(batch=self.choosen_batch)

    def make_user_as_hod(self):
        hod_instance = hod(
            department = self.choose_staff.department,
            staff = self.choose_staff,
        )

        hod_instance.save()

    def create_timetable_entries(self):

        hr_counter = 1
        for days in ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']:
            timetable_obj = time_table(
                course=self.choosen_course,
                department=self.choosen_course.department,
                day=day.objects.get(day_name=days),
                hour=hr_counter,
                semester=self.sem_inst,
                batch=self.choosen_batch,
                created_date=timezone.now(),

            )
            timetable_obj.save()
            # print(vars(timetable_obj))
            hr_counter += 1
        while hr_counter < 8:
            timetable_obj = time_table(
                course=self.choosen_course,
                department=self.choosen_course.department,
                day=day.objects.get(day_name='Friday'),
                hour=hr_counter,
                semester=self.sem_inst,
                batch=self.choosen_batch,
                created_date=timezone.now()

            )
            timetable_obj.save()
            # print(vars(timetable_obj))
            hr_counter += 1

    def create_attendance_request_entries(self):
        self.reallow_inst = reallow_attendance_requests(
            staff=self.choose_staff,
            date=self.choosen_date,
            course=self.choosen_course,
            hour=2,
            semester=self.sem_inst,
            is_allowed=False,
        )
        self.reallow_inst.save()

    def test_access_without_login(self):
        url_string = '/reallow_attendance_request/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='reallow_attendance/reallow_requests.html')

    def test_access_without_permission(self):
        self.login_staff()

        url_string = '/reallow_attendance_request/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='reallow_attendance/reallow_requests.html')

    def test_access_with_permission(self):
        self.make_user_as_hod()
        self.login_staff()

        url_string = '/reallow_attendance_request/'
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='reallow_attendance/reallow_requests.html')

    def test_normal_get(self):
        self.make_user_as_hod()
        self.login_staff()
        self.create_semester_plan()
        self.create_attendance_request_entries()

        temp = {}
        temp['staff_instance'] = self.choose_staff
        temp['date'] = self.choosen_date
        temp['hour'] = 2
        temp['course'] = self.choosen_course
        temp['reallow_inst_id'] = self.reallow_inst.pk
        expected_entries = []
        expected_entries.append(temp)

        url_string = '/reallow_attendance_request/'
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='reallow_attendance/reallow_requests.html')
        self.maxDiff = None

        self.assertEqual(response.context['list_of_requests'], expected_entries)
