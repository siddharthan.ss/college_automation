import unittest

from django.contrib.auth.models import Group
from django.test import Client
from django.test import TestCase
from django.utils import timezone

from accounts.models import staff, CustomUser, batch, site_settings, regulation, programme, department_programmes
from curriculum.models import courses, semester, staff_course, department_sections, programme_coordinator
from curriculum.views import get_current_batches


class allotStaffTest(TestCase):
    fixtures = [
        'day.json',
        'active_batches.json',
        'college_schedule.json',
        'regulation.json',
        'ct.json',
        'perms.json',
        'group.json',
        'customuser.json',
        'batch.json',
        'department.json',
        'department_sections.json',
        'student.json',
        'section_students.json',
        'staff.json',
        'courses.json'
    ]

    def setUp(self):
        self.client = Client()
        self.choose_staff = staff.objects.filter(department__acronym='CSE').order_by('?').first()
        self.mat_staff = staff.objects.filter(department__acronym='MAT').exclude(pk=self.choose_staff.pk).order_by(
            '?').first()
        self.phy_staff = staff.objects.filter(department__acronym='PHY').exclude(pk=self.choose_staff.pk).order_by(
            '?').first()

        programme_inst = programme.objects.create(
            name="Under Graduate",
            acronym="UG"
        )
        department_programmes_inst = department_programmes.objects.create(
            department=self.choose_staff.department,
            programme=programme_inst
        )
        pc_table_inst = programme_coordinator.objects.create(
            department_programme=department_programmes_inst
        )
        pc_table_inst.programme_coordinator_staffs.add(self.choose_staff)
        pc_table_inst.save()

        self.choosen_date = timezone.now().date()

        list_of_batches = get_current_batches(self.choose_staff.department)
        self.choosen_batch = batch.objects.get(pk=(list_of_batches[0]['batch_obj']))

        self.choosen_course = courses.objects.filter(semester=1).order_by('?').first()
        # create two sections
        self.dept_sec_A = department_sections.objects.get(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='A'
        )
        self.dept_sec_B = department_sections(
            department=self.choose_staff.department,
            batch=self.choosen_batch,
            section_name='B'
        )
        self.dept_sec_B.save()

    def login_pc(self):
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

    def create_semester_plan(self):
        self.assertTrue(self.choose_staff.user.groups.filter(name="programme_coordinator").exists())
        url_string = '/create_semester_plan/'
        post_data = {
            u'semester_plan': [u'abc'],
            u'selected_tab': [self.dept_sec_A.pk],
            u'choosen_faculty_advisors': [self.choose_staff.pk],
            u'start_term_1': [timezone.now().date().strftime("%d/%m/%Y")],
        }
        response = self.client.post(url_string, post_data)
        self.sem_inst = semester.objects.get(batch=self.choosen_batch)

    def test_access_without_login(self):
        url_string = '/allot_staff/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='timetable/allot_staff.html')

    def test_access_without_permission(self):
        self.login_pc()
        self.create_semester_plan()
        cust_inst = CustomUser.objects.get(email='s1@gmail.com')
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email='s1@gmail.com', password='123456')
        self.assertTrue(logged)

        url_string = '/allot_staff/'
        response = self.client.get(url_string)
        self.assertRedirects(response, '/login?next=' + url_string)
        self.assertTemplateNotUsed(response, template_name='timetable/allot_staff.html')

    def test_access_with_permission(self):
        self.login_pc()
        self.create_semester_plan()
        cust_inst = self.choose_staff.user
        cust_inst.is_approved = True
        cust_inst.save()

        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        url_string = '/allot_staff/'
        response = self.client.get(url_string)
        print()
        print()
        print(vars(response))
        print()
        print()
        self.assertTemplateUsed(response, template_name='allot_staff/allot_staff.html')

    def set_first_year_managing_department(self):
        self.selected_department_id = self.mat_staff.department.id
        site_settings.objects.create(
            key="FIRST_YEAR_MANAGING_DEPARTMENT",
            value=str(self.selected_department_id),
        )
        g = Group.objects.get(name='programme_coordinator')
        g.user_set.add(self.mat_staff.user)

    @unittest.skip('not found')
    def test_redirect_eligible_first_year_staff(self):
        self.create_semester_plan()
        self.set_first_year_managing_department()
        cust_inst = self.mat_staff.user
        cust_inst.is_approved = True
        cust_inst.save()
        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        # if not self.mat_staff.department.is_core:
        #     query = site_settings.objects.filter(key="FIRST_YEAR_MANAGING_DEPARTMENT")
        #     managing_department_id = query.get().value
        #     if self.selected_department_id == managing_department_id:

        url_string = '/allot_staff_first_year/'
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='allot_staff/allot_staff_first_year.html')

    @unittest.skip('not found')
    def test_redirect_not_eligible_first_year_staff(self):
        self.create_semester_plan()
        self.set_first_year_managing_department()
        cust_inst = self.phy_staff.user
        cust_inst.is_approved = True
        cust_inst.save()
        logged = self.client.login(email=cust_inst.email, password='123456')
        self.assertTrue(logged)

        # if not self.mat_staff.department.is_core:
        #     query = site_settings.objects.filter(key="FIRST_YEAR_MANAGING_DEPARTMENT")
        #     managing_department_id = query.get().value
        #     if self.selected_department_id == managing_department_id:

        url_string = '/allot_staff_first_year/'
        response = self.client.get(url_string)
        self.assertTemplateNotUsed(response, template_name='allot_staff/allot_staff_first_year.html')

    def test_without_semester_instance(self):
        self.login_pc()

        url_string = '/allot_staff/'
        response = self.client.get(url_string)
        msg = {
            'page_title': 'No Semester Plan',
            'title': 'No Semester Plan',
            'description': 'Create Semester Plan first, in order to create timetable',
        }
        # expected_display_text = expected_semster_list[0]['display_text']

        self.assertEqual(response.context['message'], msg)

    # @unittest.skip('Courses')
    def test_without_courses(self):
        self.login_pc()
        self.create_semester_plan()

        url_string = '/allot_staff/'
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='allot_staff/allot_staff.html')
        no_of_courses = courses.objects.filter(
            semester=self.sem_inst.semester_number,
            department=self.sem_inst.department,
            programme=self.sem_inst.batch.programme,
            regulation=self.sem_inst.batch.regulation
        ).count()
        self.assertEqual(response.context['no_of_courses'], no_of_courses)

    def test_proper_courses_returned(self):
        self.login_pc()
        self.create_semester_plan()
        department_instance = self.choose_staff.department
        batch_programme = 'UG'
        regulation_instance = regulation.objects.get(start_year=2016)
        batch_instance = batch.objects.get(start_year=2016, programme=batch_programme)
        semester_instance = semester.objects.get(department=department_instance, batch=batch_instance,
                                                 semester_number=1)
        current_semester_number = semester_instance.semester_number

        list_of_courses = []
        staff_from_departments = []

        for each in courses.objects.filter(department=department_instance, regulation=regulation_instance,
                                           programme=batch_programme, semester=current_semester_number,
                                           is_elective=False, is_open=False, is_one_credit=False):
            temp_list = {}
            temp_list['text'] = each
            temp_list['pk'] = each.pk
            temp_list['course_id'] = each.course_id
            staff_list = []
            if staff_course.objects.filter(course=each.course_id, batch=batch_instance, semester=semester_instance,
                                           department=department_instance):
                for instance in staff_course.objects.filter(course=each.course_id, batch=batch_instance,
                                                            semester=semester_instance,
                                                            department=department_instance):
                    for each_staff in instance.staffs.all():
                        for id_instance in staff.objects.filter(staff_id=each_staff.staff_id):
                            staff_id = id_instance.id
                            staff_from_departments.append(str(id_instance.department.id))
                        staff_list.append(str(staff_id))
                    # print(staff_list)
                    temp_list['staff_list'] = staff_list
            temp_list['course_name'] = each.course_name
            # print(each.course_name)
            temp_list['code'] = each.code
            list_of_courses.append(temp_list)
        for each in courses.objects.filter(department=department_instance, regulation=regulation_instance,
                                           programme=batch_programme, semester=current_semester_number,
                                           is_one_credit=True):
            temp_list = {}
            temp_list['text'] = each

            temp_list['pk'] = each.pk
            temp_list['course_id'] = each.course_id
            staff_list = []
            if staff_course.objects.filter(course=each.course_id, batch=batch_instance, semester=semester_instance,
                                           department=department_instance):
                for instance in staff_course.objects.filter(course=each.course_id, batch=batch_instance,
                                                            semester=semester_instance,
                                                            department=department_instance):
                    for each_staff in instance.staffs.all():
                        for id_instance in staff.objects.filter(staff_id=each_staff.staff_id):
                            staff_id = id_instance.id
                            staff_from_departments.append(str(id_instance.department.id))
                        staff_list.append(str(staff_id))
                    # print(staff_list)
                    temp_list['staff_list'] = staff_list
            temp_list['course_name'] = each.course_name
            # print(each.course_name)
            temp_list['code'] = each.code
            list_of_courses.append(temp_list)
        for each in courses.objects.filter(department=department_instance, regulation=regulation_instance,
                                           programme=batch_programme, semester=current_semester_number,
                                           is_elective=True):
            temp_list = {}
            temp_list['text'] = each
            temp_list['pk'] = each.pk
            temp_list['course_id'] = each.course_id
            staff_list = []
            if staff_course.objects.filter(course=each.course_id, batch=batch_instance, semester=semester_instance,
                                           department=department_instance):
                for instance in staff_course.objects.filter(course=each.course_id, batch=batch_instance,
                                                            semester=semester_instance,
                                                            department=department_instance):
                    for each_staff in instance.staffs.all():
                        for id_instance in staff.objects.filter(staff_id=each_staff.staff_id):
                            staff_id = id_instance.id
                            staff_from_departments.append(str(id_instance.department.id))
                        staff_list.append(str(staff_id))
                    # print(staff_list)
                    temp_list['staff_list'] = staff_list
            temp_list['course_name'] = each.course_name
            # print(each.course_name)
            temp_list['code'] = each.code
            list_of_courses.append(temp_list)

        url_string = '/allot_staff/'
        response = self.client.get(url_string)
        self.assertTemplateUsed(response, template_name='allot_staff/allot_staff.html')
        self.assertEqual(response.context['list_of_courses'], list_of_courses)


"""

    def test_get_with_semester_plan(self):
        self.login_pc()
        self.create_semester_plan()
        self.maxDiff = None

        url_string = '/allot_staff/'
        response = self.client.get(url_string)

        expected_batch_list = get_current_batches(self.choose_staff.department)

        expected_display_text = expected_batch_list[0]['display_text']
        expected_programme = expected_batch_list[0]['programme']

        self.assertEqual(response.context['batch_list'], expected_batch_list)
        self.assertEqual(response.context['display_text'], expected_display_text)
        self.assertEqual(response.context['display_programme'], expected_programme)
        self.assertTrue(response.context['fa_avail'])

        list_of_staffs = []
        for every in staff.objects.filter(user__is_approved=True).filter(department=self.choose_staff.department).exclude(
                designation='Network Admin'):
            temp = {}
            temp['staff_name'] = str(every)
            temp['department'] = every.department.acronym
            temp['id'] = every.id
            list_of_staffs.append(temp)

        self.assertEqual(response.context['list_of_staffs'], list_of_staffs)

        regulation_intance = self.choosen_batch.regulation
        active_batch_inst = active_batches.objects.filter(
            department=self.choose_staff.department
        ).get(
            batch=self.choosen_batch
        )

        course_list = []
        for each in courses.objects.filter(semester=active_batch_inst.current_semester_number_of_this_batch, programme=self.choosen_batch.programme, department=active_batch_inst.department, is_open=False,is_one_credit=False , regulation=regulation_intance):
            temp_list = {}
            temp_list['course_id'] = each.course_id
            temp_list['course_name'] = each.course_name
            course_list.append(temp_list)

        self.assertEqual(response.context['list_of_courses'], course_list)


        temp_courses_avail = []
        for each in courses.objects.filter(semester=active_batch_inst.current_semester_number_of_this_batch, programme=self.choosen_batch.programme, department=active_batch_inst.department, is_open=False,
                                           is_one_credit=False, regulation=regulation_intance):
            temp_list = {}
            temp_list['course_id'] = each.course_id
            for exist in time_table.objects.filter(course_id=each.course_id, batch=self.choosen_batch):
                temp_list['staff_name'] = str(exist.staff)
                break
            temp_list['course_name'] = each.course_name
            temp_list['code'] = each.code
            temp_courses_avail.append(temp_list)

        self.assertEqual(response.context['courses_avail'],temp_courses_avail)
"""
