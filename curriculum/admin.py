from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

from accounts.models import issue_onduty, onduty_request, hourly_onduty_request, regulation, student_achievements
from curriculum.models import courses, staff_course, time_table, student_enrolled_courses, department_sections, \
    section_students, day, college_schedule, attendance, elective_course_staff, one_credit_course_staff, \
    open_course_staff, semester, hod, programme_coordinator, forceTimetableSlot
from messaging.models import message, draft_box, notifications
from quiz.models import quiz_timings, quiz_questions, attend_quiz_timings, attend_quiz_answers


class CourseAdmin(admin.ModelAdmin):
    list_display = (
        'course_id', 'course_name', 'code', 'semester', 'subject_type', 'programme', 'departments', 'regulation',
        'is_elective', 'is_one_credit', 'is_open')
    list_filter = ('regulation', 'department', 'semester', 'subject_type', 'programme', 'common_course')
    search_fields = ('course_id', 'course_name', 'code')

    ordering = ('semester', 'course_id',)

    def departments(self, obj):
        return obj.department.acronym


class StaffCourseAdmin(admin.ModelAdmin):
    list_display = ('id', 'course', 'staff', 'semester', 'batch')
    list_filter = ('course__regulation', 'department', 'course__programme', 'batch', 'semester')
    search_fields = (
        'course__course_id', 'id', 'course__course_name', 'staffs__first_name', 'staffs__middle_name',
        'staffs__last_name',
        'staffs__user__email')

    ordering = ('semester', 'course__course_id', 'course__course_name', 'id')

    def departments(self, obj):
        return obj.department.acronym

    def staff(self, obj):
        return ", ".join([str(p) for p in obj.staffs.all()])


class TimetableAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'departments', 'course', 'day', 'hour', 'semester', 'batch', 'is_active', 'created_date',
                    'modified_date')
    list_filter = (
        'course__regulation', 'department', 'day', 'hour', 'course__programme', 'batch', 'semester', 'course')
    search_fields = ('course__course_id', 'course__course_name', 'id')

    ordering = ('semester', 'day', 'hour', 'course__course_id', 'course__course_name', 'id')

    def departments(self, obj):
        return obj.department.acronym


class StudentEnrolledCourseAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'departments', 'reg_no', 'student', 'course', 'under_staff', 'studied_semester', 'request_course',
                    'approved',
                    'cleared', 'cleared_semester')
    list_filter = ('course__regulation', 'course__department', 'studied_semester__batch', 'studied_semester')
    search_fields = (
        'course__course_id', 'course__course_name', 'under_staff__first_name', 'under_staff__middle_name',
        'under_staff__last_name', 'under_staff__user__email',
        'student__first_name', 'student__middle_name', 'student__last_name', 'student__roll_no', 'student__user__email',
        'id')

    ordering = ('course__course_id', 'student__roll_no', 'under_staff', 'id')

    def departments(self, obj):
        return str(obj.student.department.acronym) + ' - ' + str(obj.under_staff.department.acronym)

    def reg_no(self, obj):
        return obj.student.roll_no


class DepartmentSectionAdmin(admin.ModelAdmin):
    list_display = ('id', 'departments', 'batch', 'section_name')
    list_filter = ('department', 'batch', 'section_name', 'batch__regulation', 'batch__programme')
    search_fields = ('department__acronym', 'department__name', 'id')

    ordering = ('batch__start_year', 'batch__end_year', 'section_name', 'id')

    def departments(self, obj):
        return obj.department.acronym


class SectionStudentAdmin(admin.ModelAdmin):
    list_display = ('id', 'reg_no', 'student', 'section')
    list_filter = ('section__department', 'student__batch', 'section__section_name', 'student__batch__regulation',
                   'student__batch__programme')
    search_fields = (
        'student__roll_no', 'student__first_name', 'student__middle_name', 'student__last_name', 'student__user__email',
        'id')

    ordering = ('student__roll_no', 'section__section_name', 'id')

    def departments(self, obj):
        return str(obj.student.department.acronym) + ' - ' + str(obj.department_sections.department.acronym)

    def reg_no(self, obj):
        return obj.student.roll_no


admin.site.register(courses, CourseAdmin)
admin.site.register(staff_course, StaffCourseAdmin)
admin.site.register(time_table, TimetableAdmin)
admin.site.register(student_enrolled_courses, StudentEnrolledCourseAdmin)
admin.site.register(department_sections, DepartmentSectionAdmin)
admin.site.register(section_students, SectionStudentAdmin)

admin.site.register(day)
admin.site.register(college_schedule)


class AttendanceAdmin(admin.ModelAdmin):
    search_fields = ('course', 'date', 'hour', 'staff')
    list_display = ('course', 'date', 'hour', 'staff', 'granted_edit_access')


admin.site.register(attendance, AttendanceAdmin)

"""curriculum"""
# admin.site.register(time_table, SimpleHistoryAdmin)
admin.site.register(elective_course_staff, SimpleHistoryAdmin)
admin.site.register(one_credit_course_staff, SimpleHistoryAdmin)
admin.site.register(open_course_staff, SimpleHistoryAdmin)
admin.site.register(semester, SimpleHistoryAdmin)
# admin.site.register(staff_course, SimpleHistoryAdmin)
# admin.site.register(student_enrolled_courses, SimpleHistoryAdmin)

"""accounts"""
admin.site.register(issue_onduty, SimpleHistoryAdmin)
admin.site.register(onduty_request, SimpleHistoryAdmin)
admin.site.register(hourly_onduty_request, SimpleHistoryAdmin)
admin.site.register(regulation, SimpleHistoryAdmin)
# admin.site.register(staff_papers, SimpleHistoryAdmin)
# admin.site.register(staff_course_attended, SimpleHistoryAdmin)
admin.site.register(student_achievements, SimpleHistoryAdmin)

"""messaging"""
admin.site.register(message, SimpleHistoryAdmin)
admin.site.register(draft_box, SimpleHistoryAdmin)
admin.site.register(notifications, SimpleHistoryAdmin)

"""quiz"""
admin.site.register(quiz_timings, SimpleHistoryAdmin)
admin.site.register(quiz_questions, SimpleHistoryAdmin)
admin.site.register(attend_quiz_timings, SimpleHistoryAdmin)
admin.site.register(attend_quiz_answers, SimpleHistoryAdmin)

admin.site.register(hod)
admin.site.register(programme_coordinator)
admin.site.register(forceTimetableSlot)
