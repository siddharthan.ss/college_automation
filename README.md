# README #

### What is this repository for? ###

* This repo contains source codes for the college automation project of GCT,coimbatore
* version - 2016.0

### How do I get set up? ###

* Download and install **pycharm profession edition** IDE. It is a paid version, but free license is available for educational purposes.
* **NOTE:** The communtiy edition does not have Django support
* If you are not okay with Pycharm , you can opt for Eclipse IDE with pydev plugin 

* Install git : **sudo apt-get install git**
* Clone this repository into your desired location : **git clone https://gitlab.com/siddharthan.ss/college_automation.git**
* 
* Will be needing virtual environment for running djnago (google about its uses and significance before you proceed)
* Install virtualenv : **sudo apt-get install virtualenv**
* Create a new virtual env : **virtualenv your_environment_name -p python3**
* Replace 'your_environment_name' with your env name such as 'ca_env'
* Enbale the virutal env : **source ca_env/bin/activate**
* 
* All the dependency packages will be installed via pip from requirements.txt
* Before that certain packages requires dependency at OS level
* sudo apt-get install build-essential libssl-dev libffi-dev python3-dev
* For lxml - sudo apt-get install python-dev libxml2-dev libxslt1-dev zlib1g-dev
* For pillow - sudo apt-get install libjpeg-dev
* 
* Install pip packages : **pip install -r requirements.txt**
* Comment out production db settings in settings.py
* Use sqlite database for development setup
* Remove all existing migration files : **sh rm_migrations.sh**
* Makemigration files : **python3 manage.py makemigrations**
* Migrate db : **python3 manage.py migrate**
* Run setup files to load inital testing data in db : **python3 manage.py shell** -> **execfile('setup.py')**
* Run the development server
* have a look into the available test users in setup/test_users_setup.py




### Who do I talk to? ###

* siddharthan.ss@gmail.com - repo owner
* harshaktg@gmail.com 
* muthamil.ons@gmail.com 