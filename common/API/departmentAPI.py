from accounts.models import department, sub_department


def getAllDepartments():
    return department.objects.all()


def getCoreDepartments():
    return department.objects.filter(is_core=True)


def getDepartmentOfPk(departmentPk):
    return department.objects.get(pk=departmentPk)


def isSubDepartment(department_instance):
    sub_department.objects.filter(child_departments=department_instance).exists()


def getParentDepartment(department_instance):
    sub_department_instance = sub_department.objects.get(child_departments=department_instance)
    return sub_department_instance.main_department
