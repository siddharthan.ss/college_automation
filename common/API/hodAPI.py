from curriculum.models import hod


def getHodOfDepartment(departmentInstance):
    if hod.objects.filter(department=departmentInstance):
        hodInstance = hod.objects.get(department=departmentInstance)
        return hodInstance

    return None


def updateHodOfDepartment(departmentInstance, staffInstance):
    hodInstance = getHodOfDepartment(departmentInstance)

    if hodInstance:
        hodInstance.staff = staffInstance
        hodInstance.save()
    else:
        hodInstance = hod(department=departmentInstance, staff=staffInstance)
        hodInstance.save()
