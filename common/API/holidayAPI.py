from .departmentAPI import (
    isSubDepartment,
    getParentDepartment
)
from curriculum.models import holidays#,notHolidays

def getAllHolidaysForDepartment():
    return holidays.objects.all()

def getHolidaysForDepartmentInDateRange(start_date, end_date):
    return  holidays.objects.filter(
        date__gte=start_date,
        date__lte=end_date
    )
