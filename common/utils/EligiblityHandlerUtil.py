from accounts.models import department

def get_eligible_department_instances(CustomUserInstance):
    if CustomUserInstance.is_staff():
        if CustomUserInstance.is_principal():
            return department.objects.all()
        else:
            return CustomUserInstance.get_staff_instance().department

def get_eligible_semester_instances(CustomUserInstances):
    pass

def get_eligible_staff_course_allotment_instances(CustomUserInstance):
    pass

def is_eligible_department_instance(CustomUserInstance):
    pass