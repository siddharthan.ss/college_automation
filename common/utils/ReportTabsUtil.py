from curriculum.models import hod, programme_coordinator, semester_migration, staff_course, semester, \
    department_sections
from accounts.models import staff, department, site_settings


def departmentHasMultipleSections(departmentInstance, batchInstance):
    if department_sections.objects.filter(department=departmentInstance, batch=batchInstance).count() > 1:
        return True
    return False


def getDepartmentsOfHod(staffInstance):
    departmentListForHod = []
    for eachEntry in hod.objects.filter(staff=staffInstance):
        if eachEntry.department not in departmentListForHod:
            departmentListForHod.append(eachEntry.department)
    return departmentListForHod


def getDepartmentsOfPc(staffInstance):
    departmentListForPc = []
    for eachEntry in programme_coordinator.objects.filter(programme_coordinator_staffs=staffInstance):
        if eachEntry.department_programme.department not in departmentListForPc:
            departmentListForPc.append(eachEntry.department_programme.department)
    return departmentListForPc


def getDepartmentProgrammesOfPc(staffInstance):
    departmentProgrammeListForPc = []
    for eachEntry in programme_coordinator.objects.filter(programme_coordinator_staffs=staffInstance):
        if eachEntry.department_programme not in departmentProgrammeListForPc:
            departmentProgrammeListForPc.append(eachEntry.department_programme)
    return departmentProgrammeListForPc


def getDepartmentsForFacultyAdvisor(staffInstance):
    departmentListForFacultyAdvisor = []
    for eachEntry in semester_migration.objects.filter(semester__faculty_advisor=staffInstance):
        if eachEntry.semester.department not in departmentListForFacultyAdvisor:
            departmentListForFacultyAdvisor.append(eachEntry.semester.department)
    return departmentListForFacultyAdvisor


def getDepartmentsForFaculty(staffInstance):
    departmentListForFaculty = []
    activeSemesters = semester_migration.objects.all().values_list('semester')
    for staffCourseEntry in staff_course.objects.filter(staffs=staffInstance, semester__in=activeSemesters):
        if staffCourseEntry.department not in departmentListForFaculty:
            departmentListForFaculty.append(staffCourseEntry.department)
    return departmentListForFaculty


def getSemestersAsFacultyAdvisor(staffInstance):
    semesterList = semester.objects.filter(faculty_advisor=staffInstance)
    return semesterList


def isPcAccessibleSemester(staffInstance, semesterInstance):
    for entry in programme_coordinator.objects.filter(programme_coordinator_staffs=staffInstance,
                                                      department_programme__department=semesterInstance.department):
        for migration in semester_migration.objects.filter(department=semesterInstance.department,
                                                           semester__batch__programme=entry.department_programme.programme.acronym).order_by(
            '-semester__batch__programme', '-semester__batch__start_year'):
            if migration.semester == semesterInstance:
                return True

    return False


def isFacultyAdvisorAccessibleSemester(staffInstance, semesterInstance):
    if semester.objects.filter(pk=semesterInstance.pk, faculty_advisor=staffInstance):
        return True
    return False


def getCoursesHandlingByStaff(staffInstance):
    activeSemesters = semester_migration.objects.all().values_list('semester')

    courseList = []
    for entry in staff_course.objects.filter(staffs=staffInstance).filter(semester__in=activeSemesters):
        temp = {}
        temp['staffCourseInstance'] = entry

        temp['displayText'] = str(entry.semester.department.acronym) + '-'

        department_instance = entry.semester.department
        batch_instance = entry.semester.batch
        if departmentHasMultipleSections(department_instance, batch_instance):
            temp['displayText'] += str(entry.semester.department_section.section_name) + '-'
            temp['section'] = str(entry.semester.department_section.section_name)

        if entry.course.code != '':
            temp['displayText'] += str(entry.course.code)
        else:
            temp['displayText'] += str(entry.course.course_id)

        courseList.append(temp)

    return courseList


def getStaffCoursesForSemester(semesterInstance):
    courseList = []
    for entry in staff_course.objects.filter(semester=semesterInstance):
        temp = {}
        temp['staffCourseInstance'] = entry
        temp['displayText'] = str(entry.semester.department.acronym) + '-'
        departmentInstance = entry.semester.department
        batchInstance = entry.semester.batch
        if departmentHasMultipleSections(departmentInstance, batchInstance):
            temp['displayText'] += str(entry.semester.department_section.section_name) + '-'
            temp['section'] = str(entry.semester.department_section.section_name)

        if entry.course.code != '':
            temp['displayText'] += str(entry.course.code)
        else:
            temp['displayText'] += str(entry.course.course_id)

        courseList.append(temp)

    return courseList


def getFilteredCoursesForStaff(staffInstance, semesterInstance):
    courseList = []
    for entry in staff_course.objects.filter(staffs=staffInstance, semester=semesterInstance):
        temp = {}
        temp['staffCourseInstance'] = entry

        temp['displayText'] = str(entry.semester.department.acronym) + '-'

        department_instance = entry.semester.department
        batch_instance = entry.semester.batch
        if departmentHasMultipleSections(department_instance, batch_instance):
            temp['displayText'] += str(entry.semester.department_section.section_name) + '-'
            temp['section'] = str(entry.semester.department_section.section_name)

        if entry.course.code != '':
            temp['displayText'] += str(entry.course.code)
        else:
            temp['displayText'] += str(entry.course.course_id)

        courseList.append(temp)

    return courseList


def staffBelongsToNonCore(staffInstance):
    if staffInstance.department.is_core:
        return False
    else:
        return True


def getFirstManagingDepartment():
    departmentInstance = None
    query = site_settings.objects.filter(key="FIRST_YEAR_MANAGING_DEPARTMENT")
    if query.exists():
        departmentInstance = department.objects.get(pk=query.get().value)
    return departmentInstance


def staffEligibleForFirstYearManagingDepartment(staffInstance):
    departmentInstance = getFirstManagingDepartment()
    if departmentInstance is not None:
        if staffInstance.department == departmentInstance:
            return True
        else:
            return False

    return False


def getActiveDepartmentsForStaff(staffInstance):
    departmentList = []

    if staffInstance.user.is_principal() or staffInstance.user.is_coe_staff():
        return department.objects.filter(is_core=True)

    if staffBelongsToNonCore(staffInstance) and staffEligibleForFirstYearManagingDepartment(
            staffInstance) and staffInstance.user.is_hod():
        return department.objects.filter(is_core=True)

    if staffInstance.user.is_hod():
        for eachDepartment in getDepartmentsOfHod(staffInstance):
            departmentList.append(eachDepartment)

    if staffInstance.user.is_programme_coordinator():
        for eachDepartment in getDepartmentsOfPc(staffInstance):
            departmentList.append(eachDepartment)

    if staffInstance.user.is_faculty_advisor():
        for eachDepartment in getDepartmentsForFacultyAdvisor(staffInstance):
            departmentList.append(eachDepartment)

    for eachDepartment in getDepartmentsForFaculty(staffInstance):
        departmentList.append(eachDepartment)

    duplicatedDepartmentList = departmentList.copy()
    departmentList.clear()

    for eachDepartment in duplicatedDepartmentList:
        if eachDepartment not in departmentList:
            departmentList.append(eachDepartment)

    return departmentList


def getActiveSemesterTabsForStaff(staffInstance, departmentInstance):
    activeSemesters = []

    if staffInstance.user.is_principal() or staffInstance.user.is_coe_staff():
        activeSemesters = semester_migration.objects.filter(department=departmentInstance).order_by(
            '-semester__batch__programme', '-semester__batch__start_year')

    elif staffBelongsToNonCore(staffInstance) and staffEligibleForFirstYearManagingDepartment(
            staffInstance) and staffInstance.user.is_hod():
        for migration in semester_migration.objects.filter(semester__batch__programme='UG',
                                                           department=departmentInstance,
                                                           semester__semester_number__lte=2).order_by(
            '-semester__batch__programme', '-semester__batch__start_year'):
            activeSemesters.append(migration)

        courseSemesters = []
        courseList = getCoursesHandlingByStaff(staffInstance)
        for eachCourse in courseList:
            courseSemesters.append(eachCourse['staffCourseInstance'].semester)

        for migration in semester_migration.objects.filter(department=departmentInstance,
                                                           semester__in=courseSemesters).order_by(
            '-semester__batch__programme', '-semester__batch__start_year'):
            if migration not in activeSemesters:
                activeSemesters.append(migration)


    elif staffInstance.user.is_hod() and hod.objects.filter(staff=staffInstance, department=departmentInstance):
        activeSemesters = semester_migration.objects.filter(department=departmentInstance).order_by(
            '-semester__batch__programme', '-semester__batch__start_year')

    else:
        if staffInstance.user.is_programme_coordinator():
            for entry in programme_coordinator.objects.filter(programme_coordinator_staffs=staffInstance,department_programme__department=departmentInstance):
                for migration in semester_migration.objects.filter(department=departmentInstance,semester__batch__programme=entry.department_programme.programme.acronym).order_by('-semester__batch__programme', '-semester__batch__start_year'):
                    activeSemesters.append(migration)

        if staffInstance.user.is_faculty_advisor():
            semesterList = getSemestersAsFacultyAdvisor(staffInstance)
            for migration in semester_migration.objects.filter(department=departmentInstance,
                                                               semester__in=semesterList).order_by(
                '-semester__batch__programme', '-semester__batch__start_year'):
                if migration not in activeSemesters:
                    activeSemesters.append(migration)

        courseList = getCoursesHandlingByStaff(staffInstance)

        courseSemesters = []
        for eachCourse in courseList:
            courseSemesters.append(eachCourse['staffCourseInstance'].semester)

        for migration in semester_migration.objects.filter(department=departmentInstance,
                                                           semester__in=courseSemesters).order_by(
            '-semester__batch__programme', '-semester__batch__start_year'):
            if migration not in activeSemesters:
                activeSemesters.append(migration)

    activeSemestersList = []
    for entry in activeSemesters:
        temp = {}
        temp['semesterInstance'] = entry.semester
        temp['displayText'] = 'SEM ' + str(entry.semester_number) + ' ( ' + str(
            entry.semester.batch.programme) + ' )'
        if departmentHasMultipleSections(entry.department, entry.semester.batch):
            temp['displayText'] += '-' + str(entry.semester.department_section.section_name)
        activeSemestersList.append(temp)

    return activeSemestersList


def getActiveCoursesTabsForStaff(staffInstance, semesterInstance):
    departmentInstance = semesterInstance.department
    semesterInstance = semester.objects.get(pk=semesterInstance.pk)

    if staffInstance.user.is_principal() or staffInstance.user.is_coe_staff():
        courseList = getStaffCoursesForSemester(semesterInstance)

    elif staffBelongsToNonCore(staffInstance) and staffEligibleForFirstYearManagingDepartment(staffInstance):
        if semesterInstance.semester_number <= 2:
            courseList = getStaffCoursesForSemester(semesterInstance)
        else:
            courseList = getFilteredCoursesForStaff(staffInstance, semesterInstance)

    elif staffInstance.user.is_hod() and hod.objects.filter(staff=staffInstance, department=departmentInstance):
        courseList = getStaffCoursesForSemester(semesterInstance)

    elif staffInstance.user.is_programme_coordinator() and isPcAccessibleSemester(staffInstance, semesterInstance):
        courseList = getStaffCoursesForSemester(semesterInstance)

    elif staffInstance.user.is_faculty_advisor() and isFacultyAdvisorAccessibleSemester(staffInstance,
                                                                                        semesterInstance):
        courseList = getStaffCoursesForSemester(semesterInstance)

    else:
        courseList = getFilteredCoursesForStaff(staffInstance, semesterInstance)

    return courseList


def courseViewPermissionByStaff(staffCourseInstance, staffInstance):
    if staffInstance in staffCourseInstance.staffs.all():
        return True
    if staffInstance.user.is_hod() and staffInstance.department == staffCourseInstance.course.department:
        return True
    if staffInstance.user.is_principal():
        return True
    if staffInstance.user.is_coe_staff():
        return True
    if staffInstance.user.is_faculty_advisor():
        semesterInstance = staffCourseInstance.semester
        semesterList = getSemestersAsFacultyAdvisor(staffInstance)
        for each in semester_migration.objects.filter(department=staffCourseInstance.department,
                                                      semester__in=semesterList):
            if semesterInstance == each.semester:
                return True

    return False


def getActiveSemesterTabsForAllotment(staffInstance):
    activeSemesters = []
    if staffBelongsToNonCore(staffInstance):
        firstYearStaff = True
        if staffEligibleForFirstYearManagingDepartment(staffInstance):
            activeSemesters = semester_migration.objects.filter(semester__batch__programme='UG').filter(
                semester__semester_number__lte=2)
    else:
        firstYearStaff = False
        departmentInstance = getDepartmentsOfPc(staffInstance)
        activeSemesters = semester_migration.objects.filter(department__in=departmentInstance).order_by(
            '-semester__batch__programme', '-semester__batch__start_year')

    activeSemestersList = []
    for entry in activeSemesters:
        temp = {}
        temp['semesterInstance'] = entry.semester
        if firstYearStaff:
            temp['displayText'] = entry.semester.getDisplayTextForFirstYearAllotmentTab()
        else:
            temp['displayText'] = entry.semester.getDisplayTextForAllotmentTab()
        activeSemestersList.append(temp)

    return activeSemestersList
