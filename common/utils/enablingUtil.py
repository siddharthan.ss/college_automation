from datetime import datetime


def canEnableTimeTableSlots(semesterInstance):
    dateInstance = datetime.strptime("18-12-2017","%d-%m-%Y").date()

    if semesterInstance.start_term_1 >= dateInstance:
        return True

    return False