from datetime import datetime

from common.utils.enablingUtil import canEnableTimeTableSlots
from common.utils.slotNumberUtil import getDayInstanceForSemesterInstance
from curriculum.models import time_table, day
from curriculum.views import is_holiday, get_staff_course_allotment_instances_for_staff

def getDayInstanceForTodaysTimetable(semsterInstance):
    return getDayInstanceForGivenDate(semsterInstance, datetime.now().date())

def getDayInstanceForGivenDate(semesterInstance, dateInstance):
    if canEnableTimeTableSlots(semesterInstance):
        dayInstance = getDayInstanceForSemesterInstance(semesterInstance, dateInstance)
    else:
        dayInstance = day.objects.get(day_name=dateInstance.strftime("%A"))
    return dayInstance

def getActiveSemesters(staffCourseList):
    activeSemesters = []
    for staffCourse in staffCourseList:
        if staffCourse.semester not in activeSemesters:
            activeSemesters.append(staffCourse.semester)

    return activeSemesters

def getCourseList(staffCourseList):
    coursesList = []
    for staffCourse in staffCourseList:
        if staffCourse.course not in coursesList:
            coursesList.append(staffCourse.course)

    return coursesList

def getTodayTimetable(staffInstance):
    currentDate = datetime.now().date()

    if is_holiday(currentDate):
        return None

    staffCourseList = get_staff_course_allotment_instances_for_staff(staffInstance)

    activeSemesters = getActiveSemesters(staffCourseList)
    coursesList = getCourseList(staffCourseList)

    timetableList = []
    for semester in activeSemesters:
        dayInstance = getDayInstanceForTodaysTimetable(semester)
        for entry in time_table.objects.filter(is_active=True, semester=semester, course__in=coursesList,
                                               day=dayInstance):
            timetableList.append(entry)

    return timetableList

