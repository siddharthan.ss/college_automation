from datetime import timedelta, datetime
from pprint import pprint

from curriculum.models import timeTableSlot, day, time_table, staff_course, semester_migration, forceTimetableSlot
from curriculum.views.common_includes import is_holiday, get_staff_course_allotment_instances_for_staff
from django.utils import timezone
from curriculum.models import holidays#,notHolidays
from common.API import holidayAPI



def getTodayDayInstanceForSemesterInstance(semesterInstance):
    return getDayInstanceForSemesterInstance(semesterInstance, timezone.now().date())


def getDayInstanceForSemesterInstance(semesterInstance, dateIntance):
    slotNumber = getSlotNumberForSemesterInstance(semesterInstance, dateIntance)
    if slotNumber:
        return day.objects.get(slot_number=slotNumber)
    return None

def is_weekend(dateInstance):
    return dateInstance.weekday() == 5 or dateInstance.weekday() == 6


def getSlotNumberForSemesterInstance(semesterInstance, dateInstance):
    if is_holiday(dateInstance):
        return None
    if is_weekend(dateInstance):# and not notHolidays.objects.filter(department=semesterInstance.department,date=dateInstance).exists():
        return None
    startingSlotNumber = 1
    forceTimetableSlotQuery = forceTimetableSlot.objects.filter(
        date__lte=dateInstance,
    )
    if forceTimetableSlotQuery.exists():
        forceTimetableSlotInstance = forceTimetableSlotQuery.first()
        startingSlotNumber = forceTimetableSlotInstance.slotNumber
    slotNumber = getSlotNumber(semesterInstance.start_term_1, dateInstance, semesterInstance.department, startingSlotNumber)
    timeTableSlotInstance = timeTableSlot(
        date=dateInstance,
        slotday=day.objects.get(slot_number=slotNumber)
    )
    timeTableSlotInstance.save()
    return slotNumber


def getSlotNumber(startDateInstance, endDateInstance, departmentInstance, startingSlotNumber):
    if type(startDateInstance) is datetime:
        startDateInstance = startDateInstance.date()
    if type(endDateInstance) is datetime:
        endDateInstance = endDateInstance.date()
    if is_weekend(startDateInstance) or is_weekend(endDateInstance):
        return 0
    daysSinceLastMondayOfStartDate = startDateInstance.weekday()
    daysSinceLastMondayOfEndDate = endDateInstance.weekday()

    lastMondaySinceStartDate = startDateInstance - timedelta(days=daysSinceLastMondayOfStartDate)
    lastMondaySinceEndDate = endDateInstance - timedelta(days=daysSinceLastMondayOfEndDate)

    noOfWeeks = (lastMondaySinceEndDate - lastMondaySinceStartDate) / 7.0
    noOfWeekendDays = noOfWeeks.days * 2

    # daysBeforeNextSunday = 6 - startDateInstance.weekday()
    # daysSincePrevSaturday = endDateInstance.weekday() + 2
    #
    # nextSunday = startDateInstance + timedelta(days = (daysBeforeNextSunday))
    # prevFriday = endDateInstance - timedelta(days = (daysSincePrevSaturday))
    #
    # noOfDaysexclusive = (prevFriday - nextSunday)
    # noOfDaysInclusive = noOfDaysexclusive.days + 1
    # noOfWeekends = (noOfDaysInclusive + 1) / 7.0
    #
    # noOfWeekendDays = noOfWeekends * 2

    # startWeekNumber = startDateInstance.isocalendar()[1]
    # endWeekNumber = endDateInstance.isocalendar()[1]
    # noOfWeeks = endWeekNumber - startWeekNumber
    #
    # noOfWeekendDays = noOfWeeks * 2

    holidaysQueryset = holidayAPI.getHolidaysForDepartmentInDateRange(startDateInstance, endDateInstance)

    holidaysQueryset = [holiday for holiday in holidaysQueryset if holiday.date.weekday() not in (5, 6)]
    
    #notHolidaysQueryset=holidayAPI.getNotHolidaysForDeptInDateRange(departmentInstance,startDateInstance, endDateInstance)

    noOfholidays = len(holidaysQueryset)

    noOfDays = endDateInstance - startDateInstance - timedelta(days=noOfWeekendDays) - timedelta(days=noOfholidays) + timedelta(days=1)# + timedelta(days=len(notHolidaysQueryset))

    noOfDays = noOfDays.days
    # adding startingSlotNumber
    noOfDays += (startingSlotNumber - 1)

    print(noOfDays % 5)
    slotNumber = noOfDays % 5
    if slotNumber == 0:
        return 5
    else:
        return slotNumber


    # slotNumber = noOfDaysInclusive - noOfWeekendDays + daysBeforeNextSunday + daysSincePrevSaturday
    #
    # return  slotNumber