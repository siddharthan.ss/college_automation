from curriculum.models import section_students
from accounts.models import active_batches
from curriculum.models import semester_migration, courses, staff_course, student_enrolled_courses
from enrollment.models import course_enrollment


def getSemesterPlanofStudent(studentInstance):
    departmentInstance = studentInstance.department
    sectionInstance = section_students.objects.get(student=studentInstance).section

    batchInstance = sectionInstance.batch
    activeBatchInstance = active_batches.objects.get(department=departmentInstance, batch=batchInstance)

    try:
        semesterMigrationInstance = semester_migration.objects.get(semester__batch=batchInstance,
                                                                   department=departmentInstance,
                                                                   semester__department_section=sectionInstance,
                                                                   semester_number=activeBatchInstance.current_semester_number_of_this_batch)
        return semesterMigrationInstance.semester
    except semester_migration.DoesNotExist:
        return None


def getStaffCoursesForStudent(studentInstance):
    regulationInstance = studentInstance.batch.regulation
    semesterInstance = getSemesterPlanofStudent(studentInstance)

    courseList = []

    for entry in staff_course.objects.filter(semester=semesterInstance):
        if regulationInstance.start_year >= 2016:
            if course_enrollment.objects.filter(student=studentInstance, course=entry.course,
                                                semester=semesterInstance,
                                                approved=True):
                courseList.append(entry)
        else:
            if entry.course.is_elective or entry.course.is_one_credit:
                if student_enrolled_courses.objects.filter(course=entry.course, student=studentInstance, approved=True,
                                                           studied_semester=semesterInstance):
                    courseList.append(entry)
            elif entry.course.is_open:
                pass
            else:
                courseList.append(entry)

    return courseList


def courseEligibleForStudent(studentInstance, staffCourseInstance):
    semesterInstance = staffCourseInstance.semester
    courseInstance = staffCourseInstance.course

    studentSemesterInstance = getSemesterPlanofStudent(studentInstance)

    if semesterInstance == studentSemesterInstance:
        if staffCourseInstance.course.regulation.start_year >= 2016:
            if course_enrollment.objects.filter(student=studentInstance, semester=semesterInstance,
                                                course=courseInstance,
                                                approved=True):
                return True
            else:
                return False
        else:
            if courseInstance.is_elective or courseInstance.is_one_credit:
                if student_enrolled_courses.objects.filter(student=studentInstance, studied_semester=semesterInstance,
                                                           course=courseInstance, approved=True):
                    return True
                else:
                    return False
            if courseInstance.is_open:
                return False
            return True
    else:
        return False


def getCourseListStudyingByStudent(studentInstance):
    courseList = []
    departmentInstance = studentInstance.department
    regulationInstance = studentInstance.batch.regulation
    programmeInstance = studentInstance.qualification
    semesterInstance = getSemesterPlanofStudent(studentInstance)
    if semesterInstance:
        semesterNumber = semesterInstance.semester_number

        courseListAll = courses.objects.filter(regulation=regulationInstance, department=departmentInstance,
                                               programme=programmeInstance, semester=semesterNumber, subject_type='T')

        for eachCourse in courseListAll.filter(is_elective=False, is_open=False, is_one_credit=False):
            if regulationInstance.start_year >= 2016:
                if course_enrollment.objects.filter(student=studentInstance, course=eachCourse,
                                                    semester=semesterInstance,
                                                    approved=True):
                    if staff_course.objects.filter(course=eachCourse, semester=semesterInstance):
                        courseList.append(eachCourse)
            else:
                if staff_course.objects.filter(course=eachCourse, semester=semesterInstance):
                    courseList.append(eachCourse)

        for eachCourse in courseListAll.filter(is_elective=True, is_one_credit=False, is_open=False):
            if regulationInstance.start_year >= 2016:
                if course_enrollment.objects.filter(student=studentInstance, course=eachCourse,
                                                    semester=semesterInstance,
                                                    approved=True):
                    if staff_course.objects.filter(course=eachCourse, semester=semesterInstance):
                        courseList.append(eachCourse)
            else:
                if student_enrolled_courses.objects.filter(course=eachCourse, student=studentInstance, approved=True,
                                                           studied_semester=semesterInstance):
                    if staff_course.objects.filter(course=eachCourse, semester=semesterInstance):
                        courseList.append(eachCourse)

        for eachCourse in courseListAll.filter(is_elective=False, is_one_credit=True, is_open=False):
            if regulationInstance.start_year >= 2016:
                if course_enrollment.objects.filter(student=studentInstance, course=eachCourse,
                                                    semester=semesterInstance,
                                                    approved=True):
                    if staff_course.objects.filter(course=eachCourse, semester=semesterInstance):
                        courseList.append(eachCourse)
            else:
                if student_enrolled_courses.objects.filter(course=eachCourse, student=studentInstance, approved=True,
                                                           studied_semester=semesterInstance):
                    if staff_course.objects.filter(course=eachCourse, semester=semesterInstance):
                        courseList.append(eachCourse)

    return courseList
