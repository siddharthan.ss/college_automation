from accounts.models import staff
from curriculum.models import courses, staff_course


def getCoursesForSemester(semesterInstance):
    courseList = []
    departmentInstance = semesterInstance.department
    regulationInstance = semesterInstance.batch.regulation
    programmeInstance = semesterInstance.batch.programme
    semesterNumber = semesterInstance.semester_number

    courseListAll = courses.objects.filter(department=departmentInstance, regulation=regulationInstance,
                                           programme=programmeInstance, semester=semesterNumber)

    for eachCourse in courseListAll.filter(is_elective=False, is_one_credit=False, is_open=False):
        courseList.append(eachCourse)
    for eachCourse in courseListAll.filter(is_elective=True, is_one_credit=False, is_open=False):
        courseList.append(eachCourse)
    for eachCourse in courseListAll.filter(is_elective=False, is_one_credit=True, is_open=False):
        courseList.append(eachCourse)

    return courseList


def getStaffsFromDepartments(departmentPkList):
    return staff.objects.filter(department_id__in=departmentPkList, user__is_approved=True)


def getOrCreateStaffCourses(courseList, semesterInstance):
    staffCourseList = []
    for eachCourse in courseList:
        if staff_course.objects.filter(semester=semesterInstance, course=eachCourse):
            staffCourseInstance = staff_course.objects.get(semester=semesterInstance, course=eachCourse)
        else:
            staffCourseInstance = staff_course(semester=semesterInstance, course=eachCourse,
                                               batch=semesterInstance.batch, department=semesterInstance.department)
            staffCourseInstance.save()

        staffCourseList.append(staffCourseInstance)

    return staffCourseList


def getStaffCourses(semesterInstance):
    return staff_course.objects.filter(semester=semesterInstance)


def getstaffDepartmentPksOfStaffCourse(staffCourseList):
    departmentPkList = []
    for staffCourse in staffCourseList:
        for eachStaff in staffCourse.staffs.all():
            if eachStaff.department.pk not in departmentPkList:
                departmentPkList.append(eachStaff.department.pk)

    departmentPkList.append(staffCourseList[0].semester.department.pk)

    return departmentPkList
