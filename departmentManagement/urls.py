from django.conf.urls import url

from departmentManagement.views.allotHod import allotHod
from departmentManagement.views.allotStaff import allotStaff

urlpatterns = [

    url(r'^allot_hod/$', allotHod, name="allotHod"),
    url(r'^allot_staff/$', allotStaff, name='allotStaff'),

]
