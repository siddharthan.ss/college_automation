from django.apps import AppConfig


class DepartmentmanagementConfig(AppConfig):
    name = 'departmentManagement'
