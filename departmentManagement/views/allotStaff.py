from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from accounts.models import staff
from curriculum.models import semester, courses

from common.utils.ReportTabsUtil import getActiveSemesterTabsForAllotment
from common.API.departmentAPI import getAllDepartments
from departmentManagement.common.utils.allotStaffUtil import getStaffCourses, getstaffDepartmentPksOfStaffCourse, \
    getStaffsFromDepartments, getCoursesForSemester, getOrCreateStaffCourses


@login_required
@permission_required('curriculum.can_allot_staffs')
def allotStaff(request):
    selectedDepartmentPkList = []
    staffInstance = staff.objects.get(user=request.user)

    departmentList = getAllDepartments()
    semesterList = getActiveSemesterTabsForAllotment(staffInstance)

    if not semesterList:
        msg = {
            'page_title': 'No Semester Plan',
            'title': 'No Semester Plan',
            'description': 'Create Semester Plan first, in order to create timetable',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    semesterInstance = semesterList[0]['semesterInstance']

    if request.method == 'POST':
        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)

        if 'departmentSelectBox' in request.POST:
            semesterPk = int(request.POST.get('hiddenSemesterPkOnDepartmentSelection'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            selectedDepartmentPkList = request.POST.getlist('departmentSelectBox')
            selectedDepartmentPkList = list(map(int, selectedDepartmentPkList))

        if 'allotStaff' in request.POST:
            semesterPk = int(request.POST.get('hiddenSemesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)

            staffCourseList = getStaffCourses(semesterInstance)

            for staffCourse in staffCourseList:
                courseStaffList = request.POST.getlist('select-' + str(staffCourse.pk))
                courseStaffList = list(map(int, courseStaffList))
                for eachStaffPk in courseStaffList:
                    courseStaffInstance = staff.objects.get(pk=eachStaffPk)
                    if courseStaffInstance not in staffCourse.staffs.all():
                        staffCourse.staffs.add(courseStaffInstance)

                for eachStaff in staffCourse.staffs.all():
                    if eachStaff.pk not in courseStaffList:
                        staffCourse.staffs.remove(eachStaff)

                shortCode = str(request.POST.get('shortCode-' + str(staffCourse.pk)))
                courseInstance = courses.objects.get(pk=staffCourse.course.pk)
                courseInstance.code = shortCode.upper()
                courseInstance.save()

    courseList = getCoursesForSemester(semesterInstance)

    if not courseList:
        return render(request, 'allotStaff/allotStaff.html',
                      {
                          'noCourse': True,
                          'semesterList': semesterList,
                          'semesterInstance': semesterInstance,
                          'departmentList': departmentList,
                      })

    staffCourseList = getOrCreateStaffCourses(courseList, semesterInstance)

    departmentPksFromStaffCourse = getstaffDepartmentPksOfStaffCourse(staffCourseList)

    selectedDepartmentPkList = list(set(selectedDepartmentPkList + departmentPksFromStaffCourse))

    staffList = getStaffsFromDepartments(selectedDepartmentPkList)

    return render(request, 'allotStaff/allotStaff.html',
                  {
                      'semesterList': semesterList,
                      'semesterInstance': semesterInstance,
                      'departmentList': departmentList,
                      'selectedDepartmentPkList': selectedDepartmentPkList,
                      'staffCourseList': staffCourseList,
                      'staffList': staffList
                  })
