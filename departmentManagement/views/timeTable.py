from curriculum.models import courses, staff_course


def getCoursesForTimetable(semesterInstance):
    departmentInstance = semesterInstance.department
    batchInstance = semesterInstance.batch
    regulationInstance = semesterInstance.batch.regulation
    programme = semesterInstance.batch.programme
    currentSemesterNumber = semesterInstance.semester_number

    courseList = []
    availableCourseList = courses.objects.filter(department=departmentInstance, regulation=regulationInstance,
                                                 programme=programme, semester=currentSemesterNumber)

    for eachCourse in availableCourseList.filter(common_course=False, is_open=False, isOpenElectiveCourse=False):
        courseList.append(getPrintableCourse(eachCourse, semesterInstance))

    if semesterInstance.open_courses and semesterInstance.open_courses > 0:
        i = 1
        while i <= semesterInstance.open_courses:
            courseInstance = courses.objects.get(course_id="OE" + str(i))
            temp = {}
            temp['displayTexr'] = str(courseInstance)
            temp['pk'] = courseInstance.pk
            temp['courseId'] = courseInstance.course_id
            temp['courseName'] = courseInstance.course_name
            temp['courseCode'] = courseInstance.course_id
            courseList.append(temp)
            i = i + 1

    return courseList


def getPrintableCourse(courseInstance, semesterInstance):
    temp = {}
    temp['displayText'] = str(courseInstance)
    temp['pk'] = courseInstance.pk
    temp['courseId'] = courseInstance.course_id
    temp['courseName'] = courseInstance.course_name
    temp['courseCode'] = courseInstance.code
    if courseInstance.is_elective:
        temp['courseType'] = '(Elective)'
    if courseInstance.is_one_credit:
        temp['courseType'] = '(One Credit)'
    staffList = []
    staffCourseList = staff_course.objects.filter(course=courseInstance, semester=semesterInstance)
    if staffCourseList:
        staffCourseInstance = staffCourseList[0]
        for eachStaff in staffCourseInstance.staffs.all():
            staffList.append(str(eachStaff))
        temp['staffList'] = staffList
    return temp
