from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from accounts.models import department, staff
from curriculum.models import hod

from curriculum.views.common_includes import get_all_eligible_staffs

from common.API.departmentAPI import getAllDepartments, getDepartmentOfPk
from common.API.hodAPI import getHodOfDepartment, updateHodOfDepartment


@login_required
@permission_required('accounts.can_allot_hod')
def allotHod(request):
    departmentList = getAllDepartments()

    if not departmentList:
        msg = {
            'page_title': 'No Departments',
            'title': 'No Departments Created',
            'description': 'Contact Admin to add departments',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    departmentInstance = departmentList[0]

    if request.method == 'POST':
        if 'departmentPk' in request.POST:
            departmentPk = request.POST.get('departmentPk')
            departmentInstance = getDepartmentOfPk(departmentPk)

        if 'hodStaffPk' in request.POST:
            departmentPk = request.POST.get('hiddenDepartmentPk')
            staffPk = request.POST.get('hodStaffPk')

            departmentInstance = getDepartmentOfPk(departmentPk)
            staffInstance = staff.objects.get(pk=staffPk)

            updateHodOfDepartment(departmentInstance, staffInstance)

    staffList = get_all_eligible_staffs(departmentInstance)

    currentHod = getHodOfDepartment(departmentInstance)

    print(currentHod)

    if currentHod:
        return render(request, 'allotHod/allotHod.html',
                      {
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                          'staffList': staffList,
                          'currentHod': currentHod
                      })

    return render(request, 'allotHod/allotHod.html',
                  {
                      'departmentList': departmentList,
                      'departmentInstance': departmentInstance,
                      'staffList': staffList
                  })
