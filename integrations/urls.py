from django.conf.urls import url
from integrations.views import login_integration

urlpatterns = [
    url(r'^login/$', login_integration, name="oauth_login"),
]