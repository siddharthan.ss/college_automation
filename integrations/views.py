from django.shortcuts import render

# Create your views here.


def login_integration(request):
    return render(request, 'integrations/login_integration.html');