from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from feedback.views import FeedbackPdfReport
from feedback.views.feedback import setFeedbackFormat, viewFeedbackFormat, doFeedback, feedbackReports

urlpatterns = [
    url(r'^set_feedback_format/$', setFeedbackFormat, name="setFeedbackFormat"),
    url(r'^view_feedback_format/$', viewFeedbackFormat, name="viewFeedbackFormat"),
    url(r'^give_feedback/$', doFeedback, name="doFeedback"),
    url(r'^feedback_reports/$', feedbackReports, name="feedbackReports"),
    url(r'^feedback_pdf_view/(?P<staffCoursePk>[0-9]+)', login_required(FeedbackPdfReport.as_view()),
        name='feedbackPdfReport'),
]
