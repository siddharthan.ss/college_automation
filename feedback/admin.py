from django.contrib import admin
from feedback.models import FeedbackFormat, FeedbackCategory, FeedbackQuestion, FeedbackGrades, FeedbackByStudents

# Register your models here.

admin.site.register(FeedbackFormat)
admin.site.register(FeedbackCategory)
admin.site.register(FeedbackQuestion)
admin.site.register(FeedbackGrades)
admin.site.register(FeedbackByStudents)
