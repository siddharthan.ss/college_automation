from django.db import models
from accounts.models import student
from curriculum.models import staff_course


class FeedbackFormat(models.Model):
    noOfQuestions = models.PositiveSmallIntegerField()
    noOfCategories = models.PositiveSmallIntegerField()
    noOfGrades = models.PositiveSmallIntegerField()
    formatName = models.CharField(max_length=300)
    createdDate = models.DateTimeField()
    isLive = models.BooleanField(default=False)
    isPast=models.BooleanField(default=False)


class FeedbackCategory(models.Model):
    feedbackFormat = models.ForeignKey(FeedbackFormat)
    categoryName = models.CharField(max_length=500)
    noOfQuestions = models.PositiveSmallIntegerField()

    def __str__(self):
        return str(self.categoryName)


class FeedbackQuestion(models.Model):
    feedbackCategory = models.ForeignKey(FeedbackCategory)
    questionNumber = models.PositiveSmallIntegerField()
    question = models.CharField(max_length=1000, null=True, blank=True)

    def __str__(self):
        return str(self.feedbackCategory) + '-' + str(self.questionNumber)


class FeedbackGrades(models.Model):
    feedbackFormat = models.ForeignKey(FeedbackFormat)
    gradeName = models.CharField(max_length=100)
    gradeShort = models.CharField(max_length=20)
    gradePriority = models.PositiveSmallIntegerField()

    def __str__(self):
        return str(self.gradeName)


class FeedbackByStudents(models.Model):
    feedbackFormat=models.ForeignKey(FeedbackFormat)
    feedbackQuestion = models.ForeignKey(FeedbackQuestion)
    feedbackGrades = models.ForeignKey(FeedbackGrades)
    student = models.ForeignKey(student)
    staffCourse = models.ForeignKey(staff_course)
    internal=models.IntegerField(default=3)

    def __str__(self):
        return str(self.staffCourse.course) + '-' + str(self.feedbackGrades)

    class Meta:
        unique_together = ('feedbackQuestion', 'student', 'staffCourse','internal')