from pprint import pprint
'''
if FeedbackFormat.objects.all().count() > 0:
    msg = {
        'page_title': 'Access Denied',
        'title': 'Only one Format creation is allowed',
        'description': 'You can create only one feedback format, you can create new format by deleting the old one. CAUTION: DELETING WILL IN TURN DELETE FEEDBACKS GIVEN BY THE STUDENTS',
    }
    return render(request, 'prompt_pages/error_page_base.html', {'message': msg})'''
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.shortcuts import render, redirect
from django.utils import timezone
from django.core.urlresolvers import reverse
from easy_pdf.views import PDFTemplateView

from curriculum.models import courses, semester
from feedback.models import FeedbackFormat, FeedbackCategory, FeedbackQuestion, FeedbackGrades, FeedbackByStudents
from accounts.models import staff, department, student
from curriculum.models import staff_course
from common.utils.ReportTabsUtil import getActiveDepartmentsForStaff, getActiveSemesterTabsForStaff, \
    getActiveCoursesTabsForStaff, courseViewPermissionByStaff
from common.utils.studentUtil import getSemesterPlanofStudent, getStaffCoursesForStudent
from common.API.departmentAPI import getDepartmentOfPk


@login_required
@permission_required('enrollment.can_enroll_students')
def setFeedbackFormat(request):
    if request.method == 'POST':
        if 'noOfQuestions' in request.POST:
            noOfQuestions = int(request.POST.get('noOfQuestions'))
            noOfCategories = int(request.POST.get('noOfCategories'))
            noOfGrades = int(request.POST.get('noOfGrades'))
            formatName = request.POST.get('formatName')

            feedbackFormatInstance = FeedbackFormat(noOfQuestions=noOfQuestions, noOfCategories=noOfCategories,
                                                    noOfGrades=noOfGrades, formatName=formatName,
                                                    createdDate=timezone.now())
            feedbackFormatInstance.save()

            counter = 1
            while counter <= noOfCategories:
                categoryName = str(request.POST.get('category-' + str(counter)))
                categoryQuestions = int(request.POST.get('categoryQuestions-' + str(counter)))

                feedbackCategoryInstance = FeedbackCategory(feedbackFormat=feedbackFormatInstance,
                                                            categoryName=categoryName.upper(),
                                                            noOfQuestions=categoryQuestions)
                feedbackCategoryInstance.save()

                question = 1
                while question <= categoryQuestions:
                    feedbackQuestionInstance = FeedbackQuestion(feedbackCategory=feedbackCategoryInstance,
                                                                questionNumber=question)
                    feedbackQuestionInstance.save()
                    question = question + 1

                counter = counter + 1

            counter = 1
            while counter <= noOfGrades:
                gradeName = str(request.POST.get('gradeName-' + str(counter)))
                gradeShort = str(request.POST.get('gradeShort-' + str(counter)))

                feedbackGradesInstance = FeedbackGrades(feedbackFormat=feedbackFormatInstance,
                                                        gradeName=gradeName.upper(),
                                                        gradeShort=gradeShort.upper(), gradePriority=counter)
                feedbackGradesInstance.save()

                counter = counter + 1

            request.session['needEdit'] = True
            return redirect(reverse(viewFeedbackFormat))

    return render(request, 'feedback/setFeedbackFormat.html', {

    })


@login_required
@permission_required('enrollment.can_enroll_students')
def viewFeedbackFormat(request):
    if 'needEdit' in request.session:
        del request.session['needEdit']
        formatList = FeedbackFormat.objects.all()

        return render(request, 'feedback/viewFeedbackFormat.html', {
            'formatList': formatList,
            'needEdit': True
        })

    if request.method == 'POST':
        if 'deleteFormat' in request.POST:
            feedbackFormatPk = int(request.POST.get('deleteFormat'))
            feedbackFormatInstance = FeedbackFormat.objects.get(pk=feedbackFormatPk)
            if feedbackFormatInstance.isPast == True:
                formatList = FeedbackFormat.objects.all()
                return render(request, 'feedback/viewFeedbackFormat.html', {
                    'formatList': formatList,
                    'deleteApastRecord': True
                })
            elif feedbackFormatInstance.isLive == True:
                formatList = FeedbackFormat.objects.all()
                return render(request, 'feedback/viewFeedbackFormat.html', {
                    'formatList': formatList,
                    'deleteAsActive': True
                })
            else:
                feedbackFormatInstance.delete()

        elif 'updateActivation' in request.POST:
            formatList = FeedbackFormat.objects.all()
            feedbackFormatPk = int(request.POST.get('updateActivation'))

            feedbackFormatInstance = FeedbackFormat.objects.get(pk=feedbackFormatPk)

            if feedbackFormatInstance.isLive == False:
                if formatList.filter(isLive=True).count() == 0:
                    feedbackCategoryList = FeedbackCategory.objects.filter(feedbackFormat=feedbackFormatInstance)
                    feedbackQuestionList = FeedbackQuestion.objects.filter(
                        feedbackCategory__in=feedbackCategoryList).order_by(
                        'feedbackCategory', 'questionNumber')

                    for question in feedbackQuestionList:
                        if not question.question:
                            return render(request, 'feedback/viewFeedbackFormat.html', {
                                'formatList': formatList,
                                'noQuestionsSet': True
                            })

                    feedbackFormatInstance.isLive = True
                    feedbackFormatInstance.save()
                else:
                    formatList = FeedbackFormat.objects.all()

                    return render(request, 'feedback/viewFeedbackFormat.html', {
                        'formatList': formatList,
                        'moreThanOneActive': True
                    })
            else:
                feedbackFormatInstance.isLive = False
                if FeedbackByStudents.objects.filter(feedbackFormat=feedbackFormatInstance).exists():
                    feedbackFormatInstance.isPast = True
                feedbackFormatInstance.save()

        elif 'updateFormatQuestions' in request.POST:
            feedbackFormatPk = request.POST.get('feedbackFormatPk')
            feedbackFormatInstance = FeedbackFormat.objects.get(pk=feedbackFormatPk)
            feedbackCategoryList = FeedbackCategory.objects.filter(feedbackFormat=feedbackFormatInstance)

            feedbackQuestionList = FeedbackQuestion.objects.filter(feedbackCategory__in=feedbackCategoryList).order_by(
                'feedbackCategory', 'questionNumber')

            for question in feedbackQuestionList:
                updatedQuestion = str(request.POST.get('question-' + str(question.pk)))
                question.question = updatedQuestion
                question.save()

        else:
            formatList = FeedbackFormat.objects.all()

            feedbackFormatInstance = ''
            for each in formatList:
                if str(request.POST.get('edit-' + str(each.pk))) == "Edit":
                    if each.isPast == True:
                        formatList = FeedbackFormat.objects.all()
                        return render(request, 'feedback/viewFeedbackFormat.html', {
                            'formatList': formatList,
                            'deleteApastRecord': True
                        })
                    feedbackFormatInstance = each
                    break
            feedbackCategoryList = FeedbackCategory.objects.filter(feedbackFormat=feedbackFormatInstance)

            feedbackQuestionList = []

            for category in feedbackCategoryList:
                temp = {}
                temp['category'] = category
                temp['questionList'] = FeedbackQuestion.objects.filter(feedbackCategory=category).order_by(
                    'questionNumber')
                feedbackQuestionList.append(temp)

            return render(request, 'feedback/editFeedbackFormat.html', {
                'feedbackQuestionList': feedbackQuestionList,
                'feedbackFormatInstance': feedbackFormatInstance,
            })

    formatList = FeedbackFormat.objects.all()
    if formatList.filter(isLive=True).count() == 0:
        return render(request, 'feedback/viewFeedbackFormat.html', {
            'formatList': formatList,
            'noActiveFormats': True
        })

    return render(request, 'feedback/viewFeedbackFormat.html', {
        'formatList': formatList
    })


@login_required
@permission_required('curriculum.can_choose_courses')
def doFeedback(request):
    studentInstance = student.objects.get(user=request.user)
    semesterInstance = getSemesterPlanofStudent(studentInstance)

    if not semesterInstance:
        msg = {
            'page_title': 'Access Withheld',
            'title': 'Access Withheld',
            'description': 'You have not assigned to any of the section. Contact your Programme Coordinator for immediate remedy.',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    courseList = getStaffCoursesForStudent(studentInstance)

    try:
        if 'feedbackCourseTab' in request.session:
            staffCoursePk = int(request.session['feedbackCourseTab'])
        else:
            staffCoursePk = int(courseList[0].pk)
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses Found',
            'description': 'Contact your Programme co-ordinator for more information',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'staffCoursePk' in request.POST:
            staffCoursePk = str(request.POST.get('staffCoursePk'))

        if 'submitFeedback' in request.POST:
            staffCoursePk = str(request.POST.get('hiddenStaffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            feedbackFormatPk = str(request.POST.get('feedbackFormatPk'))
            feedbackFormatInstance = FeedbackFormat.objects.get(pk=feedbackFormatPk)

            feedbackGradesList = FeedbackGrades.objects.filter(feedbackFormat=feedbackFormatInstance)
            feedbackCategoryList = FeedbackCategory.objects.filter(feedbackFormat=feedbackFormatInstance)
            feedbackQuestionList = FeedbackQuestion.objects.filter(feedbackCategory__in=feedbackCategoryList).order_by(
                'feedbackCategory', 'questionNumber')

            # condition to check if feedback has already been provided
            if FeedbackByStudents.objects.filter(staffCourse=staffCourseInstance, student=studentInstance,internal=3).exists() and \
                            FeedbackByStudents.objects.filter(staffCourse=staffCourseInstance,
                                                              student=studentInstance,internal=3).count() == feedbackFormatInstance.noOfQuestions:
                return render(request, 'feedback/doFeedback.html', {
                    'staffCourseInstance': staffCourseInstance,
                    'courseList': courseList,
                    'feedbackMarked': True
                })

            for feedback in feedbackQuestionList:
                gradePk = int(request.POST.get('question-' + str(feedback.pk)))
                feedbackGradeInstance = feedbackGradesList.get(pk=gradePk)

                feedbackByStudents = FeedbackByStudents(feedbackFormat=feedbackFormatInstance,feedbackQuestion=feedback, feedbackGrades=feedbackGradeInstance,
                                                        student=studentInstance, staffCourse=staffCourseInstance)
                feedbackByStudents.save()

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    if FeedbackFormat.objects.filter(isLive=True).count() == 0:
        msg = {
            'page_title': 'Access Denied',
            'title': 'Feedback page not Opened',
            'description': 'You will access the page if permission granted from COE. Contact your Faculties for more information',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if FeedbackByStudents.objects.filter(staffCourse=staffCourseInstance, student=studentInstance).exists():
        return render(request, 'feedback/doFeedback.html', {
            'staffCourseInstance': staffCourseInstance,
            'courseList': courseList,
            'feedbackMarked': True
        })

    feedbackFormatInstance = FeedbackFormat.objects.get(isLive=True)
    feedbackCategoryList = FeedbackCategory.objects.filter(feedbackFormat=feedbackFormatInstance)

    feedbackQuestionList = []

    for category in feedbackCategoryList:
        temp = {}
        temp['category'] = category
        temp['questionList'] = FeedbackQuestion.objects.filter(feedbackCategory=category).order_by(
            'questionNumber')
        feedbackQuestionList.append(temp)

    feedbackGradesList = FeedbackGrades.objects.filter(feedbackFormat=feedbackFormatInstance)
    if FeedbackByStudents.objects.filter(student=studentInstance,staffCourse=staffCourseInstance,internal=1).exists():
        if FeedbackByStudents.objects.filter(student=studentInstance,staffCourse=staffCourseInstance,internal=2).exists():
            internal=3
        else:
            internal=2
    else:
        internal=1

    return render(request, 'feedback/doFeedback.html', {
        'feedbackQuestionList': feedbackQuestionList,
        'feedbackGradesList': feedbackGradesList,
        'feedbackFormatInstance': feedbackFormatInstance,
        'staffCourseInstance': staffCourseInstance,
        'courseList': courseList,
        'internal':internal
    })


def canViewFeedback(user):
    if user.has_perm('marks.can_add_internal_marks') or \
            user.has_perm('curriculum.can_view_overall_reports'):
        return True

def GetFeedbackReport(staffCourseInstance):
    dictionary = {}
    feedbackList = []
    studentListPk = []
    studentList=[]
    feedbackQuestionGradeList = []
    studentsGivenFeedbacks=0
    dictionary['formatavail']=False
    dictionary['avail'] = False
    for a in FeedbackByStudents.objects.filter(staffCourse=staffCourseInstance):
        feedbackFormatInstance=a.feedbackFormat
        dictionary['formatavail'] = True
        break
    if dictionary['formatavail']==True:
        feedbackGradeList = FeedbackGrades.objects.filter(feedbackFormat=feedbackFormatInstance)
        feedbackCategoryList = FeedbackCategory.objects.filter(feedbackFormat=feedbackFormatInstance)

        feedbackQuestionList = FeedbackQuestion.objects.filter(feedbackCategory__in=feedbackCategoryList).order_by(
            'feedbackCategory', 'questionNumber')

        feedbackByStudents = FeedbackByStudents.objects.filter(feedbackQuestion__in=feedbackQuestionList,
                                                           staffCourse=staffCourseInstance)


        for eachStudentPk in feedbackByStudents.values_list('student', flat=True).distinct():
            dictionary['avail']=True
            studentListPk.append(eachStudentPk)
        studentsGivenFeedbacks = len(studentListPk)

        studentList = student.objects.filter(id__in=studentListPk).order_by('roll_no')

        for category in feedbackCategoryList:
            temp = {}
            temp['category'] = category
            gradeList = []
            for grade in feedbackGradeList:
                temp_list = {}
                temp_list['grade'] = grade
                temp_list['count'] = feedbackByStudents.filter(feedbackQuestion__feedbackCategory=category,
                                                           feedbackGrades=grade).count()
                gradeList.append(temp_list)
            temp['gradeList'] = gradeList
            feedbackQuestionGradeList.append(temp)


        for grade in feedbackGradeList:
            temp = {}
            temp['grade'] = grade
            temp['count'] = feedbackByStudents.filter(feedbackGrades=grade).count()
            feedbackList.append(temp)
    dictionary['feedbackList']=feedbackList
    dictionary['feedbackQuestionGradeList']=feedbackQuestionGradeList
    dictionary['studentsGivenFeedbacks']=studentsGivenFeedbacks
    dictionary['studentList']=studentList
    return dictionary

@login_required
@user_passes_test(canViewFeedback)
def feedbackReports(request):
    staffInstance = staff.objects.get(user=request.user)
    departmentList = getActiveDepartmentsForStaff(staffInstance)

    if not departmentList:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact Programme co-ordinator to ensure whether courses are properly alloted',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    departmentInstance = department.objects.get(pk=departmentList[0].pk)

    if request.method == 'POST':
        if 'departmentPk' in request.POST:
            departmentPk = int(request.POST.get('departmentPk'))
            departmentInstance = getDepartmentOfPk(departmentPk)

        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            departmentInstance = semesterInstance.department

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            departmentInstance = staffCourseInstance.department

    semesterList = getActiveSemesterTabsForStaff(staffInstance, departmentInstance)

    if not semesterList:
        return render(request, 'feedback/feedbackReports.html',
                      {
                          'noSemester': True,
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                      })

    semesterInstance = semesterList[0]['semesterInstance']

    if request.method == 'POST':
        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            departmentInstance = semesterInstance.department

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            semesterInstance = staffCourseInstance.semester
            departmentInstance = staffCourseInstance.department

    courseList = getActiveCoursesTabsForStaff(staffInstance, semesterInstance)

    try:
        if not 'staffCoursePk' in request.POST:
            staffCoursePk = courseList[0]['staffCourseInstance'].pk
    except:
        return render(request, 'feedback/feedbackReports.html',
                      {
                          'noCourses': True,
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                          'semesterInstance': semesterInstance,
                          'semesterList': semesterList,
                      })

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    if FeedbackFormat.objects.filter(isLive=True).count() == 0:
        msg = {
            'page_title': 'Access Denied',
            'title': 'No Feedback Format found!',
            'description': 'First set Feedback format, in order to view Student\'s feedback reports',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    feedbackFormatInstance = FeedbackFormat.objects.get(isLive=True)
    feedbackGradeList = FeedbackGrades.objects.filter(feedbackFormat=feedbackFormatInstance)
    feedbackCategoryList = FeedbackCategory.objects.filter(feedbackFormat=feedbackFormatInstance)

    feedbackQuestionList = FeedbackQuestion.objects.filter(feedbackCategory__in=feedbackCategoryList).order_by(
        'feedbackCategory', 'questionNumber')

    feedbackByStudents = FeedbackByStudents.objects.filter(feedbackQuestion__in=feedbackQuestionList,
                                                           staffCourse=staffCourseInstance)

    studentListPk = []
    for eachStudentPk in feedbackByStudents.values_list('student_id', flat=True).distinct():
        studentListPk.append(eachStudentPk)

    studentsGivenFeedbacks = len(studentListPk)

    studentList = student.objects.filter(id__in=studentListPk).order_by('roll_no')

    feedbackQuestionGradeList = []

    for category in feedbackCategoryList:
        temp = {}
        temp['category'] = category
        gradeList = []
        for grade in feedbackGradeList:
            temp_list = {}
            temp_list['grade'] = grade
            temp_list['count'] = feedbackByStudents.filter(feedbackQuestion__feedbackCategory=category,
                                                           feedbackGrades=grade).count()
            gradeList.append(temp_list)
        temp['gradeList'] = gradeList
        feedbackQuestionGradeList.append(temp)

    feedbackList = []

    for grade in feedbackGradeList:
        temp = {}
        temp['grade'] = grade
        temp['count'] = feedbackByStudents.filter(feedbackGrades=grade).count()
        feedbackList.append(temp)

    return render(request, 'feedback/feedbackReports.html',
                  {
                      'departmentList': departmentList,
                      'departmentInstance': departmentInstance,
                      'semesterInstance': semesterInstance,
                      'semesterList': semesterList,
                      'staffCourseInstance': staffCourseInstance,
                      'courseList': courseList,
                      'feedbackQuestionGradeList': feedbackQuestionGradeList,
                      'feedbackList': feedbackList,
                      'studentsGivenFeedbacks': studentsGivenFeedbacks,
                      'studentList': studentList
                  })


def getFeedbackReport(request, staffCoursePk):
    staffInstance = staff.objects.get(user=request.user)
    if not staff_course.objects.filter(pk=staffCoursePk):
        return {'noPermission': True}
    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    if not courseViewPermissionByStaff(staffCourseInstance, staffInstance):
        return {'noPermission': True}

    if FeedbackFormat.objects.filter(isLive=True).count() == 0:
        return {'noPermission': True}

    feedbackFormatInstance = FeedbackFormat.objects.get(isLive=True)
    feedbackGradeList = FeedbackGrades.objects.filter(feedbackFormat=feedbackFormatInstance)
    feedbackCategoryList = FeedbackCategory.objects.filter(feedbackFormat=feedbackFormatInstance)

    feedbackQuestionList = FeedbackQuestion.objects.filter(feedbackCategory__in=feedbackCategoryList).order_by(
        'feedbackCategory', 'questionNumber')

    feedbackByStudents = FeedbackByStudents.objects.filter(feedbackQuestion__in=feedbackQuestionList,
                                                           staffCourse=staffCourseInstance)

    studentListPk = []
    for eachStudentPk in feedbackByStudents.values_list('student_id', flat=True).distinct():
        studentListPk.append(eachStudentPk)

    studentsGivenFeedbacks = len(studentListPk)

    studentList = student.objects.filter(id__in=studentListPk).order_by('roll_no')

    feedbackQuestionGradeList = []

    for category in feedbackCategoryList:
        temp = {}
        temp['category'] = category
        gradeList = []
        for grade in feedbackGradeList:
            temp_list = {}
            temp_list['grade'] = grade
            temp_list['count'] = feedbackByStudents.filter(feedbackQuestion__feedbackCategory=category,
                                                           feedbackGrades=grade).count()
            gradeList.append(temp_list)
        temp['gradeList'] = gradeList
        feedbackQuestionGradeList.append(temp)

    feedbackList = []

    for grade in feedbackGradeList:
        temp = {}
        temp['grade'] = grade
        temp['count'] = feedbackByStudents.filter(feedbackGrades=grade).count()
        feedbackList.append(temp)

    context_data = {
        'staffCourseInstance': staffCourseInstance,
        'feedbackQuestionGradeList': feedbackQuestionGradeList,
        'feedbackList': feedbackList,
        'studentsGivenFeedbacks': studentsGivenFeedbacks,
        'studentList': studentList
    }

    return context_data


class FeedbackPdfReport(PDFTemplateView):
    template_name = "feedback/feedbackPdfReport.html"

    def get(self, request, *args, **kwargs):
        self.staffCoursePk = kwargs.pop('staffCoursePk')
        return super(FeedbackPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = getFeedbackReport(self.request,
                                         staffCoursePk=self.staffCoursePk
                                         )
        return super(FeedbackPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )