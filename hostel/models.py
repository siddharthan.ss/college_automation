from django.db import models
from simple_history.models import HistoricalRecords

from accounts.models import student, department, staff


# Create your models here.

class temp_hostel_student_details(models.Model):
    roll_no = models.CharField(unique=True, max_length=7)
    name = models.CharField(max_length=50)
    department = models.ForeignKey(department)
    current_semester = models.SmallIntegerField()
    is_registered = models.BooleanField(default=False)
    gender = models.CharField(choices=staff.GENDER_CHOICES, max_length=1)
    history = HistoricalRecords()


class mess_bill_duration(models.Model):
    start_date = models.DateField()
    end_date = models.DateField()
    history = HistoricalRecords()

    def __str__(self):
        return str(self.start_date.strftime("%b-%d,%Y")) + ' to ' + str(self.end_date.strftime("%b-%d,%Y"))


class mess_bill(models.Model):
    temp_student_id = models.ForeignKey(temp_hostel_student_details, null=True)
    student = models.ForeignKey(student, null=True)
    duration = models.ForeignKey(mess_bill_duration)
    no_of_days = models.SmallIntegerField()
    cost_per_day = models.FloatField()
    mess_charge = models.FloatField()
    extra_charge = models.FloatField()
    nrsc = models.FloatField()
    inam = models.FloatField()
    cleaning_charges = models.FloatField()
    security = models.FloatField()
    total = models.FloatField()
    round_up = models.FloatField()
    advance = models.FloatField(default=0)
    dues = models.FloatField(default=0)
    fine = models.FloatField(default=0)
    history = HistoricalRecords()

    class Meta:
        unique_together = (
            ('duration', 'temp_student_id', 'student')
        )


class common_rates(models.Model):
    duration = models.ForeignKey(mess_bill_duration)
    year = models.IntegerField()
    boys_girls = models.CharField(choices=staff.GENDER_CHOICES, max_length=1)
    cost_per_day = models.FloatField()
    extra_charge = models.FloatField()
    inam = models.FloatField()
    cleaning_charges = models.FloatField()
    security = models.FloatField()
    history = HistoricalRecords()
