from django import forms

from hostel.models import *


class enter_fee_form(forms.ModelForm):
    choosen_fields = [
        'advance',
        'cleaning_charges',
        'cost_per_day',
        'extra_charge',
        'fine',
        'inam',
        'dues',
        'no_of_days',
        'nrsc',
        'security',
    ]

    class Meta:
        model = mess_bill
        fields = [
            'no_of_days',
            'nrsc',
            'advance',
            'dues',
            'fine',
            'cost_per_day',
            'extra_charge',
            'inam',
            'cleaning_charges',
            'security',
        ]

        exclude = ['student', 'temp_student_id', 'duration', 'total', 'round_up', 'mess_charge']

    def __init__(self, *args, **kwargs):
        super(enter_fee_form, self).__init__(*args, **kwargs)

        for field in enter_fee_form.choosen_fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })


class enter_common_fee_form(forms.ModelForm):
    choosen_fields = [
        'cleaning_charges',
        'cost_per_day',
        'extra_charge',
        'inam',
        'security',
    ]

    class Meta:
        model = common_rates
        fields = [
            'cleaning_charges',
            'cost_per_day',
            'extra_charge',
            'inam',
            'security',
        ]

    def __init__(self, *args, **kwargs):
        super(enter_common_fee_form, self).__init__(*args, **kwargs)

        for field in enter_common_fee_form.choosen_fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control',
            })


class add_duration_form(forms.ModelForm):
    end_date = forms.DateField(input_formats=('%d/%m/%Y'))

    class Meta:
        model = mess_bill_duration
        exclude = []

    def __init__(self, *args, **kwargs):
        super(add_duration_form, self).__init__(*args, **kwargs)

        self.fields['start_date'].widget.attrs.update({
            'class': 'form-control',
        })

        self.fields['end_date'].widget.attrs.update({
            'class': 'form-control',
        })
