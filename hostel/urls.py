from django.conf.urls import url

from hostel.views.common_fees import common_mess_rates, CommonFeesCreateUpdateView
from hostel.views.duration import add_duration
from hostel.views.mees_bill_reports import fee_details, mess_bill_pdf, \
    custom_mess_bill_reports, custom_mess_bill_pdf
from hostel.views.mess_fees import MessFeesCreateUpdateView
from hostel.views.view_my_mess_bill import view_my_mess_bill

urlpatterns = [
    url(r'^fee_details$', fee_details, name="fee_details"),
    url(r'^enter_bill', MessFeesCreateUpdateView.as_view(), name="enter_bill"),
    url(r'^add_duration', add_duration, name="add_duration"),
    url(r'^common_rates', common_mess_rates, name="common_rates"),

    url(r'^view_my_mess_bill', view_my_mess_bill, name="view_my_mess_bill"),
    url(r'^edit_common_rates', CommonFeesCreateUpdateView.as_view(), name="edit_common_rates"),
    url(r'^mess_bill_pdf/(?P<batch_obj>\d+)/(?P<duration_id>\d+)/(?P<dept_acronym>[A-Z]+)/(?P<gender>M|F)',
        mess_bill_pdf.as_view(), name="mess_bill_pdf"),

    url(r'^custom_mess_bill_reports', custom_mess_bill_reports, name="custom_mess_bill_reports"),
    url(r'^custom_mess_bill_pdf', custom_mess_bill_pdf, name="custom_mess_bill_pdf"),

]
