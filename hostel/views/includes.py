from pprint import pprint

from hostel.models import temp_hostel_student_details


def get_list_of_temp_hostel_students(dept_inst, batch_inst, gender):
    curr_sem = 0
    if batch_inst.start_year == 2013:
        curr_sem = 8
    elif batch_inst.start_year == 2014:
        curr_sem = 6
    elif batch_inst.start_year == 2015:
        curr_sem = 4
    elif batch_inst.start_year == 2016:
        curr_sem = 2

    ret_query = temp_hostel_student_details.objects.filter(
        department=dept_inst
    ).filter(
        current_semester=curr_sem
    ).filter(
        is_registered=False
    ).filter(
        gender=gender
    )

    pprint(ret_query)

    return ret_query
