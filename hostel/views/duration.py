from datetime import datetime

from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render

from hostel.forms import add_duration_form
from hostel.models import mess_bill_duration


@login_required
@permission_required('hostel.can_enter_fee')
def add_duration(request):
    form = add_duration_form()

    if request.method == "POST":
        start_date = request.POST.get('start_date')
        end_date = request.POST.get('end_date')

        start_date = datetime.strptime(start_date, "%d/%m/%Y").strftime('%Y-%m-%d')
        end_date = datetime.strptime(end_date, "%d/%m/%Y").strftime('%Y-%m-%d')

        if start_date > end_date:
            modal = {
                'heading': 'Error',
                'body': 'Start date is greater than end date',
            }
            return render(request, 'hostel/add_duration.html', {'toggle_model': 'true', 'modal': modal, 'form': form})

        date_validation_query = mess_bill_duration.objects.filter(start_date__gte=start_date).filter(
            end_date__lte=end_date)

        if date_validation_query.exists():
            modal = {
                'heading': 'Error',
                'body': 'A duration in the specified date range already exists',
            }
            return render(request, 'hostel/add_duration.html',
                          {'toggle_model': 'true', 'modal': modal, 'form': form})

        mess_bill_duration.objects.create(
            start_date=start_date,
            end_date=end_date,
        )

        modal = {
            'heading': 'Success',
            'body': 'Successfully added duration',
        }
        return render(request, 'hostel/add_duration.html',
                      {'toggle_model': 'true', 'modal': modal, 'form': form})

    return render(request, 'hostel/add_duration.html', {'form': form})
