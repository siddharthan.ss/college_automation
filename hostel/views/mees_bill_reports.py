from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Max
from django.shortcuts import render
from easy_pdf.views import PDFTemplateView

from accounts.models import department, active_batches
from hostel.models import mess_bill_duration

from hostel.views.mess_fees import fee_details


class mess_bill_pdf(PDFTemplateView):
    template_name = "hostel/mess_bill_pdf.html"
    batch_obj = None
    dept_acronym = None
    duration_id = None
    gender = None

    def get(self, request, *args, **kwargs):
        self.batch_obj = kwargs.pop('batch_obj')
        self.dept_acronym = kwargs.pop('dept_acronym')
        self.duration_id = kwargs.pop('duration_id')
        self.gender = kwargs.pop('gender')
        return super(mess_bill_pdf, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = fee_details(self.request,
                                   s_batch=self.batch_obj,
                                   s_dept=self.dept_acronym,
                                   s_duration=self.duration_id,
                                   s_gender=self.gender,
                                   data_for_pdf=True
                                   )
        print(context_data)
        return super(mess_bill_pdf, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )


class custom_mess_bill_pdf(PDFTemplateView):
    template_name = "hostel/mess_bill_pdf.html"
    c_data = None

    def get_context_data(self, **kwargs):
        context_data = self.c_data
        print(context_data)
        return super(custom_mess_bill_pdf, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )


@login_required
@permission_required('hostel.can_enter_fee')
def custom_mess_bill_reports(request):
    all_deparments = department.objects.filter(is_core=True).order_by('acronym')

    all_durations = mess_bill_duration.objects.all()
    max_end_date = all_durations.aggregate(Max('end_date'))
    try:
        selected_duration = mess_bill_duration.objects.get(end_date=max_end_date['end_date__max'])
    except mess_bill_duration.DoesNotExist:
        modal = {
            'heading': 'Error',
            'body': 'Add a duration first',
        }
        return render(request, 'dashboard/dashboard.html',
                      {'toggle_model': 'true', 'modal': modal, })

    if request.method == "POST":
        selected_departments = request.POST.getlist('checked_departments')
        selected_years = request.POST.getlist('checked_year')
        selected_gender = request.POST.getlist('checked_gender')
        selected_duration = request.POST.get('selected_duration')

        print(locals())

        context_data_list = []
        for dept in all_deparments:
            print(dept.pk)
            print(selected_departments)
            if str(dept.pk) in selected_departments:
                print(locals())
                for year in selected_years:
                    # calculated odd and even sems
                    year = int(year)
                    even_semester = year * 2
                    odd_semester = year * 2 - 1
                    print(locals())
                    print(even_semester)
                    print(odd_semester)
                    active_batch_inst = active_batches.objects.filter(
                        department=dept
                    ).filter(
                        current_semester_number_of_this_batch__gte=odd_semester
                    ).filter(
                        current_semester_number_of_this_batch__lte=even_semester
                    ).get(
                        programme='UG'
                    )
                    print('active batch')
                    print(active_batch_inst)
                    for gender in selected_gender:
                        return_dict = fee_details(
                            request=request,
                            s_batch=active_batch_inst.batch.pk,
                            s_duration=selected_duration,
                            s_dept=dept.acronym,
                            s_gender=gender,
                            data_for_pdf=True,
                        )
                        context_data_list.append(return_dict)

    return render(request, 'hostel/choose_pdf_reports.html',
                  {
                      'all_durations': all_durations,
                      'all_departments': all_deparments,
                      'selected_duration': selected_duration,
                  })
