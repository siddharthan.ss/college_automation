from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Max
from django.shortcuts import render

from accounts.models import student
from hostel.models import mess_bill_duration, mess_bill


@login_required
@permission_required('hostel.can_view_own_fee')
def view_my_mess_bill(request):
    all_durations = mess_bill_duration.objects.all()
    max_end_date = all_durations.aggregate(Max('end_date'))
    try:
        selected_duration = mess_bill_duration.objects.get(end_date=max_end_date['end_date__max'])
    except mess_bill_duration.DoesNotExist:
        modal = {
            'heading': 'Error',
            'body': 'No mess bill available for you yet',
        }
        return render(request, 'dashboard/dashboard.html',
                      {'toggle_model': 'true', 'modal': modal, })

    if 'selected_duration' in request.POST:
        selected_duration_id = request.POST['selected_duration']
        selected_duration = mess_bill_duration.objects.get(pk=selected_duration_id)

    print(selected_duration)

    student_inst = student.objects.get(user=request.user)

    mess_bill_query = mess_bill.objects.filter(student=student_inst).filter(duration=selected_duration)

    if mess_bill_query.exists():
        mess_bill_inst = mess_bill_query.get()
    else:
        modal = {
            'heading': 'Error',
            'body': 'No mess bill available for you for the selected duration',
        }
        return render(request, 'dashboard/dashboard.html',
                      {'toggle_model': 'true', 'modal': modal, })

    return render(request, 'hostel/view_my_mess_bill.html',
                  {
                      'all_durations': all_durations,
                      'mess_bill_inst': mess_bill_inst,
                  })
