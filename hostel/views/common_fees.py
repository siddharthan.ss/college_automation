from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Max
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic.detail import SingleObjectTemplateResponseMixin
from django.views.generic.edit import ModelFormMixin, ProcessFormView

from hostel.forms import *


# Create your views here.

@login_required
@permission_required('hostel.can_enter_fee')
def common_mess_rates(request):
    all_durations = mess_bill_duration.objects.all()
    max_end_date = all_durations.aggregate(Max('end_date'))
    try:
        selected_duration = mess_bill_duration.objects.get(end_date=max_end_date['end_date__max'])
    except mess_bill_duration.DoesNotExist:
        modal = {
            'heading': 'Error',
            'body': 'Add a duration first',
        }
        return render(request, 'hostel/common_rates.html',
                      {'toggle_model': 'true', 'modal': modal, })

    if 'selected_duration' in request.POST:
        selected_duration_id = request.POST['selected_duration']
        selected_duration = mess_bill_duration.objects.get(pk=selected_duration_id)

    print(selected_duration)

    list_of_entries = []

    temp = {}
    temp['description'] = 'I Year Boys'
    temp['query_string'] = '?type=1b'
    common_rates_query = common_rates.objects.filter(duration=selected_duration).filter(boys_girls='M').filter(year=1)
    if common_rates_query.exists():
        temp['common_rates'] = common_rates_query.get()
    else:
        temp['common_rates'] = None
    list_of_entries.append(temp)

    temp = {}
    temp['description'] = 'I Year Girls'
    temp['query_string'] = '?type=1g'
    common_rates_query = common_rates.objects.filter(duration=selected_duration).filter(boys_girls='F').filter(year=1)
    if common_rates_query.exists():
        temp['common_rates'] = common_rates_query.get()
    else:
        temp['common_rates'] = None
    list_of_entries.append(temp)

    temp = {}
    temp['description'] = 'II Year Boys'
    temp['query_string'] = '?type=2b'
    common_rates_query = common_rates.objects.filter(duration=selected_duration).filter(boys_girls='M').filter(year=2)
    if common_rates_query.exists():
        temp['common_rates'] = common_rates_query.get()
    else:
        temp['common_rates'] = None
    list_of_entries.append(temp)

    temp = {}
    temp['description'] = 'II Year Girls'
    temp['query_string'] = '?type=1g'
    common_rates_query = common_rates.objects.filter(duration=selected_duration).filter(boys_girls='F').filter(year=2)
    if common_rates_query.exists():
        temp['common_rates'] = common_rates_query.get()
    else:
        temp['common_rates'] = None
    list_of_entries.append(temp)

    temp = {}
    temp['description'] = 'III Year Boys'
    temp['query_string'] = '?type=3b'
    common_rates_query = common_rates.objects.filter(duration=selected_duration).filter(boys_girls='M').filter(year=3)
    if common_rates_query.exists():
        temp['common_rates'] = common_rates_query.get()
    else:
        temp['common_rates'] = None
    list_of_entries.append(temp)

    temp = {}
    temp['description'] = 'III Year Girls'
    temp['query_string'] = '?type=3g'
    common_rates_query = common_rates.objects.filter(duration=selected_duration).filter(boys_girls='F').filter(year=3)
    if common_rates_query.exists():
        temp['common_rates'] = common_rates_query.get()
    else:
        temp['common_rates'] = None
    list_of_entries.append(temp)

    temp = {}
    temp['description'] = 'IV Year Boys'
    temp['query_string'] = '?type=4b'
    common_rates_query = common_rates.objects.filter(duration=selected_duration).filter(boys_girls='M').filter(year=4)
    if common_rates_query.exists():
        temp['common_rates'] = common_rates_query.get()
    else:
        temp['common_rates'] = None
    list_of_entries.append(temp)

    temp = {}
    temp['description'] = 'IV Year Girls'
    temp['query_string'] = '?type=4g'
    common_rates_query = common_rates.objects.filter(duration=selected_duration).filter(boys_girls='F').filter(year=4)
    if common_rates_query.exists():
        temp['common_rates'] = common_rates_query.get()
    else:
        temp['common_rates'] = None
    list_of_entries.append(temp)

    return render(request, 'hostel/common_rates.html',
                  {
                      'list_of_entries': list_of_entries,
                      'all_durations': all_durations,
                      'selected_duration': selected_duration,
                  }
                  )


class CommonFeesCreateUpdateView(SingleObjectTemplateResponseMixin, ModelFormMixin,
                                 ProcessFormView):
    template_name = 'hostel/edit_common_rates.html'
    success_url = '/common_rates'
    form_class = enter_common_fee_form

    def dispatch(self, request, *args, **kwargs):
        return super(CommonFeesCreateUpdateView, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        try:
            got_type = self.request.GET['type']
            if got_type == '1b':
                year = 1
                gender = 'M'
            elif got_type == '1g':
                year = 1
                gender = 'F'
            elif got_type == '2b':
                year = 2
                gender = 'M'
            elif got_type == '2g':
                year = 2
                gender = 'F'
            elif got_type == '3b':
                year = 3
                gender = 'M'
            elif got_type == '3g':
                year = 3
                gender = 'F'
            elif got_type == '4b':
                year = 4
                gender = 'M'
            elif got_type == '4g':
                year = 4
                gender = 'F'
            duration_inst = mess_bill_duration.objects.get(pk=self.request.GET['duration_id'])
            obj = common_rates.objects.filter(year=year).filter(boys_girls=gender).get(duration=duration_inst)
            return obj
        except:
            return None

    def form_valid(self, form):
        got_type = self.request.GET['type']
        if got_type == '1b':
            year = 1
            gender = 'M'
        elif got_type == '1g':
            year = 1
            gender = 'F'
        elif got_type == '2b':
            year = 2
            gender = 'M'
        elif got_type == '2g':
            year = 2
            gender = 'F'
        elif got_type == '3b':
            year = 3
            gender = 'M'
        elif got_type == '3g':
            year = 3
            gender = 'F'
        elif got_type == '4b':
            year = 4
            gender = 'M'
        elif got_type == '4g':
            year = 4
            gender = 'F'

        form.instance.year = year
        form.instance.boys_girls = gender

        duration_instance = mess_bill_duration.objects.get(pk=self.request.GET.get('duration_id'))

        form.instance.duration = duration_instance

        self.object = form.save()

        return HttpResponseRedirect(self.get_success_url())

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(CommonFeesCreateUpdateView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(CommonFeesCreateUpdateView, self).post(request, *args, **kwargs)
