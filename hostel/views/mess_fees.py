from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Max
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic.detail import SingleObjectTemplateResponseMixin
from django.views.generic.edit import ModelFormMixin, ProcessFormView

from accounts.models import department, batch, student
from curriculum.views import get_current_batches
from curriculum.views import get_list_of_hostel_students
from curriculum.views.common_includes import *
from hostel.forms import enter_fee_form
from hostel.models import mess_bill_duration, mess_bill, common_rates, temp_hostel_student_details
from hostel.views.includes import get_list_of_temp_hostel_students


def keyfn(x):
    return x['roll_no']


@login_required
@permission_required('hostel.can_enter_fee')
def fee_details(request, s_dept=None, s_batch=None, s_duration=None, s_gender=None, data_for_pdf=False):
    all_deparments = department.objects.filter(is_core=True).order_by('acronym')
    selected_department = all_deparments.first()

    if 'selected_dept_tab' in request.POST:
        selected_department_acronym = request.POST['selected_dept_tab']
        selected_department = department.objects.get(acronym=selected_department_acronym)

    if s_dept:
        selected_department = department.objects.get(acronym=s_dept)

    print(selected_department)

    temp_batches_of_department = get_current_batches(selected_department)
    batches_of_department = []
    print("batches of department")
    print((batches_of_department))
    for entry in temp_batches_of_department:
        if entry['programme'] == 'UG':
            batches_of_department.append(entry)

    print((batches_of_department))
    if batches_of_department:
        selected_batch = batches_of_department[0]['batch_obj']
    else:
        selected_batch = None

    if 'selected_batch' in request.POST:
        selected_batch = request.POST['selected_batch']
        # selected_batch = batch.objects.get(pk=selected_batch_id)

    if s_batch:
        selected_batch = s_batch

    print(selected_batch)

    selected_batch_instance = batch.objects.get(pk=selected_batch)

    all_durations = mess_bill_duration.objects.all()
    max_end_date = all_durations.aggregate(Max('end_date'))
    try:
        selected_duration = mess_bill_duration.objects.get(end_date=max_end_date['end_date__max'])
    except mess_bill_duration.DoesNotExist:
        modal = {
            'heading': 'Error',
            'body': 'Add a duration first',
        }
        return render(request, 'dashboard/dashboard.html',
                      {'toggle_model': 'true', 'modal': modal, })

    if 'selected_duration' in request.POST:
        selected_duration_id = request.POST['selected_duration']
        selected_duration = mess_bill_duration.objects.get(pk=selected_duration_id)

    if s_duration:
        selected_duration = mess_bill_duration.objects.get(pk=s_duration)

    print(selected_duration)

    selected_gender = 'M'
    if 'selected_gender' in request.POST:
        selected_gender = request.POST['selected_gender']

    if s_gender:
        selected_gender = s_gender

    print(selected_gender)

    list_of_entries = []

    list_of_hostel_students = get_list_of_hostel_students(selected_department, selected_batch_instance, selected_gender)

    for each_entry in list_of_hostel_students:
        temp_dict = {}
        temp_dict['roll_no'] = each_entry.roll_no
        temp_dict['name'] = str(each_entry)
        temp_dict['is_registered'] = True
        mess_bill_query = mess_bill.objects.filter(student=each_entry).filter(duration=selected_duration)
        if mess_bill_query.exists():
            temp_dict['fee_entries'] = mess_bill_query.get()
        else:
            if each_entry.current_semester % 2 == 0:
                year = (each_entry.current_semester) / 2
            else:
                year = (each_entry.current_semester + 1) / 2
            common_rates_query = common_rates.objects.filter(
                duration=selected_duration
            ).filter(
                year=year
            ).filter(
                boys_girls=each_entry.gender
            )
            if common_rates_query.exists():
                temp_dict['fee_entries'] = common_rates_query.get()

        temp_table_inst = temp_hostel_student_details.objects.filter(
            roll_no=each_entry.roll_no
        )

        if temp_table_inst.exists():
            temp_table_inst = temp_table_inst.get()
            if temp_table_inst.is_registered == False:
                temp_table_inst.is_registered = True
                temp_table_inst.save()

        list_of_entries.append(temp_dict)

    list_of_temp_host_students = get_list_of_temp_hostel_students(selected_department, selected_batch_instance,
                                                                  selected_gender)

    for each_entry in list_of_temp_host_students:
        temp_dict = {}
        temp_dict['roll_no'] = each_entry.roll_no
        temp_dict['name'] = str(each_entry.name)
        temp_dict['is_registered'] = False
        mess_bill_query = mess_bill.objects.filter(temp_student_id=each_entry).filter(duration=selected_duration)
        if mess_bill_query.exists():
            temp_dict['fee_entries'] = mess_bill_query.get()
        else:
            if each_entry.current_semester % 2 == 0:
                year = (each_entry.current_semester) / 2
            else:
                year = (each_entry.current_semester + 1) / 2
            common_rates_query = common_rates.objects.filter(
                duration=selected_duration
            ).filter(
                year=year
            ).filter(
                boys_girls=each_entry.gender
            )
            if common_rates_query.exists():
                temp_dict['fee_entries'] = common_rates_query.get()

        pprint(temp_dict)

        list_of_entries.append(temp_dict)

    print('hostel_students')
    list_of_entries = sorted(list_of_entries, key=keyfn)
    pprint(list_of_entries)

    if data_for_pdf == True:
        active_batches_inst = active_batches.objects.filter(department=selected_department).get(batch=selected_batch)
        current_sem_of_given_batch = active_batches_inst.current_semester_number_of_this_batch
        print(current_sem_of_given_batch)
        if int(current_sem_of_given_batch) % 2 == 0:
            year = (current_sem_of_given_batch) / 2
        else:
            year = (current_sem_of_given_batch + 1) / 2

        description_string = ''
        if year == 1:
            description_string = 'I Year ' + str(selected_department.acronym) + ' '
        elif year == 2:
            description_string = 'II Year ' + str(selected_department.acronym) + ' '
        elif year == 3:
            description_string = 'III Year ' + str(selected_department.acronym) + ' '
        elif year == 4:
            description_string = 'IV Year ' + str(selected_department.acronym) + ' '

        if selected_gender == 'M':
            description_string += 'Boys'
        elif selected_gender == 'F':
            description_string += 'Girls'

        context_data = {
            'all_departments': all_deparments,
            'selected_department': selected_department,
            'batches_of_department': batches_of_department,
            'selected_batch': selected_batch,
            'list_of_hostel_students': list_of_entries,
            'all_durations': all_durations,
            'selected_duration': selected_duration,
            'selected_gender': selected_gender,
            'description_string': description_string,
        }
        return context_data

    return render(request, 'hostel/fee_details.html', {
        'all_departments': all_deparments,
        'selected_department': selected_department,
        'batches_of_department': batches_of_department,
        'selected_batch': int(selected_batch),
        'list_of_hostel_students': list_of_entries,
        'all_durations': all_durations,
        'selected_duration': selected_duration,
        'selected_gender': selected_gender,
    })


class MessFeesCreateUpdateView(SingleObjectTemplateResponseMixin, ModelFormMixin,
                               ProcessFormView):
    template_name = 'hostel/enter_bill.html'
    success_url = '/fee_details'
    form_class = enter_fee_form

    def get_initial(self):
        super(MessFeesCreateUpdateView, self).get_initial()

        if self.object == None:
            try:
                student_query = student.objects.get(
                    roll_no=self.request.GET['roll_no']
                )
                if student_query.current_semester % 2 == 0:
                    year = (student_query.current_semester) / 2
                else:
                    year = (student_query.current_semester + 1) / 2
                common_rates_query = common_rates.objects.filter(
                    duration=self.request.GET['duration_id']
                ).filter(
                    year=year
                ).filter(
                    boys_girls=student_query.gender
                )
            except student.DoesNotExist:
                temp_student_query = temp_hostel_student_details.objects.get(
                    roll_no=self.request.GET['roll_no']
                )
                if temp_student_query.current_semester % 2 == 0:
                    year = (temp_student_query.current_semester) / 2
                else:
                    year = (temp_student_query.current_semester + 1) / 2
                common_rates_query = common_rates.objects.filter(
                    duration=self.request.GET['duration_id']
                ).filter(
                    year=year
                ).filter(
                    boys_girls=temp_student_query.gender
                )

            if common_rates_query.exists():
                cost_per_day = common_rates_query.get().cost_per_day
                extra_charge = common_rates_query.get().extra_charge
                security = common_rates_query.get().security
                cleaning_charges = common_rates_query.get().cleaning_charges
                inam = common_rates_query.get().inam

                self.initial = {
                    "cost_per_day": cost_per_day,
                    "extra_charge": extra_charge,
                    "security": security,
                    "cleaning_charges": cleaning_charges,
                    "inam": inam,
                }
        return self.initial

    def get_object(self, queryset=None):
        try:
            student_query = student.objects.get(
                roll_no=self.request.GET['roll_no']
            )
            obj = mess_bill.objects.filter(
                student=student_query
            ).get(
                duration=self.request.GET['duration_id']
            )
            return obj
        except:
            try:
                temp_student_query = temp_hostel_student_details.objects.get(
                    roll_no=self.request.GET['roll_no']
                )
                obj = mess_bill.objects.filter(
                    temp_student_id=temp_student_query
                ).get(
                    duration=self.request.GET['duration_id']
                )
                return obj
            except:
                return None

    def form_valid(self, form):
        try:
            student_instance = student.objects.get(roll_no=self.request.GET.get('roll_no'))
            form.instance.student = student_instance
        except student.DoesNotExist:
            temp_student_instance = temp_hostel_student_details.objects.get(roll_no=self.request.GET.get('roll_no'))
            form.instance.temp_student_id = temp_student_instance

        duration_instance = mess_bill_duration.objects.get(pk=self.request.GET.get('duration_id'))

        form.instance.duration = duration_instance

        form.instance.mess_charge = form.cleaned_data['no_of_days'] * form.cleaned_data['cost_per_day']
        form.instance.total = form.instance.mess_charge + form.cleaned_data['extra_charge'] + form.cleaned_data[
            'nrsc'] + form.cleaned_data['inam'] + form.cleaned_data['cleaning_charges'] + \
                              form.cleaned_data['security'] + form.cleaned_data['dues'] + form.cleaned_data['fine'] - \
                              form.cleaned_data['advance']

        form.instance.round_up = form.instance.total
        self.object = form.save()

        return HttpResponseRedirect(self.get_success_url())

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(MessFeesCreateUpdateView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(MessFeesCreateUpdateView, self).post(request, *args, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        return super(MessFeesCreateUpdateView, self).dispatch(request, *args, **kwargs)
