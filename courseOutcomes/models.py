from django.db import models

from accounts.models import student, batch, staff
from curriculum.models import department, courses, semester, staff_course


class CoPattern(models.Model):
    staffCourse = models.ForeignKey(staff_course)
    unitTest = models.PositiveSmallIntegerField()
    questionNumber = models.PositiveSmallIntegerField()
    userPrespectiveQuestionNumber = models.CharField(max_length=10)
    coMapped = models.PositiveSmallIntegerField()
    maximumMark = models.FloatField()

    class Meta:
        unique_together = (
            'staffCourse', 'unitTest', 'questionNumber'
        )


class CoMarks(models.Model):
    coPattern = models.ForeignKey(CoPattern)
    student = models.ForeignKey(student)
    markObtained = models.FloatField()

    class Meta:
        unique_together = (
            'coPattern', 'student'
        )


class CoWeightage(models.Model):
    staffCourse = models.ForeignKey(staff_course)
    unitTest = models.PositiveSmallIntegerField()
    coMapped = models.PositiveSmallIntegerField()
    weightage = models.PositiveSmallIntegerField()