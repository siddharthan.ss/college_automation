from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.shortcuts import render
from django.http import JsonResponse

from accounts.models import department, staff, student
from curriculum.models import semester, staff_course
from courseOutcomes.models import CoPattern, CoMarks

from courseOutcomes.views.courseOutcomes import canViewCourseOutcomes, getCoMarksOfUnitTest

from common.utils.ReportTabsUtil import getActiveDepartmentsForStaff, getActiveSemesterTabsForStaff, \
    getActiveCoursesTabsForStaff
from common.utils.studentUtil import getSemesterPlanofStudent, getStaffCoursesForStudent
from common.API.departmentAPI import getDepartmentOfPk


@login_required
@user_passes_test(canViewCourseOutcomes)
def coReports(request):
    staffInstance = staff.objects.get(user=request.user)
    departmentList = getActiveDepartmentsForStaff(staffInstance)

    if not departmentList:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact Programme co-ordinator to ensure whether courses are properly alloted',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    departmentInstance = department.objects.get(pk=departmentList[0].pk)

    if request.method == 'POST':
        if 'departmentPk' in request.POST:
            departmentPk = int(request.POST.get('departmentPk'))
            departmentInstance = getDepartmentOfPk(departmentPk)

        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            departmentInstance = semesterInstance.department

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            departmentInstance = staffCourseInstance.department

    semesterList = getActiveSemesterTabsForStaff(staffInstance, departmentInstance)

    if not semesterList:
        return render(request, 'courseOutcomes/coReports.html',
                      {
                          'noSemester': True,
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                      })

    semesterInstance = semesterList[0]['semesterInstance']

    if request.method == 'POST':
        if 'semesterPk' in request.POST:
            semesterPk = int(request.POST.get('semesterPk'))
            semesterInstance = semester.objects.get(pk=semesterPk)
            departmentInstance = semesterInstance.department

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
            semesterInstance = staffCourseInstance.semester
            departmentInstance = staffCourseInstance.department

    courseList = getActiveCoursesTabsForStaff(staffInstance, semesterInstance)

    try:
        if not 'staffCoursePk' in request.POST:
            staffCoursePk = courseList[0]['staffCourseInstance'].pk
    except:
        return render(request, 'courseOutcomes/coReports.html',
                      {
                          'noCourses': True,
                          'departmentList': departmentList,
                          'departmentInstance': departmentInstance,
                          'semesterInstance': semesterInstance,
                          'semesterList': semesterList,
                      })

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    if request.method == 'POST':
        if request.is_ajax():
            unitTestNumber = int(request.POST.get('unitTestNumberAjax'))
            staffCoursePk = int(request.POST.get('staffCoursePkAjax'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getCoMarksOfUnitTest(staffCourseInstance, unitTestNumber)

            return JsonResponse(dictionary)

    return render(request, 'courseOutcomes/coReports.html',
                  {
                      'departmentList': departmentList,
                      'departmentInstance': departmentInstance,
                      'semesterInstance': semesterInstance,
                      'semesterList': semesterList,
                      'staffCourseInstance': staffCourseInstance,
                      'courseList': courseList
                  })


@login_required
@permission_required('curriculum.can_choose_courses')
def studentCoMark(request):
    studentInstance = student.objects.get(user=request.user)

    semesterInstance = getSemesterPlanofStudent(studentInstance)

    if not semesterInstance:
        msg = {
            'page_title': 'Access Withheld',
            'title': 'Access Withheld',
            'description': 'You have not assigned to any of the section. Contact your Programme Coordinator for immediate remedy.',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    courseList = getStaffCoursesForStudent(studentInstance)

    try:
        staffCoursePk = int(courseList[0].pk)
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses Found',
            'description': 'Contact your Programme co-ordinator for more information',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    coMarks = []

    unitTestNumberList = [1, 2, 3]

    for unitTestNumber in unitTestNumberList:
        coList = []
        totalMarks = 0
        obtainedMarks = 0
        markEntered = False

        for entry in CoPattern.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).order_by(
                'questionNumber'):
            for data in CoMarks.objects.filter(coPattern=entry, student=studentInstance):
                temp = {}
                temp['coPattern'] = entry
                temp['markObtained'] = data.markObtained
                coList.append(temp)
                obtainedMarks = obtainedMarks + data.markObtained
                totalMarks = totalMarks + entry.maximumMark
                markEntered = True

        temp = {}
        temp['markEntered'] = markEntered
        temp['coList'] = coList
        temp['totalMarks'] = totalMarks
        temp['obtainedMarks'] = obtainedMarks

        coMarks.append(temp)

    return render(request, 'courseOutcomes/studentCoMark.html', {
        'courseList': courseList,
        'staffCourseInstance': staffCourseInstance,
        'coMarks': coMarks
    })
