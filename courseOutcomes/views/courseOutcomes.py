from decimal import Decimal

from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.shortcuts import render
from easy_pdf.views import PDFTemplateView
from django.http import JsonResponse

from curriculum.views.common_includes import get_current_course_allotment_with_display_text, get_list_of_students

from accounts.models import staff, student
from curriculum.models import staff_course
from courseOutcomes.models import CoPattern, CoMarks, CoWeightage
from marks.models import internalmarks, internalmarksold

from common.utils.ReportTabsUtil import courseViewPermissionByStaff


@login_required
@permission_required('marks.can_add_internal_marks')
def setCoPattern(request):
    staffInstance = staff.objects.get(user=request.user)

    courseList = []
    for each in get_current_course_allotment_with_display_text(staffInstance):
        if each['staff_course_instance'].course.subject_type == 'T':
            courseList.append(each)

    try:
        staffCoursePk = int(courseList[0]['staff_course_instance'].pk)
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course that have quiz marks',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if request.is_ajax():
            unitTestNumber = int(request.POST.get('unitTestNumber'))
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))

            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            list = []
            dictionary = {}
            if CoPattern.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).exists():
                dictionary['avail'] = 'true'
                for entry in CoPattern.objects.filter(staffCourse=staffCourseInstance,
                                                      unitTest=unitTestNumber).order_by('questionNumber'):
                    temp = {}
                    temp['questionNumber'] = entry.userPrespectiveQuestionNumber
                    temp['coMapped'] = entry.coMapped
                    if float(entry.maximumMark).is_integer():
                        temp['maximumMark'] = int(entry.maximumMark)
                    else:
                        temp['maximumMark'] = entry.maximumMark
                    list.append(temp)
                dictionary['list'] = list
            else:
                dictionary['avail'] = 'false'
            return JsonResponse(dictionary)

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))

        if 'totalQuestions' in request.POST:
            totalQuestions = int(request.POST.get('totalQuestions'))
            unitTestNumber = int(request.POST.get('unitTestNumber'))
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePkOnSet'))

            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            counter = 1
            subDivisionList = []
            while counter <= totalQuestions:
                # print('subdivision ' + str(counter) + ':' + str(request.POST.get('subQuestion' + str(counter))))
                subCounter = 1
                list = []
                while subCounter <= int(request.POST.get('subQuestion' + str(counter))):
                    temp = {}
                    temp['coMapped'] = str(request.POST.get('coMapped' + str(counter) + '-' + str(subCounter)))
                    temp['maximumMark'] = str(request.POST.get('maximumMark' + str(counter) + '-' + str(subCounter)))
                    list.append(temp)
                    subCounter = subCounter + 1
                subDivisionList.append(list)
                counter = counter + 1

            questionCounter = 0
            userPrespectiveNumberCharacter = 0
            for entry in subDivisionList:
                userPrespectiveNumberCharacter = userPrespectiveNumberCharacter + 1
                alphacounter = 'a'
                for each in entry:
                    questionCounter = questionCounter + 1
                    if len(entry) == 1:
                        userPrespectiveNumber = userPrespectiveNumberCharacter
                    else:
                        userPrespectiveNumber = str(userPrespectiveNumberCharacter) + str(alphacounter)

                    coPatternInstance = CoPattern(
                        staffCourse=staffCourseInstance,
                        unitTest=unitTestNumber,
                        questionNumber=questionCounter,
                        userPrespectiveQuestionNumber=userPrespectiveNumber,
                        coMapped=int(each['coMapped']),
                        maximumMark=float(each['maximumMark'])
                    )
                    coPatternInstance.save()

                    # print(str(userPrespectiveNumber) + '-' + str(alphacounter) + '==>' + str(each))
                    alphacounter = chr(ord(alphacounter) + 1)

        if 'deleteUnitTest' in request.POST:
            staffCoursePk = int(request.POST.get('deleteStaffCoursePk'))
            deleteUnitTestNumber = int(request.POST.get('deleteUnitTest'))

            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getCoMarksOfUnitTest(staffCourseInstance, deleteUnitTestNumber)

            if dictionary['studentMarkAvail'] == 'true':
                return render(request, 'courseOutcomes/preDeleteConfirmationCoPattern.html',
                              {
                                  'staffCoursePk': staffCoursePk,
                                  'unitTestNumber': deleteUnitTestNumber,
                                  'dictionary': dictionary
                              })
            else:
                CoPattern.objects.filter(staffCourse=staffCourseInstance, unitTest=deleteUnitTestNumber).delete()

        if 'confirmDeleteStaffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('confirmDeleteStaffCoursePk'))
            deleteUnitTestNumber = int(request.POST.get('confirmDeleteUnitTest'))

            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            CoPattern.objects.filter(staffCourse=staffCourseInstance, unitTest=deleteUnitTestNumber).delete()

    return render(request, 'courseOutcomes/setCoPattern.html',
                  {
                      'staffCoursePk': int(staffCoursePk),
                      'courseList': courseList
                  })


def getCoMarksOfUnitTest(staffCourseInstance, unitTestNumber):
    semesterInstance = staffCourseInstance.semester
    courseInstance = staffCourseInstance.course
    coPatternList = []
    dictionary = {}
    dictionary['avail'] = 'false'
    dictionary['studentAvail'] = 'false'
    dictionary['studentMarkAvail'] = 'false'

    if CoPattern.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).exists():
        dictionary['avail'] = 'true'
        for entry in CoPattern.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).order_by(
                'questionNumber'):
            temp = {}
            temp['pk'] = entry.pk
            temp['userPrespectiveQuestionNumber'] = entry.userPrespectiveQuestionNumber
            temp['questionNumber'] = entry.questionNumber
            temp['coMapped'] = entry.coMapped
            if float(entry.maximumMark).is_integer():
                temp['maximumMark'] = int(entry.maximumMark)
            else:
                temp['maximumMark'] = entry.maximumMark
            coPatternList.append(temp)

        dictionary['totalQuestions'] = len(coPatternList)

        studentList = get_list_of_students(semesterInstance, courseInstance)

        currentBatch = courseInstance.regulation.start_year

        coMarkList = []
        for eachStudent in studentList:
            dictionary['studentAvail'] = 'true'
            marksByAddingCo = 0
            coList = []
            temp = {}
            temp['pk'] = str(eachStudent['student_obj'].pk)
            temp['studentName'] = str(eachStudent['student_obj'])
            temp['registerNumber'] = str(eachStudent['student_obj'].roll_no)
            for entry in coPatternList:
                for data in CoMarks.objects.filter(coPattern=entry['pk'], student=eachStudent['student_obj']):
                    dictionary['studentMarkAvail'] = 'true'
                    markObtained = data.markObtained
                    if float(markObtained).is_integer():
                        markObtained = int(markObtained)
                    coList.append(markObtained)
                    marksByAddingCo = marksByAddingCo + data.markObtained
            temp['coList'] = coList

            totalMarks = -1

            if currentBatch >= 2016:
                for take in internalmarks.objects.filter(course=courseInstance, semester=semesterInstance,
                                                         student=eachStudent['student_obj']):
                    if unitTestNumber == 1:
                        totalMarks = take.ut1
                    elif unitTestNumber == 2:
                        totalMarks = take.ut2
                    elif unitTestNumber == 3:
                        totalMarks = take.ut3
            else:
                for take in internalmarksold.objects.filter(course=courseInstance, semester=semesterInstance,
                                                            student=eachStudent['student_obj']):
                    if unitTestNumber == 1:
                        totalMarks = take.ut1
                    elif unitTestNumber == 2:
                        totalMarks = take.ut2
                    elif unitTestNumber == 3:
                        totalMarks = take.ut3

            if totalMarks == None:
                temp['totalMarks'] = 'AB'
            elif totalMarks != -1:
                temp['totalMarks'] = totalMarks
            else:
                temp['totalMarks'] = 'Not entered'
            if marksByAddingCo == totalMarks:
                temp['markEqualsCo'] = 'true'
            coMarkList.append(temp)

        dictionary['coMarkList'] = coMarkList

        dictionary['coPatternList'] = coPatternList

    return dictionary


@login_required
@permission_required('marks.can_add_internal_marks')
def addCoMarks(request):
    staffInstance = staff.objects.get(user=request.user)
    unitTestNumber = ''

    courseList = []
    for each in get_current_course_allotment_with_display_text(staffInstance):
        if each['staff_course_instance'].course.subject_type == 'T':
            courseList.append(each)

    try:
        staffCoursePk = int(courseList[0]['staff_course_instance'].pk)
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if request.is_ajax():
            unitTestNumber = int(request.POST.get('unitTestNumberAjax'))
            staffCoursePk = int(request.POST.get('staffCoursePkAjax'))
            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            dictionary = getCoMarksOfUnitTest(staffCourseInstance, unitTestNumber)

            return JsonResponse(dictionary)

        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))

        elif 'editUnitTestNumber' in request.POST:
            unitTestNumber = int(request.POST.get('editUnitTestNumber'))
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))

            studentPkList = []
            staffCourseInstance = staff_course.objects.get(id=staffCoursePk)
            semesterInstance = staffCourseInstance.semester
            courseInstance = staffCourseInstance.course

            studentList = get_list_of_students(semesterInstance, courseInstance)
            for stud in studentList:
                studentPkList.append(stud['student_obj'].pk)

            required_edit = ''

            for each in studentPkList:
                if str(request.POST.get(str(each))) == 'Edit':
                    required_edit = int(each)

            studentInstance = student.objects.get(pk=required_edit)

            currentBatch = studentInstance.batch.start_year

            internalMarkEntered = False
            isStudentAbsent = False
            totalMarks = 0

            if currentBatch >= 2016:
                for entry in internalmarks.objects.filter(course=courseInstance, student=studentInstance,
                                                          semester=semesterInstance):
                    internalMarkEntered = True
                    if unitTestNumber == 1:
                        totalMarks = entry.ut1
                    elif unitTestNumber == 2:
                        totalMarks = entry.ut2
                    elif unitTestNumber == 3:
                        totalMarks = entry.ut3

                    if totalMarks == None:
                        isStudentAbsent = True
            else:
                for entry in internalmarksold.objects.filter(course=courseInstance, student=studentInstance,
                                                             semester=semesterInstance):
                    internalMarkEntered = True
                    if unitTestNumber == 1:
                        totalMarks = entry.ut1
                    elif unitTestNumber == 2:
                        totalMarks = entry.ut2
                    elif unitTestNumber == 3:
                        totalMarks = entry.ut3

                    if totalMarks == None:
                        isStudentAbsent = True

            coPatternList = []
            for entry in CoPattern.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).order_by(
                    'questionNumber'):
                temp = {}
                temp['pk'] = entry.pk
                temp['userPrespectiveQuestionNumber'] = entry.userPrespectiveQuestionNumber
                temp['questionNumber'] = entry.questionNumber
                temp['coMapped'] = entry.coMapped
                if float(entry.maximumMark).is_integer():
                    temp['maximumMark'] = int(entry.maximumMark)
                else:
                    temp['maximumMark'] = entry.maximumMark
                for take in CoMarks.objects.filter(coPattern=entry, student=studentInstance):
                    markObtained = take.markObtained
                    if float(markObtained).is_integer():
                        markObtained = int(markObtained)
                    temp['markObtained'] = markObtained
                coPatternList.append(temp)

            return render(request, 'courseOutcomes/updateCoMarks.html',
                          {
                              'staffCourseInstance': staffCourseInstance,
                              'coPatternList': coPatternList,
                              'unitTestNumber': unitTestNumber,
                              'studentInstance': studentInstance,
                              'internalMarkEntered': internalMarkEntered,
                              'totalMarks': totalMarks,
                              'isStudentAbsent': isStudentAbsent,
                              'totalQuestions': len(coPatternList),
                          })

        elif 'goBack' in request.POST:
            unitTestNumber = int(request.POST.get('goBackUnitTestNumber'))
            staffCoursePk = str(request.POST.get('goBackStaffCoursePk'))

        elif 'studentPk' in request.POST:
            unitTestNumber = int(request.POST.get('hiddenUnitTestNumberOnUpdate'))
            staffCoursePk = str(request.POST.get('hiddenStaffCoursePkOnUpdate'))
            studentPk = int(request.POST.get('studentPk'))
            totalQuestions = int(request.POST.get('totalQuestions'))

            studentInstance = student.objects.get(pk=studentPk)

            counter = 1
            while counter <= totalQuestions:
                coPatternPk = str(request.POST.get('pattern-' + str(counter)))
                markObtained = float(request.POST.get('mark-' + str(counter)))

                coPatternInstance = CoPattern.objects.get(pk=coPatternPk)

                if CoMarks.objects.filter(coPattern=coPatternInstance, student=studentInstance):
                    coMarksIntance = CoMarks.objects.get(coPattern=coPatternInstance, student=studentInstance)
                    coMarksIntance.markObtained = markObtained
                    coMarksIntance.save()
                else:
                    coMarksIntance = CoMarks(coPattern=coPatternInstance, student=studentInstance,
                                             markObtained=markObtained)
                    coMarksIntance.save()
                counter = counter + 1

    return render(request, 'courseOutcomes/addCoMarks.html',
                  {
                      'staffCoursePk': int(staffCoursePk),
                      'courseList': courseList,
                      'unitTestNumber': unitTestNumber
                  })


def canViewCourseOutcomes(user):
    if user.has_perm('marks.can_add_internal_marks') or \
            user.has_perm('curriculum.can_view_overall_reports'):
        return True


@login_required
@user_passes_test(canViewCourseOutcomes)
def viewGraph(request, staffCoursePk, unitTestNumber):
    staffInstance = staff.objects.get(user=request.user)

    try:
        staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    except:
        msg = {
            'page_title': 'Access Denied',
            'title': 'No Permission',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to this course to gain access',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if not courseViewPermissionByStaff(staffCourseInstance, staffInstance):
        msg = {
            'page_title': 'Access Denied',
            'title': 'No Permission',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to this course to gain access',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    coPatternList = []
    for entry in CoPattern.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).order_by(
            'questionNumber'):
        temp = {}
        temp['pk'] = entry.pk
        temp['userPrespectiveQuestionNumber'] = entry.userPrespectiveQuestionNumber
        temp['questionNumber'] = entry.questionNumber
        temp['coMapped'] = entry.coMapped
        if float(entry.maximumMark).is_integer():
            temp['maximumMark'] = int(entry.maximumMark)
        else:
            temp['maximumMark'] = entry.maximumMark
        coPatternList.append(temp)

    semesterInstance = staffCourseInstance.semester
    courseInstance = staffCourseInstance.course

    studentList = get_list_of_students(semesterInstance, courseInstance)

    if not studentList:
        msg = {
            'page_title': 'No Students',
            'title': 'No Students opted',
            'description': 'Make sure students are enrolled for the course',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    coNumbers = []
    for each in coPatternList:
        if each['coMapped'] not in coNumbers:
            coNumbers.append(each['coMapped'])

    mark = []
    for co in coNumbers:
        sum = 0
        studentMark = 0
        for each in coPatternList:
            if co == each['coMapped']:
                sum = sum + each['maximumMark']
                for data in CoMarks.objects.filter(coPattern=each['pk']):
                    studentMark = studentMark + data.markObtained
                    if float(studentMark).is_integer():
                        studentMark = int(studentMark)

        temp = {}
        temp['coMapped'] = co
        temp['coMaximumMark'] = sum
        temp['studentMark'] = studentMark
        temp['studentAverage'] = format(studentMark / len(studentList), '.2f')

        if CoWeightage.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber, coMapped=co):
            coWeightageInstance = CoWeightage.objects.get(staffCourse=staffCourseInstance, unitTest=unitTestNumber,
                                                          coMapped=co)
            temp['studentAverageWeight'] = format((sum / 100) * coWeightageInstance.weightage, '.2f')
            temp['coWeightage'] = coWeightageInstance.weightage
        else:
            msg = {
                'page_title': 'No CO Weightage',
                'title': 'CO Weightage - Not found',
                'description': 'Set CO Weightage in order to generate graph',
            }
            return render(request, 'prompt_pages/error_page_base.html', {'message': msg})
        mark.append(temp)

    markList = []
    for eachStudent in studentList:
        list = []
        for co in coNumbers:
            studentMark = 0
            for each in coPatternList:
                if co == each['coMapped']:
                    for data in CoMarks.objects.filter(coPattern=each['pk'], student=eachStudent['student_obj']):
                        studentMark = studentMark + data.markObtained
                        if float(studentMark).is_integer():
                            studentMark = int(studentMark)
            temp = {}
            temp['coMapped'] = co
            temp['studentMark'] = studentMark
            list.append(temp)
        markList.append(list)

    coAttainedList = []

    for co in mark:
        attained = 0
        for eachStudent in markList:
            for each in eachStudent:
                if each['coMapped'] == co['coMapped']:
                    if each['studentMark'] >= float(co['studentAverageWeight']):
                        attained = attained + 1
        temp = {}
        temp['coNumber'] = co['coMapped']
        temp['coAttained'] = attained
        temp['coNotAttained'] = len(studentList) - attained
        coAttainedList.append(temp)

    coList = []

    for each in mark:
        for every in coAttainedList:
            if each['coMapped'] == every['coNumber']:
                temp = {}
                temp['coNumber'] = each['coMapped']
                temp['coMaximumMark'] = each['coMaximumMark']
                temp['studentMark'] = each['studentMark']
                temp['studentAverage'] = each['studentAverage']
                temp['studentAverageWeight'] = each['studentAverageWeight']
                temp['coWeightage'] = each['coWeightage']
                temp['coAttained'] = every['coAttained']
                temp['coNotAttained'] = every['coNotAttained']
                coList.append(temp)

    try:
        space_split = 400 / len(mark)
    except:
        space_split = 40
    graphValues = []

    xlen = 50
    for each in mark:
        temp = {}
        temp['height'] = float(each['studentAverage']) * 5
        temp['yAxis'] = 300 - float(temp['height'])
        temp['xAxis'] = xlen + (space_split / 2) - 14
        temp['coPosition'] = float(temp['xAxis']) + 14
        temp['coMapped'] = each['coMapped']
        temp['coMaxYAxis'] = 300 - (float(each['coMaximumMark']) * 5)
        temp['coMaxStart'] = float(temp['xAxis']) - 5
        temp['coMaxEnd'] = float(temp['xAxis']) + 33
        temp['coMaxCoPostion'] = float(temp['coMaxYAxis']) - 20
        xlen = xlen + space_split
        graphValues.append(temp)

    return render(request, 'courseOutcomes/viewGraph.html',
                  {
                      'coList': coList,
                      'graphValues': graphValues,
                      'staffCourseInstance': staffCourseInstance,
                      'unitTestNumber': unitTestNumber,
                      'co_attained_list': coAttainedList
                  })


@login_required
@user_passes_test(canViewCourseOutcomes)
def setCoWeightage(request):
    staffInstance = staff.objects.get(user=request.user)
    unitTestNumber = -1
    courseList = []
    for entry in get_current_course_allotment_with_display_text(staffInstance):
        if entry['staff_course_instance'].course.subject_type != 'P':
            courseList.append(entry)

    try:
        staffCoursePk = courseList[0]['staff_course_instance'].pk
    except:
        msg = {
            'page_title': 'No courses',
            'title': 'No courses alloted',
            'description': 'Contact your Programme co-ordinator to ensure whether you have been properly alloted to a course. For practicals click on Set Lab Pattern from the side pane to set pattern for practicals and then click on Add Lab Marks to add marks',
        }
        return render(request, 'prompt_pages/error_page_base.html', {'message': msg})

    if request.method == 'POST':
        if 'staffCoursePk' in request.POST:
            staffCoursePk = int(request.POST.get('staffCoursePk'))

        if 'selectedUnitTestNumber' in request.POST:
            unitTestNumber = int(request.POST.get('selectedUnitTestNumber'))
            print('unitTestNumber:' + str(unitTestNumber))
            staffCoursePk = int(request.POST.get('hiddenStaffCoursePk'))

        if 'totalCo' in request.POST:
            unitTestNumber = request.POST.get('hiddenUnitTestNumberOnWeightage')
            staffCoursePk = request.POST.get('hiddenStaffCoursePkOnWeightage')
            totalCo = int(request.POST.get('totalCo'))

            staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

            counter = 1
            while counter <= totalCo:
                coMapped = int(request.POST.get('co-' + str(counter)))
                weightageInPercentage = int(request.POST.get('weightage-' + str(counter)))

                if CoWeightage.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber,
                                              coMapped=coMapped):
                    coWeightageInstance = CoWeightage.objects.get(staffCourse=staffCourseInstance,
                                                                  unitTest=unitTestNumber, coMapped=coMapped)
                    coWeightageInstance.weightage = weightageInPercentage
                    coWeightageInstance.save()
                else:
                    coWeightageInstance = CoWeightage(staffCourse=staffCourseInstance, unitTest=unitTestNumber,
                                                      coMapped=coMapped, weightage=weightageInPercentage)
                    coWeightageInstance.save()

                counter = counter + 1

    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)

    if unitTestNumber != -1:
        coPatternList = []
        for entry in CoPattern.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).order_by(
                'questionNumber'):
            temp = {}
            temp['pk'] = entry.pk
            temp['userPrespectiveQuestionNumber'] = entry.userPrespectiveQuestionNumber
            temp['questionNumber'] = entry.questionNumber
            temp['coMapped'] = entry.coMapped
            if float(entry.maximumMark).is_integer():
                temp['maximumMark'] = int(entry.maximumMark)
            else:
                temp['maximumMark'] = entry.maximumMark
            coPatternList.append(temp)

        if len(coPatternList) == 0:
            return render(request, 'courseOutcomes/setCoWeightage.html', {
                'noPattern': True,
                'staffCoursePk': int(staffCoursePk),
                'staffCourseInstance': staffCourseInstance,
                'courseList': courseList,
                'unitTestNumber': int(unitTestNumber)
            })

        coNumbers = []
        removeDuplicateCo = []
        for each in coPatternList:
            if each['coMapped'] not in removeDuplicateCo:
                removeDuplicateCo.append(each['coMapped'])
                temp = {}
                temp['coMapped'] = each['coMapped']
                if CoWeightage.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber,
                                              coMapped=each['coMapped']):
                    coWeightageInstance = CoWeightage.objects.get(staffCourse=staffCourseInstance,
                                                                  unitTest=unitTestNumber, coMapped=each['coMapped'])
                    temp['weightage'] = coWeightageInstance.weightage
                coNumbers.append(temp)

        return render(request, 'courseOutcomes/setCoWeightage.html', {
            'totalCo': len(coNumbers),
            'coNumbers': coNumbers,
            'staffCoursePk': int(staffCoursePk),
            'staffCourseInstance': staffCourseInstance,
            'courseList': courseList,
            'unitTestNumber': int(unitTestNumber)
        })

    return render(request, 'courseOutcomes/setCoWeightage.html', {
        'staffCoursePk': int(staffCoursePk),
        'staffCourseInstance': staffCourseInstance,
        'courseList': courseList,
        'unitTestNumber': int(unitTestNumber)
    })


@login_required
@user_passes_test(canViewCourseOutcomes)
def coPdfData(request, staffCoursePk, unitTestNumber):
    staffInstance = staff.objects.get(user=request.user)
    staffCourseInstance = staff_course.objects.get(pk=staffCoursePk)
    semesterInstance = staffCourseInstance.semester
    courseInstance = staffCourseInstance.course

    if not courseViewPermissionByStaff(staffCourseInstance, staffInstance):
        context_data = {
            'noPermission': True
        }
        return context_data

    coPatternList = []
    totalCoMarks = 0
    for entry in CoPattern.objects.filter(staffCourse=staffCourseInstance, unitTest=unitTestNumber).order_by(
            'questionNumber'):
        temp = {}
        temp['pk'] = entry.pk
        temp['userPrespectiveQuestionNumber'] = entry.userPrespectiveQuestionNumber
        temp['questionNumber'] = entry.questionNumber
        temp['coMapped'] = entry.coMapped
        if float(entry.maximumMark).is_integer():
            temp['maximumMark'] = int(entry.maximumMark)
        else:
            temp['maximumMark'] = entry.maximumMark
        totalCoMarks = totalCoMarks + entry.maximumMark
        coPatternList.append(temp)

    studentList = get_list_of_students(semesterInstance, courseInstance)

    coNumbers = []
    for each in coPatternList:
        if each['coMapped'] not in coNumbers:
            coNumbers.append(each['coMapped'])

    coMarks = []
    for co in coNumbers:
        mark_count = 0
        for each in coPatternList:
            if co == each['coMapped']:
                mark_count = mark_count + each['maximumMark']
        coMarks.append(mark_count)

    studentMarkList = []
    for eachStudent in studentList:
        coList = []
        temp = {}
        studentCoMarks = []
        studentCoMarkAverage = []
        for co in coNumbers:
            co_mark = 0
            studentMark = 0
            for each in coPatternList:
                if co == each['coMapped']:
                    co_mark = co_mark + each['maximumMark']
                    for data in CoMarks.objects.filter(coPattern=each['pk'], student=eachStudent['student_obj']):
                        studentMark = studentMark + data.markObtained
            if float(studentMark).is_integer():
                studentMark = int(studentMark)
            studentCoMarks.append(studentMark)
            studentCoMarkAverage.append(Decimal(format((studentMark / co_mark) * 100, '.2f')).normalize())
        temp['studentCoMarks'] = studentCoMarks
        temp['studentCoMarkAverage'] = studentCoMarkAverage
        temp['studentName'] = str(eachStudent['student_obj'])
        temp['registerNumber'] = eachStudent['student_obj'].roll_no
        for each in coPatternList:
            for data in CoMarks.objects.filter(coPattern=each['pk'], student=eachStudent['student_obj']):
                markObtained = data.markObtained
                if float(markObtained).is_integer():
                    markObtained = int(markObtained)
                coList.append(markObtained)

        totalMarks = 0
        if courseInstance.regulation.start_year >= 2016:
            for take in internalmarks.objects.filter(course=courseInstance, student=eachStudent['student_obj'],
                                                     semester=semesterInstance):
                if int(unitTestNumber) == 1:
                    totalMarks = take.ut1
                elif int(unitTestNumber) == 2:
                    totalMarks = take.ut2
                elif int(unitTestNumber) == 3:
                    totalMarks = take.ut3
        else:
            for take in internalmarksold.objects.filter(course=courseInstance, student=eachStudent['student_obj'],
                                                        semester=semesterInstance):
                if int(unitTestNumber) == 1:
                    totalMarks = take.ut1
                elif int(unitTestNumber) == 2:
                    totalMarks = take.ut2
                elif int(unitTestNumber) == 3:
                    totalMarks = take.ut3

        if totalMarks == None:
            totalMarks = 'AB'

        temp['totalMarks'] = totalMarks
        temp['coList'] = coList
        studentMarkList.append(temp)

    columnWidth = 75 / (len(coPatternList) + len(coNumbers) + 1 + len(coNumbers))

    context_data = {
        'staffCoursePk': staffCoursePk,
        'staffCourseInstance': staffCourseInstance,
        'studentMarkList': studentMarkList,
        'coPatternList': coPatternList,
        'coNumbers': coNumbers,
        'coMarks': coMarks,
        'totalCoMarks': totalCoMarks,
        'columnWidth': columnWidth,
    }

    return context_data


class COPdfReport(PDFTemplateView):
    template_name = "courseOutcomes/coPdfReport.html"

    def get(self, request, *args, **kwargs):
        self.staffCoursePk = kwargs.pop('staffCoursePk')
        self.unitTestNumber = kwargs.pop('unitTestNumber')
        return super(COPdfReport, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = coPdfData(self.request,
                                 staffCoursePk=self.staffCoursePk,
                                 unitTestNumber=self.unitTestNumber
                                 )
        return super(COPdfReport, self).get_context_data(
            pagesize="A4",
            context=context_data,
            **kwargs
        )
