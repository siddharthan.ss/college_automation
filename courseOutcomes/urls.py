from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from courseOutcomes.views.courseOutcomes import setCoPattern, addCoMarks, setCoWeightage, viewGraph, COPdfReport
from courseOutcomes.views.courseOutcomesReports import coReports, studentCoMark

urlpatterns = [
    url(r'^set_co_pattern/$', setCoPattern, name="setCoPattern"),
    url(r'^add_co_marks/$', addCoMarks, name="addCoMarks"),
    url(r'^set_co_weightage/$', setCoWeightage, name="setCoWeightage"),
    url(r'^view_graph/(?P<staffCoursePk>[0-9]+)/(?P<unitTestNumber>[1-3])/$', viewGraph, name="viewGraph"),
    url(r'^pdf_co_marks/(?P<staffCoursePk>[0-9]+)/(?P<unitTestNumber>[1-3])', login_required(COPdfReport.as_view()),
        name='pdfCoMarks'),
    url(r'^co_reports/$', coReports, name="coReports"),
    url(r'^my_co_marks/$', studentCoMark, name="studentCoMark"),
]
